# UnitTestingKi

## Create a test file

The files under test fut.c/.h call functions from calls.h

- [x] First make sure Ruby is installed on your system (if it's not already).
- [x] Then, from a command prompt: `$ gem install ceedling`
- [x] Under UnitTestingKi/test/{duplicate the same path as fut wihin /src}
- [x] Create **test_fut.c**
- [ ] File **template** shall be as below
- [ ] Build and run only **your tests** `$ ceedling test:test_fut`
- [ ] Final check regression testing for the whole project build and **run all tests** `$ ceedling -T`



```
#include "unity.h"
#include "fut.h"
#include "mock_calls.h"

void setUp(void)

{
    // These functions is run before each of the test functions in the test file.
    // You can use this to adjust variables static to the fut before running teh test
}

void tearDown(void)
{
    // These functions is run ater each of the test functions in the test file.
}

void test_name_of_your_first_test(void)

{
    // For example
    callsFunction_IgnoreAndReturn(true);
    TEST_ASSERT_EQUAL(true, futFunction());  
}
```

## Create a module for TDD

This will create your test file ** and the source and header project file:**

`$ ceedling module:create[module_path/file_name]`

## Erase all generated build files before commit and pull-request

Although build folders content is ignored better clean before you leave:

`$ ceedling clobber`
