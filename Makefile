#!/usr/bin/env make

NAME = pcore

SRC_DIR = src
OBJ_DIR = build
BIN_DIR = bin

ifeq ($(OS),Windows_NT)
	SDK_VERSION := 11.3.1_5262

	MAPFILE := "$(BIN_DIR)/$(NAME).map"

	SDK_PATH := "C:\NXP\MCUXoressoIDE_$(SDK_VERSION)\ide"

	CC 			:= $(SDK_PATH)\tools\bin\arm-none-eabi-gcc.exe
	AR 			:= $(SDK_PATH)\tools\bin\arm-none-eabi-gcc-ar.exe
	OBJCOPY 	:= $(SDK_PATH)\tools\bin\arm-none-eabi-objcopy.exe
	CHECKSUM 	:= $(SDK_PATH)\binaries\checksum.exe

	RM 		:= del /F /Q
	RMDIR 	:= del /F /Q
	MKDIR 	:= mkdir
else
	WSLENV ?= notwsl
	ifdef WSLENV
		MAPFILE := "$(BIN_DIR)/$(NAME).map"

		ifneq ($(CI),true)
			SDK_VERSION := 11.4.0_6224
			SDK_PATH := "/usr/local/share/nxp/sdk/v$(SDK_VERSION)"
		else
			SDK_VERSION := 11.4.0_6237
			SDK_PATH := "/usr/local/mcuxpressoide-$(SDK_VERSION)/ide"
		endif

		CC 			:= $(SDK_PATH)/tools/bin/arm-none-eabi-gcc
		AR 			:= $(SDK_PATH)/tools/bin/arm-none-eabi-gcc-ar
		OBJCOPY 	:= $(SDK_PATH)/tools/bin/arm-none-eabi-objcopy
		CHECKSUM 	:= $(SDK_PATH)/binaries/checksum
	else
		SDK_VERSION := 11.3.1_5262

		MAPFILE := \"$(BIN_DIR)/$(NAME).map\"

		SDK_PATH := "C:/NXP/MCUXpressoIDE_$(SDK_VERSION)/ide"

		EXEC := powershell.exe

		CC 			:= $(EXEC) $(SDK_PATH)/tools/bin/arm-none-eabi-gcc.exe
		AR 			:= $(EXEC) $(SDK_PATH)/tools/bin/arm-none-eabi-gcc-ar.exe
		OBJCOPY 	:= $(EXEC) $(SDK_PATH)/tools/bin/arm-none-eabi-objcopy.exe
		CHECKSUM 	:= $(EXEC) $(SDK_PATH)/binaries/checksum.exe
	endif

	RM 		:= rm -f
	RMDIR 	:= rm -rf
	MKDIR 	:= mkdir -p
endif

CFLAGS =
CFLAGS += -g
CFLAGS += -O0
CFLAGS += -mcpu=cortex-m3
CFLAGS += -mthumb
CFLAGS += -fmessage-length=0
CFLAGS += -fno-common
CFLAGS += -fno-builtin
CFLAGS += -ffunction-sections
CFLAGS += -fdata-sections
CFLAGS += -fmerge-constants
CFLAGS += -fstack-usage
CFLAGS += -specs=$(SDK_PATH)/tools/arm-none-eabi/lib/nano.specs
CFLAGS += -Wall

CFLAGS_I :=
CFLAGS_I += -I$(SRC_DIR)
CFLAGS_I += $(addprefix -I,$(shell find $(SRC_DIR) -type d))
CFLAGS += $(CFLAGS_I)

CFLAGS_D :=
CFLAGS_D += -DCORE_M3
CFLAGS_D += -D__LPC15XX__
CFLAGS_D += -DNDEBUG
CFLAGS_D += -DDONT_ENABLE_SWVTRACECLK
CFLAGS_D += -D__NEWLIB__
CFLAGS_D += -D__CODE_RED
CFLAGS_D += -DENVIRONMENT_TARGET=1
CFLAGS_D += -DENVIRONMENT_PC=0
CFLAGS += $(CFLAGS_D)

LDFLAGS :=
LDFLAGS += -nostdlib
LDFLAGS += -mcpu=cortex-m3
LDFLAGS += -mthumb
LDFLAGS += -Xlinker -Map=$(MAPFILE)
LDFLAGS += -Xlinker --gc-sections
LDFLAGS += -Xlinker -print-memory-usage
LDFLAGS += -L pcore_image
LDFLAGS += -T "pcore_pcore_image.ld"

SRC_FILES_C = $(SRC_FILES_C_APP)

SRC_FILES_C_APP :=
SRC_FILES_C_APP += $(shell find $(SRC_DIR) -type f -name "*.c")

OBJ_FILES_C_APP := $(SRC_FILES_C_APP:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)

OBJ_FILES :=
OBJ_FILES += $(OBJ_FILES_C_APP)

DEP_FILES := $(OBJ_FILES:%.o=%.o.d)

LIBS :=
LIBS += -lm

.PHONY: clean

$(BIN_DIR)/$(NAME)-image.bin: $(BIN_DIR)/$(NAME).axf
	$(OBJCOPY) -v -O binary $< $@
	$(CHECKSUM) -p LPC1519 -d $@

$(BIN_DIR)/$(NAME).axf: $(OBJ_FILES) | $(BIN_DIR)
	$(CC) $^ $(LDFLAGS) -o $@ $(LIBS)

clean:
	-$(RMDIR) $(OBJ_DIR) $(BIN_DIR)

$(sort $(OBJ_DIR) $(BIN_DIR) $(dir $(OBJ_FILES))): %:
	$(MKDIR) $@

$(OBJ_FILES_C_APP): $(OBJ_DIR)/%.o: $(SRC_DIR)/%.c | $(dir $(OBJ_FILES_C_APP))
	$(CC) -c $(CFLAGS) -DCOMPILE_IMAGE -MMD -MP -MF $@.d -MT $@ -MT $@.d -o $@ $<

-include $(DEP_FILES)
