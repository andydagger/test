/*
==========================================================================
 Name        : bootloader.h
 Project     : pcore
 Path        : /pcore/src/bootloader.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 13 Mar 2018
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

#ifndef BOOTLOADER_H_
#define BOOTLOADER_H_

#define BOOT_ATTEMPTS_LIMIT 3

void bootloader_main(void);

//-----------------------------     BOOTLOADER ERRORS     -------------------------

enum bootloader_error_flags{
    REMOVING_OLD_FILE,
    CREATING_OLD_FILE,
    WRITING_OLD_FILE,
    SEEK_OLD_FILE,
    TRUNC_OLD_FILE,
    CLOSE_OLD_FILE,
    OLD_IM_LENGTH_NULL,
    IM_LENGTH_NULL,
    OPEN_IM_FILE,
    SEEK_IM_FILE,
    READ_IM_FILE,
    CLOSE_IM_FILE,
    PREP_SECTOR_FOR_ERASE,
    ERASE_SECTOR,
    PREP_SECTOR_FOR_WRITE,
    RAM_TO_FLASH,
    DELETE_NEW_IMAGE,
    DELETE_OLD_IMAGE,
    RENAME_NEW_IMAGE,
    OPEN_LOG,
    SEEK_LOG,
    READ_LOG,
    WRITE_LOG,
    CLOSE_LOG,
    DELETE_LOG,
    LOG_CORRUPT,
    BOOT_ATTEMPTS,
    OPEN_OLD_IM_FILE,
    REV_OLD_IM_LENGTH_NULL,
    CLOSE_OLD_IM_FILE,
    REVERTING,
    UPDATING,
    DELETING_NEW
};

#endif /* BOOTLOADER_H_ */
