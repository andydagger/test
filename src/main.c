/*
 ==========================================================================
 Name        : main.c
 Project     : pcore
 Path        : /pcore/src/main.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 23 Aug 2017
 Description : Project main source code includes the main loop.
               The main function of the main loop is to service each
               branch of the firmware at regular interval.
 Notes       : Import of NewLib Nano Version 2.4.0
 ==========================================================================
 */

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "chip.h"
#include "hw.h"
#include "bootloader.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "im_main.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
// ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Main function calling image or bootloader main
 * @param[in] void
 * @return void
 *******************************************************************************/
int main(void){

    __enable_irq();
    hw_setup();

#ifdef COMPILE_IMAGE
    image_main_new();
#else
    bootloader_main();
#endif

    return 1;

}


/********************* EOF ****************************************************/
