/*
==========================================================================
 Name        : bootloader.c
 Project     : pcore
 Path        : /pcore/src/bootloader.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 13 Mar 2018
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/
#include "chip.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "config_pinout.h"
#include "led.h"
#include "debug.h"
#include "hw.h"
#include "ff.h"
#include "config_fatfs.h"
#include "diskio.h"
#include "flash.h"
#include "iap.h"
#include "relay.h"
#include "common.h"
#include "bootloader.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static FIL old_im_file;
static FIL new_im_file;
static FIL boot_log_file;
static FATFS fs;           /* Filesystem object */

static UINT f_wr_bytes = 0; // not used by required to avoid passing NULL pointer
static UINT f_rd_bytes = 0; // not used by required to avoid passing NULL pointer


/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static bool mount_fat_drive(void);
static bool check_for_new_fw_file_available(void);
static bool update_code(FIL * p_file,
                        const char * file_name);
static void boot_image( uint32_t image_address);
static bool copy_code(  FIL* p_file,
                        uint32_t source_address,
                        uint32_t target_code_sector);

static void report_error(uint32_t error_code);
static bool delete_new_image(void);
static bool new_image_is_available(void);
static void process_new_image(void);
static bool local_image_is_safe(void);
static void regenerate_bootlog(void);
static void day_burner(void);
static bool bootlog_is_valid(void);

/*****************************************************************************
 * Public functions
 ****************************************************************************/


void bootloader_main(void) {

    // Switch on red led
    led_off(GREEN);
    led_on(RED);

    debug_printf(BOOT, "\r\n");
    debug_printf(BOOT, "************************************************\r\n");
    debug_printf(BOOT, "\r\n");

    // if the drive is mounted and a new fw imgae is waiting
    if( new_image_is_available() == true ){
        debug_printf(BOOT, "A new fw image is awaiting processing...\r\n");
        led_on(GREEN);
        process_new_image();
        debug_printf(BOOT, "The new fw image image has been copied locally.\r\n");
    }
    //
    else{
        if( local_image_is_safe() == false){
            debug_printf(BOOT, "Loaded image is corrupt >> activate day-burner.\r\n");
            day_burner();
        }else{
            debug_printf(BOOT, "Loaded image deemed safe >> boot-up...\r\n");
        }
    }


    // boot-up local image
    debug_printf(BOOT, "Booting up loaded image...\r\n");
   boot_image(IMAGE_ADDRESS);

   // done
   debug_printf(BOOT, "!! SHOULD NEVER REACH THIS POINT !!\r\n");
   while (1);

    return;
}

/*******************************************************************************
 * @brief Checks drive availability and presence of new fw image
 * @param[in] void
 * @return bool true if drive AND new image available and false otherwise.
 *******************************************************************************/
static bool new_image_is_available(void){

    // first check that the drive can be mounted
    if( mount_fat_drive() == true ){
        // then check that a new fw image is waiting
        if( check_for_new_fw_file_available() == true ){
            return true;
        }
    }
    // If either of the tests above fail return false
    return false;
}

/*******************************************************************************
 * @brief Process the new firmware image
 * @param[in] void
 * @return void
 *******************************************************************************/
static void process_new_image(void){
    uint8_t boot_attempts;

    // Make sure there is a valid boot log file to work with
    if( bootlog_is_valid() == false){
        regenerate_bootlog();
    }

    // open log file - should already exist
    catch_file_err_report_void(OPEN_LOG,
                            f_open( &boot_log_file,
                                    BOOT_LOG_FILE_NAME,
                                    FA_READ | FA_WRITE ));
    catch_file_err_report_void(READ_LOG,
                            f_read( &boot_log_file,
                                    (uint8_t *)&boot_attempts,
                                    1,
                                    &f_rd_bytes));

    if (boot_attempts >= (uint8_t)BOOT_ATTEMPTS_LIMIT) {
        boot_attempts = 0;
        debug_printf(BOOT, "New Image corrupt - Reverting.\r\n");
        catch_err_report_void( DELETING_NEW, delete_new_image() );
        catch_err_report_void(  REVERTING,
                                update_code(    &old_im_file,
                                                FW_OLD_FILE_NAME) );
    } else {
        boot_attempts++;
        debug_printf(BOOT, "boot log attempt = %d\r\n", boot_attempts);
        catch_err_report_void(     UPDATING,
                                update_code(    &new_im_file,
                                                FW_NEW_FILE_NAME) );
    }

    // move bootlog file ptr back to start
    catch_file_err_report_void(SEEK_LOG, f_lseek( &boot_log_file, 0) );

    // overwrite previous value
    catch_file_err_report_void(WRITE_LOG,
                            f_write( &boot_log_file,
                                    (uint8_t *)&boot_attempts,
                                    1,
                                    &f_wr_bytes));

    catch_file_err_report_void(CLOSE_LOG, f_close(&boot_log_file) );

    return;
}

/*******************************************************************************
 * @brief Verifies the integrity of the local image
 * @param[in] void
 * @return bool with true for valid image
 *******************************************************************************/
static bool local_image_is_safe(void){

    // TODO: Implement CRC check of the local image
    return true;
}

/*******************************************************************************
 * @brief Attempt to mount drive
 * @param[in] void
 * @return true if drive available
 *******************************************************************************/
static bool mount_fat_drive(void){
    return ( f_mount(&fs, "", 1) == FR_OK ) ? true : false;
}

/*******************************************************************************
 * @brief Delete new image file
 * @param[in] void
 * @return true if ok
 *******************************************************************************/
static bool delete_new_image(void){

    f_close( &new_im_file); // cannot remove an open file
    catch_file_error_report(DELETE_NEW_IMAGE,
                            f_unlink((const char *)FW_NEW_FILE_NAME));
    debug_printf(BOOT, "Deleted new image file.\r\n" );
    return true;
}

/*******************************************************************************
 * @brief Checks if there is a new fw image file available
 * @param[in] void
 * @return true if this is the case
 *******************************************************************************/
static bool check_for_new_fw_file_available(void){
    return (    f_stat( (const char *)FW_NEW_FILE_NAME, NULL ) == FR_OK ) ?
                true :
                false;
}

/*******************************************************************************
 * @brief Turns node into day-burner (until next power-cycle)
 * @param[in] void
 * @return void
 *******************************************************************************/
static void day_burner(void){
    uint8_t count_down = 20;
    // 20 sec delay minimum
    do{
        hw_kick_watchdog();
        sys_delay_ms(1000);
    }while(--count_down > 0);

    // close the relay
    relay_update_wait(RELAY_CLOSE);
    debug_printf(BOOT, "Now in day-burner mode.\r\n");

    // keep hicking dog forever
    do{
        hw_kick_watchdog();
        sys_delay_ms(1000);
    }while(1);

    return;
}

/*******************************************************************************
 * @brief Recreates a blank bootlog file
 * @param[in] void
 * @return void
 *******************************************************************************/
static void regenerate_bootlog(void){
    static uint8_t boot_attempts;

    // make sure file is closed
    f_close( &boot_log_file);

    // force delete current file - ignore returned error
    f_unlink((const char *)BOOT_LOG_FILE_NAME);

    // create new log file
    catch_file_err_report_void(OPEN_LOG,
                            f_open( &boot_log_file,
                                    BOOT_LOG_FILE_NAME,
                                    FA_OPEN_ALWAYS | FA_READ | FA_WRITE ));
    boot_attempts = 0;

    catch_file_err_report_void(WRITE_LOG,
                            f_write( &boot_log_file,
                                    (uint8_t *)&boot_attempts,
                                    1,
                                    &f_wr_bytes));

    catch_file_err_report_void(CLOSE_LOG, f_close(&boot_log_file) );

    return;
}

/*******************************************************************************
 * @brief Checks the integrity of the boot log file
 * @param[in] void
 * @return bool with true for valid or false if corrupt in any way
 *******************************************************************************/
static bool bootlog_is_valid(void){
    uint8_t boot_attempts;

    // open log file - should already exist
    catch_file_error_report(OPEN_LOG,
                            f_open( &boot_log_file,
                                    BOOT_LOG_FILE_NAME,
                                    FA_READ | FA_WRITE ));
    catch_file_error_report(READ_LOG,
                            f_read( &boot_log_file,
                                    (uint8_t *)&boot_attempts,
                                    1,
                                    &f_rd_bytes));

    catch_file_error_report(CLOSE_LOG, f_close(&boot_log_file) );

    // finally, check if the num of retries recorded is greater than expected.
    catch_test_report(LOG_CORRUPT,
                    (boot_attempts > (uint8_t) (BOOT_ATTEMPTS_LIMIT + 1)));

    return true;
}

/*******************************************************************************
 * @brief TODO:
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool update_code(FIL * p_file, const char * file_name){
    uint32_t image_length_bytes = 0;
    uint32_t sectors_required = 0;
    uint32_t next_code_sector = 0;
    uint32_t last_code_sector = 0;
    uint32_t source_address = 0;

    debug_printf( BOOT, "Updating code space with %s\r\n", file_name);

    /*
     * open new fw image file
     */
    catch_file_error_report(    OPEN_IM_FILE,
                                f_open( p_file,
                                        file_name,
                                        FA_READ ));

    /*
     * Work out code length and number of sectors
     */
    image_length_bytes = f_size(p_file);
    sectors_required =  ( (image_length_bytes % CODE_SECTOR_SIZE) == 0 ) ?
                        ( image_length_bytes / CODE_SECTOR_SIZE ) :
                        ( 1 + image_length_bytes / CODE_SECTOR_SIZE );
    next_code_sector = IMAGE_ADDRESS / CODE_SECTOR_SIZE;
    last_code_sector = next_code_sector + sectors_required - 1;

    debug_printf(BOOT, "image_length_bytes = %lu\r\n", image_length_bytes);
    debug_printf(BOOT, "sectors_required = %lu\r\n", sectors_required);
    debug_printf(BOOT, "next_code_sector = %lu\r\n", next_code_sector);
    debug_printf(BOOT, "last_code_sector = %lu\r\n", last_code_sector);

    catch_test_report(  IM_LENGTH_NULL,
                        image_length_bytes == 0 );

    /*
     * Copy code one sector at a time
     */
    while( next_code_sector <= last_code_sector )
    {
        if( copy_code(p_file, source_address, next_code_sector) == false ){
            return false;
        }else{}
        source_address += CODE_SECTOR_SIZE;
        next_code_sector++;
        hw_kick_watchdog();
    }

    /*
     * close file
     */
    catch_file_error_report(CLOSE_IM_FILE,
                            f_close( p_file) );

    return true;
}

/*******************************************************************************
 * @brief TODO:
 * @param[in] ...
 * @return bool true if all went well or false otherwise.
 *******************************************************************************/
static bool copy_code( FIL* p_file, uint32_t source_address, uint32_t target_code_sector){

    static uint32_t copy_buf[CODE_SECTOR_SIZE/4] = {0};
    char * p_copy_buf_bytes = (char *)copy_buf;
    uint32_t next_cpy_len = 0;
    uint32_t dest_code_add = (uint32_t)(target_code_sector << 12);

     // work out how many bytes are left to copy from source
    uint32_t bytes_left_in_file = f_size(p_file) - source_address;

    debug_printf(BOOT, "bytes_left_in_file = %lu\r\n", bytes_left_in_file);

    next_cpy_len =  ( bytes_left_in_file < CODE_SECTOR_SIZE) ?
                    bytes_left_in_file :
                    CODE_SECTOR_SIZE;

     // adjust read ptr offset and read out data
    debug_printf(   BOOT,
                    "Copy %lu bytes from address %lu\r\n",
                    next_cpy_len, source_address);

    catch_file_error_report(    SEEK_IM_FILE,
                                f_lseek(p_file, source_address));

    debug_printf( BOOT, "moved ptr to %lu\r\n", source_address);

    catch_file_error_report(    READ_IM_FILE,
                                f_read( p_file,
                                        p_copy_buf_bytes,
                                        next_cpy_len,
                                        &f_rd_bytes));

     // paste data into code space
    debug_printf(BOOT, "Paste to sector %lu\r\n", target_code_sector);

    __disable_irq();
    // Prepare sector
    catch_test_report(  PREP_SECTOR_FOR_ERASE,
                        Chip_IAP_PreSectorForReadWrite( target_code_sector,
                                                        target_code_sector)
                        != IAP_CMD_SUCCESS);
    // Erase sector
    catch_test_report(  ERASE_SECTOR,
                        Chip_IAP_EraseSector(   target_code_sector,
                                                target_code_sector)
                        != IAP_CMD_SUCCESS);
    // Prepare sector
    catch_test_report(  PREP_SECTOR_FOR_WRITE,
                        Chip_IAP_PreSectorForReadWrite( target_code_sector,
                                                        target_code_sector)
                        != IAP_CMD_SUCCESS);
    // copy from ram to code space
    catch_test_report(  RAM_TO_FLASH,
                        Chip_IAP_CopyRamToFlash(    (uint32_t)dest_code_add,
                                                    (uint32_t *)copy_buf,
                                                    CODE_SECTOR_SIZE)
                        != IAP_CMD_SUCCESS);
    // All done
    __enable_irq();

     // All operations were successful
    return true;
}

/*******************************************************************************
 * @brief Boots up image at given address
 * @param[in] void
 * @return void
 *******************************************************************************/
static void boot_image(uint32_t image_address){
    void (*user_code_entry)(void);
    uint32_t *p;

    debug_printf(BOOT, "Booting Image...\r\n");

    // Start by disabling interrupts, before changing interrupt vectors
    __disable_irq();
    //switch to IRC
    Chip_Clock_SetMainClockSource(SYSCTL_MAINCLKSRC_IRC);

    // Set vector table offset
    SCB->VTOR = image_address;

    p = (uint32_t *)image_address;

    // Set stack pointer to given address
    __set_MSP(*p);

    // Set address for RESET
    p++;
    user_code_entry = (void (*)(void)) (*p);

    // Jump to application
    user_code_entry();

    return;
}


/*******************************************************************************
 * @brief Report error to sysmon task
 * @param[in] caller id and error code
 * @return void
 *******************************************************************************/
static void report_error(uint32_t error_code){
    debug_printf( BOOT, "ERR %lu\r\n", error_code);
    return;
}
