/*
==========================================================================
 Name        : MCP39F511A.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/mcp39f511a.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 30 March 2021
 Description : Microchip power monitor defines etc
==========================================================================
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef PUBLIC_PERIPHERAL_POWER_MCP39F511A_H
#define PUBLIC_PERIPHERAL_POWER_MCP39F511A_H

/*---------------------- Private Enums/Constants/Types -----------------------*/

/*---------------------- Public Enums/Constants/Types ------------------------*/

/***************************** Master commands ********************************/

#define MCP_CMD_HEADER                   0xA5
#define MCP_CMD_REG_READ                 0x4E
#define MCP_CMD_REG_WRITE                0x4D
#define MCP_CMD_SET_ADDR_POINTER         0x41
#define MCP_CMD_SAVE_REGS_TO_FLASH       0x53
#define MCP_CMD_EEPROM_PAGE_READ         0x42
#define MCP_CMD_EEPROM_PAGE_WRITE        0x50
#define MCP_CMD_EEPROM_BULK_ERASE        0x4F
#define MCP_CMD_AUTO_CAL_GAIN            0x5A
#define MCP_CMD_AUTO_CAL_REACTIVE_GAIN   0x7A
#define MCP_CMD_AUTO_CAL_FREQUENCY       0x76
#define MCP_CMD_SAVE_COUNTERS_TO_EEPROM  0x45

/*************************** MCP39F511A replies *******************************/

#define MCP_ACK        0x06
#define MCP_NAK        0x15
#define MCP_CS_FAIL    0x51  // checksum fail

// max payload is 32 bytes, + ACK + size of message + checksum
#define MCP_MAX_RXD_MESSAGE_SIZE       35

/*************************** Register Addresses *******************************/

#define MCP_REG_INSTRUCTION_PTR        0x0000 // Instruction Pointer r/o u16 
#define MCP_REG_SYSTEM_STATUS          0x0002 // System Status R/O b16 System Status reg
#define MCP_REG_SYSTEM_VERSION         0x0004 // System Version R/O u16 System version date 
                                              // code information for MCP39F511A, format YYWW
#define MCP_REG_VOLTAGE_RMS            0x0006 // Voltage RMS output R/O u16
#define MCP_REG_LINE_FREQUENCY         0x0008 // Line Frequency Output R/O u16
#define MCP_REG_THERMISTOR_VOLTAGE     0x000A // Thermistor Voltage for temperature compensation R/O u16
                                              // Output of the 10-bit SAR ADC.
#define MCP_REG_POWER_FACTOR           0x000C // Power Factor Output R/O s16
#define MCP_REG_RMS_CURRENT            0x000E // Current RMS R/O u32
#define MCP_REG_ACTIVE_POWER           0x0012 // Active Power output R/O u32
#define MCP_REG_REACTIVE_POWER         0x0016 // Reactive Power output R/O u32
#define MCP_REG_APPARENT_POWER         0x001A // Apparent Power output R/O u32
#define MCP_REG_IMPORT_ACTIVE_ENERGY_COUNTER 0x001E // Import Active Energy Counter R/O u64
                                                    // Accumulator for active energy, import
#define MCP_REG_EXPORT_ACTIVE_ENERGY_COUNTER 0x0026 // Export Active Energy Counter R/O u64
                                                    // Accumulator for active energy, export
#define MCP_REG_IMPORT_REACTIVE_ENERGY_COUNTER 0x002E // Accumulator for reactive energy, import R/O u64 
#define MCP_REG_EXPORT_REACTIVE_ENERGY_COUNTER 0x0036 // Accumulator for reactive energy, export R/O u64 
#define MCP_REG_MINIMUM_RECORD_1               0x003E // Minimum value of the output quantity address
                                                      // in Min/Max Pointer 1 registerR/0 u32 

#define MCP_REG_MINIMUM_RECORD_2       0x0042 // Minimum Value of the output quantity address 
                                              // in Min/Max Pointer 2 registerR/O u32 

#define MCP_REG_MAXIMUM_RECORD_1       0x0046 // Maximum Value of the output quantity address
                                              // in Min/Max Pointer 1 register R/O u32
#define MCP_REG_MAXIMUM_RECORD_2       0x004A // Maximum Value of the output quantity address
                                              // in Min/Max Pointer 2 register R/O u32 

/********************* Calibration Registers (AC mode) ************************/

#define MCP_REG_CALIBRATION_DELIMITER  0x004E // Calibration Register Delimiter May be used to initiate loading  
                                              // of the default calibration coefficients at start-up R/W u16
#define MCP_REG_GAIN_CURRENT_RMS       0x0050 // Gain calibration factor for RMS current R/W u16 
#define MCP_REG_GAIN_VOLTAGE_RMS       0x0052 // Gain calibration factor for RMS voltage R/W u16 
#define MCP_REG_GAIN_ACTIVE_POWER      0x0054 // Gain calibration factor for active power R/W u16 
#define MCP_REG_GAIN_REACTIVE_POWER    0x0056 // Gain calibration factor for reactive power R/W u16 
#define MCP_REG_OFFSET_CURRENT_RMS     0x005A // Offset calibration factor for RMS current R/W s16 
#define MCP_REG_OFFSET_ACTIVE_POWER    0x005C // Offset calibration factor for active power R/W s16 
#define MCP_REG_OFFSET_REACTIVE_POWER  0x005E // Offset calibration factor for reactive power R/W s16 
#define MCP_REG_GAIN_LINE_FREQUENCY    0x0060 // Gain calibration factor for line frequency R/W u16 
#define MCP_REG_PHASE_COMPENSATION     0x0062 // Phase Compensation R/W s16 

/******************** EMI Filter Compensation Registers ***********************/

#define MCP_REG_VOLT_DROP_COMP         0x0064 // Voltage drop compensation (DC and AC mode) R/W u16 
#define MCP_REG_INCAP_CURRENT_COMP     0x0066 // Input capacitor current compensation (AC mode) R/W u16 
#define MCP_REG_RANGE_VDROP_INCAP_COMP 0x0068 // Scaling factors for the voltage drop and input 
                                              // capacitor current compensation R/W u16 

/********************* Calibration Registers (DC mode) ************************/

#define MCP_REG_DC_GAIN_CURRENT_RMS    0x006C // Gain calibration factor for RMS current R/W u16
#define MCP_REG_DC_GAIN_VOLTAGE_RMS    0x006E // Gain calibration factor for RMS voltage R/W u16 
#define MCP_REG_DC_GAIN_ACTIVE_POWER   0x0070 // Gain calibration factor for active power R/W u16
#define MCP_REG_DC_OFFSET_CURRENT_RMS  0x0072 // Offset calibration factor for RMS current R/W s16
#define MCP_REG_DC_OFFSET_ACTIVE_POWER 0x0074 // Offset calibration factor for active power R/W s16 

/************************** ADC Offset Registers ******************************/

#define MCP_REG_OFFCAL_MSB             0x007A // MSbs of the 24-bit offset values for CH0 (current channel)
                                              // and CH1 (voltage channel) R/W b16 
#define MCP_REG_OFFCAL_CH0             0x007C // Lower 16-bit of the 24-bit offset for CH0 (current channel) R/W b16 
#define MCP_REG_OFFCAL_CH1             0x007E // Lower 16-bit of the 24-bit offset for CH1 (voltage channel) R/W b16 

/******************* Temperature Compensation Registers ***********************/

#define MCP_REG_TEMP_POS_COMP_FREQ     0x0080 // Temperature compensation for frequency for T > TCAL R/W u16 
#define MCP_REG_TEMP_NEG_COMP_FREQ     0x0082 // Temperature compensation for frequency for T < TCAL R/W u16
#define MCP_REG_TEMP_POS_COMP_CURRENT  0x0084 // Temperature compensation for current for T > TCAL R/W u16
#define MCP_REG_TEMP_NEG_COMP_CURRENT  0x0086 // Temperature compensation for current for T < TCAL R/W u16 
#define MCP_REG_TEMP_POS_COMP_POWER    0x0088 // Temperature compensation for power for T > TCAL R/W u16 
#define MCP_REG_TEMP_NEG_COMP_POWER    0x008A // Temperature compensation for power for T < TCAL R/W u16 

/********************** Design Configuration Registers ************************/

#define MCP_REG_SYSTEM_CONFIGURATION   0x0094 // Control for device configuration, including ADC configuration R/W b32
#define MCP_REG_EVENT_CONFIGURATION    0x0098 // Settings for the event pins including relay control R/W b32
#define MCP_REG_SCALING_FACTOR         0x009C // Scaling factor for outputs R/W b32 
#define MCP_REG_CALIBRATION_CURRENT    0x00A0 // Target current to be used during single-point calibration R/W u32
#define MCP_REG_CALIBARTION_VOLTAGE    0x00A4 // Target voltage to be used during single-point calibration R/W u16

#define MCP_REG_CALIBRATION_POWER_ACTIVE   0x00A6 // Target active power to be used during single-point calibration R/W u32
#define MCP_REG_CALIBRATION_POWER_REACTIVE 0x00AA // Target active power to be used during single-point calibration R/W u32

#define MCP_REG_APP_POWER_DIVISOR_DIGITS 0x00BE // Sets the RMS (IRMS and VRMS) indications precision and  
                                                // the desired precision for apparent power. R/W u16

#define MCP_REG_ACC_INTERVAL_PARAM       0x00C0 // Accumulation Interval Parameter -> N for 2N number of line 
                                                // cycles to be used during a single computation cycle R/W u16
#define MCP_REG_PWM_PERIOD               0x00C2 // Input register controlling PWM period R/W u16
#define MCP_REG_PWM_DUTY_CYCLE           0x00C4 // Input register controlling PWM duty cycle R/W u16
#define MCP_REG_MIN_MAX_POINTER_1        0x00C6 // Address pointer for Min/Max 1 outputs R/W u16
#define MCP_REG_MIN_MAX_POINTER_2        0x00C8 // Address pointer for Min/Max 2 outputs R/W u16
#define MCP_REG_LINE_FREQ_REFERENCE      0x00CA // Reference value for the nominal line frequency R/W u16
#define MCP_REG_THERMISTOR_VOLTAGE_CAL   0x00CC // Thermistor calibration value for temperature  
                                                // compensation of the calculation engine R/W u16
#define MCP_REG_VOLTAGE_SAG_LIMIT        0x00CE // RMS voltage threshold at which an event flag is recorded R/W u16 
#define MCP_REG_VOLTAGE_SURGE_LIMIT      0x00D0 // RMS voltage threshold at which an event flag is recorded R/W u16 
#define MCP_REG_OVER_CURRENT_LIMIT       0x00D2 // RMS current threshold at which an event flag is recorded R/W u32 
#define MCP_REG_OVER_POWER_LIMIT         0x00D6 // Active power limit at which an event flag is recorded R/W u32 
#define MCP_REG_OVER_TEMPERATURE_LIMIT   0x00DA // Limit at which an overtemperature event flag is recorded R/W u16
#define MCP_REG_VOLTAGE_LOW_THRESHOLD    0x00DC // Input voltage save to EE Low threshold R/W u16 
#define MCP_REG_VOLTAGE_HIGH_THRESHOLD   0x00DE // Input voltage Save to EE High threshold R/W u16 
#define MCP_REG_NO_LOAD_THRESHOLD        0x00E0 // No load threshold for energy counting R/W u16

/*---------------------------- Public Variables ------------------------------*/


/*---------------------- Public Function Declarations ------------------------*/


/*------------------------------- End of File --------------------------------*/
#endif // PUBLIC_PERIPHERAL_POWER_MCP39F511A_H
