/*
==========================================================================
 Name        : microchipMetering_dataProcess.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/microchip/microchipMetering_dataProcess.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 1st April 2021
 Description :
==========================================================================
*/
/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PUBLIC_PERIPHERAL_POWER_MICROCHIP_MICROCHIP_MICROCHIPMETERING_DATAPROCESS_H_
#define PUBLIC_PERIPHERAL_POWER_MICROCHIP_MICROCHIP_MICROCHIPMETERING_DATAPROCESS_H_

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/


/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
#define MCP_CHECKSUM_SIZE  sizeof(uint8_t)

// Received message indices
#define MCP_RXD_ACK_IDX          0
#define MCP_RXD_NO_OF_BYTES_IDX  1
#define MCP_RXD_DATA_START_IDX   2

/*****************************************************************************
 * Public Variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
 powerMonitor_Result mcp_process_instant_dataset( uint8_t * message_ptr, PowerCalcDataset * pData);
 powerMonitor_Result mcp_process_energy_dataset( uint8_t * message_ptr, PowerCalcDataset * pData);
 bool mcp_validate_checksum( uint8_t * message_ptr, uint8_t message_length);

/*------------------------------- End of File --------------------------------*/
#endif /* PUBLIC_PERIPHERAL_POWER_MICROCHIP_MICROCHIP_MICROCHIPMETERING_DATAPROCESS_H_ */
