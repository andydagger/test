/*
==========================================================================
 Name        : microchipMetering_impl.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/microchip/microchipMetering_impl.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 1st April 2021
 Description :
==========================================================================
*/
/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PUBLIC_PERIPHERAL_POWER_MICROCHIP_MICROCHIPMETERING_IMPL_H_
#define PUBLIC_PERIPHERAL_POWER_MICROCHIP_MICROCHIPMETERING_IMPL_H_

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
#define HW_DETECT_REQ_SIZE      8
// reply : ack, no.of bytes, uint16_t value, checksum
#define HW_DETECT_REPLY_SIZE    5

#define INSTANT_DATA_REQ_SIZE   8
// reply : ack, no.of bytes, 16 byte values, checksum
#define INSTANT_DATA_REPLY_SIZE 19

#define POWER_DATA_REQ_SIZE     8

#ifdef MCP_EXPORT_REG_INCLUDED

// reply : ack, no.of bytes, 16 byte values, checksum
#define POWER_DATA_REPLY_SIZE  19

#else

// reply : ack, no.of bytes, 8 byte values, checksum
#define POWER_DATA_REPLY_SIZE  11

#endif


/*****************************************************************************
 * Public Variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
 void mcp_init_comms( void);
 void mcp_init_measurement( PowerCalcDataset * ptr_dataset);
 powerMonitor_Result mcp_hardware_detect( void);
 void mcp_uart_isr( void);

 powerMonitor_Result mcp_fetch_instant_dataset( void);
 powerMonitor_Result mcp_fetch_power_dataset( void);
 powerMonitor_Result mcp_send_request( const uint8_t* message_ptr,
                                             uint8_t tx_length,
		                                     uint8_t rx_length);
/*------------------------------- End of File --------------------------------*/
#endif /* PUBLIC_PERIPHERAL_POWER_MICROCHIP_MICROCHIPMETERING_IMPL_H_ */
