/*
==========================================================================
 Name        : microchipMetering_calibration.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/microchip_microchipMetering_calibration.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 1st April 2021
 Description :
==========================================================================
*/
/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PUBLIC_PERIPHERAL_POWER_MICROCHIP_MICROCHIPMETERING_CALIBRATION_H_
#define PUBLIC_PERIPHERAL_POWER_MICROCHIP_MICROCHIPMETERING_CALIBRATION_H_

#include "powerMonitor.h"
#include "stdint.h"
#include "stdbool.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/


/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public Variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
 powerMonitor_Result microchipMetering_calibrate( PowerCalcDataset * dataset_ptr);
 powerMonitor_Result microchipMetering_calibrateReadings( PowerCalcDataset * dataset_ptr);

/*------------------------------- End of File --------------------------------*/
#endif /* PUBLIC_PERIPHERAL_POWER_MICROCHIP_MICROCHIPMETERING_CALIBRATION_H_ */
