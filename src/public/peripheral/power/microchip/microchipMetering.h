/*
==========================================================================
 Name        : microchipMetering.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/microchip/microchipMetering.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 1st April 2021
 Description :
==========================================================================
*/
/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PUBLIC_PERIPHERAL_POWER_MICROCHIP_MICROCHIPMETERING_H_
#define PUBLIC_PERIPHERAL_POWER_MICROCHIP_MICROCHIPMETERING_H_

#include "powerMonitor.h"
#include "stdint.h"
#include "stdbool.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
typedef enum __tag_mcp_state
{
	MCP_STATE_IDLE = 0,
	MCP_STATE_INSTANT_DATA_WAIT,
	MCP_STATE_POWER_DATA_WAIT,
	MCP_STATE_MAX
} mcp_state;


#define MCP_UART_BAUD_RATE          9600
#define MCP_UART_COMM_PARAMS        (UART_CFG_DATALEN_8 | UART_CFG_PARITY_NONE | UART_CFG_STOPLEN_1)

// Wait times are in 1s increments due to get_systick_sec()
#define MCP_INSTANT_DATA_WAIT_TIME  30
#define MCP_POWER_DATA_WAIT_TIME    30

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/


/*****************************************************************************
 * Public Variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void microchipMetering_init( void);
void microchipMetering_initMeasurement( PowerCalcDataset * ptr_dataset);
powerMonitor_Result microchipMetering_detectHW( void);
powerMonitor_Result microchipMetering_service( void);
void microchipMetering_uartISR( void);
void microchipMetering_interruptPinISR( void);
void microchipMetering_processEvent( void);

#endif /* PUBLIC_PERIPHERAL_POWER_MICROCHIP_MICROCHIPMETERING_H_ */
/*------------------------------- End of File --------------------------------*/
