/*
==========================================================================
 Name        : microchipMetering_impl.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/microchip/microchipMetering_impl.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 1st April 2021
 Description : Microchip power metering implementation layer
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "led.h"
#include "config_pinout.h"
#include "config_dataset.h"
#include "FreeRTOS.h"
#include "FreeRTOSCommonHooks.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "powerMonitor.h"
#include "mcp39f511a.h"
#include "microchipMetering.h"
#include "microchipMetering_dataProcess.h"
#include "microchipMetering_impl.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
// request is 8 bytes @ 9600 = 8.3ms
// reply is 19 bytes @ 9600  = 19.8ms
// Timeout value in ms
#define MCP_INSTANT_REPLY_TIMEOUT  32

// request is 8 bytes @ 9600 = 8.3ms
// reply is 19 bytes @ 9600  = 19.8ms
// Timeout value in ms
#define MCP_POWER_REPLY_TIMEOUT    32

// request is 8 bytes @ 9600 = 8.3ms
// reply is 5 bytes @ 9600  = 5.3ms
// Timeout value in ms
#define MCP_HW_DETECT_REPLY_TIMEOUT 20

// FreeRTOS delay value while waiting for a reply message
#define MCP_WAIT_DELAY_MS        5


static volatile uint8_t    comms_rx_buffer[ MCP_MAX_RXD_MESSAGE_SIZE];
static volatile uint8_t    comms_rx_index;
static volatile uint8_t    comms_rx_length;
static volatile uint8_t    comms_tx_length;
static volatile const uint8_t * comms_tx_ptr;
static volatile bool       comms_rx_enabled;

static PowerCalcDataset * dataset_ptr;

/*****************************************************************************
 * Private Constants
 ****************************************************************************/


// microchip request for HW detection using the only register
// that is a constant : System Version
static const uint8_t hw_detect_request[ HW_DETECT_REQ_SIZE] =
{
    0xA5,  // Header
	0x08,  // No. of bytes in message
	0x41,  // Command -> Set Address
	0x00,  // Address MSB
	0x04,  // Address LSB  -> MCP_REG_SYSTEM_VERSION
	0x4E,  // Command -> Read Bytes
	0x02,  // No. Of bytes to read (uint16_t)
	0x42   // Message checksum
};


// microchip request for reading instant dataset
static const uint8_t instant_data_request[ INSTANT_DATA_REQ_SIZE] =
{
    0xA5,  // Header
	0x08,  // No. of bytes in message
	0x41,  // Command -> Set Address
	0x00,  // Address MSB
	0x06,  // Address LSB -> MCP_REG_VOLTAGE_RMS
	0x4E,  // Command -> Read Bytes
	0x10,  // No. Of bytes to read
	0x52   // Message checksum
};

#define POWER_DATA_REQ_SIZE    8

#ifdef MCP_EXPORT_REG_INCLUDED

// microchip request for reading power accumulator dataset export and import regs
static const uint8_t power_data_request[ POWER_DATA_REQ_SIZE] =
{
    0xA5,  // Header
	0x08,  // No. of bytes in message
	0x41,  // Command -> Set Address
	0x00,  // Address MSB
	0x1E,  // Address LSB -> MCP_REG_IMPORT_ACTIVE_ENERGY_COUNTER
	0x4E,  // Command -> Read Bytes
	0x10,  // No. Of bytes to read
	0x6A   // Message checksum
};

#else

// microchip request for reading power accumulator dataset import reg only
static const uint8_t power_data_request[ POWER_DATA_REQ_SIZE] =
{
    0xA5,  // Header
	0x08,  // No. of bytes in message
	0x41,  // Command -> Set Address
	0x00,  // Address MSB
	0x1E,  // Address LSB -> MCP_REG_IMPORT_ACTIVE_ENERGY_COUNTER
	0x4E,  // Command -> Read Bytes
	0x08,  // No. Of bytes to read
	0x62   // Message checksum
};
#endif // #ifdef MCP_EXPORT_REG_INCLUDED

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 *****************************************************************************/

/*******************************************************************************
 * @brief microchip metering initialisation
 *
 * @param[in] PowerCalcDataset *  - pointer to instant dataset
 *
 * @return    void
 *
 ******************************************************************************/
void mcp_init_comms( void)
{
	comms_rx_index   = 0;
	comms_tx_length  = 0;
	comms_rx_length  = 0;
	comms_tx_ptr     = NULL;
	comms_rx_enabled = false;

    // Setup the UART for the MCP device 9600, 8bits ,no parity, 1 stop
    hw_setup_power_monitor_uart_params( MCP_UART_BAUD_RATE, MCP_UART_COMM_PARAMS);

	hw_power_monitor_uart_irq_enable();
}

/*******************************************************************************
 * @brief microchip metering measurement initialisation
 *
 * @param[in] PowerCalcDataset *  - pointer to instant dataset
 *
 * @return    void
 *
 ******************************************************************************/
void mcp_init_measurement( PowerCalcDataset * ptr_dataset)
{
	dataset_ptr = ptr_dataset;
}


/*******************************************************************************
 * @brief microchip hardware detection
 *        This is called from the power monitor abstraction layer to detect
 *        if the microchip part is present.
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result -result of the HW detection
 *
 *******************************************************************************/
powerMonitor_Result mcp_hardware_detect( void)
{
	powerMonitor_Result result;
	uint32_t prev_tick_value;

	result = mcp_send_request( hw_detect_request,
    		                   HW_DETECT_REQ_SIZE,
					           HW_DETECT_REPLY_SIZE);

    if( result == POWER_MONITOR_RESULT_OK)
    {
		// wait until message sent and rx message received
		// request is 8 bytes @ 9600 = 8.3ms
		// reply is 5 bytes @ 9600  = 5.3ms
		prev_tick_value = get_systick_ms();

		// now wait for the reply message
		do
		{
			// Let lower priority tasks run
			FreeRTOSDelay( MCP_WAIT_DELAY_MS);

			// have we timed out waiting for the reply??
			if(( get_systick_ms() - prev_tick_value) >= MCP_HW_DETECT_REPLY_TIMEOUT)
			{
	            debug_printf( PWR, "MCP HW detect timeout\r\n");

				result = POWER_MONITOR_RESULT_TIMEOUT;
				// cancel RXD
				comms_rx_enabled = false;
				break;
			}

		}
		while( comms_rx_enabled != false);

		// verify message contents if replied
		if( result == POWER_MONITOR_RESULT_OK)
		{
			// Verify message is correct
			if(( comms_rx_buffer[ MCP_RXD_ACK_IDX]         != MCP_ACK) ||
			   ( comms_rx_buffer[ MCP_RXD_NO_OF_BYTES_IDX] != HW_DETECT_REPLY_SIZE-3) ||
			   ( mcp_validate_checksum( (uint8_t*)comms_rx_buffer, HW_DETECT_REPLY_SIZE) == false))
			{
                debug_printf( PWR, "MCP HW detect data error\r\n");

				result = POWER_MONITOR_RESULT_DATA_ERROR;
			}
		}
    }

	return result;
}


/*******************************************************************************
 * @brief microchip uart isr - mapped to UART1.
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void mcp_uart_isr( void)
{
    uint32_t intsrc = 0;
    uint8_t  readByte;

    intsrc = hw_power_monitor_uart_get_int_status();

    // received data ??
    if( intsrc & UART_STAT_RXRDY)
    {
		// must read the byte irrespective of state to flush buffer
    	readByte = hw_power_monitor_uart_rx_byte();

		// only accept chars if waiting for a reply
		if( comms_rx_enabled)
		{
			// check for overflow of the buffer
			if(( comms_rx_index < MCP_MAX_RXD_MESSAGE_SIZE) &&
			   ( comms_rx_index < comms_rx_length))
			{
				comms_rx_buffer[ comms_rx_index++] = readByte;

				if( comms_rx_index >= comms_rx_length)
				{
					// disable RXD
					comms_rx_enabled = false;
				}
			}
		}
    }

    // TX empty ready to send next char ?
    if( intsrc & UART_STAT_TXRDY)
    {
    	if( comms_tx_length > 0)
    	{
    		hw_power_monitor_uart_tx_byte( *comms_tx_ptr++);

    		comms_tx_length--;
    	}
    	else
    	{
    		// disable TX RDY interrupt when all chars sent
    		hw_power_monitor_uart_tx_disable();
    	}
    }
}

/*******************************************************************************
 * @brief fetches the instant data from the microchip power monitor
 *        this includes the voltage, current, power and power factor registers
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - result of the dataset fetch
 *
 *******************************************************************************/
powerMonitor_Result mcp_fetch_instant_dataset( void)
{
    uint32_t prev_tick_value;
    powerMonitor_Result result;

    result = mcp_send_request( instant_data_request,
    	    	               INSTANT_DATA_REQ_SIZE,
			    		       INSTANT_DATA_REPLY_SIZE);

    if( result == POWER_MONITOR_RESULT_OK)
    {
		// wait until message sent and rx message received
		// request is 8 bytes @ 9600 = 8.3ms
		// reply is 19 bytes @ 9600  = 19.8ms
		prev_tick_value = get_systick_ms();

		// now wait for the reply message
		do
		{
			// Let lower priority tasks run
			FreeRTOSDelay( MCP_WAIT_DELAY_MS);
			// have we timed out waiting for the reply??
			if((get_systick_ms() - prev_tick_value) >= MCP_INSTANT_REPLY_TIMEOUT)
			{
                debug_printf( PWR, "MCP instant data fetch timeout\r\n");
				// report the timeout error
				result = POWER_MONITOR_RESULT_TIMEOUT;
				comms_rx_enabled = false;
				break;
			}

		}
		while( comms_rx_enabled != false);

		if( result == POWER_MONITOR_RESULT_OK)
		{
			result = mcp_process_instant_dataset( (uint8_t *)comms_rx_buffer, dataset_ptr);
		}
    }

	return result;
}

/*******************************************************************************
 * @brief fetches the accumulator data from the microchip power monitor
 *        this includes Active energy accumulators for import and export
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - the result of the data fetch
 *
 *******************************************************************************/
powerMonitor_Result mcp_fetch_power_dataset( void)
{
    uint32_t prev_tick_value;
    powerMonitor_Result result;

    result = mcp_send_request( power_data_request,
	  					       POWER_DATA_REQ_SIZE,
						       POWER_DATA_REPLY_SIZE);

    if( result == POWER_MONITOR_RESULT_OK)
    {
		// wait until message sent and rx message received
		// request is 8 bytes @ 9600 = 8.3ms
		// reply is 19 bytes @ 9600  = 19.8ms
		prev_tick_value = get_systick_ms();

		// now wait for the reply message
		do
		{
			// Let lower priority tasks run
			FreeRTOSDelay( MCP_WAIT_DELAY_MS);

			// have we timed out waiting for the reply??
			if((get_systick_ms() - prev_tick_value) >= MCP_POWER_REPLY_TIMEOUT)
			{
                debug_printf( PWR, "MCP power data fetch timeout\r\n");
				// report the timeout error
				result = POWER_MONITOR_RESULT_TIMEOUT;
				comms_rx_enabled = false;
				break;
			}

		}
		while( comms_rx_enabled != false);

		if( result == POWER_MONITOR_RESULT_OK)
		{
			result = mcp_process_energy_dataset( (uint8_t *)comms_rx_buffer, dataset_ptr);
		}
    }

	return result;
}

/*******************************************************************************
 * @brief fetches the accumulator data from the microchip power monitor
 *        this includes Active energy accumulators for import and export
 *
 * @param[in] const uint8_t* - pointer to request message stored in flash
 * @param[in] uint8_t        - TX message length in bytes
 * @param[in] uint8_t        - RX message length in bytes
 *
 * @return    powerMonitor_Result - result of the send request
 *
 *******************************************************************************/
powerMonitor_Result mcp_send_request( const uint8_t* message_ptr,
                                      uint8_t tx_length,
					                  uint8_t rx_length)
{
	// make sure input params are valid
	if(( comms_tx_ptr != NULL) && ( tx_length > 0) && ( rx_length > 0))
	{
		// configure the receive
		comms_rx_index   = 0;
		comms_rx_length  = rx_length;
		comms_rx_enabled = true;

		comms_tx_length  = tx_length;
		comms_tx_ptr     = message_ptr;

		// the 1st char will be sent in the UART ISR
		hw_power_monitor_uart_tx_enable();

		return POWER_MONITOR_RESULT_OK;
	}

	return POWER_MONITOR_RESULT_PARAM_ERROR;
}


/*------------------------------ End of File -------------------------------*/

