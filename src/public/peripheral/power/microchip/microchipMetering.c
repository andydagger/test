/*
==========================================================================
 Name        : microchipMetering.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/microchip/microchipMetering.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 1st April 2021
 Description : Microchip MCP39F511A metering solution
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "led.h"
#include "config_pinout.h"
#include "config_dataset.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "powerMonitor.h"
#include "mcp39f511a.h"
#include "microchipMetering.h"
#include "microchipMetering_dataProcess.h"
#include "microchipMetering_impl.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

static volatile mcp_state  state;
static uint32_t            time_of_last_sample;
static bool                microchip_event;

/*****************************************************************************
 * Private Constants
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 *****************************************************************************/

/*******************************************************************************
 * @brief microchip metering initialisation
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    void
 *
 ******************************************************************************/
void microchipMetering_init( void)
{
	// setup the state machine and timer for the 1st data read
    state = MCP_STATE_INSTANT_DATA_WAIT;

    microchip_event = false;

    time_of_last_sample = get_systick_sec();

    // get the comms ready
    mcp_init_comms();
}

/*******************************************************************************
 * @brief microchip metering measurement initialisation
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] PowerCalcDataset *  - pointer to dataset
 *
 * @return    void
 *
 ******************************************************************************/
void microchipMetering_initMeasurement( PowerCalcDataset * pDataset)
{
    // pass pointer to dataset fetching
    mcp_init_measurement( pDataset);
}


/*******************************************************************************
 * @brief microchip hardware detection
 *        This is called from the power monitor abstraction layer to detect
 *        if the microchip part is present.
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - result of the HW detection
 *
 *******************************************************************************/
powerMonitor_Result microchipMetering_detectHW( void)
{
    powerMonitor_Result result;

    result = mcp_hardware_detect();

	state = MCP_STATE_IDLE;

	return result;
}

/*******************************************************************************
 * @brief microchip metering service
 *        This is called from the vTaskSensorLfp every 100ms. The function
 *        contains the state machine and timings for receiving data from the
 *        microchip power monitor and any averaging of data required.
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - result of the service
 *
 *******************************************************************************/
powerMonitor_Result microchipMetering_service( void)
{
	powerMonitor_Result result = POWER_MONITOR_RESULT_IDLE;

	uint32_t seconds;

	// have we had an event interrupt from the power monitoring?
	if( microchip_event)
	{
	    microchip_event = false;

	    microchipMetering_processEvent();
	}

	seconds = get_systick_sec();

	switch( state)
	{
		case MCP_STATE_IDLE:
			break;

		case MCP_STATE_INSTANT_DATA_WAIT:
			// is it time to request the dataset??
	        if(( seconds - time_of_last_sample) >= MCP_INSTANT_DATA_WAIT_TIME)
			{
                result = mcp_fetch_instant_dataset();
                // setup the wait for the power data fetch
                time_of_last_sample = seconds;

				state = MCP_STATE_POWER_DATA_WAIT;
			}
			break;

		case MCP_STATE_POWER_DATA_WAIT:
			// is it time to request the dataset??
            if(( seconds - time_of_last_sample) >= MCP_POWER_DATA_WAIT_TIME)
			{
                result = mcp_fetch_power_dataset();

                // setup the wait for the instant data fetch
                time_of_last_sample = seconds;

                state = MCP_STATE_INSTANT_DATA_WAIT;
			}
			break;

		default:
			break;

	}

	return result;
}

/*******************************************************************************
 * @brief microchip uart isr - mapped to UART1.
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void microchipMetering_uartISR( void)
{
	// call the implementation layer isr
	mcp_uart_isr();
}

/*******************************************************************************
 * @brief microchip interrupt pin isr
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer, the microchip part is configure to interrupt
 *        the processor on voltage surge and sag, over current etc
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void microchipMetering_interruptPinISR( void)
{
    microchip_event = true;
}

/*******************************************************************************
 * @brief processes the event by reading the status regs and INT status
 *        to reset the interrupt line and report the events
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void microchipMetering_processEvent( void)
{

}


/*------------------------------ End of File -------------------------------*/

