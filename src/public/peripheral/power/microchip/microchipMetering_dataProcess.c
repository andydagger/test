/*
==========================================================================
 Name        : microchipMetering_dataProcess.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/microchip/microchipMetering_dataProcess.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 1st April 2021
 Description : Microchip metering data processing implementation
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "led.h"
#include "config_pinout.h"
#include "config_dataset.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "powerMonitor.h"
#include "mcp39f511a.h"
#include "microchipMetering.h"
#include "microchipMetering_calibration.h"
#include "microchipMetering_dataProcess.h"
#include "microchipMetering_impl.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/


// Divide values for data set values
#define MCP_ENERGY_DIVIDE_VALUE        20000
#define MCP_VOLTAGE_DIVIDE_VALUE       100
#define MCP_CURRENT_DIVIDE_VALUE       100
#define MCP_POWER_DIVIDE_VALUE         100
#define MCP_POWER_FACTOR_DIVIDE_VALUE  100

/*****************************************************************************
 * Private Constants
 ****************************************************************************/


/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 *****************************************************************************/

/*******************************************************************************
 * @brief processes the message received from the microchip power monitor
 *        and populates the instant dataset ready to report over the network
 *
 * @param[in] uint8_t * - pointer to the received Message
 *
 * @return    powerMonitor_Result - result of the dataset processing
 *
 *******************************************************************************/
powerMonitor_Result mcp_process_instant_dataset( uint8_t * message_ptr, PowerCalcDataset * pData)
{
	uint32_t index;
	uint16_t voltage;
	uint16_t powerFactor;
	uint32_t current;
	uint32_t activePower;

	if(( message_ptr == NULL) || ( pData == NULL))
	{
        debug_printf( PWR, "MCP process instant data NULL ptr\r\n");

		return POWER_MONITOR_RESULT_PARAM_ERROR;
	}

	if(( message_ptr[ MCP_RXD_ACK_IDX]         == MCP_ACK) &&
	   ( message_ptr[ MCP_RXD_NO_OF_BYTES_IDX] == INSTANT_DATA_REPLY_SIZE-3) &&
	   ( mcp_validate_checksum( message_ptr, INSTANT_DATA_REPLY_SIZE) == true))
	{
		index = MCP_RXD_DATA_START_IDX;

		voltage = (uint16_t)(message_ptr[ index] << 8) | (uint16_t)message_ptr[ index + 1];

		// jump across the thermistor voltage (uint16_t) and line frequency (uint16_t)
		// registers as they are not currently used
		index += (sizeof(uint16_t) * 3);

		powerFactor = (int16_t)(( message_ptr[ index] << 8) | message_ptr[ index + 1]);

		index += sizeof(uint16_t);

		current = ((uint32_t)message_ptr[ index]     << 24) |
				  ((uint32_t)message_ptr[ index + 1] << 16) |
				  ((uint32_t)message_ptr[ index + 2] << 8)  |
				  ((uint32_t)message_ptr[ index + 3]);

		index += sizeof(uint32_t);

		activePower = ((uint32_t)message_ptr[ index]     << 24) |
					  ((uint32_t)message_ptr[ index + 1] << 16) |
					  ((uint32_t)message_ptr[ index + 2] << 8)  |
					  ((uint32_t)message_ptr[ index + 3]);


		pData->instantVrms = (float)(voltage / MCP_VOLTAGE_DIVIDE_VALUE);

		pData->vrmsAverage += pData->instantVrms;

		if( pData->instantVrms > pData->vrmsMax)
		{
			pData->vrmsMax = pData->instantVrms;
		}

		if( pData->instantVrms < pData->vrmsMin)
		{
			pData->vrmsMin = pData->instantVrms;
		}

		pData->instantIrms = (float)(current / MCP_CURRENT_DIVIDE_VALUE);

		pData->instantPower = (float)(activePower / MCP_POWER_DIVIDE_VALUE);

		pData->instantPowerFactor = (float)(powerFactor / MCP_POWER_FACTOR_DIVIDE_VALUE);

		return POWER_MONITOR_RESULT_OK;
	}

	return POWER_MONITOR_RESULT_DATA_ERROR;
}

/*******************************************************************************
 * @brief processes the message received from the microchip power monitor
 *        and populates the power dataset ready to report over the network
 *
 * @param[in] uint8_t * - pointer to the received Message
 *
 * @return    powerMonitor_Result - result of the dataset processing
 *
 *******************************************************************************/
powerMonitor_Result mcp_process_energy_dataset( uint8_t * message_ptr, PowerCalcDataset * pData)
{
	uint32_t index;
	uint64_t import;
#ifdef MCP_EXPORT_REG_INCLUDED
	uint64_t export;
#endif

	if(( message_ptr == NULL) || ( pData == NULL))
	{
        debug_printf( PWR, "MCP process energy data NULL ptr\r\n");

		return POWER_MONITOR_RESULT_PARAM_ERROR;
	}

	if(( message_ptr[ MCP_RXD_ACK_IDX]         == MCP_ACK) &&
	   ( message_ptr[ MCP_RXD_NO_OF_BYTES_IDX] == POWER_DATA_REPLY_SIZE-3) &&
	   ( mcp_validate_checksum( message_ptr, POWER_DATA_REPLY_SIZE) == true))
	{
		index = MCP_RXD_DATA_START_IDX;

		import =  ((uint64_t)message_ptr[ index]     << 56) |
				  ((uint64_t)message_ptr[ index + 1] << 48) |
				  ((uint64_t)message_ptr[ index + 2] << 40) |
				  ((uint64_t)message_ptr[ index + 3] << 32) |
				  ((uint64_t)message_ptr[ index + 4] << 24) |
				  ((uint64_t)message_ptr[ index + 5] << 16) |
				  ((uint64_t)message_ptr[ index + 6] << 8)  |
				  ((uint64_t)message_ptr[ index + 7]);

#ifdef MCP_EXPORT_REG_INCLUDED
		index += sizeof(uint64_t);

		export =  ((uint64_t)message_ptr[ index]     << 56) |
				  ((uint64_t)message_ptr[ index + 1] << 48) |
				  ((uint64_t)message_ptr[ index + 2] << 40) |
				  ((uint64_t)message_ptr[ index + 3] << 32) |
				  ((uint64_t)message_ptr[ index + 4] << 24) |
				  ((uint64_t)message_ptr[ index + 5] << 16) |
				  ((uint64_t)message_ptr[ index + 6] << 8)  |
				  ((uint64_t)message_ptr[ index + 7]);
#endif

		pData->accumulatedEnergy_20WhrUnits = (uint32_t)(import / MCP_ENERGY_DIVIDE_VALUE);

		return POWER_MONITOR_RESULT_OK;
	}

	return POWER_MONITOR_RESULT_DATA_ERROR;
}


/*******************************************************************************
 * @brief calculate checksum of message received into comms_rx_buffer[]
 *
 * @param[in] uint8_t * - pointer to the message to checksum
 *            uint8_t   - message length
 *
 * @return    bool      - TRUE if checksum correct, otherwise FALSE
 *
 *******************************************************************************/
bool mcp_validate_checksum( uint8_t * message_ptr, uint8_t message_length)
{
	uint8_t index;
	uint8_t checksum = 0;

	if( message_ptr == NULL)
	{
        debug_printf( PWR, "MCP validate checksum NULL ptr\r\n");

	    return false;
	}

	for( index = 0; index < (message_length - MCP_CHECKSUM_SIZE); index++)
	{
		checksum += message_ptr[ index];
	}

	if( checksum == message_ptr[ index])
	{
		return true;
	}

	return false;
}

/*------------------------------ End of File -------------------------------*/

