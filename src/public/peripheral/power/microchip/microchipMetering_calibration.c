/*
==========================================================================
 Name        : microchipMetering_calibration.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/microchip/microchipMetering_calibration.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 1st April 2021
 Description : Microchip calibration implementation
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "led.h"
#include "config_pinout.h"
#include "config_dataset.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "powerMonitor.h"
#include "mcp39f511a.h"
#include "microchipMetering.h"
#include "microchipMetering_calibration.h"
#include "microchipMetering_dataProcess.h"
#include "microchipMetering_impl.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define MICROCHIP_VOLT_CALIB_CHECK_VAL    240     // 240VAC
#define MICROCHIP_CURR_CALIB_CHECK_VAL    0.417F  // I = P/V
#define MICROCHIP_POWER_CALIB_CHECK_VAL   100     // 100W

/*****************************************************************************
 * Private Constants
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 *****************************************************************************/

/*******************************************************************************
 * @brief calibration process for microchip power monitor
 *
 * @param[in] PowerCalcDataset * pointer to the calibration dataset
 *
 * @return    powerMonitor_Result - Result of the calibration
 *
 *******************************************************************************/
powerMonitor_Result microchipMetering_calibrate( PowerCalcDataset * dataset_ptr)
{
	return POWER_MONITOR_RESULT_OK;
}

/*******************************************************************************
 * @brief calibrates the readings received from the metering device
 *
 * @param[in] PowerCalcDataset *  - dataset pointer
 *
 * @return    powerMonitor_Result - Result of the calibrate readings
 *
 *******************************************************************************/
powerMonitor_Result microchipMetering_calibrateReadings( PowerCalcDataset * dataset_ptr)
{
	return POWER_MONITOR_RESULT_OK;
}

/*------------------------------ End of File -------------------------------*/

