/*
==========================================================================
 Name        : stm8Metering_calibration.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/stm8/stm8Metering_calibration.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 1st April 2021
 Description : stm8 calibration implementation
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "config_pinout.h"
#include "config_dataset.h"
#include "FreeRTOS.h"
#include "FreeRTOSCommonHooks.h"
#include "config_queues.h"
#include "sysmon.h"
#include "relay.h"
#include "powerMonitor.h"
#include "stm8Metering.h"
#include "stm8Metering_impl.h"
#include "stm8Metering_dataProcess.h"
#include "stm8Metering_calibration.h"
#include "stm8Metering_calibDataProcess.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

// timeout defines
#define STM8_STARTUP_WAIT_TIME       60  // 60 seconds
#define STM8_POLL_TIME               1000 // 1 second in ms
#define STM8_CALIBRATION_TIMEOUT     10000 // 10 seconds in ms

stm8_CalibState           stm8_state;
static stm8_samples       stm8_calibSamples;
static PowerCalcDataset * stm8_calibDataset_ptr;

/*****************************************************************************
 * Private Constants
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 *****************************************************************************/

/*******************************************************************************
 * @brief calibration process for stm8 power monitor
 *        this replaces the vTaskSensorLfp task loop for the calibration
 *        this code is blocking until success or failure.
 *        stm8_init() and stm8_initMeasurement() must called before this function
 *
 * @param[in] PowerCalcDataset * pointer to the calibration dataset
 *
 * @return    powerMonitor_Result - Result of the calibration
 *
 *******************************************************************************/
powerMonitor_Result stm8Metering_calibrate( PowerCalcDataset * dataset_ptr)
{
    uint32_t currentTime;
    uint32_t calibStartTime;
    uint32_t lastSampleTime;
    powerMonitor_Result result;

    if( dataset_ptr == NULL)
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }
    
    calibStartTime = get_systick_sec();  // used to time the whole calib process
    lastSampleTime = get_systick_sec();  // used to time between sample broadcasts

    stm8_calibDataset_ptr = dataset_ptr;

    // zero the calibration samples
    memset( (void*)&stm8_calibSamples, 0, sizeof(stm8_samples));

    result = POWER_MONITOR_RESULT_IDLE;
    // start the STM8 Zero Point Calib
    stm8_state = STM8_CALIB_STATE_ZERO_POINT_CAL;

    do
    {
        // delay 100ms for RXD message from stm8
        FreeRTOSDelay( STM8_CALIB_RTOS_DELAY);

        currentTime = get_systick_sec();

        if(( currentTime - calibStartTime) > STM8_CALIBRATION_TIMEOUT)
        {
            result = POWER_MONITOR_RESULT_TIMEOUT;
            stm8_state = STM8_CALIB_STATE_FAILED;
        }
        else

        // check if data is waiting to be processed
        if( stm8_isMessageReceived() == false)
        {
            if(( currentTime - lastSampleTime) > STM8_CALIBRATION_SAMPLE_TIMEOUT)
            {
                result = POWER_MONITOR_RESULT_TIMEOUT;
                stm8_state = STM8_CALIB_STATE_FAILED;
            }
        }
        else
        {
            lastSampleTime = currentTime;
            // process the message including status flags
            result = stm8_calibProcessRxMessage();
        }
    }
    while(( result == POWER_MONITOR_RESULT_CALIBRATION_ACTIVE) ||
          ( result == POWER_MONITOR_RESULT_IDLE));

    return result;
}


/*******************************************************************************
 * @brief processes the received message and actions the state machine
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - Result of the calibrate readings
 *
 *******************************************************************************/
powerMonitor_Result stm8_calibProcessRxMessage( void)
{
    powerMonitor_Result result;
    stm8_rawValues      rawvalues;
    stm8_instantValues  instantValues;

    // reset the received flag
    stm8_resetMessageReceived();

    // copy the values from the message buffer
    memcpy( (void*)&rawvalues.crc, (const void*)stm8_getRXBuffer(), STM8_RX_BUFF_SIZE);

    // validate the checksum before further processing
    result = stm8_validateCheckum( &rawvalues);

    if( result == POWER_MONITOR_RESULT_OK)
    {
        result = stm8_noiseShaping( &rawvalues);

        if( result == POWER_MONITOR_RESULT_OK)
        {
            result = stm8_calcInstantValues( &rawvalues, stm8_getCalibCoeffs(), &instantValues);

            if (result == POWER_MONITOR_RESULT_OK)
            {
                switch (stm8_state)
                {
                    case STM8_CALIB_STATE_ZERO_POINT_CAL:
                        // Open the relay for zero point cal
                        relay_update_wait(RELAY_OPEN);
                        // request the calibration
                        stm8_sendResponse(STM8_REQUEST_ZERO_CAL);

                        stm8_state = STM8_CALIB_STATE_WAIT_ZERO_POINT_CAL;

                        result = POWER_MONITOR_RESULT_CALIBRATION_ACTIVE;
                        break;

                    case STM8_CALIB_STATE_WAIT_ZERO_POINT_CAL:

                        result = stm8_getZeroPointCalibStatus(&rawvalues);
                        // when zero point calculated OK move to next state
                        if (result == POWER_MONITOR_RESULT_OK)
                        {
                            stm8_state = STM8_CALIB_STATE_CALIB_CALC;

                            result = POWER_MONITOR_RESULT_CALIBRATION_ACTIVE;
                            // Switch ON Relay
                            relay_update_wait(RELAY_CLOSE);

                            debug_printf(PWR, "RELAY_CLOSE\r\n");
                        }

                        stm8_sendResponse(STM8_REQUEST_NONE);
                        break;

                    case STM8_CALIB_STATE_CALIB_CALC:

                        result = stm8_processCalibSamples(&stm8_calibSamples, &instantValues);

                        if (result == POWER_MONITOR_RESULT_OK)
                        {
                            stm8_updateDataset(stm8_calibDataset_ptr, &stm8_calibSamples);

                            // Switch OFF Relay
                            relay_update_wait(RELAY_OPEN);

                            stm8_state = STM8_CALIB_STATE_FINISHED;
                        }
                        else if (result == POWER_MONITOR_RESULT_CALIBRATION_FAILED)
                        {
                            stm8_updateDataset(stm8_calibDataset_ptr, &stm8_calibSamples);

                            // Still disconnect load even if test failed
                            relay_update_wait(RELAY_OPEN);

                            stm8_state = STM8_CALIB_STATE_FAILED;
                        }

                        stm8_sendResponse(STM8_REQUEST_NONE);
                        break;

                    default:
                        stm8_sendResponse(STM8_REQUEST_NONE);
                        break;
                }
            }
        }
    }

    return result;
}

/*******************************************************************************
 * @brief processes the received message and actions the state machine
 *
 * @param[in] PowerCalcDataset * - pointer to the dataset received from the meter
 *
 * @return    powerMonitor_Result - Result of applying the readings
 *
 *******************************************************************************/
powerMonitor_Result stm8Metering_applyCalibReadings( PowerCalcDataset * meter_dataset_ptr)
{
    powerMonitor_Result result;

    result = stm8_calculateCoefficients( stm8_calibDataset_ptr, meter_dataset_ptr);

    if( result == POWER_MONITOR_RESULT_OK)
    {
        stm8_saveCalibCoeffs();
    }

    return result;
}

/*------------------------------ End of File -------------------------------*/

