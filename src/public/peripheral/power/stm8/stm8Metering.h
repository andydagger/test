/*
==========================================================================
 Name        : stm8Metering.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/stm8/stm8Metering.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 31 May 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_H_
#define PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_H_

#include "stdint.h"
#include "stdbool.h"
#include "config_dataset.h"
#include "powerMonitor.h"

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/

#define STM8_UART_BAUD_RATE           9600
#define STM8_UART_COMM_PARAMS         (UART_CFG_DATALEN_8 | UART_CFG_PARITY_NONE | UART_CFG_STOPLEN_1)

#define STM8_POWER_SAMPLE_TIMEOUT_SEC (10)

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
typedef struct __tag_stm8_CalibCoeff
{
	float voltage;
	float current;
	float power;

} stm8_CalibCoeff;

typedef struct __tag_stm8_instantValues
{
	float voltage;
	float current;
	float power;

} stm8_instantValues;


/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
 void stm8Metering_init( void);
 void stm8Metering_initMeasurement( PowerCalcDataset* ptr_dataset);
 void stm8Metering_uartISR( void);
 powerMonitor_Result stm8Metering_service( void);

#endif /* PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_H_ */

/*------------------------------ End of File -------------------------------*/
