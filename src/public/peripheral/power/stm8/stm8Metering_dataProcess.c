/*
==========================================================================
 Name        : stm8Metering_dataProcess.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/stm8/stm8Metering_dataProcess.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 1st April 2021
 Description : stm8 data processing implementation
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "config_pinout.h"
#include "config_dataset.h"
#include "sysmon.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "powerMonitor.h"
#include "stm8Metering.h"
#include "stm8Metering_impl.h"
#include "stm8Metering_dataProcess.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
 //#define STM8_VRMS_CONST                  (0.0095751)
#define STM8_VRMS_CONST                    (0.0096709)
#define STM8_IRMS_CONST                    (0.00015894)
//#define STM8_POWER_CONST                 (0.0000015222*1000)
#define STM8_POWER_CONST                   (0.0000015)

static powerMonitor_Measurements pwr;

/*****************************************************************************
 * Private Constants
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 *****************************************************************************/

/*******************************************************************************
 * @brief calculates the instant values based on the calibration values
 *
 * @param[in] stm8_rawValues  *    - pointer to the raw data RXD from the stm8
 *            stm8_CalibCoeff *    - pointer to the calibration coeffs
 *            stm8_instantValues * - pointer to the instant values
 *
 * @return    powerMonitor_Result - the result of the calculation
 *
  ******************************************************************************/
powerMonitor_Result stm8_calcInstantValues( stm8_rawValues     * raw_ptr,
		                                    stm8_CalibCoeff    * coeff_ptr,
						                    stm8_instantValues * inst_ptr)
{
	if(( raw_ptr != NULL) && ( coeff_ptr != NULL) && ( inst_ptr != NULL))
	{
		inst_ptr->voltage = (sqrt((float)raw_ptr->cv2) * STM8_VRMS_CONST)  * coeff_ptr->voltage;
		inst_ptr->current = (sqrt((float)raw_ptr->ci2) * STM8_IRMS_CONST)  * coeff_ptr->current;
		inst_ptr->power   = ((float)raw_ptr->cr        * STM8_POWER_CONST) * coeff_ptr->power;

		return POWER_MONITOR_RESULT_OK;
	}

	return POWER_MONITOR_RESULT_PARAM_ERROR;
}

/*******************************************************************************
 * @brief calculates the instant values based on the calibration values
 *
 * @param[in] stm8_rawValues     * - pointer to the raw data RXD from the stm8
 *            stm8_avgValues     * - pointer to the averaged values
 *            stm8_instantValues * - pointer to the instant values
 *            PowerCalcDataset   * - pointer to the dataset stored in abstraction layer
 *            float              * - pointer to the last power var
 *
 * @return    powerMonitor_Result - the result of the data processing
 *
  ******************************************************************************/
powerMonitor_Result stm8_processData( stm8_rawValues     * raw_ptr,
									  stm8_avgValues     * avg_ptr,
									  stm8_instantValues * instant_ptr,
		                              PowerCalcDataset   * dataset_ptr,
									  float              * last_power_ptr)
{
	powerMonitor_Result result = POWER_MONITOR_RESULT_OK;

    float volt_div = 0;
    float cur_div = 0;
    float pwr_div = 0;

    // validate the pointers
	if(( raw_ptr == NULL) ||
	   ( avg_ptr == NULL) ||
	   ( instant_ptr == NULL) ||
	   ( dataset_ptr == NULL) ||
	   ( last_power_ptr == NULL))
	{
        debug_printf( PWR, "stm8 ProcessData NULL ptr\r\n");

		return POWER_MONITOR_RESULT_PARAM_ERROR;
	}

    //---------------------------------------------------------------------
    // KF-118 Move flag parsing section before max-power check so as not
    //  to be ignore when power measurement is corrupt due to LOAR TX
    //---------------------------------------------------------------------
    // Check last-gasp status changes
    // Last-gasp detected but not yet reported to sys mon
    if(( raw_ptr->status & STM8_STATUS_POWER_LOSS_DETECTED) &&
       ( false == get_sysmon_last_gasp_is_active()))
    {
        debug_printf( PWR, "stm8 Power Fail\r\n");

    	return POWER_MONITOR_RESULT_POWER_FAIL;
    }
    // last-gasp recovered but not yet reported to sys mon
    else if(( 0x00 == (raw_ptr->status & STM8_STATUS_POWER_LOSS_DETECTED)) &&
            ( true == get_sysmon_last_gasp_is_active()))
    {
        debug_printf( PWR, "stm8 Power Restore\r\n");

        return POWER_MONITOR_RESULT_POWER_RESTORE;
    }

    //---------------------------------------------------------------------
    // KF-118 Move flag parsing section before max-power check so as not
    //  to be ignore when power measurement is corrupt due to LOAR TX
    //---------------------------------------------------------------------
    if( instant_ptr->power > 5000)
    {
        debug_printf(PWR, "stm8 Corrupt Meas %lu W > Ignore!\r\n", instant_ptr->power);

        return POWER_MONITOR_RESULT_DATA_ERROR;
    }

    *last_power_ptr = instant_ptr->power;

    //---------------------------------------------------------------------
    // add to the averaging values

    avg_ptr->volt    += instant_ptr->voltage;
    avg_ptr->current += instant_ptr->current;
    avg_ptr->power   += instant_ptr->power;

    // got enough samples so get average value ?
    if( ++avg_ptr->count >= STM8_NO_POWER_SAMPLES)
    {
        // Calibration variables
        volt_div = powf(2, (float)((raw_ptr->status & STM8_STATUS_VOLTAGE_RANGE_MASK) >> 8));
        cur_div  = powf(2, (float)((raw_ptr->status & STM8_STATUS_CURRENT_RANGE_MASK) >> 10));
        pwr_div  = volt_div * cur_div;

        //prevent divide by zero
        if( volt_div >= 0)
        {
            volt_div = 1.0f;
        }

        if( cur_div >= 0)
        {
            cur_div = 1.0f;
        }

        if( pwr_div >= 0)
        {
            pwr_div = 1.0f;
        }

        pwr.vrms = (avg_ptr->volt    / (float)avg_ptr->count) / volt_div;
        pwr.irms = (avg_ptr->current / (float)avg_ptr->count) / cur_div;
        pwr.p    = (avg_ptr->power   / (float)avg_ptr->count) / pwr_div;
        pwr.va   = (pwr.vrms * pwr.irms);

        /* prevent divide by zero */
        if (pwr.va > 0)
        {
            pwr.pf = pwr.p / pwr.va;
        }

        // reset the averaging vars.
        avg_ptr->count   = 0;
        avg_ptr->volt    = 0.0F;
        avg_ptr->current = 0.0F;
        avg_ptr->power   = 0.0F;

        debug_printf(PWR,
        		     "stm8 av: V10 %lu, mA %lu, mW %lu, mVA %lu, mPF %lu \r\n",
					 (uint32_t)((pwr.vrms + 0.5F) * 10),
					 (uint32_t)((pwr.irms + 0.5F) * 1000),
					 (uint32_t)((pwr.p + 0.5F) * 1000),
					 (uint32_t)((pwr.va + 0.5F) * 1000),
					 (uint32_t)((pwr.pf + 0.5F) * 1000));

        // Prevent the RTOS kernel swapping out the task whilst
        // the dataset is being retrieved.
        vTaskSuspendAll();

        //  Store averaged values in dataset
		dataset_ptr->instantVrms        = pwr.vrms;
		dataset_ptr->instantIrms        = pwr.irms;
		dataset_ptr->instantPowerFactor = pwr.pf;
		dataset_ptr->instantPower       = pwr.p;

		// keep track of the minimum and maximum voltages
		if( dataset_ptr->instantVrms > dataset_ptr->vrmsMax)
		{
			dataset_ptr->vrmsMax = dataset_ptr->instantVrms;
		}

		if( dataset_ptr->instantVrms < dataset_ptr->vrmsMin)
		{
			dataset_ptr->vrmsMin = dataset_ptr->instantVrms;
		}

        if (dataset_ptr->vrmsAverage == 0)
        {
            dataset_ptr->vrmsAverage = dataset_ptr->instantVrms;
        }
        else
        {
            dataset_ptr->vrmsAverage += dataset_ptr->instantVrms;
            dataset_ptr->vrmsAverage /= 2;
        }

		// The operation is complete. Restart the RTOS kernel.
	    xTaskResumeAll();
    }

    return result;
}

/*******************************************************************************
 * @brief checks the limits and thresholds of the raw data received from the stm8
 *
 * @param[in] stm8_rawValues     * - pointer to the raw data RXD from the stm8
 *
 * @return    powerMonitor_Result - the result of the noise shaping
 *
  ******************************************************************************/
powerMonitor_Result stm8_noiseShaping( stm8_rawValues * raw_ptr)
{
    if( raw_ptr == NULL)
    {
        debug_printf( PWR, "stm8 noiseShaping NULL ptr\r\n");

    	return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

	// noise shaping (KGP)
    if( raw_ptr->ci2 > STM8_CURRENT_OFFSET)
    {
    	raw_ptr->ci2 -= STM8_CURRENT_OFFSET;
    }
    else
    {
    	raw_ptr->ci2 = 0;
    }

    if( raw_ptr->cr > STM8_REAL_POWER_OFFSET)
    {
    	raw_ptr->cr -= STM8_REAL_POWER_OFFSET;
    }
    else
    {
    	raw_ptr->cr = 0;
    }

    if( raw_ptr->cv2 > STM8_REAL_POWER_VOLTAGE_THRESHOLD)
    {
        if( raw_ptr->cr > (raw_ptr->cv2 - STM8_REAL_POWER_VOLTAGE_THRESHOLD) / STM8_REAL_POWER_VOLTAGE_MODIFIER)
        {
        	raw_ptr->cr -= (raw_ptr->cv2 - STM8_REAL_POWER_VOLTAGE_THRESHOLD) / STM8_REAL_POWER_VOLTAGE_MODIFIER;
        }
        else
        {
        	raw_ptr->cr = 0;
        }
    }

    // Before correction below was testing on 3kW and forcing to 800W
    // Now testing to 3W and forcing to 800mW
    if( raw_ptr->cr < (STM8_POWER_MINIMUM_THRESHOLD / STM8_POWER_CONST / 1000))
    {
    	raw_ptr->cr = ((STM8_REPORTED_STANDBY_POWER + 1) / STM8_POWER_CONST / 1000);
    }

    return POWER_MONITOR_RESULT_OK;
}


/*******************************************************************************
 * @brief validates the checksum of the raw data received from the stm8
 *
 * @param[in] stm8_rawValues    * - pointer to the raw data RXD from the stm8
 *
 * @return    powerMonitor_Result - result of the checksum validation
 *
  ******************************************************************************/
powerMonitor_Result stm8_validateCheckum( stm8_rawValues * raw_ptr)
{
    if( raw_ptr == NULL)
    {
        debug_printf( PWR, "stm8 validateCheckum NULL ptr\r\n");

    	return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    if( raw_ptr->crc == (raw_ptr->cv2 ^ raw_ptr->ci2 ^ raw_ptr->cr ^ raw_ptr->status))
    {
        return POWER_MONITOR_RESULT_OK;
    }

    debug_printf( PWR, "stm8 checksum error\r\n");

    return POWER_MONITOR_RESULT_DATA_ERROR;
}


/*------------------------------ End of File -------------------------------*/

