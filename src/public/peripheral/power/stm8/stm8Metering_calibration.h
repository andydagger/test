/*
==========================================================================
 Name        : stm8Metering_calibration.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/stm8/stm8Metering_calibration.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 1st April 2021
 Description :
==========================================================================
*/
/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_CALIBRATION_H_
#define PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_CALIBRATION_H_

#include "powerMonitor.h"
#include "stdint.h"
#include "stdbool.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
// timeout is in 100ms increments
#define STM8_CALIBRATION_SAMPLE_TIMEOUT 10
// 100ms RTOS delay
#define STM8_CALIB_RTOS_DELAY           100

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
/*!
 * Power Calibration process State Machine states
 */
typedef enum __tag_STM8_CalibState
{
    STM8_CALIB_STATE_IDLE = 0,
    STM8_CALIB_STATE_ZERO_POINT_CAL,
    STM8_CALIB_STATE_WAIT_ZERO_POINT_CAL,
    STM8_CALIB_STATE_CALIB_CALC,
    STM8_CALIB_STATE_FINISHED,
    STM8_CALIB_STATE_FAILED
} stm8_CalibState;

#define STM8_REQUIRED_SAMPLE_COUNT   10

typedef struct __tag_stm8_sample
{
    float   samples[ STM8_REQUIRED_SAMPLE_COUNT];
    uint8_t curr_sample_idx;
    uint8_t sample_count;
    float   calib_stored;
    bool    all_samples_within_margins; // 3 consecutive samples within 1% window
} stm8_calibSample;

typedef struct __tag_stm8_samples
{
    stm8_calibSample volts;
    stm8_calibSample current;
    stm8_calibSample power;
} stm8_samples;


/*****************************************************************************
 * Public Variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

powerMonitor_Result stm8Metering_calibrate( PowerCalcDataset * dataset_ptr);
powerMonitor_Result stm8_calibProcessRxMessage(void);
powerMonitor_Result stm8Metering_applyCalibReadings( PowerCalcDataset * meter_dataset_ptr);

/*------------------------------- End of File --------------------------------*/
#endif /* PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_CALIBRATION_H_ */
