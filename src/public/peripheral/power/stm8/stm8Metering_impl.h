/*
==========================================================================
 Name        : stm8Metering_impl.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/stm8/stm8Metering_impl.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 31 May 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_IMPL_H_
#define PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_IMPL_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/

#include "stdint.h"
#include "stdbool.h"
#include "config_dataset.h"
#include "powerMonitor.h"
#include "stm8Metering.h"
#include "config_eeprom.h"
#include "NonVolatileMemory.h"

#define STM8_RX_BUFF_SIZE       20

#define STM8_INTER_BYTE_MAX_DELAY_MS   5

#define STM8_TX_BUFF_SIZE 		8
#define STM8_FLAGS_SIZE			4
#define STM8_FLAGS_POS			4
#define STM8_CRC_POS			0
#define STM8_CRC_SIZE	        4

#define NO_OF_STM8_TX			3

// raw values passed from stm8
typedef struct __Tag_stm8_rawValues
{
	uint32_t crc;      //CRC
	uint32_t status;   //status flags + revision number
	uint32_t cr;	   //real power
	uint32_t ci2;      //current
	uint32_t cv2;      //Voltage

} stm8_rawValues;

// value averaging structure
typedef struct __Tag_stm8_avgValues
{
    float   volt;
    float   current;
    float   power;
    uint8_t count;

} stm8_avgValues;

//get major minor, range and power flags value from raw status
typedef union __Tag_stm8_status
{
	uint32_t status;

	struct
	{
		uint8_t flags;  //bit 0 power loss, bit 1 zero point not set, bit 2 cal not set, bit 3 overflow error
		uint8_t range;  //bits 0,1 voltage range, bits 2,3 current range
		uint8_t minor;  //stm8 sw minor
		uint8_t major;  //stm8 sw major
	} bytes;

} stm8_status;

// status flags read from the stm8 (uint32_t)
#define STM8_STATUS_POWER_LOSS_DETECTED        0x00000001
#define STM8_STATUS_ZERO_PT_CAL_NOT_PERFORMED  0x00000002
#define STM8_STATUS_FREQUENCY_60HZ             0x00000004
#define STM8_STATUS_NOT_PASSED_SELF_TEST       0x00000080
#define STM8_STATUS_VOLTAGE_RANGE_MASK         0x00000300
#define STM8_STATUS_CURRENT_RANGE_MASK         0x00000C00
#define STM8_STATUS_FW_VERSION_MINOR_MASK      0x00FF0000
#define STM8_STATUS_FW_VERSION_MAJOR_MASK      0xFF000000

typedef union __Tag_stm8_flags
{
	uint32_t data_stm;

	struct
	{
		uint8_t data_0;  //not currently used
		uint8_t data_1;  //not currently used
		uint8_t data_2;  //not currently used
		uint8_t data_3;  //bit 0 lamp power
	} byte;

} stm8_flags;

// Flag definitions for the stm8 flags data_3
#define STM8_LOAD_RELAY_CLOSED     0x01
#define STM8_RESET_PRESERVE_CALIB  0x02
#define STM8_DALI_SET_VOLT_CONTROL 0x04
#define STM8_RESET_DISCARD_CALIB   0x08
#define STM8_LIGHT_RED_LED         0x80

typedef enum __tag_stm8_requestZeroCal
{
    STM8_REQUEST_NONE = 0,
    STM8_REQUEST_ZERO_CAL

} stm8_requestZeroCal;

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void stm8_init( void);
void stm8_initMeasurement( PowerCalcDataset * ptr_dataset);
void stm8_uartISR( void);
uint8_t * stm8_getRXBuffer( void);
stm8_CalibCoeff * stm8_getCalibCoeffs( void);
void stm8_saveCalibCoeffs( void);
bool stm8_isMessageReceived( void);
void stm8_resetMessageReceived( void);
void stm8_sendResponse( stm8_requestZeroCal request);
powerMonitor_Result stm8_processReceivedMessage( void);
float stm8_getPowerCalcPowerSample( void);

#endif /* PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_IMPL_H_ */
/*------------------------------ End of File -------------------------------*/
