/*
==========================================================================
 Name        : stm8Metering.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/stm8/stm8Metering.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 31 May 2018
 Description : stm8 power metering
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "math.h"
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "config_pinout.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "relay.h"
#include "config_dataset.h"
#include "sysmon.h"
#include "FreeRTOSMacros.h"
#include "nwk_com.h"
#include "powerMonitor.h"
#include "stm8Metering.h"
#include "stm8Metering_impl.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static uint32_t stm8_last_sample_time;
static bool     b_power_sampling_fault_latch;


/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief stm8 metering initialisation
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 * 
 * @param[in] void
 *
 * @return    void
 *
  ******************************************************************************/
void stm8Metering_init( void)
{
    // call the implementation layer
	stm8_init();
	// avoid running at 1st power-on
    stm8_last_sample_time = get_systick_sec();

	b_power_sampling_fault_latch = false;
}

/*******************************************************************************
 * @brief stm8 metering measurment initialisation
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] PowerCalcDataset *  - pointer to instant dataset
 *
 * @return    void
 *
  ******************************************************************************/
void stm8Metering_initMeasurement( PowerCalcDataset * ptr_dataset)
{
    // call the implementation layer
    stm8_initMeasurement( ptr_dataset);
}

/*******************************************************************************
 * @brief stm8 uart isr - mapped to UART1.
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void stm8Metering_uartISR( void)
{
    // call the implementation layer
	stm8_uartISR();
}

/*******************************************************************************
 * @brief stm8 metering service
 *        This is called from the vTaskSensorLfp every 100ms. The function
 *        contains the timings for receiving data from the stm8 power monitor
 *        All message processing is done in the implementation and data
 *        processing layers.
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - result of the service
 *
 *******************************************************************************/
powerMonitor_Result stm8Metering_service( void)
{
	powerMonitor_Result result = POWER_MONITOR_RESULT_IDLE;

    // check if data is waiting to be processed
    if( stm8_isMessageReceived() == false)
    {
        // Log Fault?
        if(( get_systick_sec() - stm8_last_sample_time) > STM8_POWER_SAMPLE_TIMEOUT_SEC)
        {
            // ignore if mains supply not present
            if( false == get_sysmon_last_gasp_is_active())
            {
				// only report once
				if( b_power_sampling_fault_latch == false)
				{
					b_power_sampling_fault_latch = true;

				    debug_printf( PWR, "stm8 sample timeout\r\n");

					result = POWER_MONITOR_RESULT_TIMEOUT;
				}
            }
        }
    }
    else
    {
		b_power_sampling_fault_latch = false;

		stm8_last_sample_time = get_systick_sec();
		// call the processing in the implementation layer
		result = stm8_processReceivedMessage();
    }

    return result;
}

/*------------------------------ End of File -------------------------------*/
