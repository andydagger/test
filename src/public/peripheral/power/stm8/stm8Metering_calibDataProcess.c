/*
==========================================================================
 Name        : stm8Metering_calibDataProcess.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/microchipMetering_calibDataProcess.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 25th April 2021
 Description : stm8 calibration data processing
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "config_pinout.h"
#include "config_dataset.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "sysmon.h"
#include "FreeRTOSMacros.h"
#include "relay.h"
#include "powerMonitor.h"
#include "stm8Metering.h"
#include "stm8Metering_impl.h"
#include "stm8Metering_dataProcess.h"
#include "stm8Metering_calibration.h"
#include "stm8Metering_calibDataProcess.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private Constants
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 *****************************************************************************/


/*******************************************************************************
 * @brief processes the received message status flags to check if zero point
 *        calibration has been successful
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - Result of the status
 *
 *******************************************************************************/
powerMonitor_Result stm8_getZeroPointCalibStatus( stm8_rawValues * raw_ptr)
{
    if( raw_ptr == NULL)
    {
        debug_printf(PWR, "stm8_getZeroPointCalibStatus NULL ptr\r\n");

        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }
    
    /* Poll for Zero-Point Cal to goto Zero to indicate it has finished */
    if( raw_ptr->status & STM8_STATUS_ZERO_PT_CAL_NOT_PERFORMED)
    {
        debug_printf( PWR, "stm8 Zero Point Cal in Progress\r\n");

        return POWER_MONITOR_RESULT_CALIBRATION_ACTIVE;
    }
    else
    {
        /* Is Self Test Bit 7 Zero'd */
        if( raw_ptr->status & STM8_STATUS_NOT_PASSED_SELF_TEST)
        {
            debug_printf( PWR, "stm8 Self Test Failed\r\n");

            return POWER_MONITOR_RESULT_CALIBRATION_FAILED;
        }
        else
        {
            /* Now start the Single Point Coeffs Calculation */
            debug_printf( PWR, "stm8 Single Point Cal can start\r\n");

            /* Next time round start Power Coeffs Calculation */
            return POWER_MONITOR_RESULT_OK;
        }
    }
}


/*******************************************************************************
 * @brief processes the calibration samples, checking the margins and
 *        limits of voltage, current and power
 *
 * @param[in] stm8_calibSample *  - pointer the samples structure
 *            float               - the new sample value
 *
 * @return    powerMonitor_Result - Result of processing the samples
 *
 *******************************************************************************/
powerMonitor_Result stm8_processCalibSamples( stm8_samples * samples_ptr, stm8_instantValues * instant_ptr)
{
    powerMonitor_Result result;

    if(( samples_ptr == NULL) || ( instant_ptr == NULL))
    {
        debug_printf( PWR, "stm8_processCalibSamples NULL ptr\r\n");

        result = POWER_MONITOR_RESULT_PARAM_ERROR;
    }
    else
    {
        result = POWER_MONITOR_RESULT_CALIBRATION_ACTIVE;

        // For every new sample - up to 10 samples
        if( false == samples_ptr->volts.all_samples_within_margins)
        {
            stm8_insertNewSample(&samples_ptr->volts, instant_ptr->voltage);

            samples_ptr->volts.all_samples_within_margins = stm8_checkSampleMargins(&samples_ptr->volts);
        }

        if( false == samples_ptr->current.all_samples_within_margins)
        {
            stm8_insertNewSample(&samples_ptr->current, instant_ptr->current);

            samples_ptr->current.all_samples_within_margins = stm8_checkSampleMargins(&samples_ptr->current);
        }

        if( false == samples_ptr->power.all_samples_within_margins)
        {
            stm8_insertNewSample(&samples_ptr->power, instant_ptr->power);

            samples_ptr->power.all_samples_within_margins = stm8_checkSampleMargins(&samples_ptr->power);
        }

        // were all consecutive samples within +/-1% margin??
        if((samples_ptr->volts.all_samples_within_margins) &&
           (samples_ptr->current.all_samples_within_margins) &&
           (samples_ptr->power.all_samples_within_margins))
        {
            // Now check against ref within +/- 10%
            if( STM8_LIMITS_RESULT_OK != stm8_validateSampleLimits(samples_ptr))
            {
                debug_printf( PWR, "stm8 Calibration Failed!!\r\n");

                result = POWER_MONITOR_RESULT_CALIBRATION_FAILED;
            }
            else
            {
                debug_printf( PWR, "stm8 Calibration Passed\r\n");

                result = POWER_MONITOR_RESULT_OK;

            }
        }
    }
    return result;
}

/*******************************************************************************
 * @brief adds a new sample into the sample array
 *        called from stm8_processCalibSamples()
 *
 * @param[in] stm8_calibSample *  - pointer the samples structure
 *            float               - the new sample value
 *
 * @return    powerMonitor_Result - Result of processing the samples
 *
 *******************************************************************************/
powerMonitor_Result stm8_insertNewSample( stm8_calibSample * sample_ptr, float new_sample)
{
    uint8_t index;
    
    if (sample_ptr == NULL)
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    for( index = 0; index < (STM8_REQUIRED_SAMPLE_COUNT-1); index++)
    {
        sample_ptr->samples[ index] = sample_ptr->samples[ index + 1];
    }

    sample_ptr->samples[ (STM8_REQUIRED_SAMPLE_COUNT-1)] = new_sample;
    sample_ptr->sample_count++;

    return POWER_MONITOR_RESULT_OK;
}

/*******************************************************************************
 * @brief adds a new sample into the sample array
 *        called from stm8_processCalibSamples()
 *
 * @param[in] stm8_calibSample *  - pointer the samples structure
 *
 * @return    bool - true if 3 samples received and samples OK, otherwise false
 *
 *******************************************************************************/
bool stm8_checkSampleMargins( stm8_calibSample * sample_ptr)
{
    float sample_min     = 99999.9F;
    float sample_max     = 0.0F;
    float sample_current = 0.0F;
    float sample_total   = 0.0F;
    uint8_t index;

    if (sample_ptr == NULL)
    {
        return false;
    }
 
    // return true if all samples within 1% margin

    if( sample_ptr->sample_count < STM8_REQUIRED_SAMPLE_COUNT)
    {
        return false;
    }

    for( index = 0; index < STM8_REQUIRED_SAMPLE_COUNT; index++)
    {
        sample_current = sample_ptr->samples[ index];

        /* Keep an accumulated total of the Samples */
        sample_total += sample_current;

        /* refresh the Min value or check if it's less than the previously stored value */
        if( sample_current < sample_min)
        {
            sample_min = sample_current;
        }

        /* Check if the stored Max it's greater than the previously stored value */
        if( sample_current > sample_max)
        {
            sample_max = sample_current;
        }
    }

    /* Is the Max within 102% of the Min */
    if( sample_min >= (sample_max * 0.98F))
    {
        /* Store the Average of the Samples */
        sample_ptr->calib_stored = sample_total / STM8_REQUIRED_SAMPLE_COUNT;

        return true;
    }

    return false;
}

/*******************************************************************************
 * @brief validate the samples are within limits
 *
 * @param[in] stm8_samples *  - pointer all the samples
 *
 * @return    uint8_t - Result of the limits check, bit mask based errors
 *
 *******************************************************************************/
uint8_t stm8_validateSampleLimits( stm8_samples * samples_ptr)
{
    uint8_t result = STM8_LIMITS_RESULT_OK;

    if( samples_ptr == NULL)
    {
        debug_printf( PWR, "stm8 SampleLimits NULL ptr\r\n");

        result = STM8_LIMITS_RESULT_ERROR_POINTER;
    }
    else
    {
        /* Is the Voltage with +- 10% of the recommended load */
        if(( samples_ptr->volts.calib_stored > VOLT_CALIB_MAX) ||
           ( samples_ptr->volts.calib_stored < VOLT_CALIB_MIN))
        {
            debug_printf( PWR, "stm8 calib fail voltage\r\n");

            result |= STM8_LIMITS_RESULT_ERROR_VOLTS;
        }

        /* Is the Current with +- 10% of the recommended load */
        if(( samples_ptr->current.calib_stored > CURRENT_CALIB_MAX) ||
           ( samples_ptr->current.calib_stored < CURRENT_CALIB_MIN))
        {
            debug_printf( PWR, "stm8 calib fail current\r\n");

            result |= STM8_LIMITS_RESULT_ERROR_CURRENT;
        }

        /* Is the Power with +- 10% of the recommended load */
        if(( samples_ptr->power.calib_stored > POWER_CALIB_MAX) ||
           ( samples_ptr->power.calib_stored < POWER_CALIB_MIN))
        {
            debug_printf( PWR, "stm8 calib fail power\r\n");

            result |= STM8_LIMITS_RESULT_ERROR_POWER;
        }

        debug_printf( PWR,
                      "stm calib stored: V10 %lu, mA %lu, mW %lu\r\n",
                      (uint32_t)(samples_ptr->volts.calib_stored * 10),
                      (uint32_t)(samples_ptr->current.calib_stored * 1000),
                      (uint32_t)(samples_ptr->power.calib_stored * 1000));
    }
    
    return result;
}

/*******************************************************************************
 * @brief calculates the coefficients values ready for storing to eeprom
 *
 * @param[in] PowerCalcDataset *  - calibration dataset pointer
 *            PowerCalcDataset *  - meter dataset pointer (values from meter)
 *
 * @return    powerMonitor_Result - result of the calculation
 *
 *******************************************************************************/
powerMonitor_Result stm8_calculateCoefficients( PowerCalcDataset * calib_data_ptr, PowerCalcDataset * meter_data_ptr)
{
    if(( calib_data_ptr == NULL) || ( meter_data_ptr == NULL))
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    // coeffs stored in the stmMetering_impl
    stm8_CalibCoeff* coeff_ptr = stm8_getCalibCoeffs();

    // Store Coefficient values in the stm8_metering_impl
    coeff_ptr->voltage = meter_data_ptr->instantVrms / calib_data_ptr->instantVrms;
    coeff_ptr->current = meter_data_ptr->instantIrms / calib_data_ptr->instantIrms;
    coeff_ptr->power   = meter_data_ptr->instantPower / calib_data_ptr->instantPower;

    debug_printf( PWR, "stm8 cal coeffs calculated: V:%0.4f C:%0.4f P:0.4f\r\n");

    return POWER_MONITOR_RESULT_OK;
}

/*******************************************************************************
 * @brief calculates the coefficients values ready for storing to eeprom
 *
 * @param[in] PowerCalcDataset * - pointer the calibration dataset
 *            stm8_samples *     - pointer to all the samples
 *
 * @return    void
 *
 *******************************************************************************/
void stm8_updateDataset( PowerCalcDataset * dataset_ptr, stm8_samples * samples_ptr)
{
    if(( dataset_ptr != NULL) && ( samples_ptr != NULL))
    {
        //  Store values in dataset
        dataset_ptr->instantVrms        = samples_ptr->volts.calib_stored;
        dataset_ptr->instantIrms        = samples_ptr->current.calib_stored;
        dataset_ptr->instantPower       = samples_ptr->power.calib_stored;

        dataset_ptr->instantPowerFactor = dataset_ptr->instantPower / (dataset_ptr->instantVrms * dataset_ptr->instantIrms);
    }
}


/*------------------------------ End of File -------------------------------*/

