/*
==========================================================================
 Name        : stm8Metering_impl.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/stm8/stm8Metering_impl.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 31 May 2018
 Description : Stm8 metering implementation layer
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "config_pinout.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "relay.h"
#include "sysmon.h"
#include "FreeRTOSMacros.h"
#include "nwk_com.h"
#include "config_dataset.h"
#include "config_eeprom.h"
#include "NonVolatileMemory.h"
#include "NVMData.h"
#include "powerMonitor.h"
#include "stm8Metering.h"
#include "stm8Metering_impl.h"
#include "stm8Metering_dataProcess.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/* format of rxbuff[]  (STM8 -> NXP)

voltage :- 			bytes 3,2,1,0  to make a uint32_t value
current :- 			bytes 7,6,5,4  to make uint32_t value
real power : - 		bytes 11,10,9,8 to make a uint32_t value
flags, control, revision: - 	bytes 15,14,13,12
CRC: - 				bytes 19,18,17,16 to make a uint32_t value

*/
static volatile uint8_t  stm8_rx_buffer[ STM8_RX_BUFF_SIZE];

/* format of txbuff[] (NXP -> STM8)
 	flags: - bytes 3,2,1,0
 	CRC: - 	 bytes 7,6,5,4
 */
static volatile uint8_t  stm8_tx_buffer[ STM8_TX_BUFF_SIZE];

// stm8 comms variables
static volatile bool     stm8_message_received;
static volatile uint8_t  stm8_comms_tx_message_count;
static volatile uint8_t  stm8_comms_rx_index;
static volatile uint8_t  stm8_comms_tx_index;
static volatile uint32_t prev_byte_ms_tick_count;

/* stm8 calculation variables */

static bool b_have_power_data;
static PowerCalcDataset * dataset_ptr;
static float last_power_sample;
static bool b_power_self_test_not_completed;
static bool b_stm8_lorawan_joined;

static stm8_CalibCoeff calibCoeffs;

static stm8_avgValues averagingValues;
static stm8_status    status_data;
static stm8_flags     flags;
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

extern QueueHandle_t xQuTxData; // vTaskTxData

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief stm8 metering initialisation
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    void
 *
  ******************************************************************************/
void stm8_init( void)
{
    stm8_message_received       = false;
    stm8_comms_tx_message_count = 0;
    stm8_comms_tx_index         = 0;
    stm8_comms_rx_index         = 0;
    prev_byte_ms_tick_count     = get_systick_ms();

    b_power_self_test_not_completed = false;
    b_stm8_lorawan_joined = false;

    // Setup the UART for the AD device 9600, 8bits ,no parity, 1 stop
    hw_setup_power_monitor_uart_params( STM8_UART_BAUD_RATE, STM8_UART_COMM_PARAMS);

	hw_power_monitor_uart_irq_enable();
}

/*******************************************************************************
 * @brief stm8 metering measurment initialisation
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] PowerCalcDataset *  - pointer to instant dataset
 *
 * @return    void
 *
  ******************************************************************************/
void stm8_initMeasurement( PowerCalcDataset * ptr_dataset)
{
    NVM_meteringCalibCoef_t storedCalib;

    dataset_ptr = ptr_dataset;

    NVMData_readMeteringCalibCoef( &storedCalib);

    calibCoeffs.voltage = storedCalib.voltage;
    calibCoeffs.current = storedCalib.current;
    calibCoeffs.power   = storedCalib.power;

    // check for divide by zero
    if ((calibCoeffs.voltage < FLT_MIN) && (calibCoeffs.voltage > -FLT_MIN))
    {
        calibCoeffs.voltage = 1.0F;
    }

    if ((calibCoeffs.current < FLT_MIN) && (calibCoeffs.current > -FLT_MIN))
    {
        calibCoeffs.current = 1.0F;
    }

    if ((calibCoeffs.power < FLT_MIN) && (calibCoeffs.power > -FLT_MIN))
    {
        calibCoeffs.power = 1.0F;
    }

    b_have_power_data = false;

    last_power_sample = 0.0F;

    flags.data_stm = 0;

    // reset the averaging vars.
	averagingValues.count   = 0;
	averagingValues.volt    = 0.0F;
	averagingValues.current = 0.0F;
	averagingValues.power   = 0.0F;

	//--------------------
	// debug print
	debug_printf(PWR,
		"stm8 volt calib coeff (*1000): %lu\r\n",
		(uint32_t)(calibCoeffs.voltage * 1000));

	debug_printf(PWR,
		"stm8 cur calib coeff (*1000): %lu\r\n",
		(uint32_t)(calibCoeffs.current * 1000));

	debug_printf(PWR,
		"stm8 pwr calib coeff (*1000): %lu\r\n",
		(uint32_t)(calibCoeffs.power * 1000));
}

/*******************************************************************************
 * @brief stm8 metering UART ISR
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    void
 *
  ******************************************************************************/
void stm8_uartISR( void)
{
    uint32_t intsrc;
    uint8_t  readbyte;

    intsrc = hw_power_monitor_uart_get_int_status();

    // received a byte ??
    if( intsrc & UART_STAT_RXRDY)
    {
        // always read the byte so it flushes the buffer
        readbyte = hw_power_monitor_uart_rx_byte();

    	// if time since last char was too long
		if(( get_systick_ms() - prev_byte_ms_tick_count) > STM8_INTER_BYTE_MAX_DELAY_MS)
        {
			// force this as being first byte
			stm8_comms_rx_index = 0;
		}

		// ignore data until previous string is processed
		if( stm8_message_received == true)
        {
			return;
		}

		if( stm8_comms_rx_index < STM8_RX_BUFF_SIZE)
		{
            prev_byte_ms_tick_count = get_systick_ms();

            stm8_message_received = false;

            stm8_rx_buffer[ stm8_comms_rx_index] = readbyte;
            
			// if this was the last char in the string
			if( ++stm8_comms_rx_index >= STM8_RX_BUFF_SIZE)
			{
				// mark string as complete
				stm8_message_received = true;

				stm8_comms_rx_index = 0;
				stm8_comms_tx_index = 0;
				
                //if data to send back to stm8 enable tx interrupt here
				if( stm8_comms_tx_message_count != 0)
				{
					hw_power_monitor_uart_tx_enable();

					stm8_comms_tx_message_count--;
				}
			}
		}
    }
    // Is the TXD ready to send another byte??
    else if( intsrc & UART_STAT_TXRDY)
    {
    	//tx bytes
    	if( stm8_comms_tx_index < STM8_TX_BUFF_SIZE)
    	{
    		hw_power_monitor_uart_tx_byte( stm8_tx_buffer[ stm8_comms_tx_index]);

			// last byte sent then disable tx interrupt
			if( ++stm8_comms_tx_index >= STM8_TX_BUFF_SIZE)
			{
				stm8_comms_tx_index = 0;

				hw_power_monitor_uart_tx_disable();
			}
    	}
    }
}

/*******************************************************************************
 * @brief returns a pointer to the stm8 receive buffer
 *
 * @param[in] void
 *
 * @return    uint8_t * - pointer to the byte array buffer
 *
  ******************************************************************************/
uint8_t * stm8_getRXBuffer( void)
{
    return (uint8_t*)stm8_rx_buffer;
}


/*******************************************************************************
 * @brief returns a pointer to the calibration coeffs
 *
 * @param[in] void
 *
 * @return    stm8_CalibCoeff * - pointer to the struct containing the coeffs
 *
  ******************************************************************************/
stm8_CalibCoeff * stm8_getCalibCoeffs( void)
{
    return &calibCoeffs;
}

/*******************************************************************************
 * @brief Saves the calibration values to eeprom
 *
 * @param[in] void
 *
 * @return    void
 *
  ******************************************************************************/
void stm8_saveCalibCoeffs( void)
{
    NVM_meteringCalibCoef_t storedCalib;

    NVMData_readMeteringCalibCoef( &storedCalib);

    // save the coeffs to NVM
    storedCalib.voltage = calibCoeffs.voltage;
    storedCalib.current = calibCoeffs.current;
    storedCalib.power   = calibCoeffs.power;

    NVMData_writeMeteringCalibCoef( &storedCalib);
}

/*******************************************************************************
 * @brief sends a message to the stm8
 *        This function is only referenced from implementation layer
 *
 * @param[in] uint32_t * - pointer to the 32bit word to send
 *
 * @return    void
 *
  ******************************************************************************/
void stm8_txMessage( uint32_t *datapwin)
{
	static uint32_t datalast = 255;

	// uint32_t gets copied as four bytes
	memcpy( (void *)&stm8_tx_buffer[STM8_CRC_POS], (const void*)datapwin, STM8_CRC_SIZE);

	// note as are only sending one uint32_t value for flags we just repeat
	// this flag value for CRC as no other values to compare against
	memcpy( (void *)&stm8_tx_buffer[STM8_FLAGS_POS], (const void*)datapwin, STM8_FLAGS_SIZE);

	// check if data has changed from last time
	if( *datapwin != datalast)
	{
		datalast = *datapwin;

		stm8_comms_tx_index = 0;
		// number of times the message needs to be sent
		stm8_comms_tx_message_count = NO_OF_STM8_TX;
	}
}

/*******************************************************************************
 * @brief returns whether a message has been received.
 *        called from the stm8Metering layer
 *
 * @param[in] void
 *
 * @return    bool - TRUE if messaged received, otherwise FALSE
 *
  ******************************************************************************/
bool stm8_isMessageReceived( void)
{
    return stm8_message_received;
}

/*******************************************************************************
 * @brief clears the message received flag.
 *        called from the calibration layer
 *
 * @param[in] void
 *
 * @return    void
 *
  ******************************************************************************/
void stm8_resetMessageReceived( void)
{
    stm8_message_received = false;
    // reset the receive index
    stm8_comms_rx_index = 0;
}

/*******************************************************************************
 * @brief sends the relay (lamp) state to the stm8
 *
 * @param[in] stm8_requestZeroCal - request zero cal by using STM8_REQUEST_ZERO_CAL
 *                   otherwise STM8_DONT_REQUEST_ZERO_CAL
 *
 * @return    bool - TRUE if messaged received, otherwise FALSE
 *
  ******************************************************************************/
void stm8_sendResponse( stm8_requestZeroCal request)
{
    flags.byte.data_3 = 0x00;

    //get relay state
	if( !b_get_relay_state())
	{
        flags.byte.data_3 = STM8_LOAD_RELAY_CLOSED;
	}

	if(( !get_nwk_com_is_connected()) && ( !b_stm8_lorawan_joined))
	{
		flags.byte.data_3 |= STM8_LIGHT_RED_LED;
	}
	else
	{
        b_stm8_lorawan_joined = true;
	}

	if( request == STM8_REQUEST_ZERO_CAL)
	{
        flags.byte.data_3 |= STM8_RESET_DISCARD_CALIB;
	}

	//return relay (lamp) state to stm8
	stm8_txMessage( &flags.data_stm);
}

/*******************************************************************************
 * @brief processes the received message from the stm8
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - the result of the message processing
 *
  ******************************************************************************/
powerMonitor_Result stm8_processReceivedMessage( void)
{
	powerMonitor_Result result = POWER_MONITOR_RESULT_IDLE;
    stm8_rawValues      rawvalues;
    stm8_instantValues  instantValues;

    stm8_resetMessageReceived();

    // copy the values from the message buffer
    memcpy( (void*)&rawvalues.crc, (const void*)stm8_rx_buffer, STM8_RX_BUFF_SIZE);

    // validate the checksum before further processing
    result = stm8_validateCheckum( &rawvalues);

    if( result == POWER_MONITOR_RESULT_OK)
    {
        // take a copy of the status
        status_data.status = rawvalues.status;

        // store state of stm8 self-test locally
        if( rawvalues.status & STM8_STATUS_NOT_PASSED_SELF_TEST)
        {
            b_power_self_test_not_completed = true;
        }
        else
        {
            b_power_self_test_not_completed = false;
        }

        result = stm8_noiseShaping( &rawvalues);

        if( result == POWER_MONITOR_RESULT_OK)
        {
            b_have_power_data = true;

            stm8_sendResponse( STM8_REQUEST_NONE);

            result = stm8_calcInstantValues( &rawvalues, &calibCoeffs, &instantValues);

            if( result == POWER_MONITOR_RESULT_OK)
            {
                // process the data including averaging, instant value calcs etc
                result = stm8_processData( &rawvalues,
                                           &averagingValues,
                                           &instantValues,
                                           dataset_ptr,
                                           &last_power_sample);
            }
        }
    }

    return result;
}

/*******************************************************************************
 * @brief get status of the stm8 self test
 *
 * @param[in] void
 *
 * @return    bool - TRUE if not completed (active), otherwise FALSE
 *
  ******************************************************************************/
bool stm8_selfTestNotCompleted( void)
{
    return b_power_self_test_not_completed;
}

/*******************************************************************************
 * @brief Return the last power sample (not averaged)
 *
 * @param[in] void
 *
 * @return    float  - power sample in watts
 *
  ******************************************************************************/
float stm8_getPowerCalcPowerSample(void)
{
    return last_power_sample;
}

/*******************************************************************************
 * @brief get stm8 version no and flags
 *
 * @param[in] uint8_t * - pointer to copy the data to
 *
 * @return    bool - b_have_power_data
 *
  ******************************************************************************/
bool stm8_getPowerVersionFlags(uint8_t* data)
{
    // Prevent the RTOS kernel swapping out the task whilst
    // the dataset is being retrieved.
    vTaskSuspendAll();

    memcpy( (void*)data,
    		(const void*)&status_data,
			sizeof(stm8_status));

    // The operation is complete.  Restart the RTOS kernel.
    xTaskResumeAll();

    return b_have_power_data;
}

/*******************************************************************************
 * @brief Return the FW version of the power board major & minor
 *
 * @param[in] void
 *
 * @return    uint16_t - MSB = major, LSB = minor
 *
  ******************************************************************************/
uint16_t stm8_getFirmwareVersion( void)
{
    uint16_t ver = 0;

    // Prevent the RTOS kernel swapping out the task whilst
    // the dataset is being retrieved.
    vTaskSuspendAll();

    ver  = (uint16_t)(status_data.bytes.major << 8);
    ver |= (uint16_t)status_data.bytes.minor;

    // The operation is complete.  Restart the RTOS kernel.
    xTaskResumeAll();

    return ver;
}

/*------------------------------ End of File -------------------------------*/
