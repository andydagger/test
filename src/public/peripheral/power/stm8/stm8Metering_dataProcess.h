/*
==========================================================================
 Name        : stm8Metering_dataProcess.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/stm8/stm8Metering_dataProcess.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 31 May 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_DATAPROCESS_H_
#define PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_DATAPROCESS_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "stm8Metering.h"
#include "stm8Metering_impl.h"


//#define STM8_CURRENT_OFFSET 3800
#define STM8_CURRENT_OFFSET                1900
#define STM8_REAL_POWER_OFFSET             20000
#define STM8_REAL_POWER_VOLTAGE_THRESHOLD  550000000
#define STM8_REAL_POWER_VOLTAGE_MODIFIER   2000
//#define STM8_REAL_POWER_VOLTAGE_THRESHOLD 350000000
//#define STM8_REAL_POWER_VOLTAGE_MODIFIER 1000

#define STM8_POWER_MINIMUM_THRESHOLD       3000 // in mW
#define STM8_REPORTED_STANDBY_POWER        800 // in mW

#define STM8_NO_POWER_SAMPLES              60

typedef struct
{
    float vrms;
    float irms;
    float p;
    float va;
    float pf;
} powerMonitor_Measurements;

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

 powerMonitor_Result stm8_calcInstantValues( stm8_rawValues     * raw_ptr,
                                                   stm8_CalibCoeff    * coeff_ptr,
                                                   stm8_instantValues * inst_ptr);

 powerMonitor_Result stm8_processData( stm8_rawValues   * raw_ptr,
		                                     stm8_avgValues   * avg_ptr,
 										     stm8_instantValues * instant_ptr,
                                             PowerCalcDataset * dataset_ptr,
		                                     float            * last_power_ptr);

 powerMonitor_Result stm8_noiseShaping( stm8_rawValues * raw_ptr);
 powerMonitor_Result stm8_validateCheckum( stm8_rawValues * raw_ptr);

#endif /* PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_DATAPROCESS_H_ */
/*------------------------------ End of File -------------------------------*/
