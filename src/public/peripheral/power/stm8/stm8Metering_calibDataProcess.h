/*
==========================================================================
 Name        : stm8Metering_calibDataProcess.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/stm8/stm8Metering_calibDataProcess.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 26th April 2021
 Description :
==========================================================================
*/
/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_CALIBDATAPROCESS_H_
#define PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_CALIBDATAPROCESS_H_

#include "config_dataset.h"
#include "powerMonitor.h"
#include "stm8Metering_impl.h"
#include "stm8Metering_calibration.h"
#include "stdint.h"
#include "stdbool.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define VOLT_CALIB_CHECK_VAL         240 // 240VAC
#define CURR_CALIB_CHECK_VAL         0.417F // I = P/V
#define POWER_CALIB_CHECK_VAL        100 // 100W
#define POSITIVE_RANGE               1.10F  // +10 %
#define NEGATIVE_RANGE               0.90F  // -10 %

#define VOLT_CALIB_MAX      (VOLT_CALIB_CHECK_VAL * POSITIVE_RANGE)
#define VOLT_CALIB_MIN      (VOLT_CALIB_CHECK_VAL * NEGATIVE_RANGE)
#define CURRENT_CALIB_MAX   (CURR_CALIB_CHECK_VAL * POSITIVE_RANGE)
#define CURRENT_CALIB_MIN   (CURR_CALIB_CHECK_VAL * NEGATIVE_RANGE)
#define POWER_CALIB_MAX     (POWER_CALIB_CHECK_VAL * POSITIVE_RANGE)
#define POWER_CALIB_MIN     (POWER_CALIB_CHECK_VAL * NEGATIVE_RANGE)

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
typedef enum __Tag_stm8_limitsResult
{
	STM8_LIMITS_RESULT_OK = 0,
	STM8_LIMITS_RESULT_ERROR_POINTER = 0x01,
	STM8_LIMITS_RESULT_ERROR_VOLTS = 0x02,
	STM8_LIMITS_RESULT_ERROR_CURRENT = 0x04,
	STM8_LIMITS_RESULT_ERROR_POWER = 0x08,

} stm8_limitsResult;

/*****************************************************************************
 * Public Variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

 powerMonitor_Result stm8_getZeroPointCalibStatus( stm8_rawValues * raw_ptr);
 powerMonitor_Result stm8_processCalibSamples( stm8_samples * samples_ptr, stm8_instantValues * instant_ptr);
 powerMonitor_Result stm8_insertNewSample( stm8_calibSample * sample_ptr, float latest_sample);
 powerMonitor_Result stm8_calculateCoefficients( PowerCalcDataset * calib_data_ptr, PowerCalcDataset * meter_data_ptr);
 bool stm8_checkSampleMargins( stm8_calibSample * sample_ptr);
 uint8_t stm8_validateSampleLimits( stm8_samples * samples_ptr);
 void stm8_updateDataset( PowerCalcDataset * dataset_ptr, stm8_samples * samples_ptr);

/*------------------------------- End of File --------------------------------*/
#endif /* PUBLIC_PERIPHERAL_POWER_STM8_STM8METERING_CALIBDATAPROCESS_H_ */
