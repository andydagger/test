/*
==========================================================================
 Name        : ade9153aMetering_calibStartStop.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices_ade9153aMetering_calibStartStop.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 11th June 2021
 Description :
==========================================================================
*/
/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_CALIBSTARTSTOP_H_
#define PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_CALIBSTARTSTOP_H_

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#include "powerMonitor.h"

 /*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public Variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
powerMonitor_Result ade9153a_startCalibration( uint32_t calToStart);
powerMonitor_Result ade9153a_stopCalibration( void);


/*------------------------------- End of File --------------------------------*/
#endif /* PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_CALIBSTARTSTOP_H_ */
