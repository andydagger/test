/*
==========================================================================
 Name        : ade9153aMetering_calibDataProcess.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices_ade9153aMetering_calibDataProcess.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 17th May 2021
 Description :
==========================================================================
*/
/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_CALIBDATAPROCESS_H_
#define PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_CALIBDATAPROCESS_H_

#include "stdint.h"
#include "stdbool.h"
#include "powerMonitor.h"
#include "ade9153aMetering.h"
#include "ade9153aMetering_calibration.h"
#include "ade9153aMetering_calibStartStop.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/


/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public Variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
powerMonitor_Result ade9153a_calibrationReadValues( PowerCalcDataset * dataset_ptr);
powerMonitor_Result ade9153a_readAutoCalRegisters( ade9153a_autoCalRegs * regs_ptr);
powerMonitor_Result ade9153a_applyCalibrationGains( ade9153a_autoCalRegs * regs_ptr);
powerMonitor_Result ade9153a_calibrateCurrentChannel( void);
powerMonitor_Result ade9153a_calibrateVoltageChannel( void);

/*------------------------------- End of File --------------------------------*/
#endif /* PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_CALIBDATAPROCESS_H_ */
