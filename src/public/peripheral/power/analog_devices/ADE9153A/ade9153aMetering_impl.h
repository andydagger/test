/*
==========================================================================
 Name        : ade9153aMetering_impl.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices/ade9153aMetering_impl.h
 Author      : jeff barker
 Copyright   : Lucy Zodion Ltd
 Created     : 21 May 2021
 Description : 
==========================================================================
*/

#ifndef PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADMETERING_IMPL_H_
#define PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADMETERING_IMPL_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "config_dataset.h"      // these included for ceedling unit test
#include "powerMonitor.h"
#include "ade9153aMetering.h"
#include "ade9153aMetering_dataProcess.h"
#include "ade9153aMetering_transport.h"


/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

// Startup delay after issuing a RUN command
#define ADE9153A_STARTUP_DELAY         100
// temperature acquisition delay
#define ADE9153A_TEMP_ACQ_DELAY        2

#define ADE9153A_VERSION_PRODUCT_VAL   0x0009153A  // Product part number in hex


/* Configuration Registers */
#define ADE9153A_AI_PGAGAIN_INIT_VAL   0x000A      // Signal on IAN, current channel gain=16x
#define ADE9153A_CONFIG0_INIT_VAL      0x00000000  // Datapath settings at default
#define ADE9153A_CONFIG1_INIT_VAL      0x0300      // Chip settings at default
#define ADE9153A_CONFIG2_INIT_VAL      0x0C00      // High-pass filter corner, fc=0.625Hz
#define ADE9153A_CONFIG3_INIT_VAL      0x0000      // Peak and overcurrent settings
#define ADE9153A_ACCMODE_INIT_VAL      0x0010      // Energy accumulation modes, Bit 4, 0 for 50Hz, 1 for 60Hz

#define ADE9153A_VLEVEL_INIT_VAL       0x002C11E8  // Assuming Vnom=1/2 of fullscale
#define ADE9153A_ZX_CFG_INIT_VAL       0x0000      // ZX low-pass filter select

#define ADE9153A_ACT_NL_LVL_INIT_VAL   0x000033C8  // (13256)
#define ADE9153A_REACT_NL_LVL_INIT_VAL 0x000033C8
#define ADE9153A_APP_NL_LVL_INIT_VAL   0x000033C8

/* Constant Definitions */
#define ADE9153A_RUN_ON                0x0001      // DSP On
#define ADE9153A_COMPMODE_INIT_VAL     0x0005      // Initialize for proper operation

#define ADE9153A_VDIV_RSMALL_INIT_VAL  0x03E8      // Small resistor on board is 1kOhm=0x3E8

/* Energy Accumulation Settings */
#define ADE9153A_EP_CFG_INIT_VAL       0x0009      // Enable accumulators and reset on read, no-load sample = 64
#define ADE9153A_EGY_TIME_INIT_VAL     0x0F9F      // Accumulate energy for 4000 samples

/* Temperature Sensor Settings */
#define ADE9153A_TEMP_CFG_INIT_VAL     0x000C      // Temperature sensor configuration

// these level values equate to the level detection being switched off
// voltage level detection will need to change dependent on input voltage
#define ADE9153A_REG_SWELL_LVL_INIT_VAL  0x00FFFFFF  // (260 / ADE9153A_CAL_VRMS_CC) * 1000;
#define ADE9153A_REG_SWELL_CYC_INIT_VAL  0xFFFF
#define ADE9153A_REG_DIP_LVL_INIT_VAL    0x00000000  // (220 / ADE9153A_CAL_VRMS_CC) * 1000;
#define ADE9153A_REG_DIP_CYC_INIT_VAL    0xFFFF
#define ADE9153A_REG_OI_LVL_INIT_VAL     0x00FFFFFF




// AIGAIN to -1 to account for IAP-IAN swap
#define ADE9153A_IGAIN_INIT_VAL        (uint32_t)((int32_t)-268435456)


//#define ADE9153A_MASK_INIT_VAL         0x00000100  // Enable EGYRDY interrupt
#define ADE9153A_MASK_INIT_VAL         (ADE9153A_MASK_CHIP_STAT_MASK  | \
                                        ADE9153A_MASK_EVENT_STAT_MASK | \
                                        ADE9153A_MASK_MSURE_STAT_MASK | \
                                        ADE9153A_MASK_WATTNL_MASK)



/* Ideal Calibration Values for ADE9153A Shield Based on Sensor Values */
#define ADE9153A_CAL_IRMS_CC           838.190     // (uA/code)
#define ADE9153A_CAL_VRMS_CC           13411.05    // (uV/code)
#define ADE9153A_CAL_POWER_CC          1508.743    // (uW/code) Applicable for Active, reactive and apparent power
#define ADE9153A_CAL_ENERGY_CC         0.858307    // (uWhr/xTHR_HI code)Applicable for Active, reactive and apparent energy

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void ade9153a_init( void);
void ade9153a_initMeasurement( PowerCalcDataset * ptr_dataset);
powerMonitor_Result ade9153a_hardware_detect( void);
powerMonitor_Result ade9153a_readAndProcessInstantData( void);
powerMonitor_Result ade9153a_readTemperature( float * temp_ptr);
powerMonitor_Result ade9153a_processEvent( void);

#endif /* PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADMETERING_IMPL_H_ */
/*------------------------------ End of File -------------------------------*/
