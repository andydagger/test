/*
==========================================================================
 Name        : ade9153aMetering_dataProcess.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices/ade9153aMetering_dataProcess.h
 Author      : jeff barker
 Copyright   : Lucy Zodion Ltd
 Created     : 21 May 2021
 Description : 
==========================================================================
*/

#ifndef PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_DATAPROCESS_H_
#define PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_DATAPROCESS_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "ade9153aMetering.h"
#include "ade9153aMetering_impl.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
powerMonitor_Result ade9153a_calcInstantValues( ade9153a_rawValues * raw_ptr, ade9153a_instantValues * instant_ptr);
powerMonitor_Result ade9153a_addValuesToDataset( ade9153a_instantValues * instant_ptr, PowerCalcDataset * dataset_ptr);
powerMonitor_Result ade9153a_readInstantRegisterValues(ade9153a_rawValues* raw_ptr);


#endif /* PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_DATAPROCESS_H_ */

/*------------------------------ End of File -------------------------------*/

