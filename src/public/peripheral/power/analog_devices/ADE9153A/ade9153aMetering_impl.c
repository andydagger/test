/*
==========================================================================
 Name        : ade9153aMetering_impl.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices/ade9153aMetering_impl.c
 Author      : jeff barker
 Copyright   : Lucy Zodion Ltd
 Created     : 21 May 2021
 Description : analog devices metering implementation layer
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "math.h"
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "config_pinout.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "relay.h"
#include "config_dataset.h"
#include "sysmon.h"
#include "FreeRTOS.h"
#include "FreeRTOSCommonHooks.h"
#include "powerMonitor.h"
#include "ADE9153A.h"
#include "ade9153aMetering.h"
#include "ade9153aMetering_impl.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/* calculation variables */
static PowerCalcDataset *  ad_dataset_ptr;
static ade9153a_rawValues  ad_regValues;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Analog devices metering initialisation
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    void
 *
  ******************************************************************************/
void ade9153a_init( void)
{
    ad_dataset_ptr = NULL;
}

/*******************************************************************************
 * @brief analog devices metering measurement initialisation
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] PowerCalcDataset *  - pointer to instant dataset
 *
 * @return    void
 *
  ******************************************************************************/
void ade9153a_initMeasurement( PowerCalcDataset * ptr_dataset)
{
	ad_dataset_ptr = ptr_dataset;

	ade9153a_writeRegister_16( ADE9153A_REG_AI_PGAGAIN_16,   ADE9153A_AI_PGAGAIN_INIT_VAL);

    ade9153a_writeRegister_32( ADE9153A_REG_CONFIG0_32,      ADE9153A_CONFIG0_INIT_VAL);

    ade9153a_writeRegister_16( ADE9153A_REG_CONFIG1_16,      ADE9153A_CONFIG1_INIT_VAL);
	ade9153a_writeRegister_16( ADE9153A_REG_CONFIG2_16,      ADE9153A_CONFIG2_INIT_VAL);
	ade9153a_writeRegister_16( ADE9153A_REG_CONFIG3_16,      ADE9153A_CONFIG3_INIT_VAL);
	ade9153a_writeRegister_16( ADE9153A_REG_ACCMODE_16,      ADE9153A_ACCMODE_INIT_VAL);
	ade9153a_writeRegister_32( ADE9153A_REG_VLEVEL_32,       ADE9153A_VLEVEL_INIT_VAL);
    ade9153a_writeRegister_16( ADE9153A_REG_ZX_CFG_16,       ADE9153A_ZX_CFG_INIT_VAL);
    // NO LOAD detection levels
    ade9153a_writeRegister_32( ADE9153A_REG_ACT_NL_LVL_32,   ADE9153A_ACT_NL_LVL_INIT_VAL);
    ade9153a_writeRegister_32( ADE9153A_REG_REACT_NL_LVL_32, ADE9153A_REACT_NL_LVL_INIT_VAL);
    ade9153a_writeRegister_32( ADE9153A_REG_APP_NL_LVL_32,   ADE9153A_APP_NL_LVL_INIT_VAL);
    // resistor size
    ade9153a_writeRegister_32( ADE9153A_REG_VDIV_RSMALL_32,  ADE9153A_VDIV_RSMALL_INIT_VAL);
    ade9153a_writeRegister_16( ADE9153A_REG_COMPMODE_16,     ADE9153A_COMPMODE_INIT_VAL);
    ade9153a_writeRegister_16( ADE9153A_REG_EGY_TIME_16,     ADE9153A_EGY_TIME_INIT_VAL);       // Energy accumulation ON
    ade9153a_writeRegister_16( ADE9153A_REG_TEMP_CFG_16,     ADE9153A_TEMP_CFG_INIT_VAL);

    // initialise the thresholds to off as we can't set the limits until we 
    // have calibrated and know the voltage etc
    // Voltage Swell threshold and number of cycles
    ade9153a_writeRegister_32(ADE9153A_REG_SWELL_LVL_32,     ADE9153A_REG_SWELL_LVL_INIT_VAL);
    ade9153a_writeRegister_16(ADE9153A_REG_SWELL_CYC_16,     ADE9153A_REG_SWELL_CYC_INIT_VAL);

    // Voltage Dip threshold and number of cycles
    ade9153a_writeRegister_32(ADE9153A_REG_DIP_LVL_32,       ADE9153A_REG_DIP_LVL_INIT_VAL);
    ade9153a_writeRegister_16(ADE9153A_REG_DIP_CYC_16,       ADE9153A_REG_DIP_CYC_INIT_VAL);
    // over current threshold
    ade9153a_writeRegister_32(ADE9153A_REG_OI_LVL_32,        ADE9153A_REG_OI_LVL_INIT_VAL);

    // AIGAIN to -1 to account for IAP-IAN swap
    ade9153a_writeRegister_32( ADE9153A_REG_AIGAIN_32,       ADE9153A_IGAIN_INIT_VAL);
    // enable the DSP and energy accumulation
    ade9153a_writeRegister_16( ADE9153A_REG_EP_CFG_16,       ADE9153A_EP_CFG_INIT_VAL);

    // prorgam the events to trigger interrupt line
    ade9153a_writeRegister_32( ADE9153A_REG_MASK_32,         ADE9153A_MASK_INIT_VAL);
}

/*******************************************************************************
 * @brief analog devices hardware detection
 *        This is called from the power monitor abstraction layer to detect
 *        if the analog devices part is present.
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result -result of the HW detection
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_hardware_detect( void)
{
    powerMonitor_Result result;
    uint32_t version;

    ade9153a_writeRegister_16( ADE9153A_REG_RUN_16, ADE9153A_RUN_ON);
    // delay 100ms
    FreeRTOSDelay( ADE9153A_STARTUP_DELAY);

    result = ade9153a_readRegister_32( ADE9153A_REG_VERSION_PRODUCT_32, &version);

    if( result == POWER_MONITOR_RESULT_OK)
    {
        if( version != ADE9153A_VERSION_PRODUCT_VAL)
        {
            debug_printf( PWR, "ade9153a HW detected but wrong version %08x\r\n", version);

            result = POWER_MONITOR_RESULT_HW_NOT_DETECTED;
        }
    }
    else
    {
        result = POWER_MONITOR_RESULT_HW_NOT_DETECTED;
    }


    return result;
}

/*******************************************************************************
 * @brief read the raw values, calcs the true values, update the dataset from the device
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - result of the process
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_readAndProcessInstantData( void)
{
    powerMonitor_Result     result;
    ade9153a_instantValues  instantValues = {0};

    result = ade9153a_readInstantRegisterValues( &ad_regValues);

    if( result == POWER_MONITOR_RESULT_OK)
    {
        result = ade9153a_calcInstantValues( &ad_regValues, &instantValues);

        if( result == POWER_MONITOR_RESULT_OK)
        {
            result = ade9153a_addValuesToDataset( &instantValues, ad_dataset_ptr);
        }
    }

    return result;
}

/*******************************************************************************
 * @brief read the Temperature register and converts to degC
 *        this is not currently part of the dataset but could be later and/or used for debug
 *        if not used it will be linked out of the executable
 *
 * @param[in] float * pointer to float that holds the temperature
 *
 * @return    powerMonitor_Result - result of the process
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_readTemperature( float * temp_ptr)
{
    powerMonitor_Result result;
    uint32_t trim;
    uint16_t gain;
    uint16_t offset;
    uint16_t tempReg;

    if( temp_ptr == NULL)
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    // Start temperature acquisition cycle
    result = ade9153a_writeRegister_16( ADE9153A_REG_TEMP_CFG_16, ADE9153A_TEMP_CFG_INIT_VAL);

    if( result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    // Wait acquisition cycle
    FreeRTOSDelay( ADE9153A_TEMP_ACQ_DELAY);

    // read the trim gain and offset values
    result = ade9153a_readRegister_32( ADE9153A_REG_TEMP_TRIM_32, &trim);

    if( result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    // Read Temperature result register
    result = ade9153a_readRegister_16( ADE9153A_REG_TEMP_RSLT_16, &tempReg);

    if( result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    // Extract gain (16bit LSW)
    gain = (uint16_t)(trim & 0xFFFF);

    // Extract offset (16bit MSW)
    offset = (uint16_t)(trim >> 16);

    *temp_ptr = ((float)offset / 32.00) - ((float)tempReg * (float)gain / (float)131072);

    return result;
}

/*******************************************************************************
 * @brief reads the status regs to determine what event has occurred
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - result of the processing
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_processEvent( void)
{
    powerMonitor_Result result;
    uint32_t status;
    uint16_t status_irq;
    uint16_t chip_status;
    uint16_t event_status;

    // Reading the status tells us if chip and/or event status bits are set
    result = ade9153a_readRegister_32( ADE9153A_REG_STATUS_32, &status);

    if( result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    // Chip interrupt flags are Tier 2 and need the chip status to be read
    // UART errors need the UARt to be reset
    // HW error flags need a software or hard reset
    if( status & ADE9153A_STATUS_CHIP_STAT_MASK)
    {
        // chip status includes UART and HW errors 0 to 3
        result = ade9153a_readRegister_16( ADE9153A_REG_CHIP_STATUS_16, &chip_status);

        if( result != POWER_MONITOR_RESULT_OK)
        {
            return result;
        }

        // TODO: process the HW and UART errors here

        if( chip_status & ADE9153A_CHIP_STATUS_HW_ERROR_MASK)
        {
            // TODO: HARD OR SOFT RESET HERE
            // Must return after a chip reset as no comms until power-on timeout
            // has elapsed
            return result;
        }

        if( chip_status & ADE9153A_CHIP_STATUS_UART_ERROR_MASK)
        {
            // TODO: RESET THE UART HERE
        }
    }

    //
    if( status & ADE9153A_STATUS_EVENT_STAT_MASK)
    {
        result = ade9153a_readRegister_16( ADE9153A_REG_EVENT_STATUS_16, &event_status);

        if( result != POWER_MONITOR_RESULT_OK)
        {
            return result;
        }

        // over current channel A?
        if( event_status & ADE9153A_EVENT_STATUS_OIA_MASK)
        {
            // TODO: process the over current here
        }

        // Voltage channel Swell?
        if( event_status & ADE9153A_EVENT_STATUS_SWELLA_MASK)
        {
            // TODO: process the voltage swell here
        }

        // Voltage channel DIP?
        if( event_status & ADE9153A_EVENT_STATUS_DIPA_MASK)
        {
            // TODO: process the voltage dip here
        }
    }

    if( status & ADE9153A_STATUS_MSURE_STAT_MASK)
    {
        result = ade9153a_readRegister_16( ADE9153A_REG_MS_STATUS_IRQ_16, &status_irq);

        if( result != POWER_MONITOR_RESULT_OK)
        {
            return result;
        }

        // TODO: MSURE Status processing here
    }

    // Tier 1 interrupt flags are cleared by writing a 1 back to the register
    if( event_status & ADE9153A_STATUS_TIER_1_MASK)
    {
        // This used for the entry/exit no-load condition
        result = ade9153a_writeRegister_32( ADE9153A_REG_STATUS_32, (event_status & ADE9153A_STATUS_TIER_1_MASK));

        if( result != POWER_MONITOR_RESULT_OK)
        {
            return result;
        }

        // TODO: Status processing here -> No load conditions
    }

    return result;
}


/*------------------------------ End of File -------------------------------*/
