/*
==========================================================================
 Name        : ade9153aMetering_impl.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices/ade9153aMetering_transport.h
 Author      : jeff barker
 Copyright   : Lucy Zodion Ltd
 Created     : 10th June 2021
 Description : 
==========================================================================
*/

#ifndef PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADMETERING_TRANSPORT_H_
#define PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADMETERING_TRANSPORT_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "powerMonitor.h"
#include "ade9153aMetering.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define ADE9153A_RX_BUFF_SIZE          8
#define ADE9153A_TX_BUFF_SIZE          8

// UART transmit buffer indices
#define ADE9153A_TX_IDX_CMD0           0
#define ADE9153A_TX_IDX_CMD1           1
#define ADE9153A_TX_IDX_DATA0          2
#define ADE9153A_TX_IDX_DATA1          3
#define ADE9153A_TX_IDX_CRC_MSB_16     4
#define ADE9153A_TX_IDX_CRC_LSB_16     5
#define ADE9153A_TX_IDX_DATA2          4
#define ADE9153A_TX_IDX_DATA3          5
#define ADE9153A_TX_IDX_CRC_MSB_32     6
#define ADE9153A_TX_IDX_CRC_LSB_32     7

// UART receive buffer indices
#define ADE9153A_RX_IDX_DATA0          0
#define ADE9153A_RX_IDX_DATA1          1
#define ADE9153A_RX_IDX_CRC_MSB_16     2
#define ADE9153A_RX_IDX_CRC_LSB_16     3
#define ADE9153A_RX_IDX_DATA2          2
#define ADE9153A_RX_IDX_DATA3          3
#define ADE9153A_RX_IDX_CRC_MSB_32     4
#define ADE9153A_RX_IDX_CRC_LSB_32     5

// protocol message sizes
#define ADE9153A_READ_TX_SIZE          2
#define ADE9153A_WRITE_TX_SIZE_16      6
#define ADE9153A_WRITE_TX_SIZE_32      8

#define ADE9153A_READ_RX_SIZE_NONE     0
#define ADE9153A_READ_RX_SIZE_16       4
#define ADE9153A_READ_RX_SIZE_32       6

// serial delay for send message polling loop
#define ADE9153A_SERIAL_MSG_DELAY_MS   5
// timeout for tx and rx of message
// max 8 bytes @ 4800 = 16.67ms, also allow for other task timeslice
#define ADE9153A_SERIAL_MSG_TIMEOUT_MS 25

// serial protocol command bit defines
#define ADE9153A_CMD_READ              0x0008
#define ADE9153A_CMD_CHECKSUM          0x0004
// CRC16 seed used in the serial protocol
#define ADE9153A_CRC16_SEED            0xFFFF

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void ade9153a_transportInit( void);
void ade9153a_uartISR( void);
powerMonitor_Result ade9153a_readRegister_16( uint16_t address, uint16_t * dataPtr);
powerMonitor_Result ade9153a_readRegister_32( uint16_t address, uint32_t * dataPtr);
powerMonitor_Result ade9153a_writeRegister_16( uint16_t address, uint16_t value);
powerMonitor_Result ade9153a_writeRegister_32( uint16_t address, uint32_t value);
powerMonitor_Result ade9153a_sendMessage( uint8_t tx_size, uint8_t rx_size);

#endif /* PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADMETERING_TRANSPORT_H_ */
/*------------------------------ End of File -------------------------------*/
