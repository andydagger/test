/*
==========================================================================
 Name        : ade9153aMetering.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices/ADE9153A/ade9153aMetering.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 6th May 2021
 Description :
==========================================================================
*/
/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_H_
#define PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_H_

#include "powerMonitor.h"
#include "stdint.h"
#include "stdbool.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

#define ADE9153A_UART_BAUD_RATE          4800
#define ADE9153A_UART_COMM_PARAMS        (UART_CFG_DATALEN_8 | UART_CFG_PARITY_ODD | UART_CFG_STOPLEN_1)

// Wait times are in 1s increments due to get_systick_sec()
// Page 24 of ade9153a tech doc states the energy accumulator will
// overflow after 13.3s with full-scale-inputs, obviously the inputs will never be
// full scale due to the lamps being used, so 15 seconds is a good compromise
#define ADE9153A_INSTANT_DATA_WAIT_TIME  15

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
typedef struct __Tag_ade9153a_AutoCalRegs
{
    float AICC;
    float AVCC;
    int32_t AcalAICCReg;
    int32_t AcalAICERTReg;
    int32_t AcalAVCCReg;
    int32_t AcalAVCERTReg;

} ade9153a_autoCalRegs;

typedef struct __tag_ade9153a_instantValues
{
    float voltage;
    float current;
    float power;
    float powerFactor;
    float energy;

} ade9153a_instantValues;

typedef struct __tag_ade9153a_rawValues
{
    int32_t currentRMS;
    int32_t voltageRMS;
    int32_t powerFactor;
    int32_t activePower;
    int32_t activeEnergy;

} ade9153a_rawValues;


/*****************************************************************************
 * Public Variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void ade9153aMetering_init( void);
void ade9153aMetering_initMeasurement( PowerCalcDataset * ptr_dataset);
void ade9153aMetering_uartISR( void);
powerMonitor_Result ade9153aMetering_detectHW( void);
powerMonitor_Result ade9153aMetering_service( void);
void ade9153aMetering_interruptPinISR( void);
void ade9153aMetering_processEvent( void);

#endif /* PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_H_ */
/*------------------------------- End of File --------------------------------*/
