/*
==========================================================================
 Name        : ade9153aMetering.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices/ade9153aMetering.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 3rd June 2021
 Description : analog devices ADE9153A metering solution
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "config_pinout.h"
#include "config_dataset.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "powerMonitor.h"
#include "ADE9153A.h"
#include "ade9153aMetering.h"
#include "ade9153aMetering_impl.h"
#include "ade9153aMetering_transport.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

uint32_t  ade9153a_last_sample_time;
bool      ade9153a_event;

/*****************************************************************************
 * Private Constants
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 *****************************************************************************/

/*******************************************************************************
 * @brief analog devices metering initialisation
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    void
 *
 ******************************************************************************/
void ade9153aMetering_init( void)
{
    ade9153a_last_sample_time = get_systick_sec();

    ade9153a_event = false;

    ade9153a_init();

    // get the comms ready
    ade9153a_transportInit();
}

/*******************************************************************************
 * @brief analog devices metering measurement initialisation
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] PowerCalcDataset *  - pointer to dataset
 *
 * @return    void
 *
 ******************************************************************************/
void ade9153aMetering_initMeasurement( PowerCalcDataset * pDataset)
{
    // pass pointer to dataset fetching
    ade9153a_initMeasurement( pDataset);
}


/*******************************************************************************
 * @brief analog devices hardware detection
 *        This is called from the power monitor abstraction layer to detect
 *        if the AD part is present.
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - result of the HW detection
 *
 *******************************************************************************/
powerMonitor_Result ade9153aMetering_detectHW( void)
{
    powerMonitor_Result result;

    result = ade9153a_hardware_detect();

	return result;
}

/*******************************************************************************
 * @brief analog devices metering service
 *        This is called from the vTaskSensorLfp every 100ms. The function
 *        contains the state machine and timings for receiving data from the
 *        analog devices power monitor and any averaging of data required.
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - result of the service
 *
 *******************************************************************************/
powerMonitor_Result ade9153aMetering_service( void)
{
	powerMonitor_Result result = POWER_MONITOR_RESULT_IDLE;
	uint32_t seconds;

    // have we had an event interrupt from the power monitoring?
	if( ade9153a_event)
	{
	    ade9153aMetering_processEvent();
	}

	seconds = get_systick_sec();

    // is it time to request the instant data?
    if(( seconds - ade9153a_last_sample_time) >= ADE9153A_INSTANT_DATA_WAIT_TIME)
    {
        ade9153a_last_sample_time = seconds;

        result = ade9153a_readAndProcessInstantData();
    }

	return result;
}

/*******************************************************************************
 * @brief analog devices uart isr - mapped to UART1.
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void ade9153aMetering_uartISR( void)
{
    ade9153a_uartISR();
}

/*******************************************************************************
 * @brief ade9153a interrupt pin isr
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer, the ADI part is configure to interrupt
 *        the processor on voltage surge and sag, over current etc
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void ade9153aMetering_interruptPinISR( void)
{
    ade9153a_event = true;
}

/*******************************************************************************
 * @brief processes the event by reading the status regs and INT status
 *        to reset the interrupt line and report the events
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void ade9153aMetering_processEvent( void)
{
    ade9153a_event = false;

    ade9153a_processEvent();
}


/*------------------------------ End of File -------------------------------*/
