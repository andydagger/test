/*
==========================================================================
 Name        : ade9153aMetering_calibStartStop.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices/ade9153aMetering_calibStartStop.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 11th June 2021
 Description : analog devices calibration start / stop functions
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "config_pinout.h"
#include "config_dataset.h"
#include "FreeRTOS.h"
#include "FreeRTOSCommonHooks.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "powerMonitor.h"
#include "ADE9153A.h"
#include "ade9153aMetering.h"
#include "ade9153aMetering_impl.h"
#include "ade9153aMetering_transport.h"
#include "ade9153aMetering_calibration.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private Constants
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 *****************************************************************************/

/*******************************************************************************
 * @brief Stops the auto calibration
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - Result of the stop calibration
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_stopCalibration( void)
{
    debug_printf( PWR, "ade9153a Stop Calibration\r\n");

    return ade9153a_writeRegister_32( ADE9153A_REG_MS_ACAL_CFG_32, 0x0);
}

/*******************************************************************************
 * @brief Starts the auto calibration on the respective channel
 *
 * @param[in] uint32_t - the calibration to start can be one of the following:
 *                       ADE9153A_RUN_AUTOCAL_AV, ADE9153A_RUN_AUTOCAL_BI_NORMAL, ADE9153A_RUN_AUTOCAL_AI_NORMAL
 *                       ADE9153A_RUN_AUTOCAL_BI_TURBO, ADE9153A_RUN_AUTOCAL_AI_TURBO
 *
 * @return    powerMonitor_Result - Result of the start calibration
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_startCalibration( uint32_t calToStart)
{
    powerMonitor_Result result;
    uint32_t status = 0;
    uint32_t last_ms_tick;

    // check the calibration to start is valid
    if(( calToStart != ADE9153A_RUN_AUTOCAL_AV) &&
       ( calToStart != ADE9153A_RUN_AUTOCAL_BI_NORMAL) &&
       ( calToStart != ADE9153A_RUN_AUTOCAL_AI_NORMAL) &&
       ( calToStart != ADE9153A_RUN_AUTOCAL_BI_TURBO) &&
       ( calToStart != ADE9153A_RUN_AUTOCAL_AI_TURBO))
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    // do the initial read to see if the mSure is ready
    result = ade9153a_readRegister_32(ADE9153A_REG_MS_STATUS_CURRENT_32, &status);

    if (result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    last_ms_tick = get_systick_ms();

    // wait the mSure Cal system to be ready
    while ((status & ADE9153A_MSURE_SYSRDYP) == 0)
    {
        FreeRTOSDelay(ADE9153A_MSURE_WAIT_DELAY_MS);

        if ((get_systick_ms() - last_ms_tick) > ADE9153A_MSURE_READY_TIMEOUT)
        {
            debug_printf(PWR, "ade9153a Timeout waiting mSure Ready\r\n");

            result = POWER_MONITOR_RESULT_TIMEOUT;
            break;
        }

        //  Read mSure system ready bit
        result = ade9153a_readRegister_32( ADE9153A_REG_MS_STATUS_CURRENT_32, &status);

        if( result != POWER_MONITOR_RESULT_OK)
        {
            break;
        }
    }
 
    if( result == POWER_MONITOR_RESULT_OK)
    {
        result = ade9153a_writeRegister_32( ADE9153A_REG_MS_ACAL_CFG_32, calToStart);
    }

    return result;
}

/*------------------------------ End of File -------------------------------*/

