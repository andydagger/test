/*
==========================================================================
 Name        : ade9153aMetering_impl.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices/ade9153aMetering_impl.c
 Author      : jeff barker
 Copyright   : Lucy Zodion Ltd
 Created     : 21 May 2021
 Description : analog devices metering implementation layer
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "math.h"
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "config_pinout.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "config_dataset.h"
#include "sysmon.h"
#include "FreeRTOS.h"
#include "FreeRTOSCommonHooks.h"
#include "powerMonitor.h"
#include "CRC16.h"
#include "ADE9153A.h"
#include "ade9153aMetering.h"
#include "ade9153aMetering_transport.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

// UART tx/rx buffers
static volatile uint8_t  ad_rx_buffer[ ADE9153A_RX_BUFF_SIZE];
static volatile uint8_t  ad_tx_buffer[ ADE9153A_TX_BUFF_SIZE];

// ad comms variables
static volatile uint8_t  ad_comms_rx_size;
static volatile uint8_t  ad_comms_tx_size;
static volatile uint8_t  ad_comms_rx_index;
static volatile uint8_t  ad_comms_tx_index;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/


/*******************************************************************************
 * @brief initialises the transport variables
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void ade9153a_transportInit( void)
{
    ad_comms_tx_index = 0;
    ad_comms_rx_index = 0;
    ad_comms_rx_size  = 0;
    ad_comms_tx_size  = 0;
    // Setup the UART for the AD device 4800, 8bits ,odd, 1 stop bit
    hw_setup_power_monitor_uart_params( ADE9153A_UART_BAUD_RATE, ADE9153A_UART_COMM_PARAMS);

    hw_power_monitor_uart_irq_enable();
}

/*******************************************************************************
 * @brief analog devices metering UART ISR
 *        This function is only referenced from powerMonitor.c as part
 *        of the abstraction layer
 *
 * @param[in] void
 *
 * @return    void
 *
  ******************************************************************************/
void ade9153a_uartISR( void)
{
    uint32_t intsrc;
    uint8_t  readbyte;

    intsrc = hw_power_monitor_uart_get_int_status();

    // received a byte ??
    if( intsrc & UART_STAT_RXRDY)
    {
        // always read the byte so it flushes the buffer
        readbyte = hw_power_monitor_uart_rx_byte();

        if( ad_comms_rx_index < ad_comms_rx_size)
        {
            ad_rx_buffer[ ad_comms_rx_index++] = readbyte;
        }
    }
    // Is the TXD ready to send another byte??
    else if( intsrc & UART_STAT_TXRDY)
    {
        // tx bytes
        if( ad_comms_tx_index < ad_comms_tx_size)
        {
            hw_power_monitor_uart_tx_byte( ad_tx_buffer[ ad_comms_tx_index]);

            // last byte sent then disable tx interrupt
            if( ++ad_comms_tx_index >= ad_comms_tx_size)
            {
                hw_power_monitor_uart_tx_disable();
            }
        }
    }
}

/*******************************************************************************
 * @brief analog devices read 16 bit register
 *
 * @param[in] uint16_t  - address of the register
 *            uint16_t* - pointer to 16 bit variable to receive the read data
 *
 * @return    powerMonitor_Result - result of the read process
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_readRegister_16( uint16_t address, uint16_t * dataPtr)
{
    powerMonitor_Result result;
    uint16_t command;
    uint16_t value;
    uint16_t msgCRC;
    uint16_t CRC;

    // validate pointer
    if( dataPtr == NULL)
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    // shift address to align with cmd packet
    command = (((address << 4) & 0xFFF0) | ADE9153A_CMD_CHECKSUM | ADE9153A_CMD_READ);

    ad_tx_buffer[ ADE9153A_TX_IDX_CMD0] = (uint8_t)(command >> 8);
    ad_tx_buffer[ ADE9153A_TX_IDX_CMD1] = (uint8_t)(command);

    result = ade9153a_sendMessage( ADE9153A_READ_TX_SIZE, ADE9153A_READ_RX_SIZE_16);

    if( result == POWER_MONITOR_RESULT_OK)
    {
        CRC = CRC16_calculate( ADE9153A_CRC16_SEED,
                               (uint8_t*)&ad_rx_buffer[ ADE9153A_RX_IDX_DATA0],
                               sizeof(uint16_t));

        msgCRC = (uint16_t)((uint16_t)ad_rx_buffer[ ADE9153A_RX_IDX_CRC_MSB_16] << 8) |
                           ((uint16_t)ad_rx_buffer[ ADE9153A_RX_IDX_CRC_LSB_16]);

        if( CRC == msgCRC)
        {
            value = (uint16_t)((uint16_t)ad_rx_buffer[ ADE9153A_RX_IDX_DATA0] << 8) |
                              ((uint16_t)ad_rx_buffer[ ADE9153A_RX_IDX_DATA1]);

            *dataPtr = value;
        }
        else
        {
            debug_printf( PWR, "ade9153a read 16bit reg CRC error\r\n");

            result = POWER_MONITOR_RESULT_DATA_ERROR;
        }
    }

    return result;
}

/*******************************************************************************
 * @brief analog devices read 32 bit register
 *
 * @param[in] uint16_t  - address of the register
 *            uint32_t* - pointer to 32 bit variable to receive the read data
 *
 * @return    powerMonitor_Result - result of the read process
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_readRegister_32( uint16_t address, uint32_t * dataPtr)
{
    powerMonitor_Result result;
    uint16_t command;
    uint32_t value;
    uint16_t msgCRC;
    uint16_t CRC;

    // validate pointer
    if( dataPtr == NULL)
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    // shift address to align with cmd packet
    command = (((address << 4) & 0xFFF0) | ADE9153A_CMD_CHECKSUM | ADE9153A_CMD_READ);

    ad_tx_buffer[ ADE9153A_TX_IDX_CMD0] = (uint8_t)(command >> 8);
    ad_tx_buffer[ ADE9153A_TX_IDX_CMD1] = (uint8_t)(command);

    result = ade9153a_sendMessage( ADE9153A_READ_TX_SIZE, ADE9153A_READ_RX_SIZE_32);

    if( result == POWER_MONITOR_RESULT_OK)
    {
        CRC = CRC16_calculate( ADE9153A_CRC16_SEED,
                               (uint8_t*)&ad_rx_buffer[ ADE9153A_RX_IDX_DATA0],
                               sizeof(uint32_t));

        msgCRC = (uint16_t)((uint16_t)ad_rx_buffer[ ADE9153A_RX_IDX_CRC_MSB_32] << 8) |
                           ((uint16_t)ad_rx_buffer[ ADE9153A_RX_IDX_CRC_LSB_32]);

        if( CRC == msgCRC)
        {
            value = (uint32_t)((uint32_t)ad_rx_buffer[ ADE9153A_RX_IDX_DATA0] << 24) |
                              ((uint32_t)ad_rx_buffer[ ADE9153A_RX_IDX_DATA1] << 16) |
                              ((uint32_t)ad_rx_buffer[ ADE9153A_RX_IDX_DATA2] << 8)  |
                              ((uint32_t)ad_rx_buffer[ ADE9153A_RX_IDX_DATA3]);

            *dataPtr = value;
        }
        else
        {
            debug_printf( PWR, "ade9153a read 32bit reg CRC error\r\n");

            result = POWER_MONITOR_RESULT_DATA_ERROR;
        }
    }

    return result;
}

/*******************************************************************************
 * @brief analog devices read 16 bit register
 *
 * @param[in] uint16_t - address of the register
 *            uint16_t - 16 bit variable to send to the device
 *
 * @return    powerMonitor_Result - result of the read process
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_writeRegister_16( uint16_t address, uint16_t value)
{
    powerMonitor_Result result;
    uint16_t command;
    uint16_t CRC;

    // shift address to align with cmd packet
    command = (((address << 4) & 0xFFF0) | ADE9153A_CMD_CHECKSUM);

    ad_tx_buffer[ ADE9153A_TX_IDX_CMD0]   = (uint8_t)(command >> 8);
    ad_tx_buffer[ ADE9153A_TX_IDX_CMD1]   = (uint8_t)(command);
    ad_tx_buffer[ ADE9153A_TX_IDX_DATA0]  = (uint8_t)(value >> 8);
    ad_tx_buffer[ ADE9153A_TX_IDX_DATA1]  = (uint8_t)(value);

    CRC = CRC16_calculate( ADE9153A_CRC16_SEED,
                           (uint8_t*)&ad_tx_buffer[ ADE9153A_TX_IDX_DATA0],
                           sizeof(uint16_t));

    ad_tx_buffer[ ADE9153A_TX_IDX_CRC_MSB_16] = (uint8_t)(CRC >> 8);
    ad_tx_buffer[ ADE9153A_TX_IDX_CRC_LSB_16] = (uint8_t)(CRC);

    result = ade9153a_sendMessage( ADE9153A_WRITE_TX_SIZE_16, ADE9153A_READ_RX_SIZE_NONE);

    return result;
}

/*******************************************************************************
 * @brief analog devices read 32 bit register
 *
 * @param[in] uint16_t - address of the register
 *            uint32_t - 32 bit variable to send to the device
 *
 * @return    powerMonitor_Result - result of the read process
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_writeRegister_32( uint16_t address, uint32_t value)
{
    powerMonitor_Result result;
    uint16_t command;
    uint16_t CRC;

    // shift address to align with cmd packet
    command = (((address << 4) & 0xFFF0) | ADE9153A_CMD_CHECKSUM);

    ad_tx_buffer[ ADE9153A_TX_IDX_CMD0]   = (uint8_t)(command >> 8);
    ad_tx_buffer[ ADE9153A_TX_IDX_CMD1]   = (uint8_t)(command);
    ad_tx_buffer[ ADE9153A_TX_IDX_DATA0]  = (uint8_t)(value >> 24);
    ad_tx_buffer[ ADE9153A_TX_IDX_DATA1]  = (uint8_t)(value >> 16);
    ad_tx_buffer[ ADE9153A_TX_IDX_DATA2]  = (uint8_t)(value >> 8);
    ad_tx_buffer[ ADE9153A_TX_IDX_DATA3]  = (uint8_t)(value);

    CRC = CRC16_calculate( ADE9153A_CRC16_SEED,
                           (uint8_t*)&ad_tx_buffer[ ADE9153A_TX_IDX_DATA0],
                           sizeof(uint32_t));

    ad_tx_buffer[ ADE9153A_TX_IDX_CRC_MSB_32] = (uint8_t)(CRC >> 8);
    ad_tx_buffer[ ADE9153A_TX_IDX_CRC_LSB_32] = (uint8_t)(CRC);

    result = ade9153a_sendMessage( ADE9153A_WRITE_TX_SIZE_32, ADE9153A_READ_RX_SIZE_NONE);

    return result;
}

/*******************************************************************************
 * @brief sends message to device and receives answer when a register read
 *
 * @param[in] uint8_t - tx message size
 *            uint8_t - rx message size
 *
 * @return    powerMonitor_Result - result of the read/write
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_sendMessage( uint8_t tx_size, uint8_t rx_size)
{
    powerMonitor_Result result = POWER_MONITOR_RESULT_OK;
    uint32_t last_ms_tick;

    ad_comms_tx_index = 0;
    ad_comms_rx_index = 0;

    ad_comms_tx_size  = tx_size;
    ad_comms_rx_size  = rx_size;

    // enable the tx of the bytes
    hw_power_monitor_uart_tx_enable();

    last_ms_tick = get_systick_ms();

    do
    {
        // let other tasks run while waiting for the message to complete.
        FreeRTOSDelay( ADE9153A_SERIAL_MSG_DELAY_MS);

        // timed out??
        if(( get_systick_ms() - last_ms_tick) > ADE9153A_SERIAL_MSG_TIMEOUT_MS)
        {
            debug_printf( PWR, "ade9153a send message timeout\r\n");

            result = POWER_MONITOR_RESULT_TIMEOUT;
            break;
        }
    }
    while(( ad_comms_tx_index < ad_comms_tx_size) &&
          ( ad_comms_rx_index < ad_comms_rx_size));

    return result;
}

/*------------------------------ End of File -------------------------------*/
