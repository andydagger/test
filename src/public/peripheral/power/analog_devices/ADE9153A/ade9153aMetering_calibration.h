/*
==========================================================================
 Name        : ade9153aMetering_calibration.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices_ade9153aMetering_calibration.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 17th May 2021
 Description :
==========================================================================
*/
/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_CALIBRATION_H_
#define PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_CALIBRATION_H_

#include "stdint.h"
#include "stdbool.h"
#include "powerMonitor.h"
#include "ade9153aMetering.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
// 1.1 seconds wait for MSure system to be ready
#define ADE9153A_MSURE_READY_TIMEOUT    1100
// freertos delay between sampling mSure status bit
#define ADE9153A_MSURE_WAIT_DELAY_MS    50

// current calibration time 12 seconds Gives 0.5% accuracy
#define ADE9153A_MSURE_AI_CAL_DELAY_MS  12000
// voltage calibration time 6 seconds
#define ADE9153A_MSURE_AV_CAL_DELAY_MS  6000

// this is the number readings required after a cal.
#define ADE9153A_CAL_NO_OF_READINGS     8
// this the delay between polling the device registers
#define ADE9153A_CAL_POLL_DELAY_MS      2000

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public Variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
powerMonitor_Result ade9153aMetering_calibrate( PowerCalcDataset * dataset_ptr);


/*------------------------------- End of File --------------------------------*/
#endif /* PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153AMETERING_CALIBRATION_H_ */
