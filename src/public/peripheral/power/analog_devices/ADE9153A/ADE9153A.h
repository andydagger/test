/*
==========================================================================
 Name        : ADE9153A.h
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices/ADE9153A.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 6th May 2021
 Description : C header file contains macros for Registers' address relative to instances and plain bit-fields
==========================================================================
*/
/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153A_H_
#define PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153A_H_


#define ADE9153A_REG_AIGAIN_32                    0x0000    /* Phase A current gain adjust. */
#define ADE9153A_REG_APHASECAL_32                 0x0001    /* Phase A phase correction factor. */
#define ADE9153A_REG_AVGAIN_32                    0x0002    /* Phase A voltage gain adjust. */
#define ADE9153A_REG_AIRMS_OS_32                  0x0003    /* Phase A current rms offset for filter-based AIRMS calculation. */
#define ADE9153A_REG_AVRMS_OS_32                  0x0004    /* Phase A voltage rms offset for filter-based AVRMS calculation. */
#define ADE9153A_REG_APGAIN_32                    0x0005    /* Phase A power gain adjust for AWATT, AVA, and AFVAR calculations. */
#define ADE9153A_REG_AWATT_OS_32                  0x0006    /* Phase A total active power offset correction for AWATT calculation. */
#define ADE9153A_REG_AFVAR_OS_32                  0x0007    /* Phase A fundamental reactive power offset correction for AFVAR calculation. */
#define ADE9153A_REG_AVRMS_OC_OS_32               0x0008    /* Phase A voltage rms offset for fast rms, AVRMS_OC calculation. */
#define ADE9153A_REG_AIRMS_OC_OS_32               0x0009    /* Phase A current rms offset for fast rms, AIRMS_OC calculation. */
#define ADE9153A_REG_BIGAIN_32                    0x0010    /* Phase B current gain adjust. */
#define ADE9153A_REG_BIRMS_OS_32                  0x0013    /* Phase B current rms offset for filter-based BIRMS calculation. */
#define ADE9153A_REG_BIRMS_OC_OS_32               0x0019    /* Phase B current rms offset for fast rms, BIRMS_OC calculation. */
#define ADE9153A_REG_CONFIG0_32                   0x0020    /* DSP configuration register. */
#define ADE9153A_REG_VNOM_32                      0x0021    /* Nominal phase voltage rms used in the calculation of apparent power, AVA, when the VNOMA_EN bit is set in the CONFIG0 register. */
#define ADE9153A_REG_DICOEFF_32                   0x0022    /* Value used in the digital integrator algorithm. If the integrator is turned on, with INTEN_BI equal to 1 in the CONFIG0 register, it is recommended to leave this register at the default value. */
#define ADE9153A_REG_BI_PGAGAIN_32                0x0023    /* PGA gain for Current Channel B ADC. */
#define ADE9153A_REG_MS_ACAL_CFG_32               0x0030    /* MSure autocalibration configuration register. */
#define ADE9153A_REG_CT_PHASE_DELAY_32            0x0049    /* Phase delay of the CT used on Current Channel B. This register is in 5.27 format and expressed in degrees. */
#define ADE9153A_REG_CT_CORNER_32                 0x004A    /* Corner frequency of the CT. This value is calculated from the CT_PHASE_DELAY value. */
#define ADE9153A_REG_VDIV_RSMALL_32               0x004C    /* This register holds the resistance value, in Ω, of the small resistor in the resistor divider. */
#define ADE9153A_REG_AI_WAV_32                    0x0200    /* Instantaneous Current Channel A waveform processed by the DSP, at 4kSPS. */
#define ADE9153A_REG_AV_WAV_32                    0x0201    /* Instantaneous Voltage Channel waveform processed by the DSP, at 4kSPS. */
#define ADE9153A_REG_AIRMS_32                     0x0202    /* Phase A filter-based current rms value updated at 4kSPS. */
#define ADE9153A_REG_AVRMS_32                     0x0203    /* Phase A filter-based voltage rms value updated at 4kSPS. */
#define ADE9153A_REG_AWATT_32                     0x0204    /* Phase A low-pass filtered total active power updated at 4kSPS. */
#define ADE9153A_REG_AVA_32                       0x0206    /* Phase A total apparent power updated at 4kSPS. */
#define ADE9153A_REG_AFVAR_32                     0x0207    /* Phase A fundamental reactive power updated at 4kSPS. */
#define ADE9153A_REG_APF_32                       0x0208    /* Phase A power factor updated at 1.024 sec. */
#define ADE9153A_REG_AIRMS_OC_32                  0x0209    /* Phase A current fast rms calculation; one cycle rms updated every half cycle. */
#define ADE9153A_REG_AVRMS_OC_32                  0x020A    /* Phase A voltage fast rms calculation; one cycle rms updated every half cycle. */
#define ADE9153A_REG_BI_WAV_32                    0x0210    /* Instantaneous Phase B Current Channel waveform processed by the DSP at 4kSPS. */
#define ADE9153A_REG_BIRMS_32                     0x0212    /* Phase B filter-based current rms value updated at 4kSPS. */
#define ADE9153A_REG_BIRMS_OC_32                  0x0219    /* Phase B Current fast rms calculation; one cycle rms updated every half cycle. */
#define ADE9153A_REG_MS_ACAL_AICC_32              0x0220    /* Current Channel A mSure CC estimation from autocalibration. */
#define ADE9153A_REG_MS_ACAL_AICERT_32            0x0221    /* Current Channel A mSure certainty of autocalibration. */
#define ADE9153A_REG_MS_ACAL_BICC_32              0x0222    /* Current Channel B mSure CC estimation from autocalibration. */
#define ADE9153A_REG_MS_ACAL_BICERT_32            0x0223    /* Current Channel B mSure certainty of autocalibration. */
#define ADE9153A_REG_MS_ACAL_AVCC_32              0x0224    /* Voltage Channel mSure CC estimation from autocalibration. */
#define ADE9153A_REG_MS_ACAL_AVCERT_32            0x0225    /* Voltage Channel mSure certainty of autocalibration. */
#define ADE9153A_REG_MS_STATUS_CURRENT_32         0x0240    /* The MS_STATUS_CURRENT register contains bits that reflect the present state of the mSure system. */
#define ADE9153A_REG_VERSION_DSP_32               0x0241    /* This register indicates the version of the ADE9153A DSP after the user writes RUN=1 to start measurements. */
#define ADE9153A_REG_VERSION_PRODUCT_32           0x0242    /* This register indicates the version of the product being used. */
#define ADE9153A_REG_AWATT_ACC_32                 0x039D    /* Phase A accumulated total active power; updated after PWR_TIME 4kSPS samples. */
#define ADE9153A_REG_AWATTHR_LO_32                0x039E    /* Phase A accumulated total active energy, least significant bits (LSBs). Updated according to the settings in the EP_CFG and EGY_TIME registers. */
#define ADE9153A_REG_AWATTHR_HI_32                0x039F    /* Phase A accumulated total active energy, most significant bits (MSBs). Updated according to the settings in the EP_CFG and EGY_TIME registers. */
#define ADE9153A_REG_AVA_ACC_32                   0x03B1    /* Phase A accumulated total apparent power; updated after PWR_TIME 4kSPS samples. */
#define ADE9153A_REG_AVAHR_LO_32                  0x03B2    /* Phase A accumulated total apparent energy, LSBs. Updated according to the settings in the EP_CFG and EGY_TIME registers. */
#define ADE9153A_REG_AVAHR_HI_32                  0x03B3    /* Phase A accumulated total apparent energy, MSBs. Updated according to the settings in the EP_CFG and EGY_TIME registers. */
#define ADE9153A_REG_AFVAR_ACC_32                 0x03BB    /* Phase A accumulated fundamental reactive power; updated after PWR_TIME 4kSPS samples. */
#define ADE9153A_REG_AFVARHR_LO_32                0x03BC    /* Phase A accumulated fundamental reactive energy, LSBs. Updated according to the settings in the EP_CFG and EGY_TIME registers. */
#define ADE9153A_REG_AFVARHR_HI_32                0x03BD    /* Phase A accumulated fundamental reactive energy, MSBs. Updated according to the settings in the EP_CFG and EGY_TIME registers. */
#define ADE9153A_REG_PWATT_ACC_32                 0x03EB    /* Accumulated positive total active power from the AWATT register; updated after PWR_TIME 4 kSPS samples. */
#define ADE9153A_REG_NWATT_ACC_32                 0x03EF    /* Accumulated negative total active power from the AWATT register; updated after PWR_TIME 4 kSPS samples. */
#define ADE9153A_REG_PFVAR_ACC_32                 0x03F3    /* Accumulated positive fundamental reactive power from the AFVAR register; updated after PWR_TIME 4 kSPS samples. */
#define ADE9153A_REG_NFVAR_ACC_32                 0x03F7    /* Accumulated negative fundamental reactive power from the AFVAR register, updated after PWR_TIME 4 kSPS samples. */
#define ADE9153A_REG_IPEAK_32                     0x0400    /* Current peak register. */
#define ADE9153A_REG_VPEAK_32                     0x0401    /* Voltage peak register. */
#define ADE9153A_REG_STATUS_32                    0x0402    /* Tier 1 interrupt status register. */
#define ADE9153A_REG_MASK_32                      0x0405    /* Tier 1 interrupt enable register. */
#define ADE9153A_REG_OI_LVL_32                    0x0409    /* Overcurrent RMS_OC detection threshold level. */
#define ADE9153A_REG_OIA_32                       0x040A    /* Phase A overcurrent RMS_OC value. If overcurrent detection on this channel is enabled with OIA_EN in the CONFIG3 register and AIRMS_OC is greater than the OILVL threshold, this value is updated. */
#define ADE9153A_REG_OIB_32                       0x040B    /* Phase B overcurrent RMS_OC value. See the OIA description. */
#define ADE9153A_REG_USER_PERIOD_32               0x040E    /* User configured line period value used for RMS_OC when the UPERIOD_SEL bit in the CONFIG2 register is set. */
#define ADE9153A_REG_VLEVEL_32                    0x040F    /* Register used in the algorithm that computes the fundamental reactive power. */
#define ADE9153A_REG_DIP_LVL_32                   0x0410    /* Voltage RMS_OC dip detection threshold level. */
#define ADE9153A_REG_DIPA_32                      0x0411    /* Phase A voltage RMS_OC value during a dip condition. */
#define ADE9153A_REG_SWELL_LVL_32                 0x0414    /* Voltage RMS_OC swell detection threshold level. */
#define ADE9153A_REG_SWELLA_32                    0x0415    /* Phase A voltage RMS_OC value during a swell condition. */
#define ADE9153A_REG_APERIOD_32                   0x0418    /* Line period on the Phase A voltage. */
#define ADE9153A_REG_ACT_NL_LVL_32                0x041C    /* No load threshold in the total active power datapath. */
#define ADE9153A_REG_REACT_NL_LVL_32              0x041D    /* No load threshold in the fundamental reactive power datapath. */
#define ADE9153A_REG_APP_NL_LVL_32                0x041E    /* No load threshold in the total apparent power datapath. */
#define ADE9153A_REG_PHNOLOAD_32                  0x041F    /* Phase no load register. */
#define ADE9153A_REG_WTHR_32                      0x0420    /* Sets the maximum output rate from the digital to frequency converter of the total active power for the CF calibration pulse output. It is recommended to leave this at WTHR = 0x00100000. */
#define ADE9153A_REG_VARTHR_32                    0x0421    /* See WTHR. It is recommended to leave this value at VARTHR = 0x00100000. */
#define ADE9153A_REG_VATHR_32                     0x0422    /* See WTHR. It is recommended to leave this value at VATHR = 0x00100000. */
#define ADE9153A_REG_LAST_DATA_32                 0x0423    /* This register holds the data read or written during the last 32-bit transaction on the SPI port. */
#define ADE9153A_REG_CF_LCFG_32                   0x0425    /* CF calibration pulse width configuration register. */
#define ADE9153A_REG_TEMP_TRIM_32                 0x0471    /* Temperature sensor gain and offset, calculated during the manufacturing process. */
#define ADE9153A_REG_CHIP_ID_HI_32                0x0472    /* Chip identification, 32 MSBs. */
#define ADE9153A_REG_CHIP_ID_LO_32                0x0473    /* Chip identification, 32 LSBs. */
/* 16-bit below */
#define ADE9153A_REG_RUN_16                       0x0480    /* Write this register to 1 to start the measurements. */
#define ADE9153A_REG_CONFIG1_16                   0x0481    /* Configuration Register 1. */
#define ADE9153A_REG_ANGL_AV_AI_16                0x0485    /* Time between positive to negative zero crossings on Phase A voltage and current. */
#define ADE9153A_REG_ANGL_AI_BI_16                0x0488    /* Time between positive to negative zero crossings on Phase A and Phase B currents. */
#define ADE9153A_REG_DIP_CYC_16                   0x048B    /* Voltage RMS_OC dip detection cycle configuration. */
#define ADE9153A_REG_SWELL_CYC_16                 0x048C    /* Voltage RMS_OC swell detection cycle configuration. */
#define ADE9153A_REG_CFMODE_16                    0x0490    /* CFx configuration register. */
#define ADE9153A_REG_COMPMODE_16                  0x0491    /* Computation mode register. Set this register to 0x0005. */
#define ADE9153A_REG_ACCMODE_16                   0x0492    /* Accumulation mode register. */
#define ADE9153A_REG_CONFIG3_16                   0x0493    /* Configuration Register 3 for configuration of power quality settings. */
#define ADE9153A_REG_CF1DEN_16                    0x0494    /* CF1 denominator register. */
#define ADE9153A_REG_CF2DEN_16                    0x0495    /* CF2 denominator register. */
#define ADE9153A_REG_ZXTOUT_16                    0x0498    /* Zero-crossing timeout configuration register. */
#define ADE9153A_REG_ZXTHRSH_16                   0x0499    /* Voltage channel zero-crossing threshold register. */
#define ADE9153A_REG_ZX_CFG_16                    0x049A    /* Zero-crossing detection configuration register. */
#define ADE9153A_REG_PHSIGN_16                    0x049D    /* Power sign register. */
#define ADE9153A_REG_CRC_RSLT_16                  0x04A8    /* This register holds the CRC of the configuration registers. */
#define ADE9153A_REG_CRC_SPI_16                   0x04A9    /* The register holds the 16-bit CRC of the data sent out on the MOSI/RX pin during the last SPI register read. */
#define ADE9153A_REG_LAST_DATA_16                 0x04AC    /* This register holds the data read or written during the last 16-bit transaction on the SPI port. When using UART, this register holds the lower 16 bits of the last data read or write. */
#define ADE9153A_REG_LAST_CMD_16                  0x04AE    /* This register holds the address and the read/write operation request (CMD_HDR) for the last transaction on the SPI port. */
#define ADE9153A_REG_CONFIG2_16                   0x04AF    /* Configuration Register 2. This register controls the high-pass filter (HPF) corner and the user period selection. */
#define ADE9153A_REG_EP_CFG_16                    0x04B0    /* Energy and power accumulation configuration. */
#define ADE9153A_REG_PWR_TIME_16                  0x04B1    /* Power update time configuration. */
#define ADE9153A_REG_EGY_TIME_16                  0x04B2    /* Energy accumulation update time configuration. */
#define ADE9153A_REG_CRC_FORCE_16                 0x04B4    /* This register forces an update of the CRC of configuration registers. */
#define ADE9153A_REG_TEMP_CFG_16                  0x04B6    /* Temperature sensor configuration register. */
#define ADE9153A_REG_TEMP_RSLT_16                 0x04B7    /* Temperature measurement result. */
#define ADE9153A_REG_AI_PGAGAIN_16                0x04B9    /* This register configures the PGA gain for Current Channel A. */
#define ADE9153A_REG_WR_LOCK_16                   0x04BF    /* This register enables the configuration lock feature. */
#define ADE9153A_REG_MS_STATUS_IRQ_16             0x04C0    /* Tier 2 status register for the autocalibration and monitoring mSure system related interrupts. Any bit set in this register causes the corresponding bit in the status register to be set. This register is cleared on a read and all bits are reset. If a new status bit arrives on the same clock on which the read occurs, the new status bit remains set; in this way, no status bit is missed. */
#define ADE9153A_REG_EVENT_STATUS_16              0x04C1    /* Tier 2 status register for power quality event related interrupts. See the MS_STATUS_IRQ description. */
#define ADE9153A_REG_CHIP_STATUS_16               0x04C2    /* Tier 2 status register for chip error related interrupts. See the MS_STATUS_IRQ description. */
#define ADE9153A_REG_UART_BAUD_SWITCH_16          0x04DC    /* This register switches the UART Baud rate between 4800 and 115,200 Baud. Writing a value of 0x0052 sets the Baud rate to 115,200 Baud; any other value maintains a Baud rate of 4800. */
#define ADE9153A_REG_VERSION_16                   0x04FE    /* Version of the ADE9153 IC. */

#define ADE9153A_REG_AI_WAV_1                     0x0600    /* SPI burst read accessible registers organized functionally. See AI_WAV. */
#define ADE9153A_REG_AV_WAV_1                     0x0601    /* SPI burst read accessible registers organized functionally. See AV_WAV. */
#define ADE9153A_REG_BI_WAV_1                     0x0602    /* SPI burst read accessible registers organized functionally. See BI_WAV. */
#define ADE9153A_REG_AIRMS_1                      0x0604    /* SPI burst read accessible registers organized functionally. See AIRMS. */
#define ADE9153A_REG_BIRMS_1                      0x0605    /* SPI burst read accessible registers organized functionally. See BIRMS. */
#define ADE9153A_REG_AVRMS_1                      0x0606    /* SPI burst read accessible registers organized functionally. See AVRMS. */
#define ADE9153A_REG_AWATT_1                      0x0608    /* SPI burst read accessible registers organized functionally. See AWATT. */
#define ADE9153A_REG_AFVAR_1                      0x060A    /* SPI burst read accessible registers organized functionally. See AFVAR. */
#define ADE9153A_REG_AVA_1                        0x060C    /* SPI burst read accessible registers organized functionally. See AVA. */
#define ADE9153A_REG_APF_1                        0x060E    /* SPI burst read accessible registers organized functionally. See APF. */
#define ADE9153A_REG_AI_WAV_2                     0x0610    /* SPI burst read accessible registers organized by phase. See AI_WAV. */
#define ADE9153A_REG_AV_WAV_2                     0x0611    /* SPI burst read accessible registers organized by phase. See AV_WAV. */
#define ADE9153A_REG_AIRMS_2                      0x0612    /* SPI burst read accessible registers organized by phase. See AIRMS. */
#define ADE9153A_REG_AVRMS_2                      0x0613    /* SPI burst read accessible registers organized by phase. See AVRMS. */
#define ADE9153A_REG_AWATT_2                      0x0614    /* SPI burst read accessible registers organized by phase. See AWATT. */
#define ADE9153A_REG_AVA_2                        0x0615    /* SPI burst read accessible registers organized by phase. See AVA. */
#define ADE9153A_REG_AFVAR_2                      0x0616    /* SPI burst read accessible registers organized by phase. See AFVAR. */
#define ADE9153A_REG_APF_2                        0x0617    /* SPI burst read accessible registers organized by phase. See APF. */
#define ADE9153A_REG_BI_WAV_2                     0x0618    /* SPI burst read accessible registers organized by phase. See BI_WAV. */
#define ADE9153A_REG_BIRMS_2                      0x061A    /* SPI burst read accessible registers organized by phase. See BIRMS. */

// register param and bit defines
#define ADE9153A_MSURE_SYSRDYP                    0x00000001 /* mSure system ready to calibrate status bit */

// Auto cal config register values
#define ADE9153A_MSURE_AUTOCAL_AV                 0x00000040 /* Enable autocal on the voltage channel */
#define ADE9153A_MSURE_AUTOCAL_BI                 0x00000020 /* enable autocal on the current channel B */
#define ADE9153A_MSURE_AUTOCAL_AI                 0x00000010 /* enable autocal on the current channel A */
#define ADE9153A_MSURE_ACALMODE_BI_TURBO          0x00000008 /* enable autocal turbo mode on channel B */
#define ADE9153A_MSURE_ACALMODE_AI_TURBO          0x00000004 /* enable autocal turbo mode on channel A */
#define ADE9153A_MSURE_ACAL_RUN                   0x00000002 /* runs autocal as specified in bits 2 to 6 */
#define ADE9153A_MSURE_ACAL_MODE                  0x00000001 /* nit must be set when running auto-cal */

#define ADE9153A_RUN_AUTOCAL_AV                   (ADE9153A_MSURE_AUTOCAL_AV|ADE9153A_MSURE_ACAL_RUN|ADE9153A_MSURE_ACAL_MODE)
#define ADE9153A_RUN_AUTOCAL_BI_NORMAL            (ADE9153A_MSURE_AUTOCAL_BI|ADE9153A_MSURE_ACAL_RUN|ADE9153A_MSURE_ACAL_MODE)
#define ADE9153A_RUN_AUTOCAL_AI_NORMAL            (ADE9153A_MSURE_AUTOCAL_AI|ADE9153A_MSURE_ACAL_RUN|ADE9153A_MSURE_ACAL_MODE)
#define ADE9153A_RUN_AUTOCAL_BI_TURBO             (ADE9153A_MSURE_AUTOCAL_BI|ADE9153A_MSURE_ACALMODE_BI_TURBO|ADE9153A_MSURE_ACAL_RUN|ADE9153A_MSURE_ACAL_MODE)
#define ADE9153A_RUN_AUTOCAL_AI_TURBO             (ADE9153A_MSURE_AUTOCAL_AI|ADE9153A_MSURE_ACALMODE_AI_TURBO|ADE9153A_MSURE_ACAL_RUN|ADE9153A_MSURE_ACAL_MODE)

// Status register bit masks
#define ADE9153A_STATUS_CHIP_STAT_MASK            0x80000000
#define ADE9153A_STATUS_EVENT_STAT_MASK           0x40000000
#define ADE9153A_STATUS_MSURE_STAT_MASK           0x20000000
#define ADE9153A_STATUS_FVARNL_MASK               0x00008000
#define ADE9153A_STATUS_VANNL_MASK                0x00004000
#define ADE9153A_STATUS_WATTNL_MASK               0x00002000

#define ADE9153A_STATUS_TIER_2_MASK               (ADE9153A_STATUS_CHIP_STAT_MASK  | \
                                                   ADE9153A_STATUS_EVENT_STAT_MASK | \
                                                   ADE9153A_STATUS_MSURE_STAT_MASK)

#define ADE9153A_STATUS_TIER_1_MASK               (uint32_t)(!ADE9153A_STATUS_TIER_2_MASK)

// Mask register bit masks
#define ADE9153A_MASK_CHIP_STAT_MASK              0x80000000
#define ADE9153A_MASK_EVENT_STAT_MASK             0x40000000
#define ADE9153A_MASK_MSURE_STAT_MASK             0x20000000
#define ADE9153A_MASK_FVARNL_MASK                 0x00008000
#define ADE9153A_MASK_VANNL_MASK                  0x00004000
#define ADE9153A_MASK_WATTNL_MASK                 0x00002000

// MS status register bit masks
#define ADE9153A_MS_STATUS_SYSRDY_MASK            0x4000
#define ADE9153A_MS_STATUS_CONFERR_MASK           0x2000
#define ADE9153A_MS_STATUS_ABSENT_MASK            0x1000
#define ADE9153A_MS_STATUS_TIMEOUT_MASK           0x0008
#define ADE9153A_MS_STATUS_READY_MASK             0x0002
#define ADE9153A_MS_STATUS_SHIFT_MASK             0x0001

// event status bit masks
#define ADE9153A_EVENT_STATUS_OIB_MASK            0x0020
#define ADE9153A_EVENT_STATUS_OIA_MASK            0x0010
#define ADE9153A_EVENT_STATUS_SWELLA_MASK         0x0004
#define ADE9153A_EVENT_STATUS_DIPA_MASK           0x0001

// Chip status register bit masks
#define ADE9153A_CHIP_STATUS_UART_RESET_MASK      0x0080     // UART interface reset detected
#define ADE9153A_CHIP_STATUS_UART_ERROR_MASK      0x00F0     // UART ERROR 0,1,2
#define ADE9153A_CHIP_STATUS_HW_ERROR_MASK        0x000F     // ERROR 0,1,2,3


#endif  /* PUBLIC_PERIPHERAL_POWER_ANALOG_DEVICES_ADE9153A_ADE9153A_H_ */
/*------------------------------ End of File -------------------------------*/
