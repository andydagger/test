/*
==========================================================================
 Name        : ade9153aMetering_dataProcess.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices/ade9153aMetering_dataProcess.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 21st May 2021
 Description : analog devices data processing implementation
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "config_pinout.h"
#include "config_dataset.h"
#include "sysmon.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "powerMonitor.h"
#include "ADE9153A.h"
#include "ade9153aMetering.h"
#include "ade9153aMetering_impl.h"
#include "ade9153aMetering_dataProcess.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/


/*****************************************************************************
 * Private Constants
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 *****************************************************************************/

/*******************************************************************************
 * @brief adds the instat values to the data set
 *
 * @param[in] ade9153a_instantValues * - pointer to the struct holding the output vals
 *            PowerCalcDataset *       - pointer to the dataset
 *
 * @return    powerMonitor_Result - result of process
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_addValuesToDataset( ade9153a_instantValues * instant_ptr, 
                                                 PowerCalcDataset       * dataset_ptr)
{
    // validate pointers
    if(( instant_ptr == NULL) || ( dataset_ptr == NULL))
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    // Prevent the RTOS kernel swapping out the task whilst
    // the dataset is being retrieved.
    vTaskSuspendAll();

    // Store values in dataset
    // voltage is in mV -> convert to V
    dataset_ptr->instantVrms        = instant_ptr->voltage / 1000;
    // current in mA -> convert to A
    dataset_ptr->instantIrms        = instant_ptr->current / 1000;
    // power in mW -> convert to W
    dataset_ptr->instantPower       = instant_ptr->power / 1000;

    dataset_ptr->instantPowerFactor = instant_ptr->powerFactor;

    // energy is in mWhr -> convert to 20Whr
    dataset_ptr->accumulatedEnergy_20WhrUnits += (uint32_t)((instant_ptr->energy / 20000) + 0.5F);

    // keep track of the minimum and maximum voltages
    if( dataset_ptr->instantVrms > dataset_ptr->vrmsMax)
    {
        dataset_ptr->vrmsMax = dataset_ptr->instantVrms;
    }

    if( dataset_ptr->instantVrms < dataset_ptr->vrmsMin)
    {
        dataset_ptr->vrmsMin = dataset_ptr->instantVrms;
    }

    if (dataset_ptr->vrmsAverage == 0)
    {
        dataset_ptr->vrmsAverage = dataset_ptr->instantVrms;
    }
    else
    {
        dataset_ptr->vrmsAverage += dataset_ptr->instantVrms;
        dataset_ptr->vrmsAverage /= 2;
    }

    // The operation is complete. Restart the RTOS kernel.
    xTaskResumeAll();

    return POWER_MONITOR_RESULT_OK;
}

/*******************************************************************************
 * @brief calculates the instant values from the register values
 *
 * @param[in] ade9153a_rawValues     * - pointer to the struct of raw values read from the device
 *            ade9153a_instantValues * - pointer to the struct holding the output vals
 *
 * @return    powerMonitor_Result - result of the calcs
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_calcInstantValues( ade9153a_rawValues     * raw_ptr, 
                                                ade9153a_instantValues * instant_ptr)
{
    // validate pointers
    if(( raw_ptr == NULL) || ( instant_ptr == NULL))
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    // Power in mW
    instant_ptr->power = (float)raw_ptr->activePower * ADE9153A_CAL_POWER_CC / 1000;

    // current RMS in mA
    instant_ptr->current = (float)raw_ptr->currentRMS * ADE9153A_CAL_IRMS_CC / 1000;

    // voltage RMS in mV
    instant_ptr->voltage = (float)raw_ptr->voltageRMS * ADE9153A_CAL_VRMS_CC / 1000;

    // Calculate PF
    instant_ptr->powerFactor = (float)raw_ptr->powerFactor / (float)134217728;

    // Energy in mWhr
    instant_ptr->energy = (float)raw_ptr->activeEnergy * ADE9153A_CAL_ENERGY_CC / 1000;

    return POWER_MONITOR_RESULT_OK;
}


/*******************************************************************************
 * @brief read the instant values from the device
 *
 * @param[in] ade9153a_rawValues * - pointer to the raw values structure
 *
 * @return    powerMonitor_Result - result of the read process
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_readInstantRegisterValues( ade9153a_rawValues* raw_ptr)
{
    powerMonitor_Result result;

    debug_printf(PWR, "ade9153a read instant values\r\n");

    // validate pointer
    if (raw_ptr == NULL)
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    result = ade9153a_readRegister_32(ADE9153A_REG_AVRMS_32, (uint32_t*)&raw_ptr->voltageRMS);

    if (result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    result = ade9153a_readRegister_32(ADE9153A_REG_AIRMS_32, (uint32_t*)&raw_ptr->currentRMS);

    if (result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    result = ade9153a_readRegister_32(ADE9153A_REG_APF_32, (uint32_t*)&raw_ptr->powerFactor);

    if (result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    result = ade9153a_readRegister_32(ADE9153A_REG_AWATT_32, (uint32_t*)&raw_ptr->activePower);

    if (result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    result = ade9153a_readRegister_32(ADE9153A_REG_AWATTHR_HI_32, (uint32_t*)&raw_ptr->activeEnergy);

    return result;
}


/*------------------------------ End of File -------------------------------*/

