/*
==========================================================================
 Name        : ade9153aMetering_calibration.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices/ade9153aMetering_calibration.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 3rd June 2021
 Description : analog devices calibration implementation
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "config_pinout.h"
#include "config_dataset.h"
#include "FreeRTOS.h"
#include "FreeRTOSCommonHooks.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "powerMonitor.h"
#include "ADE9153A.h"
#include "ade9153aMetering.h"
#include "ade9153aMetering_impl.h"
#include "ade9153aMetering_calibStartStop.h"
#include "ade9153aMetering_transport.h"
#include "ade9153aMetering_calibration.h"

 /*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static ade9153a_rawValues   ad_calRawValues;

/*****************************************************************************
 * Private Constants
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 *****************************************************************************/

/*******************************************************************************
 * @brief reads values after cal has finished to send back to the ATE
 *
 * @param[in] PowerCalcDataset * - pointer to the calibration dataset
 *
 * @return    powerMonitor_Result - Result of reading the values
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_calibrationReadValues( PowerCalcDataset * dataset_ptr)
{
    powerMonitor_Result    result = POWER_MONITOR_RESULT_OK;
    ade9153a_instantValues instantValues = {0};
    uint32_t pollCount;

    // validate pointer
    if( dataset_ptr == NULL)
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    debug_printf( PWR, "ade9153a read Calib readings\r\n");

    for( pollCount = 0; pollCount < ADE9153A_CAL_NO_OF_READINGS; pollCount++)
    {
        FreeRTOSDelay( ADE9153A_CAL_POLL_DELAY_MS);

        result = ade9153a_readInstantRegisterValues( &ad_calRawValues);

        if( result == POWER_MONITOR_RESULT_OK)
        {
            result = ade9153a_calcInstantValues( &ad_calRawValues, &instantValues);

            if( result == POWER_MONITOR_RESULT_OK)
            {
                result = ade9153a_addValuesToDataset( &instantValues, dataset_ptr);
            }
        }
        // any error break out of the loop
        if( result != POWER_MONITOR_RESULT_OK)
        {
            break;
        }
    }

    return result;
}

/*******************************************************************************
 * @brief Calibrates the current channel
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - Result of the calibration
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_calibrateCurrentChannel( void)
{
    powerMonitor_Result result;

    debug_printf( PWR, "ade9153a Calibrate Current Channel\r\n");

    result = ade9153a_startCalibration( ADE9153A_RUN_AUTOCAL_AI_TURBO);

    if( result == POWER_MONITOR_RESULT_OK)
    {
        // calibration is automatic so just wait.
        FreeRTOSDelay( ADE9153A_MSURE_AI_CAL_DELAY_MS);

        result = ade9153a_stopCalibration();
    }

    return result;
}

/*******************************************************************************
 * @brief Calibrates the voltage channel
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - Result of the calibration
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_calibrateVoltageChannel( void)
{
    powerMonitor_Result result;

    debug_printf( PWR, "ade9153a Calibrate Voltage Channel\r\n");

    result = ade9153a_startCalibration( ADE9153A_RUN_AUTOCAL_AV);

    if( result == POWER_MONITOR_RESULT_OK)
    {
        // calibration is automatic so just wait.
        FreeRTOSDelay( ADE9153A_MSURE_AV_CAL_DELAY_MS);

        result = ade9153a_stopCalibration();
    }

    return result;
}


/*******************************************************************************
 * @brief Reads the autocal registers and calc the conversion constants
 *
 * @param[in] ade9153a_AutoCalRegs - pointer to the struct holding the values
 *
 * @return    powerMonitor_Result  - Result of the read of the regs
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_readAutoCalRegisters( ade9153a_autoCalRegs * regs_ptr)
{
    powerMonitor_Result result;
    int32_t tempReg = 0;

    debug_printf( PWR, "ade9153a Read Auto-Cal registers\r\n");

    // validate pointer
    if( regs_ptr == NULL)
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    // Read AICC register
    result = ade9153a_readRegister_32( ADE9153A_REG_MS_ACAL_AICC_32, (uint32_t*)&tempReg);

    if( result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    regs_ptr->AcalAICCReg = tempReg;
    // Calculate Conversion Constant (CC)
    regs_ptr->AICC = (float)tempReg / (float)2048;

    // Read AICERT register
    result = ade9153a_readRegister_32( ADE9153A_REG_MS_ACAL_AICERT_32, (uint32_t*)&tempReg);

    if( result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    regs_ptr->AcalAICERTReg = tempReg;

    // Read AVCC register
    result = ade9153a_readRegister_32( ADE9153A_REG_MS_ACAL_AVCC_32, (uint32_t*)&tempReg);

    if( result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    regs_ptr->AcalAVCCReg = tempReg;
    // Calculate Conversion Constant (CC)
    regs_ptr->AVCC = (float)tempReg / (float)2048;

    // Read AICERT register
    result = ade9153a_readRegister_32( ADE9153A_REG_MS_ACAL_AVCERT_32, (uint32_t*)&tempReg);

    if( result != POWER_MONITOR_RESULT_OK)
    {
        return result;
    }

    regs_ptr->AcalAVCERTReg = tempReg;

    return result;
}


/*******************************************************************************
 * @brief calculates the I/V gains and writes them to the control registers
 *
 * @param[in] ade9153a_AutoCalRegs - pointer to the struct holding the values
 *
 * @return    powerMonitor_Result - Result of the process
 *
 *******************************************************************************/
powerMonitor_Result ade9153a_applyCalibrationGains( ade9153a_autoCalRegs * regs_ptr)
{
    powerMonitor_Result result;
    int32_t AIGain;
    int32_t AVGain;

    debug_printf( PWR, "ade9153a Apply calibration gains\r\n");
    // validate pointer
    if( regs_ptr == NULL)
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }
    // TODO: this calculation needs to made positive
    AIGain = (int32_t)((-( regs_ptr->AICC / ADE9153A_CAL_IRMS_CC) - 1) * (float)134217728);

    AVGain = (int32_t)((( regs_ptr->AVCC / ADE9153A_CAL_VRMS_CC) - 1) * (float)134217728);

    result = ade9153a_writeRegister_32( ADE9153A_REG_AIGAIN_32, (uint32_t)AIGain);

    if( result == POWER_MONITOR_RESULT_OK)
    {
        result = ade9153a_writeRegister_32( ADE9153A_REG_AVGAIN_32, (uint32_t)AVGain);
    }

    return result;
}


/*------------------------------ End of File -------------------------------*/

