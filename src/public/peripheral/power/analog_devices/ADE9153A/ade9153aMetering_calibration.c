/*
==========================================================================
 Name        : ade9153aMetering_calibration.c
 Project     : pcore
 Path        : /pcore/src/public/peripheral/power/analog_devices/ade9153aMetering_calibration.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 3rd June 2021
 Description : analog devices calibration implementation
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "systick.h"
#include "hw.h"
#include "debug.h"
#include "config_pinout.h"
#include "config_dataset.h"
#include "FreeRTOS.h"
#include "FreeRTOSCommonHooks.h"
#include "queue.h"
#include "task.h"
#include "config_queues.h"
#include "powerMonitor.h"
#include "productionMonitor.h"
#include "ADE9153A.h"
#include "ade9153aMetering.h"
#include "ade9153aMetering_impl.h"
#include "ade9153aMetering_transport.h"
#include "ade9153aMetering_dataProcess.h"
#include "ade9153aMetering_calibration.h"
#include "ade9153aMetering_calibDataProcess.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static ade9153a_autoCalRegs ad_calValues;

/*****************************************************************************
 * Private Constants
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 *****************************************************************************/

/*******************************************************************************
 * @brief calibration process for the analog devices power monitor
 *
 * @param[in] PowerCalcDataset * - pointer to the calibration dataset
 *
 * @return    powerMonitor_Result - Result of the calibration
 *
 *******************************************************************************/
powerMonitor_Result ade9153aMetering_calibrate( PowerCalcDataset * dataset_ptr)
{
    powerMonitor_Result result;

    // validate pointer
    if( dataset_ptr == NULL)
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    // calibrate the voltage and current channels
    // read the calibration regs and apply the gains
    memset((void*)&ad_calValues, 0, sizeof(ade9153a_autoCalRegs));

    result = ade9153a_calibrateCurrentChannel();

    if (result == POWER_MONITOR_RESULT_OK)
    {
        result = ade9153a_calibrateVoltageChannel();

        if (result == POWER_MONITOR_RESULT_OK)
        {
            result = ade9153a_readAutoCalRegisters(&ad_calValues);

            if (result == POWER_MONITOR_RESULT_OK)
            {
                result = ade9153a_applyCalibrationGains(&ad_calValues);
            }
        }
    }

    // this function is called on every power-on, we don't need to read values
    // for the calibration dataset if not under test
    if( productionMonitor_getUnitIsUndertest() == true)
    {
        if( result == POWER_MONITOR_RESULT_OK)
        {
            result = ade9153a_calibrationReadValues( dataset_ptr);
        }
    }

    if( result != POWER_MONITOR_RESULT_OK)
    {
        debug_printf( PWR, "ade9153a calibration failed: Error: %d\r\n", result);
    }

    return result;
}


/*------------------------------ End of File -------------------------------*/

