/*
==========================================================================
 Name        : DaliDriver_Nema.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliDriver_Nama.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 7 Nov 2019
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "hw.h"
#include "irq.h"
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

extern volatile bool dali_timerRunning;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

void DaliHw_startTimer(uint32_t timerFreq)
{
    dali_start_timer(timerFreq);
    dali_timerRunning = true;
}

void DaliHw_SetOutputTo(bool outState)
{
	dali_out(outState);
}

bool DaliHw_ReadInput(void)
{
	return dali_in();
}

void DaliHw_StopTimer(void)
{
	dali_timerRunning = false;
	//dali_stop_timer();
}
