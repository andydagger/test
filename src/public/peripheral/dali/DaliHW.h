/*
==========================================================================
 Name        : DaliDriver.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliDriver.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 7 Nov 2019
 Description : 
==========================================================================
*/

#ifndef PUBLIC_PERIPHERAL_DALI_DALIHW_H_
#define PUBLIC_PERIPHERAL_DALI_DALIHW_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/

#include "stdbool.h"
#include "stdint.h"

enum{
    DALI_TX_LOW = 0,
    DALI_TX_HIGH = 1
};
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

void DaliHw_startTimer(uint32_t timerFreq);
void DaliHw_SetOutputTo(bool outState);
bool DaliHw_ReadInput(void);
void DaliHw_StopTimer(void);

void DaliHw_EnableRxIrq(void);
void DaliHw_DisableRxIrq(void);

#endif /* PUBLIC_PERIPHERAL_DALI_DALIHW_H_ */
