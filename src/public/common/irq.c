/*
==========================================================================
 Name        : irq.c
 Project     : pcore
 Path        : /pcore/src/public/common/irq.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 25 Aug 2017
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "chip.h"
#include "stdbool.h"
#include "irq.h"
#include "systick.h"
#include "nwk_tek.h"
#include "flash.h"
#include "config_pinout.h"
#include "DaliTx.h"
#include "DaliRx.h"
#include "debug.h"
#include "adc_procx.h"
#include "config_adc_procx.h"
#include "adc_procx.h"
#include "gps.h"
#include "config_timers.h"
#include "rtc_procx.h"
#include "MurataDriver.h"
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
volatile bool dali_timerRunning = false;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/


/*******************************************************************************
 * @brief IRQ Handler for LORAWAN module com port
 * @param[in] void
 * @return void
 *******************************************************************************/
//void UART_LORA_HANDLER(void){
//    lorawanhw_irq_handler();
//    return;
//}
void UART0_IRQHandler(void){
    nwk_tek_trx_isr();
    return;
}

/*******************************************************************************
 * @brief IRQ Handler timers inc DALI tx/rx
 * @param[in] void
 * @return void
 *******************************************************************************/
void MRT_IRQHandler(void)
{
	uint32_t int_pend;
	static uint32_t rtcCount = 0;

	/* Get and clear interrupt pending status for all timers */
	int_pend = Chip_MRT_GetIntPending();
	Chip_MRT_ClearIntPending(int_pend);

    /* Channel 0 SYSTICK 1000Hz*/
    if (int_pend & MRTn_INTFLAG(SYSTICK_MRT_CH)) {
        systick_isr();

        // Check if RTC tick should be updated
        if(++rtcCount >= 1000)
        {
        	rtcCount = 0;

        	rtc_procx_IncCounter();
        }
		nwk_tek_checkForStringEnd();
    }

    /* Channel 1 DALI TX_RX variable*/
    if (int_pend & MRTn_INTFLAG(DALI_MRT_CH)) {
    	if (dali_timerRunning)
    	{
    		DaliTx_ISR();
    		DaliRx_TimerISR();
    	}
    }

	/* Channel 2  = 10 Hz*/
	if (int_pend & MRTn_INTFLAG(ADC_MRT_CH)) {
		// if enough samples ahve been acquired calculate average
		if( adc_procx_process_samples() == false){
			adc_procx_calculate_average();
		}else{}
		// kick off new sample
		Chip_ADC_StartSequencer(ADC_PHOTO_IF, ADC_PHOTO_SEQ);
	}

}

/*******************************************************************************
 * @brief IRQ Handler for adc sampling sequence terminated
 * @param[in] void
 * @return void
 *******************************************************************************/
void ADC1A_IRQHandler(void)
{
	uint32_t pending;


//	Get pending interrupts
	pending = Chip_ADC_GetFlags(ADC_PHOTO_IF);

//	 Sequence A completion interrupt
	if (pending & ADC_FLAGS_SEQA_INT_MASK) {
		// store samples
		set_adc_procx_photo_ic_spl(
				ADC_DR_RESULT(	Chip_ADC_GetDataReg(	ADC_PHOTO_IF,
														ADC_PHOTO_CH)));
		set_adc_procx_pwm_fb_spl(
				ADC_DR_RESULT(	Chip_ADC_GetDataReg(	ADC_PHOTO_IF,
														ADC_PWM_FB_CH)));
		set_adc_procx_an_driver_in_spl(
				ADC_DR_RESULT(	Chip_ADC_GetDataReg(	ADC_PHOTO_IF,
														ADC_AN_DRIV_IN_CH)));
        set_adc_procx_p_det_spl(
                ADC_DR_RESULT(  Chip_ADC_GetDataReg(    ADC_PHOTO_IF,
                                                        ADC_AN_P_DET_CH)));
	}

//	/Clear any pending interrupts
	Chip_ADC_ClearFlags(ADC_PHOTO_IF, ADC_SEQ_IRQ_FLAGS);
}

/*******************************************************************************
 * @brief IRQ Handler for gps i2c receiver
 * @param[in] void
 * @return void
 *******************************************************************************/
void I2C0_IRQHandler(void){

	// I2C slavecallback function list
	const I2CS_XFER_T i2csCallBacks = {
		&gps_rx_start,
		&gps_tx_byte,
		&gps_rx_byte,
		&gps_rx_done
	};
	uint32_t state = Chip_I2C_GetPendingInt(GPS_I2C_IF);

	// Error handling
	if (state & (I2C_INTSTAT_MSTRARBLOSS | I2C_INTSTAT_MSTSTSTPERR)) {
		Chip_I2CM_ClearStatus(GPS_I2C_IF, I2C_STAT_MSTRARBLOSS | I2C_STAT_MSTSTSTPERR);
	}

	// I2C slave related interrupt
	while (state & (I2C_INTENSET_SLVPENDING | I2C_INTENSET_SLVDESEL)) {
		Chip_I2CS_XferHandler(GPS_I2C_IF, &i2csCallBacks);
		// Update state
		state = Chip_I2C_GetPendingInt(GPS_I2C_IF);
	}
}

