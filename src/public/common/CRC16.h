/*
==========================================================================
 Name        : CRC16.h
 Project     : pcore
 Path        : /pcore/src/public/common/CRC16.h
 Author      : jeff barker
 Copyright   : Lucy Zodion Ltd
 Created     : 28 May 2021
 Description :
==========================================================================
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef PUBLIC_COMMON_RING_CRC16_H_
#define PUBLIC_COMMON_RING_CRC16_H_

/*---------------------- Private Enums/Constants/Types -----------------------*/

/*---------------------- Public Enums/Constants/Types ------------------------*/
     
/*---------------------------- Public Variables ------------------------------*/

/*---------------------- Public Function Declarations ------------------------*/
uint16_t CRC16_calculate( uint16_t seed, uint8_t * data_ptr, uint8_t length);

/*------------------------------- End of File --------------------------------*/
#endif /* PUBLIC_COMMON_RING_CRC16_H_ */
