/*
==========================================================================
 Name        : systick.c
 Project     : pcore
 Path        : /pcore/src/public/common/systick.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 25 Aug 2017
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "chip.h"
#include "systick.h"
#include "irq.h"
#include "flash.h"
#include "diskio.h"
#include "debug.h"

#include "FreeRTOS.h"
#include "task.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static volatile uint32_t systick_ms_count = 0;
static volatile uint32_t systick_sec_count = 0;  /* Add separate counter for the Systick Seconds */
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief SysTick ISR increments ms counter
 * @param[in] void
 * @return void
 *******************************************************************************/
void systick_isr(void){
	systick_ms_count++;
	/* Increment the Systick Seconds counter on every 1000th ms */
	if(0 == (systick_ms_count % 1000)) systick_sec_count++;
}


/*******************************************************************************
 * @brief Use to read back the sys ms counter
 * @param[in] void
 * @return uint32_t ms count
 *******************************************************************************/
uint32_t get_systick_ms(void) {
    return systick_ms_count;
}

/*******************************************************************************
 * @brief Use to read back the sys sec counter
 * @param[in] void
 * @return uint32_t sec count
 *******************************************************************************/
uint32_t get_systick_sec(void) {
    return systick_sec_count;
}

