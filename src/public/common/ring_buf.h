/*
==========================================================================
 Name        : ring_buf.h
 Project     : pcore
 Path        : /pcore/src/public/common/ring_buf.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 9 Jan 2019
 Description : 
==========================================================================
*/

#ifndef PUBLIC_COMMON_RING_BUF_H_
#define PUBLIC_COMMON_RING_BUF_H_


#include "lpc_types.h"

/** @defgroup Ring_Buffer CHIP: Simple ring buffer implementation
 * @ingroup CHIP_Common
 * @{
 */

/**
 * @brief Ring buffer structure
 */
typedef struct {
    void *data;
    int count;
    int itemSz;
    uint32_t head;
    uint32_t tail;
} RINGBUFF_TYPE;

/**
 * @def     RB_VHEAD(rb)
 * volatile typecasted head index
 */
#define RB_VHEAD(rb)              (*(volatile uint32_t *) &(rb)->head)

/**
 * @def     RB_VTAIL(rb)
 * volatile typecasted tail index
 */
#define RB_VTAIL(rb)              (*(volatile uint32_t *) &(rb)->tail)

/**
 * @brief   Initialize ring buffer
 * @param   RingBuff    : Pointer to ring buffer to initialize
 * @param   buffer      : Pointer to buffer to associate with RingBuff
 * @param   itemSize    : Size of each buffer item size
 * @param   count       : Size of ring buffer
 * @note    Memory pointed by @a buffer must have correct alignment of
 *          @a itemSize, and @a count must be a power of 2 and must at
 *          least be 2 or greater.
 * @return  Nothing
 */
int ring_buf_init(RINGBUFF_TYPE *RingBuff, void *buffer, int itemSize, int count);

/**
 * @brief   Resets the ring buffer to empty
 * @param   RingBuff    : Pointer to ring buffer
 * @return  Nothing
 */
STATIC INLINE void ring_buf_flush(RINGBUFF_TYPE *RingBuff)
{
    RingBuff->head = RingBuff->tail = 0;
}

/**
 * @brief   Return size the ring buffer
 * @param   RingBuff    : Pointer to ring buffer
 * @return  Size of the ring buffer in bytes
 */
STATIC INLINE int get_ring_buf_size(RINGBUFF_TYPE *RingBuff)
{
    return RingBuff->count;
}

/**
 * @brief   Return number of items in the ring buffer
 * @param   RingBuff    : Pointer to ring buffer
 * @return  Number of items in the ring buffer
 */
STATIC INLINE int get_ring_buf_count(RINGBUFF_TYPE *RingBuff)
{
    return RB_VHEAD(RingBuff) - RB_VTAIL(RingBuff);
}

/**
 * @brief   Return number of free items in the ring buffer
 * @param   RingBuff    : Pointer to ring buffer
 * @return  Number of free items in the ring buffer
 */
STATIC INLINE int get_ring_buf_free(RINGBUFF_TYPE *RingBuff)
{
    return RingBuff->count - get_ring_buf_count(RingBuff);
}

/**
 * @brief   Return number of items in the ring buffer
 * @param   RingBuff    : Pointer to ring buffer
 * @return  1 if the ring buffer is full, otherwise 0
 */
STATIC INLINE int get_ring_buf_is_full(RINGBUFF_TYPE *RingBuff)
{
    return (get_ring_buf_count(RingBuff) >= RingBuff->count);
}

/**
 * @brief   Return empty status of ring buffer
 * @param   RingBuff    : Pointer to ring buffer
 * @return  1 if the ring buffer is empty, otherwise 0
 */
STATIC INLINE int get_ring_buf_is_empty(RINGBUFF_TYPE *RingBuff)
{
    return RB_VHEAD(RingBuff) == RB_VTAIL(RingBuff);
}

/**
 * @brief   Insert a single item into ring buffer
 * @param   RingBuff    : Pointer to ring buffer
 * @param   data        : pointer to item
 * @return  1 when successfully inserted,
 *          0 on error (Buffer not initialized using
 *          ring_buf__Init() or attempted to insert
 *          when buffer is full)
 */
int ring_buf_insert(RINGBUFF_TYPE *RingBuff, const void *data);
// below added by BR to force write into ring-buffer even if full
int ring_buf_force_write_at_back(RINGBUFF_TYPE *RingBuff, const void *data);
int ring_buf_force_write_at_front(RINGBUFF_TYPE *RingBuff, const void *data);

/**
 * @brief   Insert an array of items into ring buffer
 * @param   RingBuff    : Pointer to ring buffer
 * @param   data        : Pointer to first element of the item array
 * @param   num         : Number of items in the array
 * @return  number of items successfully inserted,
 *          0 on error (Buffer not initialized using
 *          ring_buf__Init() or attempted to insert
 *          when buffer is full)
 */
int ring_buf_insert_multi(RINGBUFF_TYPE *RingBuff, const void *data, int num);

/**
 * @brief   Pop an item from the ring buffer
 * @param   RingBuff    : Pointer to ring buffer
 * @param   data        : Pointer to memory where popped item be stored
 * @return  1 when item popped successfuly onto @a data,
 *          0 When error (Buffer not initialized using
 *          ring_buf__Init() or attempted to pop item when
 *          the buffer is empty)
 */
int ring_buf_pop(RINGBUFF_TYPE *RingBuff, void *data);

/**
 * @brief   Pop an array of items from the ring buffer
 * @param   RingBuff    : Pointer to ring buffer
 * @param   data        : Pointer to memory where popped items be stored
 * @param   num         : Max number of items array @a data can hold
 * @return  Number of items popped onto @a data,
 *          0 on error (Buffer not initialized using ring_buf__Init()
 *          or attempted to pop when the buffer is empty)
 */
int ring_buf_pop_multi(RINGBUFF_TYPE *RingBuff, void *data, int num);


/**
 * @}
 */

#endif /* PUBLIC_COMMON_RING_BUF_H_ */
