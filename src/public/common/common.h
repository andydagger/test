/*
==========================================================================
 Name        : common.h
 Project     : pcore
 Path        : /pcore/src/public/common/common.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 24 Aug 2017
 Description :
==========================================================================
*/

#ifndef COMMON_H_
#define COMMON_H_

#include "chip.h"
#include "systick.h"
#include "stdbool.h"
#include "stdint.h"

#define KEYS_ADDRESS (uint32_t)0xB000
#define IMAGE_ADDRESS (uint32_t)0xC000
#define CODE_SECTOR_SIZE (uint32_t)0x1000
#define PROD_TEST_ADDRESS (uint32_t) 0x34000

#define FW_VERSION_MAJOR    "01_MAJOR"
#define FW_VERSION_MINOR    "64_MINOR"
#define FW_VERSION_TEST     "10_TEST"

#ifndef COMPILE_IMAGE
    #define _freezeTask(); {printf(" >> TASK FREEZE!!");}
#else
    #define _freezeTask(); {while(1);}
#endif

#define matrix_n_rows(x) (sizeof(x)/sizeof(x[0]))
#define ARRAY_LEN(X) (sizeof(X)/sizeof((X)[0]))

void common_raw_to_ascii( uint8_t * p_raw, char * p_ascii, uint8_t n_bytes);
uint32_t common_array_to_uint32(char**p_buffer);
int32_t common_array_to_int32(char**p_buffer);

uint32_t lz_ascii_to_uint32_ascii(char*p_buffer);
uint32_t lz_char_to_uint32_ascii(char*p_buffer);
uint16_t lz_char_to_uint16_ascii(char*p_buffer);
uint8_t lz_char_to_uint8_ascii(char*p_buffer);

bool lz_strcmp( char * ref_sting, char * tested_string, uint8_t n_char);
bool lz_strcmp_nchar( char * ref_sting, char * tested_string, uint8_t nchar);

uint32_t common_varint_to_uint32( char ** p_p_source_str, uint8_t * p_var_int_byte_shift);
int32_t lz_decode_zigzag_ul( uint32_t in);
int32_t lz_decode_zigzag_varint( char ** p_p_source_str, uint8_t * p_var_int_byte_shift);
uint32_t flip_uint32(uint32_t a);
uint16_t fetch_fw_version_decimal(void);
uint8_t common_uint16_varint(uint16_t value, uint8_t* output) ;

float common_getFloatFromFlash(uint32_t flashAddr);
bool common_checkFlashClear(uint32_t flashAddr, uint32_t len);


uint32_t common_trueRandomGenerator(uint32_t maxValue);

void set_rtos_started(void);
bool get_rtos_started(void);

uint32_t get_random_delay_ms(void);
void delay_ms(uint32_t delay);

#define pdMS_TO_TICKS_LZ( xTimeInMs ) ( ( TickType_t ) ( xTimeInMs ) )

#define sys_delay_ms(x) delay_ms(x)
//FreeRTOSDelay(x)
#define get_tmstp() get_systick_ms()

/*
 * ERROR CATCHING ROUTINES
 */

// catch an error = (function returns false) and break loop
#define catch_error_break(x); {if( (x) == false ) break; }

// catch an error = (function returns false) and then return from function
#define catch_error_return(x); {if( (x) == false ) return; }

// catch an error = (function returns false) and return false = fail
#define catch_error_fail(x); {if( (x) == false ) return false; }

// catch an error = (test returns true) and return false = fail
#define catch_test_fail(x); {if( (x) == true ) return false; }

// catch an error = (test returns true) and return false = fail
#define catch_test_break(x); {if( (x) == true ) break; }

// catch error = (fatfs reurn not FR_OK) report and return false = fail
#define catch_file_error_report(e, t); {if((t) != FR_OK) { report_error(e); return false; }}

// catch error = (function returns false) report and return false = fail
#define catch_error_report(e, t); {if((t) == false) { report_error(e); return false; }}

// catch error = (test returns true) report and return false = fail
#define catch_test_report(e, t); {if((t) == true) { report_error(e); return false; }}

// catch error = (fatfs reurn not FR_OK) report and return
#define catch_file_err_report_void(e, t); {if((t) != FR_OK) { report_error(e); return; }}

// catch error = (function returns false) report and return
#define catch_err_report_void(e, t); {if((t) == false) { report_error(e); return; }}

// catch error = (test returns true) report and return
#define catch_test_report_void(e, t); {if((t) == true) { report_error(e); return; }}

#endif /* COMMON_H_ */
