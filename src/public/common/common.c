/*
==========================================================================
 Name        : common.c
 Project     : pcore
 Path        : /pcore/src/public/common/common.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 24 Aug 2017
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "stdint.h"
#include "stdbool.h"
#include <string.h>
#include <stdlib.h>
#include "stdio.h"
#include "common.h"
#include "systick.h"
#include "config_lorawan.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
* @brief Returns random delay in ms using dev eui as source
* @param[in] void
* @return uint32_t delay in ms
*******************************************************************************/
uint32_t get_random_delay_ms(void){
    char str[17];
    uint32_t connect_delay = 0;

    snprintf(str, sizeof str, "%s", DEV_EUI);
    connect_delay = strtol(&str[11], NULL, 16);

    if(connect_delay > 0xFFFFF){
        connect_delay = 0xFFFFF;
    }

    return connect_delay;
}

/*******************************************************************************
* @brief converts char to uint32
* @param[in] ptr to string to convert
* @return uint32_t of the converted decimal value
*******************************************************************************/
void delay_ms(uint32_t delay){
    // do not set systick_on_entry as static so that different tasks can
    // store its value in their own stack
    uint32_t systick_on_entry = get_systick_ms();
    while((get_systick_ms() - systick_on_entry) < delay);
    return;
}

/*******************************************************************************
* @brief converts char to uint32
* @param[in] ptr to string to convert
* @return uint32_t of the converted decimal value
*******************************************************************************/
void common_raw_to_ascii( uint8_t * p_raw, char * p_ascii, uint8_t n_bytes){
    while(n_bytes > 0){
        sprintf( p_ascii, "%02X", *p_raw );
        n_bytes--;
        p_ascii += 2;
        p_raw++;
    }
    *p_ascii = '\0';
}

/*******************************************************************************
* @brief converts char to uint32
* @param[in] ptr to string to convert
* @return uint32_t of the converted decimal value
*******************************************************************************/
uint32_t common_array_to_uint32(char ** p_p_buffer) {
    uint32_t number = 0;

    number += **p_p_buffer; // MSB
    number = number << 8;
    (*p_p_buffer)++;
    number += **p_p_buffer;
    number = number << 8;
    (*p_p_buffer)++;
    number += **p_p_buffer;
    number = number << 8;
    (*p_p_buffer)++;
    number += **p_p_buffer; // LSB
    (*p_p_buffer)++;

    return number;
}

/*******************************************************************************
* @brief converts char to int32
* @param[in] ptr to string to convert
* @return int32_t of the converted decimal value
*******************************************************************************/
int32_t common_array_to_int32(char ** p_p_buffer) {
    int32_t number = 0;

    number += **p_p_buffer; // MSB
    number = number << 8;
    (*p_p_buffer)++;
    number += **p_p_buffer;
    number = number << 8;
    (*p_p_buffer)++;
    number += **p_p_buffer;
    number = number << 8;
    (*p_p_buffer)++;
    number += **p_p_buffer; // LSB
    (*p_p_buffer)++;

    return number;
}


/*******************************************************************************
* @brief converts ascii to uint32
* @param[in] ptr to string to convert
* @return uint32_t of the converted decimal value
*******************************************************************************/
uint32_t lz_ascii_to_uint32_ascii(char*p_buffer) {
	char local_str[11] = {0};
	uint32_t number = 0;

	strncpy(local_str, p_buffer, 10);
	number = (uint32_t) strtol(local_str, NULL, 10);

	return number;
}
/*******************************************************************************
* @brief converts char to uint32
* @param[in] ptr to string to convert
* @return uint32_t of the converted decimal value
*******************************************************************************/
uint32_t lz_char_to_uint32_ascii(char*p_buffer) {
	char local_str[9] = {0};
	uint32_t number = 0;

	strncpy(local_str, p_buffer, 8);
	number = (uint32_t) strtol(local_str, NULL, 16);

	return number;
}
/*******************************************************************************
* @brief converts char to uint16
* @param[in] ptr to string to convert
* @return uint16_t of the converted decimal value
*******************************************************************************/
uint16_t lz_char_to_uint16_ascii(char*p_buffer) {
	char local_str[5] = {0};
	uint16_t number = 0;

	strncpy(local_str, p_buffer, 4);
	number = (uint16_t) strtol(local_str, NULL, 16);

	return number;
}
/*******************************************************************************
* @brief converts char to uint8
* @param[in] ptr to string to convert
* @return uint32_t of the converted decimal value
*******************************************************************************/
uint8_t lz_char_to_uint8_ascii(char*p_buffer) {
	char local_str[3] = {0};
	uint8_t number = 0;

	strncpy(local_str, p_buffer, 2);
	number = (uint8_t) strtol(local_str, NULL, 16);

	return number;
}

/*******************************************************************************
* @brief Compares 2 strings up to but ecluding the \n char
* @param[in]
* ptr to refernce string
* ptr to string being tested
* n_char number of chars to be testes
* @return bool true for matching strings
*******************************************************************************/
bool lz_strcmp( char * ref_sting, char * tested_string, uint8_t n_char){
    bool strings_match = false;

    while( n_char-- )
    {
        if( *ref_sting++ != *tested_string++){
            strings_match = false;
            break;
        }else{
            strings_match = true;
        }
    }

    return strings_match;
}

/*******************************************************************************
* @brief Compares 2 strings up to the number of given chars
* @param[in] ptr to refernce string and ptr to string being tested
* @return bool true for matching strings
*******************************************************************************/
 bool lz_strcmp_nchar( char * ref_sting, char * tested_string, uint8_t nchar){
 	bool strings_match = true;

 	while( nchar--)
 	{
 		if( *ref_sting++ != *tested_string++){
 			strings_match = false;
 			break;
 		}
 	}

 	return strings_match;
 }


/*******************************************************************************
* @brief Convert varint to uint32_t and shifts the source ptr from the caller
* @param[in] ptr to ptr to source bytes and byte_shift record
* @return void
*******************************************************************************/
 uint32_t common_varint_to_uint32(  char ** p_p_source,
                                    uint8_t * p_var_int_byte_shift)
 {
     uint32_t result = 0;
     uint32_t cur_byte = 0;
     uint8_t bits_shift = 0;
     /*
      * example of conversions:
      * 0x001 >> 0x80
      * 0x808001 >> 0x4000
      */

     *p_var_int_byte_shift = 0;

     //
     while((**p_p_source) & 0x80){
         cur_byte = **p_p_source;
         result += (cur_byte & 0x7F) << bits_shift;
         (*p_p_source)++;
         (*p_var_int_byte_shift) += 1;
         bits_shift += 7;
     }
     cur_byte = **p_p_source;
     result += cur_byte << bits_shift;
     *p_var_int_byte_shift += 1;
     (*p_p_source)++;

     return result;
 }

/*******************************************************************************
* @brief Decode zigzag uint32_t input
* @param[in] uint32_t zigzag encoded
* @return int32_t zigzag decoded
*******************************************************************************/
 int32_t lz_decode_zigzag_ul( uint32_t in)
 {
     /*
      *     in      out
      *     0       0
      *     1       -1
      *     2       +1
      *     3       -2
      *     4       +2
      *     ...
      */
     return (int32_t)( in >> 1 ) ^ (- (in & 1) );
 }

/*******************************************************************************
* @brief Decode varint zigzag input
* @param[in] ptr to ptr to source bytes and byte_shift record
* @return int32_t zigzag decoded
*******************************************************************************/
int32_t lz_decode_zigzag_varint(    char ** p_p_source_str,
                                    uint8_t * p_var_int_byte_shift)
{
  uint32_t varint_out = 0;

  varint_out = common_varint_to_uint32(p_p_source_str, p_var_int_byte_shift);

  return lz_decode_zigzag_ul(varint_out);
}

/*******************************************************************************
* @brief Flip uint32_t bytes
* @param[in] void
* @return void
*******************************************************************************/
uint32_t flip_uint32(uint32_t a)
{
  a = ((a & 0x000000FF) << 24) |
      ((a & 0x0000FF00) <<  8) |
      ((a & 0x00FF0000) >>  8) |
      ((a & 0xFF000000) >> 24);
  return a;
}

/*******************************************************************************
* @brief Flip uint32_t bytes
* @param[in] void
* @return uint16_t fw version in decimal for paylaods
*******************************************************************************/
uint16_t fetch_fw_version_decimal(void)
{
    uint16_t fw_version = 0;
    uint8_t major = 0;
    uint8_t minor = 0;
    uint8_t test = 0;
    char ascii_section[3]={0};

    strncpy( ascii_section, (const char *)FW_VERSION_MAJOR, 2);
    major = (uint8_t)atoi(ascii_section);

    strncpy( ascii_section, (const char *)FW_VERSION_MINOR, 2);
    minor = (uint8_t)atoi(ascii_section);

    strncpy( ascii_section, (const char *)FW_VERSION_TEST, 2);
    test = (uint8_t)atoi(ascii_section);


    fw_version = major*100 + minor;

    // even if test is at 0 = production release still incldue in fw version
    fw_version = fw_version*100 + (uint16_t)test;

  return fw_version;
}
/*******************************************************************************
* @brief Varint encoding of 16 bit values
* @param[in] void
* @return void
*******************************************************************************/
uint8_t common_uint16_varint(uint16_t value, uint8_t* output) {
    uint8_t outputSize = 0;
    //While more than 7 bits of data are left, occupy the last output byte
    // and set the next byte flag
    while (value > 127) {
        //|128: Set the next byte flag
        output[outputSize] = ((uint8_t)(value & 127)) | 128;
        //Remove the seven bits we just wrote
        value >>= 7;
        outputSize++;
    }
    output[outputSize++] = ((uint8_t)value) & 127;
    return outputSize;
}

/*******************************************************************************
* @brief Returns float that is stored in flash
* @param[in] address of float in flash
* @return float value
*******************************************************************************/
float common_getFloatFromFlash(uint32_t flashAddr)
{
	float* fP = (float*) flashAddr;
	float f = *fP;
	return f;
}

/*******************************************************************************
* @brief Returns true if flash region is all 0xff, otherwise returns false
* @param[in] start address of flash, length (number of bytes)
* @return bool
*******************************************************************************/
bool common_checkFlashClear(uint32_t flashAddr, uint32_t len)
{
	uint32_t i;

	uint8_t* fB = (uint8_t*) flashAddr;

	for(i=0; i<len; i++)
	{
		if(0xff != fB[i])
		{
			return false;
		}
	}
	return true;
}

uint32_t common_trueRandomGenerator(uint32_t maxValue)
{
    return (uint32_t)((uint32_t)rand() % maxValue);
}
