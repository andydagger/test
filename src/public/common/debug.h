/*
==========================================================================
 Name        : debug.h
 Project     : pcore
 Path        : /pcore/src/public/common/debug.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 24 Aug 2017
 Description : 
==========================================================================
*/


#ifndef COMMON_DEBUG_H_
#define COMMON_DEBUG_H_

#include "config_queues.h"


/*****************************************************************************
 * Definitions
 ****************************************************************************/
#define DEBUG_ENABLED

#ifdef DEBUG_ENABLED
    #define debug_printf    debugprint
#else
    #define debug_printf
#endif

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/


/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void debugprint(fw_module_id caller_id, const char *fmt, ...);
void debug_print_queue(QuDebugMsg MsgToPrint);
void debug_print_instant(fw_module_id caller_id, const char *fmt, ...);

#endif /* COMMON_DEBUG_H_ */
