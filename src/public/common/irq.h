/*
==========================================================================
 Name        : irq.h
 Project     : pcore
 Path        : /pcore/src/public/common/irq.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 25 Aug 2017
 Description : 
==========================================================================
*/


#ifndef COMMON_IRQ_H_
#define COMMON_IRQ_H_

/*****************************************************************************
 * Definitions
 ****************************************************************************/
/*
 * Setting up priority level with __NVIC_PRIO_BITS = 3
 * (1<<__NVIC_PRIO_BITS) = 8
 * 7 is the lowest and 0 the highest in case of PLC15xx
 * ((1<<__NVIC_PRIO_BITS) - 1) = 7 lowest
 * ((1<<__NVIC_PRIO_BITS) - 8) = 0 highest
 */
#define DALI_PRIORITY_LEVEL ((1<<__NVIC_PRIO_BITS) - 7) //2
#define LORAWAN_PRIORITY_LEVEL ((1<<__NVIC_PRIO_BITS) - 7) //1
#define MRT_TIMERS_PRIORITY_LEVEL ((1<<__NVIC_PRIO_BITS) - 6) //2
#define POWER_PRIORITY_LEVEL ((1<<__NVIC_PRIO_BITS) - 5) //3
#define ADC_PRIORITY_LEVEL ((1<<__NVIC_PRIO_BITS) - 4) //4

/* Priorities passed to NVIC_SetPriority() do not require shifting as the
function does the shifting itself.  Note these priorities need to be equal to
or lower than configMAX_SYSCALL_INTERRUPT_PRIORITY - therefore the numeric
value needs to be equal to or greater than 5 (on the Cortex-M3 the lower the
numeric value the higher the interrupt priority). */

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
//void UART0_IRQHandler(void);
//void RIT_IRQHandler(void);

void UART0_IRQHandler(void);
void MRT_IRQHandler(void);
void ADC1A_IRQHandler(void);

#endif /* COMMON_IRQ_H_ */
