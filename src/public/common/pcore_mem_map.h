/*
==========================================================================
 Name        : pcore_mem_map.h
 Project     : pcore_bootloader
 Path        : /pcore_bootloader/src/memory_map/pcore_mem_map.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 26 Oct 2017
 Description : 
==========================================================================
*/

#ifndef MEMORY_MAP_PCORE_MEM_MAP_H_
#define MEMORY_MAP_PCORE_MEM_MAP_H_

/*****************************************************************************
 * Definitions
 ****************************************************************************/

// LPC1519 After Vector starts at 0x278
// The vector table length depends on the RAM mapping definition
// Thus it should never be altered!
#define VECTOR_CRC_ADD          0x278
#define IMAGE_CRC_ADD           0x27C
#define IMAGE_VERSION_ADD       0x280
#define IMAGE_LENGTH_ADD        0x284
#define IMAGE_CRC_CALC_START    0x288

typedef struct{
    uint32_t ram_validity_word;
    uint32_t start_up_add;
    uint32_t start_up_version;
    uint32_t boot_retries;
}bootram_struct;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/



#endif /* MEMORY_MAP_PCORE_MEM_MAP_H_ */
