/*
==========================================================================
 Name        : debug.c
 Project     : pcore
 Path        : /pcore/src/public/common/debug.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 24 Aug 2017
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "chip.h"
#include "stdbool.h"
#include "stdint.h"
#include "debug.h"
#include "config_queues.h"
#include "config_pinout.h"
#include "config_debug.h"
#include "hw.h"
#include "systick.h"
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include "config_network.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
typedef struct{
    fw_module_id caller_id;
    char * caller_name_string;
    bool b_is_enabled;
}debug_caller_struct;

static const debug_caller_struct caller_list[] =
{
        { BOOT,     "BOOT: \t\t" ,      true},
        { MAIN,     "MAIN: \t\t" ,      true},
        { LORA,     "LORA: \t\t" ,      true},
        { LORAINIT, "LORAINIT: \t\t" ,  true},
        { LORAMAC,  "LORAMAC: \t" ,     true},
        { LORA_TX,  "LORA_TX: \t" ,     true},
        { LORA_RX,  "LORA_RX: \t" ,     true},
        { LUMINROP, "LUMINAIRE_OP: \t", true},
        { DALI,     "DALI: \t\t" ,      true},
        { NWK,      "NWK: \t\t" ,       true},
        { FFS,      "FATFS: \t\t" ,     false},
        { FLASH,    "FLASH: \t\t" ,     false},
        { PATCH_RX, "PATCH RX: \t" ,    true},
        { PATCH_PRO,"PATCH PRO: \t" ,   true},
        { LUMINR,   "LUMINR: \t" ,      true},
        { CALEN,     "CAL: \t",         false},
        { RTC,      "RTC: \t" ,         true},
        { PWR,      "PWR: \t" ,         true},
        { GPS,      "GPS: \t" ,         false},
        { FILESYS,  "FSYS: \t" ,        true},
        { SYSMON,   "SYSMON: \t",       true},
        { SENSOR,   "SENSOR: \t",       true},
        { COMSNG,   "COMSNG: \t",       true},
        { PROD,     "PROD: \t",         true},
        { NVM,      "NVM: \t",          true}
};

//    static char debug_printf_queue[DEBUG_QUEUE_DEPTH][DEBUG_QUEUE_WIDTH];
//    static uint8_t queue_position = 0;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
QueueHandle_t xQuDebug = NULL;

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void lzprintf(char * p_str);
static void debug_tx_byte(char * pCharToSend);

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Loads queue with debug strings inc timestamp.
 * @param[in] void
 * @return void
 *******************************************************************************/
void debugprint(fw_module_id caller_id, const char *fmt, ...) {
    va_list args;

    // Do not attempt to access queue before the have been created
    if(xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED) return;

    // Fiter for disabling  individual stack debug
    if( caller_list[caller_id].b_is_enabled == false ){
        return;
    }

    // Prepare message for Debug queue
    QuDebugMsg debug_msg;
    debug_msg.caller_id = caller_id;
    debug_msg.tick_count = get_systick_ms();
    // load formatted string
    va_start(args, fmt);
    vsnprintf(debug_msg.msg, sizeof(debug_msg.msg), fmt, args);
    va_end(args);

    // Insure string always include CR and LF
    debug_msg.msg[sizeof(debug_msg.msg)-2] = '\r';
    debug_msg.msg[sizeof(debug_msg.msg)-1] = '\n';

#ifdef COMPILE_IMAGE
    // Add msg to queue if more than 1 slot available
    if ( uxQueueSpacesAvailable(xQuDebug) > 1){
        xQueueSend(xQuDebug, &debug_msg, 0);
    }else{
        sprintf(    debug_msg.msg,
                    "************ DEBUG QUEUE FULL ***********\r\n");
        xQueueSend(xQuDebug, &debug_msg, 0);
    }
#else
    debug_print_instant(caller_id, debug_msg.msg);
#endif



    return;
}

/*******************************************************************************
 * @brief Print out of qqueued msg
 * @param[in] void
 * @return void
 *******************************************************************************/
void debug_print_queue(QuDebugMsg MsgToPrint) {
    uint32_t hr, min, sec, msec;
    char tmstp_str[30];

    // Print out timestamp
    msec = MsgToPrint.tick_count;
    hr = msec / 3600000;
    msec -= hr * 3600000;
    min = msec / 60000;
    msec -= min * 60000;
    sec = msec / 1000;
    msec -= sec * 1000;
    sprintf(    tmstp_str,
                " %02ld:%02ld:%02ld::%03ld\t",
                hr, min, sec, msec);
    lzprintf(tmstp_str);

    // Print out callder ID
    lzprintf(caller_list[MsgToPrint.caller_id].caller_name_string);

    // Print out actual msg
    lzprintf(MsgToPrint.msg);

    return;
}

/*******************************************************************************
 * @brief Print out msg instantly - does not use RTOS queue
 * @param[in] void
 * @return void
 *******************************************************************************/
void debug_print_instant(fw_module_id caller_id, const char *fmt, ...) {
    char msg[100];
    va_list args;

    // load formatted string
    va_start(args, fmt);
    vsnprintf(msg, sizeof(msg), fmt, args);
    va_end(args);

    // Insure string always include CR and LF just in case
    msg[sizeof(msg)-3] = '\r';
    msg[sizeof(msg)-2] = '\n';
    msg[sizeof(msg)-1] = '\0';

    lzprintf(msg);

    return;
}

static void lzprintf(char * p_str)
{
    // printf until last char
    do
    {
        debug_uart_tx_byte(p_str);
        p_str++;
    } while ((*p_str != '\0') && (*p_str != '\n'));
    
    if (*p_str != '\0')
    {
        // printf last char
        debug_uart_tx_byte(p_str);
    }
}

// wrapper for sending debug char either to UART or debugger
static void debug_tx_byte(char * pCharToSend)
{
    // this code copied from core_cm3.h
    if ((ITM->TCR & ITM_TCR_ITMENA_Msk) &&      /* ITM enabled */
        (ITM->TER & (1UL << 0)))                /* ITM Port #0 enabled */
    {
        // block until channel available
        while (ITM->PORT[0].u32 == 0);
        ITM->PORT[0].u8 = (uint8_t)*pCharToSend;
    }
    else // send to uart if debugger not attached
    {
        debug_uart_tx_byte((void*)pCharToSend);
    }
}

