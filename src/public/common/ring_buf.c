/*
==========================================================================
 Name        : ring_buf.c
 Project     : pcore
 Path        : /pcore/src/public/common/ring_buf.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 9 Jan 2019
 Description : 
==========================================================================
*/
#include <string.h>
#include "ring_buf.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

#define RB_INDH(rb)                ((rb)->head & ((rb)->count - 1))
#define RB_INDT(rb)                ((rb)->tail & ((rb)->count - 1))

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/* Initialize ring buffer */
int ring_buf_init(RINGBUFF_TYPE *RingBuff, void *buffer, int itemSize, int count)
{
    RingBuff->data = buffer;
    RingBuff->count = count;
    RingBuff->itemSz = itemSize;
    RingBuff->head = RingBuff->tail = 0;

    return 1;
}

/* Insert a single item into Ring Buffer */
int ring_buf_insert(RINGBUFF_TYPE *RingBuff, const void *data)
{
    uint8_t *ptr = RingBuff->data;

    /* We cannot insert when queue is full */
    if (get_ring_buf_is_full(RingBuff))
        return 0;

    ptr += RB_INDH(RingBuff) * RingBuff->itemSz;
    memcpy(ptr, data, RingBuff->itemSz);
    RingBuff->head++;

    return 1;
}

/* Force write a single item into Ring Buffer - if full ditch oldest*/
int ring_buf_force_write_at_back(RINGBUFF_TYPE *RingBuff, const void *data)
{
    uint8_t *ptr = RingBuff->data;

    /* We cannot insert when queue is full - so get rid of oldest*/
    if (get_ring_buf_is_full(RingBuff)){
        RingBuff->tail++;
    }

    ptr += RB_INDH(RingBuff) * RingBuff->itemSz;
    memcpy(ptr, data, RingBuff->itemSz);
    RingBuff->head++;

    return 1;
}

/* Overwrite a single item at the front of the queue*/
int ring_buf_force_write_at_front(RINGBUFF_TYPE *RingBuff, const void *data)
{
    uint8_t *ptr = RingBuff->data;

    // if buffer empty move on head
    // TODO: needs optimising to not scrap tail message if queue not full.
    if( 1 == get_ring_buf_is_empty(RingBuff) ){
        RingBuff->head++;
    }

    // Overwrite tail (oldest message)
    ptr += RB_INDT(RingBuff) * RingBuff->itemSz;
    memcpy(ptr, data, RingBuff->itemSz);

    return 1;
}

/* Insert multiple items into Ring Buffer */
int ring_buf_insert_multi(RINGBUFF_TYPE *RingBuff, const void *data, int num)
{
    uint8_t *ptr = RingBuff->data;
    int cnt1, cnt2;

    /* We cannot insert when queue is full */
    if (get_ring_buf_is_full(RingBuff))
        return 0;

    /* Calculate the segment lengths */
    cnt1 = cnt2 = get_ring_buf_free(RingBuff);
    if (RB_INDH(RingBuff) + cnt1 >= RingBuff->count)
        cnt1 = RingBuff->count - RB_INDH(RingBuff);
    cnt2 -= cnt1;

    cnt1 = MIN(cnt1, num);
    num -= cnt1;

    cnt2 = MIN(cnt2, num);
    num -= cnt2;

    /* Write segment 1 */
    ptr += RB_INDH(RingBuff) * RingBuff->itemSz;
    memcpy(ptr, data, cnt1 * RingBuff->itemSz);
    RingBuff->head += cnt1;

    /* Write segment 2 */
    ptr = (uint8_t *) RingBuff->data + RB_INDH(RingBuff) * RingBuff->itemSz;
    data = (const uint8_t *) data + cnt1 * RingBuff->itemSz;
    memcpy(ptr, data, cnt2 * RingBuff->itemSz);
    RingBuff->head += cnt2;

    return cnt1 + cnt2;
}

/* Pop single item from Ring Buffer */
int ring_buf_pop(RINGBUFF_TYPE *RingBuff, void *data)
{
    uint8_t *ptr = RingBuff->data;

    /* We cannot pop when queue is empty */
    if (get_ring_buf_is_empty(RingBuff))
        return 0;

    ptr += RB_INDT(RingBuff) * RingBuff->itemSz;
    memcpy(data, ptr, RingBuff->itemSz);
    RingBuff->tail++;

    return 1;
}

/* Pop multiple items from Ring buffer */
int ring_buf_pop_multi(RINGBUFF_TYPE *RingBuff, void *data, int num)
{
    uint8_t *ptr = RingBuff->data;
    int cnt1, cnt2;

    /* We cannot insert when queue is empty */
    if (get_ring_buf_is_empty(RingBuff))
        return 0;

    /* Calculate the segment lengths */
    cnt1 = cnt2 = get_ring_buf_count(RingBuff);
    if (RB_INDT(RingBuff) + cnt1 >= RingBuff->count)
        cnt1 = RingBuff->count - RB_INDT(RingBuff);
    cnt2 -= cnt1;

    cnt1 = MIN(cnt1, num);
    num -= cnt1;

    cnt2 = MIN(cnt2, num);
    num -= cnt2;

    /* Write segment 1 */
    ptr += RB_INDT(RingBuff) * RingBuff->itemSz;
    memcpy(data, ptr, cnt1 * RingBuff->itemSz);
    RingBuff->tail += cnt1;

    /* Write segment 2 */
    ptr = (uint8_t *) RingBuff->data + RB_INDT(RingBuff) * RingBuff->itemSz;
    data = (uint8_t *) data + cnt1 * RingBuff->itemSz;
    memcpy(data, ptr, cnt2 * RingBuff->itemSz);
    RingBuff->tail += cnt2;

    return cnt1 + cnt2;
}
