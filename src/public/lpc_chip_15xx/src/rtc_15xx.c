/*
 * @brief LPC15xx RTC chip driver
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2013
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */

#include "chip.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

uint32_t Chip_RTC_Get(void)
{
	return Chip_RTC_GetCount(LPC_RTC);
}

void Chip_RTC_Set(uint32_t Count)
{
	Chip_RTC_SetCount(LPC_RTC, Count);
}

//void Chip_RTC_Reset(void)
//{
//	Chip_RTC_SetCount(LPC_RTC, 0);
//}

bool Chip_RTC_IsRunning(void)
{
	// Check if RTC osc is enabled
	if(LPC_SYSCTL->RTCOSCCTRL != 1)
		return false;
	// Check if CLOCK into RTC is enabled
	if(((LPC_SYSCTL->SYSAHBCLKCTRL[0]) & (1UL << SYSCTL_CLOCK_RTC)) == 0)
		Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_RTC);
	// Check if RTC is enabled
	if((LPC_RTC->CTRL & RTC_CTRL_RTC_EN) == 0)
		return false;
	return true;
}
