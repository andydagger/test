/*
==========================================================================
 Name        : config_lorawan.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/config_lorawan.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 13 Jul 2017
 Description :
==========================================================================
*/

#ifndef PERIPHERAL_LORAWAN_CONFIG_LORAWAN_H_
#define PERIPHERAL_LORAWAN_CONFIG_LORAWAN_H_

/*****************************************************************************
 * Definitions
 ****************************************************************************/

#define UART_LORA_BAUDRATE                                57600
#define UART_LORA_PARAMETERS                            (UART_CFG_DATALEN_8 | UART_CFG_PARITY_NONE | UART_CFG_STOPLEN_1)

// Remapping of lorawan uart port irq handler
//#define UART1_IRQHandler() lorawanhw_irq_handler()

// ----------------------- SYSTEM CONFIG ------------------------------------
#define LORAWAN_END_CHAR '\n'
#define LORAWAN_HOST_JOIN_REQ_LIM 3
#define LORAWAN_RESTORE_SESSION_UPLINK_COUNT_THRESHOLD 10
// Had to extend primary response time-out to take into coms of long
// payloads to the module
#define MCHP_PRIMARY_RESP_TIMEOUT_MS 200
#define MCHP_PRIMARY_SLOW_RESP_TIMEOUT_MS 4000
#define MCHP_SECONDARY_RESP_TIMEOUT_MS 60000*15
// TODO: encounter problems if uplink and check network occur at exact same time.
#define LORAWAN_NETWORK_CHECK_PERIOD_MS 45500

// ----------------------- LORAWAN TIMEOUTS ---------------------------------
#define INIT_TIMEOUT_SEC 60
#define RAND_JOIN_DELAY_TIMEOUT_SEC 40
#define JOIN_NETWORK_TIMEOUT_SEC 300
#define CHECK_NETWORK_TIMEOUT_SEC 20
#define MCAST_CHANGE_TIMEOUT_SEC 10

// Makse sure this is smaller than MCHP_SECONDARY_RESP_TIMEOUT_MS so as to
// catch error by SysMon - second resp time-out may be redundant with
// state-machine time-out faile-safe in place.
#define LORAWAN_SEND_CMD_TIMEOUT_SEC (60*20)

// ----------------------- INIT SM TIMEOUTS ---------------------------------
#define INIT_IDLE_TIMEOUT_SEC -1
#define START_TIMEOUT_SEC 1
#define TRIGGER_HW_RESET_TIMEOUT_SEC 1
#define WAIT_FOR_BOOTUP_TIMEOUT_SEC 3
#define MAC_RESET_TIMEOUT_SEC 10
#define GET_HWEUI_TIMEOUT_SEC 1
#define SETUP_TIMEOUT_SEC 30

// ----------------------- LORAWAN PARAM------------------------------------
// Addresses below point to fixed locations within separate flash
// memory section dedicated to network keys: KeyFLASH 0x1F00 to 0x2000
//#define DEV_EUI "00000000000000D1"//(const char *) 0x1F00
//#define APP_EUI "00000000000000A1"//(const char *) 0x1F11
//#define APP_KEY "000000000000000000000000000000A1"//(const char *) 0x1F22
/*
 * Flask Section: keys, starts from 0xB000 and includes
 * SER_NUM 7 bytes + NULL
 * APP_EUI 16 bytes (ascii) + NULL
 * DEV_EUI 16 bytes (ascii) + NULL
 * APP_KEY 32 bytes (ascii) + NULL
 */

#define PROD_APP_EUI     "8877665544332211"
#define PROD_APP_KEY     "11223344556677881122334455667788"

#ifndef COMPILE_IMAGE
    #define DEV_SER     "UT_001"
    #define APP_EUI     "0102030405060708"
    #define DEV_EUI     "1122334455667788"
    #define APP_KEY     "01020304050607080102030405060708"
#else
    #define DEV_SER     (const char *) 0xB000
    #define APP_EUI     (const char *) 0xB007 // +6+1
    #define DEV_EUI     (const char *) 0xB018 // +8*2+1
    #define APP_KEY     (const char *) 0xB029 // +8*2+1
#endif
#define BUILD_CODE  (const char *) 0xB04A // +16*2+1
#define PWR_HW_REV  (const char *) 0xB050 // +5+1
#define MAIN_HW_REV (const char *) 0xB052 // +1+1
#define RAD_HW_REV  (const char *) 0xB054 // +1+1
#define HW_REV      (const char *) 0xB056 // +1+1

//-------------------------------
// Addresses of calibration coefficients in flash (after key fields)
#define FLASH_ADDR_PWR_VOLT_CALIB_COEFF     (void* const)0xB058
#define FLASH_ADDR_PWR_CUR_CALIB_COEFF      (void* const)0xB05D
#define FLASH_ADDR_PWR_PWR_CALIB_COEFF      (void* const)0xB062

#define FLASH_ADDR_PHOTO_CALIB_COEFF        0xB067
//-------------------------------

#define MCAST_DEV_ADDR "016fe84b"
#define MCAST_NWKSKEY "0b6f6b0d48939e18da0af09d488c2539"
#define MCAST_APPSKEY "cd3311c0e3b15c838dc7468c0b9524d7"
#define DEV_ADDR "00001122"
#define NWKSKEY "12341234123412345678567856785678"
#define APPSKEY "11223344556677889900aabbccddeeff"

#define MCAST_INFO_ON "1,016FE84B,0B6F6B0D48939E18DA0AF09D488C2539,CD3311C0E3B15C838DC7468C0B9524D7"
#define MCAST_INFO_OFF "1,01010101,01010101010101010101010101010101,01010101010101010101010101010101"

/*
 * lora-query -x session add '{"deveui":"1deeafc656cc3d03","dev_addr":"016fe84b","appeui":"00-00-00-00-00-00-00-a0","joineui":"00-99-99-99-00-00-e1-9c","net_id":"000000","app_senc_key":"cd3311c0e3b15c838dc7468c0b9524d7","fnwk_sint_key":"0b6f6b0d48939e18da0af09d488c2539"}'
 * lora-query -x device update 1deeafc656cc3d03 ulc 1
 */
//---------------------------------------------------------
#define UP_ACK_RETRIES        "6"
#define LINK_CHECK_PERIOD    "0" // disabled
#define RECEIVE_DELAY1        "1000"
#define RX2_DR_AND_FREQ        "0 869525000"

// ----------------------- MICROCHIP ------------------------------------
#define MCHP_STATUS_MASK_NETWORKJOINED (1 << 4)
#define MCHP_STATUS_MASK_REJOINNEEDED (1 << 17)

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/



/*typedef union
{
    uint32_t value;
    struct
    {
        unsigned macState :4;                        //determines the state of transmission (rx window open, between tx and rx, etc)
        unsigned networkJoined :1;                   //if set, the network is joined
        unsigned automaticReply :1;                  //if set, ACK and uplink packets sent due to  FPending will be sent immediately
        unsigned adr :1;                             //if set, adaptive data rate is requested by server or application
        unsigned silentImmediately :1;               //if set, the Mac command duty cycle request was received
        unsigned macPause :1;                        //if set, the mac Pause function was called. LoRa modulation is not possible
        unsigned rxDone :1;                          //if set, data is ready for reception
        unsigned linkCheck :1;                       //if set, linkCheck mechanism is enabled
        unsigned channelsModified :1;                //if set, new channels are added via CFList or NewChannelRequest command or enabled/disabled via Link Adr command
        unsigned txPowerModified :1;                 //if set, the txPower was modified via Link Adr command
        unsigned nbRepModified :1;                   //if set, the number of repetitions for unconfirmed frames has been modified
        unsigned prescalerModified :1;               //if set, the prescaler has changed via duty cycle request
        unsigned secondReceiveWindowModified :1;     //if set, the second receive window parameters have changed
        unsigned rxTimingSetup :1;                   //if set, the delay between the end of the TX uplink and the opening of the first reception slot has changed
        unsigned rejoinNeeded :1;                    //if set, the device must be rejoined as a frame counter issue happened
        unsigned mcastEnable :1;                     //if set, the device is in multicast mode and can receive multicast messages
    };
} LorawanStatus_t;

typedef enum
{
    IDLE                      =0,
    TRANSMISSION_OCCURRING      ,
    BEFORE_RX1                  ,         //between TX and RX1, FSK can occur
    RX1_OPEN                    ,
    BETWEEN_RX1_RX2             ,         //FSK can occur
    RX2_OPEN                    ,
    RETRANSMISSION_DELAY        ,         //used for ADR_ACK delay, FSK can occur
    ABP_DELAY                   ,         //used for delaying in calling the join callback for ABP
    CLASS_C_RX2_1_OPEN          ,
    CLASS_C_RX2_2_OPEN          ,
} LoRaMacState_t;*/

#endif /* PERIPHERAL_LORAWAN_CONFIG_LORAWAN_H_ */
