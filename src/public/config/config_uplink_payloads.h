/*
==========================================================================
 Name        : config_uplink_payloads.h
 Project     : pcore
 Path        : /pcore/src/public/config/config_uplink_payloads.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 17 Jul 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_CONFIG_CONFIG_UPLINK_PAYLOADS_H_
#define PUBLIC_CONFIG_CONFIG_UPLINK_PAYLOADS_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "config_dataset.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

//----------------------------------------------------------------------------
// NWK PROTOCOL: 01.62.00 < FIRMWARE VERSION  < 02.00.00
//----------------------------------------------------------------------------

/*
 * All variables are BIG-ENDIAN first and shall be decoded accordingly
 */

//----------------------------------------------------------------------------
// PRODUCTION REPORT
typedef struct __attribute__((packed)){
    uint8_t     msg_type; // 
    uint8_t     pkt_num; // Will always be 0x01
    uint8_t     length_pkts; // Will always be 0x01
	float       calibrationVrms; // calibration voltage V
	float       calibrationIrms; // calibration current A
	float       calibrationPowerFactor; // calibration Power Factor
	float       calibrationPower; // calibration Power W
    uint8_t 	measuredLux; // latest average lux reading
}prod_report_payload_struct;

//----------------------------------------------------------------------------
// FAULT PAYLOAD
typedef struct __attribute__((packed)){
    uint8_t     msg_type;
    uint8_t     pkt_num; // Will always be 0x01
    uint8_t     length_pkts; // Will always be 0x01
    uint32_t    fault_flags; // fault flags listed in sysmon.h
    uint32_t    tmstp; // unix timestamp of the node local time (without DST)
}fault_payload_struct;

//----------------------------------------------------------------------------
// INITIAL PAYLOAD
typedef struct __attribute__((packed)){
    uint8_t     msg_type;
    uint8_t     pkt_num; // Will always be 0x01
    uint8_t     length_pkts; // Will always be 0x01
    uint16_t    fw_ver; // 01.63.00 => 16300
    uint32_t    fault_flags; // fault flags listed in sysmon.h
    uint16_t    luminr_info; // [Driver/Lamp, PhyMin]
    /*
     * MSB: Physical Minimum Brightness level teh driver can achieve in %
     * LSB: DriverType b1:0 = 0: SWITCH, 1: DALI, 2: 0 to 10V, 3: 1 to 10V
     *      LampType b5:2 = 6 for LED (see IEC62386 –102 Table 17)
     */
    uint32_t    tmstp; // unix timestamp of the node local time (without DST)
    uint8_t     gps_accuracy_coeff;
    int32_t     lat_100000;
    int32_t     long_100000;
    uint8_t     dali_gtin[SIZEOF_GTIN];
    uint8_t     dali_ser_num[SIZEOF_SERNO_TYPE_2];
}initial_payload_struct;

//----------------------------------------------------------------------------
// BILLING PAYLOAD
typedef struct __attribute__((packed)){
    uint8_t     msg_type;
    uint8_t     pkt_num; // Will always be 0x01
    uint8_t     length_pkts; // Will always be 0x01
    uint32_t    switch_on_tmstp; // Switch on time as unix timestamp of the node local time (without DST)
    uint32_t    switch_off_tmstp; // Switch off time as unix timestamp of the node local time (without DST)
    int16_t     tiltX_x100; // raw accelerometer reading on x axis // Conical angle = acos[Z/sqrt(X^2+Y^2+Z^2)] 
    int16_t     tiltY_x100; // raw accelerometer reading on y axis with (+/-2048 = +/-2g)
    int16_t     tiltZ_x100; // raw accelerometer reading on z axis with (+/-2048 = +/-2g)
    uint16_t    calendarVersion; // calendar version as in calendar file
    uint32_t    calendarFileRef; // calendar file ref as in calendar file
    uint8_t     calendarProfileId; // profile ID that was being executed as in calendar file
    uint32_t 	accumulatedEnergy_20WhrUnits; // ABSOLUTE accumulated energy in 20Whr units
    uint16_t    vrmsMax_x10; // Maximum Vrms measured since last report (typically 24hours)
    uint16_t    vrmsMin_x10; // Minimum Vrms measured since last report (typically 24hours) 
    uint16_t    vrmsAverage_x10; // Averaged Vrms measured since last report (typically 24hours)
    uint8_t     rebootIdByte; // Reboot Id - Eng use only
    uint8_t     rebootValueByte; // Reboot Value - Eng use only
}daily_payload_struct;

//----------------------------------------------------------------------------
// PERIODIC PAYLOAD
typedef struct __attribute__((packed)){
    uint32_t 	fault_flags; // fault flags listed in sysmon.h
    uint16_t 	instantVrms_x10; // single reading with max latency of 1 minute
    uint16_t 	instantIrms_x100; // single reading with max latency of 1 minute
    uint8_t 	instantPowerFactor_x100; // single reading with max latency of 1 minute
    uint16_t 	instantPower_x10; // single reading with max latency of 1 minute
    uint8_t 	brightnessLevel; // current brightness level executed in % (LOO included)
    uint8_t 	measuredLux; // latest average lux reading as used by calendar stack in lux
}periodic_dataset_struct;

typedef struct __attribute__((packed)){
    uint8_t msg_type;
    uint8_t pkt_num; // Will always be 0x01
    uint8_t length_pkts; // Will always be 0x01
    uint16_t fw_ver; // 01.63.00 => 16300
    uint32_t tmstp; // unix timestamp of the node local time (without DST)
    periodic_dataset_struct current_dataset;
    periodic_dataset_struct previous_dataset;
}periodic_payload_struct;

//----------------------------------------------------------------------------
// MISSED PACKETS PAYLOAD
typedef struct __attribute__((packed)){
    uint8_t     msg_type;
    uint8_t     pkt_num; // Will always be 0x01
    uint8_t     length_pkts; // Will always be 0x01
    char        listOfMissedPackets[94]; // 47 ascii char + framing = 50 bytes max
}rpl_payload_struct;

//----------------------------------------------------------------------------
// CALENDAR CONFIRMATION PAYLAOD
typedef struct __attribute__((packed)){
    uint8_t     msg_type;
    uint8_t     pkt_num; // Will always be 0x01
    uint8_t     length_pkts; // Will always be 0x01
    uint32_t    calendarFileRef; // calendar file ref as in calendar file
    uint16_t    calendarVersion; // calendar version as in calendar file
}conf_cal_payload_struct;

//----------------------------------------------------------------------------
// CALIBRATION REFRENCE DOWNLINK
typedef struct __attribute__((packed)){
    uint8_t     msg_type; // Will always be 0xA1
    uint8_t     pkt_num; // Will always be 0x01
    uint8_t     length_pkts; // Will always be 0x01
    float 	referenceVrms; 
    float 	referenceIrms; 
    float 	referencePowerFactor; 
    float 	referencePower;
    uint8_t 	referenceLux; 
}reference_payload_struct;
//----------------------------------------------------------------------------
// INFO DOWNLINK
typedef struct __attribute__((packed)){
    uint8_t     msg_type; // // Will always be 0xA2
    uint8_t     pkt_num; // Will always be 0x01
    uint8_t     length_pkts; // Will always be 0x01
    char     SerialNumberString[6]; // string length 32 upper hex characters
	char     AppKeyString[32]; // string length 32 upper hex characters
	uint32_t partNumber; // 6810 for F6810
	uint8_t  powerBoardVersion; // Major.Minor 0x1A for v1.10
	uint8_t  mainBoardVersion; // Major.Minor 0x1A for v1.1
	uint8_t  radioBoardVersion; // Major.Minor 0x1A for v1.10
	uint8_t  assemblyVersion;  // Major.Minor 0x1A for v1.10
}info_payload_struct;
//----------------------------------------------------------------------------
// READY UPLINK
typedef struct __attribute__((packed)){
    uint8_t     msg_type; // Will always be 0xAF
    uint8_t     pkt_num; // Will always be 0x01
    uint8_t     length_pkts; // Will always be 0x01
}ready_payload_struct;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/























#endif /* PUBLIC_CONFIG_CONFIG_UPLINK_PAYLOADS_H_ */
