/*
 * config_an_driver.h
 *
 *  Created on: 12 Apr 2018
 *      Author: benraiton_lz
 */

#ifndef PUBLIC_CONFIG_CONFIG_AN_DRIVER_H_
#define PUBLIC_CONFIG_CONFIG_AN_DRIVER_H_

#define AN_DRIVER_PWM_BASE_FREQ		1000
#define ANA_PWM_IDX 				1

#define ANALOG_IN_DETECT_TH_VOLT	((float)4.0)
#define ANALOG_DRIVER_COEF ((float)4.037)

#endif /* PUBLIC_CONFIG_CONFIG_AN_DRIVER_H_ */
