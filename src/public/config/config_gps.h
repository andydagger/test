/*
==========================================================================
 Name        : config_gps.h
 Project     : pcore
 Path        : /pcore/src/public/config/config_gps.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 17 Apr 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_CONFIG_CONFIG_GPS_H_
#define PUBLIC_CONFIG_CONFIG_GPS_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/


// I2C clock is set to 1.8MHz
#define GPS_I2C_CLK_DIVIDER     (40)

// 400KHz I2C bit-rate - going too fast may prevent the salev from responding
//   in time
#define GPS_I2C_BITRATE         (400000)

// Standard I2C mode
#define GPS_I2C_MODE    (0)

// Emulated EEPROM slave addresses
#define GPS_RX       (0x60)
#define GPS_TX       (0x62)


/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

#endif /* PUBLIC_CONFIG_CONFIG_GPS_H_ */
