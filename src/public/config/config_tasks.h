/*
==========================================================================
 Name        : config_tasks.h
 Project     : pcore
 Path        : /pcore/src/public/config/config_tasks.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 23 May 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_CONFIG_CONFIG_TASKS_H_
#define PUBLIC_CONFIG_CONFIG_TASKS_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/

#define NUM_TASKS       7

/*
 * For example, if the stack is 32-bits wide and usStackDepth is
 * passed in as 100, then 400 bytes of stack space will be
 * allocated (100 * 4 bytes).
 */

#define STACKSIZE_LUMINROP      ( (uint16_t) 250)
#define STACKSIZE_CALENDAR      ( (uint16_t) 340)
#define STACKSIZE_PATCHING      ( (uint16_t) 240)
#define STACKSIZE_NWK_COM       ( (uint16_t) 300)
#define STACKSIZE_SENSOR_LFP    ( (uint16_t) 200)
#define STACKSIZE_DEBUG         ( (uint16_t) 200)
#define STACKSIZE_SYS_MON       ( (uint16_t) 200)

/* The highest interrupt priority that can be used by any interrupt service
   routine that makes calls to interrupt safe FreeRTOS API functions.  DO NOT CALL
   INTERRUPT SAFE FREERTOS API FUNCTIONS FROM ANY INTERRUPT THAT HAS A HIGHER
   PRIORITY THAN configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY! (higher priorities
   are lower numeric values. */

#define TASK_PRIORITY_SYS_MON           5 // * * * * *
#define TASK_PRIORITY_NWK_COM           4 // * * * *
#define TASK_PRIORITY_LUMINROP          3 // * * * 
#define TASK_PRIORITY_SENSOR_LFP        3 // * * *
#define TASK_PRIORITY_CALENDAR          2 // * *
#define TASK_PRIORITY_DEBUG             2 // * *
#define TASK_PRIORITY_PATCHING          1 // * *

// RTOS keep tasks priorities lower (and not equal) top configMAX_PRIORITIES

/*
 * The priority to which the subject task is to be set.
 * This is capped automatically to the maximum available priority of
 * (configMAX_PRIORITIES – 1), where configMAX_PRIORITIES is a compile
 * time constant set in the FreeRTOSConfig.h header file
 *
 */

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

#endif /* PUBLIC_CONFIG_CONFIG_TASKS_H_ */
