/*
==========================================================================
 Name        : config_pinout.h
 Project     : pcore
 Path        : /pcore/src/public/config/config_pinout.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 24 Aug 2017
 Description : 
==========================================================================
*/


#ifndef CONFIG_CONFIG_PINOUT_H_
#define CONFIG_CONFIG_PINOUT_H_

//*****************************************************************************
//
// LORA UART & GPIO pins.
//
//*****************************************************************************
#define UART_LORA_IF                                    LPC_USART0
#define UART_LORA_IRQ                                   UART0_IRQn
#define UART_LORA_HANDLER                               UART0_IRQHandler
#define GPIO_LORA_UART_TXD_SWM                          SWM_UART0_TXD_O
#define GPIO_LORA_UART_RXD_SWM                          SWM_UART0_RXD_I
#define GPIO_LORA_UART_RTS_SWM                          SWM_UART0_RTS_O
#define GPIO_LORA_UART_CTS_SWM                          SWM_UART0_CTS_I
#define GPIO_LORA_UART_TXD_PORT                         0
#define GPIO_LORA_UART_TXD_PIN                          29
#define GPIO_LORA_UART_RXD_PORT                         0
#define GPIO_LORA_UART_RXD_PIN                          28
#define GPIO_LORA_UART_RTS_PORT                         2
#define GPIO_LORA_UART_RTS_PIN                          10
#define GPIO_LORA_UART_CTS_PORT                         0
#define GPIO_LORA_UART_CTS_PIN                          26
#define GPIO_LORA_RESET_N_PORT                          2
#define GPIO_LORA_RESET_N_PIN                           11

//*****************************************************************************
//
// BT UART & GPIO pins.
//
//*****************************************************************************
#define UART_BT_IF                                      LPC_USART1
#define UART_BT_IRQ                                     UART1_IRQn
#define UART_BT_HANDLER                                 UART1_IRQHandler
#define GPIO_BT_UART_TXD_SWM                            SWM_UART1_TXD_O
#define GPIO_BT_UART_RXD_SWM                            SWM_UART1_RXD_I
#define GPIO_BT_UART_RTS_SWM                            SWM_UART1_RTS_O
#define GPIO_BT_UART_CTS_SWM                            SWM_UART1_CTS_I
#define GPIO_BT_UART_TXD_PORT                           0
#define GPIO_BT_UART_TXD_PIN                            16
#define GPIO_BT_UART_RXD_PORT                           0
#define GPIO_BT_UART_RXD_PIN                            15
#define GPIO_BT_UART_RTS_PORT                           0
#define GPIO_BT_UART_RTS_PIN                            14
#define GPIO_BT_UART_CTS_PORT                           0
#define GPIO_BT_UART_CTS_PIN                            13
#define GPIO_BT_WAKE_PORT                               1
#define GPIO_BT_WAKE_PIN                                4
#define GPIO_BT_SWAKE_PORT                              1
#define GPIO_BT_SWAKE_PIN                               5
#define GPIO_BT_CMD_PORT                                1
#define GPIO_BT_CMD_PIN                                 27

//*****************************************************************************
//
// STC (soft) UART pins.
//
//*****************************************************************************
#define SCT_UART_TX_SCT_IF                              LPC_SCT0
#define SCT_UART_TX_IRQ                                 SCT0_IRQn
#define SCT_UART_TX_HANDLER                             SCT0_IRQHandler
#define GPIO_SCT_UART_TXD_SWM                           SWM_SCT0_OUT0_O

#define SCT_UART_RX_SCT_IF                              LPC_SCT1
#define SCT_UART_RX_SCT_NUMBER                          1
#define SCT_UART_RX_SCT_INPUT_SOURCE                    SCT1_INMUX_SCTIPU_ABORT
#define SCT_UART_RX_SCTIPU_ABORT_INPUT                  SCTIPU_ABTENA_SCT_ABORT0
#define SCT_UART_RX_IRQ                                 SCT1_IRQn
#define SCT_UART_RX_HANDLER                             SCT1_IRQHandler
#define GPIO_SCT_UART_RXD_SWM                           SWM_SCT_ABORT0_I


//*****************************************************************************
//
// DEBUG UART pins.
//
//*****************************************************************************
#define UART_DEBUG_IF                                   LPC_USART2
#define UART_DEBUG_IRQ                                  UART2_IRQn
#define UART_DEBUG_HANDLER                              UART2_IRQHandler
#define GPIO_DEBUG_UART_TXD_SWM                         SWM_UART2_TXD_O
#define GPIO_DEBUG_UART_RXD_SWM                         SWM_UART2_RXD_I
#define GPIO_DEBUG_UART_TXD_PORT                        2
#define GPIO_DEBUG_UART_TXD_PIN                         7
#define GPIO_DEBUG_UART_RXD_PORT                        2
#define GPIO_DEBUG_UART_RXD_PIN                         6

//*****************************************************************************
//
// LED GPIO pins.
//
//*****************************************************************************
#define GPIO_LED_RED_PORT                               1
#define GPIO_LED_RED_PIN                                30
#define GPIO_LED_RED_POLARITY                           1
#define GPIO_LED_GREEN_PORT                             1
#define GPIO_LED_GREEN_PIN                              31
#define GPIO_LED_GREEN_POLARITY                         1

//*****************************************************************************
//
// REED SWITCH GPIO pin.
//
//*****************************************************************************
#define GPIO_REED_SWITCH_PORT                           0
#define GPIO_REED_SWITCH_PIN                            24

//*****************************************************************************
//
// DALI GPIO pin.
//
//*****************************************************************************
/*
#define TIMER_DALI_IF                                   LPC_SCT2
#define TIMER_DALI_SCT_NUMBER                           2
#define TIMER_DALI_SCT_INPUT_SOURCE                     SCT2_INMUX_SCTIPU_SAMPLE3   // SCT2_IN0 at P1_26 (input SAMPLE_IN_A3)
#define TIMER_DALI_IRQ                                  SCT2_IRQn
#define TIMER_DALI_HANDLER                              SCT2_IRQHandler
*/
// Mark Hooper
//#define TIMER_DALI_IF                                   LPC_RITIMER
//#define TIMER_DALI_IRQ                                  RITIMER_IRQn
//#define TIMER_DALI_HANDLER                              RIT_IRQHandler
#define TIMER_DALI_MRT_IDX								1

#define GPIO_DALI_TX_PORT                               1
#define GPIO_DALI_TX_PIN                                25
#define GPIO_DALI_RX_PORT                               1
#define GPIO_DALI_RX_PIN                                26
#define GPIO_DALI_ENA_PORT                              1
#define GPIO_DALI_ENA_PIN                               24
#define GPIO_UP_SMOOTH_PORT                             1
#define GPIO_UP_SMOOTH_PIN                              23
#define GPIO_UP_HI_CURRENT_PORT                         1
#define GPIO_UP_HI_CURRENT_PIN                          11
// Combined dali/ana hardware
#define GPIO_ANA_PWM_PORT								0
#define GPIO_ANA_PWM_PIN								18
#define GPIO_PWM_FB_PORT								1
#define GPIO_PWM_FB_PIN									2
#define ADC_PWM_FB_CH										4
#define ADC_PWM_FB_SWM                                  SWM_FIXED_ADC1_4
#define GPIO_ANA_P_DET_PORT                             1
#define GPIO_ANA_P_DET_PIN                              3
#define GPIO_ANA_IN_PORT								0
#define GPIO_ANA_IN_PIN									10
#define ADC_AN_DRIV_IN_CH								2
#define ADC_AN_IN_SWM                                   SWM_FIXED_ADC1_2
#define ADC_AN_P_DET_CH                                 5
#define ADC_AN_P_DET_SWM                                SWM_FIXED_ADC1_5
// PWM o/p
#define ANA_PWM_SCT										LPC_SCT0
#define ANA_PWM_SCT_OP									5
#define ANA_PWM_SCT_PIN_FUNC							SWM_FIXED_SCT0_OUT5
#define ADC_SEQ_IRQ										ADC1_SEQA_IRQn
#define ADC_SEQ_IRQ_FLAGS								ADC_FLAGS_SEQA_INT_MASK
#define SDC_EN_INT_FLAG									ADC_INTEN_SEQA_ENABLE

//*****************************************************************************
//
// ADC Voltage, Current Measure & Photo Sensor pins.
//
//*****************************************************************************
#define ADC_PHOTO_IF                                    LPC_ADC1
#define ADC_PHOTO_CH                                    0
#define ADC_PHOTO_SEQ                                   ADC_SEQA_IDX
#define ADC_PHOTO_PORT                                  1
#define ADC_PHOTO_PIN                                   1
#define ADC_PHOTO_SWM                                   SWM_FIXED_ADC1_0
#define ADC_DALI_BUS_IF                                 LPC_ADC1
#define ADC_DALI_BUS_CH                                 2
#define ADC_DALI_BUS_SEQ                                ADC_SEQA_IDX
#define ADC_DALI_BUS_PORT                               0
#define ADC_DALI_BUS_PIN                                10
#define ADC_DALI_BUS_SWM                                SWM_FIXED_ADC1_2
#define ADC_COMMON                                      // Two channels A & B on the same ADC0 or ADC1

//*****************************************************************************
//
//  FLASH SPI & GPIO pins.
//
//*****************************************************************************
#define SPI_FLASH_IF                                    LPC_SPI0
#define SPI_FLASH_IRQ                                   SPI0_IRQn
#define SPI_FLASH_HANDLER                               SPI0_IRQHandler
#define GPIO_FLASH_SPI_MOSI_SWM                         SWM_SPI0_MOSI_IO
#define GPIO_FLASH_SPI_MISO_SWM                         SWM_SPI0_MISO_IO
#define GPIO_FLASH_SPI_SCK_SWM                          SWM_SPI0_SCK_IO
#define GPIO_FLASH_SPI_CS_SWM                           SWM_SPI0_SSELSN_0_IO
#define GPIO_FLASH_SPI_SDI_PORT                         0
#define GPIO_FLASH_SPI_SDI_PIN                          5
#define GPIO_FLASH_SPI_SDO_PORT                         0
#define GPIO_FLASH_SPI_SDO_PIN                          6
#define GPIO_FLASH_SPI_SPC_PORT                         0
#define GPIO_FLASH_SPI_SPC_PIN                          7
#define GPIO_FLASH_SPI_CS_PORT                          0
#define GPIO_FLASH_SPI_CS_PIN                           4
#define GPIO_FLASH_SPI_WP_PORT                          1
#define GPIO_FLASH_SPI_WP_PIN                           15


//*****************************************************************************
//
// ACCELEROMETER SPI & GPIO pins.
//
//*****************************************************************************
#define SPI_ACC_IF                                      LPC_SPI1
#define SPI_ACC_IRQ                                     SPI1_IRQn
#define SPI_ACC_HANDLER                                 SPI1_IRQHandler
#define GPIO_ACC_SPI_SDO_SWM                            SWM_SPI1_MOSI_IO
#define GPIO_ACC_SPI_SDI_SWM                            SWM_SPI1_MISO_IO
#define GPIO_ACC_SPI_SPC_SWM                            SWM_SPI1_SCK_IO
#define GPIO_ACC_SPI_CS_SWM                             SWM_SPI1_SSELSN_0_IO
#define GPIO_CRY_SPI_CS_SWM                             SWM_SPI1_SSELSN_1_IO
#define GPIO_ACC_SPI_SDI_PORT                           1
#define GPIO_ACC_SPI_SDI_PIN                            6
#define GPIO_ACC_SPI_SDO_PORT                           2
#define GPIO_ACC_SPI_SDO_PIN                            2
#define GPIO_ACC_SPI_SPC_PORT                           2
#define GPIO_ACC_SPI_SPC_PIN                            3
#define GPIO_ACC_SPI_CS_PORT                            1
#define GPIO_ACC_SPI_CS_PIN                             7
#define GPIO_CRY_SPI_CS_PORT                            1
#define GPIO_CRY_SPI_CS_PIN                             8
#define GPIO_ACC_INT1_PORT                              2
#define GPIO_ACC_INT1_PIN                               13
#define GPIO_ACC_INT2_PORT                              2
#define GPIO_ACC_INT2_PIN                               12
#define SPI_FLASH_TXCTL_SSEL                            SPI_TXCTL_ASSERT_SSEL0 | SPI_TXCTL_DEASSERT_SSEL1 | SPI_TXCTL_DEASSERT_SSEL2 | SPI_TXCTL_DEASSERT_SSEL3

//*****************************************************************************
//
// RELAY GPIO pin.
//
//*****************************************************************************
#define GPIO_RELAY_CLOSE_PORT                           1
#define GPIO_RELAY_CLOSE_PIN                            28
#define GPIO_RELAY_OPEN_PORT                            1
#define GPIO_RELAY_OPEN_PIN                             29

//*****************************************************************************
//
// WATCHDOG GPIO pin.
//
//*****************************************************************************
#define GPIO_WATCHDOG_PORT                              2
#define GPIO_WATCHDOG_PIN                               0

//*****************************************************************************
//
// POWER METERING pin.
//
//*****************************************************************************
// using UART1 for POWER i/f.
#define UART_POWER_METERING_IF                          LPC_USART1
#define UART_POWER_METERING_IRQ                         UART1_IRQn
#define UART_POWER_METERING_HANDLER                     UART1_IRQHandler
#define GPIO_POWER_METERING_UART_TXD_SWM                SWM_UART1_TXD_O
#define GPIO_POWER_METERING_UART_RXD_SWM                SWM_UART1_RXD_I

#define GPIO_POWER_METERING_UART_TXD_PORT               1
#define GPIO_POWER_METERING_UART_TXD_PIN                21
#define GPIO_POWER_METERING_UART_RXD_PORT               1
#define GPIO_POWER_METERING_UART_RXD_PIN                22

//*****************************************************************************
//
// SCT UART Pins Mappings.
//
//*****************************************************************************
// using SW UART for debug
#define GPIO_SCT_UART_TXD_PORT                          GPIO_DEBUG_UART_TXD_PORT
#define GPIO_SCT_UART_TXD_PIN                           GPIO_DEBUG_UART_TXD_PIN
#define GPIO_SCT_UART_RXD_PORT                          GPIO_DEBUG_UART_RXD_PORT
#define GPIO_SCT_UART_RXD_PIN                           GPIO_DEBUG_UART_RXD_PIN

//*****************************************************************************
//
// GPS GPIO pin.
//
//*****************************************************************************
#define GPIO_GPS_I2C_SCL_PORT							0
#define GPIO_GPS_I2C_SCL_PIN							22
#define GPIO_GPS_I2C_SDA_PORT							0
#define GPIO_GPS_I2C_SDA_PIN							23
#define GPIO_GPS_1PPS_PORT								0
#define GPIO_GPS_1PPS_PIN								0
#define GPIO_GPS_RST_PORT								0
#define GPIO_GPS_RST_PIN								1
#define GPIO_GPS_WAKEUP_PORT							0
#define GPIO_GPS_WAKEUP_PIN								30
#define GPIO_GPS_ON_PORT								1
#define GPIO_GPS_ON_PIN									0
#define GPS_I2C_IF										LPC_I2C0
#define GPS_I2C_IRQ										I2C0_IRQn
#define GPS_IRQ_HANDLER									I2C0_IRQHandler
#define GPS_I2C_SCL_FIXED_PIN							SWM_FIXED_I2C0_SCL
#define GPS_I2C_SDA_FIXED_PIN							SWM_FIXED_I2C0_SDA

#endif /* CONFIG_CONFIG_PINOUT_H_ */












