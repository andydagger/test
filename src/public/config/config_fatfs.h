/*
==========================================================================
 Name        : config_fatfs.h
 Project     : pcore
 Path        : /pcore/src/public/config/config_fatfs.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 12 Feb 2018
 Description : 
==========================================================================
*/



#ifndef PUBLIC_CONFIG_CONFIG_FATFS_H_
#define PUBLIC_CONFIG_CONFIG_FATFS_H_

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/
#define CAL_PATCH_FILE_NAME     "cal_ptch.bin"
#define CAL_OLD_FILE_NAME       "cal_old.bin"
#define CAL_NEW_FILE_NAME       "cal_new.bin"
#define FW_PATCH_FILE_NAME      "fw_patch.bin"
#define FW_OLD_FILE_NAME        "fw_old.bin"
#define FW_NEW_FILE_NAME        "fw_new.bin"
#define BOOT_LOG_FILE_NAME      "boot.log"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

#endif /* PUBLIC_CONFIG_CONFIG_FATFS_H_ */
