/*
==========================================================================
 Name        : config_debug.h
 Project     : pcore
 Path        : /pcore/src/public/config/config_debug.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 24 Aug 2017
 Description : 
==========================================================================
*/


#ifndef CONFIG_CONFIG_DEBUG_H_
#define CONFIG_CONFIG_DEBUG_H_


/*****************************************************************************
 * Definitions
 ****************************************************************************/
#define DEBUG_BAUD_RATE 115200
#define DEBUG_COM_PARAMETERS (UART_CFG_DATALEN_8 | UART_CFG_PARITY_NONE | UART_CFG_STOPLEN_1)

#define DEBUG_QUEUE_WIDTH (NETWORK_STRING_MAX_LENGTH + 30)
#define DEBUG_QUEUE_DEPTH 20
#define DEBUG_STR_END_CHAR '\n'

#endif /* CONFIG_CONFIG_DEBUG_H_ */
