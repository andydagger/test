/*
==========================================================================
 Name        : config_queues.h
 Project     : pcore
 Path        : /pcore/src/public/config/config_queues.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 23 May 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_CONFIG_CONFIG_QUEUES_H_
#define PUBLIC_CONFIG_CONFIG_QUEUES_H_

#include "stdbool.h"
//#include "debug.h"
#include "config_network.h"

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/

//------------------------------------------------------------
//  DEBUG MSG CALLER IDs
typedef enum{
    BOOT,
    MAIN,
    LORA,
    LORAINIT,
    LORAMAC,
    LORA_TX,
    LORA_RX,
    LUMINROP,
    DALI,
    NWK,
    FFS,
    FLASH,
    PATCH_RX,
    PATCH_PRO,
    LUMINR,
    CALEN,
    RTC,
    PWR,
    GPS,
    FILESYS,
    SYSMON,
    SENSOR,
    COMSNG,
    PROD,
    NVM
}fw_module_id;


//-----------------------------------------------------------------------------
// Task IDs
typedef enum{
	TASK_LUMINR, // 0
	TASK_CAL, // 1
	TASK_PATCHING, // 2
	TASK_NWK_COM, // 3
	TASK_SENSOR_LFP, // 4
	TASK_DEBUG, // 5
    TASK_SYS_MON // 6
}task_id;

//-----------------------------------------------------------------------------
// Debug Queue
#define QU_DEPTH_DEBUG		30
typedef struct __attribute__((packed))
{
    fw_module_id caller_id;
	uint32_t tick_count;
	char msg[50];
}QuDebugMsg;

//-----------------------------------------------------------------------------
// Error Report Queue
#define QU_DEPTH_ERROR_REPORT   10
typedef struct __attribute__((packed))
{
    fw_module_id source_fw_module;
    uint32_t code;
    uint32_t tmstp;
}QuErrorReportMsg;

//-----------------------------------------------------------------------------
// Patch Data Queue
#define QU_DEPTH_PATCH_DATA    3
typedef struct __attribute__((packed))
{
    task_id source_task_id;
    task_id target_task_id;
    uint8_t msg_type;
    uint32_t pkt_number;
    uint32_t message_length;
    uint8_t content_byte_count;
    char data[DOWN_MSG_PAYLOAD_ASCII_WITH_TERM];
}QuPatchDataMsg;

//-----------------------------------------------------------------------------
// Nwk Com Queue
#define NO_DELAY    0
#define LOW_PRIORITY false
#define HIGH_PRIORITY true
typedef enum{
    QOS_0,
    QOS_1,
    QOS_2
}uplink_qos_type;
#define QU_DEPTH_NWK_COM		2
typedef struct __attribute__((packed))
{
	char payload[UP_MSG_PAYLOAD_ASCII_WITH_TERM];
	uint32_t delay_sec;
	bool b_is_high_priority;
	uplink_qos_type qos;
}QuNwkComMsg;


//-----------------------------------------------------------------------------
// Luminr Op Queue
#define QU_DEPTH_LUMINR_OP   2
typedef struct __attribute__((packed))
{
    task_id  source_task_id;
    uint8_t  lamp_output_level;
    uint32_t lamp_start_time_utc;
    uint32_t lamp_stop_time_utc;
}QuLuminrOpMsg;

//-----------------------------------------------------------------------------
// Process Patch Queue
#define QU_DEPTH_PROCESS_PATCH   2
typedef struct __attribute__((packed))
{
    char patch_file_name[20];
}QuProcessPatchgMsg;

//-----------------------------------------------------------------------------
// Tilt Queue
#define QU_DEPTH_TILT_DET   2
typedef struct __attribute__((packed))
{
    bool b_not_vertical;
}QuTiltDetMsg;


/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

#endif /* PUBLIC_CONFIG_CONFIG_QUEUES_H_ */
