/*
==========================================================================
 Name        : config_timers.h
 Project     : pcore
 Path        : /pcore/src/public/config/config_timers.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 29 May 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_CONFIG_CONFIG_TIMERS_H_
#define PUBLIC_CONFIG_CONFIG_TIMERS_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/

#include "config_network.h"

// SYSTEM
#define SYSTICK_MRT_CH  0
#define DALI_MRT_CH  TIMER_DALI_MRT_IDX
#define ADC_MRT_CH  2

// RTOS
#define NUM_TIMERS 5

// MCAST Time-Out set to 3 hours
#define BCAST_TIME_OUT_MS   (uint32_t)pdMS_TO_TICKS_LZ(60000*60*3)

// number of ticks(ms) to block when changing the status period timer
#define CHANGE_TIMER_BLOCK_TICKS  100
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
enum rtos_timers{
    BCAST_TIME_OUT,
    STATUS_UPLINK,
    RTOS_TIMERS_MAX
};

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

#endif /* PUBLIC_CONFIG_CONFIG_TIMERS_H_ */
