/*
==========================================================================
 Name        : config_dataset.h
 Project     : pcore
 Path        : /pcore/src/public/config/config_dataset.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 16 Jul 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_CONFIG_CONFIG_DATASET_H_
#define PUBLIC_CONFIG_CONFIG_DATASET_H_


/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include <stdbool.h>
#include <stdint.h>

#define SIZEOF_GTIN			6
#define SIZEOF_SERNO_TYPE_2	8
#define SIZEOF_SERNO_TYPE_1 4

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

//----------------------------------------------------------------------------
// Dataset from Power Calc
/*
 * The dataset will hold values as accurate as the selected technology can
 * provide. It will be for compileUplink to multiply and typecast as required.
 */
typedef struct{
	float    instantVrms; // instant voltage V
	float    instantIrms; // instant current A
	float    instantPowerFactor; // instant Power Factor
	float    instantPower; // instant Power W
	uint32_t accumulatedEnergy_20WhrUnits; // ABSOLUTE Accumulated Energy in 20Whr Units
	float    vrmsMax; // Maximum Vrms over 24hrs in V
	float    vrmsMin; // Minimum Vrms over 24hrs in V
	float    vrmsAverage; // Average Vrms over 24hrs in V
}PowerCalcDataset;

//----------------------------------------------------------------------------
// Dataset from Billing
typedef struct{
    uint32_t    switch_on_tmstp; /* unix time on switch-point was satisfied UTC */
    uint32_t    switch_off_tmstp; /* unix time off switch-point was satisfied UTC */
    uint16_t    calendarVersion;
    uint32_t    calendarFileRef;
    uint8_t     calendarProfileId;
}BillingDataset;

//----------------------------------------------------------------------------
// Dataset from Tilt
typedef struct{
    uint16_t angle_x_100; /* angle vs x azis x 100 */
    uint16_t angle_y_100; /* angle vs y azis x 100 */
    uint16_t angle_z_100; /* angle vs z azis x 100 */
    bool b_column_is_vertical; /* column status */
    uint8_t tilt_threshold; /* column integrity threshold */
}TiltDataset;

//----------------------------------------------------------------------------
// Dataset from GPS
typedef struct{
    int32_t latitude; /* float x 100,000 */
    int32_t longitude; /* float x 100,000 */
    uint8_t accuracy; /* 1 for low and 2 for high accuracy */
    bool    b_data_ready;
}GpsDataset;

//----------------------------------------------------------------------------
// Dataset from Luminaire
/*
 * Luminr Info
 * Bit 0:1 DRIVER_TYPE
 * Bit 2:5 LAMP_TYPE
 * Bit 8:15 PHY_MIN
 */
typedef struct{
    uint8_t driver_and_lamp_type; /*  */
    uint8_t phy_min; /*  */
}luminr_info_struct;

typedef struct{
    bool b_is_active;
    uint8_t level; /*  */
    uint32_t start_time_unix; /*  */
    uint32_t end_time_unix; /*  */
}luminr_override_struct;

typedef struct{
    luminr_info_struct info; /*  */
    uint8_t gtin[SIZEOF_GTIN]; /* SIZEOF_GTIN */
    uint8_t serno[SIZEOF_SERNO_TYPE_2]; /* SIZEOF_SERNO_TYPE_2 */
    bool        b_data_is_ready;
}LuminrDataset;

//----------------------------------------------------------------------------
// Dataset from Sensor
typedef struct{
    uint16_t col_temp; /*  */
    bool b_door_is_open; /*  */
}SensorDataset;

//----------------------------------------------------------------------------
// Dataset from Solar Clock
typedef struct{
    uint32_t solar_sun_rise; /*  */
    uint32_t solar_sun_set; /*  */
}SolarClockDataset;

//----------------------------------------------------------------------------
// INFO DOWNLINK
typedef struct{
    char     SerialNumberString[6]; // serial number 6 upper characters
	char     AppKeyString[32]; // string length 32 upper hex characters
	uint16_t partNumber; // 6810 for F6810
	uint8_t  powerBoardVersion; // Major.Minor 0x1A for v1.10
	uint8_t  mainBoardVersion; // Major.Minor 0x1A for v1.1
	uint8_t  radioBoardVersion; // Major.Minor 0x1A for v1.10
	uint8_t  assemblyVersion;  // Major.Minor 0x1A for v1.10
}nodeInfoDataset;


/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

#endif /* PUBLIC_CONFIG_CONFIG_DATASET_H_ */
