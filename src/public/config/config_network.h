/*
==========================================================================
 Name        : config_network.h
 Project     : pcore
 Path        : /pcore/src/public/config/config_network.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 7 Sep 2017
 Description : 
==========================================================================
*/


#ifndef PUBLIC_CONFIG_CONFIG_NETWORK_H_
#define PUBLIC_CONFIG_CONFIG_NETWORK_H_

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/
#include <stdint.h>
#include <stddef.h>

//----------------------------------------------------------------------------
// NWK PROTOCOL VERSION 0.10 11th October 18
//----------------------------------------------------------------------------

//------------------------------
//      UPLINKS
//------------------------------
#define PERIODIC_MSG        0x11
#define DAILY_MSG           0xB1
#define FAULT_MSG           0xE0
#define ERROR_MSG           0xE1
#define INITIAL_MSG         0x10
#define PATCH_RPL_ID        0xDA
#define PATCH_RPL_ID_V2     0xDC
#define CAL_UPD_ACK         0xDB
#define PROD_REPORT         0xA0
#define PROD_DONE_MSG       0xAF
//------------------------------

//------------------------------
//      DOWNLINKS
//------------------------------
#define PATCH_RX_FUT        0xD0
#define PATCH_RX_PCP        0xD1
#define PATCH_RX_PRT        0xD2
#define PATCH_RX_CUT        0xD3
#define PATCH_RX_PTP        0xDF
#define TIME_SYNC_CORR      0x21
#define SETUP_TILT          0x22
#define SETUP_COORD         0x23
#define LUMINR_OVERRIDE     0x24
#define SYS_RESET           0xFF
//      Engineering Extras
#define FAT_DRIVE_RESET     0xF1
#define FORCE_REJOIN        0xF2
#define GET_STATUS_REQ      0xF3
#define DRIVER_RESET        0xF4
#define SET_STATUS_PERIOD   0xF5
// FUOTA test jig
#define FLAKEY_NODE         0xDD
#define CRASH_NODE          0xDE
// Production
#define PROD_REF_CAL_DATA   0xA1
#define PROD_NODE_DATA      0xA2
//------------------------------


// Due to ascii format and mac_rx_portid_...
// "mac_rx 166 " 11 byes
// payload 51 bytes x2
// \r\n\0 +3
// total 115

#define UP_MSG_PAYLOAD (50)
// + \r\n
#define UP_MSG_PAYLOAD_ASCII_NO_TERM (UP_MSG_PAYLOAD*2)
#define UP_MSG_PAYLOAD_ASCII_WITH_TERM (UP_MSG_PAYLOAD*2 + 2)
// + [mac tx uncnf 123 ]
#define UP_MSG_MICROCHIP_STRING_MAX (UP_MSG_PAYLOAD_ASCII_WITH_TERM + 17)

#define DOWN_MSG_PAYLOAD (50)
// +\r\n
#define DOWN_MSG_PAYLOAD_ASCII_WITH_TERM (DOWN_MSG_PAYLOAD*2 + 2)
// + [mac rx 123 ]
#define DOWN_MSG_MICROCHIP_STRING_MAX (DOWN_MSG_PAYLOAD_ASCII_WITH_TERM + 11)

#define NETWORK_INITIAL_MESSAGE_DELAY_SEC     5

#define NETWORK_BROADCAST_SESSION_TIMEOUT_SEC 60
// period is specified in minutes, e.g. 30 mins will produce
// a 120s jitter, 60 min 240s jitter, (mins * 4) = seconds jitter
#define NETWORK_UPLINK_JITTER_MULTIPLIER      4

#define LAST_GASP_TIME_OUT_SEC  120


#define JOIN_REQ_AT_SAME_DR_LIMIT 3
#define INTER_JOIN_REQ_DELAY_SEC 120

#define NWK_HW_EUI_LENGTH 16

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
typedef struct
{
    uint8_t msg_type;
    uint32_t pkt_number;
    uint32_t msg_length;
    uint8_t content_byte_count;
    char pkt_content[DOWN_MSG_PAYLOAD];
}pkt_format;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/


#endif /* PUBLIC_CONFIG_CONFIG_NETWORK_H_ */
