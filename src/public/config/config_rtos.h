/*
==========================================================================
 Name        : config_rtos.h
 Project     : pcore
 Path        : /pcore/src/public/config/config_rtos.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 15 Feb 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_CONFIG_CONFIG_RTOS_H_
#define PUBLIC_CONFIG_CONFIG_RTOS_H_

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/
/*
 * For example, if the stack is 32-bits wide and usStackDepth is
 * passed in as 100, then 400 bytes of stack space will be
 * allocated (100 * 4 bytes).
 */
#define STACKSIZE_PRINT_DEBUG       ( (uint16_t) 110)
#define STACKSIZE_GENERIC_SERVICING ( (uint16_t) 220)
#define STACKSIZE_NWK_JOBS          ( (uint16_t) 300 )
#define STACKSIZE_RX_APP_MSG        ( (uint16_t) 210 )
#define STACKSIZE_PER_UPLINK        ( (uint16_t) 200 )
#define STACKSIZE_LUMINROP          ( (uint16_t) 300 )
#define STACKSIZE_SYS_MON           ( (uint16_t) 250 )
#define STACKSIZE_PATCHING          ( (uint16_t) 400 )

/* The highest interrupt priority that can be used by any interrupt service
   routine that makes calls to interrupt safe FreeRTOS API functions.  DO NOT CALL
   INTERRUPT SAFE FREERTOS API FUNCTIONS FROM ANY INTERRUPT THAT HAS A HIGHER
   PRIORITY THAN configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY! (higher priorities
   are lower numeric values. */
#define TASK_PRIORITY_NWK_JOBS              (tskIDLE_PRIORITY + 4)

#define TASK_PRIORITY_RX_APP_MSG            (tskIDLE_PRIORITY + 3)
#define TASK_PRIORITY_PER_UPLINK            (tskIDLE_PRIORITY + 3)
#define TASK_PRIORITY_LUMINROP              (tskIDLE_PRIORITY + 3)

#define TASK_PRIORITY_PRINT_DEBUG           (tskIDLE_PRIORITY + 2)

#define TASK_PRIORITY_GENERIC_SERVICING     (tskIDLE_PRIORITY + 1)
#define TASK_PRIORITY_SYS_MON               (tskIDLE_PRIORITY + 1)
#define TASK_PRIORITY_PATCHING              (tskIDLE_PRIORITY + 1)

// RTOS keep tasks priorities lower (and not equal) top configMAX_PRIORITIES

/*
 * The priority to which the subject task is to be set.
 * This is capped automatically to the maximum available priority of
 * (configMAX_PRIORITIES – 1), where configMAX_PRIORITIES is a compile
 * time constant set in the FreeRTOSConfig.h header file
 *
 */

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/


/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/



#endif /* PUBLIC_CONFIG_CONFIG_RTOS_H_ */
