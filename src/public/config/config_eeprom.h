/*
 * config_eeprom.h
 *
 *  Created on: 20 Apr 2021
 *      Author: benraiton
 */

#ifndef PUBLIC_CONFIG_CONFIG_EEPROM_H_
#define PUBLIC_CONFIG_CONFIG_EEPROM_H_

/******************************************************************************
 * The EEprom is an on chip 4kB EEprom accessed via the IAP module.
 * the read/write functions are part of the LPC15XX abstraction layer library
 * So defined in HW.C/.H
******************************************************************************/
#define NVM_CRC16_SEED                    0xFFFF
// Addresses for the different sections of storage, padding should be used for
// future expansion of structures and sections placed on logical boundaries.
// start address must be above the area used by the legacy EEprom layout
#define NVM_ADDRESS_ERROR_CODE            0x0000   // ( 5 bytes)
#define NVM_ADDRESS_METERING_CALIB_COEF   0x0010   // ( 16 bytes)
#define NVM_ADDRESS_LAMP_OUTPUT_OVERRIDE  0x0030   // ( 13 bytes)
#define NVM_ADDRESS_DALI_ADDRESSES        0x0050   // ( 11 bytes)
#define NVM_ADDRESS_RUNTIME_PARAMS        0x0080   // ( 4 bytes)

/******************************************************************************
 * Error code structure used for storing the last error code ID and its
 * associated value
******************************************************************************/

#define NVM_ERROR_CODE_STRUCT_VER     0x01
// default values
#define NVM_DEFAULT_ERROR_CODE_ID     0
#define NVM_DEFAULT_ERROR_CODE_VALUE  0

// current length in bytes = 5
typedef struct  __attribute__((packed)) __Tag_NVM_errorCode
{
    uint8_t  version;
    uint8_t  id;
    uint8_t  value;
    uint16_t crc;

} NVM_errorCode_t;

/******************************************************************************
 * Metering Calibration Coefficients used by stm8 and Microchip types
*******************************************************************************/

#define NVM_METERING_CALIB_COEF_STRUCT_VER  0x01
// default values
#define NVM_DEFAULT_METERING_CALIB_PASSED   0
#define NVM_DEFAULT_METERING_CALIB_VOLTAGE  0.0f
#define NVM_DEFAULT_METERING_CALIB_CURRENT  0.0f
#define NVM_DEFAULT_METERING_CALIB_POWER    0.0f

// current length in bytes = 15
typedef struct __attribute__((packed))
{
    uint8_t  version;
    uint8_t  calibPassed;
    float    voltage;
    float    current;
    float    power;
    uint16_t crc;

} NVM_meteringCalibCoef_t;


/******************************************************************************
 * LOO -> Lamp Output Override message storage
******************************************************************************/

#define NVM_LAMP_OUTPUT_OVERRIDE_STRUCT_VER   0x01
// default values
#define NVM_DEFAULT_LOO_STATE         0   // this equal to disabled
#define NVM_DEFAULT_LOO_OUTPUT_LEVEL  0
#define NVM_DEFAULT_LOO_START_TIME    0
#define NVM_DEFAULT_LOO_STOP_TIME     0

// current length in bytes = 13
typedef struct __attribute__((packed))
{
    uint8_t   version;
    uint8_t   state;
    uint8_t   outputLevel;
    uint32_t  startTimeUtc;
    uint32_t  stopTimeUtc;
    uint16_t  crc;

} NVM_lampOutputOverride_t;


/******************************************************************************
 * Dali Addresses struct, addresses are allocated via 64 bit mask
******************************************************************************/

#define NVM_DALI_ADDRESSES_STRUCT_VER   0x01
// default values
#define NVM_DEFAULT_DALI_ADDRESSES_MASK  0

typedef struct __attribute__((packed))
{
    uint8_t  version;
    uint64_t addressMask;
    uint16_t crc;

} NVM_DaliAddresses_t;

/******************************************************************************
 * runtime params struct
******************************************************************************/

#define NVM_RUNTIME_PARAMS_STRUCT_VER   0x01
// default values
#define NVM_DEFAULT_STATUS_PERIOD       30 // 30 Minutes

typedef struct __attribute__((packed))
{
    uint8_t  version;
    uint8_t  status_period;   // status reporting period in minutes
    uint16_t crc;

} NVM_RuntimeParams_t;

#endif /* PUBLIC_CONFIG_CONFIG_EEPROM_H_ */
