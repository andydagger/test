/*
==========================================================================
 Name        : config_patch.h
 Project     : pcore
 Path        : /pcore/src/public/config/config_patch.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 12 Feb 2018
 Description : 
==========================================================================
*/



#ifndef PUBLIC_CONFIG_CONFIG_PATCH_H_
#define PUBLIC_CONFIG_CONFIG_PATCH_H_


/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/
#define BYTES_PER_PKT   45
#define PATCH_INFO_SIZE BYTES_PER_PKT
#define MAX_RPL_DELAY   (60*60*2)// limit was only 60 sec now is 2hrs

// 512 x 8 x 45 = 184,320
// 8 PCP encoding per byte
// 45 bytes of patch data per packet

// rpl of 512 bytes max
#define RPL_STORAGE_SIZE        512
// 8 pcp flags per rpl byte = 4096 packets
#define PATCH_MAX_PACKETS       (RPL_STORAGE_SIZE * 8)
// 45 bytes of data per pcp = 184,320 bytes
#define PATCH_MAX_LENGTH_BYTES  (PATCH_MAX_PACKETS * BYTES_PER_PKT)
// limit image size to 200kB
#define FILE_MAX_LENGTH_BYTES   200000

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

#endif /* PUBLIC_CONFIG_CONFIG_PATCH_H_ */
