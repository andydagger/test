/*
==========================================================================
 Name        : hw.c
 Project     : pcore
 Path        : /pcore/src/public/hardware/hw.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 24 Aug 2017
 Description :  Initialisation of the processor.
                No Chip_...() calls shall be  made outside hw.c and hw.h,
                except via the use of macros.
==========================================================================
*/

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "config_pinout.h"
#include "config_debug.h"
#include "config_lorawan.h"
#include "config_dali.h"
#include "config_flash.h"
#include "hw.h"
#include "common.h"
#include "debug.h"
#include "systick.h"
#include "irq.h"
#include "config_an_driver.h"
#include "config_photo.h"
#include "config_gps.h"
#include "config_timers.h"
#include "config_power.h"

#include "config_dataset.h"
#include "nwk_tek.h"
#include "sysmon.h"
#include "photo.h"
#include "powerMonitor.h"
#include "stm8/stm8Metering.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
typedef struct{
    uint8_t pin_port;
    uint8_t pin_number;
    bool b_is_input;
    bool output_state;
}pinout_struct;

static const pinout_struct pcore_pinout_matrix[] =
{
    { GPIO_LORA_UART_TXD_PORT,  GPIO_LORA_UART_TXD_PIN,  IO_OUT, IO_LOW},
    { GPIO_LORA_UART_RXD_PORT,  GPIO_LORA_UART_RXD_PIN,  IO_IN,  IO_LOW},
    { GPIO_LORA_UART_RTS_PORT,  GPIO_LORA_UART_RTS_PIN,  IO_OUT,  IO_LOW},
    { GPIO_LORA_UART_CTS_PORT,  GPIO_LORA_UART_CTS_PIN,  IO_IN,  IO_LOW},
    { GPIO_LORA_RESET_N_PORT,   GPIO_LORA_RESET_N_PIN,   IO_OUT,  IO_LOW},
    { GPIO_BT_UART_TXD_PORT,    GPIO_BT_UART_TXD_PIN,    IO_IN,  IO_LOW},
    { GPIO_BT_UART_RXD_PORT,    GPIO_BT_UART_RXD_PIN,    IO_IN,  IO_LOW},
    { GPIO_BT_UART_RTS_PORT,    GPIO_BT_UART_RTS_PIN,    IO_IN,  IO_LOW},
    { GPIO_BT_UART_CTS_PORT,    GPIO_BT_UART_CTS_PIN,    IO_IN,  IO_LOW},
    { GPIO_BT_WAKE_PORT,        GPIO_BT_WAKE_PIN,        IO_OUT, IO_HIGH},
    { GPIO_BT_SWAKE_PORT,       GPIO_BT_SWAKE_PIN,       IO_OUT, IO_HIGH},
	{ GPIO_BT_CMD_PORT,         GPIO_BT_CMD_PIN,         IO_IN, IO_LOW},
    { GPIO_DEBUG_UART_TXD_PORT, GPIO_DEBUG_UART_TXD_PIN, IO_OUT, IO_LOW},
    { GPIO_DEBUG_UART_RXD_PORT, GPIO_DEBUG_UART_RXD_PIN, IO_IN, IO_LOW},
    { GPIO_LED_RED_PORT,        GPIO_LED_RED_PIN,        IO_OUT, IO_LOW},
    { GPIO_LED_GREEN_PORT,      GPIO_LED_GREEN_PIN,      IO_OUT, IO_LOW},
    { GPIO_REED_SWITCH_PORT,    GPIO_REED_SWITCH_PIN,    IO_IN, IO_LOW},
    { GPIO_DALI_TX_PORT,        GPIO_DALI_TX_PIN,        IO_OUT, IO_LOW},
    { GPIO_DALI_RX_PORT,        GPIO_DALI_RX_PIN,        IO_IN, IO_LOW},
    { GPIO_DALI_ENA_PORT,       GPIO_DALI_ENA_PIN,       IO_OUT, IO_LOW},
    { GPIO_UP_SMOOTH_PORT,      GPIO_UP_SMOOTH_PIN,      IO_OUT, IO_LOW},
    { GPIO_UP_HI_CURRENT_PORT,  GPIO_UP_HI_CURRENT_PIN,  IO_IN, IO_LOW},
    { GPIO_ANA_PWM_PORT,  		GPIO_ANA_PWM_PIN,		 IO_OUT, IO_HIGH},
    { GPIO_PWM_FB_PORT,  		GPIO_PWM_FB_PIN,		 IO_IN, IO_LOW},
    { GPIO_ANA_IN_PORT,  		GPIO_ANA_IN_PIN,		 IO_IN, IO_LOW},
    { GPIO_ANA_P_DET_PORT, 		GPIO_ANA_P_DET_PIN,		 IO_IN, IO_LOW},
    { ADC_PHOTO_PORT,           ADC_PHOTO_PIN,           IO_IN, IO_LOW},
    { ADC_DALI_BUS_PORT,        ADC_DALI_BUS_PIN,        IO_IN, IO_LOW},
    { GPIO_FLASH_SPI_SDI_PORT,  GPIO_FLASH_SPI_SDI_PIN,  IO_IN, IO_LOW},
    { GPIO_FLASH_SPI_SDO_PORT,  GPIO_FLASH_SPI_SDO_PIN,  IO_IN, IO_LOW},
    { GPIO_FLASH_SPI_SPC_PORT,  GPIO_FLASH_SPI_SPC_PIN,  IO_IN, IO_LOW},
    { GPIO_FLASH_SPI_CS_PORT,   GPIO_FLASH_SPI_CS_PIN,   IO_IN, IO_LOW},
    { GPIO_FLASH_SPI_WP_PORT,   GPIO_FLASH_SPI_WP_PIN,   IO_OUT, IO_LOW},
    { GPIO_ACC_SPI_SDI_PORT,    GPIO_ACC_SPI_SDI_PIN,    IO_IN, IO_LOW},
    { GPIO_ACC_SPI_SDO_PORT,    GPIO_ACC_SPI_SDO_PIN,    IO_IN, IO_LOW},
    { GPIO_ACC_SPI_SPC_PORT,    GPIO_ACC_SPI_SPC_PIN,    IO_IN, IO_LOW},
    { GPIO_ACC_SPI_CS_PORT,     GPIO_ACC_SPI_CS_PIN,     IO_IN, IO_LOW},
    { GPIO_CRY_SPI_CS_PORT,     GPIO_CRY_SPI_CS_PIN,     IO_IN, IO_LOW},
    { GPIO_ACC_INT1_PORT,       GPIO_ACC_INT1_PIN,       IO_IN, IO_LOW},
    { GPIO_ACC_INT2_PORT,       GPIO_ACC_INT2_PIN,       IO_IN, IO_LOW},
    { GPIO_RELAY_CLOSE_PORT,    GPIO_RELAY_CLOSE_PIN,    IO_OUT, IO_LOW},
    { GPIO_RELAY_OPEN_PORT,     GPIO_RELAY_OPEN_PIN,     IO_OUT, IO_LOW},
    { GPIO_WATCHDOG_PORT,       GPIO_WATCHDOG_PIN,       IO_OUT, IO_LOW},
    { GPIO_POWER_METERING_UART_TXD_PORT, GPIO_POWER_METERING_UART_TXD_PIN, IO_OUT, IO_LOW},
    { GPIO_POWER_METERING_UART_RXD_PORT, GPIO_POWER_METERING_UART_RXD_PIN, IO_IN, IO_LOW},
    { GPIO_GPS_I2C_SCL_PORT, 	GPIO_GPS_I2C_SCL_PIN, 	IO_OUT, IO_LOW},
    { GPIO_GPS_I2C_SDA_PORT, 	GPIO_GPS_I2C_SDA_PIN, 	IO_OUT, IO_LOW},
    { GPIO_GPS_1PPS_PORT, 		GPIO_GPS_1PPS_PIN, 		IO_IN, IO_LOW},
    { GPIO_GPS_RST_PORT, 		GPIO_GPS_RST_PIN, 		IO_OUT, IO_HIGH},
    { GPIO_GPS_WAKEUP_PORT, 	GPIO_GPS_WAKEUP_PIN, 	IO_IN, IO_LOW},
    { GPIO_GPS_ON_PORT, 		GPIO_GPS_ON_PIN, 		IO_OUT, IO_LOW},
};

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
/* System oscillator rate and RTC oscillator rate */
const uint32_t OscRateIn = 12000000;
const uint32_t RTCOscRateIn = 32768;

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void hw_setupmuxing(void);
static void hw_setupcloking(void);
static void hw_setuppins(void);
static void hw_setupcoms(void);

static void hw_setupcoms_debug(void);
static void hw_setupcoms_lorawan(void);
static void hw_setupcoms_flash(void);
static void hw_setupcoms_i2c_gps(void);

static void hw_setupcoms_power(void);

static void hw_setup_pwm(void);
static void hw_setup_adc(void);

static void hw_restorePhotoCoeffs();

static void hw_setup_mrt_timers(void);


/*****************************************************************************
 * Public functions
 ****************************************************************************/



/*******************************************************************************
 * @brief Overall function to initialise the processor
 * @param[in] void
 * @return void
 *******************************************************************************/
void hw_setup(void)
{
    // Setup system clocking and muxing
    hw_setupmuxing();
    hw_setupcloking();

    // Update SystemCoreClock chip var
    SystemCoreClockUpdate();


    // Enable EEPROM Block
    Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_EEPROM);
    Chip_SYSCTL_PeriphReset(RESET_EEPROM);

    // First setup systick timer for delays
    hw_setup_mrt_timers(); // used by dali

    // Initialise System
    hw_setuppins();
    hw_setupcoms();
    hw_setup_pwm(); // used for an driver o/p
    hw_setup_adc();

    // Restore coefficients from flash.

    hw_restorePhotoCoeffs();

    return;
}

/*******************************************************************************
 * @brief Sets the baud rate and comm params of the power monitoring uart
 *        The analog devices part runs at a different baud rate and parity
 *        to the other implementations
 *
 * @param[in] uint32_t baud rate
 *            uint32_t comm params -> stop bits, party, byte size
 *
 * @return void
 *
 *******************************************************************************/
void hw_setup_power_monitor_uart_params( uint32_t baudRate, uint32_t commParams)
{
    Chip_UART_Disable( UART_POWER_METERING_IF);

    Chip_UART_SetBaud( UART_POWER_METERING_IF, baudRate);

    Chip_UART_ConfigData( UART_POWER_METERING_IF, commParams);

    Chip_UART_Enable( UART_POWER_METERING_IF);

}


/*******************************************************************************
 * @brief Setup of pin muxing matrix
 * @param[in] void
 * @return void
 *******************************************************************************/
static void hw_setupmuxing(void)
{

    // Enable SWM and IOCON clocks
    Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_IOCON);
    Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_SWM);
    Chip_SYSCTL_PeriphReset(RESET_IOCON);

    return;
}

/*******************************************************************************
 * @brief Setup main clock to 72MHz
 * @param[in] void
 * @return void
 *******************************************************************************/
static void hw_setupcloking(void)
{
    Chip_SetupXtalClocking();

    // Set USB PLL input to main oscillator
    Chip_Clock_SetUSBPLLSource(SYSCTL_PLLCLKSRC_MAINOSC);
    /* Setup USB PLL  (FCLKIN = 12MHz) * 4 = 48MHz
       MSEL = 3 (this is pre-decremented), PSEL = 1 (for P = 2)
       FCLKOUT = FCLKIN * (MSEL + 1) = 12MHz * 4 = 48MHz
       FCCO = FCLKOUT * 2 * P = 48MHz * 2 * 2 = 192MHz (within FCCO range) */
    Chip_Clock_SetupUSBPLL(3, 1);

    // Powerup USB PLL
    Chip_SYSCTL_PowerUp(SYSCTL_POWERDOWN_USBPLL_PD);

    // Wait for PLL to lock
    while (!Chip_Clock_IsUSBPLLLocked()) {}

    // Set default system tick divder to 1
    Chip_Clock_SetSysTickClockDiv(1);

    return;
}


/*******************************************************************************
 * @brief Initialisation of all IOs
 * @param[in] void
 * @return void
 *******************************************************************************/
static void hw_setuppins(void){
    // start at the last line of the pinout matrix
    // KF-722: -1 as pin_id ranging from 49 to 0
    uint8_t pin_id = matrix_n_rows(pcore_pinout_matrix) - 1;

    // Initialise GPIO Block
    Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_GPIO0);
    Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_GPIO1);
    Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_GPIO2);
    Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_MUX);

    Chip_SYSCTL_PeriphReset(RESET_MUX);

    // decrement pin_id until all pins are setup
    do{
        // Pin is an input
        if( pcore_pinout_matrix[pin_id].b_is_input ){
            Chip_GPIO_SetPinDIRInput(   LPC_GPIO,
                                        pcore_pinout_matrix[pin_id].pin_port,
                                        pcore_pinout_matrix[pin_id].pin_number);
        }
        // Pin is an output
        else{
            // Setup as input or output
            Chip_GPIO_SetPinDIROutput(  LPC_GPIO,
                                        pcore_pinout_matrix[pin_id].pin_port,
                                        pcore_pinout_matrix[pin_id].pin_number);
            // Set or clear by default
            Chip_GPIO_SetPinState(      LPC_GPIO,
                                        pcore_pinout_matrix[pin_id].pin_port,
                                        pcore_pinout_matrix[pin_id].pin_number,
                                        pcore_pinout_matrix[pin_id].output_state);
        }
        // Setup as digital i/o by default and disable pull-up/dpwn
        Chip_IOCON_PinMuxSet(   LPC_IOCON,
                                pcore_pinout_matrix[pin_id].pin_port,
                                pcore_pinout_matrix[pin_id].pin_number,
                                (IOCON_MODE_INACT | IOCON_DIGMODE_EN) );

    }while(pin_id--);

    return;
}

/*******************************************************************************
 * @brief Initialise all communication ports
 * @param[in] void
 * @return void
 *******************************************************************************/
static void hw_setupcoms(void){

    // Use main clock rate as base for UART baud rate divider
    Chip_Clock_SetUARTBaseClockRate(Chip_Clock_GetMainClockRate(), false);

    hw_setupcoms_lorawan();
    hw_setupcoms_flash();
    hw_setupcoms_debug();
    hw_setupcoms_power();
    hw_setupcoms_i2c_gps();

    return;
}

/*******************************************************************************
 * @brief Initialise debug coms
 * @param[in] void
 * @return void
 *******************************************************************************/
static void hw_setupcoms_debug(void){

    // UART signal muxing via SWM
    Chip_SWM_MovablePortPinAssign(  GPIO_DEBUG_UART_RXD_SWM,
                                    GPIO_DEBUG_UART_TXD_PORT,
                                    GPIO_DEBUG_UART_TXD_PIN);

    Chip_SWM_MovablePortPinAssign(  GPIO_DEBUG_UART_TXD_SWM,
                                    GPIO_DEBUG_UART_RXD_PORT,
                                    GPIO_DEBUG_UART_RXD_PIN);

    // Setup UART
    Chip_UART_Init( UART_DEBUG_IF);
    Chip_UART_ConfigData( UART_DEBUG_IF, DEBUG_COM_PARAMETERS);
    Chip_UART_SetBaud( UART_DEBUG_IF, DEBUG_BAUD_RATE);
    Chip_UART_Enable( UART_DEBUG_IF);
    Chip_UART_TXEnable( UART_DEBUG_IF);

    return;
}


static void hw_setupcoms_lorawan(void){

    // UART signal muxing via SWM.
    Chip_SWM_MovablePortPinAssign(  GPIO_LORA_UART_TXD_SWM,
                                    GPIO_LORA_UART_TXD_PORT,
                                    GPIO_LORA_UART_TXD_PIN);
    Chip_SWM_MovablePortPinAssign(  GPIO_LORA_UART_RXD_SWM,
                                    GPIO_LORA_UART_RXD_PORT,
                                    GPIO_LORA_UART_RXD_PIN);
    Chip_SWM_MovablePortPinAssign(  GPIO_LORA_UART_RTS_SWM,
                                    GPIO_LORA_UART_RTS_PORT,
                                    GPIO_LORA_UART_RTS_PIN);
    Chip_SWM_MovablePortPinAssign(  GPIO_LORA_UART_CTS_SWM,
                                    GPIO_LORA_UART_CTS_PORT,
                                    GPIO_LORA_UART_CTS_PIN);

    // Disable the UART before configuring it.
    Chip_UART_Disable(UART_LORA_IF);
    Chip_UART_Init(UART_LORA_IF);
    Chip_UART_ConfigData(UART_LORA_IF, UART_LORA_PARAMETERS);
    Chip_UART_SetBaud(UART_LORA_IF, UART_LORA_BAUDRATE);
    NVIC_SetPriority (UART_LORA_IRQ, LORAWAN_PRIORITY_LEVEL);
    Chip_UART_Enable(UART_LORA_IF);

    return;
}

/*******************************************************************************
 * @brief Initialise spi for external flash serial port
 * @param[in] void
 * @return void
 *******************************************************************************/
static void hw_setupcoms_flash(void){
    SPI_CFG_T           flash_spi_cnfg;
    SPI_DELAY_CONFIG_T  flash_spi_delay_cnfg;

    Chip_SWM_MovablePortPinAssign(  GPIO_FLASH_SPI_CS_SWM,
                                    GPIO_FLASH_SPI_CS_PORT,
                                    GPIO_FLASH_SPI_CS_PIN);
    Chip_SWM_MovablePortPinAssign(  GPIO_FLASH_SPI_MOSI_SWM,
                                    GPIO_FLASH_SPI_SDI_PORT,
                                    GPIO_FLASH_SPI_SDI_PIN);
    Chip_SWM_MovablePortPinAssign(  GPIO_FLASH_SPI_MISO_SWM,
                                    GPIO_FLASH_SPI_SDO_PORT,
                                    GPIO_FLASH_SPI_SDO_PIN);
    Chip_SWM_MovablePortPinAssign(  GPIO_FLASH_SPI_SCK_SWM,
                                    GPIO_FLASH_SPI_SPC_PORT,
                                    GPIO_FLASH_SPI_SPC_PIN);

    // Prepare flash spi config
    // Divider of 3 from main clock at 72MHz
    flash_spi_cnfg.ClkDiv = 3; //Chip_SPI_CalClkRateDivider(SPI_FLASH_IF, FLASH_BIT_RATE);
    flash_spi_cnfg.ClockMode = SPI_CLOCK_MODE0;
    flash_spi_cnfg.DataOrder = SPI_DATA_MSB_FIRST;
    flash_spi_cnfg.Mode = SPI_MODE_MASTER;
    flash_spi_cnfg.SSELPol = (SPI_CFG_SPOL0_LO
                            | SPI_CFG_SPOL1_LO
                            | SPI_CFG_SPOL2_LO
                            | SPI_CFG_SPOL3_LO);

    // Prepare Delay Registers
    flash_spi_delay_cnfg.FrameDelay = 0;
    flash_spi_delay_cnfg.PostDelay = 10;
    flash_spi_delay_cnfg.PreDelay = 10;
    flash_spi_delay_cnfg.TransferDelay = 10;

    // Configure SPI module
    Chip_SPI_Disable(SPI_FLASH_IF);
    Chip_SPI_Init(SPI_FLASH_IF);
    Chip_SPI_SetConfig(SPI_FLASH_IF, &flash_spi_cnfg);
    // All dextra delays 0 bu default
    Chip_SPI_DelayConfig(SPI_FLASH_IF, &flash_spi_delay_cnfg);
//    Chip_SPI_EnableLoopBack(SPI_FLASH_IF); // Loop back MOSI and MISO for testing
    Chip_SPI_Enable(SPI_FLASH_IF);

    return;
}

/*******************************************************************************
 * @brief Initialise i2c bus for GPS module com
 * @param[in] void
 * @return void
 *******************************************************************************/
static void hw_setupcoms_i2c_gps(void){

    // Setup as digital i/o and set RST as OPEN DRAIN
	// Let the GPS Internal pull up do the work
    Chip_IOCON_PinMuxSet(   LPC_IOCON,
    		GPIO_GPS_RST_PORT,
			GPIO_GPS_RST_PIN,
                            (IOCON_MODE_INACT | IOCON_OPENDRAIN_EN) );

	//--------------------------------------------
	// 				RST MODULE
	//--------------------------------------------
	Chip_GPIO_SetPinState(LPC_GPIO, GPIO_GPS_RST_PORT, GPIO_GPS_RST_PIN, false);
	sys_delay_ms(2);
	Chip_GPIO_SetPinState(LPC_GPIO, GPIO_GPS_RST_PORT, GPIO_GPS_RST_PIN, true);
	sys_delay_ms(500);

    // leave time for GPS RTC to start
	hw_kick_watchdog();

	//--------------------------------------------
	// 				WAKE UP MODULE
	//--------------------------------------------
	Chip_GPIO_SetPinState(LPC_GPIO, GPIO_GPS_ON_PORT, GPIO_GPS_ON_PIN, true);
	sys_delay_ms(100);

	//--------------------------------------------
	// 				SETUP I2C PINS
	//--------------------------------------------
	Chip_SWM_EnableFixedPin(GPS_I2C_SCL_FIXED_PIN);
	Chip_SWM_EnableFixedPin(GPS_I2C_SDA_FIXED_PIN);

	//--------------------------------------------
	// 					SETUP I2C
	//--------------------------------------------
	// Enable I2C clock and reset I2C peripheral
	Chip_I2C_Init(GPS_I2C_IF);
	// Setup clock rate for I2C
	Chip_I2C_SetClockDiv(GPS_I2C_IF, GPS_I2C_CLK_DIVIDER);
	// Setup I2CM transfer rate
	Chip_I2CM_SetBusSpeed(GPS_I2C_IF, GPS_I2C_BITRATE);

	//--------------------------------------------
	// 				SETUP I2C SLAVE
	//--------------------------------------------
	// Emulated EEPROM 0 is on slave index 0
	Chip_I2CS_SetSlaveAddr(GPS_I2C_IF, 0, GPS_TX);
	// Disable Qualifier for Slave Address 0
	Chip_I2CS_SetSlaveQual0(GPS_I2C_IF, false, 0);
	// Enable Slave Address 0
	Chip_I2CS_EnableSlaveAddr(GPS_I2C_IF, 0);

	//--------------------------------------------
	// 				SETUP I2C IRQ
	//--------------------------------------------
	// Clear interrupt status and enable slave interrupts
	Chip_I2CS_ClearStatus(GPS_I2C_IF, I2C_STAT_SLVDESEL);
	Chip_I2C_EnableInt(GPS_I2C_IF, 	I2C_INTENSET_SLVPENDING |
									I2C_INTENSET_SLVDESEL);

	// Only enable interrupt once task has been created

	// Enable I2C slave interface
	Chip_I2CS_Enable(GPS_I2C_IF);
	// Enable I2C IRQ
	NVIC_EnableIRQ(GPS_I2C_IRQ);

	// $GPGGA - Global Positioning System Fix Data
	// $GPGSA - GPS DOP and active satellites
	// $GPGSV - GPS Satellites in view
	// $GPRMC - Recommended minimum specific GPS/Transit data

    return;
}


/*******************************************************************************
 * @brief Initialise power coms Mark Hooper
 * @param[in] void
 * @return void
 *******************************************************************************/
static void hw_setupcoms_power(void){

    // UART signal muxing via SWM
    Chip_SWM_MovablePortPinAssign(  GPIO_POWER_METERING_UART_TXD_SWM,
                                    GPIO_POWER_METERING_UART_TXD_PORT,
                                    GPIO_POWER_METERING_UART_TXD_PIN);

    Chip_SWM_MovablePortPinAssign(  GPIO_POWER_METERING_UART_RXD_SWM,
                                    GPIO_POWER_METERING_UART_RXD_PORT,
                                    GPIO_POWER_METERING_UART_RXD_PIN);
    // Setup UART
    Chip_UART_Init( UART_POWER_METERING_IF);
    Chip_UART_ConfigData( UART_POWER_METERING_IF, POWER_COM_PARAMETERS);
    Chip_UART_SetBaud( UART_POWER_METERING_IF, STM8_UART_BAUD_RATE);
    Chip_UART_Enable( UART_POWER_METERING_IF);
    Chip_UART_TXEnable( UART_POWER_METERING_IF);
//    Chip_UART_RXEnable(UART_POWER_IF);
    Chip_UART_IntEnable(UART_POWER_METERING_IF, UART_INTEN_RXRDY);
    Chip_UART_IntDisable(UART_POWER_METERING_IF, UART_INTEN_TXRDY);
    NVIC_SetPriority(UART_POWER_METERING_IRQ, POWER_PRIORITY_LEVEL);
//    NVIC_EnableIRQ(UART_POWER_IRQ);

    return;
}

/*******************************************************************************
 * @brief Initialise MRT timers
 * @param[in] void
 * @return void
 *******************************************************************************/
static void hw_setup_mrt_timers(void){
	int mrtch;

	// MRT Initialization and disable all timers
	Chip_MRT_Init();
	for (mrtch = 0; mrtch < MRT_CHANNELS_NUM; mrtch++) {
		Chip_MRT_SetDisabled(Chip_MRT_GetRegPtr(mrtch));
	}

	// Enable the interrupt for the MRT
	NVIC_SetPriority(MRT_IRQn, MRT_TIMERS_PRIORITY_LEVEL);
	NVIC_EnableIRQ(MRT_IRQn);

	// Setup 1ms systick timer
	hw_setup_mrt_repeat(SYSTICK_MRT_CH, 1000, 38);

	// Enable timer idx TIMER_DALI_MRT_IDX for DALI IRQ
	dali_start_timer(DALI_TX_FREQ);

	// WIll trigger an adc sample sequence 10 times a second
	hw_setup_mrt_repeat(ADC_MRT_CH, 10, 0);

    return;
}


/*******************************************************************************
 * @brief Setup pwm for an driver ouput
 * @param[in] void
 * @return void
 *******************************************************************************/
static void hw_setup_pwm(void){

	// Init PWM
	Chip_SCTPWM_Init(ANA_PWM_SCT);
	// Set sampling rate
	Chip_SCTPWM_SetRate( ANA_PWM_SCT, AN_DRIVER_PWM_BASE_FREQ);
	// enable SCT0_OUT5 on GPIO_ANA_PWM_PIN
	an_driver_enable_pwm_pin();
	// Set sct output
	Chip_SCTPWM_SetOutPin( ANA_PWM_SCT, ANA_PWM_IDX, ANA_PWM_SCT_OP);
	// Set initial duty cycle at 50%
	an_driver_set_pwm_dc(50);
	// Start pwm
	//an_driver_start_pwm();

	return;
}

static void hw_setup_adc(void){



	// Disables pullups/pulldowns and disable digital mode */
	Chip_IOCON_PinMuxSet(	LPC_IOCON,
							ADC_PHOTO_PORT,
							ADC_PHOTO_PIN,
							(IOCON_MODE_INACT | IOCON_ADMODE_EN));
	Chip_IOCON_PinMuxSet(	LPC_IOCON,
							GPIO_PWM_FB_PORT,
							GPIO_PWM_FB_PIN,
							(IOCON_MODE_INACT | IOCON_ADMODE_EN));
	Chip_IOCON_PinMuxSet(	LPC_IOCON,
							GPIO_ANA_PWM_PORT,
							GPIO_ANA_PWM_PIN,
							(IOCON_MODE_INACT | IOCON_ADMODE_EN));
	Chip_IOCON_PinMuxSet(	LPC_IOCON,
							GPIO_ANA_P_DET_PORT,
							GPIO_ANA_P_DET_PIN,
							(IOCON_MODE_INACT | IOCON_ADMODE_EN));

	// Assign ADCx_x to PIOx_x via SWM (fixed pin)
	Chip_SWM_EnableFixedPin(ADC_PHOTO_SWM);
	Chip_SWM_EnableFixedPin(ADC_PWM_FB_SWM);
	Chip_SWM_EnableFixedPin(ADC_AN_IN_SWM);
    Chip_SWM_EnableFixedPin(ADC_AN_P_DET_SWM);

	// init and calibrate ADC
	Chip_ADC_Init(ADC_PHOTO_IF, 0);

	// Setup ADC to continuously sample all an channels:
	// PHOTO_IC ADC1_0
	// PWM_FB ADC1_4
	// ANALOG_IN ADC1_2
	Chip_ADC_SetupSequencer(	ADC_PHOTO_IF,
								ADC_PHOTO_SEQ,
								(ADC_SEQ_CTRL_CHANSEL(ADC_PHOTO_CH) |
								ADC_SEQ_CTRL_CHANSEL(ADC_PWM_FB_CH) |
								ADC_SEQ_CTRL_CHANSEL(ADC_AN_DRIV_IN_CH) |
                                ADC_SEQ_CTRL_CHANSEL(ADC_AN_P_DET_CH) |
								ADC_SEQ_CTRL_MODE_EOS));

	// calibrate adc after it is setup
	Chip_ADC_StartCalibration(ADC_PHOTO_IF);
	while (!(Chip_ADC_IsCalibrationDone(ADC_PHOTO_IF)));

	// set sampling clock rate after calibration
	// max resulting divider parameter for Chip_ADC_SetDivider() is 255!!
	// Result in ADC freq being 72Mhz / 255 = 280kHz
	Chip_ADC_SetDivider(ADC_PHOTO_IF, (uint8_t)0xFF);

	// Clear all pending interrupts
	Chip_ADC_ClearFlags(ADC_PHOTO_IF, Chip_ADC_GetFlags(ADC_PHOTO_IF));

	// Enable sequence A completion interrupts for ADC0
	Chip_ADC_EnableInt(ADC_PHOTO_IF, SDC_EN_INT_FLAG);


	// Enable related ADC NVIC interrupts
	NVIC_SetPriority(ADC_SEQ_IRQ, ADC_PRIORITY_LEVEL);
	NVIC_EnableIRQ(ADC_SEQ_IRQ);

	// Enable sequencers
	Chip_ADC_EnableSequencer(ADC_PHOTO_IF, ADC_PHOTO_SEQ);

	// Kick-off new sampling sequence
	Chip_ADC_StartSequencer(ADC_PHOTO_IF, ADC_PHOTO_SEQ);

	return;
}

/*******************************************************************************
 * @brief Restore (i.e. retrieve from flash) photo calibration coefficient(s)
 * @param[in] none
 * @return void
 *******************************************************************************/
static void hw_restorePhotoCoeffs()
{
	float f;

	if(false == common_checkFlashClear(FLASH_ADDR_PHOTO_CALIB_COEFF, 4))
	{
		f = common_getFloatFromFlash(FLASH_ADDR_PHOTO_CALIB_COEFF);
	}
	else
	{
		f = 1.0;
	}
	photo_setCalibCoeff(f);
}
/*******************************************************************************
 * @brief Setup MRT timer for repeat mode !! MIN 5Hz !!
 * @param[in] timer idx, frequency, ppm offset to apply to period
 * @return void
 *******************************************************************************/
void hw_setup_mrt_repeat(uint8_t ch, uint32_t rate, float ppm)
{
	LPC_MRT_CH_T *pMRT;

	// MINIMUM 5Hz due to lpc15xx 24bit timer based on 72MHz clock
	if(rate < 5) rate = 5;

	// Get pointer to timer selected by ch
	pMRT = Chip_MRT_GetRegPtr(ch);

	// Setup timer with rate based on MRT clock
    float interval = (float)(Chip_Clock_GetSystemClockRate() / rate);
    // A positive ppm value will slow down the clock
    interval = interval * ( 1 + ppm / 1000000);
	Chip_MRT_SetInterval(	pMRT,
							(uint32_t)interval
							|MRT_INTVAL_LOAD);

	// Timer mode
	Chip_MRT_SetMode(pMRT, MRT_MODE_REPEAT);

	// Clear pending interrupt and enable timer
	Chip_MRT_IntClear(pMRT);
	Chip_MRT_SetEnabled(pMRT);
}


// Implemented for Unit Testing
void lorawan_disable_module(){ Chip_GPIO_SetPinState(LPC_GPIO, GPIO_LORA_RESET_N_PORT,GPIO_LORA_RESET_N_PIN, 0);}
void lorawan_enable_module(){ Chip_GPIO_SetPinState(LPC_GPIO, GPIO_LORA_RESET_N_PORT,GPIO_LORA_RESET_N_PIN, 1);}
void lorawan_tx_enable(){ Chip_UART_IntEnable(UART_LORA_IF, UART_INTEN_TXRDY);}
void lorawan_tx_disable(){ Chip_UART_IntDisable( UART_LORA_IF, UART_INTEN_TXRDY);}
void lorawan_rx_enable(){ Chip_UART_IntEnable(UART_LORA_IF, UART_INTEN_RXRDY);}
void lorawan_rx_disable(){ Chip_UART_IntDisable(UART_LORA_IF, UART_INTEN_RXRDY);}
void lorawan_irq_enable(){ NVIC_EnableIRQ(UART_LORA_IRQ);}
void lorawan_irq_disable(){ NVIC_DisableIRQ(UART_LORA_IRQ);}
void lorawan_tx_byte(char x){  Chip_UART_SendByte(UART_LORA_IF, x);}
char lorawan_rx_byte(){   return Chip_UART_ReadByte(UART_LORA_IF);}
uint32_t lorawan_get_int_status(){   return Chip_UART_GetIntStatus(UART_LORA_IF);}
bool lorawan_was_rx_isr(uint32_t intStatus){return (intStatus & UART_STAT_RXRDY)?true:false;}
bool lorawan_was_tx_isr(uint32_t intStatus){return (intStatus & UART_STAT_TXRDY)?true:false;}
void lorawan_set_baudrate(uint32_t baudRate){ Chip_UART_SetBaud(UART_LORA_IF, baudRate);}

uint8_t eepromWriteByte(uint32_t dstAdd, uint8_t *ptr, uint32_t byteswrt)
{return Chip_EEPROM_Write( dstAdd, ptr,  byteswrt);}
uint8_t eepromReadByte(uint32_t srcAdd, uint8_t *ptr, uint32_t bytesrd)
{return Chip_EEPROM_Read( srcAdd, ptr, bytesrd);}

// power monitoring
void hw_power_monitor_uart_tx_enable(){Chip_UART_IntEnable(UART_POWER_METERING_IF, UART_INTEN_TXRDY);}
void hw_power_monitor_uart_tx_disable(){Chip_UART_IntDisable(UART_POWER_METERING_IF, UART_INTEN_TXRDY);}
void hw_power_monitor_uart_tx_byte(char x){  Chip_UART_SendByte(UART_POWER_METERING_IF, x);}
uint8_t hw_power_monitor_uart_rx_byte(){   return (uint8_t)Chip_UART_ReadByte(UART_POWER_METERING_IF);}
uint32_t hw_power_monitor_uart_get_int_status(){   return Chip_UART_GetIntStatus(UART_POWER_METERING_IF);}
void hw_power_monitor_uart_irq_enable() {NVIC_EnableIRQ(UART_POWER_METERING_IRQ);}
void hw_power_monitor_uart_irq_disable() {NVIC_DisableIRQ(UART_POWER_METERING_IRQ);}

//DALI CIRCUIT
void dali_set_psu(bool x){ Chip_GPIO_SetPinState(LPC_GPIO, GPIO_DALI_ENA_PORT, GPIO_DALI_ENA_PIN, x);}
void dali_out(bool x){ Chip_GPIO_SetPinState(LPC_GPIO, GPIO_DALI_TX_PORT, GPIO_DALI_TX_PIN, x);}
void an_driver_set_an_smooth(bool x){Chip_GPIO_SetPinState(LPC_GPIO, GPIO_UP_SMOOTH_PORT, GPIO_UP_SMOOTH_PIN, x);}
void an_driver_disable_pwm_pin(void){ Chip_SWM_FixedPinEnable( ANA_PWM_SCT_PIN_FUNC, false);}

// gps
void hw_disable_gps_module(void){Chip_GPIO_SetPinState(LPC_GPIO, GPIO_GPS_RST_PORT, GPIO_GPS_RST_PIN, false);}

//Internal flash
void hw_writeToInternalFlashSingleSector(uint32_t address, uint32_t * ptrData, uint32_t dataLengthBytes)
{
    uint32_t sector = (uint32_t)(address>>12);

    __disable_irq();

    // Prepare sector
	Chip_IAP_PreSectorForReadWrite( sector, sector);

    // Erase sector
    Chip_IAP_EraseSector( sector,  sector);

    // Prepare sector
    Chip_IAP_PreSectorForReadWrite( sector, sector);

    // copy from ram to code space
    Chip_IAP_CopyRamToFlash( address, ptrData, dataLengthBytes);

    // All done
    __enable_irq();
}
/***************************** EOF ******************************************/
