/*
==========================================================================
 Name        : rtc_procx.h
 Project     : pcore
 Path        : /pcore/src/public/hardware/adc_procx.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 13 Apr 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_COMMON_RTC_PROCX_H_
#define PUBLIC_COMMON_RTC_PROCX_H_

#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
/*!
 * \brief Real time structure
 */
typedef struct {
    uint32_t RTC_Sec;     /*!< Second value - [0,59] */
    uint32_t RTC_Min;     /*!< Minute value - [0,59] */
    uint32_t RTC_Hour;    /*!< Hour value - [0,23] */
    uint32_t RTC_Mday;    /*!< Day of the month value - [1,31] */
    uint32_t RTC_Mon;     /*!< Month value - [1,12] */
    uint32_t RTC_Year;    /*!< Year value - [0,4095] */
    uint32_t RTC_Wday;    /*!< Day of week value - [0,6] */
    uint32_t RTC_Yday;    /*!< Day of year value - [1,365] */
} RTCTime;
/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

uint32_t rtc_procx_GetCounter(void);
void rtc_procx_SetCounter(uint32_t Counter);
void rtc_procx_IncCounter();

RTCTime rtc_procx_GetTime(void);
void rtc_procx_SetTime(RTCTime Time);

void rtc_procx_Correction(int32_t Correction);

void rtc_procx_SetDstIsApplicable(bool val);

char * getTimeAsString(void);
char * getRtcAsString(void);

#endif /* PUBLIC_COMMON_RTC_PROCX_H_ */
