/*
==========================================================================
 Name        : rtc_procx.c
 Project     : pcore
 Path        : /pcore/src/public/hardware/rtc_procx.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 19 Sept 2019
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "time.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "rtc_procx.h"


#include "FreeRTOS.h"
#include "task.h"
#include "FreeRTOSMacros.h"
#include "FreeRTOSCommonHooks.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
struct tm *TD_Time;
static volatile time_t Unix_Time;
static bool bDstApplicable = false;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static time_t convertDateTimeToTimestamp(struct tm DateTime);
static struct tm convertTimestampToDateTime(time_t unixTimeStamp);

static struct tm convertFromCalendarStruct(RTCTime calendarDateTime);
static RTCTime convertToCalendarStruct(struct tm timeDateTime);

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*
The mktime() function converts a broken-down time structure, expressed as
local time, to calendar time representation.

The function ignores the values supplied by the caller in the tm_wday and
tm_yday fields.

The value specified in the tm_isdst field informs mktime() whether or not
daylight saving time (DST) is in effect for the time supplied in the
tm structure: a positive value means DST is in effect; zero means that
DST is not in effect; and a negative value means that mktime() should
(use timezone information and system databases to) attempt to determine
whether DST is in effect at the specified time.

The mktime() function modifies the fields of the tm structure as follows:
tm_wday and tm_yday are set to values determined from the contents of the other
fields;

if structure members are outside their valid interval, they will be
normalized (so that, for example, 40 October is changed into 9 November);

tm_isdst is set (regardless of its initial value) to a positive value or to 0,
respectively, to indicate whether DST is or is not in effect at the specified
time.

Calling mktime() also sets the external variable tzname with information
about the current timezone.
 */

/*
    // Set timezone to Eastern Standard Time and print local time
    setenv("TZ", "EST5EDT,M3.2.0/2,M11.1.0", 1);
    tzset();
    localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI(TAG, "The current date/time in New York is: %s", strftime_buf);

    // Set timezone to China Standard Time
    setenv("TZ", "CST-8", 1);
    tzset();
    localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI(TAG, "The current date/time in Shanghai is: %s", strftime_buf);
 */
/*
The TZ environment variable is expected to be in the following POSIX
format:
  stdoffset1[dst[offset2][,start[/time1],end[/time2]]]
where: std is the name of the standard time-zone (minimum 3 chars)
       offset1 is the value to add to local time to arrive at Universal time
         it has the form:  hh[:mm[:ss]]
       dst is the name of the alternate (daylight-savings) time-zone (min 3 chars)
       offset2 is the value to add to local time to arrive at Universal time
         it has the same format as the std offset
       start is the day that the alternate time-zone starts
       time1 is the optional time that the alternate time-zone starts
         (this is in local time and defaults to 02:00:00 if not specified)
       end is the day that the alternate time-zone ends
       time2 is the time that the alternate time-zone ends
         (it is in local time and defaults to 02:00:00 if not specified)
 */
static time_t convertDateTimeToTimestamp(struct tm DateTime)
{
    DateTime.tm_isdst = -1;
    return mktime( &DateTime );
}

static struct tm convertTimestampToDateTime(time_t unixTimeStamp)
{
    struct tm s;
    s = *(localtime( (time_t *)&unixTimeStamp) );
    return s;
}

static struct tm convertFromCalendarStruct(RTCTime calendarDateTime)
{
    struct tm timeDateTime;
    timeDateTime.tm_sec =   (int)calendarDateTime.RTC_Sec;
    timeDateTime.tm_min =   (int)calendarDateTime.RTC_Min;
    timeDateTime.tm_hour =  (int)calendarDateTime.RTC_Hour;
    timeDateTime.tm_mday =  (int)calendarDateTime.RTC_Mday;
    timeDateTime.tm_mon =   (int)calendarDateTime.RTC_Mon - 1;
    timeDateTime.tm_year =  (int)calendarDateTime.RTC_Year - 1900;
    timeDateTime.tm_wday =  (int)calendarDateTime.RTC_Wday;
    timeDateTime.tm_yday =  (int)calendarDateTime.RTC_Yday - 1;
    return timeDateTime;
}

static RTCTime convertToCalendarStruct(struct tm timeDateTime)
{
    RTCTime calendarDateTime;
    calendarDateTime.RTC_Sec =  (uint32_t)timeDateTime.tm_sec;
    calendarDateTime.RTC_Min =  (uint32_t)timeDateTime.tm_min;
    calendarDateTime.RTC_Hour = (uint32_t)timeDateTime.tm_hour;
    calendarDateTime.RTC_Mday = (uint32_t)timeDateTime.tm_mday;
    calendarDateTime.RTC_Mon =  (uint32_t)timeDateTime.tm_mon + 1;
    calendarDateTime.RTC_Year = (uint32_t)timeDateTime.tm_year + 1900;
    calendarDateTime.RTC_Wday = (uint32_t)timeDateTime.tm_wday;
    calendarDateTime.RTC_Yday = (uint32_t)timeDateTime.tm_yday + 1;
    return calendarDateTime;
}

void rtc_procx_SetDstIsApplicable(bool val)
{
    bDstApplicable = val;
    if( true == bDstApplicable ){
        setenv("TZ", "UTC00DST,M3.5.0/1,M10.5.0/2", 1);
    }else{
        setenv("TZ", "UTC00", 1);
    }
    tzset();
}

char * getTimeAsString(void)
{
    struct tm p;
    char * p_s;
    p = *(localtime( (time_t *)&Unix_Time) );
    p_s =  asctime(&p);
    return p_s;
}

char * getRtcAsString(void)
{
    RTCTime Rtc = rtc_procx_GetTime();
    static char s[50]={0};
    snprintf(s, sizeof s,"%02d/%02d/%02d %02d:%02d:%02d",
            (uint16_t)Rtc.RTC_Mday, (uint16_t)Rtc.RTC_Mon, (uint16_t)Rtc.RTC_Year,
			(uint16_t)Rtc.RTC_Hour,(uint16_t)Rtc.RTC_Min, (uint16_t)Rtc.RTC_Sec);
    return s;
}

//----------------------------------------------------------------------
// Existing Code API
//----------------------------------------------------------------------

uint32_t rtc_procx_GetCounter(void)
{
    return (uint32_t)Unix_Time;
}

void rtc_procx_SetCounter(uint32_t Counter)
{
    Unix_Time = (time_t)Counter;
    return;
}

void rtc_procx_IncCounter()
{
    Unix_Time++;
    return;
}


RTCTime rtc_procx_GetTime(void)
{
    return convertToCalendarStruct( convertTimestampToDateTime( Unix_Time ) );
}

void rtc_procx_SetTime(RTCTime Time)
{
    Unix_Time = convertDateTimeToTimestamp( convertFromCalendarStruct( Time ) );
    return;
}


void rtc_procx_Correction(int32_t Correction)
{
    int32_t running_time = (int32_t)rtc_procx_GetCounter();
    running_time = running_time + Correction;
    rtc_procx_SetCounter( (uint32_t)running_time );

}
