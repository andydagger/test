/*
==========================================================================
 Name        : adc_procx.h
 Project     : pcore
 Path        : /pcore/src/public/hardware/adc_procx.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 13 Apr 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_COMMON_ADC_PROCX_H_
#define PUBLIC_COMMON_ADC_PROCX_H_

#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/

#define convert_av_to_volt(x) ((x)*(float)(ADC_REF_VOLT/4095))
#define ANALOG_P_DETECT_COEF ((float)6.6)

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

void set_adc_procx_photo_ic_spl(uint32_t val);
void set_adc_procx_pwm_fb_spl(uint32_t val);
void set_adc_procx_an_driver_in_spl(uint32_t val);
void set_adc_procx_p_det_spl(uint32_t val);

uint32_t get_adc_procx_photo_ic_spl(void);
uint32_t get_adc_procx_pwm_fb_spl(void);
uint32_t get_adc_procx_an_driver_in_spl(void);

float get_adc_procx_photo_ic_av_raw(void);
float get_adc_procx_pwm_fb_av_volt(void);
float get_adc_procx_an_driver_in_av_volt(void);
float get_adc_procx_p_detect_volt(void);

void adc_procx_calculate_average(void);
bool adc_procx_process_samples(void);


#endif /* PUBLIC_COMMON_ADC_PROCX_H_ */
