/*
==========================================================================
 Name        : led.c
 Project     : pcore
 Path        : /pcore/src/hardware/led.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 25 Aug 2017
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "chip.h"
#include "stdbool.h"
#include "stdint.h"
#include "config_pinout.h"
#include "led.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
typedef struct
{
    led_list colour;
    uint8_t port;
    uint8_t pin;
    bool b_polarity;
}led_list_struct;

static const led_list_struct led_matrix[] =
{
        { GREEN, GPIO_LED_GREEN_PORT, GPIO_LED_GREEN_PIN, GPIO_LED_GREEN_POLARITY },
        { RED, GPIO_LED_RED_PORT, GPIO_LED_RED_PIN, GPIO_LED_RED_POLARITY },
};
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Updates the statuc of an led
 * @param[in] led_list selected colour
 * @return void
 *******************************************************************************/
void led_on( led_list colour){

//    Chip_GPIO_SetPinState(  LPC_GPIO,
//                            led_matrix[colour].port,
//                            led_matrix[colour].pin,
//                            led_matrix[colour].b_polarity ^ true);

    return;
}

/*******************************************************************************
 * @brief Updates the statuc of an led
 * @param[in] led_list selected colour
 * @return void
 *******************************************************************************/
void led_off( led_list colour){

    Chip_GPIO_SetPinState(  LPC_GPIO,
                            led_matrix[colour].port,
                            led_matrix[colour].pin,
                            led_matrix[colour].b_polarity ^ false);

    return;
}

/*******************************************************************************
 * @brief Updates the statuc of an led
 * @param[in] led_list selected colour
 * @return void
 *******************************************************************************/
void led_toggle( led_list colour){

//    Chip_GPIO_SetPinToggle(  LPC_GPIO,
//                            led_matrix[colour].port,
//                            led_matrix[colour].pin);

    return;
}

