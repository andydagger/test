/*
==========================================================================
 Name        : adc_procx.c
 Project     : pcore
 Path        : /pcore/src/public/hardware/adc_procx.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 13 Apr 2018
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include "patch_rx.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "config_pinout.h"
#include "config_adc_procx.h"
#include "adc_procx.h"
#include "debug.h"
#include "config_an_driver.h"

typedef struct {
	uint32_t spl;
	float av;
	uint32_t av_sum;
}spl_ip;

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static volatile spl_ip photo_ic = {0};
static volatile spl_ip pwm_fb = {0};
static volatile spl_ip an_driver_ip = {0};
static volatile spl_ip p_detect = {0};

static volatile uint8_t av_spl_idx = 0;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

void set_adc_procx_photo_ic_spl(uint32_t val){
	photo_ic.spl = val & 0xFFF;
	return;
}

void set_adc_procx_pwm_fb_spl(uint32_t val){
	pwm_fb.spl = val & 0xFFF;
	return;
}

void set_adc_procx_an_driver_in_spl(uint32_t val){
	an_driver_ip.spl = val & 0xFFF;
	return;
}

void set_adc_procx_p_det_spl(uint32_t val){
	p_detect.spl = val & 0xFFF;
	return;
}

void adc_procx_calculate_average(void){
	photo_ic.av = (float)(photo_ic.av_sum/ADC_AVERAGE_CYCLES);
	photo_ic.av_sum  = 0;

	pwm_fb.av = (float)(pwm_fb.av_sum/ADC_AVERAGE_CYCLES);
	pwm_fb.av_sum = 0;

	an_driver_ip.av = (float)(an_driver_ip.av_sum/ADC_AVERAGE_CYCLES);
	an_driver_ip.av_sum = 0;

	p_detect.av = (float)(p_detect.av_sum/ADC_AVERAGE_CYCLES);
	p_detect.av_sum = 0;

	av_spl_idx = 0;

	return;
}

bool adc_procx_process_samples(void){
	// if all samples received > calculate average
	if(++av_spl_idx <= ADC_AVERAGE_CYCLES){
		// keep adding up samples
		photo_ic.av_sum += photo_ic.spl;
		pwm_fb.av_sum += pwm_fb.spl;
		an_driver_ip.av_sum += an_driver_ip.spl;
		p_detect.av_sum += p_detect.spl;
		// kick off another sequence
		return true;
	}else{
		// do not kick off another sequence
		return false;
	}
}


uint32_t get_adc_procx_photo_ic_spl(void){
	return photo_ic.spl;
}

uint32_t get_adc_procx_pwm_fb_spl(void){
	return pwm_fb.spl;
}

uint32_t get_adc_procx_an_driver_in_spl(void){
	return an_driver_ip.spl;
}


float get_adc_procx_photo_ic_av_raw(void){
	return photo_ic.av;
}

float get_adc_procx_pwm_fb_av_volt(void){
	return (float)(ANALOG_DRIVER_COEF*convert_av_to_volt(pwm_fb.av));
}

float get_adc_procx_an_driver_in_av_volt(void){
	return (float)(ANALOG_DRIVER_COEF*convert_av_to_volt(an_driver_ip.av));
}

float get_adc_procx_p_detect_volt(void){
	return (float)(ANALOG_P_DETECT_COEF*convert_av_to_volt(p_detect.av));
}
