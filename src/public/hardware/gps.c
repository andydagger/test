/*
==========================================================================
 Name        : gps.c
 Project     : pcore
 Path        : /pcore/src/public/hardware/gps.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 18 Apr 2018
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "stdbool.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "gps.h"
#include "debug.h"
#include "sysmon.h"
#include "FreeRTOS.h"
#include "task.h"
#include "FreeRTOSCommonHooks.h"
#include "common.h"
#include "config_dataset.h"
#include "hw.h"
#include "rtc_procx.h"
#include "systick.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static uint8_t p = 0;
static volatile char rx_data[100] = {0};
static volatile char gpgga_data[sizeof rx_data] = {0};
static volatile char gprmc_data[sizeof rx_data] = {0};
static char coordinates[COORDINATES_SIZE] = {0};
static char timestamp[TIMESTAMP_DATE_SIZE] = {0};

COORD coordinates_dec;

static GpsDataset gps_dataset = {0};
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static bool check_string_is_pgga(void);
static bool check_string_is_prmc(void);

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Called by external function to disable the GPS Module
 * @param[in] void
 * @return void
 *******************************************************************************/
void gps_disable_module( void )
{
	hw_disable_gps_module();
}


/*******************************************************************************
 * @brief Called by IRQ Handler for gps i2c receiver: rx start char
 * @param[in] void
 * @return void
 *******************************************************************************/
void gps_rx_start(uint8_t addr)
{
	p = 0;
}

/*******************************************************************************
 * @brief Called by IRQ Handler for gps i2c receiver: rx start byte
 * @param[in] void
 * @return void
 *******************************************************************************/
uint8_t gps_rx_byte(uint8_t data)
{
		rx_data[p++] = (char)data;
		if(data == '\n'){
			//if string is in GPRMC format
			if(check_string_is_prmc() == true){

				//++ TEST CODE Hard coded GPS Data
				//	sprintf(rx_data, "$GPRMC,110347.717,V,,,,,,,111218,,,N*4B");
				//-- TEST CODE
                memcpy((char *)&gprmc_data[0], (char *)&rx_data[0], sizeof rx_data);
                // DO NOT DEBUG PRINT FROM ISR!!
			}
			//if string is in GPGGA format
			else if(check_string_is_pgga() == true){

				//++ TEST CODE Hard coded GPS Data
				//	sprintf(rx_data, "$GPGGA,100712.447,5342.3868,N,00154.4246,W,1,03");
				//-- TEST CODE

                memcpy((char *)&gpgga_data[0], (char *)&rx_data[0], sizeof rx_data);
                // DO NOT DEBUG PRINT FROM ISR!!
			}
			p = 0;
		}
	return 0;
}

/*******************************************************************************
 * @brief Called by IRQ Handler for gps i2c receiver: rx start end char
 * @param[in] void
 * @return void
 *******************************************************************************/
void gps_rx_done(void)
{
	// Nothing needs to be done here
	return;
}


// TODO: not tested
uint8_t gps_tx_byte(uint8_t *data)
{
	/* Send data from emulated EEPROM */
	*data = 0x00;

	return 0;
}

/*******************************************************************************
 * @brief Checks if last received string was PGGA
 * Global Positioning System Fix Data
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool check_string_is_pgga(void){
	// first 2 bytes contain the i2c address
	return (lz_strcmp((char *)&rx_data[1], "GPGGA", 5)) ? true : false;
}

/*******************************************************************************
 * @brief Checks if last received string was PRMC
 * Recommended minimum specific GPS/Transit data
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool check_string_is_prmc(void){
	// first 2 bytes contain the i2c address
	return (lz_strcmp((char *)&rx_data[1], "GPRMC", 5)) ? true : false;
}

/*******************************************************************************
 * @brief gets ptr to gps coordinates
 * @param[in] void
 * @return ptr to address
 *******************************************************************************/
char * get_gps_ptr_to_coordinates_str(void)
{
    return coordinates;
}

/*******************************************************************************
 * @brief gets ptr to timestamp
 * @param[in] void
 * @return ptr to address
 *******************************************************************************/
char * get_tms_ptr_to_timestamp_str(void)
{
    return timestamp;
}

/*******************************************************************************
// * @brief gets the UTC timestamp
// * @param *data
// * @return gps_lock
// *******************************************************************************/
bool gps_get_utc(char *data)
{
    uint8_t x = 0;

    while(sizeof timestamp > x)
    {
        *data = timestamp[x];
        x++;
        data++;
    }
    return true;
}

/*******************************************************************************
 * @brief Parse the gps datat string
 * @param[in] void
 * @return true if gps has accurate lock
 *******************************************************************************/
bool gps_parsed_locked_data_accurate(void)
{
	uint8_t x = 0;
	/*
	eg. $--GGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx

	hhmmss.ss = UTC of position
	llll.ll = latitude of position
	a = N or S
	yyyyy.yy = Longitude of position
	a = E or W
	x = GPS Quality indicator (0=no fix, 1=GPS fix, 2=Dif. GPS fix)
	xx = number of satellites in use
	x.x = horizontal dilution of precision
	x.x = Antenna altitude above mean-sea-level
	M = units of antenna altitude, meters
	x.x = Geoidal separation
	M = units of geoidal separation, meters
	x.x = Age of Differential GPS data (seconds)
	xxxx = Differential reference station ID
	*/

    char * p_gps_str_char;

    debug_printf(GPS, "PRGGA => %s\r\n", gpgga_data);

    p_gps_str_char = (char *)gpgga_data;

    //find location of 6th comma
    while(6 > x)
    {
    	p_gps_str_char = (char *)strchr(p_gps_str_char, ',');
    	p_gps_str_char++;
    	x++;
    }


    //must be '2' for accurate lock
    if(*p_gps_str_char == '2')
    {
        // if data valid parse accordingly
        strncpy(coordinates, (char*)&gpgga_data[18], COORDINATES_SIZE);

        //convert from string DDM into decimal DD
        convert_gps_decimal(&coordinates[0], &coordinates_dec.latitude);

        gps_dataset.latitude = (int32_t)coordinates_dec.latitude * 100000;
        gps_dataset.longitude = (int32_t)coordinates_dec.longitude * 100000;
        gps_dataset.accuracy = 2;
        // Set gps dataset is ready
        gps_dataset.b_data_ready = true;

        return true;
    }
    else{
        return false;
    }
}

/*******************************************************************************
 * @brief Parse the gps datat string
 * @param[in] void
 * @return true if gps has lock
 *******************************************************************************/
bool gps_parsed_locked_data(void)
{
	uint8_t x = 0;
	static uint32_t gps_wait_ref_start = 0;
	/*
	eg. $--GGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx

	hhmmss.ss = UTC of position
	llll.ll = latitude of position
	a = N or S
	yyyyy.yy = Longitude of position
	a = E or W
	x = GPS Quality indicator (0=no fix, 1=GPS fix, 2=Dif. GPS fix)
	xx = number of satellites in use
	x.x = horizontal dilution of precision
	x.x = Antenna altitude above mean-sea-level
	M = units of antenna altitude, meters
	x.x = Geoidal separation
	M = units of geoidal separation, meters
	x.x = Age of Differential GPS data (seconds)
	xxxx = Differential reference station ID
	*/

    char * p_gps_str_char;

    /* Init the GPS Ref start Search Time */
	if ( gps_wait_ref_start == 0 )
	{
		gps_wait_ref_start = get_systick_ms();
	}

    p_gps_str_char = (char *)gpgga_data;

	debug_printf(GPS, "G PRGGA => %s\r\n", gpgga_data);

    //find location of 6th comma
    while(6 > x)
    {
    	p_gps_str_char = strchr(p_gps_str_char, ',');
    	p_gps_str_char++;
    	x++;
    }

    //can be a '1' or '2' for a lock
    if((*p_gps_str_char == '1') || (*p_gps_str_char == '2'))
    {
        // if data valid parse accordingly
        strncpy(coordinates, (char *)&gpgga_data[18], COORDINATES_SIZE);

        //convert from string DDM into decimal DD
        convert_gps_decimal(&coordinates[0], &coordinates_dec.latitude);
        gps_dataset.latitude = (int32_t)(coordinates_dec.latitude * 100000);
        gps_dataset.longitude = (int32_t)(coordinates_dec.longitude * 100000);
        gps_dataset.accuracy =  lz_char_to_uint8_ascii(p_gps_str_char);
        // Set gps dataset is ready
        gps_dataset.b_data_ready = true;

        /* Sowerby Bridge LZ */
        /* LAT 5370653
         * LONG -190716
         */
        debug_printf(GPS, "LAT %d \r\n", gps_dataset.latitude);
        debug_printf(GPS, "LONG %d \r\n", gps_dataset.longitude);

        // mark lock as ok
        clear_sysmon_fault(NO_GPS_LOCK);

        // Adjust DST setting accordingly
        if(     ( gps_dataset.latitude != 0 ) && /* Has the GPS been acquired?  */
                ( (float)(gps_dataset.latitude / 100000) < 30.0 ) ){  /* Scale the Latitude (by 100000) and check if it's in the non-DST region < 30 degrees */
            rtc_procx_SetDstIsApplicable(false);
        }else{
            rtc_procx_SetDstIsApplicable(true);
        }

        return true;
    }
    // latch on time-out: no point reporting same fault more than once
    else if (false == gps_dataset.b_data_ready){

		// After GPS_COORDS_SEARCH_TIMEOUT milliseconds give up looking for the GPS CO-ORDS
		if((get_systick_ms() - gps_wait_ref_start) > GPS_COORDS_SEARCH_TIMEOUT)
		{
			debug_printf(GPS, "GPS_COORDS_SEARCH_TIMEOUT\r\n");

			/* Report error to the System Monitor */
//			report_error(GPS_COORDS_NOT_FOUND);
			set_sysmon_fault(NO_GPS_LOCK);

		    // Set gps dataset is ready
		    gps_dataset.b_data_ready = true;
		}

        return false;
    }
    return false;
}

/*******************************************************************************
 * @brief Parse the gpa accurate data string
 * @param[in] void
 * @return true if gps has lock
 *******************************************************************************/
bool timestamp_parsed_locked_data(void)
{
	static bool GPS_HW_check = false;
	static uint32_t gps_wait_ref_start = 0;
	/*
	   g1. $GPRMC,081836,A,3751.65,S,14507.36,E,000.0,360.0,130998,011.3,E*62
	   eg2. $GPRMC,225446,A,4916.45,N,12311.12,W,000.5,054.7,191194,020.3,E*68


	   225446       Time of fix 22:54:46 UTC
	   A            Navigation receiver warning A = OK, V = warning
	   4916.45,N    Latitude 49 deg. 16.45 min North
	   12311.12,W   Longitude 123 deg. 11.12 min West
	   000.5        Speed over ground, Knots
	   054.7        Course Made Good, True
	   191194       Date of fix  19 November 1994
	   020.3,E      Magnetic variation 20.3 deg East
	   *68          mandatory checksum
	 */

	char * p_tms_str_char;
	uint8_t i = 0;

	if ( gps_wait_ref_start == 0 )
	{
		gps_wait_ref_start = get_systick_ms();
	}

	p_tms_str_char = (char *)gprmc_data;
	p_tms_str_char = strchr(p_tms_str_char, ',');
	p_tms_str_char++;

	debug_printf(GPS, "PRMC => %s\r\n", gprmc_data);
//		debug_printf(GPS, "PRGGA => %s\r\n", gpgga_data);

	/* Check if the GPS HW check is done */
	if( GPS_HW_check == false )
	{
		/* Start of GPS return string detected  */
		if ( gprmc_data[1] == 'G')
		{
			debug_printf(GPS, "GPS FOUND\r\n");
			/* GPS HW returned a start char of the string so it must be talking to us */
			/* Don't look for the GPS HW any more */
			GPS_HW_check = true;

			clear_sysmon_fault(GPS_MODULE_NOT_RESPONDING);
		}
	    // latch on time-out: no point reporting same fault more than once
	    else if (false == gps_dataset.b_data_ready){

			// After 1 sec the GPS should be talking to us
			// After GPS_HW_SEARCH_TIMEOUT milliseconds give up looking for the GPS HW
			if((get_systick_ms() - gps_wait_ref_start) > GPS_HW_SEARCH_TIMEOUT)
			{
				/* We give up GPS HW hasn't been detected OR it's broken */
				/* Don't bother looking for the GPS HW any more */
				GPS_HW_check = true;

				debug_printf(GPS, "GPS NOT FOUND\r\n");

				/* Report error to the System Monitor */
//				report_error(GPS_HW_NOT_FOUND);
				set_sysmon_fault(GPS_MODULE_NOT_RESPONDING);

			    // Set gps dataset is ready
			    gps_dataset.b_data_ready = true;

				return false;
			}
		}
	}

	//check we have a valid timestamp
	if((*p_tms_str_char >= '0') && ('2' >= *p_tms_str_char))
	{
	    // first copy the time
		 strncpy(&timestamp[0], p_tms_str_char, TIMESTAMP_SIZE);
		 // move forward by 8 ',' separators
		 i = 8;
		 while(i--){
		        p_tms_str_char = strchr(p_tms_str_char, ',');
		        p_tms_str_char++;
		 }
		 // then copy the date
		 strncpy(&timestamp[TIMESTAMP_SIZE], p_tms_str_char, DATESTAMP_SIZE);
		 return true;
	}
	else
	{
		return false;
	}
}
/*******************************************************************************
 * @brief convert DDM gps into pure decimal
 * @param[in] void
 * @return void
 *******************************************************************************/
void convert_gps_decimal(char *datain, float *cord)
{

	uint8_t temp[COORDINATES_SIZE];

	struct{
		float lati;
		float longt;
	}cor;

	memcpy(&temp[0], datain, COORDINATES_SIZE);

	//latidude
	//convert ascii to decimal
	cor.lati = (float)(temp[2] -= '0') * 10;
	cor.lati += (float)(temp[3] -= '0');

	cor.lati += ((float)(temp[5]-= '0') * 0.1);
	cor.lati += ((float)(temp[6]-= '0') * 0.01);
	cor.lati += ((float)(temp[7]-= '0') * 0.001);
	cor.lati += ((float)(temp[8]-= '0') * 0.0001);

	//convert from minutes to pure decimal
	cor.lati /= 60;

	cor.lati += ((float)(temp[0] -= '0') * 10);
	cor.lati += (float)(temp[1] -= '0');

	//number negative if South
	if(temp[10] == 'S')
	{
		cor.lati *= (-1);
	}

	//longitude
	//convert ascii to decimal
	cor.longt = (float)(temp[15] -= '0') * 10;
	cor.longt += (float)(temp[16] -= '0');

	cor.longt += ((float)(temp[18]-= '0') * 0.1);
	cor.longt += ((float)(temp[19]-= '0') * 0.01);
	cor.longt += ((float)(temp[20]-= '0') * 0.001);
	cor.longt += ((float)(temp[21]-= '0') * 0.0001);

	//convert from minutes to pure decimal
	cor.longt /= 60;

	cor.longt += ((float)(temp[12] -= '0') * 100);
	cor.longt += ((float)(temp[13] -= '0') * 10);
	cor.longt += (float)(temp[14] -= '0');

	//number negative if West
	if(temp[23] == 'W')
	{
		cor.longt *= (-1);
	}

	*cord = cor.lati;
	cord++;
	*cord = cor.longt;

}

/*******************************************************************************
 * @brief get pointer to coordinates decimal
 * @param[in] void
 * @return ptr to address
 *******************************************************************************/
COORD * get_gps_ptr_to_coordinates_decimal(void)
{
	//return coordinates_dec;
	return &coordinates_dec;
}

/*******************************************************************************
 * @brief Get GPS Dataset Content
 * @param[in] ptr to target dataset
 * @return void
 *******************************************************************************/
void get_gps_dataset(GpsDataset * dataset){
    // Prevent the RTOS kernel swapping out the task whilst
    // the dataset is being retrieved.
    vTaskSuspendAll();
    /* Individually copy the GPS Dataset values to the returning structure */
    dataset->latitude = gps_dataset.latitude;
    dataset->longitude = gps_dataset.longitude;
    dataset->accuracy = gps_dataset.accuracy;
    // The operation is complete.  Restart the RTOS kernel.
    xTaskResumeAll();
    return;
}

/*******************************************************************************
 * @brief Get GPS Dataset Status
 * @param[in] void
 * @return bool true for ready to b eread
 *******************************************************************************/
bool get_gps_dataset_is_ready(void){
    return gps_dataset.b_data_ready;
}

/*******************************************************************************
 * @brief Set GPS Dataset Content
 * @param[in] ptr to target dataset
 * @return void
 *******************************************************************************/
void set_gps_dataset(GpsDataset * dataset){
    // Prevent the RTOS kernel swapping out the task whilst
    // the dataset is being retrieved.
    vTaskSuspendAll();
    /* Individually copy the GPS Dataset values to the returning structure */
    gps_dataset.latitude =  dataset->latitude;
    gps_dataset.longitude = dataset->longitude;
    gps_dataset.accuracy =  dataset->accuracy;

    debug_printf(GPS, "LAT %d \r\n", gps_dataset.latitude);
    debug_printf(GPS, "LONG %d \r\n", gps_dataset.longitude);
    // The operation is complete.  Restart the RTOS kernel.
    xTaskResumeAll();
    return;
}

/*******************************************************************************
 * @brief Production test routine to insure unit can receive valid time string
 * @param[in] void
 * @return bool true if done with check or false if still busy
 *******************************************************************************/
bool gps_verifyTimeStringReceptionIsDone(void)
{
	// Wait at least GPS_HW_SEARCH_TIMEOUT_PROD_MS from boot up.
	if(get_systick_ms() < GPS_HW_SEARCH_TIMEOUT_PROD_MS)
	{
		return false;
	}
	// Once GPS shoul dbe ready and sending out strings of data
	if(gprmc_data[1] != 'G')
	{
		// If still nothing give up now
		set_sysmon_fault(GPS_MODULE_NOT_RESPONDING);
		return true;	
	}
	// Has module received valid time string
	if(	( gprmc_data[GPS_GPRMC_FIRST_HOUR_DIGIT_POS] >= '0' ) && 
		( gprmc_data[GPS_GPRMC_FIRST_HOUR_DIGIT_POS] <= '2' ))
	{
		clear_sysmon_fault(GPS_HW_NOT_FOUND);
		clear_sysmon_fault(NO_GPS_LOCK);
		return true;
	}

	// Allow GPS_HW_TIME_STRING_TIMEOUT_PROD_MS to receive time string
	if(get_systick_ms() < GPS_HW_TIME_STRING_TIMEOUT_PROD_MS)
	{
		// Still busy
		return false;
	}
	else
	{
		// It's been too long - give up now
		clear_sysmon_fault(GPS_HW_NOT_FOUND);
		set_sysmon_fault(NO_GPS_LOCK);
		return true;
	}
}
