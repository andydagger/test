/*
==========================================================================
 Name        : hw.h
 Project     : pcore
 Path        : /pcore/src/public/hardware/hw.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 24 Aug 2017
 Description :
==========================================================================
*/


#ifndef HARDWARE_HW_H_
#define HARDWARE_HW_H_

#include "chip.h"
#include "config_pinout.h"
#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Definitions
 ****************************************************************************/
#define IO_IN true
#define IO_OUT false
#define IO_LOW false
#define IO_HIGH true

// SYSTEM
#define hw_kick_watchdog() Chip_GPIO_SetPinToggle(LPC_GPIO, GPIO_WATCHDOG_PORT, GPIO_WATCHDOG_PIN)

// DEBUG
#define debug_uart_tx_byte(x)    Chip_UART_SendBlocking(UART_DEBUG_IF, x, 1)

// LORAWAN
void lorawan_tx_byte(char x);
char lorawan_rx_byte();
uint32_t lorawan_get_int_status();
void lorawan_tx_enable();
void lorawan_tx_disable();
void lorawan_rx_enable();
void lorawan_rx_disable();
void lorawan_irq_enable();
void lorawan_irq_disable();
void lorawan_disable_module();
void lorawan_enable_module();
bool lorawan_was_rx_isr(uint32_t intStatus);
bool lorawan_was_tx_isr(uint32_t intStatus);
void lorawan_set_baudrate(uint32_t baudRate);

// EEPROM
uint8_t eepromWriteByte(uint32_t dstAdd, uint8_t *ptr, uint32_t byteswrt);
uint8_t eepromReadByte(uint32_t dstAdd, uint8_t *ptr, uint32_t byteswrt);

// Power Monitor
void hw_setup_power_monitor_uart_params( uint32_t baudRate, uint32_t commParams);
void hw_power_monitor_uart_tx_enable();
void hw_power_monitor_uart_tx_disable();
void hw_power_monitor_uart_tx_byte(char x);
uint8_t hw_power_monitor_uart_rx_byte();
uint32_t hw_power_monitor_uart_get_int_status();
void hw_power_monitor_uart_irq_enable();
void hw_power_monitor_uart_irq_disable();

// GPS
void hw_disable_gps_module(void);

// Internal Flash
void hw_writeToInternalFlashSingleSector(uint32_t address, uint32_t * ptrData, uint32_t dataLengthBytes);



// DALI
void dali_set_psu(bool x);
#define dali_get_psu() Chip_GPIO_GetPinState(LPC_GPIO, GPIO_DALI_ENA_PORT, GPIO_DALI_ENA_PIN)
//#define dali_enable_irq() NVIC_EnableIRQ(TIMER_DALI_IRQ)
//#define dali_stop_timer() Chip_RIT_Disable(TIMER_DALI_IF)
//#define dali_start_timer(x) Chip_RIT_SetTimerIntervalHz(TIMER_DALI_IF, x); Chip_RIT_Enable(TIMER_DALI_IF)
#define dali_enable_irq() Chip_MRT_SetEnabled(Chip_MRT_GetRegPtr(TIMER_DALI_MRT_IDX))
#define dali_stop_timer() Chip_MRT_SetDisabled(Chip_MRT_GetRegPtr(TIMER_DALI_MRT_IDX))
#define dali_start_timer(x)  hw_setup_mrt_repeat(TIMER_DALI_MRT_IDX, x, 0)
#define dali_in() Chip_GPIO_GetPinState(LPC_GPIO, GPIO_DALI_RX_PORT, GPIO_DALI_RX_PIN)
void dali_out(bool x);

//AN DRIVER
void an_driver_set_an_smooth(bool x);
#define an_driver_set_pwm_dc(x) Chip_SCTPWM_SetDutyCycle( ANA_PWM_SCT, ANA_PWM_IDX, Chip_SCTPWM_PercentageToTicks(ANA_PWM_SCT, x));
#define an_driver_start_pwm() Chip_SCTPWM_Start(ANA_PWM_SCT)
#define an_driver_stop_pwm() Chip_SCTPWM_Stop(ANA_PWM_SCT)
void an_driver_disable_pwm_pin(void);
#define an_driver_enable_pwm_pin() Chip_SWM_FixedPinEnable( ANA_PWM_SCT_PIN_FUNC, true)

// FLASH
#define flash_set_cs() Chip_GPIO_SetPinState(LPC_GPIO, GPIO_FLASH_SPI_CS_PORT,GPIO_FLASH_SPI_CS_PIN, 1)
#define flash_clear_cs() Chip_GPIO_SetPinState(LPC_GPIO, GPIO_FLASH_SPI_CS_PORT,GPIO_FLASH_SPI_CS_PIN, 0)

/**
 * @brief	I/O Control pin mux
 * @param	pIOCON	: The base of IOCON peripheral on the chip
 * @param	port	: GPIO port to mux
 * @param	pin		: GPIO pin to mux
 * @param	mode	: OR'ed values or type IOCON_*
 * @param	func	: Pin function, value of type IOCON_FUNC?
 * @return	Nothing
 */
//STATIC INLINE void Chip_IOCON_PinMux(LPC_IOCON_T *pIOCON, uint8_t port, uint8_t pin, uint16_t mode, uint8_t func)
//{
//	Chip_IOCON_PinMuxSet(pIOCON, port, pin, (uint32_t) (mode | func));
//}

// #define

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void hw_setup(void);
void hw_setup_mrt_repeat(uint8_t ch, uint32_t rate, float ppm);

#endif /* HARDWARE_HW_H_ */
