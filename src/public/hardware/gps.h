/*
==========================================================================
 Name        : gps.h
 Project     : pcore
 Path        : /pcore/src/public/hardware/gps.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 18 Apr 2018
 Description : 
==========================================================================
*/

#ifndef PUBLIC_HARDWARE_GPS_H_
#define PUBLIC_HARDWARE_GPS_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/

#include "config_dataset.h"
#include "stdbool.h"
#include "stdint.h"

#define TIMESTAMP_DATE_SIZE 12
#define TIMESTAMP_SIZE 6
#define DATESTAMP_SIZE 6
#define COORDINATES_SIZE 24
#define GPS_HW_SEARCH_TIMEOUT 		5000
#define GPS_HW_SEARCH_TIMEOUT_PROD_MS	5000
#define GPS_HW_TIME_STRING_TIMEOUT_PROD_MS	10000
#define GPS_GPRMC_FIRST_HOUR_DIGIT_POS	7
#define GPS_COORDS_SEARCH_TIMEOUT 	(60000)*15		// 15 Mins


/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
//#define TIMESTAMP_DATE_SIZE 12
//#define TIMESTAMP_SIZE 6
//#define DATESTAMP_SIZE 6
//#define COORDINATES_SIZE 24

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
typedef struct
{
	float latitude;
	float longitude;
}COORD;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void gps_rx_start(uint8_t addr);
uint8_t gps_rx_byte(uint8_t data);
void gps_rx_done(void);
uint8_t gps_tx_byte(uint8_t *data);

bool gps_parsed_locked_data_accurate(void);
bool gps_parsed_locked_data(void);
char * get_gps_ptr_to_coordinates_str(void);
char * get_tms_ptr_to_timestamp_str(void);
bool gps_get_utc(char *data);
bool timestamp_parsed_locked_data(void);
bool get_gps_lock(void);
void convert_gps_decimal(char *datain, float *cord);
COORD * get_gps_ptr_to_coordinates_decimal(void);
void gps_disable_module( void );

void get_gps_dataset(GpsDataset * dataset);
bool get_gps_dataset_is_ready(void);
void set_gps_dataset(GpsDataset * dataset);

bool gps_verifyTimeStringReceptionIsDone(void);

#endif /* PUBLIC_HARDWARE_GPS_H_ */
