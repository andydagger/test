/*
==========================================================================
 Name        : relay.h
 Project     : pcore
 Path        : /pcore/src/public/hardware/relay.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 25 Aug 2017
 Description : 
==========================================================================
*/


#ifndef HARDWARE_RELAY_H_
#define HARDWARE_RELAY_H_

#include "stdbool.h"

/*****************************************************************************
 * Definitions
 ****************************************************************************/
#define RELAY_OPEN true
#define RELAY_CLOSE !RELAY_OPEN

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void relay_update(bool state);
void relay_update_wait(bool state);
void relay_service_pulse(void);
bool b_get_relay_state(void);


#endif /* HARDWARE_RELAY_H_ */
