/*
==========================================================================
 Name        : relay.c
 Project     : pcore
 Path        : /pcore/src/public/hardware/relay.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 25 Aug 2017
 Description : 
==========================================================================
*/


/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "chip.h"
#include "stdbool.h"
#include "stdint.h"
#include "config_pinout.h"
#include "relay.h"
#include "config_relay.h"
#include "systick.h"



/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
uint32_t relay_pulse_end_ms = 0;
static bool b_relay_state = RELAY_CLOSE;
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void relay_end_pulse(void);

/*****************************************************************************
 * Public functions
 ****************************************************************************/


/*******************************************************************************
 * @brief Starts relay change pulse
 * @param[in] bool with true for close and false for open
 * @return void
 *******************************************************************************/
void relay_update(bool state) {

	 b_relay_state = state;

     Chip_GPIO_SetPinState( LPC_GPIO,
                            GPIO_RELAY_CLOSE_PORT,
                            GPIO_RELAY_CLOSE_PIN,
                            !state);

     Chip_GPIO_SetPinState( LPC_GPIO,
                            GPIO_RELAY_OPEN_PORT,
                            GPIO_RELAY_OPEN_PIN,
                            state);

     relay_pulse_end_ms = get_systick_ms() + RELAY_PULSE_LENGTH_MS;

}

/*******************************************************************************
 * @brief Relay change pulse - wait for pulse length
 * @param[in] bool with true for close and false for open
 * @return void
 *******************************************************************************/
void relay_update_wait(bool state) {

    b_relay_state = state;

     Chip_GPIO_SetPinState( LPC_GPIO,
                            GPIO_RELAY_CLOSE_PORT,
                            GPIO_RELAY_CLOSE_PIN,
                            !state);

     Chip_GPIO_SetPinState( LPC_GPIO,
                            GPIO_RELAY_OPEN_PORT,
                            GPIO_RELAY_OPEN_PIN,
                            state);

     relay_pulse_end_ms = get_systick_ms() + RELAY_PULSE_LENGTH_MS;


    // wait for pulse length
    while(get_systick_ms() < relay_pulse_end_ms);

    // end pulse
    relay_end_pulse();

    return;

}

/*******************************************************************************
 * @brief Terminates relay change pulses
 * @param[in] void
 * @return void
 *******************************************************************************/
static void relay_end_pulse(void) {

     Chip_GPIO_SetPinState( LPC_GPIO,
                            GPIO_RELAY_CLOSE_PORT,
                            GPIO_RELAY_CLOSE_PIN,
                            false);
     Chip_GPIO_SetPinState( LPC_GPIO,
                            GPIO_RELAY_OPEN_PORT,
                            GPIO_RELAY_OPEN_PIN,
                            false);
}

/*******************************************************************************
 * @brief Calls function to terminate pulse following timeout
 * @param[in] void
 * @return void
 *******************************************************************************/
void relay_service_pulse(void){

    if( get_systick_ms() > relay_pulse_end_ms ){
        relay_end_pulse();
    }else{
        // TODO: wait for pulse to expire
    }

    return;
}
/*******************************************************************************
 * @brief returns relay state. false = open, true = closed
 * @param[in] void
 * @return b_relay_state
 *******************************************************************************/
bool b_get_relay_state(void)
{
	return b_relay_state;
}
