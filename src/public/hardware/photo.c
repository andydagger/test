/*
==========================================================================
 Name        : photo.c
 Project     : pcore
 Path        : /pcore/src/public/hardware/photo.c
 Author      : M Nakhuda
 Copyright   : Lucy Zodion Ltd
 Created     : 20 May 2019
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "stdbool.h"
#include "stdint.h"
#include "adc_procx.h"
#include "photo.h"
#include "debug.h"
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define PHOTO_AVG_CYCLES 20 /* 2 seconds at 10Hz servicing*/
#define ADC_TO_LUX_COEFF (float)(0.077)
#define ADC_OFFSET  (float)10

static float calibCoeff = 1.0;
static float calc_lux_av = 0;
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

//-----------------------------------------------------------------------------
void photo_init(void){
    debug_printf(   MAIN,
                    "lux calib coeff (*1000): %lu\r\n",
                    (uint32_t)(1000*calibCoeff));
}

//-----------------------------------------------------------------------------
float get_photo_LUX(void){
	return calc_lux_av;
}


//-----------------------------------------------------------------------------
void photo_service( void )
{
	static uint8_t calc_lux_count = 0;
    float av_adc_val = 0;
    float corrected_adc_val = 0;
    float calc_lux_spl = 0;
    static float calc_lux_sum = 0;

    // Fetch the raw adc average
    av_adc_val = get_adc_procx_photo_ic_av_raw();

    // Take out offset
    corrected_adc_val =     ( av_adc_val > ADC_OFFSET ) ?
                            ( av_adc_val - ADC_OFFSET ) :
                            ( 0 );

    // Multiply by coeff and store current sample
    calc_lux_spl = corrected_adc_val * ADC_TO_LUX_COEFF;

    // Apply second stage calibration
    calc_lux_spl = calc_lux_spl * calibCoeff;

    // Accumulate and sum all the samples
    calc_lux_sum += calc_lux_spl;
    calc_lux_count++;

    // Have we got enough samples
    if( PHOTO_AVG_CYCLES <= calc_lux_count )
    {
    	// Calculate the average
    	calc_lux_av = (calc_lux_sum / PHOTO_AVG_CYCLES );

    	// Reset back to zero ready for the next calculation
    	calc_lux_sum = 0;
    	calc_lux_count = 0;
    }
}

//-----------------------------------------------------------------------------
void photo_setCalibCoeff(float value)
{
	calibCoeff = value;
}

//-----------------------------------------------------------------------------
float photo_getCalibCoeff()
{
	return calibCoeff;
}
