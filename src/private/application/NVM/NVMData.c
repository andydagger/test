/*
 * NonVolatileMemory.c
 *
 *  Created on: 20 Apr 2021
 *      Author: benraiton
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "debug.h"
#include "config_lorawan.h"
#include "config_eeprom.h"
#include "hw.h"
#include "NonVolatileMemory.h"
#include "NVMData.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

// Used to verify flash calibration values when migrating
// values into EEprom.
#define STM8_CALIB_RANGE_MIN     -2.0f
#define STM8_CALIB_RANGE_MAX     2.0f

/*****************************************************************************
 * Private constants
 ****************************************************************************/

// default error code
static const NVM_errorCode_t defaultErrorCode =
{
    .version = NVM_ERROR_CODE_STRUCT_VER,     // Current version of the structure
    .id      = NVM_DEFAULT_ERROR_CODE_ID,     // default ID
    .value   = NVM_DEFAULT_ERROR_CODE_VALUE,  // default associated value
    .crc     = 0                              // Checksum
};

// default metering calibration coefficients
static const NVM_meteringCalibCoef_t defaultMeteringCalibCoef =
{
    .version     = NVM_METERING_CALIB_COEF_STRUCT_VER,  // Current version of the structure
    .calibPassed = NVM_DEFAULT_METERING_CALIB_PASSED,
    .voltage     = NVM_DEFAULT_METERING_CALIB_VOLTAGE,
    .current     = NVM_DEFAULT_METERING_CALIB_CURRENT,
    .power       = NVM_DEFAULT_METERING_CALIB_POWER,
    .crc         = 0                                    // Checksum
};

// default lamp output override
static const NVM_lampOutputOverride_t defaultLampOutputOverride =
{
    .version      = NVM_LAMP_OUTPUT_OVERRIDE_STRUCT_VER,  // Current version of the structure
    .state        = NVM_DEFAULT_LOO_STATE,
    .outputLevel  = NVM_DEFAULT_LOO_OUTPUT_LEVEL,
    .startTimeUtc = NVM_DEFAULT_LOO_START_TIME,
    .stopTimeUtc  = NVM_DEFAULT_LOO_STOP_TIME,
    .crc          = 0                                     // Checksum
};

// default dali addresses
static const NVM_DaliAddresses_t defaultDaliAddresses =
{
    .version     = NVM_DALI_ADDRESSES_STRUCT_VER,  // Current version of the structure
    .addressMask = NVM_DEFAULT_DALI_ADDRESSES_MASK,
    .crc         = 0                                     // Checksum
};

// default runtime params, these are params adjustable via the network.
static const NVM_RuntimeParams_t default_runtime_params =
{
    .version       = NVM_RUNTIME_PARAMS_STRUCT_VER,  // Current version of the structure
    .status_period = NVM_DEFAULT_STATUS_PERIOD,
    .crc           = 0
};

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Called on power-up or reset before starting FreeRTOS, validates
 *        each structure in EEprom and restores back to defaults if required.
 *
 * @param[in] void
 *
 * @return    NVM_result_t - the result of the initialisation
 *
 *******************************************************************************/
NVM_result_t NVMData_initialise( void)
{
    NVM_result_t result;

    result = NVM_initialise();

    NVMData_readAndValidateErrorCode();

    NVMData_readAndValidateMeteringCalibCoef();

    NVMData_readAndValidateLampOutputOverride();

    NVMData_readAndValidateDaliAddresses();

    NVMData_readAndValidateRuntimeParams();

    return result;
}

/*******************************************************************************
 * @brief restores all EEprom structures back to default values
 *        this is used by the production ATE code.
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_restoreAll( void)
{
    NVMData_restoreErrorCode();

    NVMData_restoreMeteringCalibCoef();

    NVMData_restoreLampOutputOverride();

    NVMData_restoreDaliAddresses();

    NVMData_restoreRuntimeParams();
}

/*******************************************************************************
 * @brief restores to defaults values the error codes structure
 *        and writes it to EEprom
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_restoreErrorCode( void)
{
    NVM_errorCode_t errorCode;

    memcpy( (void*)&errorCode,
            (const void*)&defaultErrorCode,
            sizeof( NVM_errorCode_t));

    NVM_write( NVM_ADDRESS_ERROR_CODE,
               (uint8_t*)&errorCode,
               sizeof( NVM_errorCode_t));
}

/*******************************************************************************
 * @brief reads the error codes structure from EEprom
 *
 * @param[in] NVM_errorCode_t * - pointer to struct to read to
 *
 * @return    NVM_result_t - result of the read
 *
 *******************************************************************************/
NVM_result_t NVMData_readErrorCode( NVM_errorCode_t * errCodePtr)
{
    NVM_result_t result = NVM_RESULT_OK;

    result = NVM_read( NVM_ADDRESS_ERROR_CODE,
                       (uint8_t*)errCodePtr,
                       sizeof( NVM_errorCode_t));

    if( result == NVM_RESULT_OK)
    {
        if( errCodePtr->version != NVM_ERROR_CODE_STRUCT_VER)
        {
            debug_printf(NVM, "Invalid structure version -> error code\r\n");

            result = NVM_RESULT_ERROR_VERSION;
        }
    }

    return result;
}

/*******************************************************************************
 * @brief writes the error codes structure to EEprom including calculating
 *        the structure CRC
 *
 * @param[in] NVM_errorCode_t * - pointer to struct to write
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_writeErrorCode( NVM_errorCode_t * errCodePtr)
{
    NVM_write( NVM_ADDRESS_ERROR_CODE,
               (uint8_t*)errCodePtr,
               sizeof( NVM_errorCode_t));
}

/*******************************************************************************
 * @brief restores to defaults values the metering calibration coefficients
 *        structure and writes it to eeprom
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_restoreMeteringCalibCoef( void)
{
    NVM_meteringCalibCoef_t meteringCalibCoef;

    memcpy( (void*)&meteringCalibCoef,
            (const void*)&defaultMeteringCalibCoef,
            sizeof( NVM_meteringCalibCoef_t));

    NVM_write( NVM_ADDRESS_METERING_CALIB_COEF,
               (uint8_t*)&meteringCalibCoef,
               sizeof( NVM_meteringCalibCoef_t));
}

/*******************************************************************************
 * @brief reads the metering calibration coefficients structure from EEprom
 *
 * @param[in] NVM_meteringCalibCoef_t * pointer to structure to read
 *
 * @return    NVM_result_t - result of the read operation
 *
 *******************************************************************************/
NVM_result_t NVMData_readMeteringCalibCoef( NVM_meteringCalibCoef_t * meterCalCoefPtr)
{
    NVM_result_t result;

    result = NVM_read( NVM_ADDRESS_METERING_CALIB_COEF,
                       (uint8_t*)meterCalCoefPtr,
                       sizeof( NVM_meteringCalibCoef_t));

    if( result == NVM_RESULT_OK)
    {
        if( meterCalCoefPtr->version != NVM_METERING_CALIB_COEF_STRUCT_VER)
        {
            debug_printf(NVM, "Invalid structure version -> metering calib coef\r\n");

            result = NVM_RESULT_ERROR_VERSION;
        }
    }

    return result;
}

/*******************************************************************************
 * @brief writes the metering calibration coefficients structure to EEprom
 *        including calculating the structure CRC
 *
 * @param[in] NVM_meteringCalibCoef_t * - pointer to structure to write
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_writeMeteringCalibCoef( NVM_meteringCalibCoef_t * meterCalCoefPtr)
{
    NVM_write( NVM_ADDRESS_METERING_CALIB_COEF,
               (uint8_t*)meterCalCoefPtr,
               sizeof( NVM_meteringCalibCoef_t));
}


/*******************************************************************************
 * @brief restores to defaults values the lamp output override structure
 *        and writes it to eeprom
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_restoreLampOutputOverride( void)
{
    NVM_lampOutputOverride_t  lampOutputOverride;

    memcpy( (void*)&lampOutputOverride,
            (const void*) &defaultLampOutputOverride,
            sizeof( NVM_lampOutputOverride_t));

    NVM_write( NVM_ADDRESS_LAMP_OUTPUT_OVERRIDE,
               (uint8_t*)&lampOutputOverride,
               sizeof( NVM_lampOutputOverride_t));
}

/*******************************************************************************
 * @brief reads the lamp output override structure from EEprom
 *
 * @param[in] NVM_lampOutputOverride_t * - pointer to structure to read
 *
 * @return    NVM_result_t - result of the read operation
 *
 *******************************************************************************/
NVM_result_t NVMData_readLampOutputOverride( NVM_lampOutputOverride_t * looPtr)
{
    NVM_result_t result;

    result = NVM_read( NVM_ADDRESS_LAMP_OUTPUT_OVERRIDE,
                       (uint8_t*)looPtr,
                       sizeof( NVM_lampOutputOverride_t));

    if( result == NVM_RESULT_OK)
    {
        if( looPtr->version != NVM_LAMP_OUTPUT_OVERRIDE_STRUCT_VER)
        {
            debug_printf(NVM, "Invalid structure version -> lamp output override\r\n");

            result = NVM_RESULT_ERROR_VERSION;
        }
    }

    return result;
}

/*******************************************************************************
 * @brief writes the lamp output override structure to EEprom
 *
 * @param[in] NVM_lampOutputOverride_t * - pointer to structure to write
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_writeLampOutputOverride( NVM_lampOutputOverride_t * looPtr)
{
    NVM_write( NVM_ADDRESS_LAMP_OUTPUT_OVERRIDE,
               (uint8_t*)looPtr,
               sizeof( NVM_lampOutputOverride_t));
}


/*******************************************************************************
 * @brief restores to defaults values to the dali addresses structure
 *        and writes it to EEprom
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_restoreDaliAddresses( void)
{
    NVM_DaliAddresses_t daliAddresses;

    memcpy( (void*)&daliAddresses,
            (const void*)&defaultDaliAddresses,
            sizeof( NVM_DaliAddresses_t));

    NVM_write( NVM_ADDRESS_DALI_ADDRESSES,
               (uint8_t*)&daliAddresses,
               sizeof( NVM_DaliAddresses_t));
}

/*******************************************************************************
 * @brief reads the dali addresses structure from EEprom
 *
 * @param[in] NVM_errorCode_t * - pointer to struct to read to
 *
 * @return    NVM_result_t - result of the read
 *
 *******************************************************************************/
NVM_result_t NVMData_readDaliAddresses( NVM_DaliAddresses_t * daliAddressesPtr)
{
    NVM_result_t result = NVM_RESULT_OK;

    result = NVM_read( NVM_ADDRESS_DALI_ADDRESSES,
                       (uint8_t*)daliAddressesPtr,
                       sizeof( NVM_DaliAddresses_t));

    if( result == NVM_RESULT_OK)
    {
        if( daliAddressesPtr->version != NVM_ERROR_CODE_STRUCT_VER)
        {
            debug_printf(NVM, "Invalid structure version -> Dali Addresses\r\n");

            result = NVM_RESULT_ERROR_VERSION;
        }
    }

    return result;
}

/*******************************************************************************
 * @brief writes the dali addresses structure to EEprom including calculating
 *        the structure CRC
 *
 * @param[in] NVM_errorCode_t * - pointer to struct to write
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_writeDaliAddresses( NVM_DaliAddresses_t * daliAddressesPtr)
{
    NVM_write( NVM_ADDRESS_DALI_ADDRESSES,
               (uint8_t*)daliAddressesPtr,
               sizeof(NVM_DaliAddresses_t));
}


/*******************************************************************************
 * @brief restores to defaults values to the runtime params structure
 *        and writes it to EEprom
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_restoreRuntimeParams(void)
{
    NVM_RuntimeParams_t runtime_params;

    memcpy((void*)&runtime_params,
           (const void*)&default_runtime_params,
           sizeof(NVM_RuntimeParams_t));

    NVM_write(NVM_ADDRESS_RUNTIME_PARAMS,
              (uint8_t*)&runtime_params,
              sizeof(NVM_RuntimeParams_t));
}

/*******************************************************************************
 * @brief reads the runtime params structure from EEprom
 *
 * @param[in] NVM_RuntimeParams_t * - pointer to struct to read to
 *
 * @return    NVM_result_t - result of the read
 *
 *******************************************************************************/
NVM_result_t NVMData_readRuntimeParams(NVM_RuntimeParams_t * runtimeParamsPtr)
{
    NVM_result_t result = NVM_RESULT_OK;

    result = NVM_read(NVM_ADDRESS_RUNTIME_PARAMS,
                      (uint8_t*)runtimeParamsPtr,
                      sizeof(NVM_RuntimeParams_t));

    if (result == NVM_RESULT_OK)
    {
        if (runtimeParamsPtr->version != NVM_ERROR_CODE_STRUCT_VER)
        {
            debug_printf(NVM, "Invalid structure version -> Runtime Params\r\n");

            result = NVM_RESULT_ERROR_VERSION;
        }
    }

    return result;
}

/*******************************************************************************
 * @brief writes the runtime params structure to EEprom including calculating
 *        the structure CRC
 *
 * @param[in] NVM_RuntimeParams_t * - pointer to struct to write
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_writeRuntimeParams(NVM_RuntimeParams_t * runtimeParamsPtr)
{
    NVM_write(NVM_ADDRESS_RUNTIME_PARAMS,
              (uint8_t*)runtimeParamsPtr,
              sizeof( NVM_RuntimeParams_t));
}

/*******************************************************************************
 * @brief reads and validates the error code structure integrity by checking
 *        the CRC and structure version
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_readAndValidateErrorCode(void)
{
    NVM_result_t    result = NVM_RESULT_OK;
    NVM_errorCode_t errorCode;

    // read the structure
    result = NVMData_readErrorCode(&errorCode);

    if (result != NVM_RESULT_OK)
    {
        NVMData_restoreErrorCode();
    }
}

/*******************************************************************************
 * @brief reads and validates the metering calibration coefficients structure
 *        integrity by checking the CRC and structure version also migrates the
 *        legacy values stored in flash to the EEprom structure
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_readAndValidateMeteringCalibCoef(void)
{
    NVM_result_t result = NVM_RESULT_OK;
    NVM_meteringCalibCoef_t meteringCalibCoef;
    float voltage;
    float current;
    float power;

    // read the structure
    result = NVMData_readMeteringCalibCoef( &meteringCalibCoef);

    // validate if read OK
    if( result != NVM_RESULT_OK)
    {
        // A read error may be due to upgrading to this code and legacy code has
        // calibration information in flash, so we need to check the values in flash
        // and verify the new structure is blank
        // this should be a one time operation to move values from flash to eeprom

        // is current structure blank??
        if(( meteringCalibCoef.version == 0xFF) &&
           ( meteringCalibCoef.crc     == 0xFFFF))
        {
            // structure is blank -> are the flash locations blank??
            if(( *((uint32_t*)FLASH_ADDR_PWR_VOLT_CALIB_COEFF) != 0xFFFFFFFF) &&
               ( *((uint32_t*)FLASH_ADDR_PWR_CUR_CALIB_COEFF)  != 0xFFFFFFFF) &&
               ( *((uint32_t*)FLASH_ADDR_PWR_PWR_CALIB_COEFF)  != 0xFFFFFFFF))
            {
                // get the values as floats and make sure they are all in range
                voltage = *((float*)FLASH_ADDR_PWR_VOLT_CALIB_COEFF);
                current = *((float*)FLASH_ADDR_PWR_CUR_CALIB_COEFF);
                power   = *((float*)FLASH_ADDR_PWR_PWR_CALIB_COEFF);

                // Are calib values in range??
                if((( voltage >= STM8_CALIB_RANGE_MIN) && ( voltage <= STM8_CALIB_RANGE_MAX)) &&
                   (( current >= STM8_CALIB_RANGE_MIN) && ( current <= STM8_CALIB_RANGE_MAX)) &&
                   (( power   >= STM8_CALIB_RANGE_MIN) && ( power   <= STM8_CALIB_RANGE_MAX)))
                {
                    // create the new structure based on the legacy flash values
                    meteringCalibCoef.version     = NVM_METERING_CALIB_COEF_STRUCT_VER;
                    meteringCalibCoef.calibPassed = true;
                    meteringCalibCoef.voltage     = voltage;
                    meteringCalibCoef.current     = current;
                    meteringCalibCoef.power       = power;

                    NVMData_writeMeteringCalibCoef( &meteringCalibCoef);

                    debug_printf(NVM, "Migrated power calib values from flash to EEprom\r\n");
                    return;
                }
            }
        }

        // All flash locations are blank so restore the calib structure
        NVMData_restoreMeteringCalibCoef();
    }
}


/*******************************************************************************
 * @brief reads and validates the lamp output override structure integrity by
 *        checking the CRC and structure version
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_readAndValidateLampOutputOverride(void)
{
    NVM_result_t result;
    NVM_lampOutputOverride_t lampOutputOverride;

    // read the structure
    result = NVMData_readLampOutputOverride(&lampOutputOverride);

    if (result != NVM_RESULT_OK)
    {
        NVMData_restoreLampOutputOverride();
    }
}


/*******************************************************************************
 * @brief reads and validates the dali addresses structure integrity by
 *        checking the CRC and structure version
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_readAndValidateDaliAddresses(void)
{
    NVM_result_t result;
    NVM_DaliAddresses_t daliAddresses;

    // read the structure
    result = NVMData_readDaliAddresses(&daliAddresses);

    if (result != NVM_RESULT_OK)
    {
        NVMData_restoreDaliAddresses();
    }
}

/*******************************************************************************
 * @brief reads and validates the runtime params structure integrity by
 *        checking the CRC and structure version
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void NVMData_readAndValidateRuntimeParams( void)
{
    NVM_result_t result;
    NVM_RuntimeParams_t runtimeParams;

    // read the structure
    result = NVMData_readRuntimeParams(&runtimeParams);

    if (result != NVM_RESULT_OK)
    {
        NVMData_restoreRuntimeParams();
    }
}

/*------------------------------ End of File -------------------------------*/

