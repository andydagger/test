/*
 * NVMData.h
 *
 *  Created on: 20 Apr 2021
 *      Author: benraiton
 */

#ifndef PRIVATE_APPLICATION_NVM_NVMDATA_H_
#define PRIVATE_APPLICATION_NVM_NVMDATA_H_

#include "NonVolatileMemory.h"
#include "config_eeprom.h"

/******************************************************************************
 * Public functions
 *****************************************************************************/
NVM_result_t NVMData_initialise(void);
void NVMData_restoreAll(void);

void NVMData_restoreErrorCode(void);
NVM_result_t NVMData_readErrorCode(NVM_errorCode_t * errCodePtr);
void NVMData_writeErrorCode(NVM_errorCode_t * errCodePtr);

void NVMData_restoreMeteringCalibCoef(void);
NVM_result_t NVMData_readMeteringCalibCoef(NVM_meteringCalibCoef_t * meterCalCoefPtr);
void NVMData_writeMeteringCalibCoef(NVM_meteringCalibCoef_t * meterCalCoefPtr);

void NVMData_restoreLampOutputOverride(void);
NVM_result_t NVMData_readLampOutputOverride(NVM_lampOutputOverride_t * looPtr);
void NVMData_writeLampOutputOverride(NVM_lampOutputOverride_t * looPtr);

void NVMData_restoreDaliAddresses(void);
NVM_result_t NVMData_readDaliAddresses(NVM_DaliAddresses_t * daliAddressesPtr);
void NVMData_writeDaliAddresses(NVM_DaliAddresses_t * daliAddressesPtr);

void NVMData_restoreRuntimeParams(void);
NVM_result_t NVMData_readRuntimeParams(NVM_RuntimeParams_t * runtimeParamsPtr);
void NVMData_writeRuntimeParams(NVM_RuntimeParams_t * runtimeParamsPtr);

void NVMData_readAndValidateErrorCode(void);
void NVMData_readAndValidateMeteringCalibCoef(void);
void NVMData_readAndValidateLampOutputOverride(void);
void NVMData_readAndValidateDaliAddresses(void);
void NVMData_readAndValidateRuntimeParams(void);


#endif /* PRIVATE_APPLICATION_NVM_NVMDATA_H_ */
/*------------------------------ End of File -------------------------------*/
