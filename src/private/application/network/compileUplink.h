/*
 * compileUplink.h
 *
 *  Created on: 14 Apr 2021
 *      Author: benraiton
 */

#ifndef PRIVATE_APPLICATION_NETWORK_COMPILEUPLINK_H_
#define PRIVATE_APPLICATION_NETWORK_COMPILEUPLINK_H_

#include <stdint.h>
#include <stdbool.h>
#include "config_dataset.h"

void compileUplink_Fault(bool msgPriority, uint8_t QoS, uint32_t faulFlags);
void compileUplink_Initial(void);
void compileUplink_Status(bool applyDelay);
void compileUplink_Daily(BillingDataset billing_data);
void compileUplink_MissedPackets(uint32_t transmitDelay, char * pointerToMissedPacketsPayload);
void compileUplink_CalendarConfirmation(    uint32_t transmitDelay, 
                                            uint32_t calendarFileRef, 
                                            uint16_t calendarVersion);

void compileUplink_ProductionTestReport(void);
void compileUplink_ProductionTestingDone(void);
uint32_t compileUplink_calculate_jitter(void);

#endif /* PRIVATE_APPLICATION_NETWORK_COMPILEUPLINK_H_ */
