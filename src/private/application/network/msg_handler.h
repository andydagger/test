/*
==========================================================================
 Name        : msg_handler.h
 Project     : pcore
 Path        : /pcore/src/private/application/network/msg_handler.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 8 Jan 2019
 Description : 
==========================================================================
*/

#ifndef PRIVATE_APPLICATION_NETWORK_MSG_HANDLER_H_
#define PRIVATE_APPLICATION_NETWORK_MSG_HANDLER_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#define QOS_0_DATA_LENGTH_ASCII  (51*2+1)
#define QOS_0_BUFFER_DEPTH  4

#define QOS_1_DATA_LENGTH_ASCII         ((3+10)*2+1)
#define QOS_1_BUFFER_DEPTH             8
// The ring buffer code in lpc_chip_43xx/src/ring_buffer.c only works
// correct if the buffer size is a power of 2.

#define QOS_2_DATA_LENGTH_ASCII  (51*2+1)
#define QOS_2_BUFFER_DEPTH      2

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void msg_handler_run(void);
void msg_handler_load(QuNwkComMsg msg_to_send);
void msg_handler_init(void);

#endif /* PRIVATE_APPLICATION_NETWORK_MSG_HANDLER_H_ */
