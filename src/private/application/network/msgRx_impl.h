/*
 * msgRx_impl.h
 *
 *  Created on: 1 Jun 2021
 *      Author: benraiton
 */

#ifndef PRIVATE_APPLICATION_NETWORK_MSGRX_IMPL_H_
#define PRIVATE_APPLICATION_NETWORK_MSGRX_IMPL_H_

#include <stdbool.h>
#include <stdint.h>

bool receivedReferenceCalibData(void);
bool receivedNodeData(void);
bool received_status_period(void);
bool received_driver_reset(void);

#endif /* PRIVATE_APPLICATION_NETWORK_MSGRX_IMPL_H_ */
