/*
==========================================================================
 Name        : nwk_com.c
 Project     : pcore
 Path        : /pcore/src/private/application/network/nwk_com.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 23 May 2018
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "stdio.h"
#include "string.h"
#include "stdarg.h"
#include "nwk_com.h"
#include "nwk_tek.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "queue.h"
#include "timers.h"
#include "task.h"
#include "config_timers.h"
#include "msg_rx.h"
#include "sysmon.h"
#include "config_network.h"
#include "config_queues.h"
#include "LorawanStatusFlags.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
extern TimerHandle_t xTimer[NUM_TIMERS];

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
QueueHandle_t xQuNwkCom = NULL;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * Initialise network staclk
 *******************************************************************************/
bool nwk_com_init(){
    // If flag was set at run time do not allow sesion restore
    bool moduleFullResetFromRunTime = LorawanStatusFlags_GetModuleNeedsInitialising();
    bool requestSessionRestore = !moduleFullResetFromRunTime;
    if(moduleFullResetFromRunTime)
    {
        LorawanStatusFlags_ClearJoinedFlag();
    }
    if( nwk_tek_init( &requestSessionRestore ) == true)
    {
        // Was able to restore sessin so force joined flag to set.
        if(requestSessionRestore) LorawanStatusFlags_SetJoinedFlag();
        LorawanStatusFlags_ClearModuleNeedsInitialising();
        return true;
    }
    else
    {
        return false;
    }
}


/*******************************************************************************
 * Reconnect to the network
 *******************************************************************************/
bool nwk_com_connect(void){
    bool restoringSession = LorawanStatusFlags_GetJoinedFlag();
    if( nwk_tek_connect(restoringSession) == true)
    {
        LorawanStatusFlags_SetJoinedFlag();
        nwk_tek_save_session_info();
        return true;
    }
    else
    {
        LorawanStatusFlags_ClearJoinedFlag();
        return false;
    }
}

/*******************************************************************************
 * Send passed message as next uplink
 *******************************************************************************/
bool nwk_com_send_msg(char * p_to_data, bool b_msg_requires_acked){

    // Fail-safes
    if(p_to_data == NULL) return false;
    if(strlen(p_to_data) == 0) return false;
    if(strlen(p_to_data) > UP_MSG_PAYLOAD_ASCII_WITH_TERM) return false;

    // If we're running on super-cap save latest session info
    // Run before sending command as well in case DC limit is hit
    // and function call never returns before super-cap discharged
    if( true == get_sysmon_last_gasp_is_active()){
        // Save session info
        nwk_com_save_session_info();
    }

    bool b_res =  nwk_tek_send_msg( p_to_data, b_msg_requires_acked);

    // Save session after transmit as well
    if( true == get_sysmon_last_gasp_is_active()){
        // Save session info
        nwk_com_save_session_info();
    }

    return b_res;
}

/*******************************************************************************
 * Read message received by nwetork stack
 *******************************************************************************/
bool nwk_com_read_msg(void){
    char * ptr_to_rx_data = NULL;

    // if broadcast session running reset time-out
    if( get_nwk_com_broadcast_is_active() == true){
        xTimerReset(xTimer[BCAST_TIME_OUT], 0);
    }

    ptr_to_rx_data = nwk_tek_read_msg();

    if( NULL != ptr_to_rx_data){
        msg_rx_process(ptr_to_rx_data);
        return true;
    }else{
        return false;
    }
}

/*******************************************************************************
 * Start or Terminate broadcast session
 *******************************************************************************/
bool nwk_com_update_broabcast_status(bool b_new_mcast_status){
    // broadcast session time-out management
    if(b_new_mcast_status){
        // start time-out timer at start of session
        xTimerStart(xTimer[BCAST_TIME_OUT], 0);
    }else{
        // Stop time-out timer when session is terminated
        xTimerStop(xTimer[BCAST_TIME_OUT], 0);
    }
    bool currentSessionStatus = LorawanStatusFlags_GetMcastSessionIsActive();
    if(currentSessionStatus == b_new_mcast_status)
    {
        return true;
    }
    if( true == nwk_tek_update_broabcast_status(b_new_mcast_status))
    {
        LorawanStatusFlags_UpdateMcastStatus(b_new_mcast_status);
        return true;
    }
    else
    {
        return false;
    }
}

/*******************************************************************************
 * Get status of broadcast session
 *******************************************************************************/
bool get_nwk_com_broadcast_is_active(void){
    return LorawanStatusFlags_GetMcastSessionIsActive();
}

/*******************************************************************************
 * Request a change in mcast session status
 *******************************************************************************/
void set_nwk_com_broadcast_session_should_be_active(bool b_request){
    LorawanStatusFlags_SetMcastSessionShouldBeActive( b_request );
}

/*******************************************************************************
 * Checkfor a requested change in mcast session status
 *******************************************************************************/
bool get_nwk_com_broadcast_session_should_be_active(void){
    return LorawanStatusFlags_GetMcastSessionShouldBeActive();
}

/*******************************************************************************
 * Broadcast Session time-out call-bac: terminate session
 *******************************************************************************/
void vBroadcastTimeOutCallBack( void ){
    LorawanStatusFlags_SetMcastSessionShouldBeActive(false);
    return;
}

/*******************************************************************************
 * fetch connection status flag
 *******************************************************************************/
bool get_nwk_com_is_connected(void){
    return LorawanStatusFlags_GetJoinedFlag();
}

/*******************************************************************************
 * fetch hw initi request
 *******************************************************************************/
bool get_nwk_com_hw_needs_initialising(void){
    return LorawanStatusFlags_GetModuleNeedsInitialising();
}


/*******************************************************************************
 * find out if a msg has been received
 *******************************************************************************/
bool get_nwk_com_rx_msg_pending(void){
    return get_nwk_tek_rx_string_is_waiting();
}

/*******************************************************************************
 * loads uplink msg on nwk queue
 *******************************************************************************/
static QuNwkComMsg tx_msg;
bool nwk_com_load_uplink(   uint32_t delay_sec,
                            bool priority,
                            uint8_t qos,
                            const char *fmt, ...){
    va_list args;

    if(xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED) return false;

    // load formatted string
    va_start(args, fmt);
    vsnprintf(tx_msg.payload, sizeof(tx_msg.payload), fmt, args);
    va_end(args);

    // Insure string always include CR and LF
    tx_msg.payload[sizeof(tx_msg.payload)-2] = '\r';
    tx_msg.payload[sizeof(tx_msg.payload)-1] = '\n';

    // Load uplink parameters
    tx_msg.delay_sec = delay_sec;
    tx_msg.b_is_high_priority = priority;
    tx_msg.qos = qos;

    // Send to RTOS queue
    if( pdTRUE != xQueueSend(xQuNwkCom, &tx_msg, 0) ){
        // no space on queue
        return false;
    }else{
        return true;
    }
}

/*******************************************************************************
 * Save lorawan session info
 *******************************************************************************/
void nwk_com_save_session_info(void){
    nwk_tek_save_session_info();
    return;
}


/*******************************************************************************
 * Clear all session info
 *******************************************************************************/
void nwk_com_full_hw_reset(void){
	LorawanStatusFlags_UpdateMcastStatus(false);
    LorawanStatusFlags_ClearJoinedFlag();
    LorawanStatusFlags_SetModuleNeedsInitialising();
    return;
}

void nwk_com_force_rejoin(void)
{
	LorawanStatusFlags_UpdateMcastStatus(false);
    LorawanStatusFlags_SetModuleNeedsInitialising();
    return;
}

/* 
 * If network settings could not be optimised further
 * Trigger a rejoin.
 */
void nwk_com_optimise_settings(){
    if( nwk_tek_optimise_settings() == false)
    {
         LorawanStatusFlags_SetModuleNeedsInitialising();
    }
    LorawanStatusFlags_ClearModuleSettingsNeedOptimising();
    return;
}

bool get_nwk_com_settings_need_optimising(){
    return LorawanStatusFlags_GetModuleSettingsNeedOptimising();
}

void set_nwk_com_settings_need_optimising(){
    LorawanStatusFlags_SetModuleSettingsNeedOptimising();
    return;
}

/*******************************************************************************
 * Fetch DEVEUI
 *******************************************************************************/

void get_nwk_com_dev_eui(char * p_devEuiString)
{
    get_nwk_tek_dev_eui(p_devEuiString);
    return;
}
