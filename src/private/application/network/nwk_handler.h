/*
==========================================================================
 Name        : nwk_handler.h
 Project     : pcore
 Path        : /pcore/src/private/application/network/nwk_handler.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 8 Jan 2019
 Description : 
==========================================================================
*/

#ifndef PRIVATE_APPLICATION_NETWORK_NWK_HANDLER_H_
#define PRIVATE_APPLICATION_NETWORK_NWK_HANDLER_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
// declare the list of states
typedef enum
{
    NWK_RESET,
    NWK_SETUP,
    NWK_JOIN,
    NWK_IDLE
}nwk_sm_states_list;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
nwk_sm_states_list nwk_handler_run(void);
bool get_nwk_handler_nwk_is_ready(void);

#endif /* PRIVATE_APPLICATION_NETWORK_NWK_HANDLER_H_ */
