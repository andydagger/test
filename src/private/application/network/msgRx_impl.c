/*
 * msgRx_impl.c
 *
 *  Created on: 1 Jun 2021
 *      Author: benraiton
 */


/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include <stdbool.h>
#include <stdio.h>
#include "debug.h"
#include "msgRx_impl.h"
#include "msg_rx.h"
#include "config_network.h"
#include "productionMonitor.h"
#include "common.h"
#include "NVMData.h"
#include "sysmon.h"
#include "im_main.h"
#include "config_timers.h"
#include "luminrop.h"
#include "FreeRTOS.h"
#include "task.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define PWR_CALC_PAYLOAD_LENGTH 16

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
extern pkt_format last_pkt;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/


bool receivedReferenceCalibData(void)
{
    PowerCalcDataset refReadings = {0};
    char * p_rx_pkt = NULL;
    char **p_p_rx_pkt = &p_rx_pkt;
    uint32_t uintVrmsRef = 0;
    uint32_t uintIrmsRef = 0;
    uint32_t uintPfactorRef = 0;
    uint32_t uintPowerRef = 0;

    if(last_pkt.content_byte_count != PWR_CALC_PAYLOAD_LENGTH) return false;

    p_rx_pkt = last_pkt.pkt_content;

    // Get the 32bit value for each variable
    uintVrmsRef = common_array_to_uint32(p_p_rx_pkt);
    uintIrmsRef = common_array_to_uint32(p_p_rx_pkt);
    uintPfactorRef = common_array_to_uint32(p_p_rx_pkt);
    uintPowerRef = common_array_to_uint32(p_p_rx_pkt);

    // Map back to a float
    refReadings.instantVrms = *((float*)&uintVrmsRef);
    refReadings.instantIrms = *((float*)&uintIrmsRef);
    refReadings.instantPowerFactor = *((float*)&uintPfactorRef);
    refReadings.instantPower = *((float*)&uintPowerRef);
    
    productionMonitor_calibrateReadings(&refReadings);
    return true;
}

bool receivedNodeData(void)
{
    nodeInfoDataset infoPayload = {0};
    uint8_t i = 0;
    uint8_t * payloadPtr = (uint8_t *)&last_pkt.pkt_content[0];

    if(last_pkt.content_byte_count != sizeof infoPayload) return false;

    // Serial Number
    for(i=0; i< sizeof infoPayload.SerialNumberString; i++)
    {
        infoPayload.SerialNumberString[i] = *payloadPtr++;
    }
    // AppKey
    for(i=0; i<sizeof infoPayload.AppKeyString; i++)
    {
        infoPayload.AppKeyString[i] = *payloadPtr++;
    }

    //Part Number
    infoPayload.partNumber = (uint16_t)(*payloadPtr++)<<8;
    infoPayload.partNumber += (uint16_t)(*payloadPtr++);
    // Hardware Revisions
    infoPayload.powerBoardVersion = (*payloadPtr++);
    infoPayload.mainBoardVersion = (*payloadPtr++);
    infoPayload.radioBoardVersion = (*payloadPtr++);
    infoPayload.assemblyVersion = (*payloadPtr++);

    productionMonitor_receivedNodeInfo(&infoPayload);
    return true;
}

/*******************************************************************************
 * @brief engineering message -> status period
 *        value is in minutes
 *
 * @param[in] void
 *
 * @return bool - success of the message handling
 *******************************************************************************/
bool received_status_period(void) {

    NVM_RuntimeParams_t  runtime_params;
    uint8_t status_period_min;

    // make sure the packet length is correct
    if (last_pkt.content_byte_count != STATUS_PERIOD_MSG_CONTENT_LENGTH) {
        // not valid length
        return false;
    }

    // index 0 is a uint8_t value for the period in minutes
    status_period_min = (uint8_t)last_pkt.pkt_content[0];

    // validate the period value
    if (status_period_min > 0) {
        // read params from EEprom
        NVMData_readRuntimeParams(&runtime_params);
        // status period is in minutes
        runtime_params.status_period = status_period_min;
        // timer runs in ms ticks so adjust from minutes to ms
        im_main_set_timer_period(STATUS_UPLINK, (uint32_t)status_period_min * (60 * 1000));
        // write new period to EEprom
        NVMData_writeRuntimeParams(&runtime_params);

        return true;
    }

    return false;
}


/*******************************************************************************
 * @brief engineering message -> driver reset
 *        driver reset involves reseting the lamp override and driver information
 *        stored in eeprom, also reseting the processor.
 *
 * @param[in] void
 *
 * @return bool - success of the message handling
 *******************************************************************************/
bool received_driver_reset(void) {

    // make sure the packet length is correct
    if (last_pkt.content_byte_count != DRIVER_RESET_MSG_CONTENT_LENGTH) {
        // not valid length
        return false;
    }

    NVMData_restoreLampOutputOverride();

    NVMData_restoreDaliAddresses();

    debug_printf(NWK, "Driver Reset: System Reset in 60s\r\n");

    set_sysmon_delay_reset(DRIVER_RESET_MCU_RESET_DELAY);

    return true;
}

/*******************************************************************************
 *
 *******************************************************************************/

