/*
==========================================================================
 Name        : LorawanStatusFlags.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/LorawanStatusFlags.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 9 Oct 2019
 Description :
==========================================================================
*/

#ifndef PRIVATE_PERIPHERAL_LORAWAN_LORAWANSTATUSFLAGS_H_
#define PRIVATE_PERIPHERAL_LORAWAN_LORAWANSTATUSFLAGS_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "stdbool.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
bool LorawanStatusFlags_GetJoinedFlag(void);
void LorawanStatusFlags_SetJoinedFlag(void);
void LorawanStatusFlags_ClearJoinedFlag(void);

bool LorawanStatusFlags_GetModuleNeedsInitialising(void);
void LorawanStatusFlags_SetModuleNeedsInitialising(void);
void LorawanStatusFlags_ClearModuleNeedsInitialising(void);

bool LorawanStatusFlags_GetMcastSessionIsActive(void);
void LorawanStatusFlags_SetMcastSessionShouldBeActive(bool b_request);
bool LorawanStatusFlags_GetMcastSessionShouldBeActive(void);
void LorawanStatusFlags_UpdateMcastStatus( bool b_actualState);

bool LorawanStatusFlags_GetModuleSettingsNeedOptimising(void);
void LorawanStatusFlags_SetModuleSettingsNeedOptimising(void);
void LorawanStatusFlags_ClearModuleSettingsNeedOptimising(void);

#endif /* PRIVATE_PERIPHERAL_LORAWAN_LORAWANSTATUSFLAGS_H_ */
