/*
 * compileUplink.c
 *
 *  Created on: 14 Apr 2021
 *      Author: benraiton
 */

#include <string.h>
#include <stdio.h>
#include "common.h"
#include "compileUplink.h"
#include "sysmon.h"
#include "config_dataset.h"
#include "config_network.h"
#include "config_uplink_payloads.h"
#include "luminrop.h"
#include "calendar.h"
#include "photo.h"
#include "gps.h"
#include "powerMonitor.h"
#include "patch_rx.h"
#include "debug.h"
#include "rtc_procx.h"
#include "nwk_com.h"
#include "luminr_tek.h"
#include "Tilt.h"
#include "errorCode.h"
#include "systick.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "NVMData.h"
#include "im_main.h"
#include "config_timers.h"

static char ascii_payload[UP_MSG_PAYLOAD_ASCII_WITH_TERM];
static periodic_payload_struct payloadStatus = {0};

static unsigned int _getFloatAsHex(float value);

/*******************************************************************************
 * Using static ascii converter to facilitate Unit Tetsing
 *******************************************************************************/
static void raw_to_ascii( uint8_t * p_raw, char * p_ascii, uint8_t n_bytes){
    while(n_bytes > 0){
        sprintf( p_ascii, "%02X", *p_raw );
        n_bytes--;
        p_ascii += 2;
        p_raw++;
    }
    *p_ascii = '\0';
}

/*******************************************************************************
 * Compile and send FAULT uplink
 *******************************************************************************/
void compileUplink_Fault(bool msgPriority, uint8_t QoS, uint32_t faulFlags)
{
    fault_payload_struct faultPayload = {0};

    faultPayload.msg_type = FAULT_MSG;
    faultPayload.pkt_num = 0x01;
    faultPayload.length_pkts = 0x01;
    faultPayload.fault_flags = (uint32_t)__builtin_bswap32(faulFlags);
    faultPayload.tmstp = (uint32_t)__builtin_bswap32(rtc_procx_GetCounter());

    // parse raw data
    raw_to_ascii(           (uint8_t *)&faultPayload,
                            (char *)&ascii_payload,
                            (uint8_t)sizeof faultPayload);  

    debug_printf(NWK, "Fault Msg = %s\r\n", ascii_payload);

    nwk_com_load_uplink(    NO_DELAY,
                            msgPriority,
                            QoS,
                            "%s",
                            ascii_payload);  
    return;
}

/*******************************************************************************
 * Compile and send INITIAL uplink
 *******************************************************************************/
void compileUplink_Initial(void)
{
    GpsDataset       gps_dataset = {0};
    LuminrDataset    luminr_dataset = {0};
    initial_payload_struct payloadInitial = {0};
    // Fetch data
    get_luminr_tek_dataset(&luminr_dataset);
    get_gps_dataset(&gps_dataset);

    // msg framing
    payloadInitial.msg_type = INITIAL_MSG;
    payloadInitial.pkt_num = 0x01;
    payloadInitial.length_pkts = 0x01;
    // Compile data using local dataset
    payloadInitial.fw_ver = (uint32_t)__builtin_bswap16(fetch_fw_version_decimal());
    payloadInitial.fault_flags = (uint32_t)__builtin_bswap32(get_sysmon_fault_flags());
    memcpy( (uint8_t *)&payloadInitial.luminr_info,
            (uint8_t *)&luminr_dataset.info,
            sizeof payloadInitial.luminr_info);
    payloadInitial.tmstp = (uint32_t)__builtin_bswap32(rtc_procx_GetCounter());
    payloadInitial.gps_accuracy_coeff = gps_dataset.accuracy;
    payloadInitial.lat_100000 = (uint32_t)__builtin_bswap32(gps_dataset.latitude);
    payloadInitial.long_100000 = (uint32_t)__builtin_bswap32(gps_dataset.longitude);
    memcpy( (uint8_t *)&payloadInitial.dali_gtin,
            (uint8_t *)&luminr_dataset.gtin,
            SIZEOF_GTIN);
    memcpy( (uint8_t *)&payloadInitial.dali_ser_num,
            (uint8_t *)&luminr_dataset.serno,
            SIZEOF_SERNO_TYPE_2);

    // parse raw data
    raw_to_ascii(    (uint8_t *)&payloadInitial,
                            (char *)&ascii_payload,
                            (uint8_t)sizeof payloadInitial);

    debug_printf(NWK, "Initial Msg = %s\r\n", ascii_payload);

    nwk_com_load_uplink(    NO_DELAY,
                            LOW_PRIORITY,
                            QOS_0,
                            "%s",
                            ascii_payload);
    return;
}

/*******************************************************************************
 * @brief Compile and send STATUS uplink
 *
 * @param[in] bool     - true = apply delay, otherwise false
 *
 * @return void
 *
 *******************************************************************************/
void compileUplink_Status(bool apply_delay)
{
    PowerCalcDataset power_calc_dataset = {0};
    uint32_t delay_sec = 0;

    // Compile message
    // Copy shadow data
    memcpy( (uint8_t*)&payloadStatus.previous_dataset,
            (uint8_t*)&payloadStatus.current_dataset,
            sizeof(payloadStatus.current_dataset));

    // fetch latest datasets
    powerMonitor_getDataset(&power_calc_dataset);
    // msg framing
    payloadStatus.msg_type    = PERIODIC_MSG;
    payloadStatus.pkt_num     = 0x01;
    payloadStatus.length_pkts = 0x01;

    if (apply_delay == true) {

        delay_sec = compileUplink_calculate_jitter();
    }

    debug_printf(NWK, "uplink status jitter (S): %d\r\n", delay_sec);

    payloadStatus.fw_ver                                  = (uint16_t)__builtin_bswap16(fetch_fw_version_decimal());
    payloadStatus.tmstp                                   = (uint32_t)__builtin_bswap32(rtc_procx_GetCounter() + delay_sec);
    payloadStatus.current_dataset.fault_flags             = (uint32_t)__builtin_bswap32(get_sysmon_fault_flags());
    payloadStatus.current_dataset.instantVrms_x10         = (uint16_t)__builtin_bswap16((uint16_t)(10*power_calc_dataset.instantVrms));
    payloadStatus.current_dataset.instantIrms_x100        = (uint16_t)__builtin_bswap16((uint16_t)(100*power_calc_dataset.instantIrms));
    payloadStatus.current_dataset.instantPowerFactor_x100 = (uint8_t)(100*power_calc_dataset.instantPowerFactor);
    payloadStatus.current_dataset.instantPower_x10        = (uint16_t)__builtin_bswap16((uint16_t)(10*power_calc_dataset.instantPower));
    payloadStatus.current_dataset.brightnessLevel         = get_luminrop_LampOutputBrightnessLevel();
    payloadStatus.current_dataset.measuredLux             = (uint8_t)get_photo_LUX();

    // parse raw data
    raw_to_ascii(   (uint8_t *)&payloadStatus,
                    (char *)&ascii_payload,
                    (uint8_t)sizeof(payloadStatus));

    debug_printf(NWK, "Status Msg = %s\r\n", ascii_payload);

    nwk_com_load_uplink( delay_sec,
                         LOW_PRIORITY,
                         QOS_0,
                         "%s",
                         ascii_payload);
}

/*******************************************************************************
 * Compile and send BILLING uplink
 *******************************************************************************/
void compileUplink_Daily(BillingDataset billing_data)
{
    Tilt_output latest_xyz;
    uint8_t idByte;
    uint8_t valueByte;
    daily_payload_struct dailyPayload = {0};         
    PowerCalcDataset powerMeasDataset = {0};   

    // Freezing scheduler as multiple variables are being retrieved.
    vTaskSuspendAll();
    Tilt_getXYZ(Tilt_getRef(), &latest_xyz);
    xTaskResumeAll();

    powerMonitor_getDataset(&powerMeasDataset);

    errorCode_getCode(&idByte, &valueByte);
    errorCode_eraseCode();    

    // msg framing
    dailyPayload.msg_type = DAILY_MSG;
    dailyPayload.pkt_num = 0x01;
    dailyPayload.length_pkts = 0x01;

    dailyPayload.switch_on_tmstp = (uint32_t)__builtin_bswap32(billing_data.switch_on_tmstp);
    dailyPayload.switch_off_tmstp = (uint32_t)__builtin_bswap32(billing_data.switch_off_tmstp);
    
    dailyPayload.tiltX_x100 = (uint16_t)__builtin_bswap16(latest_xyz.X);
    dailyPayload.tiltY_x100 = (uint16_t)__builtin_bswap16(latest_xyz.Y);
    dailyPayload.tiltZ_x100 = (uint16_t)__builtin_bswap16(latest_xyz.Z);

    dailyPayload.calendarVersion = (uint16_t)__builtin_bswap16(billing_data.calendarVersion);
    dailyPayload.calendarFileRef = (uint32_t)__builtin_bswap32(billing_data.calendarFileRef);
    dailyPayload.calendarProfileId = billing_data.calendarProfileId;

    dailyPayload.accumulatedEnergy_20WhrUnits = (uint32_t)__builtin_bswap32(powerMeasDataset.accumulatedEnergy_20WhrUnits);
    dailyPayload.vrmsMax_x10 = (uint16_t)__builtin_bswap16((uint16_t)(10*powerMeasDataset.vrmsMax));
    dailyPayload.vrmsMin_x10 = (uint16_t)__builtin_bswap16((uint16_t)(10*powerMeasDataset.vrmsMin));
    dailyPayload.vrmsAverage_x10 = (uint16_t)__builtin_bswap16((uint16_t)(10*powerMeasDataset.vrmsAverage));

    dailyPayload.rebootIdByte = idByte;
    dailyPayload.rebootValueByte = valueByte;

    // parse raw data
    raw_to_ascii(   (uint8_t *)&dailyPayload,
                    (char *)&ascii_payload,
                    (uint8_t)sizeof dailyPayload);  

    debug_printf(NWK, "Billing Msg = %s\r\n", ascii_payload);

    // This will give a 1248 min or 20 hour spread 0xFFFFF/14
    nwk_com_load_uplink(    get_random_delay_ms()/14,
                            LOW_PRIORITY,
                            QOS_2,
                            "%s",
                            ascii_payload);  

}

/*******************************************************************************
 * Compile and send RPL uplink
 *******************************************************************************/
void compileUplink_MissedPackets(uint32_t transmitDelay, char * pointerToMissedPacketsPayload)
{
    rpl_payload_struct missedPacketsPayload = {0};

    missedPacketsPayload.msg_type = PATCH_RPL_ID_V2;
    missedPacketsPayload.pkt_num = 0x01;
    missedPacketsPayload.length_pkts = 0x01;

    // parse raw data
    raw_to_ascii(   (uint8_t *)&missedPacketsPayload,
                    (char *)&ascii_payload,
                    3);  

    // Copy across encoded list of missed packets as already encoded to ASCII
    memcpy( (char *)&ascii_payload[6],
            (char *)pointerToMissedPacketsPayload,
            (uint8_t)sizeof missedPacketsPayload.listOfMissedPackets);

    debug_printf(NWK, "Missed Packets Msg = %s\r\n", ascii_payload);

    nwk_com_load_uplink(    transmitDelay,
                            LOW_PRIORITY,
                            QOS_0,
                            "%s",
                            ascii_payload);

    return;
}

/*******************************************************************************
 * Compile and send CAL_UPD_ACK uplink
 *******************************************************************************/
void compileUplink_CalendarConfirmation(    uint32_t transmitDelay, 
                                            uint32_t calendarFileRef, 
                                            uint16_t calendarVersion)
{
    conf_cal_payload_struct CalendarConfPayload = {0};

    CalendarConfPayload.msg_type = CAL_UPD_ACK;
    CalendarConfPayload.pkt_num = 0x01;
    CalendarConfPayload.length_pkts = 0x01;

    CalendarConfPayload.calendarFileRef = (uint32_t)__builtin_bswap32(calendarFileRef);
    CalendarConfPayload.calendarVersion = (uint16_t)__builtin_bswap16(calendarVersion);

    // parse raw data
    raw_to_ascii(   (uint8_t *)&CalendarConfPayload,
                    (char *)&ascii_payload,
                    (uint8_t)sizeof CalendarConfPayload);  

    debug_printf(NWK, "Calendar Conf Msg = %s\r\n", ascii_payload);

    // Acknowledge new cal vesrion to cloud   
    nwk_com_load_uplink(    transmitDelay,
                            LOW_PRIORITY,
                            QOS_1,
                            "%s",
                            ascii_payload);

}


/*******************************************************************
 * compileUplink_ProductionTestReport()
 * Compiles teh paylaod in a different to others due to 
 * concerns as regards to flipping float values.
*******************************************************************/
void compileUplink_ProductionTestReport(void)
{
    PowerCalcDataset powerCalDataset = {0};

    powerMonitor_getCalibReadings(&powerCalDataset);

    sprintf(    ascii_payload, "%02X0101%08lX%08X%08X%08X%08X%02X",
                PROD_REPORT,
                (uint32_t)get_sysmon_fault_flags(),
                _getFloatAsHex(powerCalDataset.instantVrms),
                _getFloatAsHex(powerCalDataset.instantIrms),
                _getFloatAsHex(powerCalDataset.instantPowerFactor),
                _getFloatAsHex(powerCalDataset.instantPower),
                (uint8_t)get_photo_LUX());

    debug_printf(NWK, "Production Report = %s\r\n", ascii_payload);

    nwk_com_load_uplink(    0,
                            HIGH_PRIORITY,
                            QOS_2,
                            "%s",
                            ascii_payload);

	return;
}

void compileUplink_ProductionTestingDone(void)
{
    sprintf(ascii_payload, "%02X0101", PROD_DONE_MSG);

    debug_printf(NWK, "Production Test Done: %s\r\n", ascii_payload);

    nwk_com_load_uplink(    0,
                            HIGH_PRIORITY,
                            QOS_2,
                            "%s",
                            ascii_payload);
	return;
}

/*******************************************************************************
 * @brief Calculates a proportional jitter period for the uplink period
 *        that is stored in eeprom
 *
 * @param[in] void
 *
 * @return    uint32_t - jitter in seconds
 *
 *******************************************************************************/
uint32_t compileUplink_calculate_jitter(void)
{
    uint32_t divisor;
    uint32_t jitter = 0;

    // Do not apply delays to status uplinks during patch download
    // so as not to overwrite delayed RPL.
    if (get_patch_download_is_in_progess() == false) {

        // calc the divisor - this will produce a value of
        // 120 for 30 mins, 60 for 15 mins etc
        // The timer status period is in ms, so convert to minutes and multiply by the jitter multiplier
        divisor = (im_main_get_timer_period(STATUS_UPLINK) / (1000*60)) * NETWORK_UPLINK_JITTER_MULTIPLIER;
        // calculate random delay proportional to uplink period
        jitter = get_systick_ms() % divisor;
    }

    return jitter;
}



static unsigned int _getFloatAsHex(float value)
{
    union {
        float f;
        unsigned int u;
    } f2u;

    f2u.f = value;

    return f2u.u;
}

