/*
==========================================================================
 Name        : LorawanStatusFlags.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/LorawanStatusFlags.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 9 Oct 2019
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "stdbool.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static bool joinedLorawanNetwork = false;
static bool moduleNeedsInitialising = false;
static bool mcastSessionIsActive = false;
static bool mcastSessionShouldBeActive = false;
static bool moduleSettingsNeedOptimising = false;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @Lorawan joined flag
 *******************************************************************************/
bool LorawanStatusFlags_GetJoinedFlag(void){
    return joinedLorawanNetwork;
}
void LorawanStatusFlags_SetJoinedFlag(void){
    joinedLorawanNetwork = true;
    return;
}
void LorawanStatusFlags_ClearJoinedFlag(void){
    joinedLorawanNetwork = false;
    return;
}

/*******************************************************************************
 * Module initialisation request flag
 *******************************************************************************/
bool LorawanStatusFlags_GetModuleNeedsInitialising(void){
    return moduleNeedsInitialising;
}
void LorawanStatusFlags_SetModuleNeedsInitialising(void){
    moduleNeedsInitialising = true;
    return;
}
void LorawanStatusFlags_ClearModuleNeedsInitialising(void){
    moduleNeedsInitialising = false;
    return;
}

/*******************************************************************************
 * Status of the multicast session
 *******************************************************************************/
bool LorawanStatusFlags_GetMcastSessionIsActive(void){
    return mcastSessionIsActive;
}
void LorawanStatusFlags_SetMcastSessionShouldBeActive(bool b_request){
    mcastSessionShouldBeActive = b_request;
}
bool LorawanStatusFlags_GetMcastSessionShouldBeActive(void){
    return mcastSessionShouldBeActive;
}
void LorawanStatusFlags_UpdateMcastStatus( bool b_actualState)
{
    mcastSessionIsActive = b_actualState;
    return;
}

/*******************************************************************************
 * Module initialisation request flag
 *******************************************************************************/
bool LorawanStatusFlags_GetModuleSettingsNeedOptimising(void){
    return moduleSettingsNeedOptimising;
}
void LorawanStatusFlags_SetModuleSettingsNeedOptimising(void){
    moduleSettingsNeedOptimising = true;
    return;
}
void LorawanStatusFlags_ClearModuleSettingsNeedOptimising(void){
    moduleSettingsNeedOptimising = false;
    return;
}
