/*
==========================================================================
 Name        : msg_rx.c
 Project     : pcore
 Path        : /pcore/src/private/application/network/msg_rx.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 24 May 2018
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "FreeRTOSCommonHooks.h"
#include "config_queues.h"
#include "debug.h"
#include "config_network.h"
#include "config_dataset.h"
#include "common.h"
#include "sysmon.h"
#include "calendar.h"
#include "gps.h"
#include "rtc_procx.h"
#include "nwk_com.h"
#include "lorawan.h"
#include "msg_rx.h"
#include "msgRx_impl.h"
#include "luminrop.h"


/*****************************************************************************
 * Private functions
 ****************************************************************************/
// local processign functions
static uint8_t convert_payload_ascii_to_char(char * source, char * target) ;
static void msg_rx_load_msg_onto_queue(QuPatchDataMsg * rx_msg, task_id target_task_id);

// Documented
static bool received_patch_ota_msg(void);
static bool received_time_sync_corr(void);
static bool received_tilt_setup(void);
static bool received_coordinates(void);
static bool received_luminr_override(void);
static bool received_system_reset(void);
// Engineering Extras
static bool received_fat_reset(void);
static bool received_force_rejoin(void);
static bool received_get_status(void);

// FUOTA test jig
static bool flakey_node_test(void);
static bool crash_node_test(void);

static bool flakey_node_test_applies(void);
static void crash_node_test_check(void);

static bool msg_rx_is_type_valid(uint8_t * msg_info_row);
static void msg_rx_acking(void);

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
typedef struct
{
    uint8_t msg_type;
    bool (*func)(void);
    bool b_ack_expected;
}struct_msg_rx_func;

static const struct_msg_rx_func rx_msg_matrix[] =
{
    { PATCH_RX_FUT,        received_patch_ota_msg,     true  },
    { PATCH_RX_PCP,        received_patch_ota_msg,     false },
    { PATCH_RX_PRT,        received_patch_ota_msg,     true  },
    { PATCH_RX_CUT,        received_patch_ota_msg,     true  },
    { PATCH_RX_PTP,        received_patch_ota_msg,     false },
    { TIME_SYNC_CORR,      received_time_sync_corr,    true  },
    { SETUP_TILT,          received_tilt_setup,        false },
    { SETUP_COORD,         received_coordinates,       true  },
    { LUMINR_OVERRIDE,     received_luminr_override,   true  },
    { SYS_RESET,           received_system_reset,      true  },
    { GET_STATUS_REQ,      received_get_status,        false },
    // Engineering Extras
    { FAT_DRIVE_RESET,     received_fat_reset,         true  },
    { FORCE_REJOIN,        received_force_rejoin,      true  },
    { DRIVER_RESET,        received_driver_reset,      true  },
    { SET_STATUS_PERIOD,   received_status_period,     true  },
    // FUOTA test jig
    { FLAKEY_NODE,         flakey_node_test,           false },
    { CRASH_NODE,          crash_node_test,            false },
    // Production test
    { PROD_REF_CAL_DATA,   receivedReferenceCalibData, false },
    { PROD_NODE_DATA,      receivedNodeData,           false }
};

pkt_format last_pkt;

static bool b_first_pcp_round = true;
static uint8_t flakey_node_start_end[2] = {0};
static uint8_t crash_node_pkt_num = 0;
static bool b_flakey_node_test_enabled = false;
static bool b_crash_node_test_enabled = false;

static GpsDataset       gps_dataset = {0};

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
QueueHandle_t xQuRxMsg = NULL;
extern QueueHandle_t xQuLuminrOp; // vTaskLuminrOp
extern QueueHandle_t XQuPatchData; // vTaskPatching

/*****************************************************************************
 * Public functions
 ****************************************************************************/

bool msg_rx_process(char * nwk_rx_msg){
    static uint8_t msg_info_row = 0;
    static char * p_rx_pkt = NULL;
    static char **p_p_rx_pkt = &p_rx_pkt;
    static uint8_t varint_offset = 0;
    static char packet[DOWN_MSG_PAYLOAD_ASCII_WITH_TERM] = {0};
    static char packet_byte_count = 0;
    uint8_t i = 0;

    // print out received packet for debug
    debug_printf( NWK, "rx: %s", nwk_rx_msg);

    // convert back from ascii string for processing
    packet_byte_count = convert_payload_ascii_to_char(nwk_rx_msg, packet);

    // If length is too short reject msg
    if( packet_byte_count < 3 ){
        debug_printf(NWK, "Pkt too short!\r\n");
        return false;
    }else{}

    /*
     * Decode packet
     * 1: msg type
     * 2: pkt number (var int)
     * 3: msg length (var int) in number of packets
     * 4: msg content
     */

    p_rx_pkt = &packet[0];
    // byte 1
    last_pkt.msg_type = *p_rx_pkt;

    // check msg type is valid
    if( msg_rx_is_type_valid(&msg_info_row) == false){
        sysmon_report_error(NWK, RX_MSG_TYPE_NOT_RECOGNISED);
        return false;
    }else{
        // NOOP: msg valid so can safely use ptrs now
    }

    p_rx_pkt ++;
    packet_byte_count--;
    // byte 2:
    last_pkt.pkt_number = common_varint_to_uint32(p_p_rx_pkt, &varint_offset);
    packet_byte_count -= varint_offset;
    // byte 3:
    last_pkt.msg_length = common_varint_to_uint32(p_p_rx_pkt, &varint_offset);
    packet_byte_count -= varint_offset;
    // content
    last_pkt.content_byte_count = packet_byte_count;
    if( last_pkt.content_byte_count > 0 ){
        // Copy content across inclusing 0x00 (\0) chars
        for(i = 0; i <= last_pkt.content_byte_count; i++) {
            last_pkt.pkt_content[i] = p_rx_pkt[i];
        }
    }else{}

//    debug_printf(NWK, "Msg Type = %02x\r\n", last_pkt.msg_type);
//    debug_printf(NWK, "Pkt Num = %08x\r\n", last_pkt.pkt_number);
//    debug_printf(NWK, "Msg Length = %08x\r\n", last_pkt.msg_length);
//    debug_printf(NWK, "Bytes = %lu\r\n", last_pkt.content_byte_count);
//    debug_printf(NWK, "Pkt Content = ...\r\n");


    /* Before processing the received message
     * work out if the cloud will be expecting an ack
     */
    if( rx_msg_matrix[msg_info_row].b_ack_expected == true){
        debug_printf(NWK, "Ack Required.\r\n");
        msg_rx_acking();
    }

    /*
     * Functions called, depending on the msg type, will use the
     * packet information: pkt_number, msg_length, pkt_content
     * whenever required.
     */

    // call the corresponding function
    return (*rx_msg_matrix[msg_info_row].func)();


    // Shall never reach this point
    return false;
}

/*******************************************************************************
 * @brief Check if the msg type exist in the matrix
 * @param[in] voidptr to the row number
 * @return bool with tru for valid
 *******************************************************************************/
static bool msg_rx_is_type_valid(uint8_t * msg_info_row){
    uint8_t msg_type_id = 0;

    // look for msg data
    while (msg_type_id < matrix_n_rows(rx_msg_matrix)) {
        if (last_pkt.msg_type == rx_msg_matrix[msg_type_id].msg_type) {
            *msg_info_row = msg_type_id;
            return true;
        } else {}
        msg_type_id++;
    }

    return false;
}

/*******************************************************************************
 * @brief Load app-level ack
 * @param[in] void
 * @return void
 *******************************************************************************/
static void msg_rx_acking(void){
    nwk_com_load_uplink(    NO_DELAY,
                            HIGH_PRIORITY,
                            QOS_0,
                            "%02X%02X",
                            last_pkt.msg_type,
                            last_pkt.content_byte_count);

    return;

}

/*******************************************************************************
 * @brief Forward rx msg to patch rx stack
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool received_patch_ota_msg(void){
    QuPatchDataMsg patch_rx_msg;

    // if msg is PCP see if any fuotatest jig test are waiting
    if(last_pkt.msg_type == PATCH_RX_PCP){
        // crash node test?
        crash_node_test_check();
        // flakey node test?
        if(flakey_node_test_applies() == true){
            // ignore this PCP
            return true;
        }
    }

    // if this is the PTP clear b_first_pcp_round flag
    if(last_pkt.msg_type == PATCH_RX_PTP){
        b_first_pcp_round = false;
        b_crash_node_test_enabled = false;
        b_flakey_node_test_enabled = false;
    }

    // Do not attempt to access queue before the have been created
    if(xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED) return false;

    // load msg with parsed data - addressed to vTaskPatchRx
    msg_rx_load_msg_onto_queue(&patch_rx_msg, TASK_PATCHING);

    // TODO: catch error if msg can't be loaded - wait?
    xQueueSend(XQuPatchData, &patch_rx_msg, 0);

    return true;
}

/*******************************************************************************
 * @brief RTCC correction factor from the cloud
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool received_time_sync_corr(void) {

    static char * p_rx_pkt = NULL;
    static char **p_p_rx_pkt = &p_rx_pkt;

    /* Make sure we have rcvd a Correct Time Synch packet of 4 Bytes */
    if ( 4 != last_pkt.content_byte_count )
    {
    	/* Early exit */
    	debug_printf(NWK, "content_byte_count = %d\r\n",last_pkt.content_byte_count);
    	return false;
	}

    /* Set pointer to point to beginning of the pay load buffer*/
    p_rx_pkt = last_pkt.pkt_content;

    /* Set the RTC Counter Offset */
    rtc_procx_Correction(common_array_to_int32(p_p_rx_pkt));

    /* Let the Cal know a New Time Synch has been received */
    cal_set_Time_synch(true);

    debug_printf(NWK, "received_time_sync_corr\r\n");

    return true;
}

/*******************************************************************************
 * @brief Tilt threshold and zeroing command
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool received_tilt_setup(void) {
    // Not supported at the moment
    return true;
}

/*******************************************************************************
 * @brief Node coordinates from cloud > for solar clock algorithm
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool received_coordinates(void) {
    static char * p_rx_pkt = NULL;
    static char **p_p_rx_pkt = &p_rx_pkt;

    /* Make sure we have rcvd a Full GPS Co-ords packet of 13 Bytes */
    if ( 0x0D != last_pkt.content_byte_count )
    {
    	/* Early exit */
    	debug_printf(NWK, "content_byte_count = %d\r\n",last_pkt.content_byte_count);
    	return false;
	}

    /* Set pointer to point to beginning of the pay load buffer*/
    p_rx_pkt = last_pkt.pkt_content;

    /* Set the RTC Counter Offset */
    rtc_procx_Correction(common_array_to_int32(p_p_rx_pkt));

    /* Set pointer to point to beginning of the GPS coords part of pay load buffer*/
    p_rx_pkt = &last_pkt.pkt_content[5];
    gps_dataset.latitude = common_array_to_int32(p_p_rx_pkt);

    /* Set pointer to point to beginning of the GPS coords part of pay load buffer*/
    p_rx_pkt = &last_pkt.pkt_content[9];
    gps_dataset.longitude = common_array_to_int32(p_p_rx_pkt);

    /* Set the GPS accuracy so that the Solar Clock can be calculated */
    gps_dataset.accuracy = 2;

    /* Send to GPS to set the GPS Coordinates */
    set_gps_dataset(&gps_dataset);

    // Adjust DST setting accordingly
    if(     ( gps_dataset.latitude != 0 ) && /* Has the GPS been acquired?  */
            ( (float)(gps_dataset.latitude / 100000) < 30.0 ) ){  /* Scale the Latitude (by 100000) and check if it's in the non-DST region < 30 degrees */
        rtc_procx_SetDstIsApplicable(false);
    }else{
        rtc_procx_SetDstIsApplicable(true);
    }


    /* Let the Cal know a New set of GPS Coords have been received */
    cal_set_GPS_Data_synch(true);

    debug_printf(NWK, "received_coordinates\r\n");

    return true;
}

/*******************************************************************************
 * @brief Luminaire override network processing
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool received_luminr_override(void)
{
    QuLuminrOpMsg lamp_output_update_msg;
    char* arrayPtr;
    uint8_t  outputLevel;
    uint32_t startTime;
    uint32_t stopTime;
    uint32_t timeUTC;

    // Do not attempt to access queue before the have been created
    if (xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED)
    {
        return false;
    }
    // make sure the packet length is correct
    if (last_pkt.content_byte_count != LUMINAIR_OVERRIDE_MSG_CONTENT_LENGTH)
    {
        return false;
    }

    // index 0 is the brightness level inc. on/off flag
    outputLevel = last_pkt.pkt_content[0];
    
    // index 1 is the start time utc
    arrayPtr = &last_pkt.pkt_content[1];

    startTime = common_array_to_uint32(&arrayPtr);

    // common_array_to_uint32() increments the pointer so
    // is now pointing to the correct location
    stopTime = common_array_to_uint32(&arrayPtr);
    
    timeUTC = rtc_procx_GetCounter();

    // Only check for boundaries if switching on
    // we always switch off independent of utc values
    if (outputLevel & LUMINAIR_OVERRIDE_ENABLE)
    {
        // switched on time within limits?
        if ((stopTime - startTime) > LUMINAIR_OVERRIDE_MAX_ON_TIME_SEC)
        {
            return false;
        }

        // check the delay for the start time.
        if (startTime > timeUTC)
        {
            if ((timeUTC - startTime) > LUMINAIR_OVERRIDE_MAX_ON_TIME_DELAY_SEC)
            {
                return false;
            }
        }
        else
        {
            // start time was equal or less than current time
            // if stop time is less than or equal don't accept the message
            if (stopTime <= timeUTC)
            {
                return false;
            }

        }

        // Stop time behind current time ?
        if (stopTime < timeUTC)
        {
            return false;
        }
        // can't accept a stop time that is equal or less then the start time
        if (stopTime <= startTime)
        {
            return false;
        }
    }

    // reaching here means we are disabling the override or all values are within limits

    // only place in the queue if disabling or the start time has already elapsed
    // Place override command on LuminrOp Queue
    lamp_output_update_msg.source_task_id      = TASK_NWK_COM;
    lamp_output_update_msg.lamp_output_level   = outputLevel;
    lamp_output_update_msg.lamp_start_time_utc = startTime;
    lamp_output_update_msg.lamp_stop_time_utc  = stopTime;

    xQueueSend(xQuLuminrOp, &lamp_output_update_msg, 0);

    return true;
}

/*******************************************************************************
 * @brief Reset node if fw version matches
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool received_system_reset(void){
    // TODO: Reset STM8?
    // now reset the processor
    debug_printf(NWK, "Reset System in 60 sec.\r\n");
    set_sysmon_delay_reset(60);
    return true;
}

/*******************************************************************************
 * @brief System reset
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool received_fat_reset(void){
    // now reset the drive and reboot
    sysmon_format_disk_and_reset();
    return true;
}

/*******************************************************************************
 * @brief System reset
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool received_force_rejoin(void){
    // reset hw module
	nwk_com_force_rejoin();
    return true;
}



/*******************************************************************************
 * @brief Get Staus Request from Cloud
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool received_get_status(void){
    sysmon_loadStatusPayload(false);
    return true;
}

/*******************************************************************************
 * @brief FUOTA test to miss PCP intentionally FLAKEY_NODE 0xDD
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool flakey_node_test(void){
    // set flag for first pcp download round
    b_first_pcp_round = true;
    // enable test flag.
    b_flakey_node_test_enabled = true;
    // store values
    flakey_node_start_end[0] = last_pkt.pkt_content[0];
    flakey_node_start_end[1] = last_pkt.pkt_content[1];
    // make sure they are in the correct order
    if(flakey_node_start_end[0] > flakey_node_start_end[1]){
        flakey_node_start_end[1] = flakey_node_start_end[0];
    }

    debug_printf(NWK, "Will drop fuota pkt %d to %d on first round\r\n",
                    flakey_node_start_end[0],
                    flakey_node_start_end[1]);
    return true;
}
static bool flakey_node_test_applies(void){
    if( (b_first_pcp_round == true) &&
        (b_flakey_node_test_enabled == true)){
        if( (last_pkt.pkt_number >= flakey_node_start_end[0]) &&
            (last_pkt.pkt_number <= flakey_node_start_end[1])){
                // force task to ignore the PCP
                debug_printf(NWK, "Ignoring Packet\r\n");
                return true;
        }
    }
    return false;
}

/*******************************************************************************
 * @brief FUOTA test to force node crash during download CRASH_NODE 0xDE
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool crash_node_test(void){
    // set flag for first pcp download round
    b_first_pcp_round = true;
    // enable test flag.
    b_crash_node_test_enabled = true;
    // store value
    crash_node_pkt_num = last_pkt.pkt_content[0];

    debug_printf(NWK, "Will reset node on pkt %d on first round\r\n",
                crash_node_pkt_num);
    return true;
}
static void crash_node_test_check(void){
    if( (b_first_pcp_round == true) &&
        (b_crash_node_test_enabled == true)){
        if( last_pkt.pkt_number == crash_node_pkt_num){
                // force node reset
                debug_printf(   NWK,
                                "PKT %d received Crashing Node!...\r\n",
                                crash_node_pkt_num);
                sysmon_system_reset();
                return; // should not reach here.
        }
    }
    return;
}

/*******************************************************************************
 * @brief Converts payload from mchp module 0x3330 > 0x30
 * @param[in] source and target buffer
 * @return void
 *******************************************************************************/
static uint8_t convert_payload_ascii_to_char(char * source, char * target) {
    uint8_t byte_count = 0;
    do {
        *target = (char) lz_char_to_uint8_ascii(source);
        target++;
        byte_count++;
        source += 2;
    } while ( ( *source != 0x0D ) && ( byte_count < ( DOWN_MSG_PAYLOAD ) ) );
    // MCHP termination char or max n bytes processed

    /*
     * overwrite byte straight after packet content with \0
     * to rake out \r\n from MCHP module*/
    *target = '\0';
    return byte_count;
}


/*******************************************************************************
 * @brief Loads parsed message onto rx data queue
 * @param[in] void
 * @return void
 *******************************************************************************/
static void msg_rx_load_msg_onto_queue(QuPatchDataMsg * rx_msg, task_id target_task_id){
    uint8_t i = 0;

    // load msg with parsed data
    rx_msg->source_task_id = TASK_NWK_COM;
    rx_msg->target_task_id = target_task_id;
    rx_msg->msg_type = last_pkt.msg_type;
    rx_msg->pkt_number = last_pkt.pkt_number;
    rx_msg->message_length = last_pkt.msg_length;
    rx_msg->content_byte_count = last_pkt.content_byte_count;
    // copy across raw content (cannot use strncpy
    for(i = 0; i <= last_pkt.content_byte_count; i++) {
        rx_msg->data[i] = last_pkt.pkt_content[i];
    }
    return;
}

