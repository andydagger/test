/*
==========================================================================
 Name        : msg_handler.c
 Project     : pcore
 Path        : /pcore/src/private/application/network/msg_handler.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 8 Jan 2019
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include "stdint.h"
#include "stdbool.h"
#include <string.h>
#include "common.h"
#include "systick.h"
#include "debug.h"
#include "sysmon.h"
#include "nwk_com.h"
#include "nwk_handler.h"
#include "msg_handler.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "FreeRTOSMacros.h"
#include "FreeRTOSCommonHooks.h"
#include "ring_buf.h"
#include "rtc_procx.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

// Declare static ring buffers
static RINGBUFF_TYPE ring_buf_qos_0;
static RINGBUFF_TYPE ring_buf_qos_1;
static RINGBUFF_TYPE ring_buf_qos_2;

// Create Data Buffers
static char data_buffer_qos_0[QOS_0_BUFFER_DEPTH * QOS_0_DATA_LENGTH_ASCII];
static char data_buffer_qos_1[QOS_1_BUFFER_DEPTH * QOS_1_DATA_LENGTH_ASCII];
static char data_buffer_qos_2[QOS_2_BUFFER_DEPTH * QOS_2_DATA_LENGTH_ASCII];

/*
typedef enum{
    QOS_0,
    QOS_1,
    QOS_2
}uplink_qos_type;
*/
static const RINGBUFF_TYPE * ring_buffer_selector[] = {
        &ring_buf_qos_0,
        &ring_buf_qos_1,
        &ring_buf_qos_2
};

typedef struct{
    uint32_t t_send;
    char payload[UP_MSG_PAYLOAD_ASCII_WITH_TERM];
}delayed_data_struct;

static delayed_data_struct delayed_msg_selector[3] = {0};

#define DELAYED_STATUS_LIMIT_SEC 5

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void msg_handler_service_delayed_msg(void);

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Services the uplink ring-buffers if nwk connected
 * @param[in] void
 * @return void
 *******************************************************************************/

void msg_handler_run(void){
    char temp_buffer[UP_MSG_PAYLOAD_ASCII_WITH_TERM] = {0};

    // First service any received msg (downlink)
    if( true == get_nwk_com_rx_msg_pending() ){
        nwk_com_read_msg();
    }

    // Insure network handler is ready to receive uplink
    if( false == get_nwk_handler_nwk_is_ready() ){
        return;
    }

    // Service Fault Ring Buffer with QoS = 1
    // Confirmed uplink but only MAC level retries
    if( 1 != get_ring_buf_is_empty(&ring_buf_qos_1)){
        ring_buf_pop( &ring_buf_qos_1, temp_buffer);
        nwk_com_send_msg( temp_buffer, true);
    }
    // Service Critical Data Ring Buffer with QoS = 2
    // Confirmed uplink, keep trying until acked by NS
    else if( 1 != get_ring_buf_is_empty(&ring_buf_qos_2)){
        ring_buf_pop( &ring_buf_qos_2, temp_buffer);
        if( false == nwk_com_send_msg( temp_buffer, true) ){
            // If failed to send, leave as pending
            ring_buf_qos_2.tail--;
        }
    }
    // Service Non Critical Data Ring Buffer with QoS = 0
    // Unconfirmed, no ack required
    else if( 1 != get_ring_buf_is_empty(&ring_buf_qos_0)){
        ring_buf_pop( &ring_buf_qos_0, temp_buffer);
        nwk_com_send_msg( temp_buffer, false);
    }

    // Servcie delayed uplink and load into ring buffer if ready
    msg_handler_service_delayed_msg();

    return;
}

/*******************************************************************************
 * @brief Loads application uplink request into corresponding ring-buffer
 * @param[in] void
 * @return void
 *******************************************************************************/
void msg_handler_load(QuNwkComMsg msg_to_send){

    debug_printf(   NWK,
                    ">>> LOAD : %s, %lu, %d, %d\r\n",
                    msg_to_send.payload,
                    msg_to_send.delay_sec,
                    msg_to_send.b_is_high_priority,
                    msg_to_send.qos);

    // Work out if message can be loaded straight away.
    if( 0 == msg_to_send.delay_sec){
        // If high priority place at front of queue
        if( true == msg_to_send.b_is_high_priority){
            ring_buf_force_write_at_front(
                    (RINGBUFF_TYPE *)ring_buffer_selector[msg_to_send.qos],
                    (char *)msg_to_send.payload);
        }
        // otherwise place at back as low priority
        else{
            ring_buf_force_write_at_back(
                    (RINGBUFF_TYPE *)ring_buffer_selector[msg_to_send.qos],
                    (char *)msg_to_send.payload);
        }
    }else{
        // Can handle one delayed uplink per ring buffer
        // calculate calendar time to transmit in unix sec count
        delayed_msg_selector[msg_to_send.qos].t_send =
                msg_to_send.delay_sec + rtc_procx_GetCounter();
        // copy payload to send
        strncpy(delayed_msg_selector[msg_to_send.qos].payload,
                msg_to_send.payload,
				UP_MSG_PAYLOAD_ASCII_WITH_TERM);
        debug_printf( NWK,  "At %lu sec Send %s\r\n",
                            delayed_msg_selector[msg_to_send.qos].t_send,
                            delayed_msg_selector[msg_to_send.qos].payload);
    }

    return;
}

/*******************************************************************************
 * @brief Initialise ring buffers
 * @param[in] void
 * @return void
 *******************************************************************************/
void msg_handler_init(void){

    // Initialise RIng Buffers
    ring_buf_init(  &ring_buf_qos_0,
                    data_buffer_qos_0,
                    QOS_0_DATA_LENGTH_ASCII,
                    QOS_0_BUFFER_DEPTH);

    ring_buf_init(  &ring_buf_qos_1,
                    data_buffer_qos_1,
                    QOS_1_DATA_LENGTH_ASCII,
                    QOS_1_BUFFER_DEPTH);

    ring_buf_init(  &ring_buf_qos_2,
                    data_buffer_qos_2,
                    QOS_2_DATA_LENGTH_ASCII,
                    QOS_2_BUFFER_DEPTH);

    // reset to max as to be void
    delayed_msg_selector[QOS_0].t_send = (uint32_t)-1;
    delayed_msg_selector[QOS_1].t_send = (uint32_t)-1;
    delayed_msg_selector[QOS_2].t_send = (uint32_t)-1;

    return;
}

/*******************************************************************************
 * @brief Service delayed uplinks
 * @param[in] void
 * @return void
 *******************************************************************************/
static void msg_handler_service_delayed_msg(void){

    // Check for delayed critical data
    if( rtc_procx_GetCounter() > delayed_msg_selector[QOS_2].t_send ){
        ring_buf_force_write_at_back(
                (RINGBUFF_TYPE *)&ring_buf_qos_2,
                (char *)delayed_msg_selector[QOS_2].payload);
        // void delayed msg
        delayed_msg_selector[QOS_2].t_send = (uint32_t)-1;
    }

    // Check for delayed fault reporting
    if( rtc_procx_GetCounter() > delayed_msg_selector[QOS_1].t_send ){
        ring_buf_force_write_at_back(
                (RINGBUFF_TYPE *)&ring_buf_qos_1,
                (char *)delayed_msg_selector[QOS_1].payload);
        // void delayed msg
        delayed_msg_selector[QOS_1].t_send = (uint32_t)-1;
    }

    // Check for delayed non critical data
    if( rtc_procx_GetCounter() > delayed_msg_selector[QOS_0].t_send ){
        // Drop status uplink if too late to avoid unecessary TSYNC
        if(rtc_procx_GetCounter() < (delayed_msg_selector[QOS_0].t_send + DELAYED_STATUS_LIMIT_SEC))
        {
            ring_buf_force_write_at_back(
                    (RINGBUFF_TYPE *)&ring_buf_qos_0,
                    (char *)delayed_msg_selector[QOS_0].payload);
        }
        // void delayed msg
        delayed_msg_selector[QOS_0].t_send = (uint32_t)-1;
    }

    return;
}
