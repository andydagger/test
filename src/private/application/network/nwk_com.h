/*
==========================================================================
 Name        : nwk_com.h
 Project     : pcore
 Path        : /pcore/src/private/application/network/nwk_com.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 23 May 2018
 Description :
==========================================================================
*/

#ifndef PRIVATE_APPLICATION_NETWORK_NWK_COM_H_
#define PRIVATE_APPLICATION_NETWORK_NWK_COM_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
//bool nwk_com_process_msg(QuNwkComMsg nwk_com_msg);
void vBroadcastTimeOutCallBack( void );

bool nwk_com_init();
bool nwk_com_connect(void);
bool nwk_com_send_msg(char * p_to_data, bool b_msg_requires_acked);
bool nwk_com_read_msg(void);

bool nwk_com_update_broabcast_status(bool b_new_mcast_status);

bool get_nwk_com_broadcast_is_active(void);
void set_nwk_com_broadcast_session_should_be_active(bool b_request);
bool get_nwk_com_broadcast_session_should_be_active(void);

bool get_nwk_com_is_connected(void);
bool get_nwk_com_hw_needs_initialising(void);
bool get_nwk_com_rx_msg_pending(void);
bool nwk_com_load_uplink(   uint32_t delay_sec,
                            bool priority,
                            uint8_t qos,
                            const char *fmt, ...);
void nwk_com_save_session_info(void);
void nwk_com_full_hw_reset(void);
void nwk_com_force_rejoin(void);

void nwk_com_optimise_settings();
bool get_nwk_com_settings_need_optimising();


void set_nwk_com_settings_need_optimising(void);
bool get_nwk_com_fullNetworkResetRequired();
void clear_nwk_com_fullNetworkResetRequired();
void set_nwk_com_fullNetworkResetRequired();

/*****************************************************************************
 * Function to fetch the DEVEUI - return false when not yet available
 ****************************************************************************/
void get_nwk_com_dev_eui(char * p_devEuiString);

#endif /* PRIVATE_APPLICATION_NETWORK_NWK_COM_H_ */
