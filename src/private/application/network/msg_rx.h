/*
==========================================================================
 Name        : msg_rx.h
 Project     : pcore
 Path        : /pcore/src/private/application/network/msg_rx.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 24 May 2018
 Description : 
==========================================================================
*/

#ifndef PRIVATE_ABSTRACTION_NETWORK_MSG_RX_H_
#define PRIVATE_ABSTRACTION_NETWORK_MSG_RX_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include <stdbool.h>
#include <stdint.h>

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
// 1 byte for brightness, stop and start times utc 4 bytes each
#define LUMINAIR_OVERRIDE_MSG_CONTENT_LENGTH     9
// maximum on time is 6 hrs in seconds
#define LUMINAIR_OVERRIDE_MAX_ON_TIME_SEC        (60*6)
// maximum on-time delay is 7 days
#define LUMINAIR_OVERRIDE_MAX_ON_TIME_DELAY_SEC  (60*24*7)

// status period is in minutes within a uint8_t allowing up to 255 minutes
#define STATUS_PERIOD_MSG_CONTENT_LENGTH         1

// driver reset does not contain any payload
#define DRIVER_RESET_MSG_CONTENT_LENGTH          0

#define DRIVER_RESET_MCU_RESET_DELAY             60

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
bool msg_rx_process(char * nwk_rx_msg);

#endif /* PRIVATE_ABSTRACTION_NETWORK_MSG_RX_H_ */
