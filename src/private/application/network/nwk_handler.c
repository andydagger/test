/*
==========================================================================
 Name        : nwk_handler.c
 Project     : pcore
 Path        : /pcore/src/private/application/network/nwk_handler.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 8 Jan 2019
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include "stdint.h"
#include "stdbool.h"
#include <string.h>
#include "common.h"
#include "systick.h"
#include "debug.h"
#include "sysmon.h"
#include "nwk_com.h"
#include "nwk_handler.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "FreeRTOSMacros.h"
#include "FreeRTOSCommonHooks.h"
#include "productionMonitor.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define DAYS_BETWEEN_SESSION_SAVE 2
// #define SESSION_SAVE_GAP (uint32_t)(60*60*24*DAYS_BETWEEN_SESSION_SAVE)
#define SESSION_SAVE_GAP (uint32_t)(60*60*6)

static uint32_t timeOfLastSessionSave = 0;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void nwk_handler_sm_reset(void);
static void nwk_handler_sm_setup(void);
static void nwk_handler_sm_join(void);
static void nwk_handler_sm_idle(void);

/*****************************************************************************
 * Public functions
 ****************************************************************************/
// create the state-machine: smtype
typedef struct
{
    nwk_sm_states_list current_state;
    void (*func)(void);
    char * p_state_name_string;
}nwk_sm_smtype;
// declare the state to function routing
static const nwk_sm_smtype nwk_handler_sm[] =
{
    { NWK_RESET,    nwk_handler_sm_reset,   "NWK_RESET"},
    { NWK_SETUP,    nwk_handler_sm_setup,   "NWK_SETUP"},
    { NWK_JOIN,     nwk_handler_sm_join,    "NWK_JOIN"},
    { NWK_IDLE,     nwk_handler_sm_idle,    "NWK_IDLE"}
};

static nwk_sm_states_list nwk_handler_sm_state = NWK_RESET;


nwk_sm_states_list nwk_handler_run(void){
    static nwk_sm_states_list previous_state = NWK_RESET;

    // call sm sate function
    (*nwk_handler_sm[nwk_handler_sm_state].func)();

    if( previous_state != nwk_handler_sm_state){
        previous_state = nwk_handler_sm_state;
        debug_printf(   NWK,
                        "******  Handler SM State = %s ******\r\n",
                        nwk_handler_sm[nwk_handler_sm_state].p_state_name_string);
    }

    return nwk_handler_sm_state;
}


static void nwk_handler_sm_reset(void){
    // Boot up random delay
    uint32_t radioKickOffPseudoRandomDelay = 0;
    if(productionMonitor_getUnitIsUndertest() == false) 
    {
        radioKickOffPseudoRandomDelay = get_random_delay_ms();
        debug_printf( MAIN, "Random delay = %d ms\r\n", radioKickOffPseudoRandomDelay);
        FreeRTOSDelay(radioKickOffPseudoRandomDelay);
    }
    else
    {
        debug_printf( MAIN, "Random delay = N/A under test ms\r\n");
    }
    nwk_handler_sm_state = NWK_SETUP;
    return;
}

static void nwk_handler_sm_setup(void){
    // For full reset true, allowNetworkSessionRecovery shall be false
    if( true == nwk_com_init() ){
        nwk_handler_sm_state = NWK_JOIN;
    }else{
        // stay is same state until operation is successfull
    }
    return;
}

static void nwk_handler_sm_join(void){
    // wait for network to be ready when under production test
    if(productionMonitor_getNetworkReadyForDevice() == false)
    {
        return;
    }
    // Connect node to network - will always return within 12min
    if(true == nwk_com_connect()){
        nwk_handler_sm_state = NWK_IDLE;
        sysmon_status_uplink_enable();
    }else{
        if(get_nwk_com_hw_needs_initialising() == true)
        {
            nwk_handler_sm_state = NWK_SETUP;
        }
    }
    return;
}

static void nwk_handler_sm_idle(void){
    // can now transmit and receive across betwork


    // Check for changes in Broadcast status
    if (get_nwk_com_broadcast_session_should_be_active()
            != get_nwk_com_broadcast_is_active())
    {
        nwk_com_update_broabcast_status(
                get_nwk_com_broadcast_session_should_be_active());
    }

    // If not connected - attempt to rejoin
    if( get_nwk_com_is_connected() == false ){
        nwk_handler_sm_state = NWK_JOIN;
    }

    // Check if the nwk hardware needs initialising
    if( get_nwk_com_hw_needs_initialising() == true){
        nwk_handler_sm_state = NWK_SETUP;
    }

    // Only save session from IDLE state
    if( nwk_handler_sm_state == NWK_IDLE )
    {
        // Check if session info needs saving
        if( (get_systick_sec() - timeOfLastSessionSave) > SESSION_SAVE_GAP)
        {
            timeOfLastSessionSave = get_systick_sec();
            nwk_com_save_session_info();
        } 

        // Recovery Mechanism in case of disconnection
        if(get_nwk_com_settings_need_optimising() == true)
        {
            nwk_com_optimise_settings();
        }
    }

    return;
}


bool get_nwk_handler_nwk_is_ready(void){
    if(get_nwk_com_is_connected() == false) return false;
    if(get_nwk_com_hw_needs_initialising() == true) return false;
    return (NWK_IDLE == nwk_handler_sm_state) ? true : false;
}
