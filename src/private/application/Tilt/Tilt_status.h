#ifndef TILT_STATUS_H_
#define TILT_STATUS_H_

#include <stdint.h>
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

typedef enum
{
  TILT_STATUS_NULL = 0,
  TILT_STATUS_OK,
  TILT_STATUS_IMPACT_EVENT,
  TILT_STATUS_RECOVERY_EVENT,
  TILT_STATUS_ERROR

} Tilt_status;
//------------------------------------------------------------------------------

typedef struct
{
	int16_t X;
	int16_t Y;
	int16_t Z;

} Tilt_output;

//------------------------------------------------------------------------------
// General purpose Tilt_output API

void Tilt_output_copy(Tilt_output* src, Tilt_output* dest);
//------------------------------------------------------------------------------
void Tilt_output_clr(Tilt_output* _this);
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#endif /* TILT_STATUS_H_ */
