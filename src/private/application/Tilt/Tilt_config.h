

#ifndef TILT_CONFIG_H_
#define TILT_CONFIG_H_

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

// Must be 2^TILT_CONFIG_INIT_SHIFT_DIVIDE
#define TILT_CONFIG_INIT_NUM_SAMPLES                 32

// Must be Log2(TILT_CONFIG_INIT_NUM_SAMPLES)
#define TILT_CONFIG_INIT_SHIFT_DIVIDE                5

//------------------------------------------------------------------------------
#define TILT_CONFIG_TILT_IMPACT_THRESH_DEFAULT      20.0

#define TILT_CONFIG_TILT_RESTORE_THRESH_DEFAULT     10.0

//------------------------------------------------------------------------------

// Number of tilt hardware read errors (not necessarily contiguous)
// that must occur before a Tilt module reset is triggered.
#define TILT_CONFIG_HW_READ_ERROR_RESET_THRESH      10

// Max number of Tilt module resets before the Tilt module reports an error
// to its caller, i.e., the layer above.
#define TILT_CONFIG_NUM_RESETS_MAX                  3

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#endif /* TILT_CONFIG_H_ */
