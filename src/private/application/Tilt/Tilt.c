

#include "Tilt.h"
#include "Tilt_config.h"
#include <math.h>
#include <stdio.h>

//------------------------------------------------------------------------------
// This is the operational object
static Tilt tilt;

Tilt * Tilt_getRef(void){
    return &tilt;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// State machine section
enum
{
  _STATE_INIT = 0,
  _STATE_OK,
  _STATE_IMPACT,
  _STATE_ERROR
};

// State function prototype.
typedef uint8_t (*StateFunc)(Tilt* _this);

// State functions
static uint8_t init(Tilt* _this);
static uint8_t ok(Tilt* _this);
static uint8_t impact(Tilt* _this);
static uint8_t error(Tilt* _this);

// Array of state function pointers.
// MUST be in the same order as the state code enum
// (see this module's header file).
static const StateFunc g_stateFunc[] = {&init, &ok, &impact, &error};
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Multiply angle in radians by this to convert to degrees.
#define _RADIANS_TO_DEGREES                          57.3
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static void initObj(Tilt* _this);
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Common processing function, i.e., contains shared code for
// ok and impact state functions.
//
// The only variables in the Tilt object that are changed are:
// xYZBuff, the reference variables and tiltDiff
// (current average relative tilt, i.e., difference between
// reference tilt and current tilt)
//
// Returns return code below.
//
enum
{
  _PROCESS_NOW = 0,      /* There ARE new X,Y & Z values - so process them now */
  _PROCESS_NONE,         /* There are NO new X,Y & Z values - so do nothing */
  _PROCESS_ERROR_RESET,  /* Error - Tilt module should be reset now */
  _PROCESS_ERROR_EXT     /* Error - Report error externally */
};

static uint8_t commonProcess(Tilt* _this);
//------------------------------------------------------------------------------
// Updates Tilt object's error variables depending on readStatus.
// Returns return code below.

enum
{
  _NO_ERROR = 0,    /* no error */
  _RESET,           /* reset module - i.e. goto init state */
  _EXT_ERROR        /* report external error & goto error state */
};

static uint8_t errorUpdate(Tilt* _this, IhalTilt_readStatus readStatus);
//------------------------------------------------------------------------------
static float calcTilt(IhalTilt_output* pIhalTilt_output);

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

void Tilt_ctor(Tilt* _this, const uint32_t mode, char* gpStr)
{
  _this->mode = mode;

  _this->pStr = gpStr;

  _this->resetCount = 0;

  initObj(_this);
}
//------------------------------------------------------------------------------
Tilt_status Tilt_run(Tilt* _this)
{
  _this->state = g_stateFunc[_this->state](_this);

  return _this->status;
}
//------------------------------------------------------------------------------
void Tilt_getXYZ(Tilt* _this, Tilt_output* pOutput)
{
  pOutput->X = _this->refX;
  pOutput->Y = _this->refY;
  pOutput->Z = _this->refZ;
}
//------------------------------------------------------------------------------
float Tilt_getTilt(Tilt* _this)
{
  return _this->tiltDiff;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// **** Start of state functions

static uint8_t init(Tilt* _this)
{
  // Read the tilt hardware.
  IhalTilt_readStatus readStatus = IhalTilt_readSingleWait(&_this->ihalTilt_output);

  //----------------------------------
  // error processing

  uint8_t error = errorUpdate(_this, readStatus);

  if(_RESET == error)
  {
      _this->status = TILT_STATUS_OK;

      initObj(_this);

      return _STATE_INIT;
  }

  else if(_EXT_ERROR == error)
  {
      _this->status = TILT_STATUS_ERROR;

      return _STATE_ERROR;
  }

  else{ /* no error - so continue */ }

  _this->status = TILT_STATUS_OK;
  //----------------------------------

  //  debug print

  LIS2DH12_output_sprint(&_this->ihalTilt_output, _this->pStr);

  //----------------------------------

  _this->initAccX += _this->ihalTilt_output.X;
  _this->initAccY += _this->ihalTilt_output.Y;
  _this->initAccZ += _this->ihalTilt_output.Z;

  _this->initCount++;

  if(_this->initCount >= TILT_CONFIG_INIT_NUM_SAMPLES)
  {
    // Enough samples have been taken, so determine reference values now.

    _this->refX = _this->initAccX >> TILT_CONFIG_INIT_SHIFT_DIVIDE;
    _this->refY = _this->initAccY >> TILT_CONFIG_INIT_SHIFT_DIVIDE;
    _this->refZ = _this->initAccZ >> TILT_CONFIG_INIT_SHIFT_DIVIDE;

    // Calculate reference tilt from reference X, Y & Z.
    IhalTilt_output tempOutput = {_this->refX, _this->refY, _this->refZ};

    _this->refTilt = calcTilt(&tempOutput);

    return _STATE_OK;
  }
  else
  {
    return _STATE_INIT;
  }
}
//------------------------------------------------------------------------------
static uint8_t ok(Tilt* _this)
{
  _this->status = TILT_STATUS_NULL;

  uint8_t processNow = commonProcess(_this);

  if(_PROCESS_NOW == processNow)
  {

    if(_this->tiltDiff >= TILT_CONFIG_TILT_IMPACT_THRESH_DEFAULT)
    {
      // Impact event detected

        _this->status = TILT_STATUS_IMPACT_EVENT;

      return _STATE_IMPACT;
    }
    else
    {
      // No Impact event detected
      _this->status = TILT_STATUS_OK;
    }
  }

  else if(_PROCESS_NONE == processNow)
  {
    // Buffer is not full, so do nothing
    _this->status = TILT_STATUS_OK;
  }

  else if(_PROCESS_ERROR_RESET == processNow)
  {
      _this->status = TILT_STATUS_OK;

      initObj(_this);

      return _STATE_INIT;
  }

  else if(_PROCESS_ERROR_EXT == processNow)
  {
      _this->status = TILT_STATUS_ERROR;

      return _STATE_ERROR;
  }

  else
  {
      _this->status = TILT_STATUS_NULL;
  }


  //----------------------------------

  return _STATE_OK;
}
//------------------------------------------------------------------------------
static uint8_t impact(Tilt* _this)
{
  _this->status = TILT_STATUS_NULL;

  bool processNow = commonProcess(_this);

  if(true == processNow)
  {

    if(_this->tiltDiff < TILT_CONFIG_TILT_RESTORE_THRESH_DEFAULT)
    {
      // Restore event detected

      _this->status = TILT_STATUS_RECOVERY_EVENT;

      return _STATE_OK;
    }
    else
    {
      // No Restore event detected
      _this->status = TILT_STATUS_OK;
    }
  }

  else
  {
    // Buffer is not full, so do nothing
    _this->status = TILT_STATUS_OK;
  }

  //----------------------------------

  return _STATE_IMPACT;
}
// **** End of state functions
//------------------------------------------------------------------------------
static uint8_t error(Tilt* _this)
{
  return _STATE_ERROR;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

static void initObj(Tilt* _this)
{
  _this->state = _STATE_INIT;

  _this->initCount = 0;

  _this->initAccX = 0;
  _this->initAccY = 0;
  _this->initAccZ = 0;

  _this->tiltDiff = 0;

  _this->hwReadErrorCount = 0;

  Latch_ctor(&_this->ImpactLatch);

  // Initialise tilt hardware.
  IhalTilt_init(_this->mode);

  XYZBuff_ctor(&_this->xYZBuff);

  // Initialise reference time.
  _this->refTime = rtc_procx_GetTime();

}
//------------------------------------------------------------------------------
static uint8_t commonProcess(Tilt* _this)
{
    // Read the tilt hardware.
    IhalTilt_readStatus readStatus = IhalTilt_readSingleWait(&_this->ihalTilt_output);

    //----------------------------------
    // error processing

    uint8_t error = errorUpdate(_this, readStatus);

    if(_RESET == error)
    {
        return _PROCESS_ERROR_RESET;
    }

    else if(_EXT_ERROR == error)
    {
        return _PROCESS_ERROR_EXT;
    }

    else{ /* no error - so continue */ }

    _this->status = TILT_STATUS_OK;

    //----------------------------------
    //  debug print

    LIS2DH12_output_sprint(&_this->ihalTilt_output, _this->pStr);

    //----------------------------------

    // Store X,Y & Z samples in buffer and check if buffer is full.
    bool bufferFull = XYZBuff_put((XYZBuff*)&_this->xYZBuff, (Tilt_output*)&_this->ihalTilt_output);

    if(true == bufferFull)
    {
      // Buffer is full, so get average values from it.

      int16_t X = XYZBuff_getAvgX(&_this->xYZBuff);
      int16_t Y = XYZBuff_getAvgY(&_this->xYZBuff);
      int16_t Z = XYZBuff_getAvgZ(&_this->xYZBuff);

      IhalTilt_output avgOutput = {X, Y, Z};
      //----------------------------------
      //  debug print of current average X, Y & Z values.

      LIS2DH12_output_sprint(&avgOutput, _this->pStr);
      //----------------------------------

      // calculate current absolute tilt.
      float tiltNow = calcTilt(&avgOutput);

      //----------------------------------

      // calculate relative tilt, i.e., difference between absolute
      // and reference tilt.
      _this->tiltDiff = fabs((double) (tiltNow - _this->refTilt));

      return _PROCESS_NOW;
    }
    else
    {
      return _PROCESS_NONE;
    }
}
//------------------------------------------------------------------------------
static uint8_t errorUpdate(Tilt* _this, IhalTilt_readStatus readStatus)
{
  if(IHALTILT_READ_OK == readStatus)
  {
    // hardware read was ok - so continue

    return _NO_ERROR;
  }
  else
  {
      // hardware read was NOT ok

      _this->hwReadErrorCount++;

      if(_this->hwReadErrorCount >= TILT_CONFIG_HW_READ_ERROR_RESET_THRESH)
      {
          //_this->hwReadErrorCount = 0;

          _this->resetCount++;

          if(_this->resetCount >= TILT_CONFIG_NUM_RESETS_MAX)
          {
            // Time to report an external error
            return _EXT_ERROR;
          }
          else
          {
            return _RESET;
          }
      }
      else
      {
        return _NO_ERROR;
      }
  }
}
//------------------------------------------------------------------------------
static float calcTilt(IhalTilt_output* pIhalTilt_output)
{
  float r = (pIhalTilt_output->X * pIhalTilt_output->X) +\
            (pIhalTilt_output->Y * pIhalTilt_output->Y) +\
            (pIhalTilt_output->Z * pIhalTilt_output->Z);

  r = sqrt((double) r);

  float theta = acos((double) pIhalTilt_output->Z / (double) r);

  theta *= _RADIANS_TO_DEGREES;

  theta = 180 - theta;

  return theta;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Implementation of General purpose Tilt_output API

void Tilt_output_copy(Tilt_output* src, Tilt_output* dest)
{
  dest->X = src->X;
  dest->Y = src->Y;
  dest->Z = src->Z;
}
//------------------------------------------------------------------------------
void Tilt_output_clr(Tilt_output* _this)
{
  _this->X = 0;
  _this->Y = 0;
  _this->Z = 0;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

