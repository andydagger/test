
#include "Latch.h"
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void Latch_ctor(Latch* _this)
{
  _this->state = LATCH_STATE_UNSET;
}
//------------------------------------------------------------------------------
void Latch_set(Latch* _this)
{
  _this->state = LATCH_STATE_SET;
}
//------------------------------------------------------------------------------
bool Latch_setAndTest(Latch* _this)
{
  bool ret = (LATCH_STATE_UNSET == _this->state) ? true : false;

  _this->state = LATCH_STATE_SET;

  return ret;
}
//------------------------------------------------------------------------------
void Latch_reset(Latch* _this)
{
  _this->state = LATCH_STATE_UNSET;
}
//------------------------------------------------------------------------------
Latch_state getState(Latch* _this)
{
  return _this->state;
}
//------------------------------------------------------------------------------
