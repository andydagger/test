
#ifndef TILT_H_
#define TILT_H_

#include <stdint.h>
#include <stddef.h>
#include "Tilt_status.h"
#include "IhalTilt.h"
#include "XYZBuff.h"
#include "Latch.h"
#include "rtc_procx.h"
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

typedef struct
{
  //-----------------------------------
  uint32_t mode;

  uint8_t state;

  //-----------------------------------
  Tilt_status status;
  Latch ImpactLatch;
  //-----------------------------------
  //IhalTilt_readStatus ihalTilt_readStatus;
  IhalTilt_output ihalTilt_output;
  //-----------------------------------
  // Initialisation variables

  uint8_t initCount;

  int32_t initAccX;
  int32_t initAccY;
  int32_t initAccZ;
  //-----------------------------------
  // Reference  variables
  //- updated periodically (typically daily)

  int16_t refX;
  int16_t refY;
  int16_t refZ;

  float refTilt;

  RTCTime refTime;
  //-----------------------------------

  float tiltDiff;
  //-----------------------------------
  XYZBuff xYZBuff;

  //-----------------------------------
  // Error variables
  uint16_t hwReadErrorCount;

  uint8_t resetCount;
  //-----------------------------------
  char* pStr;  // general purpose debug string

} Tilt;
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// **** Tilt Module API
// Get Ref to Tilt Object
Tilt * Tilt_getRef(void);

// Constructor
void Tilt_ctor(Tilt* _this, const uint32_t mode, char* gpStr);
//------------------------------------------------------------------------------
// Runs the Tilt module.
Tilt_status Tilt_run(Tilt* _this);
//------------------------------------------------------------------------------
// Used to obtain the current absolute X, Y & Z gravity components.
void Tilt_getXYZ(Tilt* _this, Tilt_output* pOutput);
//------------------------------------------------------------------------------
// Returns difference between the reference tilt and the current tilt.
float Tilt_getTilt(Tilt* _this);
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#endif /* TILT_H_ */
