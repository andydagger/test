
#include "XYZBuff.h"
#include <string.h>
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void XYZBuff_ctor(XYZBuff* _this)
{
  uint8_t i;

  _this->head = 0;

  for(i=0; i<XYZBUFF_CONFIG_LEN; i++)
  {
    Tilt_output_clr(&_this->data[i]);
  }
}
//------------------------------------------------------------------------------
bool XYZBuff_put(XYZBuff* _this, Tilt_output* pValue)
{
  Tilt_output_copy(pValue, &_this->data[_this->head]);

  _this->head++;

  if(_this->head >= XYZBUFF_CONFIG_LEN)
  {
    // rollover
    _this->head = 0;

    return true;
  }
  else
  {
    return false;
  }
}
//------------------------------------------------------------------------------
int16_t XYZBuff_getAvgX(XYZBuff* _this)
{
  uint8_t i;
  int32_t avg = 0;

  for(i=0; i<XYZBUFF_CONFIG_LEN; i++)
  {
    avg += _this->data[i].X;
  }

  avg >>= XYZBUFF_CONFIG_SHIFT_DIVIDER;

  return (int16_t) avg;
}
//------------------------------------------------------------------------------
int16_t XYZBuff_getAvgY(XYZBuff* _this)
{
  uint8_t i;
  int32_t avg = 0;

  for(i=0; i<XYZBUFF_CONFIG_LEN; i++)
  {
    avg += _this->data[i].Y;
  }

  avg >>= XYZBUFF_CONFIG_SHIFT_DIVIDER;

  return (int16_t) avg;
}
//------------------------------------------------------------------------------
int16_t XYZBuff_getAvgZ(XYZBuff* _this)
{
  uint8_t i;
  int32_t avg = 0;

  for(i=0; i<XYZBUFF_CONFIG_LEN; i++)
  {
    avg += _this->data[i].Z;
  }

  avg >>= XYZBUFF_CONFIG_SHIFT_DIVIDER;

  return (int16_t) avg;
}
//------------------------------------------------------------------------------
bool XYZBuff_getFull(XYZBuff* _this)
{
	return false;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
