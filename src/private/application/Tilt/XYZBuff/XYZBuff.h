

#ifndef XYZBUFF_H_
#define XYZBUFF_H_

#include <stdint.h>
#include <stdbool.h>
#include "XYZBuff_config.h"
#include "Tilt_status.h"
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

typedef struct
{
  uint8_t head;

  Tilt_output data[XYZBUFF_CONFIG_LEN];

} XYZBuff;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void XYZBuff_ctor(XYZBuff* _this);
//------------------------------------------------------------------------------
// Copies cartesian coordinate data in pValue into buffer.
// Does not check full status of buffer, i.e., it overwrites.
// Wraps round if end reached.
// Returns true if end reached, i.e., overflow.
// Otherwise returns false.

bool XYZBuff_put(XYZBuff* _this, Tilt_output* pValue);
//------------------------------------------------------------------------------
int16_t XYZBuff_getAvgX(XYZBuff* _this);
//------------------------------------------------------------------------------
int16_t XYZBuff_getAvgY(XYZBuff* _this);
//------------------------------------------------------------------------------
int16_t XYZBuff_getAvgZ(XYZBuff* _this);
//------------------------------------------------------------------------------
bool XYZBuff_getFull(XYZBuff* _this);
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#endif /* XYZBUFF_H_ */
