#ifndef XYZBUFF_CONFIG_H_
#define XYZBUFF_CONFIG_H_

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

// Buffer length (number of samples)
#define XYZBUFF_CONFIG_LEN                           16

// number of bits to shift left in order to average X, Y & Z values.
// MUST equal Log2(XYZBUFF_CONFIG_LEN)
//
#define XYZBUFF_CONFIG_SHIFT_DIVIDER                 4
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#endif /* XYZBUFF_CONFIG_H_ */
