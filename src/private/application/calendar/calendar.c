/*
==========================================================================
 Name        : calendar.c
 Project     : pcore
 Path        : /pcore/src/private/calendar/calendar.c
 Author      : M Nakhuda
 Copyright   : Lucy Zodion Ltd
 Created     : 30 May 2018
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/
#include "chip.h"
#include "stdbool.h"
#include "stdint.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "hw.h"
#include "common.h"
#include "debug.h"
#include "sysmon.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "calendar_process.h"
#include "calendar.h"
#include "solar_clock.h"

#include "rtc_procx.h"
#include "adc_procx.h"
#include "gps.h"
#include "photo.h"

#include "compileUplink.h"

extern QueueHandle_t xQuTxData; // vTaskTxData

static SolarClockDataset    solar_clock_dataset = {0};

//extern NVMemoryCalendarParams_t Default_Calendar_NVMemory;

static volatile uint8_t Cal_Main_SM_State = CAL_MAIN_SM_INIT;

CalendarInfo_t CalendarInfo;

static RTCTime t;

static bool b_profile_is_on = false;
static bool b_profile_was_on = false;
static bool boolWaitForNextProfile = false;
static bool boolWaitForNextSP_ON_OFF_Profile = false;
static bool  b_cal_is_initialised = false;
static bool recalc_solar_clock = true;

static uint8_t Previous_RTC_Min = 0;
static uint8_t Lamp_Output_level = 100;  /* Default to 100% Brightness */

static uint16_t PrevDayNumber = 0;
static uint16_t TimeMinsfromNoon = 0;
static uint16_t DayNumber = 0;
static bool b_gps_locked = false;
static bool b_gps_data_locked = false;
static Photo_States_t Curr_Photo_State = PHOTO_LIGHT_LATCHED;  /* Start from Default Lamp Off State and hence Photo is in LIght mode */
static Photo_States_t Prev_Photo_State = PHOTO_NOT_READY;
static uint16_t Crossing_Delay = 0;         /* Default OFF State Crossing point Zero reached */
static uint16_t Crossing_Latch_Delay = 0;   /* Default OFF State Crossing point Zero reached */
static bool InitINProgress = false;

static BillingDataset   billing_dataset = {0};

static uint32_t         Ticks_Since_SwitchOn = 0;

static void cal_main_sm_get_rtcc(void );
static void cal_main_sm_calc_day_number(void );
static void cal_main_sm_check_exception_table(void );
static void cal_main_sm_check_weekly_table(void );
static void cal_main_sm_get_sp_from_profile_id(void );
static void cal_main_sm_get_init_photo_state(void );
static void cal_main_sm_get_init_rtcc_state(void );
static void cal_main_sm_get_sp_on_off_mode(void );
static void cal_main_sm_get_photo_state(void );
static void cal_main_sm_get_time_state(void );
static void cal_main_sm_get_solar_state(void);
static void cal_main_sm_get_next_sp(void );
static void cal_set_luminr_op( void );

static void cal_billing_process( bool b_profile_switch_on );
static bool cal_send_billing(BillingDataset billing_data);

static Photo_States_t cal_photo_check_threshold_with_params(uint16_t dThreshold);
static Photo_States_t cal_photo_check_for_dark(uint32_t adc_photo_read);
static Photo_States_t cal_photo_check_for_light(uint32_t adc_photo_read);

extern QueueHandle_t xQuLuminrOp; // vTaskLuminrOp

/*******************************************************************************
 * @brief cal_set_state
 * @param[in] Next_State state to jump to
 * @return None
 *******************************************************************************/
static void cal_set_state(uint8_t Next_State)
{
    Cal_Main_SM_State = Next_State;
}

/*******************************************************************************
 * @brief cal_photo_check_threshold_with_params
 * @param[in] uThreshold
 * @return None
 *******************************************************************************/
static Photo_States_t cal_photo_check_for_light(uint32_t adc_photo_read)
{
    uint16_t uThreshold = 0;
    uint16_t dHysteresis = 0;
    static uint32_t debounce_ref = 0;
    static uint32_t debounce_ref_latch = 0;

    uThreshold = CalendarInfo.Profile.Switching_Points[1].Trigger.Threshold;

    /* DEBUG ++ */
    if ( Curr_Photo_State != PHOTO_LIGHT )
    {

        debug_printf( CALEN,"wait for LIGHT Thresh = %d, lux =  %lu, State = %d \r\n", uThreshold, adc_photo_read, Curr_Photo_State);
    }
    else
    {
        debug_printf( CALEN,"wait L LATCH Thresh = %d, lux =  %lu, State = %d \r\n", ( uThreshold  - (uThreshold/ 4)), adc_photo_read, Curr_Photo_State);
    }
    /* DEBUG -- */

    // Photo value is compared with (Threshold + Hysteresis) value if we are in Light then we are waiting for Latch
    if((adc_photo_read > (uThreshold + dHysteresis)) || ( Curr_Photo_State == PHOTO_LIGHT ))
    {
        if ( Curr_Photo_State != PHOTO_LIGHT )
        {
            /* Wait for consecutive readings before switching */
            if((rtc_procx_GetCounter() - debounce_ref) > 10){
                Curr_Photo_State = PHOTO_LIGHT;
            }
        }
        else
        {
            /* Waiting for Latch when the Light is above preset threshold */
            if(adc_photo_read > (LIGHT_LATCH_HIGH))
            {
                /* Wait for consecutive readings before switching */
                if((rtc_procx_GetCounter() - debounce_ref_latch) > 10){
                    Curr_Photo_State = PHOTO_LIGHT_LATCHED;
                }
                debounce_ref = rtc_procx_GetCounter();
            }
            /* Latch when the Light level is 25% lower than the current required Lux level */
            else if(adc_photo_read < ( uThreshold  - (uThreshold/ 4)))
            {
                /* Wait for consecutive readings before switching */
                if((rtc_procx_GetCounter() - debounce_ref) > 10){
                    Curr_Photo_State = PHOTO_DARK_LATCHED;
                }
            }
            else
            {
                debounce_ref_latch = rtc_procx_GetCounter();
                debounce_ref = rtc_procx_GetCounter();
            }

        }
    }
    else
    {
        /* The Light level is much Lower than expected so Are we getting Lighter */
        if ( Curr_Photo_State != PHOTO_LIGHT )
        {
            debounce_ref = rtc_procx_GetCounter();
        }
    }

    debug_printf( CALEN,"L? deb = %d latch = %d  rtc = %d\r\n",
            debounce_ref, debounce_ref_latch, rtc_procx_GetCounter());

    return Curr_Photo_State;
}
/*******************************************************************************
 * @brief cal_photo_check_threshold_with_params
 * @param[in] uThreshold
 * @return None
 *******************************************************************************/
static Photo_States_t cal_photo_check_for_dark(uint32_t adc_photo_read)
{
    uint16_t uThreshold = 0;
    uint16_t dHysteresis = 0;
    static uint32_t debounce_ref = 0;
    static uint32_t debounce_ref_latch = 0;

    uThreshold = CalendarInfo.Profile.Switching_Points[0].Trigger.Threshold;

    /* DEBUG ++ */

    if ( Curr_Photo_State == PHOTO_DARK )
    {
        debug_printf( CALEN,"wait D LATCH Thresh = %d, lux =  %lu, State = %d \r\n", ( uThreshold  + (uThreshold/ 4)), adc_photo_read, Curr_Photo_State);
    }
    else
    {
        debug_printf( CALEN,"wait for DARK Thresh = %d, lux =  %lu, State = %d \r\n", uThreshold, adc_photo_read, Curr_Photo_State);
    }

    /* DEBUG -- */

    // Photo value is compared with (Threshold - Hysteresis) value if we are in Dark then we are waiting for Latch
    if((adc_photo_read < (uThreshold - dHysteresis)) || ( Curr_Photo_State == PHOTO_DARK ))
    {
        // We are waiting for DARK
        if ( Curr_Photo_State != PHOTO_DARK )
        {
            /* Wait for consecutive readings before switching */
            if((rtc_procx_GetCounter() - debounce_ref) > 10){
                Curr_Photo_State = PHOTO_DARK;
            }
        }
        else
        {
            // It is now very very very DARK
            if(adc_photo_read < LIGHT_LATCH_LOW )
            {
                /* Wait for consecutive readings before switching */
                if((rtc_procx_GetCounter() - debounce_ref_latch) > 10){
                    Curr_Photo_State = PHOTO_DARK_LATCHED;
                }
                debounce_ref = rtc_procx_GetCounter();
            }
            /* Latch when the Light level is 25% Higher than the current required Lux level */
            else if(adc_photo_read > ( uThreshold  + (uThreshold / 4)))   // Wait
            {
                /* Wait for consecutive readings before switching */
                if((rtc_procx_GetCounter() - debounce_ref) > 10){
                    Curr_Photo_State = PHOTO_LIGHT_LATCHED;
                }
            }
            else
            {
                debounce_ref_latch = rtc_procx_GetCounter();
                debounce_ref = rtc_procx_GetCounter();
            }
        }
    }
    else
    {
        /* The Light level is much higher than expected so Are we getting Lighter */
        if ( Curr_Photo_State != PHOTO_DARK )
        {
            debounce_ref = rtc_procx_GetCounter();
        }
    }

    debug_printf( CALEN,"D? deb = %d latch = %d  rtc = %d\r\n",
            debounce_ref, debounce_ref_latch, rtc_procx_GetCounter());

    return Curr_Photo_State;
}

/*******************************************************************************
 * @brief cal_photo_check_threshold_with_params
 * @param[in] uThreshold
 * @return None
 *******************************************************************************/
static Photo_States_t cal_photo_check_threshold_with_params(uint16_t uThreshold)
{
    uint32_t adc_photo_read = 0;

    adc_photo_read = (uint32_t)get_photo_LUX();

//  adc_photo_read = get_adc_procx_photo_ic_av_raw();
//
//  debug_printf( CAL," ADC_V = %04d \r\n", adc_photo_read);

    if(( Curr_Photo_State == PHOTO_LIGHT_LATCHED ) || ( Curr_Photo_State == PHOTO_DARK ))
    {
        cal_photo_check_for_dark(adc_photo_read);
    }
    else if(( Curr_Photo_State == PHOTO_LIGHT ) || ( Curr_Photo_State == PHOTO_DARK_LATCHED ))
    {
        /* If no GPS then need to check for switch OFF Light */
        /* Or running the default Profile then check as well */
        if (( b_gps_locked == false ) || (CalendarInfo.Current_Profile_ID == 0))
        {
            cal_photo_check_for_light(adc_photo_read);
        }
        else if( t.RTC_Hour < 12 )
        {
            /* Only look for Day Light between Midnight and 12pm */
            cal_photo_check_for_light(adc_photo_read);
        }
    }

    /* DEBUG ++ */
    debug_printf( CALEN," Crossing_Delay %d  Crossing_Latch_Delay %d \r\n", Crossing_Delay,Crossing_Latch_Delay);
    /* DEBUG -- */

    return Curr_Photo_State;
}

/*******************************************************************************
 * @brief calendar_check_sp
 * @param[in] None
 * @return None
 *******************************************************************************/
void calendar_check_sp(void)
{

    //debug_printf( CAL," >> Running Cal_Main_SM_State = %d\r\n", Cal_Main_SM_State);

    switch(Cal_Main_SM_State)
    {
        case CAL_MAIN_SM_INIT:                  //! Initial State
        {
            CalBinSM_t cal_file_return = CAL_BIN_GET_VERSION;

            do
            {
                /* Read until entire calendar file has been processed */
                cal_file_return = cal_file_init();

            }while (( cal_file_return != CAL_BIN_IDLE ) && ( cal_file_return != CAL_BIN_ERROR ));

            //---------------------------------------------------
            // KF-271
            //---------------------------------------------------
            // Reset last Lamp O/P from previous profile
            // Start at 0% by default
            debug_printf( CALEN,"Fetch New Profile...\r\n");
            Lamp_Output_level = 0;
            //---------------------------------------------------

            if (( cal_file_return == CAL_BIN_ERROR )|| ( b_gps_locked == false ))
            {
                /* Create default Profile Index 0 */
                cal_make_default_profile();

                cal_make_default_week();

                cal_make_default_exception();

                if ( cal_file_return == CAL_BIN_ERROR )
                {
                    /* Use default Calendar Profile Index 0 */
                    debug_printf(CALEN, "CAL_BIN_ERROR\r\n");
                }
                else
                {
                    debug_printf(CALEN, "b_gps_locked == false\r\n");
                }
            }

            debug_printf(CALEN, "Version = %d \r\n", CalendarInfo.Version);
            debug_printf(CALEN, "Profile_Cnt = %d \r\n", CalendarInfo.Profile_Cnt);

            InitINProgress = true;

            cal_set_state(CAL_MAIN_SM_GET_RTCC);

        }
        break;
        case CAL_MAIN_SM_GET_RTCC:  //! state
        {

            t = rtc_procx_GetTime();

            cal_main_sm_get_rtcc();
        }
        break;

        case CAL_MAIN_SM_CALC_DAY_NUMBER:
        {
            cal_main_sm_calc_day_number();
        }
        break;

        case CAl_MAIN_SM_CHECK_EXCEPTION_TABLE:
        {
            /* Check Exception Table */
            cal_main_sm_check_exception_table();
        }
        break;
        case CAL_MAIN_SM_CHECK_WEEKLY_TABLE:
        {
            cal_main_sm_check_weekly_table();
        }
        break;
        case CAL_MAIN_SM_GET_SP_FROM_PROFILE_ID:
        {
            cal_main_sm_get_sp_from_profile_id();
        }
        break;

        case CAL_MAIN_SM_GET_INIT_PHOTO_STATE:
        {
            cal_main_sm_get_init_photo_state();
        }
        break;
        case CAL_MAIN_SM_GET_INIT_RTCC_STATE:
        {
            cal_main_sm_get_init_rtcc_state();
        }
        break;

        case CAL_MAIN_SM_GET_SP_ON_OFF_MODE:
        {
            cal_main_sm_get_sp_on_off_mode();
        }
        break;

        case CAL_MAIN_SM_GET_PHOTO_STATE:
        {
            cal_main_sm_get_photo_state();
        }
        break;
        case CAL_MAIN_SM_GET_SOLAR_STATE:
        {
            /* Has the Solar Sunset/Sunrise offset Triggered */
            cal_main_sm_get_solar_state();
        }
        break;
        case CAL_MAIN_SM_GET_TIME_STATE:
        {
            cal_main_sm_get_time_state();
        }
        break;
        case CAL_MAIN_SM_GET_NEXT_SP:
        {
            cal_main_sm_get_next_sp();
        }
        break;
        case CAL_MAIN_SM_SET_DALI_AND_RELAY:
            /* Set DALI and the Light RELAY */
            break;
        case CAL_MAIN_SM_IDLE:      //! state
        {
            /* Keep checking RTCC , Should we use the RTCC wakeup Alarm*/
            cal_set_state(CAL_MAIN_SM_GET_RTCC);

            /* At alternate intervals check for next SP */

        }
        break;
        default:
            Cal_Main_SM_State = CAL_MAIN_SM_INIT;
            break;
    }
}

/*******************************************************************************
 * @brief cal_bin_get_exception_table
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_main_sm_get_rtcc(void )
{
    CalTrigger_States_t TriggerSource = 0;
    static bool b_prev_gps_locked = false;
    static bool b_prev_gps_data_locked = false;

    /* Check if the GPS has been updated from Time Synch function */
    if (( b_prev_gps_locked != b_gps_locked ) || ( b_prev_gps_data_locked != b_gps_data_locked ))
    {
        b_prev_gps_locked = b_gps_locked;

        b_prev_gps_data_locked = b_gps_data_locked;

        /* Have we got the GPS Time and CO-Ords yet? */
        if (( b_gps_locked ) && (b_gps_data_locked))
        {
            /* We now have what we need from the GPS Module, switch it OFF to reduce Power Consumption*/
            debug_printf(CALEN, "GPS SW OFF \r\n");
            gps_disable_module();
        }

        /* Re-synch the Solar Clock */
        cal_set_calc_solar_clock(true);

        /* Start the CAL state machine again with the current date and time */
        cal_reset_cal_sm();

        return;
    }

    /* Should we timeout after 10 mins of NO GPS signal ??? */
    if ( b_gps_locked == false )
    {
        uint8_t GPS_Buff[13]={0};
        bool get_gps_corrupt = false;

        /* Search for the GPS Co-ords */
        if (gps_parsed_locked_data())
        {
            /* Lat and Long has been found */
            b_gps_data_locked = true;
        }

        /* From a cold start this can take upto 7 mins */
        if ( timestamp_parsed_locked_data() )
        {
            (void)gps_get_utc((char *)GPS_Buff);

            debug_printf(CALEN, "GPS UTC =  %s \r\n", GPS_Buff);
            /* 104737080618 */  /* 10:47:37  08 06 2018 */
            /* 140547080618  */ /* 14:05:47  08  06 2018 */

            /* We should capture corrupt GPS coords in GPS.c then return them here */
            /* For now process here */
            for(int n = 0; n < 12; n++)
            {
                if (( GPS_Buff[n] < 0x30) || ( GPS_Buff[n] > 0x39))
                {
                    debug_printf(CALEN, "GPS CORRUPT\r\n");
                    get_gps_corrupt = true;

                    break;
                }
            }

            /* We should convert to ints in gps.c and return pointer to the structure */
            /* For now process here, code tidy up needed here //@@  */
            if (get_gps_corrupt == false)
            {
                char year[3];
                char month[3];
                char day[3];
                char hour[3];
                char minute[3];
                char second[3];

                hour[0] = GPS_Buff[0];
                hour[1] = GPS_Buff[1];

                minute[0] = GPS_Buff[2];
                minute[1] = GPS_Buff[3];

                second[0] = GPS_Buff[4];
                second[1] = GPS_Buff[5];

                day[0] = GPS_Buff[6];
                day[1] = GPS_Buff[7];

                month[0] = GPS_Buff[8];
                month[1] = GPS_Buff[9];

                year[0] = GPS_Buff[10];
                year[1] = GPS_Buff[11];

                /* Convert the Date and Time to values */
                t.RTC_Year = atoi(year);
                t.RTC_Mon = atoi(month);
                t.RTC_Mday = atoi(day);
                t.RTC_Hour = atoi(hour);
                t.RTC_Min = atoi(minute);
                t.RTC_Sec = atoi(second);


                /* Accurate Year has not been found by the GPS Yet */
                if ( t.RTC_Year >= 18 )  /* Year is less than 2018 how can that even be possible */
                {
                    /*Set the GPS lock,  TODO Does this only needs to be entered once from powerup?? */
                    b_gps_locked = true;

                    /* Re-synch the Solar Clock */
                    cal_set_calc_solar_clock(true);

                    //----------------------------------------------
                    // KF-398 GPS Time string is UTC so disable DST
                    rtc_procx_SetDstIsApplicable(false);
                    t.RTC_Year = t.RTC_Year + 2000;
                    /* Set the Date and Time */
                    rtc_procx_SetTime(t);
                    rtc_procx_SetDstIsApplicable(true);
                    //----------------------------------------------

                    /* Start the CAL state machine again with the current date and time */
                    cal_reset_cal_sm();

                    Previous_RTC_Min = (uint8_t)t.RTC_Min;

                    return;
                }
                else
                {
                    debug_printf(CALEN, "Incorrect Date\r\n");
                }


            }
        }
        else
        {
            debug_printf(CALEN, "NO LOCK\r\n");
        }
    }
    else
    {
        /* Do we have the Long and Lat Yet?  */
        if (b_gps_data_locked == false)
        {
            if (gps_parsed_locked_data() )
            {
                /* Lat and Long has been found */
                b_gps_data_locked = true;

                /* Re-synch the Solar Clock */
                cal_set_calc_solar_clock(true);

                /* Start the CAL state machine again with the New Solar Calcs */
                cal_reset_cal_sm();

                return;
            }
        }

        /* Only check RTCC SP points if the RTCC Minute has moved on */
        if( ( Previous_RTC_Min != t.RTC_Min ) || ( InitINProgress == true ))
        {
            Previous_RTC_Min = (uint8_t)t.RTC_Min;

            //------------------------------------------------------------
            // KF-367 Only check day number and exception nights at
            // boot-up time sync and noon
            //------------------------------------------------------------
            if ( (true == b_gps_locked) && ( InitINProgress == true ) )
            {
                // KF-396 Force reset of state machine at noon
                PrevDayNumber = (uint16_t)-1;
                //--------------------------------------------
                cal_set_state(CAL_MAIN_SM_CALC_DAY_NUMBER);
                return;
            }
            if( (12 == t.RTC_Hour) && (0 == t.RTC_Min))
            {
                // KF-396 Force reset of state machine at noon
                PrevDayNumber = (uint16_t)-1;
                //--------------------------------------------
                cal_set_state(CAL_MAIN_SM_CALC_DAY_NUMBER);

                // KF-274 Force end of current profile if still active
                if( true == b_profile_is_on )
                {
                    terminateProfileWithLampOn();
                    cal_set_luminr_op();
                }
                //--------------------------------------------
                return;
            }
            //------------------------------------------------------------

            if ( t.RTC_Hour > 11 && t.RTC_Min > 00 )
            {
                TimeMinsfromNoon = ( ((uint16_t)t.RTC_Hour) - 12) * 60;
                TimeMinsfromNoon = TimeMinsfromNoon + (uint16_t)t.RTC_Min;
            }
            else if ( t.RTC_Hour < 12 && t.RTC_Min < 59 )
            {
                TimeMinsfromNoon = 720 + ( ((uint16_t)t.RTC_Hour) * 60 );  /* 720 = 12 hours */
                TimeMinsfromNoon = TimeMinsfromNoon + (uint16_t)t.RTC_Min;
            }

            cal_set_state(CAL_MAIN_SM_GET_SP_ON_OFF_MODE);
            return;
        }
    }

    /* If No GPS Found or Current Profile is 0 the check PhotoCell State */
    TriggerSource = CalendarInfo.Profile.Switching_Points[CalendarInfo.Current_ON_OFF_Switching_Point].Trigger.Source_type;
    /* The mode dictates the next GET State */
    /* AND wait for next Profile to run. (Wait for 12pm Noon )*/
    if (((TriggerSource == CAL_TRIG_LUX ) && (boolWaitForNextSP_ON_OFF_Profile == false))|| (0 == CalendarInfo.Current_Profile_ID ))
    {
        cal_set_state(CAL_MAIN_SM_GET_PHOTO_STATE);
    }
    else
    {
        cal_set_state(CAL_MAIN_SM_IDLE);
    }
}

/*******************************************************************************
 * @brief cal_bin_get_exception_table
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_main_sm_calc_day_number(void)
{

    debug_printf(CALEN, "TIME %02d:%02d:%02d %02d-%02d-%d %d %i\r\n",
            t.RTC_Hour,
            t.RTC_Min,
            t.RTC_Sec,
            t.RTC_Mday,
            t.RTC_Mon,
            t.RTC_Year,
            t.RTC_Wday,
            t.RTC_Yday
    );

    //--------------------------------------------------
    // KF-400 WEEK DAY NOW CALCULATED BY RTC LOGIC ONLY
    DayNumber = (uint16_t)t.RTC_Wday;
    //--------------------------------------------------

    /* Day number has changed reset any waiting flags for next day booleans */
    if ((DayNumber != PrevDayNumber ) && ( InitINProgress == false))
    {
        PrevDayNumber = DayNumber;

        /* Synch Triggers with Photo Cell Trigger */
        Curr_Photo_State = PHOTO_LIGHT_LATCHED;
        Prev_Photo_State = PHOTO_LIGHT_LATCHED;
        Crossing_Delay = APP_ADC_LOW_LIGHT_CROSSING_DELAY;
        Crossing_Latch_Delay = APP_ADC_LATCH_LIGHT_CROSSING_DELAY;

        /* TODO Add this to a function call as a complete group */
        boolWaitForNextProfile = false;
        boolWaitForNextSP_ON_OFF_Profile =  false;
        CalendarInfo.Current_ON_OFF_Switching_Point = 0;
        CalendarInfo.Current_Switching_Point = 0x02;

        //-------------------------------------------------------
        // About to reset both state machine to fetch weekly
        // and exception table so clear Current_Profile_ID
        CalendarInfo.Current_Profile_ID = 0;
        //-------------------------------------------------------

        /* Re-calculate the Solar Clock for this New day */
        cal_set_calc_solar_clock(true);

        /* Reset the Calendar State Machine to Init and start a refreshed get of the CAL file*/
        cal_set_state(CAL_MAIN_SM_INIT);

        cal_set_bin_state(CAL_BIN_OPEN_FILE);


    }
    else
    {

        if ( InitINProgress == true )
        {
            InitINProgress = false;
            PrevDayNumber = DayNumber;
        }
        /* Check if the current Day is an Exception Day */
        cal_set_state(CAl_MAIN_SM_CHECK_EXCEPTION_TABLE);
    }

}

/*******************************************************************************
 * @brief cal_main_sm_check_exception_table
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_main_sm_check_exception_table( void )
{
    /* Check Weekly Table */
    cal_set_state(CAL_MAIN_SM_CHECK_WEEKLY_TABLE);

    /* Find exception Day */
    for( int n = 0; n < APP_NO_OF_EXCPT_CALENDAR; n++)
    {
        /* End of Exception List */
        if ( CalendarInfo.ExcptSchedule[n].Exception_day == 0)
        {
            // Exit for loop
            break;
        }

        /* Exception Night found */
        if ((CalendarInfo.ExcptSchedule[n].Exception_month == (uint8_t)t.RTC_Mon) &&
                ( CalendarInfo.ExcptSchedule[n].Exception_day == (uint8_t)t.RTC_Mday ))
        {
            /* Get the Profile ID for the current WeekDay Number */
            CalendarInfo.Current_Profile_ID = CalendarInfo.ExcptSchedule[n].Profile_ID;

            cal_bin_get_current_profile_ID();

            cal_set_state(CAL_MAIN_SM_GET_SP_FROM_PROFILE_ID);
            // Exit for loop
            break;
        }
    }
}

/*******************************************************************************
 * @brief cal_main_sm_check_weekly_table
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_main_sm_check_weekly_table(void)
{
    uint16_t WeekNightNumber = 0;

    WeekNightNumber = (uint16_t)t.RTC_Wday;

    /* Are we before/after 12pm. Before 12pm means previous day */

    /*   RTC Day                Week night Number
     *   0      Sun/Mon =   1
     *   1      Mon/Tue =   2
     *   2      Tue/Wed =   3
     *   3      Wed/Thu =   4
     *   4      Thu/Fri =   5
     *   5      Fri/Sat =   6
     *   6      Sat/Sun =   7 */

    /* Using above Weeknight table calculate the current Week Night */
    if (t.RTC_Hour > 11)
    {
            WeekNightNumber++;
    }
    else
    {
        if ( WeekNightNumber == 0  )
        {
            WeekNightNumber = 7;
        }
    }

    debug_printf(CALEN, "WeekNightNumber = %d \r\n", WeekNightNumber);

    /* 0 = Sunday , 6 = Saturday */

    /* has GPS been found or has RTTC been set */
    if ( b_gps_locked ) //|| (RTCC_Set_via_Gateway)
    {

        /* Get the Profile ID for the current WeekDay Number */
        CalendarInfo.Current_Profile_ID =  CalendarInfo.WklySchedule[WeekNightNumber].Profile_ID;

        // CalendarInfo.Profile_ID_Length =  (( CalendarInfo.Profile.Profile_len) / 2 );

        // debug_printf(CAL, "CalendarInfo.Current_Profile_ID = %d \r\n", CalendarInfo.Current_Profile_ID);
        /* Force Cal Init to TRUE needed use the Current Profile */
        b_cal_is_initialised = true;

        debug_printf(CALEN, "CalendarInfo.WklySchedule[WeekNightNumber].Profile_ID = %d \r\n", CalendarInfo.WklySchedule[WeekNightNumber].Profile_ID);
    }
    else
    {
        /* Use default Profile ID until GPS lock or RTCC Set */
        CalendarInfo.Current_Profile_ID = 0;

    //  CalendarInfo.Profile_ID_Length =  (( CalendarInfo.Profile.Profile_len) / 2 );
    }

    /* TODO May no longer be needed this is because the CAL is refreshed everyday at Noon */
    cal_bin_get_current_profile_ID();

    cal_set_state(CAL_MAIN_SM_GET_SP_FROM_PROFILE_ID);
}

/*******************************************************************************
 * @brief cal_main_sm_get_sp_from_profile_id
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_main_sm_get_sp_from_profile_id(void)
{

    if ( b_cal_is_initialised == false )
    {
         b_cal_is_initialised = true;

        if ( CalendarInfo.Profile.Switching_Points[0].Trigger.Source_type == CAL_TRIG_RTCC )
        {
            cal_set_state(CAL_MAIN_SM_GET_INIT_RTCC_STATE);
        }
        else
        {
            cal_set_state(CAL_MAIN_SM_GET_INIT_PHOTO_STATE);
        }
    }
    else
    {
        /* If the current Trigger Source is the Solar Clock Ensure the GPS Co-Ords have been Captured NWK or GPS */
        if(( CalendarInfo.Profile.Switching_Points[CalendarInfo.Current_ON_OFF_Switching_Point].Trigger.Source_type == CAL_TRIG_SOLAR ) && ( b_gps_data_locked == false))
        {
            /* Use default Profile ID until GPS lock or RTCC Set */
            CalendarInfo.Current_Profile_ID = 0;

            cal_bin_get_current_profile_ID();
        }
        /* Get the EVENTS from the Profile ID */
        cal_set_state(CAL_MAIN_SM_GET_SP_ON_OFF_MODE);
    }

    if(0 == CalendarInfo.Current_Profile_ID)
    {
        set_sysmon_fault(NO_VALID_CALENDAR);
    }else
    {
        clear_sysmon_fault(NO_VALID_CALENDAR);
    }
}

/*******************************************************************************
 * @brief cal_main_sm_get_init_photo_state
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_main_sm_get_init_photo_state( void)
{
    /* Has the Photo Lux Triggered */
    uint16_t uThreshold = 0;

    Photo_States_t Get_Photo_State = PHOTO_NOT_READY;

    /* First Check ZERO 0 */
    uThreshold = CalendarInfo.Profile.Switching_Points[CalendarInfo.Current_ON_OFF_Switching_Point].Trigger.Threshold;

    /* Has the Photo Lux Triggered */

    /* Are we Dark, Light or Not ready */
    Get_Photo_State =  cal_photo_check_threshold_with_params(uThreshold);

    debug_printf(CALEN, "CAL_MAIN_SM_GET_INIT_PHOTO_STATE = uThreshold %d\r\n",uThreshold);

    Prev_Photo_State = Get_Photo_State;

    if ( Get_Photo_State == PHOTO_NOT_READY )
    {
        /* Exit For */
        debug_printf(CALEN, "CAL INIT PHOTO NOT READY\r\n");
        /* Will have to come back again */
    }
    else if(( Get_Photo_State == PHOTO_DARK ) || (Get_Photo_State == PHOTO_DARK_LATCHED))
    {
        debug_printf(CALEN, "CAL INIT PHOTO DARK\r\n");

//      if ( CalendarInfo.Profile.Switching_Points[1].Trigger.Source_type == CAL_TRIG_RTCC )
//      {
//          CalendarInfo.Current_ON_OFF_Switching_Point = 1;
//          cal_set_state(CAL_MAIN_SM_GET_INIT_RTCC_STATE);
//      }
//      else
        {
            CalendarInfo.Current_ON_OFF_Switching_Point = 1;
            cal_set_state(CAL_MAIN_SM_GET_SP_ON_OFF_MODE);

            Lamp_Output_level = 100;  /* Force 100% at init for Now */
            b_profile_is_on = true;
            cal_set_luminr_op();
        }

    }
    else if ((Get_Photo_State == PHOTO_LIGHT ) || (Get_Photo_State == PHOTO_LIGHT_LATCHED))
    {
        // PHOTO_LIGHT
        debug_printf(CALEN, "CAL INIT PHOTO LIGHT\r\n");

//      if ( CalendarInfo.Profile.Switching_Points[1].Trigger.Source_type == CAL_TRIG_RTCC )
//      {
//          CalendarInfo.Current_ON_OFF_Switching_Point = 1;
//          cal_set_state(CAL_MAIN_SM_GET_INIT_RTCC_STATE);
//      }
//      else
        {
            CalendarInfo.Current_ON_OFF_Switching_Point = 0;
            cal_set_state(CAL_MAIN_SM_GET_SP_ON_OFF_MODE);

            terminateProfileWithLampOff();
            cal_set_luminr_op();
        }
    }
}

/*******************************************************************************
 * @brief cal_main_sm_get_init_rtcc_state
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_main_sm_get_init_rtcc_state(void)
{
    /* Has the RTCC offset/Time Triggered */
    uint16_t TimeMinsfromNoon = 0;

    if ( t.RTC_Hour > 11 )
    {
        // If it's LIGHT we are waiting for the SP OFF  (SP Index 0 )
        TimeMinsfromNoon = ( ((uint16_t)t.RTC_Hour) - 12) * 60;
        TimeMinsfromNoon = TimeMinsfromNoon + (uint16_t)t.RTC_Min;
    }
    else if ( t.RTC_Hour < 12 )
    {
        TimeMinsfromNoon = 720 + ( ((uint16_t)t.RTC_Hour) * 60 );  /* 720 = 12 hours */
        TimeMinsfromNoon = TimeMinsfromNoon + (uint16_t)t.RTC_Min;
    }

    cal_set_state(CAL_MAIN_SM_GET_SP_ON_OFF_MODE);

    if (TimeMinsfromNoon >= CalendarInfo.Profile.Switching_Points[CalendarInfo.Current_ON_OFF_Switching_Point].Trigger.Threshold )
    {
        // Switch LAMP ON
        //debugprint(CAL, "CAL RTCC LAMP OFF 0\r\n");
        //  CalendarInfo.Current_ON_OFF_Switching_Point = 0;
        //  boolWaitForNextSP_ON_OFF_Profile = true;

        debug_printf(CALEN, "Mins from Noon\tDayNumber \tSP ON OFF \tProfile ID \t SP \r\n");
        debug_printf(CALEN, "%04d    \t  %02d      \t  %02d      \t  %02d    \t  %02d  \r\n",TimeMinsfromNoon,
                                                                        DayNumber,
                                                                        CalendarInfo.Current_ON_OFF_Switching_Point,
                                                                        CalendarInfo.Current_Profile_ID,
                                                                        CalendarInfo.Current_Switching_Point
                                                                        );

        if ( CalendarInfo.Current_ON_OFF_Switching_Point == 1 )
        {
            debug_printf(CALEN, "CAL RTCC LAMP OFF 0\r\n");
            CalendarInfo.Current_ON_OFF_Switching_Point = 0;
            boolWaitForNextSP_ON_OFF_Profile = true;
            terminateProfileWithLampOff();

            /* Synch RTC TRigger with Photo Cell Trigger */
            Curr_Photo_State = PHOTO_LIGHT_LATCHED;
            Prev_Photo_State = PHOTO_LIGHT_LATCHED;
            Crossing_Delay = APP_ADC_LOW_LIGHT_CROSSING_DELAY;
            Crossing_Latch_Delay = APP_ADC_LATCH_LIGHT_CROSSING_DELAY;
        }
        else
        {
            debug_printf(CALEN, "CAL RTCC LAMP ON 0\r\n");
            CalendarInfo.Current_ON_OFF_Switching_Point = 1;
            b_profile_is_on = true;
            Lamp_Output_level = 100;  /* Force 100% at init for Now */

            /* Synch RTC TRigger with Photo Cell Trigger */
            Curr_Photo_State = PHOTO_DARK_LATCHED;
            Prev_Photo_State = PHOTO_DARK_LATCHED;
            Crossing_Latch_Delay = 0;
            Crossing_Delay = 0;
        }

        cal_set_luminr_op();
    }
    else
    {
        if ( CalendarInfo.Current_ON_OFF_Switching_Point == 1 )
        {
            debug_printf(CALEN, "CAL RTCC LAMP ON 1\r\n");
            b_profile_is_on = true;
            Lamp_Output_level = 100;  /* Force 100% at init for Now */
        }
        else
        {
            debug_printf(CALEN, "CAL RTCC LAMP OFF 1\r\n");
            terminateProfileWithLampOff();
        }

        cal_set_luminr_op();
    }
}


/*******************************************************************************
 * @brief cal_main_sm_get_sp_on_off_mode
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_main_sm_get_sp_on_off_mode( void )
{
    CalTrigger_States_t TriggerSource = 0;
    /* Check current Time with Schedule to calculate the current Event Trigger */

    /* Are we waiting for next Profile Day */
    if( boolWaitForNextSP_ON_OFF_Profile == false)
    {
        /* Solar/Lux /RTCC */
        // If it's LIGHT we are waiting for the SP ON  (SP Index 0 )
        // If it's DARK we are waiting for the SP OFF  (SP Index 1 )
        TriggerSource = CalendarInfo.Profile.Switching_Points[CalendarInfo.Current_ON_OFF_Switching_Point].Trigger.Source_type;

        /* The mode dictates the next GET State */
        if ((TriggerSource == CAL_TRIG_SOLAR ) && (b_gps_data_locked != false ))
        {
            /* Handle CAL_TRIG_SOLAR here*/
            debug_printf(CALEN, "CAL_TRIG_SOLAR\r\n");

            cal_set_state(CAL_MAIN_SM_GET_SOLAR_STATE);
        }
        else if (TriggerSource == CAL_TRIG_RTCC)
        {

            debug_printf(CALEN, "CAL_TRIG_RTCC\r\n");

            cal_set_state(CAL_MAIN_SM_GET_TIME_STATE);
        }
        else
        {
            /* Default to Lux */
            debug_printf(CALEN, "CAL_TRIG_PHOTO\r\n");

            cal_set_state(CAL_MAIN_SM_GET_PHOTO_STATE);

        }
    }
    else
    {
        /* Are we waiting for next Profile Day */
        if( boolWaitForNextProfile == false)
        {
            cal_set_state(CAL_MAIN_SM_GET_NEXT_SP);
        }
        else
        {
            cal_set_state(CAL_MAIN_SM_IDLE);
        }
    }

}


/*******************************************************************************
 * @brief cal_main_sm_get_photo_state
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_main_sm_get_photo_state(void )
{
    /* Has the Photo Lux Triggered */
    uint16_t uThreshold = 0;

    Photo_States_t Get_Photo_State = PHOTO_NOT_READY;

    /* If we are waiting for a LUX switch OFF point ( Switching Point = 1 ) AND it is before 12am AND NOT running the default Profile */
    /*  12 - 23 is Noon to Midnight */
    /*  00 - 12 is Midnight to Noon */
    if (( CalendarInfo.Current_ON_OFF_Switching_Point > 0 ) && (t.RTC_Hour > 12 ) && ( 0 != CalendarInfo.Current_Profile_ID ))
    {
        /* Return out of here and check the Switching Points */
        cal_set_state(CAL_MAIN_SM_GET_NEXT_SP);
        return;
    }


    //---------------------------------------------------------------------------------------------------------
    // KF-602 Block double-switching when executing default profile as long as node has time information.
    // Evening
    if (( CalendarInfo.Current_ON_OFF_Switching_Point > 0 ) && (t.RTC_Hour > 12 ) && ( t.RTC_Year > 1970 ) )
    {
        cal_set_state(CAL_MAIN_SM_GET_NEXT_SP);
        return;
    }
    // Morning
    if( (b_profile_is_on == false) && (boolWaitForNextSP_ON_OFF_Profile == true))
    {
        cal_set_state(CAL_MAIN_SM_GET_NEXT_SP);
        return;
    }
    //---------------------------------------------------------------------------------------------------------

    uThreshold = CalendarInfo.Profile.Switching_Points[CalendarInfo.Current_ON_OFF_Switching_Point].Trigger.Threshold;
    /* Are we Dark, Light or Not ready */
    Get_Photo_State = cal_photo_check_threshold_with_params(uThreshold);

//  debugprint(CAL, "CAL_MAIN_SM_GET_PHOTO_STATE = uThreshold %d\r\n",uThreshold);

    /* Only need to check when Photo read out is NOT Ready */
    if(Get_Photo_State == PHOTO_NOT_READY )
    {
        cal_set_state(CAL_MAIN_SM_GET_PHOTO_STATE);
    }
    else
    {
        cal_set_state(CAL_MAIN_SM_GET_NEXT_SP);
    }

    if ((Get_Photo_State == PHOTO_DARK ) || (Get_Photo_State == PHOTO_DARK_LATCHED))
    {
        debug_printf(CALEN, "CAL PHOTO_DARK\r\n");

    }
    else if ((Get_Photo_State == PHOTO_LIGHT ) || (Get_Photo_State == PHOTO_LIGHT_LATCHED))
    {
        // PHOTO_LIGHT
        debug_printf(CALEN, "CAL PHOTO_LIGHT\r\n");
    }

    if ( Get_Photo_State != Prev_Photo_State )
    {
        Prev_Photo_State = Get_Photo_State;

        if ((Get_Photo_State == PHOTO_DARK ) || (Get_Photo_State == PHOTO_DARK_LATCHED))
        {
            // Switch LAMP ON
            CalendarInfo.Current_ON_OFF_Switching_Point = 1;
            debug_printf(CALEN, "CAL PHOTO LAMP ON\r\n");

            /* If running Default Profile */
            if (CalendarInfo.Current_Profile_ID == 0)
            {
                Lamp_Output_level = CalendarInfo.Profile.Switching_Points[0].Lamp_Output;
            }

            b_profile_is_on = true;
            cal_set_luminr_op();
        }
        else if ((Get_Photo_State == PHOTO_LIGHT ) || (Get_Photo_State == PHOTO_LIGHT_LATCHED))
        {
            // PHOTO_LIGHT
            debug_printf(CALEN, "CAL PHOTO LAMP OFF\r\n");
            CalendarInfo.Current_ON_OFF_Switching_Point = 0;

            /* If we are between Midnight(00) and Noon(12) then wait for the next Profile */
            if(( t.RTC_Hour < 12 ) && (true == b_gps_locked))
            {
                boolWaitForNextSP_ON_OFF_Profile = true;
            }

            terminateProfileWithLampOff();


            cal_set_luminr_op();

        }
    }
}
/*******************************************************************************
 * @brief cal_main_sm_get_time_state
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_main_sm_get_solar_state(void)
{
    uint32_t TimeNow_s;
    int16_t Solar_Threshold = 0;

    /* Has the RTCC offset/Time Triggered */
    int32_t TimeSecondsfromSolar = 0; /* Use Time in Seconds because thats what the RTCC Get Counter returns */

    TimeNow_s = rtc_procx_GetCounter();
    /* get the current Solar Clock Data Set */
    get_solar_dataset(&solar_clock_dataset);

    Solar_Threshold = (CalendarInfo.Profile.Switching_Points[CalendarInfo.Current_ON_OFF_Switching_Point].Trigger.Threshold);

    /* Calculate if we are +ve or -Ve by checking against 720 (12 Hours in Mins) */
    /* 0 to 720 is Positive */
    /* 721 to 1441 is Negative -720 to 0 */
    if (Solar_Threshold > 720)
    {
        /*Take away the +Ve 12hrs(720 mins) & convert to -Ve */
        Solar_Threshold -= 1441;
    }

    TimeSecondsfromSolar = (int32_t)(Solar_Threshold) * 60; /* Convert Minute to seconds by Multiplying by 60  */

    /* Switch LAMP ON at Sun Set when Seconds >= the Current Time */
    if (( CalendarInfo.Current_ON_OFF_Switching_Point == 0) && ( TimeNow_s >= (solar_clock_dataset.solar_sun_set + TimeSecondsfromSolar)))
    {
        // TODO group inot a single function
        CalendarInfo.Current_ON_OFF_Switching_Point = 1;
        debug_printf(CALEN, "CAL SOLAR LAMP ON\r\n");
        b_profile_is_on = true;

        /* Synch RTC TRigger with Photo Cell Trigger */
        // TODO group inot a single function
        Curr_Photo_State = PHOTO_DARK_LATCHED;
        Prev_Photo_State = PHOTO_DARK_LATCHED;
        Crossing_Latch_Delay = 0;
        Crossing_Delay = 0;

        cal_set_luminr_op();
    }
    /* Switch LAMP OFF at Sun Rise when Seconds >= the Current Time */
    else if (( CalendarInfo.Current_ON_OFF_Switching_Point == 1)  && ( TimeNow_s >= (solar_clock_dataset.solar_sun_rise + TimeSecondsfromSolar)))
    {
        // TODO group into a single function
        debug_printf(CALEN,"CAL SOLAR LAMP OFF\r\n");
        CalendarInfo.Current_ON_OFF_Switching_Point = 0;
        boolWaitForNextSP_ON_OFF_Profile = true;
        terminateProfileWithLampOff();

        /* Synch RTC TRigger with Photo Cell Trigger */
        // TODO group into a single function
        Curr_Photo_State = PHOTO_LIGHT_LATCHED;
        Prev_Photo_State = PHOTO_LIGHT_LATCHED;
        Crossing_Delay = APP_ADC_LOW_LIGHT_CROSSING_DELAY;
        Crossing_Latch_Delay = APP_ADC_LATCH_LIGHT_CROSSING_DELAY;

        cal_set_luminr_op();
    }

    /* Are we waiting for next Profile Day */
    if( boolWaitForNextProfile == false)
    {
        cal_set_state(CAL_MAIN_SM_GET_NEXT_SP);
    }
    else
    {
        cal_set_state(CAL_MAIN_SM_IDLE);
    }


}
/*******************************************************************************
 * @brief cal_main_sm_get_time_state
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_main_sm_get_time_state(void)
{
    /* Has the RTCC offset/Time Triggered */
    uint16_t TimeMinsfromNoon = 0;

    if ( t.RTC_Hour > 11 )
    {
        // If it's LIGHT we are waiting for the SP OFF  (SP Index 0 )
        TimeMinsfromNoon =  ( ((uint16_t)t.RTC_Hour) - 12) * 60;
        TimeMinsfromNoon = TimeMinsfromNoon + (uint16_t)t.RTC_Min;
    }
    else if ( t.RTC_Hour < 12 )
    {
        TimeMinsfromNoon = 720 + ( ((uint16_t)t.RTC_Hour) * 60 );  /* 720 = 12 hours */
        TimeMinsfromNoon = TimeMinsfromNoon + (uint16_t)t.RTC_Min;

    }

    if (TimeMinsfromNoon >= CalendarInfo.Profile.Switching_Points[CalendarInfo.Current_ON_OFF_Switching_Point].Trigger.Threshold )
    {
        // Switch LAMP ON
        if ( CalendarInfo.Current_ON_OFF_Switching_Point == 0)
        {
            // TODO group into a single function
            CalendarInfo.Current_ON_OFF_Switching_Point = 1;
            debug_printf(CALEN, "CAL RTCC LAMP ON\r\n");
            b_profile_is_on = true;

            /* Synch RTC TRigger with Photo Cell Trigger */
            Curr_Photo_State = PHOTO_DARK_LATCHED;
            Prev_Photo_State = PHOTO_DARK_LATCHED;
            Crossing_Latch_Delay = 0;
            Crossing_Delay = 0;

        }
        else
        {
            // TODO group into a single function
            debug_printf(CALEN,"CAL RTCC LAMP OFF\r\n");
            CalendarInfo.Current_ON_OFF_Switching_Point = 0;
            boolWaitForNextSP_ON_OFF_Profile = true;
            terminateProfileWithLampOff();

            /* Synch RTC TRigger with Photo Cell Trigger */
            Curr_Photo_State = PHOTO_LIGHT_LATCHED;
            Prev_Photo_State = PHOTO_LIGHT_LATCHED;
            Crossing_Delay = APP_ADC_LOW_LIGHT_CROSSING_DELAY;
            Crossing_Latch_Delay = APP_ADC_LATCH_LIGHT_CROSSING_DELAY;
        }

        cal_set_luminr_op();
    }

    /* Are we waiting for next Profile Day */
    if( boolWaitForNextProfile == false)
    {
        cal_set_state(CAL_MAIN_SM_GET_NEXT_SP);
    }
    else
    {
        cal_set_state(CAL_MAIN_SM_IDLE);
    }
}

/*******************************************************************************
 * @brief cal_main_sm_get_next_sp
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_main_sm_get_next_sp(void)
{
    uint16_t TimeMinsfromNoon = 0;
    uint16_t SP_Threshold = 0;
    uint16_t Next_SP_Threshold = 0;


    /* SP 2 - 15 RTCC */

    /* Check current Time with Schedule to calculate the next SP Trigger */
    if ( t.RTC_Hour > 11 )  /* Are we AFTER 12:00 */
    {
        TimeMinsfromNoon = ( ((uint16_t)t.RTC_Hour) - 12) * 60;
        TimeMinsfromNoon = TimeMinsfromNoon + (uint16_t)t.RTC_Min;

    }
    else if ( t.RTC_Hour < 12 )  /* Are we BEFORE 12:00 */
    {
        TimeMinsfromNoon = 720 + ( ((uint16_t)t.RTC_Hour) * 60 );  /* 720 = 12 hours */
        TimeMinsfromNoon = TimeMinsfromNoon + (uint16_t)t.RTC_Min;
    }

    /* Compare with Threshold */

    SP_Threshold = CalendarInfo.Profile.Switching_Points[CalendarInfo.Current_Switching_Point].Trigger.Threshold;

    if ((TimeMinsfromNoon >= SP_Threshold )  && ( boolWaitForNextProfile == false) && (CalendarInfo.Current_Profile_ID != 0))
    {
        // LAMP OP Level
        debug_printf(CALEN, "CALENDAR Lamp Output %d %d %d\r\n", CalendarInfo.Profile.Switching_Points[CalendarInfo.Current_Switching_Point].Lamp_Output, CalendarInfo.Current_Profile_ID, CalendarInfo.Current_Switching_Point);
        Lamp_Output_level = CalendarInfo.Profile.Switching_Points[CalendarInfo.Current_Switching_Point].Lamp_Output;
        CalendarInfo.Current_Switching_Point++;
        debug_printf(CALEN, "Current_Switching_Point %02d %02d\r\n",CalendarInfo.Current_Switching_Point,CalendarInfo.Profile_ID_Length);

        /* Have we reached the end of the SP List for the current Night Profile or the End of the Max Number of SPs */
        if (( CalendarInfo.Current_Switching_Point > (CalendarInfo.Profile_ID_Length))   || ( CalendarInfo.Current_Switching_Point > (APP_NO_OF_SPS - 1)))
        {
            // The first SP Index is 2 which dictates the profile of the Lamp Output
            CalendarInfo.Current_Switching_Point = 0x02;
            // No more SPs for this day, wait till next Calendar day
            boolWaitForNextProfile = true;
        }

        /* Get the Next SP threshold before sending to the Luminar */
        Next_SP_Threshold = CalendarInfo.Profile.Switching_Points[CalendarInfo.Current_Switching_Point].Trigger.Threshold;

        /* Only set the Luminar if this is the last SP OR if the Next SP is not going to be hit */
        if(( boolWaitForNextProfile == true ) || ( TimeMinsfromNoon < Next_SP_Threshold ))
        {
            cal_set_luminr_op();
        }

    }
    else
    {
        cal_set_state(CAL_MAIN_SM_IDLE);
    }
}

/*******************************************************************************
 * @brief cal_reset_cal_sm
 * @param[in] None
 * @return None
 *******************************************************************************/
void cal_reset_cal_sm(void)
{
    /* Reset the Calendar State Machine to Init and start a refreshed get of the CAL file*/
    cal_set_state(CAL_MAIN_SM_INIT);

    cal_set_bin_state(CAL_BIN_OPEN_FILE);

    boolWaitForNextProfile = false;
    boolWaitForNextSP_ON_OFF_Profile =  false;
    CalendarInfo.Current_ON_OFF_Switching_Point = 0;
    CalendarInfo.Current_Switching_Point = 0x02;

    PrevDayNumber = 0;
}



/*******************************************************************************
 * @brief cal_set_luminr_op
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_set_luminr_op( void )
{
    // store dimming level for dim change during profile on
    // gets overwritten if required
    uint8_t snd_Lamp_Output_level = Lamp_Output_level;
    static uint8_t prevLampOutput = (uint8_t)-1;

    QuLuminrOpMsg lamp_output_update_msg;

    // Work out of this is a profile switch-on / off point
    if( b_profile_is_on != b_profile_was_on ){
        // latch into new state
        b_profile_was_on = b_profile_is_on;
        debug_printf(CALEN,"Profile is now %s.\r\n", ((b_profile_is_on)?"on":"off"));
        // capture billing info
        cal_billing_process(b_profile_is_on);
        // if profile is active send dimming level, else send 0

        // KF-274 Do not force to 0 at end of profile (commented out line) below:
        // To allow for lamp so stay on over noon - school crosser
        // snd_Lamp_Output_level = (b_profile_is_on) ? Lamp_Output_level : 0;

    }else if( b_profile_is_on == false){
        // profile currently still disabled (no change) so do not execute dimming command
        // dimming command remains stored in Lamp_Output_level
        debug_printf(CALEN,"Dim to %d%% ignored as profile not currently running.\r\n", Lamp_Output_level);
        return;
    }

    //------------------------------------------
    // KF 397 Avoid repeat of Lamp Output Command
    if( prevLampOutput == snd_Lamp_Output_level)
    {
        return;
    }
    else
    {
        prevLampOutput = snd_Lamp_Output_level;
    }
    //------------------------------------------

    debug_printf(CALEN,"Dim to %d\r\n", snd_Lamp_Output_level);

    // Do not attempt to access queue before the have been created
    if(xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED) return;

    // Send updated LO level to vTaskLuminrOp
    lamp_output_update_msg.source_task_id = TASK_CAL;
    lamp_output_update_msg.lamp_output_level = snd_Lamp_Output_level & 0x7F;
    xQueueSend(xQuLuminrOp, &lamp_output_update_msg, 0);

    return;
}


/*******************************************************************************
 * @brief Process billing info at switch on and switch off points
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_billing_process( bool b_profile_switch_on )
{

    if( b_profile_switch_on == true){
        // If time has not been synchronised - store the ticks at switch on:
        if ( (false == cal_get_Time_synch()) && (Ticks_Since_SwitchOn == 0) ) {
            Ticks_Since_SwitchOn = get_systick_sec();
        }

        billing_dataset.switch_on_tmstp = rtc_procx_GetCounter();

        debug_printf( CALEN,
                    "Billing switch on @ %lu\r\n",
                    billing_dataset.switch_on_tmstp);
        // force switch off to 0 as safety
        billing_dataset.switch_off_tmstp = 0;

    }else{ // else profile switch-off
        billing_dataset.switch_off_tmstp = rtc_procx_GetCounter();

        debug_printf( CALEN,
                    "Billing switch off @ %lu\r\n",
                    billing_dataset.switch_off_tmstp);

        cal_send_billing(billing_dataset);

        // Set to 0 after it has been used by billing:
        Ticks_Since_SwitchOn = 0;
    }

    return;
}

/*******************************************************************************
 * @brief Report billing information to the cloud
 * @param[in] BillingDataset
 * @return bool
 *******************************************************************************/
static bool cal_send_billing(BillingDataset billing_data){


    // Sanity check: to be valid, the "switch on timestamp" can't be older than 01/01/2019)
    // On timestamp is invalid:
    if (CAL_BILLING_VALID_TIMESTAMP > billing_dataset.switch_on_tmstp)
    {
        // If the off timestamp is valid - sanitise the ON timestamp:
        if (CAL_BILLING_VALID_TIMESTAMP < billing_dataset.switch_off_tmstp)
        {
            //off timestamp - the number of seconds the profile has been running for.
            // Update the ON timestamp using formula: t_on = (t_off - (tick_sec - tick_secs_at_sw_on)):
            billing_dataset.switch_on_tmstp = (billing_dataset.switch_off_tmstp - ((get_systick_sec()) - Ticks_Since_SwitchOn) );
        }
        else
        {
            // return false;
        }
    }

    billing_dataset.calendarFileRef = CalendarInfo.File_Reference;
    billing_dataset.calendarVersion = CalendarInfo.Version;
    billing_dataset.calendarProfileId = CalendarInfo.Current_Profile_ID;

    compileUplink_Daily(billing_dataset);  

    return true;
}




/*******************************************************************************
 * @brief cal_calc_solar_clock
 * @param[in] None
 * @return None
 *******************************************************************************/
bool cal_get_calc_solar_clock( void )
{
    return recalc_solar_clock;
}

/*******************************************************************************
 * @brief cal_calc_solar_clock
 * @param[in] None
 * @return None
 *******************************************************************************/
void cal_set_calc_solar_clock( bool Value )
{
    recalc_solar_clock = Value;
}

/*******************************************************************************
 * @brief cal_set_GPS_Data_synch
 * @param[in] BOOL Set Flag
 * @return None
 *******************************************************************************/
void cal_set_GPS_Data_synch( bool b_gps_data_rcvd )
{
    b_gps_data_locked = b_gps_data_rcvd;
}


/*******************************************************************************
 * @brief cal_set_Time_synch
 * @param[in] BOOL Set Flag
 * @return None
 *******************************************************************************/
void cal_set_Time_synch( bool b_gps_time_rcvd )
{
    b_gps_locked = b_gps_time_rcvd;
}

/*******************************************************************************
 * @brief cal_get_Time_synch
 * @param[in] void
 * @return bool: b_gps_locked flag
 *******************************************************************************/
bool cal_get_Time_synch( void )
{
    return b_gps_locked;
}


