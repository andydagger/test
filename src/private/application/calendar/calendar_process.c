/*
==========================================================================
 Name        : calendar_process.c
 Project     : pcore
 Path        : /pcore/src/private/calendar/calendar_process.c
 Author      : M Nakhuda
 Copyright   : Lucy Zodion Ltd
 Created     : 30 May 2018
 Description :
==========================================================================
*/
/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/
#include <time.h>
#include "chip.h"
#include "hw.h"
#include "stdbool.h"
#include "stdint.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "common.h"

#include "systick.h"
#include "debug.h"
#include "sysmon.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "FreeRTOSMacros.h"
#include "FreeRTOSCommonHooks.h"
#include "config_network.h"
#include "config_fatfs.h"
#include "config_patch.h"
#include "patch_rx.h"
#include "ff.h"
#include "led.h"
#include "file_sys.h"

#include "calendar.h"
#include "calendar_process.h"
#include "config_pinout.h"
#include "config_rtc_procx.h"

static FIL * p_cal_file;
static const char * cal_file_name = NULL;

//static uint32_t f_wr_bytes = 0; // not used by required to avoid passing NULL pointer
static UINT f_rd_bytes = 0; // not used by required to avoid passing NULL pointer

extern CalendarInfo_t CalendarInfo;

static uint16_t Calendar_Bin_Idx = 0;
static uint8_t Exception_Len = 0;
static uint8_t Profile_Cnt = 0;
static bool Profile_ID_found = false;


CalBinSM_t Cal_Bin_SM_State = CAL_BIN_OPEN_FILE;  //CAL_BIN_GET_VERSION;

static void cal_bin_open_file( void );
static void cal_bin_close_file( void );
static void cal_bin_get_version( void );
static void cal_bin_get_file_ref( void );
static void cal_bin_get_file_len ( void );
static void cal_bin_get_profile_len( void );
static void cal_bin_get_profile_ID( void );
static void cal_bin_get_profile_ID_len( void);
static void cal_bin_get_profile_ON(void);
static void cal_bin_get_profile_OFF(void);
static void cal_bin_get_profile_SP( void);
static void cal_bin_weekly_table( void );
static void cal_bin_get_exception_len( void);
static void cal_bin_get_exception_table( void );
static void report_error(uint32_t error_code);

static bool  cal_bin_get_profile_contents( uint16_t next_read_len);

// KF-268 40-byte buffer could not hold 50 exception = 100 bytes.
static uint8_t Calendar_Bin_array[110];
//
//static  uint8_t Calendar_Bin_array[] = {
// 0x00, 0x02,  // Cal Version (2 Bytes)
// 0x00, 0x00, 0x00, 0x02 ,   // Cal File Ref (4 Bytes)
//
// 0x7d , // Cal file length (7-bit Varint)
//
// 0x70 , // Profile length (7-bit Varint)
//
// 0x01 , // Profile 1
// 0x0e ,
// 0x81 , 0xf4 ,0x83 ,0xfc ,0x46 , 0x04, 0x4d , 0x88 , 0x5a , 0x10 , 0x69 , 0x0c , 0x77 , 0x6a ,
//
// 0x02 , // Profile 2
// 0x0e ,
// 0x81 , 0xf4 ,0x83 ,0xfc ,0x46 , 0x04, 0x4d , 0x88 , 0x5a , 0x10 , 0x69 , 0x0c , 0x77 , 0x6a ,
//
// 0x03 , // Profile 3
// 0x0e ,
// 0x81 , 0xf4 ,0x83 ,0xfc ,0x46 , 0x04, 0x4d , 0x88 , 0x5a , 0x10 , 0x69 , 0x0c , 0x77 , 0x6a ,
//
// 0x04 , // Profile 4
// 0x0e ,
// 0x81 , 0xf4 ,0x83 ,0xfc ,0x46 , 0x04, 0x4d , 0x88 , 0x5a , 0x10 , 0x69 , 0x0c , 0x77 , 0x6a ,
//
// 0x05 , // Profile 5
// 0x0e ,
// 0x81 , 0xf4 ,0x83 ,0xfc ,0x46 , 0x04, 0x4d , 0x88 , 0x5a , 0x10 , 0x69 , 0x0c , 0x77 , 0x6a ,
//   //0x84 , 0xb0 , 0x81 , 0xe0, 0x00 , 0x08 , 0x25 , 0x90 , 0xac , 0x84 ,
//
// 0x06 , // Profile 6
// 0x0e ,
// 0x81 , 0xf4 ,0x83 ,0xfc ,0x46 , 0x04, 0x4d , 0x88 , 0x5a , 0x10 , 0x69 , 0x0c , 0x77 , 0x6a ,
//
//
// 0x07 , // Profile 7
// 0x0e ,
// 0x81 , 0xf4 ,0x83 ,0xfc ,0x46 , 0x04, 0x4d , 0x88 , 0x5a , 0x10 , 0x69 , 0x0c , 0x77 , 0x6a ,
//
//     //// Weekly Profile
// 0x01 , 0x02 , 0x03 , 0x04 , 0x05 , 0x06,  0x07 ,
//
// 0x04 , // Exception Length
// 0x20 , 0x87 , 0x3b , 0x04
//
// };
//
//static  uint8_t Calendar_Bin_array[] = {
// 0x00, 0x01,  // Cal Version (2 Bytes)
// 0x00, 0x00, 0x00, 0x01 ,   // Cal File Ref (4 Bytes)
//
// 0x5f , // Cal file length (7-bit Varint)
//
// 0x52 , // Profile length (7-bit Varint)
//
// 0x01 , // Profile 1
// 0x0e ,
// 0x80 , 0xf0 ,0x83 ,0xfc ,0x25 , 0x84, 0x2d , 0x08 , 0x3c , 0x10 , 0x7f , 0x8c , 0x87 , 0x00 ,
//
// 0x02 , // Profile 2
// 0x08 ,
// 0x80 , 0x3c , 0x84 , 0x74 , 0x07 , 0x84, 0x2d , 0x08 ,
//
// 0x03 , // Profile 3
// 0x0c ,
// 0x81 , 0xb3 , 0x84 , 0x10 , 0x5c , 0x84 , 0x60 , 0x46 , 0x67 , 0xca , 0x80 , 0xc2,
//
// 0x04 , // Profile 4
// 0x08 ,
// 0x82 , 0x1c , 0x83 , 0xc0 , 0x2d , 0x14 , 0xac , 0x90 ,
//
// 0x05 , // Profile 5
// 0x0a ,
// 0x81 , 0xe0 , 0x84 , 0xb0, 0x00 , 0x08 , 0x25 , 0x90 , 0xac , 0x84 ,
// //0x84 , 0xb0 , 0x81 , 0xe0, 0x00 , 0x08 , 0x25 , 0x90 , 0xac , 0x84 ,
//
// 0x06 , // Profile 6
// 0x08 ,
// 0x81 , 0xcc , 0x83 , 0xc0 , 0x2d , 0x08 , 0x5a , 0x04,
//
// 0x07 , // Profile 7
// 0x08 ,
// 0x82 , 0x58 , 0x83 , 0x84 , 0x07 , 0x94 , 0x2d , 0x00 ,
//
//     //// Weekly Profile
// 0x01 , 0x02 , 0x03 , 0x04 , 0x05 , 0x06,  0x07 ,
//
// 0x04 , // Exception Length
// 0x20 , 0x87 , 0x3b , 0x04
//
// };

/*******************************************************************************
 * @brief Sets next calendar Bin file state machine state
 * @param[in] Next_State state to jump to
 * @return None
 *******************************************************************************/
void cal_set_bin_state(CalBinSM_t Next_State)
{
    Cal_Bin_SM_State = Next_State;
}


/*******************************************************************************
 * @brief cal_file_init
 * @param[in] None
 * @return Cal_Bin_SM_State
 *******************************************************************************/
CalBinSM_t cal_file_init( void )
{

    switch(Cal_Bin_SM_State)
    {
        case CAL_BIN_OPEN_FILE:                 //! Initial State
        {
            cal_bin_open_file();
        }
        break;

        case CAL_BIN_GET_VERSION:
        {
            Profile_ID_found = false;

            Profile_Cnt = 0;

            cal_bin_get_version();
        }
        break;

        case CAL_BIN_GET_FILE_REF:
        {
            cal_bin_get_file_ref();
        }
        break;

        case CAL_BIN_GET_FILE_LEN:
        {
            cal_bin_get_file_len();
        }
        break;

        case CAL_BIN_GET_PROFILE_LEN:
        {
            cal_bin_get_profile_len();
        }
        break;

        case CAL_BIN_GET_PROFILE_ID:
        {
            cal_bin_get_profile_ID();
        }
        break;

        case CAL_BIN_GET_PROFILE_ID_LEN:
        {
            cal_bin_get_profile_ID_len();
        }
        break;

        case CAL_BIN_GET_PROFILE_ON:
        {
            cal_bin_get_profile_ON();
        }
        break;

        case CAL_BIN_GET_PROFILE_OFF:
        {
            cal_bin_get_profile_OFF();
        }
        break;

        case CAL_BIN_GET_PROFILE_SP:
        {
            cal_bin_get_profile_SP();
        }
        break;

        case CAL_BIN_GET_WEEKLY_TABLE:
        {
            cal_bin_weekly_table();
        }
        break;

        case CAL_BIN_GET_EXCEPTION_LEN:
        {
            cal_bin_get_exception_len();
        }
        break;

        case CAL_BIN_GET_EXCEPTION_TABLE:
        {
            cal_bin_get_exception_table();
        }
        break;
        case CAL_BIN_FILE_CLOSE:
        {
            cal_bin_close_file();
        }
        break;
        case CAL_BIN_IDLE:
        {
            /* Reset Values for next run */
            Calendar_Bin_Idx = 0;
            Exception_Len = 0;
            Profile_Cnt = 0;

            cal_set_bin_state(CAL_BIN_IDLE);

        }
        break;

        default :
        {
            /* Do nothing for now */
        }
        break;

    }

    /* Ensure the File is Closed before exiting here */
    if ( Cal_Bin_SM_State == CAL_BIN_ERROR)
    {
        (void)f_close(p_cal_file);

        file_sys_release_file_ptr();

        /* Reset Values for next run */
        Calendar_Bin_Idx = 0;
        Exception_Len = 0;
        Profile_Cnt = 0;

    }

    return Cal_Bin_SM_State;
}

/*******************************************************************************
 * @brief cal_bin_open_file
 * @param[in] None
 * @return None
 *******************************************************************************/
static void  cal_bin_open_file( void )
{
    int cal_image_bytes = 0;

    cal_file_name = (const char *)CAL_OLD_FILE_NAME;

    //p_cal_file = get_patch_file_ptr();

    // attempt to acquire new file ptr from file_sys.c spare file
    // Taks will time-out after TASK_EXE_TIMEOUT_SEC = 2 min if file not
    // release by other task
    p_cal_file = get_file_sys_file_ptr();
    while( p_cal_file == 0 ){
        // wait 10 sec between checks
        FreeRTOSDelay(10000);
        p_cal_file = get_file_sys_file_ptr();
    }

    catch_file_err_report_void( OPEN_CAL_FILE, f_open(p_cal_file,
                                                cal_file_name,
                                                FA_READ ));

    // Get the Calendar file size
    cal_image_bytes = f_size(p_cal_file);

    /* Is there a valid Calendar Image File , check it's image size is > 0 then image must exist*/
    if( cal_image_bytes > 0)
    {
        cal_set_bin_state(CAL_BIN_GET_VERSION);
    }
    else
    {
        /* Report Error to Sys Monitor */
        report_error(EMPTY_CAL_FILE);

        cal_set_bin_state(CAL_BIN_ERROR);
    }

}

/*******************************************************************************
 * @brief cal_bin_open_file
 * @param[in] None
 * @return None
 *******************************************************************************/
static void  cal_bin_close_file( void )
{
    (void)f_close(p_cal_file);

    file_sys_release_file_ptr();

    cal_set_bin_state(CAL_BIN_IDLE);
}

/*******************************************************************************
 * @brief cal_bin_get_profile_contents
 * @param[in] None
 * @return bool: false = Failure
 *******************************************************************************/
static bool  cal_bin_get_profile_contents( uint16_t next_read_len)
{

    //----------------------------------------------------------------
    // KF-268 Now reading data straight into target buffer
    //----------------------------------------------------------------
    catch_file_error_report( READ_CAL_FILE, f_read(p_cal_file,
                                                Calendar_Bin_array,
                                                next_read_len,
                                                &f_rd_bytes));
    //----------------------------------------------------------------
    return true;
}

/*******************************************************************************
 * @brief cal_bin_get_version
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_bin_get_version( void )
{
    uint16_t Version = 0;

    Calendar_Bin_Idx = 0;

//  debugprint(CAL, "CAL_BIN_GET_VERSION \r\n");

    catch_error_return( cal_bin_get_profile_contents(2));

    Version = (uint16_t)(Calendar_Bin_array[Calendar_Bin_Idx]) << 8;
    Calendar_Bin_Idx++;
    Version = Version | Calendar_Bin_array[Calendar_Bin_Idx];
    Calendar_Bin_Idx++;

    CalendarInfo.Version = Version;

    /* Is there a valid Calendar Version Number */
    if( Version > 0)
    {
        cal_set_bin_state(CAL_BIN_GET_FILE_REF);
    }
    else
    {
        /* Report Error to Sys Monitor */
        report_error(VERSION_CAL_FILE);

        cal_set_bin_state(CAL_BIN_ERROR);
    }

}

/*******************************************************************************
 * @brief cal_bin_get_file_ref
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_bin_get_file_ref( void )
{
    uint32_t File_ref = 0;

    catch_error_return( cal_bin_get_profile_contents(4));

    File_ref = ((uint32_t)Calendar_Bin_array[0]);
    File_ref = File_ref | ( ((uint32_t)Calendar_Bin_array[1]) << 8);
    File_ref = File_ref | ( ((uint32_t)Calendar_Bin_array[2]) << 16);
    File_ref = File_ref | ( ((uint32_t)Calendar_Bin_array[3]) << 24);

    CalendarInfo.File_Reference = File_ref;

    /* Is there a valid Calendar File Ref Number */
    if( File_ref > 0)
    {
        cal_set_bin_state(CAL_BIN_GET_FILE_LEN);
    }
    else
    {
        /* Report Error to Sys Monitor */
        report_error(FILE_REF_CAL_FILE);

        cal_set_bin_state(CAL_BIN_ERROR);
    }

}

/*******************************************************************************
 * @brief cal_bin_get_file_len
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_bin_get_file_len( void )
{
    uint32_t File_Len = 0;
    static char packet[2] = {0};


    Calendar_Bin_Idx = 0;

    catch_error_return( cal_bin_get_profile_contents(1));

    File_Len = Calendar_Bin_array[Calendar_Bin_Idx];

    packet[0] = Calendar_Bin_array[Calendar_Bin_Idx];
    Calendar_Bin_Idx++;

    if ( File_Len > 0x7F )
    {

        static char * p_rx_pkt = NULL;
        static char **p_p_rx_pkt = &p_rx_pkt;
        static uint8_t varint_offset = 0;

        Calendar_Bin_Idx = 0;
        catch_error_return( cal_bin_get_profile_contents(1));

        packet[1] = Calendar_Bin_array[Calendar_Bin_Idx];

        p_rx_pkt = &packet[0];

        File_Len = common_varint_to_uint32(p_p_rx_pkt, &varint_offset);

        Calendar_Bin_Idx++;
    }

    CalendarInfo.File_Length = (uint16_t)File_Len;

    /* Is there a valid Calendar File Length within the CAL image */
    if( File_Len == 0 )
    {
        /* Report Error to Sys Monitor */
        report_error(FILE_LEN_CAL_FILE_NULL);

        cal_set_bin_state(CAL_BIN_ERROR);
    }
    else if (File_Len < CAL_FILE_MINIMUM_LENGTH)
    {
        /* Report Error to Sys Monitor */
        report_error(FILE_LEN_CAL_FILE_MIN);

        cal_set_bin_state(CAL_BIN_ERROR);
    }
    else if (File_Len > CAL_FILE_MAXIMUM_LENGTH)
    {
        /* Report Error to Sys Monitor */
        report_error(FILE_LEN_CAL_FILE_MAX);

        cal_set_bin_state(CAL_BIN_ERROR);
    }
    /* Is there a valid Calendar File Length within the CAL image */
    else
    {
        cal_set_bin_state(CAL_BIN_GET_PROFILE_LEN);
    }
}

/*******************************************************************************
 * @brief cal_bin_get_profile_len
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_bin_get_profile_len( void )
{
    uint32_t Profile_Length = 0;
    static char packet[2] = {0};

    Calendar_Bin_Idx = 0;

    catch_error_return( cal_bin_get_profile_contents(1));

    Profile_Length = Calendar_Bin_array[Calendar_Bin_Idx];

    packet[0] = Calendar_Bin_array[Calendar_Bin_Idx];
    Calendar_Bin_Idx++;

    if ( Profile_Length > 0x7F )
    {
        static char * p_rx_pkt = NULL;
        static char **p_p_rx_pkt = &p_rx_pkt;
        static uint8_t varint_offset = 0;

        Calendar_Bin_Idx = 0;
        catch_error_return( cal_bin_get_profile_contents(1));

        packet[1] = Calendar_Bin_array[Calendar_Bin_Idx];

        p_rx_pkt = &packet[0];

        Profile_Length = common_varint_to_uint32(p_p_rx_pkt, &varint_offset);
    }

    CalendarInfo.Profile_Length = (uint16_t)Profile_Length;

    if (0 == Profile_Length)
    {
        /* Report Error to Sys Monitor */
        report_error(PROFILE_LEN_CAL_FILE);
        return;
    }

    if (Profile_Length < CAL_PROFILE_SECTION_MINIMUM)
    {
        /* Report Error to Sys Monitor */
        report_error(PROFILE_SECTION_LEN_MIN);
        return;
    }

    if (Profile_Length > CAL_PROFILE_SECTION_MAXIMUM)
    {
        /* Report Error to Sys Monitor */
        report_error(PROFILE_SECTION_LEN_MAX);
        return;
    }

    cal_set_bin_state(CAL_BIN_GET_PROFILE_ID);
}

/*******************************************************************************
 * @brief cal_bin_get_profile_ID
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_bin_get_profile_ID( void )
{
    uint8_t Profile_ID = 0;

    Calendar_Bin_Idx = 0;
    catch_error_return( cal_bin_get_profile_contents(1));

    if ( Profile_ID_found == false )
    {
        Profile_ID = Calendar_Bin_array[Calendar_Bin_Idx];
        Calendar_Bin_Idx++;

        CalendarInfo.Profile_ID = Profile_ID;
    }

    Profile_Cnt++;

    if ( Profile_ID > CAL_MAX_PROFILE_ID )
    {
        /* Report Error to Sys Monitor */
        report_error(PROFILE_ID_ERROR_MAX);
    }
    else
    {
        cal_set_bin_state(CAL_BIN_GET_PROFILE_ID_LEN);
    }
}

/*******************************************************************************
 * @brief cal_bin_get_profile_ID_len
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_bin_get_profile_ID_len( void)
{

    uint8_t Profile_ID_Length = 0;

    Calendar_Bin_Idx = 0;
    catch_error_return( cal_bin_get_profile_contents(1));


    Profile_ID_Length = Calendar_Bin_array[Calendar_Bin_Idx];
    Calendar_Bin_Idx++;

    CalendarInfo.Profile_ID_Length = Profile_ID_Length;

    if ( Profile_ID_found == false )
    {
        //-----------------------------------------------------
        // KF-272
        //-----------------------------------------------------
        // Clear old data before fetching new profile.
        uint8_t p ;
        for( p = 0; p < APP_NO_OF_SPS; p++){
            CalendarInfo.Profile.Switching_Points[p].Lamp_Output = 0;
            CalendarInfo.Profile.Switching_Points[p].Trigger.Source_type = 0;
            CalendarInfo.Profile.Switching_Points[p].Trigger.Threshold = 0;
        }
        //-----------------------------------------------------

        CalendarInfo.Profile.Profile_len = Profile_ID_Length;
    }

    //CalendarInfo.Profile[CalendarInfo.Profile_ID].Profile_len = Profile_ID_Length;

    if (Profile_ID_Length < CAL_PROFILE_LENGTH_MINIMUM)
    {
        /* Report Error to Sys Monitor */
        report_error(PROFILE_LEN_CAL_FILE_MIN);
        return;
    }

    if (Profile_ID_Length > CAL_PROFILE_LENGTH_MAXIMUM)
    {
        /* Report Error to Sys Monitor */
        report_error(PROFILE_LEN_CAL_FILE_MAX);
        return;
    }

    cal_set_bin_state(CAL_BIN_GET_PROFILE_ON);
}

/*******************************************************************************
 * @brief cal_bin_get_profile_ON
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_bin_get_profile_ON(void)
{
    uint16_t Profile_ON = 0;
    uint16_t result = 0;

    Calendar_Bin_Idx = 0;
    catch_error_return( cal_bin_get_profile_contents(CalendarInfo.Profile_ID_Length));

    if ( Profile_ID_found == false )
    {
        Profile_ON = (uint16_t)(Calendar_Bin_array[Calendar_Bin_Idx]) << 8;
        Calendar_Bin_Idx++;
        Profile_ON = Profile_ON | Calendar_Bin_array[Calendar_Bin_Idx];
        Calendar_Bin_Idx++;

        result = Profile_ON & SOURCE_MASK;
        result = result >> 14;

        CalendarInfo.Profile.Switching_Points[0].Trigger.Source_type = result;

        result = Profile_ON & THRESHOLD_MASK;

        CalendarInfo.Profile.Switching_Points[0].Trigger.Threshold = result;

        CalendarInfo.Profile.Switching_Points[0].Lamp_Output = 100; /* Fixed to 100 or ON */
    }

    // Sanity check source types and thresholds:
    if ( CalendarInfo.Profile.Switching_Points[0].Trigger.Source_type >= CAL_TRIG_MAX)
    {
        /* Report Error to Sys Monitor */
        report_error(PROFILE_SP_SRC_INVALID);
        return;
    }

    // Check Min/Max Lux boundaries for switch-on:
    if ( CAL_TRIG_LUX == CalendarInfo.Profile.Switching_Points[0].Trigger.Source_type )
    {
        if( CAL_THRESHOLD_MAX_LUX < CalendarInfo.Profile.Switching_Points[0].Trigger.Threshold ){
            report_error(PROFILE_SP_THRESH_MAX);
            return;
        }

        if( CAL_THRESHOLD_MIN_LUX > CalendarInfo.Profile.Switching_Points[0].Trigger.Threshold ){
            report_error(PROFILE_SP_THRESH_MIN);
            return;
        }
    }

    // Check RTCC/Solar maximum time boundary, should be <= 1440:
    if ( ( CAL_TRIG_RTCC == CalendarInfo.Profile.Switching_Points[0].Trigger.Source_type ) ||
         ( CAL_TRIG_SOLAR == CalendarInfo.Profile.Switching_Points[0].Trigger.Source_type ) )
    {
        if ( CAL_THRESHOLD_MAX_MINS < CalendarInfo.Profile.Switching_Points[0].Trigger.Threshold ) {
            report_error(PROFILE_SP_THRESH_MAX);
            return;
        }
    }

    cal_set_bin_state(CAL_BIN_GET_PROFILE_OFF);

}

/*******************************************************************************
 * @brief cal_bin_get_profile_OFF
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_bin_get_profile_OFF(void)
{
    uint16_t Profile_OFF = 0;
    uint16_t result = 0;


    if ( Profile_ID_found == false )
    {
        Profile_OFF = (uint16_t)(Calendar_Bin_array[Calendar_Bin_Idx]) << 8;
        Calendar_Bin_Idx++;
        Profile_OFF = Profile_OFF | Calendar_Bin_array[Calendar_Bin_Idx];
        Calendar_Bin_Idx++;

        result = Profile_OFF & SOURCE_MASK;
        result = result >> 14;

        CalendarInfo.Profile.Switching_Points[1].Trigger.Source_type = result;

        result = Profile_OFF & THRESHOLD_MASK;

        CalendarInfo.Profile.Switching_Points[1].Trigger.Threshold = result;

        CalendarInfo.Profile.Switching_Points[1].Lamp_Output = 0; /* Fixed to O or OFF */
    }

    // Sanity check source types and thresholds:
    if ( CAL_TRIG_MAX <= CalendarInfo.Profile.Switching_Points[1].Trigger.Source_type )
    {
        /* Report Error to Sys Monitor */
        report_error(PROFILE_SP_SRC_INVALID);
        return;
    }

    // Check for RTCC ON and OFF time conflict
    if ( ( CAL_TRIG_RTCC == CalendarInfo.Profile.Switching_Points[DEFAULT_SP_ON_ID].Trigger.Source_type ) &&
         ( CAL_TRIG_RTCC == CalendarInfo.Profile.Switching_Points[DEFAULT_SP_OFF_ID].Trigger.Source_type ) )
    {
        // Report error if ON-time is more than SP OFF-time:
        if ( ( CalendarInfo.Profile.Switching_Points[DEFAULT_SP_ON_ID].Trigger.Threshold > CalendarInfo.Profile.Switching_Points[DEFAULT_SP_OFF_ID].Trigger.Threshold) )
        {
            report_error(PROFILE_SP_THRESH_CONFLICT);
            return;
        }
    }

    // Check for SP OFF Max Lux threshold:
    if ( CAL_TRIG_LUX == CalendarInfo.Profile.Switching_Points[DEFAULT_SP_OFF_ID].Trigger.Source_type )
    {
        if ( CAL_THRESHOLD_MAX_LUX < CalendarInfo.Profile.Switching_Points[DEFAULT_SP_OFF_ID].Trigger.Threshold ) {
            report_error(PROFILE_SP_THRESH_MAX);
            return;
        }
        if ( CAL_THRESHOLD_MIN_LUX > CalendarInfo.Profile.Switching_Points[DEFAULT_SP_OFF_ID].Trigger.Threshold ) {
            report_error(PROFILE_SP_THRESH_MIN);
            return;
        }
    }

    // Check for RTCC/Solar SP OFF Max (>1440) threshold:
    if ( ( CAL_TRIG_RTCC == CalendarInfo.Profile.Switching_Points[DEFAULT_SP_OFF_ID].Trigger.Source_type ) ||
         ( CAL_TRIG_SOLAR == CalendarInfo.Profile.Switching_Points[DEFAULT_SP_OFF_ID].Trigger.Source_type ) )
    {
        if (CAL_THRESHOLD_MAX_MINS < CalendarInfo.Profile.Switching_Points[DEFAULT_SP_OFF_ID].Trigger.Threshold) {
            report_error(PROFILE_SP_THRESH_MAX);
            return;
        }
    }

    cal_set_bin_state(CAL_BIN_GET_PROFILE_SP);
}

/*******************************************************************************
 * @brief cal_bin_get_profile_SP
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_bin_get_profile_SP( void)
{
    uint16_t Profile_SP = 0;
    uint16_t result = 0;
    uint8_t Profile_index = 2;

    if ( Profile_ID_found == false )
    {

        for(int n= 0; n < (CalendarInfo.Profile_ID_Length - 4) ; n++)
        {
            Profile_SP = ((uint16_t)Calendar_Bin_array[Calendar_Bin_Idx]) << 8;
            Calendar_Bin_Idx++;

            n++;
            Profile_SP = Profile_SP | ((uint16_t)Calendar_Bin_array[Calendar_Bin_Idx]);
            Calendar_Bin_Idx++;


            result = Profile_SP & SP_RTCC_MASK;
            result = result >> 5;
            CalendarInfo.Profile.Switching_Points[Profile_index].Trigger.Threshold  = result;

            result = Profile_SP & SP_LAMP_OP_MASK;

            CalendarInfo.Profile.Switching_Points[Profile_index].Lamp_Output = ( result * 5 ); /* Lamp output in 5% increments */

            CalendarInfo.Profile.Switching_Points[Profile_index].Trigger.Source_type = 2;  /* Fixed at RTCC */

            // Check for RTCC/Solar SP OFF Max (>1440) threshold:
            if ( CAL_THRESHOLD_MAX_MINS < CalendarInfo.Profile.Switching_Points[Profile_index].Trigger.Threshold ) {
                report_error(PROFILE_SP_THRESH_MAX);
                return;
            }

            // Check that the last SP threshold IS less than the next one:
            // Note: ensure the sampled SP is not the first on.
            if ( Profile_index > 2 )
            {
                if ( CalendarInfo.Profile.Switching_Points[Profile_index-1].Trigger.Threshold >= CalendarInfo.Profile.Switching_Points[Profile_index].Trigger.Threshold ) {
                    report_error(PROFILE_SP_THRESH_CONFLICT);
                    return;
                }
            }

            if ( CAL_THRESHOLD_MAX_LAMP_OUTPUT < CalendarInfo.Profile.Switching_Points[Profile_index].Lamp_Output )
            {
                report_error(PROFILE_SP_LAMP_OUTPUT_MAX);
                return;
            }

            Profile_index++;
        }
    }

    /* To skip past saving the Profile information */
    if( CalendarInfo.Current_Profile_ID == CalendarInfo.Profile_ID )
    {
        Profile_ID_found = true;
    }

    // If more profile data left to scan go back to fetch next profile
    if ( CalendarInfo.Profile_Length > ( CalendarInfo.Profile_ID_Length + 2 ))
    {
        // Take out of CalendarInfo.Profile_Length the bytes already processed
        CalendarInfo.Profile_Length = CalendarInfo.Profile_Length - CalendarInfo.Profile_ID_Length - 2;
        cal_set_bin_state(CAL_BIN_GET_PROFILE_ID);

    }
    // All profiles have now been scanned
    else
    {
        //--------------------------------------------------------------
        // KF-401 move following fail-safe section
        // Following test only relevant if not looking for default profile
        if( 0 != CalendarInfo.Current_Profile_ID )
        {
            // KF-399 If profile data not available trigger error
            if( (false == Profile_ID_found ) )
            {
                debug_printf(CALEN, "PROFILE ID NOT FOUND \r\n");
                report_error(PROFILE_NOT_FOUND);
            }
            else
            {
                debug_printf(CALEN, "PROFILE ID FOUND \r\n");
            }
        }
        //--------------------------------------------------------------

        cal_set_bin_state(CAL_BIN_GET_WEEKLY_TABLE);
    }
}

/*******************************************************************************
 * @brief cal_bin_weekly_table
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_bin_weekly_table( void )
{
    uint8_t Weekly_Profile_ID = 0;

    Calendar_Bin_Idx = 0;
    catch_error_return( cal_bin_get_profile_contents(7));

    for(int n= 1; n <= APP_NO_OF_CALENDAR ; n++)
    {
        Weekly_Profile_ID = Calendar_Bin_array[Calendar_Bin_Idx];
        Calendar_Bin_Idx++;

        CalendarInfo.WklySchedule[n].Profile_ID = Weekly_Profile_ID;

        if (Weekly_Profile_ID > CAL_MAX_PROFILE_ID)
        {
            report_error(PROFILE_ID_ERROR_MAX);
            return;
        }
    }

    cal_set_bin_state(CAL_BIN_GET_EXCEPTION_LEN);

}

/*******************************************************************************
 * @brief cal_bin_get_exception_len
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_bin_get_exception_len( void)
{

    Calendar_Bin_Idx = 0;
    catch_error_return( cal_bin_get_profile_contents(1));

    Exception_Len = Calendar_Bin_array[Calendar_Bin_Idx];
    Calendar_Bin_Idx++;

    //----------------------------------------------------
    // KF-268 Catch if exception length greater than 100.
    //----------------------------------------------------
    if( Exception_Len > CAL_PROFILE_EXCEPTION_MAX_LEN ){
        report_error(PROFILE_EXCEPT_LENGTH_MAX);
        return;
    }
    //----------------------------------------------------

    cal_set_bin_state(CAL_BIN_GET_EXCEPTION_TABLE);

}

/*******************************************************************************
 * @brief cal_bin_get_exception_table
 * @param[in] None
 * @return None
 *******************************************************************************/
static void cal_bin_get_exception_table( void )
{
    uint16_t Exception_Table = 0;
    uint16_t result = 0;

    Calendar_Bin_Idx = 0;
    catch_error_return( cal_bin_get_profile_contents(Exception_Len));

    // KF-268 position 0 was never written to.
    for(int n= 0; n < (Exception_Len/2) ; n++)
    {
        Exception_Table = (uint16_t)(Calendar_Bin_array[Calendar_Bin_Idx]) << 8;
        Calendar_Bin_Idx++;

//      n++;
        Exception_Table = Exception_Table | ((uint16_t)Calendar_Bin_array[Calendar_Bin_Idx]);
        Calendar_Bin_Idx++;

        result = Exception_Table & EXCP_MONTH_MASK;
        result = result >> 12;

        CalendarInfo.ExcptSchedule[n].Exception_month = result;

        result = Exception_Table & EXCP_DAY_MASK;
        result = result >> 7;
        CalendarInfo.ExcptSchedule[n].Exception_day = result;

        result = Exception_Table & EXCP_DAY_PROFILE;
        CalendarInfo.ExcptSchedule[n].Profile_ID = result;

        // Sanity check Exception payload:
        if ( CalendarInfo.ExcptSchedule[n].Exception_month > CAL_PROFILE_EXCEPTION_MONTH_MAX )
        {
            report_error(PROFILE_EXCEPT_PAYLOAD_ERROR);
            return;
        }

        // Check for NULL (zero) day or month value:
        if ( ( CAL_PROFILE_EXCEPTION_DAY_NULL == CalendarInfo.ExcptSchedule[n].Exception_day ) ||
             ( CAL_PROFILE_EXCEPTION_DAY_NULL == CalendarInfo.ExcptSchedule[n].Exception_month ) )
        {
            report_error(PROFILE_EXCEPT_PAYLOAD_NULL);
            return;
        }

        // Check for too large Exception Profile ID:
        if ( CalendarInfo.ExcptSchedule[n].Profile_ID > CAL_MAX_PROFILE_ID )
        {
            report_error(PROFILE_ID_ERROR_MAX);
            return;
        }

    }

    CalendarInfo.Profile_Cnt = Profile_Cnt;

    if(Profile_Cnt == 0 )
    {
        cal_set_bin_state(CAL_BIN_ERROR);
    }
    else
    {
        cal_set_bin_state(CAL_BIN_FILE_CLOSE);
    }


}

/*******************************************************************************
 * @brief cal_calc_solar_clock
 * @param[in] None
 * @return None
 *******************************************************************************/
void cal_make_default_profile( void )
{

    /* Make default Profile */

    CalendarInfo.Profile.Profile_len = 4;

    /* Switching On Point*/
    CalendarInfo.Profile.Switching_Points[0].Trigger.Source_type = 0;
    CalendarInfo.Profile.Switching_Points[0].Trigger.Threshold = DEFAULT_LUX_ON_THRESHOLD;
    CalendarInfo.Profile.Switching_Points[0].Lamp_Output = 100;

    /* Switching Off Point*/
    CalendarInfo.Profile.Switching_Points[1].Trigger.Source_type = 0;
    CalendarInfo.Profile.Switching_Points[1].Trigger.Threshold = DEFAULT_LUX_OFF_THRESHOLD;
    CalendarInfo.Profile.Switching_Points[1].Lamp_Output = 0;

    /* Create remaining Switching Points (SPs) */
    for(int n = 2; n < APP_NO_OF_SPS ; n++)
    {
        CalendarInfo.Profile.Switching_Points[n].Trigger.Source_type = 0;
        CalendarInfo.Profile.Switching_Points[n].Trigger.Threshold = 0;
        CalendarInfo.Profile.Switching_Points[n].Lamp_Output = 0;
    }

}

/*******************************************************************************
 * @brief cal_make_default_week
 * @param[in] None
 * @return None
 *******************************************************************************/
void cal_make_default_week( void )
{
    for(int n= 0; n <= APP_NO_OF_CALENDAR ; n++)
    {
        CalendarInfo.WklySchedule[n].Profile_ID = 0;
    }
}

/*******************************************************************************
 * @brief cal_make_default_exception
 * @param[in] None
 * @return None
 *******************************************************************************/
void cal_make_default_exception( void )
{
    for(int n= 0; n < APP_NO_OF_EXCPT_CALENDAR ; n++)
    {
        CalendarInfo.ExcptSchedule[n].Profile_ID = 0;
    }
}


/*******************************************************************************
 * @brief cal_bin_get_current_profile_ID
 * @param[in] None
 * @return None
 *******************************************************************************/
void cal_bin_get_current_profile_ID( void )
{
    CalBinSM_t cal_file_return = CAL_BIN_GET_VERSION;

    //----------------------------------------------------------------------
    // KF-401 Change of logic - see comments below
    //----------------------------------------------------------------------

    // A Profile ID has been selected - fetch profile content
    if(0 != CalendarInfo.Current_Profile_ID)
    {

            Cal_Bin_SM_State = CAL_BIN_OPEN_FILE;
            do
            {
                // Read the calendar file again to fetch the selected profile content
                // Load into RAM under CalendarInfo.Profile
                cal_file_return = cal_file_init();
            }while (( cal_file_return != CAL_BIN_IDLE ) && ( cal_file_return != CAL_BIN_ERROR ));

            // If on return CalendarInfo.Current_Profile_ID was overwritten
            // with 0 an error was detected > load default profile instead
            if(0 != CalendarInfo.Current_Profile_ID)
            {
                CalendarInfo.Profile_ID_Length =  (( CalendarInfo.Profile.Profile_len) / 2 );
                return;
            }

    }

    // Something not right so going to load and execute default profile ID = 0
    cal_make_default_profile();

    //----------------------------------------------------------------------
    return;
}


/*******************************************************************************
 * @brief Report error to sysmon task
 * @param[in] caller id and error code
 * @return void
 *******************************************************************************/
static void report_error(uint32_t error_code)
{
    //----------------------------------------------------------------------
    // Clear target profile ID so that default profile can be loade dinstead
    CalendarInfo.Current_Profile_ID = 0;
    //----------------------------------------------------------------------
    cal_set_bin_state(CAL_BIN_ERROR);
    sysmon_report_error(CALEN, error_code);
    return;
}
