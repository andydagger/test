/*
==========================================================================
 Name        : calendar.h
 Project     : pcore
 Path        : /pcore/src/private/calendar/calendar.h
 Author      : M Nakhuda
 Copyright   : Lucy Zodion Ltd
 Created     : 30 May 2018
 Description :
==========================================================================
*/


#ifndef CALENDAR_APP__H
#define CALENDAR_APP__H

#include "stdbool.h"
#include "stdint.h"

#define APP_NO_OF_PROFILES          1  // Now Only need the current running Profile
#define APP_NO_OF_SPS               16 // 16 No Of Switching Points
#define APP_NO_OF_CALENDAR          7  // 7 Day week
#define APP_NO_OF_EXCPT_CALENDAR    50 // 50 Exception Days

#define APP_ADC_LOW_LIGHT_CROSSING_DELAY    4   // Wait 10 seconds before detecting change of Light/DARK conditions
#define APP_ADC_LATCH_LIGHT_CROSSING_DELAY  7   // Wait a further 20 seconds before LATCHING
#define LIGHT_LATCH_HIGH            200  // TODO Should really be 200
#define LIGHT_LATCH_LOW             10  // TODO Do we need this to be this low?

#define CAL_BILLING_VALID_TIMESTAMP     1546300800  // Epoch timestamp at: 00:00:00 01/01/2019

/*!
 * Calendar process State Machine states
 */
typedef enum CalMainSM_e
{
    CAL_MAIN_SM_INIT = 0,                   /*!< Initial State */
    CAL_MAIN_SM_GET_INIT_PHOTO_STATE,       /*!< get initial Photo State */
    CAL_MAIN_SM_GET_INIT_RTCC_STATE,
    CAL_MAIN_SM_GET_RTCC,                   /*!< Get RTCC */
    CAL_MAIN_SM_CALC_DAY_NUMBER,            /*!< Calculate the Day Number */
    CAl_MAIN_SM_CHECK_EXCEPTION_TABLE,      /*!< Are we an Exception Day */
    CAL_MAIN_SM_CHECK_WEEKLY_TABLE,         /*!< Get the Profile ID from the Weekly Table */
    CAL_MAIN_SM_GET_SP_FROM_PROFILE_ID,     /*!< Get the Profile ID Table */
    CAL_MAIN_SM_GET_SP_ON_OFF_MODE,         /*!< Look for SP ON and SP OFF */
    CAL_MAIN_SM_GET_NEXT_SP,                /*!< Getting the Next SP */
    CAL_MAIN_SM_GET_MODE,                   /*!< Which Trigger Mode are we in  */
    CAL_MAIN_SM_GET_PHOTO_STATE,            /*!< get the Photo Cell state */
    CAL_MAIN_SM_GET_SOLAR_STATE,            /*!< Calculate the Solar Curve */
    CAL_MAIN_SM_GET_TIME_STATE,             /*!< Get the RTCC */
    CAL_MAIN_SM_SET_DALI_AND_RELAY,         /*!< Action the DALI and the Relay as required */
    CAL_MAIN_SM_IDLE,                       /*!< IDLE */
} CalMainSM_t;

/*!
 * Calendar trigger states
 */
typedef enum CalTriggerSM_e
{
    CAL_TRIG_LUX = 0,               /*!< Initial State */
    CAL_TRIG_SOLAR,                 /*!< Initiate Delay after Power Up */
    CAL_TRIG_RTCC,                  /*!< Wait appropriate Delay (For Module Ready) */
    CAL_TRIG_MAX
} CalTrigger_States_t;

typedef struct __attribute__((packed)) Trigger_s
{
    uint8_t         Source_type;        // enumerate  (Solar/Lux/RTCC)
    uint16_t        Threshold;          // ADC Lux Level, RTCC offset from Midnight
} Trigger_t;

typedef struct __attribute__((packed)) SwitchingPoint_s
{
    Trigger_t       Trigger;
    uint8_t         Lamp_Output;
} SwitchingPoint_t;

typedef struct __attribute__((packed)) Profile_s
{
    SwitchingPoint_t    Switching_Points[APP_NO_OF_SPS];
    uint8_t             Profile_len;
} Profile_t;

typedef struct __attribute__((packed)) WklySchedule_s
{
    uint8_t             Profile_ID;
} WklySchedule_t;

typedef struct __attribute__((packed)) ExcptSchedule_s
{
    uint8_t             Exception_month;
    uint8_t             Exception_day;
/*  uint16_t            Exception_night;  */ // Commenting out for now to save RAM - KF-226
    uint8_t             Profile_ID;
} ExcptSchedule_t;

/*!
 * Calendar data and related variables
 */
typedef struct CalendarInfo_s
{
    uint16_t            Version;
    uint32_t            File_Reference;
    uint16_t            File_Length;
    uint16_t            Profile_Length;
    uint8_t             Profile_ID;
    uint8_t             Profile_ID_Length;
    uint8_t             Profile_Cnt;
    Profile_t           Profile;    //[APP_NO_OF_PROFILES];             /*!< Profile */
    WklySchedule_t      WklySchedule[APP_NO_OF_CALENDAR + 1];           /*!< WklySchedule */
    ExcptSchedule_t     ExcptSchedule[APP_NO_OF_EXCPT_CALENDAR];    /*!< ExcptSchedule */
    uint8_t             Current_Profile_ID;
    uint8_t             Current_Switching_Point;
    uint8_t             Current_ON_OFF_Switching_Point;
} CalendarInfo_t;

/*!
 * Photo Lux states
 */
typedef enum Photo_States_e
{
    PHOTO_NOT_READY = 0,            /*!< Initial State */
    PHOTO_DARK,                     /*!< LUX is Dark */
    PHOTO_DARK_LATCHED,                     /*!< LUX is Dark */
    PHOTO_LIGHT,                    /*!< LUX is Light */
    PHOTO_LIGHT_LATCHED,                    /*!< LUX is Light */
    PHOTO_TRANSISTIONING,           /*!< Waiting for Photo State Change */

} Photo_States_t;

void calendar_check_sp(void);
void cal_reset_cal_sm(void);
bool cal_get_calc_solar_clock( void );
void cal_set_calc_solar_clock( bool Value );
void cal_set_Time_synch( bool b_gps_time_rcvd );
bool cal_get_Time_synch( void );
void cal_set_GPS_Data_synch( bool b_gps_data_rcvd );

/*****************/
/* Basic Defines */
/*****************/

#define terminateProfileWithLampOff()   b_profile_is_on = false; Lamp_Output_level = 0
#define terminateProfileWithLampOn()   b_profile_is_on = false


#endif // CALENDAR_APP__H


