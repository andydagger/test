
/*
==========================================================================
 Name        : solar_clock.h
 Project     : pcore
 Path        : /pcore/src/private/calendar/solar_clock.h
 Author      : M Nakhuda
 Copyright   : Lucy Zodion Ltd
 Created     : 16 July 2018
 Description :
==========================================================================
*/
#ifndef SOLAR_CLOCK__H
#define SOLAR_CLOCK__H

#include "config_dataset.h"
#include "stdbool.h"
#include "stdint.h"

void solar_main(void);
void get_solar_dataset(SolarClockDataset * dataset);

/*****************/
/* Basic Defines */
/*****************/




#endif // SOLAR_CLOCK__H
