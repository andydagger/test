/*
==========================================================================
 Name        : calendar_process.h
 Project     : pcore
 Path        : /pcore/src/private/calendar/calendar_process.h
 Author      : M Nakhuda
 Copyright   : Lucy Zodion Ltd
 Created     : 30 May 2018
 Description :
==========================================================================
*/


#ifndef CALENDAR_PROCX__H
#define CALENDAR_PROCX__H

/*!
 * Calendar BIN File INDEX
 */

#define CAL_BIN_INDEX_VERSION 			0									//! Initial State
#define CAL_BIN_INDEX_FILE_REF			CAL_BIN_INDEX_VERSION + 2			//! File Reference
#define CAL_BIN_INDEX_FILE_LEN			CAL_BIN_INDEX_FILE_REF + 4			//! File Length
#define CAL_BIN_INDEX_PROFILE_LEN		CAL_BIN_INDEX_FILE_LEN + 1			//! Profile Length

#define CAL_BIN_INDEX_PROFILE_ID		0									//! Profile Index Number
#define CAL_BIN_INDEX_PROFILE_ID_LEN	CAL_BIN_INDEX_PROFILE_ID + 1		//! Profile Index Length
#define CAL_BIN_INDEX_PROFILE_ON		CAL_BIN_INDEX_PROFILE_ID_LEN + 1	//! Profile Switching ON Point
#define CAL_BIN_INDEX_PROFILE_OFF		CAL_BIN_INDEX_PROFILE_ON + 2		//! Profile Switching OFF Point
#define CAL_BIN_INDEX_PROFILE_SP		CAL_BIN_INDEX_PROFILE_OFF + 2			//! Profile Switching Points

#define CAL_BIN_INDEX_WEEKLY_TABLE		0									//! General Weekly Table

#define CAL_BIN_INDEX_EXCEPTION_LEN		0									//! Exception Table Length
#define CAL_BIN_INDEX_EXCEPTION_TABLE	CAL_BIN_INDEX_EXCEPTION_LEN + 1		//! General Exeption Table

#define CAL_RPL_LENGTH_BYTES			255

#define SOURCE_MASK	 	0xc000  //1100 0000 0000 0000
#define THRESHOLD_MASK	0x1fff  //0001 1111 1111 1111

#define SP_RTCC_MASK	 	0xFFE0  //1111 1111 1110 0000
#define SP_LAMP_OP_MASK		0x001F  //0000 0000 0001 1111

#define EXCP_MONTH_MASK	 	0xF000  //1111 0000 0000 0000
#define EXCP_DAY_MASK		0x0F80  //0000 1111 1000 0000
#define EXCP_DAY_PROFILE	0x007F  //0000 0000 0111 1111

#define DEFAULT_LUX_ON_THRESHOLD		55
#define DEFAULT_LUX_OFF_THRESHOLD		28

#define DEFAULT_SP_ON_ID                0
#define DEFAULT_SP_OFF_ID               1

#define CAL_THRESHOLD_MAX_LUX           200
#define CAL_THRESHOLD_MIN_LUX           10
#define CAL_THRESHOLD_MAX_MINS          1440
#define CAL_THRESHOLD_MAX_LAMP_OUTPUT   100

#define CAL_FILE_MINIMUM_LENGTH         19      // KF-381: TODO - Need to check this value is correct.
#define CAL_FILE_MAXIMUM_LENGTH         2056


#define CAL_PROFILE_EXCEPTION_DAY_NULL  0
#define CAL_PROFILE_EXCEPTION_MAX_LEN   100
#define CAL_PROFILE_EXCEPTION_MONTH_MAX 12
#define CAL_PROFILE_SECTION_MINIMUM     10
#define CAL_PROFILE_SECTION_MAXIMUM     1940
#define CAL_PROFILE_LENGTH_MINIMUM      4
#define CAL_PROFILE_LENGTH_MAXIMUM      34

#define CAL_MAX_PROFILE_ID              56      // Range: 0 .. 56


/*!
 * Calendar process BIN File states
 */
typedef enum CalBinSM_e
{
	CAL_BIN_OPEN_FILE = 0,
	CAL_BIN_GET_VERSION,
	CAL_BIN_GET_FILE_REF,					//! File Reference
	CAL_BIN_GET_FILE_LEN,					//! File Length
	CAL_BIN_GET_PROFILE_LEN,				//! Profile Length
	CAL_BIN_GET_PROFILE_ID,					//! Profile Index Number
	CAL_BIN_GET_PROFILE_ID_LEN,				//! Profile Index Length
	CAL_BIN_GET_PROFILE_ON,					//! Profile Switching ON Point
	CAL_BIN_GET_PROFILE_OFF,				//! Profile Switching OFF Point
	CAL_BIN_GET_PROFILE_SP,					//! Profile Switching Points
	CAL_BIN_GET_WEEKLY_TABLE,				//! General Weekly Table
	CAL_BIN_GET_EXCEPTION_LEN,				//! Exception Table Length
	CAL_BIN_GET_EXCEPTION_TABLE,			//! General Exeption Table
	CAL_BIN_FILE_CLOSE,
	CAL_BIN_IDLE,
	CAL_BIN_ERROR
} CalBinSM_t;

/*****************/
/* Basic Defines */
/*****************/


CalBinSM_t cal_file_init( void );
void cal_make_default_profile( void );
void cal_make_default_week( void );
void cal_make_default_exception( void );
void cal_bin_get_current_profile_ID( void );
void cal_set_bin_state(CalBinSM_t Next_State);


#endif // CALENDAR_PROCX__H

