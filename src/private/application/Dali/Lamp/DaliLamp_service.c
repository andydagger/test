/*
 * DaliLamp_service.c
 *
 *  Created on: 14 May 2021
 *      Author: benraiton
 */

#include <stdint.h>

#include "DaliLamp_service.h"
#include "DaliLamp_impl.h"
#include "DaliAL.h"
#include "common.h"

/*!
	\brief 		Check we need to cange the brightness level and send a broadcast
	            command to set the new brightness level
	\param 		new_brightness New level of brightness
	\param 		prev_brightness Current brightness level
	\return     OK,ERR,BRIGHTNESS_EQUAL
*/
ZAGHA_LAMP_RET ZaghaLamp_directArcPowerControl(uint8_t new_brightness, uint8_t prev_brightness) {
	uint8_t arcLevel = 0;

	if (new_brightness == prev_brightness) {
		return ZAGHA_LAMP_RET_BRIGHTNESS_EQUAL;
	}

	if (ZaghaLamp_getArcLevelFromBrightness(new_brightness, &arcLevel) == ZAGHA_LAMP_RET_ERR) {
		return ZAGHA_LAMP_RET_ERR;
	}

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_directArcPowerControl(arcLevel, &status);

	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_LAMP_RET_ERR;
	}

	return ZAGHA_LAMP_RET_OK;
}

/*!
	\brief 		Check the brightness level of an individual lamp

	\param 		address  				Short address of the driver
	\param 		next_brighness_level 	Expected level of brightness for that lamp
*/
ZAGHA_LAMP_RET ZaghaLamp_checkdriverArcLevel(uint8_t address, uint8_t next_brightness_level)
{
	uint8_t arcLevel = 0;

	ZaghaLamp_getArcLevelFromBrightness(next_brightness_level, &arcLevel);

	return ZaghaLamp_checkBrightnessLevel(address, arcLevel);
}
