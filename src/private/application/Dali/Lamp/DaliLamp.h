/*
==========================================================================
 Name        : ZaghaLamp.h
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/ZaghaLamp.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 22 Oct 2019
 Description : 
==========================================================================
*/

#ifndef PRIVATE_APPLICATION_ZAGHA_ZAGHALAMP_H_
#define PRIVATE_APPLICATION_ZAGHA_ZAGHALAMP_H_

#include <stdint.h>
#include <stdbool.h>
#include "DaliController.h"

typedef enum
{
    ZAGHA_LAMP_RET_OK,
	ZAGHA_LAMP_RET_BRIGHTNESS_EQUAL,
	ZAGHA_LAMP_RET_ERR
} ZAGHA_LAMP_RET;

bool ZaghaLamp_init(void);
bool ZaghaLamp_reinit(void);
void ZaghaLamp_updateTargetLevel(uint8_t level);
void ZaghaLamp_service( struct dali_app_state *dali_state);
ZAGHA_LAMP_RET ZaghaLamp_setFadeTime(uint8_t fadeTime);
bool ZaghaLamp_getArcReadBackFault(void);
void ZaghaLamp_clearArcReadBackFault(void);

#endif /* PRIVATE_APPLICATION_ZAGHA_ZAGHALAMP_H_ */
