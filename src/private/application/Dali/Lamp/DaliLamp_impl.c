/*
==========================================================================
 Name        : ZaghaLamp_impl.c
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/ZaghaLamp_impl.c
 Author      : Chris Burdett-Smith-Whitting
 Copyright   : Lucy Zodion Ltd
 Created     : 25 Oct 2019
 Description :
==========================================================================
*/

#include "DaliLamp_impl.h"
#include "DaliAL.h"
#include "common.h"

#define ARC_GRANULARITY			5

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

//See BS EN 62386-102:2014 Table 3 for values.
//value is first value larger than requested
//e.g. 4.962% = 144 5.099% = 145, therefore 5% = 145
uint8_t arcLevel[21] = {
	0,
	145,
	170,
	185,
	196,
	204,
	210,
	216,
	221,
	225,
	229,
	233,
	236,
	239,
	241,
	244,
	246,
	249,
	251,
	253,
	254
};


/*!
	\brief 		Read the arc index of a lamp and check if it is the expected level
	\detail Table of brightness - arc levels

	brightness	|		|  arc
	------------------------------
		n 		| n+4 	| (n+4)/5
		0 		| 4   	| 0
		1 		| 5   	| 1
		2 		| 6   	| 1
		3 		| 7   	| 1
		4 		| 8   	| 1
		5 		| 9   	| 1
		6 		| 10  	| 2
		. 		| .   	| .
		. 		| .   	| .
		. 		| .   	| .
	   99		| 103 	| 20
	  100		| 104 	| 20

	\param 		brightness 	Brightness level
	\return 	arc index value
*/

uint8_t arcIndexFromBrightness(uint8_t brightness)
{
	uint8_t adjustedBrightness = (brightness + 4);
	uint8_t index = (adjustedBrightness / ARC_GRANULARITY);

	//This is to ensure that the value is always rounded down
	//Earlier versions of the C compiler (e.g. C89) have processor dependent
	//rounding.
	//For example if 9/5 rounds up to 2, then 2*5 > 9 so this would subtract 1
	if ((index * ARC_GRANULARITY) > adjustedBrightness)
	{
		return index -1;
	}
	return index;
}

/*!
	\brief 		Read the arc index of a lamp and check if it is the expected level
	\param 		brightness 	level of brightness
	\param 		p_retVal  equivalent arc level
*/

ZAGHA_LAMP_RET ZaghaLamp_getArcLevelFromBrightness(
		uint8_t brightness,
		uint8_t* p_retVal)
{
	if (brightness > 100) {return ZAGHA_LAMP_RET_ERR;}
	if (p_retVal == NULL) {return ZAGHA_LAMP_RET_ERR;}

	*p_retVal = arcLevel[arcIndexFromBrightness(brightness)];
	return ZAGHA_LAMP_RET_OK;
}

/*!
	\brief 		Read the arc level of a lamp and check if it is the expected level

	\param 		address  	Short address of the driver
	\param 		expected 	Arc expected level
*/
ZAGHA_LAMP_RET ZaghaLamp_checkBrightnessLevel(uint8_t address, uint8_t expected) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	uint8_t actual = DaliAL_queryActualLevel(address, &status);

	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_LAMP_RET_ERR;
	}

	if (actual != expected) {
		return ZAGHA_LAMP_RET_ERR;
	}

	return ZAGHA_LAMP_RET_OK;
}
