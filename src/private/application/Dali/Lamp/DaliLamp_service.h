/*
 * DaliLamp_service.h
 *
 *  Created on: 14 May 2021
 *      Author: benraiton
 */

#ifndef PRIVATE_APPLICATION_DALI_LAMP_DALILAMP_SERVICE_H_
#define PRIVATE_APPLICATION_DALI_LAMP_DALILAMP_SERVICE_H_

#include "DaliLamp.h"

/*****************************************************************************
 * Sends a DAPC command to the DALI AL - brightness in percent
 ****************************************************************************/
ZAGHA_LAMP_RET ZaghaLamp_directArcPowerControl(uint8_t brightness, uint8_t prev_brightness);

/*****************************************************************************
 * Checks if te driver arc level matches the requested level
 ****************************************************************************/
ZAGHA_LAMP_RET ZaghaLamp_checkdriverArcLevel(uint8_t address, uint8_t next_brightness_level);

#endif /* PRIVATE_APPLICATION_DALI_LAMP_DALILAMP_SERVICE_H_ */
