/*
==========================================================================
 Name        : ZaghaLamp.c
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/ZaghaLamp.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 22 Oct 2019
 Description : 
==========================================================================
*/

#include <stdint.h>
#include <stdbool.h>

#include "DaliLamp.h"

#include "DaliController.h"
#include "DaliLamp_impl.h"
#include "DaliLamp_service.h"
#include "DaliAL.h"
#include "common.h"
#include "systick.h"
#include "sysmon.h"
#include "debug.h"

#define ZL_DEFAULT_FADE_TIME_SEC	1
#define ZL_DEFAULT_FADE_COMMAND		2	//N.B. 1=0.7 seconds, 2 = 1 second

#define ZL_NUM_READ_BACK_MISMATCH_BEFORE_FAULT 2

static uint8_t current_brightness_level = 0;
static uint8_t next_brightness_level = 0;

static bool arc_read_back_fault = false;
static uint8_t arc_read_back_fault_debounce = 0;
static bool read_back_arc_level_is_pending = false;

/*!
	\brief 		Initialise the Lamp module
*/
bool ZaghaLamp_init(void) {
	bool ret = false;

	do {
		if (ZaghaLamp_setFadeTime(ZL_DEFAULT_FADE_COMMAND) != ZAGHA_LAMP_RET_OK) {
			break;
		}

		dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

		uint8_t curve = DaliAL_queryDimmingCurve(DALI_ADDRESS_IGNORE, DALI_DEVICE_TYPE_LED, &status);
		if (status != DALI_BUS_STATUS_OK) {
			break;
		}

		if (curve != DALI_DIMMING_CURVE_LOGARITHMIC) {
			DaliAL_selectDimmingCurve(DALI_DEVICE_TYPE_LED, DALI_DIMMING_CURVE_LOGARITHMIC, &status);
			if (status != DALI_BUS_STATUS_OK) {
				break;
			}
		}

		current_brightness_level = 101;
		ret = true;
	} while (0);

	return ret;
}

/*!
	\brief 		Reinitialise the Lamp module, once the driver is unlocked
*/
bool ZaghaLamp_reinit(void) {
	return ZaghaLamp_init();
}

/*!
	\brief 		Update the lamp unit with the next brightness level to apply
*/
void ZaghaLamp_updateTargetLevel(uint8_t level) {
    next_brightness_level = level;
    return;
}

/*!
	\brief 		Update the lamp units with the next brightness level to apply
	\details 	It sends a broadcast command to change the level and check every driver to get
	            the level of each lamp
	            If we get ZL_NUM_READ_BACK_MISMATCH_BEFORE_FAULT consecutive errors,
	            arc_read_back_fault will be set
	\param 		dali_state Structure including the number of connected Dali drivers and
	  			an array with short addresses of the connected drivers
*/

void ZaghaLamp_service(struct dali_app_state *dali_state) {
	ZAGHA_LAMP_RET arc_ret = ZAGHA_LAMP_RET_ERR;

    if (dali_state == NULL) {
    	return;
    }

    if (!read_back_arc_level_is_pending) {
		arc_ret = ZaghaLamp_directArcPowerControl(next_brightness_level, current_brightness_level);

		if (arc_ret == ZAGHA_LAMP_RET_BRIGHTNESS_EQUAL) {
			return;
		}
		else if (arc_ret == ZAGHA_LAMP_RET_OK) {
			read_back_arc_level_is_pending = true;
			return;
		}
	}

	read_back_arc_level_is_pending = false;

	for (uint8_t i = 0; i < dali_state->driver_count; i++) {
		arc_ret = ZaghaLamp_checkdriverArcLevel(dali_state->driver_address[i], next_brightness_level);

		if (arc_ret == ZAGHA_LAMP_RET_ERR) {
			break;
		}
	}

	if (arc_ret == ZAGHA_LAMP_RET_OK) {
		arc_read_back_fault_debounce = 0;
		arc_read_back_fault = false;
		current_brightness_level = next_brightness_level;
	}
	else {
		arc_read_back_fault_debounce++;
		current_brightness_level = 0xFF;
	}

	// Only trigger arc level fault after X consecutive mismatch
	if (arc_read_back_fault_debounce >= ZL_NUM_READ_BACK_MISMATCH_BEFORE_FAULT)	{
		arc_read_back_fault = true;
		arc_read_back_fault_debounce--; // avoid overflow
	}
}


/*!
	\brief 		Sends a Set fade time command to the DALI AL
*/
ZAGHA_LAMP_RET ZaghaLamp_setFadeTime(uint8_t fade_time) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_setFadeTime(fade_time, &status);

	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_LAMP_RET_ERR;
	}

	return ZAGHA_LAMP_RET_OK;
}

/*!
	\brief 		Returns whether there was a read back fault
*/
bool ZaghaLamp_getArcReadBackFault(void) {
	return arc_read_back_fault;
}

/*!
	\brief 		Clears the arc read back fault
*/
void ZaghaLamp_clearArcReadBackFault(void) {
	arc_read_back_fault = false;
}
