/*
==========================================================================
 Name        : ZaghaLamp_impl.h
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/ZaghaLamp_impl.h
 Author      : Chris Burdett-Smith-Whitting
 Copyright   : Lucy Zodion Ltd
 Created     : 25 Oct 2019
 Description :
==========================================================================
*/

#ifndef PRIVATE_APPLICATION_ZAGHA_ZAGHALAMP_IMPL_H_
#define PRIVATE_APPLICATION_ZAGHA_ZAGHALAMP_IMPL_H_

#include "DaliLamp.h"

ZAGHA_LAMP_RET ZaghaLamp_getArcLevelFromBrightness(
		uint8_t brightness,
		uint8_t* p_retVal);

ZAGHA_LAMP_RET ZaghaLamp_checkBrightnessLevel(uint8_t driver_address, uint8_t expected_level);

#endif /* PRIVATE_APPLICATION_ZAGHA_ZAGHALAMP_IMPL_H_ */
