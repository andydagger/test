/*
==========================================================================
 Name        : ZaghaController_impl.h
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/ZaghaController_impl.h
 Author      : Chris BSW
 Copyright   : Lucy Zodion Ltd
 Created     : 29 Oct 2019
 Description :
==========================================================================
*/

#ifndef PRIVATE_APPLICATION_ZAGHA_ZAGHACONTROLLER_IMPL_H_
#define PRIVATE_APPLICATION_ZAGHA_ZAGHACONTROLLER_IMPL_H_

#include <stdint.h>
#include <stdbool.h>

#include "DaliController.h"

ZAGHA_CTRL_RET ZaghaController_checkState(uint8_t, uint8_t *);
bool ZaghaController_timeToDaliPing(void);

#endif /* PRIVATE_APPLICATION_ZAGHA_ZAGHACONTROLLER_IMPL_H_ */

