/*
==========================================================================
 Name        : ZaghaController_impl.c
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/ZaghaController_impl.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 22 Oct 2019
 Description :
==========================================================================
*/

#include <stdint.h>
#include <stdbool.h>

#include "DaliAL.h"
#include "DaliController_impl.h"
#include "DaliDriver.h"
#include "DaliLamp.h"
#include "sysmon.h"
#include "common.h"
#include "systick.h"

static bool firstPing = false;
static uint32_t refTimeSinceLastPing = 0;

// Lookup table for converting DALI status codes to sysmon fault codes
static const uint16_t sysmon_status_map[][2] = {
	{DALI_DRIVER_STATUS_LAMP_FAILURE, 			LAMP_FAULT_FLAG},
	{DALI_DRIVER_STATUS_CONTROL_GEAR_FAILURE, 	DRIVER_FAULT_FLAG}
};

// Lookup table for converting DALI fault codes to sysmon fault codes
static const uint16_t sysmon_fault_map[][2] = {
	{DALI_DRIVER_FAILURE_STATUS_SHORT_CIRCUIT, 					SHORT_CIRCUIT_FLAG},
	{DALI_DRIVER_FAILURE_STATUS_OPEN_CIRCUIT, 					OPEN_CIRCUIT_FLAG},
	{DALI_DRIVER_FAILURE_STATUS_CURRENT_PROTECTOR_ACTIVE, 		CURRENT_PROTECT_FLAG},
	{DALI_DRIVER_FAILURE_STATUS_LOAD_DECREASE, 					LOAD_DECREASED_FLAG},
	{DALI_DRIVER_FAILURE_STATUS_LOAD_INCREASE, 					LOAD_INCREASED_FLAG},
	{DALI_DRIVER_FAILURE_STATUS_THERMAL_SHUTDOWN, 				THERMAL_SHUT_DOWN_FLAG},
	{DALI_DRIVER_FAILURE_STATUS_THERMAL_OVERLOAD_LIGHT_REDUCED, THERMAL_OVERLOAD_FLAG},
	{DALI_DRIVER_FAILURE_STATUS_REFERENCE_MEASUREMENT_FAILED, 	REF_MEASUREMENT_FAILED}
};

static uint16_t convert_status_to_sysmon(dali_driver_status_t status);
static uint16_t convert_failure_status_to_sysmon(dali_driver_failure_status_t status);
static uint16_t convert_readback_to_sysmon(bool status);

/*!
	\brief 		Read and store DALI driver state.

	\details 	Read-then-convert DALI status + fault codes from native values to sysmon-relevant types.
				Any faults are loaded into sysmon.
				Uses "Query Status" and "Query Failure Status" commands.

	\param 		n 		Number of DALI devices to check
	\param 		dev 	List of DALI short device addresses to check
*/
ZAGHA_CTRL_RET ZaghaController_checkState(uint8_t n, uint8_t dev[n]) {
	ZAGHA_CTRL_RET ret = ZAGHA_CTRL_RET_OK;

	if (dev == NULL) {
		n = 0;
	}

	uint16_t sysmon_flags = 0;

	for (uint8_t i = 0; i < n; i++) {
		dali_bus_status_t dali_bus_status = DALI_BUS_STATUS_INVALID;

		dali_driver_status_t dali_driver_status = DaliAL_queryStatus(dev[i], &dali_bus_status);

		if (dali_bus_status == DALI_BUS_STATUS_OK) {
			sysmon_flags |= convert_status_to_sysmon(dali_driver_status);
		}
		else {
			ret = ZAGHA_CTRL_RET_ERROR;
		}

		dali_driver_failure_status_t dali_driver_fail_status = DaliAL_queryFailureStatus(
			dev[i],
			DALI_DEVICE_TYPE_LED,
			&dali_bus_status
		);

		if (dali_bus_status != DALI_BUS_STATUS_OK) {
			ret = ZAGHA_CTRL_RET_ERROR;
			continue;
		}

		sysmon_flags |= convert_failure_status_to_sysmon(dali_driver_fail_status);
	}

	sysmon_flags |= convert_readback_to_sysmon(ZaghaLamp_getArcReadBackFault());

	sysmon_update_dali_faults(sysmon_flags);

	return ret;
}

bool ZaghaController_timeToDaliPing(void) {
	// Random 0-5min delay for first ping
	if (!firstPing) {
		uint32_t delayBeforeFirstPing = common_trueRandomGenerator(DALI_FIRST_PING_MAX_DELAY_MS);
		refTimeSinceLastPing = (uint32_t) (delayBeforeFirstPing - DALI_PING_INTERVAL_MS);
		firstPing = true;
	}

	// Periodic ping out
	if ((get_systick_ms() - refTimeSinceLastPing) > DALI_PING_INTERVAL_MS) {
		refTimeSinceLastPing = get_systick_ms();
		return true;
	}
	else {
		return false;
	}
}

static uint16_t convert_status_to_sysmon(dali_driver_status_t status) {
	uint16_t sysmon_flags = 0x0000;

	for (int16_t i = 0; i < ARRAY_LEN(sysmon_status_map); i++) {
		if ((uint16_t) status & sysmon_status_map[i][0]) {
			sysmon_flags |= sysmon_status_map[i][1];
		}
	}

	return sysmon_flags;
}

static uint16_t convert_failure_status_to_sysmon(dali_driver_failure_status_t status) {
	uint16_t sysmon_flags = 0x0000;

	for (int16_t i = 0; i < ARRAY_LEN(sysmon_fault_map); i++) {
		if ((uint16_t) status & sysmon_fault_map[i][0]) {
			sysmon_flags |= sysmon_fault_map[i][1];
		}
	}

	return sysmon_flags;
}

static uint16_t convert_readback_to_sysmon(bool status) {
	uint16_t sysmon_flags = 0x0000;

	if (status) {
		sysmon_flags |= (uint16_t) READ_BACK_DIFF_FLAG;
	}

	return sysmon_flags;
}
