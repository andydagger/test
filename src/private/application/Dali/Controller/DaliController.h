/*
==========================================================================
 Name        : ZaghaController.h
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/ZaghaController.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 22 Oct 2019
 Description : 
==========================================================================
*/

#ifndef PRIVATE_APPLICATION_ZAGHA_ZAGHACONTROLLER_H_
#define PRIVATE_APPLICATION_ZAGHA_ZAGHACONTROLLER_H_

#include <stdint.h>
#include <stdbool.h>

#include "config_dataset.h"
#include "DaliDriver_common.h"

#define DALI_MAX_DRIVERS 	16

#define DRIVER_STARTUP_TIME_OUT_SEC 15
#define DRIVER_ACCESS_LEVEL    		0x57
#define DRIVER_PASSWORD				{0x8C,0x54,0x57,0xFE}

#define ZAGHA_UNIT_SERVICING_PERIOD_MS (6 * 1000)

// DALI_PING_INTERVAL every 10 minutes
#define DALI_FIRST_PING_MAX_DELAY_MS  (uint32_t)( 5 * 60 * 1000 )
#define DALI_PING_INTERVAL_MS (uint32_t)( 10 * 60 * 1000 )

typedef enum
{
    ZAGHA_CTRL_RET_OK,
	ZAGHA_CTRL_RET_ERROR
} ZAGHA_CTRL_RET;

struct dali_app_state {
	uint8_t driver_count;
	uint8_t driver_address[DALI_MAX_DRIVERS];
};

void ZaghaController_reset(void);
bool ZaghaController_init(void);
void ZaghaController_service(void);
bool ZaghaController_updateLampOutput(uint8_t level);
void ZaghaController_getDataset(LuminrDataset * dataset);
void ZaghaController_updateFadeTime(uint8_t fadeTime);
void ZaghaController_requestZaghaStackInit(void);
bool ZaghaController_getZaghaStackInitRequested(void);

#endif /* PRIVATE_APPLICATION_ZAGHA_ZAGHACONTROLLER_H_ */
