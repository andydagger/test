/*
==========================================================================
 Name        : ZaghaController.c
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/ZaghaController.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 22 Oct 2019
 Description : 
==========================================================================
*/

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "DaliController.h"
#include "DaliController_impl.h"
#include "DaliAL.h"
#include "DaliLamp.h"
#include "DaliDriver.h"
#include "DaliDriver_common.h"
#include "debug.h"
#include "sysmon.h"
#include "FreeRTOSCommonHooks.h"
#include "systick.h"
#include "relay.h"
#include "common.h"

#define LAMP_TYPE_SHIFT					2
#define DRIVER_TYPE_DALI				1

static uint8_t g_fadeTime;
static uint32_t g_timeSinceLastServiceMs = 0;

static struct dali_app_state state = {0};

typedef struct ZaghaController_Flags
{
	bool reinitialiseFlag;
	bool setFadeTimeFlag;
	bool serviceStackFlag;
	bool initialiseDriverFlag;
	bool initialiseLampFlag;
} ZC_Flags;

static ZC_Flags g_flags = {
	.reinitialiseFlag = false,
	.setFadeTimeFlag = true,
	.serviceStackFlag = true,
	.initialiseDriverFlag = true,
	.initialiseLampFlag = true
};

static inline bool resetDali(void)
{
	dali_bus_status_t status;
	DaliAL_reset(&status);

	return (status == DALI_BUS_STATUS_OK);
}

static inline bool initialiseDriver(void)
{
	if (g_flags.initialiseDriverFlag)
	{
		// initialise driver
		// change required here when new dali driver merged
		// to provide the driver count to the ZaghaDriver_init() function.
		// currently hard coded to '1'
		if (ZaghaDriver_init(&state) == false) {
			return false;
		}
	}

	return true;
}

static inline bool initialiseLamp(void)
{
	if (g_flags.initialiseLampFlag)
	{
		// initialise Lamp
		catch_error_fail( ZaghaLamp_init() );
	}

	return true;
}

static inline bool serviceReinitialise(void)
{
	if( g_flags.reinitialiseFlag == true )
	{
		if( ZaghaController_init() == true )
		{
			g_flags.reinitialiseFlag = false;
		}
		return true;
	}

	return false;
}

static inline void serviceSetFadeTime(void)
{
	if (true == g_flags.setFadeTimeFlag)
	{
		g_flags.setFadeTimeFlag = false;
		ZaghaLamp_setFadeTime(g_fadeTime);
	}
}

static inline bool timeToServiceStack(void)
{
	return (get_systick_ms() - g_timeSinceLastServiceMs) >
		ZAGHA_UNIT_SERVICING_PERIOD_MS;
}

static inline void serviceStack(void)
{
	/*
	 * Do not service Dali stack when relay is open
	 * Delay by ZAGHA_UNIT_SERVICING_PERIOD_MS post close
	*/
	if (true == g_flags.serviceStackFlag) {
		// Only service Zagha Units every fixed period
		if (timeToServiceStack()) {
			debug_printf(LUMINROP, "Zagha Service...\r\n");
			g_timeSinceLastServiceMs = get_systick_ms();

			if (ZaghaController_timeToDaliPing()) {
				debug_printf(LUMINROP, "Zagha Dali Ping\r\n");
				DaliAL_ping(NULL);
			}

			serviceSetFadeTime();

			ZaghaController_checkState(state.driver_count, state.driver_address);

			ZaghaLamp_service(&state);

		}
	}
}

/*!
	\brief 		Reset ZaghaController to default state
*/
void ZaghaController_reset(void)
{
	memset(&state, 0, sizeof(state));

	g_timeSinceLastServiceMs = 0;
	g_flags.reinitialiseFlag = false;
	g_flags.setFadeTimeFlag = true;
	g_flags.serviceStackFlag = true;
	g_flags.initialiseDriverFlag = true;
	g_flags.initialiseLampFlag = true;
}

/*!
	\brief 		Initialise DALI stack

	\return 	true if DALI devices are detected and initialisation is successful
*/
bool ZaghaController_init(void)
{
	debug_printf(LUMINROP, "Zagha Init Start...\r\n");

	// Initialise the driver
	if (!resetDali())
	{
		return false;
	}

	initialiseDriver();
	initialiseLamp();

	debug_printf(LUMINROP, "Zagha Init Done...\r\n");

	g_flags.serviceStackFlag = true;
	g_flags.setFadeTimeFlag = false;
	return true;
}

/*!
	\brief 		Service stack
*/
void ZaghaController_service(void)
{
	// First check if the stack needs initialising
	if (serviceReinitialise()) return;

	// Skip stack service if in power failure state or relay open.
	if(	(get_sysmon_last_gasp_is_active() == true) || 
		(b_get_relay_state() == RELAY_OPEN))
	{
		g_timeSinceLastServiceMs = get_systick_ms();
	}
	else {
		serviceStack();
	}

	return;
}

/*!
	\brief 		Update lamp output brightness level
*/
bool ZaghaController_updateLampOutput(uint8_t level)
{
	// Return false if value > 100% brightness
	if( level > 100)
	{
		debug_printf( LUMINROP, "Brightness > 100\r\n");
		return false;
	}
	// Send Lamp unit next level to apply
	ZaghaLamp_updateTargetLevel(level);
	debug_printf(LUMINROP, "Zagha LOO = %d\r\n", level);
	// Force instant statck service to process without delay
	g_timeSinceLastServiceMs = (uint32_t)(get_systick_ms() - ZAGHA_UNIT_SERVICING_PERIOD_MS);
	g_timeSinceLastServiceMs += 500; // allow delay before sending DAPC for when relay just closed
	return true;
}

/*!
	\brief 		Get stack dataset
*/
void ZaghaController_getDataset(LuminrDataset * dataset)
{
	ZaghaDriver_Information driverInfo;
	ZaghaDriver_getDriverInfo(&driverInfo);

	memcpy(dataset->gtin, driverInfo.gtin, sizeof(dataset->gtin));
	memcpy(dataset->serno, driverInfo.serial, sizeof(dataset->serno));

	dataset->info.phy_min = driverInfo.phyMin;
	dataset->info.driver_and_lamp_type = DRIVER_TYPE_DALI;
}

/*!
	\brief 		Sets a flag to update the fade time on next service
*/
void ZaghaController_updateFadeTime(uint8_t fadeTime)
{
	g_flags.setFadeTimeFlag = true;
	g_fadeTime = fadeTime;
}

/*!
	\brief 		Request for whole Zagha stack to be initialised at next service call
*/
void ZaghaController_requestZaghaStackInit(void)
{
	g_flags.reinitialiseFlag = true;
}

/*!
	\brief 		Get status of init requested flag
*/
bool ZaghaController_getZaghaStackInitRequested(void)
{
	return g_flags.reinitialiseFlag;
}
