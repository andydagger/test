/*
==========================================================================
 Name        : ZaghaDriver.c
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/ZaghaDriver.c
 Author      : Chris BSW
 Copyright   : Lucy Zodion Ltd
 Created     : 14 Nov 2019
 Description :
==========================================================================
*/

#include <stdint.h>
#include <string.h>

#include "DaliDriver.h"
#include "DaliDriver_impl.h"
#include "DaliController.h"

static ZaghaDriver_Information localDriverInfo = {0};

/*!
	\brief 		Initialise the Driver module
*/
bool ZaghaDriver_init(struct dali_app_state *state) {
	bool ret = false;

	do {
		if (state == NULL) {
			break;
		}

		if (state->driver_count == 1) {
			if (ZaghaDriver_readGTIN(&localDriverInfo) != ZAGHA_DRIVER_RET_OK) {
				break;
			}

			if (ZaghaDriver_readSerial(&localDriverInfo) != ZAGHA_DRIVER_RET_OK) {
				break;
			}
		}

		if (ZaghaDriver_readPhyMin(state, &localDriverInfo.phyMin) != ZAGHA_DRIVER_RET_OK) {
			break;
		}

		ret = true;
	} while (0);

	return ret;
}

/*!
	\brief 		Reinitialise the Driver module, once the driver is unlocked
*/
bool ZaghaDriver_reinit(struct dali_app_state *state) {
	return ZaghaDriver_init(state);
}

/*!
	\brief 		Copy the driver info into the provided pointer
*/
void ZaghaDriver_getDriverInfo(ZaghaDriver_Information *p_info) {
	if (p_info == NULL) {
		return;
	}

	memcpy(p_info, &localDriverInfo, sizeof(localDriverInfo));
}
