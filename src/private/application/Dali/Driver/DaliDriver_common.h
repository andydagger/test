/*
==========================================================================
 Name        : ZaghaDriver_common.h
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/Driver/ZaghaDriver_common.h
 Author      : Chris BSW
 Copyright   : Lucy Zodion Ltd
 Created     : 14 Nov 2019
 Description :
==========================================================================
*/

#ifndef PRIVATE_APPLICATION_ZAGHA_DRIVER_ZAGHADRIVER_COMMON_H_
#define PRIVATE_APPLICATION_ZAGHA_DRIVER_ZAGHADRIVER_COMMON_H_

#include <stdint.h>

typedef enum
{
    ZAGHA_DRIVER_RET_OK,
	ZAGHA_DRIVER_RET_ERR
} ZAGHA_DRIVER_RET;

typedef struct
{
	uint8_t gtin[6];
	uint8_t serial[8];
	uint8_t phyMin;	//Physical minimum
} DALI_DRIVER;

typedef DALI_DRIVER ZaghaDriver_Information;


#endif //PRIVATE_APPLICATION_ZAGHA_DRIVER_ZAGHADRIVER_COMMON_H_
