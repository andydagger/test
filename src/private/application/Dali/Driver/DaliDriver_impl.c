/*
==========================================================================
 Name        : ZaghaDriver_impl.c
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/ZaghaDriver_impl.c
 Author      : Chris BSW
 Copyright   : Lucy Zodion Ltd
 Created     : 14 Nov 2019
 Description :
==========================================================================
*/

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "DaliDriver_impl.h"
#include "DaliController.h"
#include "DaliAL.h"
#include "common.h"

//Zagha Driver Information memory bank 0
#define ZD_MB_INFO				0
//GTIN Address within MB 0
#define ZD_ADDR_GTIN			3
//Serial Address within MB 0
#define ZD_ADDR_SERIAL			11
//Firmware Version Address within MB 0
#define ZD_ADDR_FW_VERSION							0x09
//Hardware Version Address within MB 0
#define ZD_ADDR_HW_VERSION							0x13
//101 Version Address within MB 0
#define ZD_ADDR_101_VERSION							0x15
//102 Version Address within MB 0
#define ZD_ADDR_102_VERSION							0x16
//103 Version Address within MB 0
#define ZD_ADDR_103_VERSION							0x17

//Zagha Driver Extended Device Information memory bank 50
#define ZD_MB_EXT_INFORMATION						0x32
//SR Version address within MB 50
#define ZD_ADDR_SR_VERSION							0x08

static bool readMemoryLocation(uint8_t bank, uint8_t address, uint8_t *memory) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	do {

		DaliAL_dataTransferRegister1(bank, &status);
		if (status != DALI_BUS_STATUS_OK) {
			break;
		}

		DaliAL_dataTransferRegister0(address, &status);
		if (status != DALI_BUS_STATUS_OK) {
			break;
		}

		*memory = DaliAL_readMemoryLocation(DALI_ADDRESS_IGNORE, &status);
	} while (0);

	return (status == DALI_BUS_STATUS_OK);
 }

/*!
	\brief 		Read the GTIN
*/
ZAGHA_DRIVER_RET ZaghaDriver_readGTIN(ZaghaDriver_Information *p_info) {
	if (p_info == NULL) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	ZAGHA_DRIVER_RET ret = ZAGHA_DRIVER_RET_OK;

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	do {
		DaliAL_dataTransferRegister1(ZD_MB_INFO, &status);
		if (status != DALI_BUS_STATUS_OK) {
			ret = ZAGHA_DRIVER_RET_ERR;
			break;
		}

		DaliAL_dataTransferRegister0(ZD_ADDR_GTIN, &status);
		if (status != DALI_BUS_STATUS_OK) {
			ret = ZAGHA_DRIVER_RET_ERR;
			break;
		}

		for (uint8_t i = 0; i < sizeof(p_info->gtin); i++) {
			p_info->gtin[i] = DaliAL_readMemoryLocation(DALI_ADDRESS_IGNORE, &status);
			if (status != DALI_BUS_STATUS_OK) {
				ret = ZAGHA_DRIVER_RET_ERR;
				break;
			}
		}
	} while (0);

	return ret;
}

/*!
	\brief 		Read the Serial number
*/
ZAGHA_DRIVER_RET ZaghaDriver_readSerial(ZaghaDriver_Information* p_info) {
	if (p_info == NULL) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_dataTransferRegister1(ZD_MB_INFO, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	DaliAL_dataTransferRegister0(ZD_ADDR_SERIAL, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	for (size_t i = 0; i < sizeof(p_info->serial); i++) {
		p_info->serial[i] = DaliAL_readMemoryLocation(DALI_ADDRESS_IGNORE, &status);

		if (status != DALI_BUS_STATUS_OK) {
			return ZAGHA_DRIVER_RET_ERR;
		}
	}

	return ZAGHA_DRIVER_RET_OK;
}


/*!
	\brief 		Read the Physical minimum that the driver can support
	\details 	Search all known drivers, and report the accumulated highest minimum value
*/
ZAGHA_DRIVER_RET ZaghaDriver_readPhyMin(struct dali_app_state *state, uint8_t *minimum) {
	ZAGHA_DRIVER_RET ret = ZAGHA_DRIVER_RET_OK;

	uint8_t maxmin = 0;

	do {
		dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

		if (state == NULL) {
			ret = ZAGHA_DRIVER_RET_ERR;
			break;
		}

		for (uint8_t i = 0; i < state->driver_count; i++) {
			uint8_t localmin = DaliAL_queryPhysicalMinimum(state->driver_address[i], &status);

			if (status != DALI_BUS_STATUS_OK) {
				ret = ZAGHA_DRIVER_RET_ERR;
				break;
			}

			if (localmin > maxmin) {
				maxmin = localmin;
			}
		}
	} while (0);

	if (minimum != NULL) {
		*minimum = maxmin;
	}

	return ret;
}

/*!
	\brief 		Get the 101 version number of the driver
*/
ZAGHA_DRIVER_RET ZaghaDriver_get101VersionInformation(uint8_t *p_major, uint8_t *p_minor) {
	uint8_t value;

	if (p_major == NULL || p_minor == NULL) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	if (!readMemoryLocation(ZD_MB_INFO, ZD_ADDR_101_VERSION, &value)) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	*p_major = value >> 2;	//high 6 bits is major
	*p_minor = value & 0x03; //low 2 bits is minor

	return ZAGHA_DRIVER_RET_OK;
}

/*!
	\brief 		Gets the 102 version number of the driver
*/
ZAGHA_DRIVER_RET ZaghaDriver_get102VersionInformation(uint8_t *p_major, uint8_t *p_minor) {
	uint8_t value;

	if (p_major == NULL || p_minor == NULL) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	if (!readMemoryLocation(ZD_MB_INFO, ZD_ADDR_102_VERSION, &value)) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	*p_major = value >> 2;	//high 6 bits is major
	*p_minor = value & 0x03; //low 2 bits is minor

	return ZAGHA_DRIVER_RET_OK;
}


/*!
	\brief 		Gets the 103 version number of the driver
*/
ZAGHA_DRIVER_RET ZaghaDriver_get103VersionInformation(uint8_t* p_major, uint8_t* p_minor)
{
	uint8_t value;
	if (NULL == p_major || NULL == p_minor)
	{
		return ZAGHA_DRIVER_RET_ERR;
	}
	if (!readMemoryLocation(ZD_MB_INFO, ZD_ADDR_103_VERSION, &value))
	{
		return ZAGHA_DRIVER_RET_ERR;
	}
	*p_major = value >> 2;	//high 6 bits is major
	*p_minor = value & 0x03; //low 2 bits is minor
	return ZAGHA_DRIVER_RET_OK;
}

/*!
	\brief 		Gets the Firmware version number of the driver
*/
ZAGHA_DRIVER_RET ZaghaDriver_getFirmwareVersionInformation(uint8_t *p_major, uint8_t *p_minor) {
	if (NULL == p_major || NULL == p_minor) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_dataTransferRegister1(ZD_MB_INFO, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	DaliAL_dataTransferRegister0(ZD_ADDR_FW_VERSION, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	*p_major = DaliAL_readMemoryLocation(DALI_ADDRESS_IGNORE, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	*p_minor = DaliAL_readMemoryLocation(DALI_ADDRESS_IGNORE, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	return ZAGHA_DRIVER_RET_OK;
}

/*!
	\brief 		Gets the Hardware version number of the driver
*/
ZAGHA_DRIVER_RET ZaghaDriver_getHardwareVersionInformation(uint8_t *p_major, uint8_t *p_minor)
{
	if (p_major == NULL || p_minor == NULL) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_dataTransferRegister1(ZD_MB_INFO, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	DaliAL_dataTransferRegister0(ZD_ADDR_HW_VERSION, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	*p_major = DaliAL_readMemoryLocation(DALI_ADDRESS_IGNORE, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	*p_minor = DaliAL_readMemoryLocation(DALI_ADDRESS_IGNORE, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	return ZAGHA_DRIVER_RET_OK;
}

/*!
	\brief 		Gets the SR version number of the driver
*/
ZAGHA_DRIVER_RET ZaghaDriver_getSRVersionInformation(uint8_t *p_major, uint8_t *p_minor)
{
	if (p_major == NULL || p_minor == NULL) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_dataTransferRegister1(ZD_MB_EXT_INFORMATION, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	DaliAL_dataTransferRegister0(ZD_ADDR_SR_VERSION, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	*p_major = DaliAL_readMemoryLocation(DALI_ADDRESS_IGNORE, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	*p_minor = DaliAL_readMemoryLocation(DALI_ADDRESS_IGNORE, &status);
	if (status != DALI_BUS_STATUS_OK) {
		return ZAGHA_DRIVER_RET_ERR;
	}

	return ZAGHA_DRIVER_RET_OK;
}
