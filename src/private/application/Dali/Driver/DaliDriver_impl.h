/*
==========================================================================
 Name        : ZaghaDriver_impl.h
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/ZaghaDriver_impl.h
 Author      : Chris BSW
 Copyright   : Lucy Zodion Ltd
 Created     : 14 Nov 2019
 Description :
==========================================================================
*/

#ifndef PRIVATE_APPLICATION_ZAGHA_ZAGHADRIVER_IMPL_H_
#define PRIVATE_APPLICATION_ZAGHA_ZAGHADRIVER_IMPL_H_

#include <stdint.h>

#include "DaliDriver_common.h"

#include "DaliController.h"

ZAGHA_DRIVER_RET ZaghaDriver_readGTIN(ZaghaDriver_Information* p_info);
ZAGHA_DRIVER_RET ZaghaDriver_readSerial(ZaghaDriver_Information* p_info);
ZAGHA_DRIVER_RET ZaghaDriver_readPhyMin(struct dali_app_state *state, uint8_t *minimum);
ZAGHA_DRIVER_RET ZaghaDriver_get101VersionInformation(uint8_t *p_major, uint8_t *p_minor);
ZAGHA_DRIVER_RET ZaghaDriver_get102VersionInformation(uint8_t *p_major, uint8_t *p_minor);
ZAGHA_DRIVER_RET ZaghaDriver_get103VersionInformation(uint8_t *p_major, uint8_t *p_minor);
ZAGHA_DRIVER_RET ZaghaDriver_getFirmwareVersionInformation(uint8_t *p_major, uint8_t *p_minor);
ZAGHA_DRIVER_RET ZaghaDriver_getHardwareVersionInformation(uint8_t *p_major, uint8_t *p_minor);
ZAGHA_DRIVER_RET ZaghaDriver_getSRVersionInformation(uint8_t *p_major, uint8_t *p_minor);

#endif /* PRIVATE_APPLICATION_ZAGHA_ZAGHADRIVER_IMPL_H_ */
