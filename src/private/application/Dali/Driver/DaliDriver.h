/*
==========================================================================
 Name        : ZaghaDriver.h
 Project     : pcore
 Path        : /pcore/src/private/application/Zagha/ZaghaDriver.h
 Author      : Chris BSW
 Copyright   : Lucy Zodion Ltd
 Created     : 14 Nov 2019
 Description :
==========================================================================
*/

#ifndef PRIVATE_APPLICATION_ZAGHA_ZAGHADRIVER_H_
#define PRIVATE_APPLICATION_ZAGHA_ZAGHADRIVER_H_

#include <stdint.h>
#include <stdbool.h>

#include "DaliController.h"
#include "DaliDriver_common.h"

bool ZaghaDriver_init(struct dali_app_state *state);
bool ZaghaDriver_reinit(struct dali_app_state *state);
void ZaghaDriver_getDriverInfo(ZaghaDriver_Information *p_info);

#endif /* PRIVATE_APPLICATION_ZAGHA_ZAGHADRIVER_H_ */
