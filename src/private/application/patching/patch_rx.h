/*
==========================================================================
 Name        : fuota.h
 Project     : pcore
 Path        : /pcore/src/private/application/fw_update/fuota.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 9 Feb 2018
 Description :
==========================================================================
*/


#ifndef PRIVATE_APPLICATION_PATCHING_PATCH_RX_H_
#define PRIVATE_APPLICATION_PATCHING_PATCH_RX_H_

#include "chip.h"
#include "integer.h"
#include "ff.h"
#include "config_queues.h"

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
typedef struct
{
    uint32_t patch_crc;
    uint32_t patch_length_bytes;
    uint32_t patch_pckts;
    uint32_t new_file_crc;
    uint32_t new_file_length;
    uint32_t patch_file_size;
    uint32_t response_delay_sec;
    uint32_t rx_pkt_list_length_bytes;
}patch_information;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
bool patch_rx_process_msg(QuPatchDataMsg patch_msg);

bool get_patch_download_is_in_progess(void);
void patch_rx_remote_kill(void);
FIL * get_patch_file_ptr(void);
bool get_patch_rx_patch_is_pending(void);
char * get_patch_rx_file_name(void);


#endif /* PRIVATE_APPLICATION_PATCHING_PATCH_RX_H_ */
