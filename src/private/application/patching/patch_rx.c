/*
==========================================================================
 Name        : fuota.c
 Project     : pcore
 Path        : /pcore/src/private/application/fw_update/fuota.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 9 Feb 2018
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/
#include "chip.h"
#include "stdbool.h"
#include "stdint.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "common.h"
#include "systick.h"
#include "debug.h"
#include "sysmon.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "FreeRTOSMacros.h"
#include "config_network.h"
#include "config_fatfs.h"
#include "config_patch.h"
#include "ff.h"
#include "FreeRTOSCommonHooks.h"
#include "patch_process.h"
#include "led.h"
#include "config_queues.h"
#include "patch_rx.h"
#include "file_sys.h"
#include "nwk_com.h"
#include "compileUplink.h"

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static bool patch_rx_start(void);
static bool patch_rx_content(void);
static bool patch_rx_retry(void);
static bool patch_rx_terminate(void);
static bool patch_rx_kill(void);

static bool create_patch_file(void);
static bool write_content_to_patch_file(void);
static bool check_patch_against_crc(void);

static void notify_application(void);
static patch_information flip_patch_info(patch_information source);
static void report_error(uint32_t error_code);

static bool rpl_reset(void);
static bool rpl_set_pcp( uint32_t pcp_number);
static bool rpl_report(void);
static bool rpl_get_next_missed_pcp(uint16_t from_pcp, uint16_t * p_next_missed_pcp);
static bool rpl_check_if_all_received(bool * b_all_pcp_received);


/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
typedef struct
{
    uint8_t msg_type;
    bool (*func)(void);
    bool b_available_before_fut;
}patch_rx_routing;

static const patch_rx_routing fuota_msg_matrix[] =
{
    { PATCH_RX_FUT,  patch_rx_start,    true},
    { PATCH_RX_PCP,  patch_rx_content,  false},
    { PATCH_RX_PRT,  patch_rx_retry,    false},
    { PATCH_RX_PTP,  patch_rx_terminate,false},
    { PATCH_RX_CUT,  patch_rx_start,    true}

};

static patch_information patch_info;
static bool b_all_packets_downloaded = false;
static bool b_downloading_patch = false;
static bool b_patch_is_pending = false;
static FIL patch_file;

/*
 * p_rx_packet points to the oldest message in the xQueuePatchRx queue that was
 * "received" (read) and is temporarily stored by vTaskPatchRx
 * vTaskPatchRx will only process one packet at a time , thus the data pointed
 * to will remain unchanged until the return from patch_rx_process_msg()
 */
static QuPatchDataMsg rx_packet;

static const char * patch_file_name = NULL;

static UINT f_wr_bytes = 0; // not used by required to avoid passing NULL pointer
static UINT f_rd_bytes = 0; // not used by required to avoid passing NULL pointer


static bool patch_awaiting_processing = false;
static bool b_rpl_report_sent = false;
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
QueueHandle_t XQuPatchData = NULL;
extern QueueHandle_t xQuNwkCom; // vTaskNwkCom
extern QueueHandle_t xQuProcessPatch; // vTaskProcessPatch

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Return patch file ptr
 * @param[in] void
 * @return ptr to patch file
 *******************************************************************************/
FIL * get_patch_file_ptr(void){
    return &patch_file;
}

/*******************************************************************************
 * @brief Process received patch messages
 * @param[in] void
 * @return void
 *******************************************************************************/
bool patch_rx_process_msg(QuPatchDataMsg patch_rx_msg){
    uint8_t msg_type_id = 0;

    // save msg locally
    rx_packet = patch_rx_msg;


    // call the corresponding function to process msg
    while (msg_type_id < matrix_n_rows(fuota_msg_matrix)) {
        if (rx_packet.msg_type == fuota_msg_matrix[msg_type_id].msg_type) {
            // check if the function shall be called
            if( get_patch_download_is_in_progess() == false
                &&
                fuota_msg_matrix[msg_type_id].b_available_before_fut == false){
                // return true as no need to kill process - just ignore
                return true;
            }
            // return whatever the function returns
            return (*fuota_msg_matrix[msg_type_id].func)();
        } else {}
        msg_type_id++;
    }

    report_error(PATCH_MSG_ID);
    return false;
}

/*******************************************************************************
 * @brief Kick-off patch data reeption
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool patch_rx_start(void){
    char * p_rx_data = NULL;
    char **p_p_rx_data = &p_rx_data;
    uint8_t varint_offset = 0;

    /*
     * Received patch file download trigger
     *
     * NOTE:
     * It is allowed to have patch_rx_status != PATCH_RX_IDLE
     * at this stage > cloud decide to start from scratch
     */

    // Select the patch file name accordingly
    if( rx_packet.msg_type == PATCH_RX_FUT)
    {
        patch_file_name = (const char *)FW_PATCH_FILE_NAME;
    }else{
        patch_file_name = (const char *)CAL_PATCH_FILE_NAME;
    }

    // Parse patch info

    // initialise ptr to start of content data
    p_rx_data = rx_packet.data;

    // patch file CRC
    patch_info.patch_crc = common_array_to_uint32(p_p_rx_data);
    catch_test_report( PARSE_PATCH_CRC_NULL, patch_info.patch_crc == 0);


    // patch length in bytes varint (received as number of 32-bit words
    patch_info.patch_length_bytes = 4 * common_varint_to_uint32(
                                            p_p_rx_data,
                                            &varint_offset);
    catch_test_report( PARSE_PATCH_LENGTH_NULL, patch_info.patch_length_bytes
                                                == 0);
    catch_test_report( PARSE_PATCH_LENGTH_OVER, patch_info.patch_length_bytes
                                                > PATCH_MAX_LENGTH_BYTES);

    // Use first BYTES_PER_PKT for patch info: CRC, length...
    // and PATCH_MAX_PACKETS for RPL storage
    patch_info.patch_file_size =    RPL_STORAGE_SIZE +
                                    PATCH_INFO_SIZE +
                                    patch_info.patch_length_bytes;

    // number of packets making up patch
    patch_info.patch_pckts = common_varint_to_uint32(   p_p_rx_data,
                                                        &varint_offset);
    catch_test_report( PARSE_PATCH_PKTS_NULL,   patch_info.patch_pckts == 0);
    catch_test_report( PARSE_PATCH_PKTS_OVER,   patch_info.patch_pckts
                                                > PATCH_MAX_PACKETS);

    // image content crc
    patch_info.new_file_crc = common_array_to_uint32(p_p_rx_data);
    catch_test_report( PARSE_FILE_CRC_NULL, patch_info.new_file_crc == 0);

    // image length in bytes varint
    patch_info.new_file_length = 4 *    common_varint_to_uint32(
                                            p_p_rx_data,
                                            &varint_offset);
    catch_test_report( PARSE_FILE_SIZE_NULL,    patch_info.new_file_length
                                                == 0);
    catch_test_report( PARSE_FILE_SIZE_OVER,    patch_info.new_file_length
                                                > FILE_MAX_LENGTH_BYTES);

    // last packet to rpl delay
    patch_info.response_delay_sec = common_varint_to_uint32(
                                        p_p_rx_data,
                                        &varint_offset);
    catch_test_report( RPL_DELAY,   patch_info.response_delay_sec
                                    > MAX_RPL_DELAY);

    // create and open new patch file.
    catch_error_fail( create_patch_file() );

    // generate list of packet received flags.
    catch_error_fail( rpl_reset() );

    // Initiate mcast session
    set_nwk_com_broadcast_session_should_be_active( true );

    b_downloading_patch = true;
    patch_awaiting_processing = false;
    b_rpl_report_sent = false;
    b_patch_is_pending = false;

    debug_printf(PATCH_RX, "patch rx started!\r\n");

    return true;
}


/*******************************************************************************
 * @brief Received patch content data
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool patch_rx_content(void){

    b_rpl_report_sent = false;

    // check pkt number is valid
    catch_test_report( PKT_NUM_OVER,
                        rx_packet.pkt_number > patch_info.patch_pckts);

    // write content to patch file
    catch_error_fail( write_content_to_patch_file() );

    // update flag in rpl
    catch_error_fail( rpl_set_pcp(rx_packet.pkt_number) );

    debug_printf(PATCH_RX, "saved pkt %lu!\r\n", rx_packet.pkt_number);

    return true;
}


/*******************************************************************************
 * @brief Restart patch data reception
 * @param[in] void
 * @return true if processed successfully
 *******************************************************************************/
static bool patch_rx_retry(void){
    debug_printf(PATCH_RX, "No longer used - ignored.\r\n");
    return true;
}


/*******************************************************************************
 * @brief Terminate patch data reception
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool patch_rx_terminate(void) {

    // check if all packets have been received
    catch_error_fail( rpl_check_if_all_received( &b_all_packets_downloaded ) );

    // if all packet shave been received - verify patch file integrity
    if (b_all_packets_downloaded == true) {

        debug_printf(PATCH_RX, "terminated - all packets received.\r\n");

        // check patch crc
        catch_error_fail( check_patch_against_crc() );

        // close file
        catch_file_error_report( CLOSE_PATCH_RX, f_close(&patch_file) );

        // update download session flag
        b_downloading_patch = false;

        //  leave mcast session
        set_nwk_com_broadcast_session_should_be_active(false);

        // if this point is reached then patch received and verified
        notify_application();

    } else {

        debug_printf(PATCH_RX, "terminated - not all received.\r\n");

        // First wait for delay as instructed by cloud
//        sys_delay_ms((uint32_t)(patch_info.response_delay_sec*1000));
//        FreeRTOSDelay(patch_info.response_delay_sec*1000);


        // send rpl report to cloud
        if(b_rpl_report_sent == false){
            catch_error_fail( rpl_report() );

//            block_and_print_patch_file();

            // latch so as to only send once
            b_rpl_report_sent = true;
        }else{
            debug_printf(PATCH_RX, "rpl report already sent.\r\n");
        }
    }

    return true;
}

/*******************************************************************************
 * @brief Notify Application that a valid patch is waiting to be processed.
 * @param[in] void
 * @return void
 *******************************************************************************/
static void notify_application(void){
    b_patch_is_pending = true;
    return;
}

bool get_patch_rx_patch_is_pending(void){
    return b_patch_is_pending;
}

char * get_patch_rx_file_name(void){
    return (char *)patch_file_name;
}

/*******************************************************************************
 * @brief Kills the whole patch download sequence
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool patch_rx_kill(void){
    f_close(&patch_file);
    f_unlink(patch_file_name);
    set_nwk_com_broadcast_session_should_be_active( false );
    b_downloading_patch = false;
    b_patch_is_pending = false;
    return true;
}

void patch_rx_remote_kill(void){
    patch_rx_kill();
    return;
}

/*******************************************************************************
 * @brief Create and oen new patch file
 * @param[in] void
 * @return true if patch file create and info written successfully
 *******************************************************************************/
static bool create_patch_file(void){
    patch_information flipped_patch_info;

    /*
     * Use f_expand to pre-allocate continuous data area in the drive
     * File length saved in: patch_info.patch_length_bytes
     * All packet (before last) will contain 45 bytes of patch data
     * First packet will be 1
     * >> Use first 45 bytes to save patch file info (CRC, length, date)
     *
     * If patch file already open: close and delete
     */

    debug_printf(PATCH_RX, "Create patch: %s \r\n", patch_file_name);

    // Delete existing patch file
    if( f_stat( patch_file_name , NULL) == FR_OK){
        debug_printf(PATCH_RX, "Patch file already exist -> delete\r\n");
        f_close(&patch_file); // cannot remove an open file
        catch_file_error_report( REMOVE_PATCH_FILE, f_unlink(patch_file_name));
    }else{} // NOOP: no file to remove.

    // Now no patch file exist, create new one and prepare for read/write access
    catch_file_error_report(    OPEN_PATCH_RX,
                                f_open(    &patch_file,
                                            patch_file_name ,
                                            FA_OPEN_ALWAYS|FA_READ|FA_WRITE ));
    // Prepare and allocate required memory area for expected patch size
    catch_file_error_report(    EXPAND_PATCH,
                                f_expand(   &patch_file,
                                            (FSIZE_t)(patch_info.patch_file_size),
                                            1 ));


    flipped_patch_info = flip_patch_info(patch_info);
    // write patch info in first BYTES_PER_PKT bytes of file.

    // adjust write pointer
    catch_file_error_report(SCROLL_PATCH_RX,
                            f_lseek( &patch_file, RPL_STORAGE_SIZE));

    catch_file_error_report(    WRITE_PATCH_INFO,
                                f_write(    &patch_file,
                                            (uint32_t *)&flipped_patch_info,
                                            sizeof(patch_info),
                                            &f_wr_bytes));
    // patch file create and info written successfully
    return true;
}



/*******************************************************************************
 * @brief Flip bytes of each parameter in patch_info struct
 * @param[in] void
 * @return flipped patch_info struct
 *******************************************************************************/
static patch_information flip_patch_info(patch_information source){
    patch_information flipped;
    flipped.patch_crc = flip_uint32(patch_info.patch_crc);
    flipped.patch_length_bytes = flip_uint32(patch_info.patch_length_bytes);
    flipped.patch_pckts = flip_uint32(patch_info.patch_pckts);
    flipped.patch_file_size = flip_uint32(patch_info.patch_file_size);
    flipped.new_file_crc = flip_uint32(patch_info.new_file_crc);
    flipped.new_file_length = flip_uint32(patch_info.new_file_length);
    flipped.response_delay_sec = flip_uint32(patch_info.response_delay_sec);
    flipped.rx_pkt_list_length_bytes = flip_uint32(patch_info.rx_pkt_list_length_bytes);
    return flipped;
}

/*******************************************************************************
 * @brief Write to patch content file
 * @param[in] void
 * @return true if patch content written to file successfully
 *******************************************************************************/
static bool write_content_to_patch_file(void){
    FSIZE_t packet_content_write_offset = 0;
    /*
     * Use f_lseek and a comination of 45 bytes * packet number to select
     * where to write the data into the file.
     * Extra shift by PATCH_MAX_PACKETS to go past RPL
     * All packet (before last) will contain 45 bytes of patch data
     */

    // calculate content write ptr offset - past rpl storage
    packet_content_write_offset = (FSIZE_t)(RPL_STORAGE_SIZE +
                                            rx_packet.pkt_number * BYTES_PER_PKT);

    // check for potential write overflow
    catch_test_report(  PATCH_WRITE_OVERFLOW,
                        (packet_content_write_offset + rx_packet.content_byte_count)
                        > patch_info.patch_file_size);

    // adjust write pointer
    catch_file_error_report(SCROLL_PATCH_RX,
                            f_lseek( &patch_file, packet_content_write_offset));

    // Write packet content to file
    catch_file_error_report(WRITE_PATCH,
                            f_write(&patch_file,
                                    (char *)rx_packet.data,
                                    (UINT)rx_packet.content_byte_count,
                                    &f_wr_bytes));

    // content written to file successfully
    return true;
}


/*******************************************************************************
 * @brief Return the status of the patch download
 * @param[in] void
 * @return bool with true dor download in progress
 *******************************************************************************/
bool get_patch_download_is_in_progess(void){
    return b_downloading_patch;
}

/*******************************************************************************
 * @brief Verify the patch content against its 32-bit CRC
 * @param[in] void
 * @return bool with true for crc check pass
 *******************************************************************************/
static bool check_patch_against_crc(void){
    uint32_t calc_crc = 0;
    static char check_buf[64] = {0};
    uint16_t next_check_len = 0;
    uint32_t bytes_left_to_check = 0;
    uint32_t read_pos = 0;


    const uint32_t *data;
    uint32_t words;

    // skip patch 45 bytes header
    read_pos = RPL_STORAGE_SIZE + BYTES_PER_PKT;

    // initialise CRC calc
    Chip_CRC_Init();
    Chip_CRC_UseDefaultConfig(CRC_POLY_CRC32);
    Chip_CRC_UseCRC32();

    // TODO: add check against provided length.

    // work out file length
    bytes_left_to_check = f_size(&patch_file) - BYTES_PER_PKT - RPL_STORAGE_SIZE;
    // check this matches patch_info
    catch_test_report(  CRC_PATCH_SIZE,
                        bytes_left_to_check != patch_info.patch_length_bytes);

    // loop crc calc one buf length at a time
    while( bytes_left_to_check > 0 )
    {
        // next time chech bytes length
        next_check_len =    (bytes_left_to_check > sizeof(check_buf)) ?
                            sizeof(check_buf) :
                            bytes_left_to_check;

        // adjust read ptr offset and read out data
        catch_file_error_report(    CRC_PATCH_SCROLL,
                                    f_lseek(&patch_file, read_pos));

        catch_file_error_report(    CRC_PATCH_READ,
                                    f_read( &patch_file,
                                            (char *)check_buf,
                                            next_check_len,
                                            &f_rd_bytes));

        // point at start of crc buffer
        data = (uint32_t *)check_buf;
        // calculate number of crc calc iteratuins
        words = (uint32_t)(next_check_len / 4);
        // execute crc calc
        while (words > 0) {
            Chip_CRC_Write32(*data);
            data++;
            words--;
        }

        // decrement total length to check by length of previous check
        bytes_left_to_check -= next_check_len;

        // shift the read position accordingly
        read_pos += next_check_len;
    }

    calc_crc = Chip_CRC_Sum();
    Chip_CRC_Deinit();

    debug_printf(PATCH_RX, "calc_crc = 0x%08X | read_crc = 0x%08X\r\n",
            calc_crc,
            patch_info.patch_crc);

    catch_test_report( PATCH_CRC, calc_crc != patch_info.patch_crc);

    return true;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*******************************************************************************
 * @brief Reset the RPL content with all 0
 * @param[in] void
 * @return false if any error occured
 ******************************************************************************/
static bool rpl_reset(void)
{
    FSIZE_t rpl_byte_pos = 0;
    char erase_byte_val = 0x00;

    debug_printf(PATCH_RX, "Reset RPL...\r\n");

    // file already open

    while (rpl_byte_pos < RPL_STORAGE_SIZE)
    {
        // start at byte 0
        catch_file_error_report(    RPL_RESET_SCROLL,
                                    f_lseek(&patch_file, rpl_byte_pos));

        // Overwrite with 0x00
        catch_file_error_report(RPL_RESET_ERASE,
                                f_write(    &patch_file,
                                            (char *)&erase_byte_val,
                                            (UINT)1,
                                            &f_wr_bytes));
        rpl_byte_pos++;
    }


    debug_printf(PATCH_RX, "Done for %d bytes.\r\n", rpl_byte_pos);

    return true;
}

/*******************************************************************************
 * @brief Set the flag for a given pcp number
 * @param[in] uint32_t pcp number
 * @return false if any error occured
 ******************************************************************************/
static bool rpl_set_pcp( uint32_t pcp_number){
    uint32_t rpl_n_byte = 0;
    uint32_t pckt_with_bitzero = 0;
    char rpl_byte_temp = 0;

    debug_printf(PATCH_RX, "Set rx flag for PCP #%lu...\r\n", pcp_number);

    // make sure pcp number is not too high
    catch_test_report(  RPL_SET_PCP_NUMBER,
                        pcp_number > patch_info.patch_pckts);

    // offset past bit 0
    pckt_with_bitzero = pcp_number + 1;
    // work out byte number
    rpl_n_byte =   (pckt_with_bitzero / 8);
    if( pckt_with_bitzero % 8 ) rpl_n_byte++;

    // update packet flag
    //p_rx_pkt_list[rpl_n_byte - 1] |=  1 << (pkt_number % 8);

    // scroll in position
    catch_file_error_report(    RPL_UPDATE_SCROLL,
                                f_lseek(&patch_file, rpl_n_byte - 1));
    // read current byte value
    catch_file_error_report(    RPL_UPDATE_READ,
                                f_read( &patch_file,
                                        &rpl_byte_temp,
                                        (UINT)1,
                                        &f_rd_bytes));
//    debug_printf(PATCH_RX,  "read byte #%lu = %02X\r\n",
//                            rpl_n_byte - 1,
//                            rpl_byte_temp);
    // edit byte and write back
    rpl_byte_temp |=  1 << (pcp_number % 8);

    // scroll in position
    catch_file_error_report(    RPL_UPDATE_SCROLL,
                                f_lseek(&patch_file, rpl_n_byte - 1));

    catch_file_error_report(    RPL_UPDATE_WRITE,
                                f_write(    &patch_file,
                                            (char *)&rpl_byte_temp,
                                            (UINT)1,
                                            &f_wr_bytes));

//    debug_printf(PATCH_RX, "write = %02X\r\n", rpl_byte_temp);

//    debug_printf(PATCH_RX, "Done.\r\n");
    return true;
}

/*******************************************************************************
 * @brief Compile the RPL uplink to send to the cloud
 * @param[in] void
 * @return false if any error occured
 ******************************************************************************/
static bool rpl_report(void){
    uint16_t next_missed_pcp = 1;
    uint8_t rpl_report_buffer_pos = 0;
    uint8_t encoded_pcp_number[5] = {0};
    uint8_t encoded_output_size = 0;
    char payload[UP_MSG_PAYLOAD_ASCII_WITH_TERM] = {0};

    // load msg content
    rpl_report_buffer_pos = 6;

    while(1){
        rpl_get_next_missed_pcp(next_missed_pcp, &next_missed_pcp);
        if(next_missed_pcp == 0){
            break;
        }
//        debug_printf(PATCH_RX, "Missing PCP #%lu\r\n", next_missed_pcp);

        encoded_output_size = common_uint16_varint( next_missed_pcp,
                                                    encoded_pcp_number);

//        debug_printf(   PATCH_RX,
//                        "Encoded output size = %d\r\n",
//                        encoded_output_size);

        encoded_pcp_number[encoded_output_size] = 0;

//        debug_printf(   PATCH_RX,
//                        "Encoded output %02X%02X%02X\r\n",
//                        encoded_pcp_number[0],
//                        encoded_pcp_number[1],
//                        encoded_pcp_number[2]);

        // make sure next encoded pcp can fit in rpl report payload
        if( (rpl_report_buffer_pos + encoded_output_size * 2) >
        // 6 bytes already used by DA0101
                (UP_MSG_PAYLOAD_ASCII_WITH_TERM - 6 - 1)){
            break;
        }

        common_raw_to_ascii(    encoded_pcp_number,
                                (char *)&payload[rpl_report_buffer_pos],
                                encoded_output_size);

        rpl_report_buffer_pos += encoded_output_size * 2;

        // start looking from following pcp
        next_missed_pcp++;
    }

    compileUplink_MissedPackets(patch_info.response_delay_sec, payload);

    return true;
}

/*******************************************************************************
 * @brief Scan through the RPL bit by bit and return the next missed PCP
 * @param[in] uint16_t starting pcp position, uint16_t following missed pcp
 * @return false if any error occured
 ******************************************************************************/
static bool rpl_get_next_missed_pcp(uint16_t from_pcp, uint16_t * p_next_missed_pcp){
    uint32_t pckt_id = 0;
    uint32_t rpl_n_byte = 0;
    uint32_t pckt_with_bitzero = 0;
    char rpl_byte_temp = 0;

    // scann until last packet id reached
    pckt_id = from_pcp;
    while( pckt_id <= patch_info.patch_pckts){

        // offset for crc bit0
        pckt_with_bitzero = pckt_id + 1;

        // work out byte number
        rpl_n_byte =   (pckt_with_bitzero / 8);
        if( pckt_with_bitzero % 8 ) rpl_n_byte++;

        // scroll in position
        catch_file_error_report(    RPL_CHECK_SCROLL,
                                    f_lseek(&patch_file, rpl_n_byte - 1));

        // read current byte value
        catch_file_error_report(    RPL_UPDATE_READ,
                                    f_read( &patch_file,
                                            &rpl_byte_temp,
                                            1,
                                            &f_rd_bytes));

        if( rpl_byte_temp & 1 << (pckt_id % 8)){
            // flag was set so check next one along
            pckt_id++;
        }else{
            *p_next_missed_pcp = (uint16_t)pckt_id;
            return true;
        }
    }

    // if we reach this stage all PCP have been checked
    *p_next_missed_pcp = 0;

    return true;
}


/*******************************************************************************
 * @brief Scan through the RPL bit by bit and to work out if any were missed
 * @param[in] ptr to result yes_no boolean
 * @return false if any error occured
 ******************************************************************************/
static bool rpl_check_if_all_received(bool * b_all_pcp_received){
    uint16_t next_missed_pcp = 0;

    catch_error_fail( rpl_get_next_missed_pcp( 1, &next_missed_pcp) );

    debug_printf(PATCH_RX, "First PCP missed was = %lu\r\n", next_missed_pcp);

    // Function will retunr 0 if no PCP were missed.
    *b_all_pcp_received = ( next_missed_pcp == 0 ) ? true: false;

    return true;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*******************************************************************************
 * @brief Report error to sysmon task
 * @param[in] caller id and error code
 * @return void
 *******************************************************************************/
static void report_error(uint32_t error_code){
	sysmon_report_error(PATCH_RX, error_code);
    return;
}

