/*
==========================================================================
 Name        : patch_process.c
 Project     : pcore
 Path        : /pcore/src/private/application/patching/patch_process.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 8 Mar 2018
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/
#include "chip.h"
#include "config_queues.h"
#include "patch_process.h"
#include "patch_rx.h"
#include "stdbool.h"
#include "stdint.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "common.h"
#include "systick.h"
#include "debug.h"
#include "sysmon.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "FreeRTOSMacros.h"
#include "FreeRTOSCommonHooks.h"
#include "config_network.h"
#include "config_fatfs.h"
#include "config_patch.h"
#include "ff.h"
#include "led.h"
#include "file_sys.h"
#include "nwk_com.h"
#include "compileUplink.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static FIL old_file;
static FIL * p_new_file;
static FIL * p_patch_file;
static const char * file_name_patch = NULL;
static const char * file_name_old = NULL;
static const char * file_name_new = NULL;
static patch_information patch_info;
static UINT f_wr_bytes = 0; // not used by required to avoid passing NULL pointer
static UINT f_rd_bytes = 0; // not used by required to avoid passing NULL pointer
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
QueueHandle_t xQuProcessPatch = NULL;

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static bool parse_patch_encoded_data(   FIL * p_source_file,
                                        uint32_t * p_patch_offset,
                                        uint32_t * p_prev_copy_offset,
                                        uint32_t * p_parsed_info);
static bool copy_data(  FIL * p_source_file,
                            uint32_t * p_read_pos,
                            FIL * p_target_file,
                            uint32_t copy_length,
                            bool b_insert);
static bool open_required_files(void);
static bool close_used_files(void);
static bool parse_patch_info(char * p_data);
static bool check_new_file_crc(void);
static void report_error(uint32_t error_code);
static bool check_for_new_fw_file_available(void);
static bool clear_boot_log(void);
static bool patch_process_apply(   const char * fil_name_old,
                            const char * fil_name_patch,
                            const char * fil_name_new);
/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief PCalled by RTOS to apply patch file
 * @param[in] name of patch file to apply
 * @return true for patch applied successfully or false if any error occured.
 *******************************************************************************/
bool patch_process_exe(const char * patch_name){

    // Is this a firmware patch?
    if(strcmp(  (const char *)patch_name,
                (const char *)FW_PATCH_FILE_NAME) == 0){
        return patch_process_apply( (const char *)FW_OLD_FILE_NAME,
                                    (const char *)FW_PATCH_FILE_NAME,
                                    (const char *)FW_NEW_FILE_NAME);
    }

    // Is this a calendar patch ?
    if(strcmp(  (const char *)patch_name,
                (const char *)CAL_PATCH_FILE_NAME) == 0){
        return patch_process_apply( (const char *)CAL_OLD_FILE_NAME,
                                    (const char *)CAL_PATCH_FILE_NAME,
                                    (const char *)CAL_NEW_FILE_NAME);
    }

    // Else not recognised
    return false;
}

/*******************************************************************************
 * @brief Process patch to generate new file
 * @param[in] file names ( old, patch, new )
 * @return true for patch applied successfully or false if any error occured.
 *******************************************************************************/
static bool patch_process_apply(    const char * fil_name_old,
                                    const char * fil_name_patch,
                                    const char * fil_name_new){

    // Patch processing related Vars
    uint32_t patch_offset = 0;
    uint32_t patch_file_length = 0;
    uint32_t copy_offset = 0;
    uint32_t insert_length = 0;
    uint32_t copy_length = 0;

    // copy file names locally
    file_name_old = fil_name_old;
    file_name_patch = fil_name_patch;
    file_name_new = fil_name_new;

    // stop now if new file name is not given
    catch_test_report( NEW_NAME, file_name_new == NULL);

    // acquire file ptr from patch_rx.c
    p_patch_file = get_patch_file_ptr();
    catch_test_report( PATCH_PTR, p_patch_file == NULL);

    // attempt to acquire new file ptr from file_sys.c spare file
    // Taks will time-out after TASK_EXE_TIMEOUT_SEC = 2 min if file not
    // release by other task
    p_new_file = get_file_sys_file_ptr();
    while( p_new_file == 0 ){
        // wait 10 sec between checks
        FreeRTOSDelay(10000);
        p_new_file = get_file_sys_file_ptr();
    }

    // if we are doing a firmware update - create fresh backup of image
    if( strcmp( (const char *)file_name_old,
                (const char *)FW_OLD_FILE_NAME) == 0){
        catch_error_fail( file_sys_create_initial_ref_image() );
    }

    /**
     * Loop over the patch file. The format is:
     * 1. Insert length VarInt
     * 2. The actual bytes for the insert (if previous value wasn't 0)
     * 3. Copy source index VarInt
     * 4. Copy source length VarInt
     *
     * Then back to 1 and repeat until the patch is processed.
     */

    // open all files required
    catch_error_fail( open_required_files() );

    // adjust the patch read ptr to skip the rpl storage and patch_info data
    patch_offset = RPL_STORAGE_SIZE + BYTES_PER_PKT;
    patch_file_length = patch_info.patch_file_size;

    debug_printf(PATCH_PRO, "files opened - starting insert/copy loop.\r\n");

    /*
     * INSERT - COPY LOOP
     */
    while( patch_offset < patch_file_length )
    {
        // ----------------------- INSERT ----------------------------

        // Parse insert length
        catch_error_break(  parse_patch_encoded_data(   p_patch_file,
                                                        &patch_offset,
                                                        NULL,
                                                        &insert_length));


        // If length to insert is > 0 copy bytes from patch to new image.
        if( insert_length > 0 )
        {
            catch_error_break(  copy_data( p_patch_file,
                                            &patch_offset,
                                            p_new_file,
                                            insert_length,
                                            true ));
        }
        else{} // NOOP: noting to insert so carry-on


        // Check if more patch data is still to be processed, else exit loop.
        catch_test_break(patch_offset >= patch_file_length);

        // ----------------------- COPY ----------------------------
        // Parse copy offset
        catch_error_break(   parse_patch_encoded_data(  p_patch_file,
                                                        &patch_offset,
                                                        &copy_offset,
                                                        &copy_offset));


        // Check if more patch data is still to be processed, else exit loop.
        catch_test_break(patch_offset >= patch_file_length);

        // Parse copy length
        catch_error_break(   parse_patch_encoded_data(  p_patch_file,
                                                        &patch_offset,
                                                        NULL,
                                                        &copy_length));


        // Copy data from old image to new image
        catch_error_break(   copy_data( &old_file,
                                        &copy_offset,
                                        p_new_file,
                                        copy_length,
                                        false));

        debug_printf( PATCH_PRO, "PO = 0x%08X | IL = %lu | CO = %lu | CL = %lu\r\n",
                                    patch_offset,
                                    insert_length,
                                    copy_offset - BYTES_PER_PKT,
                                    copy_length);
    }

    // If loop exit (too soon) due to error close files and fail process
    catch_test_report( INS_CPY_UNCOMPLETE, patch_offset < patch_file_length );

    // check new file integrity using CRC provided in patch
    catch_error_report( NEW_FILE_CRC_FAIL, check_new_file_crc());

    // close all files
    catch_error_fail( close_used_files());

    debug_printf(PATCH_PRO, "patch applied - new file checked and closed.\r\n");

    // Process successful
    return true;
}

/*******************************************************************************
 * @brief Called by patch_process_apply to close all files
 * @param[in] see below
 * @return true if successful, false otherwise
 *******************************************************************************/
void patch_process_kill(void){
    close_used_files();
    debug_printf(PATCH_PRO, "Files Closed.\r\n");
    debug_printf(PATCH_PRO, "Deleting New Files... \r\n");
    f_unlink(FW_NEW_FILE_NAME);
    f_unlink(CAL_NEW_FILE_NAME);
    return;
}


/*******************************************************************************
 * @brief Parse length information from given source data file
 * @param[in] ptr to source file
 * @param[in] ptr to patchread offset
 * @param[in] ptr to fatfs op res
 * @param[in] ptre to previous copy address - if NULL parse length info
 * @param[in] ptr to parsing result
 * @return true if data parsed successfully
 *******************************************************************************/
static bool parse_patch_encoded_data(   FIL * p_source_file,
                                        uint32_t * p_patch_offset,
                                        uint32_t * p_prev_copy_offset,
                                        uint32_t * p_parsed_info){
    char varint_buf[5] = {0};
    char * p_length_data = NULL;
    char **p_p_length_data = &p_length_data;
    uint8_t varint_len_bytes = 0;
    int32_t zigzag_res = 0;

    /*
     * Parse the number of bytes to insert:
     * Read sufficient bytes into local buffer
     * Decode varint > insert_length
     */

    // update patch file read ptr before starting
    catch_file_error_report( SCROLL_PATCH, f_lseek( p_source_file,
                                                    *p_patch_offset));

    // read 5 bytes of patch data
    catch_file_error_report( READ_PATCH, f_read(p_source_file,
                                                varint_buf,
                                                5,
                                                &f_rd_bytes));

    // ptr to ptr used by lz_varint_to_ul()
    p_length_data = varint_buf;

    if(p_prev_copy_offset == NULL)
    {
        // parse varint length variable
        *p_parsed_info = common_varint_to_uint32(   p_p_length_data,
                                                    &varint_len_bytes);
    }else
    {
        // parse source copy offset variable
        zigzag_res = lz_decode_zigzag_varint( p_p_length_data, &varint_len_bytes);
        *p_parsed_info = (uint32_t)(zigzag_res + *p_prev_copy_offset);
    }

    // calculate the rolling patch read ptr offset
    *p_patch_offset += (uint32_t)varint_len_bytes;

    // Data parsed successfully
    return true;
}

/*******************************************************************************
 * @brief Copy bytes from source to target file & auto target-file ptr shift
 * @param[in] see below
 * @return true if successful, false otherwise
 *******************************************************************************/
static bool copy_data(  FIL * p_source_file, // ptr to source file
                        uint32_t * p_read_pos, // ptr to reading pos
                        FIL * p_target_file, // ptr to target file
                        uint32_t copy_length, // length to copy
                        bool b_insert){ // true for insert / false for copy

    static char copy_buf[64] = {0};
    uint16_t next_cpy_len = 0;
    uint32_t bytes_left_to_copy = copy_length;
    uint32_t read_pos = *p_read_pos;

    // process until no bytes left to insert/copy
    while( bytes_left_to_copy > 0 )
    {
        // next time copy? full copy_buf length or what's left over
        next_cpy_len =  (bytes_left_to_copy > sizeof(copy_buf)) ?
                        sizeof(copy_buf) :
                        bytes_left_to_copy;

        // make sure next read will not go over source EOF
        catch_test_report( SOURCE_OVERFLOW,     (read_pos + next_cpy_len) >
                                                f_size(p_source_file));

        // adjust read ptr offset and read out data
        catch_file_error_report( SCROLL_FILE, f_lseek(  p_source_file,
                                                        read_pos));
        catch_file_error_report( READ_FILE, f_read( p_source_file,
                                                    copy_buf,
                                                    next_cpy_len,
                                                    &f_rd_bytes));

        // make sure next write will not go over target EOF
        catch_test_report( TARGET_OVERFLOW, (f_tell(p_target_file) + next_cpy_len)
                                            > f_size(p_target_file));

        // paste data into new file and update write ptr
        catch_file_error_report( WRITE_FILE, f_write(   p_target_file,
                                                        copy_buf,
                                                        next_cpy_len,
                                                        &f_wr_bytes));

        // decrement total length to copy by length of previous copy
        bytes_left_to_copy -= next_cpy_len;

        // shift the read position accordingly
        read_pos += next_cpy_len;
    }

    // if operation was INSERT update p_read_pos
    if(b_insert == true){
        *p_read_pos = read_pos;
    }else{} // NOOP: operation was COPY so leave p_read_pos as is

    // copy/insert op was successful.
    return true;
}

/*******************************************************************************
 * @brief Open all files used for patch processing
 * @param[in] void
 * @return true for successful operations
 *******************************************************************************/
static bool open_required_files(void){
    static char patch_info_buffer[BYTES_PER_PKT] = {0};

    debug_printf(PATCH_PRO, "old: %s\r\n", file_name_old);
    debug_printf(PATCH_PRO, "patch: %s\r\n", file_name_patch);
    debug_printf(PATCH_PRO, "new: %s\r\n", file_name_new);

    // Open the old/reference image file and the patch file
    catch_file_error_report( OPEN_OLD, f_open(  &old_file,
                                                file_name_old,
                                                FA_READ ));

    catch_file_error_report( OPEN_PATCH, f_open(p_patch_file,
                                                file_name_patch,
                                                FA_READ ));

    // Extract patch file info located after RPL storage section
    catch_file_error_report( SCROLL_PATCH, f_lseek( p_patch_file,
                                                    RPL_STORAGE_SIZE));

    catch_file_error_report( READ_PATCH, f_read(p_patch_file,
                                                patch_info_buffer,
                                                BYTES_PER_PKT,
                                                &f_rd_bytes));

    catch_error_report( PATCH_INFO, parse_patch_info(patch_info_buffer));

    // If required remove any previous new file
    if( f_stat( file_name_new , NULL) == FR_OK)
    {
        f_close(p_new_file); // cannot remove an open file
        catch_file_error_report( REMOVE_NEW, f_unlink( file_name_new));
    }
    else{} // NOOP: file desn't exist - no need to remove

    // Create fresh new file
    catch_file_error_report( CREATE_NEW,
                            f_open( p_new_file,
                                    file_name_new,
                                    FA_OPEN_ALWAYS | FA_READ | FA_WRITE ));

    // Prepare and allocate required memory area for expected new image size
    catch_file_error_report( EXPAND_NEW,
                            f_expand(   p_new_file,
                                        (FSIZE_t)(patch_info.new_file_length),
                                        1 ));

    // opening operations successful
    return true;
}

/*******************************************************************************
 * @brief Parse patch info from file first BYTES_PER_PKT bytes
 * @param[in] ptr to raw data
 * @return true if all read data seems valid
 *******************************************************************************/
static bool parse_patch_info(char * p_data)
{
    char * p_rx_pkt_content = NULL;
    char ** p_p_rx_pkt_content = &p_rx_pkt_content;



    // initialise ptr to start of content data
    p_rx_pkt_content = p_data;
    // patch file CRC
    patch_info.patch_crc = common_array_to_uint32(p_p_rx_pkt_content);
    catch_test_report( READ_PATCH_CRC_NULL, patch_info.patch_crc == 0);

    // patch length in bytes
    patch_info.patch_length_bytes = common_array_to_uint32(p_p_rx_pkt_content);
    catch_test_report( READ_PATCH_LENGTH_NULL,  patch_info.patch_length_bytes
                                                == 0);

    catch_test_report( READ_PATCH_LENGTH_OVER,  patch_info.patch_length_bytes
                                                > PATCH_MAX_LENGTH_BYTES);

    // number of packets making up patch
    patch_info.patch_pckts = common_array_to_uint32(p_p_rx_pkt_content);

    // image content crc
    patch_info.new_file_crc = common_array_to_uint32(p_p_rx_pkt_content);
    catch_test_report( READ_FILE_CRC, patch_info.new_file_crc == 0);

    // new file length in bytes
    patch_info.new_file_length = common_array_to_uint32(p_p_rx_pkt_content);
    catch_test_report( READ_FILE_LENGTH_NULL,   patch_info.new_file_length == 0);
    catch_test_report( READ_FILE_LENGTH_OVER,   patch_info.new_file_length
                                                > FILE_MAX_LENGTH_BYTES);

    // patch file size in bytes
    patch_info.patch_file_size = common_array_to_uint32(p_p_rx_pkt_content);
    catch_test_report(  READ_PATCH_LENGTH_FILE,
                        patch_info.patch_file_size !=
                        patch_info.patch_length_bytes + BYTES_PER_PKT + RPL_STORAGE_SIZE);
    catch_test_report(  PATCH_SIZE_MISMATCH,
                        f_size(p_patch_file) != patch_info.patch_file_size);

    // RPL delay used by calendar
    patch_info.response_delay_sec = common_array_to_uint32(p_p_rx_pkt_content);

    debug_printf(PATCH_PRO, "PCRC = 0x%08X, PL = %lu, NFCRC = 0x%08X, NFL = %lu, PS = %lu\r\n",
                patch_info.patch_crc,
                patch_info.patch_length_bytes,
                patch_info.new_file_crc,
                patch_info.new_file_length,
                patch_info.patch_file_size);

    return true;
}

/*******************************************************************************
 * @brief Close all files used for fw update
 * @param[in] void
 * @return true of all files closed successfuly
 *******************************************************************************/
static bool close_used_files(void){
    FRESULT res = FR_OK;
    res |= f_close(&old_file);
    res |= f_close(p_patch_file);
    res |= f_close(p_new_file);
    catch_test_report( CLOSE_FILES, res != FR_OK);

    return true;
}

/*******************************************************************************
 * @brief Verify the new file content against its 32-bit CRC
 * @param[in] void
 * @return bool with true for crc check pass
 *******************************************************************************/
static bool check_new_file_crc(void){
    uint32_t calc_crc = 0;
    static char check_buf[64] = {0};
    uint16_t next_check_len = 0;
    uint32_t bytes_left_to_check = 0;
    uint32_t read_pos = 0;

    const uint32_t *data;
    uint32_t words;

    // initialise CRC calc
    Chip_CRC_Init();
    Chip_CRC_UseDefaultConfig(CRC_POLY_CRC32);
    Chip_CRC_UseCRC32();

    // work out file length
    bytes_left_to_check = f_size(p_new_file);

    // check this matches patch_info
    catch_test_report(  NEW_FILE_SIZE,
                        bytes_left_to_check != patch_info.new_file_length);

    // loop crc calc one buf length at a time
    while( bytes_left_to_check > 0 )
    {
        // next time chech bytes length
        next_check_len =    (bytes_left_to_check > sizeof(check_buf)) ?
                            sizeof(check_buf) :
                            bytes_left_to_check;

        // adjust read ptr offset and read out data
        catch_file_error_report( CRC_SCROLL, f_lseek( p_new_file, read_pos));

        catch_file_error_report( CRC_READ, f_read(  p_new_file,
                                                    (char *)check_buf,
                                                    next_check_len,
                                                    &f_rd_bytes));

        // point at start of crc buffer
        data = (uint32_t *)check_buf;
        // calculate number of crc calc iteratuins
        words = (uint32_t)(next_check_len / 4);
        // execute crc calc
        while (words > 0) {
            Chip_CRC_Write32(*data);
            data++;
            words--;
        }

        // decrement total length to check by length of previous check
        bytes_left_to_check -= next_check_len;

        // shift the read position accordingly
        read_pos += next_check_len;
    }

    calc_crc = Chip_CRC_Sum();
    Chip_CRC_Deinit();

    debug_printf(   PATCH_PRO,
                    "FS = %lu | CRC_READ = 0x%08X | CRC_CALC = 0x%08X\r\n",
                    f_size(p_new_file),
                    patch_info.new_file_crc,
                    calc_crc);

    return (calc_crc == patch_info.new_file_crc) ? true : false;
}

/*******************************************************************************
 * @brief Delete new image file if it exists
 * @param[in] void
 * @return void
 *******************************************************************************/
void patch_process_validate_new_image_file(void){

    if( check_for_new_fw_file_available() == true){
        // remove new image file
        if( f_unlink((const char *)FW_NEW_FILE_NAME) != FR_OK) {
            report_error(IM_DELETE);
        }else{
            debug_printf(PATCH_PRO, "New image file deleted.\r\n");
        }
        // reset boot attenmpt log to 0
        if( clear_boot_log() == false) {
            report_error(IM_CLEAR_LOG);
        }else{
            debug_printf(PATCH_PRO, "Bootup Log Reset.\r\n");
        }
        // release shared file ptr
        file_sys_release_file_ptr();
    }
    return;
}

/*******************************************************************************
 * @brief Create a boot log file and start at 0
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool clear_boot_log(void){
    uint8_t boot_attempts;

    // attempt to acquire new file ptr from file_sys.c spare file
    // Taks will time-out after TASK_EXE_TIMEOUT_SEC = 2 min if file not
    // release by other task
    p_new_file = get_file_sys_file_ptr();
    while( p_new_file == 0 ){
        // wait 10 sec between checks
        FreeRTOSDelay(10000);
        p_new_file = get_file_sys_file_ptr();
    }

    // open log file
    catch_file_error_report(IM_OPEN_LOG,
                            f_open( p_new_file,
                                    BOOT_LOG_FILE_NAME,
                                    FA_WRITE ));
    boot_attempts = 0;

    catch_file_error_report(IM_WRITE_LOG,
                            f_write( p_new_file,
                                    (uint8_t *)&boot_attempts,
                                    1,
                                    &f_wr_bytes));

    catch_file_error_report(IM_CLOSE_LOG, f_close( p_new_file ));

    return true;
}

/*******************************************************************************
 * @brief TODO:
 * @param[in] void
 * @return void
 *******************************************************************************/
static bool check_for_new_fw_file_available(void){
    return (    f_stat( (const char *)FW_NEW_FILE_NAME, NULL ) == FR_OK ) ?
                true :
                false;
}

/*******************************************************************************
 * @brief Final steps to firmware update
 * @param[in] bool patching success result
 * @return void
 *******************************************************************************/
void patch_process_finalise_fw_update(bool b_patching_valid){
    if(b_patching_valid == false){
        debug_printf(MAIN, "Patch Pro FAIL - deleting file.\r\n");
        patch_process_kill();
        file_sys_release_file_ptr();
    }else{
        debug_printf(MAIN, "Patch Pro OK\r\n");
        sysmon_system_reset();
    }
    return;
}

/*******************************************************************************
 * @brief Final steps to calendar update
 * @param[in] bool patching success result
 * @return void
 *******************************************************************************/
void patch_process_finalise_cal_update(bool b_patching_valid){
    char cal_info[6] = {0};

    file_sys_release_file_ptr();

    if(b_patching_valid == false){
        debug_printf(MAIN, "Patch Pro FAIL - deleting file.\r\n");
        patch_process_kill();
    }else{
        // Rename file from new to old
        // TODO: Calendar stack should be validating file first maybe
        f_unlink((const char *)CAL_OLD_FILE_NAME);
        f_rename( (const char *)CAL_NEW_FILE_NAME, (const char *)CAL_OLD_FILE_NAME);
//        block_and_print_file_basic(CAL_PATCH_FILE_NAME);
//        block_and_print_file_basic(CAL_OLD_FILE_NAME);

        // Extract calendar version to report to cloud.
        f_open(&old_file, (const char *)CAL_OLD_FILE_NAME, FA_READ);
        f_read(&old_file, (char *)cal_info, 6, &f_rd_bytes);
        f_close(&old_file);

    compileUplink_CalendarConfirmation( patch_info.response_delay_sec,
                                        (uint16_t)( ((uint16_t)cal_info[1]<<8)+
                                                     (uint16_t)cal_info[0]),
                                        (uint32_t)( ((uint32_t)cal_info[5]<<24)+
                                                    ((uint32_t)cal_info[4]<<16)+
                                                    ((uint32_t)cal_info[3]<<8)+
                                                     (uint32_t)cal_info[2]));

    }
    return;
}

/*******************************************************************************
 * @brief Report error to sysmon task
 * @param[in] caller id and error code
 * @return void
 *******************************************************************************/
static void report_error(uint32_t error_code){
#ifdef COMPILE_IMAGE
    sysmon_report_error(PATCH_PRO, error_code);
#else
    debug_printf(PATCH_PRO, "ERR %lu", error_code);
#endif
    return;
}
