/*
==========================================================================
 Name        : patch_process.h
 Project     : pcore
 Path        : /pcore/src/private/application/patching/patch_process.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 8 Mar 2018
 Description : 
==========================================================================
*/



#ifndef PRIVATE_APPLICATION_PATCHING_PATCH_PROCESS_H_
#define PRIVATE_APPLICATION_PATCHING_PATCH_PROCESS_H_
/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/


#define VAR_INT_BYTES_REQ 5
#define BUIL_CODE_STRING_LENGTH_BYTES 8

enum patch_type_list{
    PATCH_TYPE_FW,
    PATCH_TYPE_CAL
};

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

bool patch_process_exe(const char * patch_notify);
void patch_process_kill(void);
void patch_process_validate_new_image_file(void);

void patch_process_finalise_fw_update(bool b_patching_valid);
void patch_process_finalise_cal_update(bool b_patching_valid);

#endif /* PRIVATE_APPLICATION_PATCHING_PATCH_PROCESS_H_ */
