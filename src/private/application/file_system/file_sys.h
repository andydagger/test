/*
==========================================================================
 Name        : file_sys.h
 Project     : pcore
 Path        : /pcore/src/private/application/file_system/file_sys.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 7 Aug 2018
 Description : 
==========================================================================
*/

#ifndef PRIVATE_APPLICATION_FILE_SYSTEM_FILE_SYS_H_
#define PRIVATE_APPLICATION_FILE_SYSTEM_FILE_SYS_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#define EOT_FF_COUNT 64

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
bool file_sys_mount_fatfs_drive(void);
FIL * get_file_sys_file_ptr(void);
void file_sys_release_file_ptr(void);

bool file_sys_mount_fat_drive(FATFS * p_fs);
void file_sys_create_fatfs_drive(FATFS * p_fs);
bool file_sys_create_initial_ref_image(void);
bool file_sys_create_boot_log(void);
bool file_sys_create_cal_file(void);

void block_and_print_file_basic(const char * file_name);

#endif /* PRIVATE_APPLICATION_FILE_SYSTEM_FILE_SYS_H_ */
