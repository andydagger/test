/*
==========================================================================
 Name        : file_sys.c
 Project     : pcore
 Path        : /pcore/src/private/application/file_system/file_sys.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 7 Aug 2018
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "config_pinout.h"
#include "led.h"
#include "debug.h"
#include "hw.h"
#include "ff.h"
#include "config_fatfs.h"
#include "diskio.h"
#include "flash.h"
#include "iap.h"
#include "relay.h"
#include "common.h"
#include "patch_rx.h"
#include "sysmon.h"
#include "file_sys.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "FreeRTOSMacros.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static UINT f_wr_bytes = 0; // not used by required to avoid passing NULL pointer
static UINT f_rd_bytes = 0; // not used by required to avoid passing NULL pointer

static bool b_drive_in_use = false;

static FIL spare_file;
static FATFS fs;
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void report_error(uint32_t error_code);


/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Mount the FAT12 drive
 * @param[in] void
 * @return void
 *******************************************************************************/
bool file_sys_mount_fatfs_drive(void)
{
  uint8_t initialise_disk_attempts = 0;
  uint8_t createNewDriveAttempts = 0;
  
    // Initialise flash - with timeout on failed attempts
    while(flash_disk_initialise() == false)
    {
        sys_delay_ms(250);
        hw_kick_watchdog();
    	if(++initialise_disk_attempts > 5)
    	{
    		debug_print_instant(MAIN, "Cannot init flash!!\r\n");
            set_sysmon_fault(EXT_MEM_NOT_RESPONDING);
            report_error(FS_INIT_FLASH);
            return false;
    	}
    }
    clear_sysmon_fault(EXT_MEM_NOT_RESPONDING);

    // Is there a drive to mount?
    if( file_sys_mount_fat_drive(&fs) == true)
    {
        debug_print_instant(MAIN, "Drive Mounted!\r\n");
        clear_sysmon_fault(MOUNTING_DRIVE);
        return true;
    }
    else
    {
    	debug_print_instant(MAIN, "No Drive Available!\r\n");
    	set_sysmon_fault(MOUNTING_DRIVE);
    }

    // attempt to mount new fatfs drive  - with timeout on failed attempts
    while(file_sys_mount_fat_drive(&fs) == false)
    {
        // sys_delay_ms(250); 
        hw_kick_watchdog();
    	flash_wipe_memory();
    	file_sys_create_fatfs_drive(&fs);
    	if(++createNewDriveAttempts > 5)
    	{
    		report_error(FS_ERASE_FLASH);
    		set_sysmon_fault(EXT_MEM_NOT_RESPONDING);
    		return false;
    	}
    }
    file_sys_create_boot_log();
    file_sys_create_cal_file();

    return true;
}


/*******************************************************************************
 * @brief Take control of spare file if free else return null ptr
 * @param[in] void
 * @return ptr to file, null if in use
 *******************************************************************************/
FIL * get_file_sys_file_ptr(void){
    FIL * p_ptr = 0;

    vTaskSuspendAll();

    if( b_drive_in_use == true){
        // drive already in use
        p_ptr = 0;
    }else{
        // drive not in use
        b_drive_in_use = true;
        p_ptr = &spare_file;
    }

    xTaskResumeAll();

    return p_ptr;
}

/*******************************************************************************
 * @brief Release use of spare file
 * @param[in] void
 * @return void
 *******************************************************************************/
void file_sys_release_file_ptr(void){
    b_drive_in_use = false;
    return;
}


/*******************************************************************************
 * @brief Create the FAT12 drive
 * @param[in] void
 * @return void
 *******************************************************************************/
void file_sys_create_fatfs_drive(FATFS * p_fs){
    BYTE * p_work;

    p_work = (BYTE *)get_patch_file_ptr();

	// create drive
	debug_print_instant(FILESYS, "Creating new drive....\r\n");
	f_mkfs("", FM_FAT, (DWORD)FF_MAX_SS, p_work, (UINT)FF_MAX_SS);
	f_mount(p_fs, "", 1);

    return;
}


/*******************************************************************************
 * @brief Create a boot log file and start at 0
 * @param[in] void
 * @return void
 *******************************************************************************/
bool file_sys_create_boot_log(void){
    static uint8_t boot_attempts;

    debug_print_instant(FILESYS, "Creating bootlog file.....\r\n");

    // First force delete any existing log file
    f_unlink((const char *)BOOT_LOG_FILE_NAME);

    // create log file if does not yet exist
    catch_file_error_report(FS_OPEN_LOG,
                            f_open( &spare_file,
                                    BOOT_LOG_FILE_NAME,
                                    FA_OPEN_ALWAYS | FA_READ | FA_WRITE ));
    boot_attempts = 0;

    catch_file_error_report(FS_WRITE_LOG,
                            f_write( &spare_file,
                                    (uint8_t *)&boot_attempts,
                                    1,
                                    &f_wr_bytes));

    catch_file_error_report(FS_CLOSE_LOG, f_close(&spare_file) );

    debug_print_instant(FILESYS, "Done.\r\n");
    return true;
}

/*******************************************************************************
 * @brief Create an empty calendar file
 * @param[in] void
 * @return void
 *******************************************************************************/
bool file_sys_create_cal_file(void){

    debug_print_instant(FILESYS, "Creating calendar file.....\r\n");
    // create calendar file if does not yet exist
    catch_file_error_report(FS_OPEN_LOG,
                            f_open( &spare_file,
                                    CAL_OLD_FILE_NAME,
                                    FA_OPEN_ALWAYS | FA_READ | FA_WRITE ));

//    catch_file_error_report(WRITE_LOG,
//                            f_write( &boot_log_file,
//                                    (uint8_t *)&boot_attempts,
//                                    1,
//                                    &f_wr_bytes));

    catch_file_error_report(FS_CLOSE_LOG, f_close(&spare_file) );
    debug_print_instant(FILESYS, "Done.\r\n");
    return true;
}

/*******************************************************************************
 * @brief Attempt to mount drive
 * @param[in] void
 * @return true if drive available
 *******************************************************************************/
bool file_sys_mount_fat_drive(FATFS * p_fs){
    return ( f_mount(p_fs, "", 1) == FR_OK ) ? true : false;
}

/*******************************************************************************
 * @brief Create ref fw image file
 * @param[in] void
 * @return void
 *******************************************************************************/
bool file_sys_create_initial_ref_image(void){
    char * p_source_code = NULL;
    uint32_t code_size_bytes = 0;
    uint8_t end_code_count = 0;

    p_source_code = (char *)IMAGE_ADDRESS;

    /*
     * CREATE OLD FW IMAGE
     */

    // If required remove any previous new file
    if( f_stat( (const char *)FW_OLD_FILE_NAME , NULL) == FR_OK)
    {
        f_close(&spare_file); // cannot remove an open file
        catch_file_error_report(FS_REMOVING_OLD_FILE,
                                f_unlink( (const char *)FW_OLD_FILE_NAME ));
    }
    else{}

    catch_file_error_report(FS_CREATING_OLD_FILE,
                            f_open( &spare_file,
                                    (const char *)FW_OLD_FILE_NAME,
                                     FA_OPEN_ALWAYS | FA_READ | FA_WRITE  ));

    debug_print_instant(FILESYS, "old image blank file ready.\r\n");
    /*
     * copy from code to file until:
     * - series of 0xFF have been copied
     * - and the file size is a modulo of 4 (4x8=32bit)
     */
    debug_print_instant(FILESYS, "Copying bytes.....\r\n");
    do{
        catch_file_error_report(FS_WRITING_OLD_FILE,
                                f_write(    &spare_file,
                                            (char *)p_source_code,
                                            1,
                                            &f_wr_bytes));
        if( *p_source_code == 0xFF ){
            end_code_count++;
        }else{
            end_code_count = 0;
        }

//        debug_print_instant(FILESYS, "*"); // progress
        hw_kick_watchdog();
        // move on to next byte
        p_source_code++;
        code_size_bytes++;

        // loop until EOT_FF_COUNT 0xFF have been copied
        // &
        // the code size is a modulo of 4 ( 32-bit aligned )
    }while( !(  (end_code_count >= EOT_FF_COUNT)
                &&
                ((code_size_bytes % 4) == 0) ));

    debug_print_instant(FILESYS, "Bytes copied = %lu bytes\r\n", code_size_bytes);

     // Remove EOF test bytes
    catch_file_error_report(FS_SEEK_OLD_FILE,
                            f_lseek(    &spare_file,
                                        f_tell(&spare_file) - EOT_FF_COUNT)
                                        );
    catch_file_error_report(FS_TRUNC_OLD_FILE,
                            f_truncate(&spare_file));

    debug_print_instant(   FILESYS,
                    "Final file size = %lu bytes = 0x%08X\r\n",
                    f_size(&spare_file),
                    f_size(&spare_file));

    catch_test_report(  FS_OLD_IM_LENGTH_NULL,
                        f_size(&spare_file) == 0);

    /*
     * CLOSE FILE
     */
    catch_file_error_report(FS_CLOSE_OLD_FILE,
                            f_close( &spare_file ));

    debug_print_instant(FILESYS, "Old image file created successfully!\r\n");
    debug_print_instant(FILESYS, "Done.\r\n");
    return true;
}

/*******************************************************************************
 * @brief Report error to sysmon task
 * @param[in] caller id and error code
 * @return void
 *******************************************************************************/
static void report_error(uint32_t error_code){
#ifdef COMPILE_IMAGE
    sysmon_report_error(FILESYS, error_code);
#else
    debug_printf(FILESYS, "ERR %lu", error_code);
#endif
    return;
}


void block_and_print_file_basic(const char * file_name){
    char read_buf[16] = {0};
    char ascii_buffer[33] = {0};
    char * p_to_target;
    uint32_t next_read_len = 0;
    uint32_t bytes_left_to_print = 0;//
    uint32_t read_pos = 0;
    uint32_t n_bytes = 0;

    // for debugging so force pointer take over
    debug_print_instant(FILESYS, "Open and print-out: %s\r\n", file_name);

    // Freeze RTOS
    vTaskSuspendAll();

    // open file
    f_open(&spare_file, file_name, FA_READ);

    // work out file length
    bytes_left_to_print = f_size(&spare_file);

    debug_print_instant(FILESYS, "Size =  %lu bytes\r\n", bytes_left_to_print);

    while( bytes_left_to_print > 0 )
    {
        // next time chech bytes length
        next_read_len =    (bytes_left_to_print > sizeof(read_buf)) ?
                            sizeof(read_buf) :
                            bytes_left_to_print;

        // adjust read ptr offset and read out data
        f_lseek( &spare_file, read_pos);

        f_read(  &spare_file, (char *)read_buf, next_read_len, &f_rd_bytes);

        n_bytes = next_read_len;
        p_to_target = ascii_buffer;

        while(n_bytes > 0){
            sprintf( p_to_target, "%02X", read_buf[next_read_len - n_bytes] );
            n_bytes--;
            p_to_target += 2;
        }
        *p_to_target = '\0';

        // print out data
        debug_print_instant(FILESYS, "%s\r\n", ascii_buffer);

        // decrement total length to print by length of previous print
        bytes_left_to_print -= next_read_len;

        // shift the read position accordingly
        read_pos += next_read_len;
    }

    debug_print_instant(FILESYS, "EOF.\r\n", read_buf);

    f_close(&spare_file);

    // Resume RTOS
    xTaskResumeAll();

    return;
}

