/*
 * luminrop.h
 *
 *  Created on: 21 Aug 2017
 *      Author: Ben Raiton
 */

#ifndef SRC_APPLICATION_LUMINROP_H_
#define SRC_APPLICATION_LUMINROP_H_

#include "config_queues.h"
#include <stdbool.h>
#include <stdint.h>

typedef enum
{
    OVERRIDE_STATE_DISABLED = 0,
    OVERRIDE_STATE_WAIT_ENABLED,
    OVERRIDE_STATE_ENABLED,
    OVERRIDE_STATE_MAX

} override_state_t;

// bit 7 of the output level is the enable/disable flag
#define LUMINAIR_OVERRIDE_ENABLE                 0x80


void luminrop_init(void);
void luminrop_process_msg( QuLuminrOpMsg msg );
void luminrop_service_stack(void);
bool get_luminrop_ready_to_use(void);
uint8_t get_luminrop_LampOutputBrightnessLevel(void);

#define DALI_RESTART_TIMEOUT_SEC 30

#endif /* SRC_APPLICATION_LUMINROP_H_ */
