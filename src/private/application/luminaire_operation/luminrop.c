/*
==========================================================================
 Name        : luminrop.c
 Project     : pcore
 Path        : /pcore/src/private/application/luminaire_operation/luminrop.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 31 Aug 2017
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "lorawan.h"
#include "lorawanhw.h"
#include "common.h"
#include "systick.h"
#include "debug.h"
#include "config_network.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "FreeRTOSMacros.h"
#include "luminr_tek.h"
#include "sysmon.h"
#include "luminrop.h"
#include "config_eeprom.h"
#include "NonVolatileMemory.h"
#include "NVMData.h"
#include "rtc_procx.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static override_state_t override_state = OVERRIDE_STATE_DISABLED;
static bool             luminr_op_ready = false;
static uint8_t          last_output_level = 0;
static uint8_t          last_calendar_output_level = 0;
static uint32_t         override_event_time_sec = 0;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
QueueHandle_t xQuLuminrOp = NULL;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Initialise lamp driver interface
 * @param[in] void
 * @return void
 *******************************************************************************/
void luminrop_init(void)
{
    NVM_lampOutputOverride_t storedLampOverride;
    uint32_t timeUTC;

    /* Reset the Luminar OP Ready Flag before starting the Luminar Search */
	luminr_op_ready = false;
	// catch error if failed to init
	while(luminr_tek_init() == false);

	last_output_level = 0;
	/* Luminar Init is ow complete */
	/* Before leaving this function set flag to allow other services to interrogate the Luminar op status */
	luminr_op_ready = true;

    // read the LOO EEprom store to get the lamp override status
	// the node may have been switched off during an override.
    NVMData_readLampOutputOverride( &storedLampOverride);

    // If the node was powered-off during an override re-instate it
    if( storedLampOverride.state == OVERRIDE_STATE_ENABLED)
    {
        // we may have GPS lock so check the time UTC
        timeUTC = rtc_procx_GetCounter();

        if( timeUTC >= storedLampOverride.stopTimeUtc)
        {
            // powered up after the end of the override
            // so update override state and save back to EEprom
            override_state = OVERRIDE_STATE_DISABLED;

            storedLampOverride.state = (uint8_t)override_state;

            NVMData_writeLampOutputOverride( &storedLampOverride);

            debug_printf(LUMINROP, "Power-up disable override >> %d\r\n", last_output_level);
        }
        else // override the output level
        {
            luminr_tek_update_lamp_output( storedLampOverride.outputLevel);

            last_output_level       = storedLampOverride.outputLevel;
            // load the stop time
            override_event_time_sec = storedLampOverride.stopTimeUtc;

            debug_printf(LUMINROP, "Power-up override enabled >> %d\r\n", last_output_level);
        }
    }

}

/*******************************************************************************
 * @brief Servicing lamp driver interface
 * @param[in] void
 * @return void
 *******************************************************************************/
void luminrop_service_stack(void)
{
	static bool prev_Last_Gasp_active = FALSE;  // Store the previous state so that the current state can be compared
	static uint32_t time_of_last_gasp_recovery = 0;

	NVM_lampOutputOverride_t storedLampOverride;
    uint32_t timeUTC;

	switch( override_state)
	{
	    case OVERRIDE_STATE_DISABLED:
	        break;

	    case OVERRIDE_STATE_WAIT_ENABLED:
	        timeUTC = rtc_procx_GetCounter();

	        if( timeUTC >= override_event_time_sec)
            {
                override_state = OVERRIDE_STATE_ENABLED;

                // read the EEprom store to get the outputLevel and stop time
                NVMData_readLampOutputOverride( &storedLampOverride);

                luminr_tek_update_lamp_output( storedLampOverride.outputLevel);

                last_output_level       = storedLampOverride.outputLevel;
                // load the stop time
                override_event_time_sec = storedLampOverride.stopTimeUtc;

                // save the new state back to EEprom.
                storedLampOverride.state = (uint8_t)override_state;

                NVMData_writeLampOutputOverride( &storedLampOverride);

                debug_printf(LUMINROP, "Override enabled >> %d\r\n", last_output_level);
            }
            break;

            // the override is active we now wait till the stop time
	    case OVERRIDE_STATE_ENABLED:
	        timeUTC = rtc_procx_GetCounter();

	        // have we reached the override stop time?
	        if( timeUTC >= override_event_time_sec)
	        {
                override_state = OVERRIDE_STATE_DISABLED;

                luminr_tek_update_lamp_output( last_calendar_output_level);

                last_output_level = last_calendar_output_level;

                // now store the state change to EEprom
                NVMData_readLampOutputOverride( &storedLampOverride);

                storedLampOverride.state = (uint8_t)override_state;

                NVMData_writeLampOutputOverride( &storedLampOverride);

                debug_printf(LUMINROP, "Override disabled >> %d\r\n", last_output_level);
	        }
	        break;

	    default:
            // should not get here but just in case.....
	        override_state = OVERRIDE_STATE_DISABLED;
	        break;
	}

    // Has the Last Gasp status changed since the last time we were here.
    if ( prev_Last_Gasp_active != get_sysmon_last_gasp_is_active())
    {
        prev_Last_Gasp_active = get_sysmon_last_gasp_is_active();

        // It was TRUE and has now become FALSE
    	if ( false == get_sysmon_last_gasp_is_active())
    	{
			// Record reference from when power is restored.
    		time_of_last_gasp_recovery = get_systick_sec();
		}
    }

	// service interface stack
    // DO NOT service driver interface when there is no mains.
    if(false == get_sysmon_last_gasp_is_active() )
    {
    	// If this is following a power-failure DALI_RESTART_TIMEOUT_SEC wait is required to allow super-cap 
        // to charge-up before the DALI bus can reliably be used.
        if(((get_systick_sec() - time_of_last_gasp_recovery ) > DALI_RESTART_TIMEOUT_SEC )  || (0 == time_of_last_gasp_recovery))
        {
        	// Reset it back to Zero
        	time_of_last_gasp_recovery = 0;
        	luminr_tek_service();
        }
        else
        {
            // Wait for timeout to expire
        }
    }

}

/*******************************************************************************
 * @brief Process queued msg addressed to luminrop module
 * @param[in] void
 * @return void
 *******************************************************************************/
void luminrop_process_msg( QuLuminrOpMsg msg)
{
    NVM_lampOutputOverride_t storedLampOverride;
    uint32_t timeUTC;

	// Message received from the calendar task?
    if( msg.source_task_id == TASK_CAL)
    {
        // store this value for when override ends
        last_calendar_output_level = msg.lamp_output_level;

        // If override active ignore commands from Calendar for now
	    if( override_state == OVERRIDE_STATE_ENABLED )
	    {
	        debug_printf(LUMINROP, "Override active: Calendar ignored for now.\r\n");
	        // don't output the lamp output level as we are in override
	        return;
	    }
	    else
	    {
	        debug_printf(LUMINROP, "Calendar Request >> %d\r\n", msg.lamp_output_level);

	        // then pass new level to corresponding stack
	        luminr_tek_update_lamp_output( last_calendar_output_level);
	        // remember the last value
	        last_output_level = last_calendar_output_level;
	    }
	}

	// If command from network, check override
    // message has been parsed for errors in the network task
	if( msg.source_task_id == TASK_NWK_COM)
	{
        NVMData_readLampOutputOverride( &storedLampOverride);

        timeUTC = rtc_procx_GetCounter();

        if( msg.lamp_output_level & LUMINAIR_OVERRIDE_ENABLE)
	    {
            // preload the EEprom struct with data except the state
            storedLampOverride.outputLevel  = msg.lamp_output_level & ~LUMINAIR_OVERRIDE_ENABLE;
            storedLampOverride.startTimeUtc = msg.lamp_start_time_utc;
            storedLampOverride.stopTimeUtc  = msg.lamp_stop_time_utc;

	        // check for immediate start
	        if (storedLampOverride.startTimeUtc <= timeUTC)
	        {
                override_state = OVERRIDE_STATE_ENABLED;

                // load the stop time
                override_event_time_sec = storedLampOverride.stopTimeUtc;

                // save the new state back to eeprom.
                storedLampOverride.state = (uint8_t)override_state;

                NVMData_writeLampOutputOverride( &storedLampOverride);

                debug_printf(LUMINROP, "Enable override immediate >> %d\r\n", storedLampOverride.outputLevel);

                // then pass new level to corresponding stack
                luminr_tek_update_lamp_output( storedLampOverride.outputLevel);
                // remember the last value
                last_output_level = storedLampOverride.outputLevel;
	        }
	        else
	        {
                // wait for the enable start time
	            override_state = OVERRIDE_STATE_WAIT_ENABLED;

                override_event_time_sec = storedLampOverride.startTimeUtc;

                // save the new state back to EEprom.
                storedLampOverride.state = (uint8_t)override_state;

                NVMData_writeLampOutputOverride( &storedLampOverride);

                debug_printf(LUMINROP, "Override wait enabled >> %d utc\r\n", override_event_time_sec);
	        }

	    }
	    else // disabling the override
	    {
            switch( override_state)
            {
                case OVERRIDE_STATE_DISABLED:
                    // the override is already disabled
                    break;

                case OVERRIDE_STATE_WAIT_ENABLED:
                    // if waiting to override then only change state and store to EEprom
                    override_state = OVERRIDE_STATE_DISABLED;

                    // save the new state back to EEprom.
                    storedLampOverride.state = (uint8_t)override_state;

                    NVMData_writeLampOutputOverride( &storedLampOverride);

                    debug_printf(LUMINROP, "Disable override when waiting to enable\r\n");
                    break;

                case OVERRIDE_STATE_ENABLED:
                    // clear the override
                    override_state = OVERRIDE_STATE_DISABLED;

                    // save the new state back to EEprom.
                    storedLampOverride.state = (uint8_t)override_state;

                    NVMData_writeLampOutputOverride( &storedLampOverride);

                    debug_printf(LUMINROP, "Disable override >> %d\r\n", last_calendar_output_level);

                    // then pass new level to corresponding stack
                    luminr_tek_update_lamp_output( last_calendar_output_level);
                    // remember the last value
                    last_output_level = last_calendar_output_level;
                    break;

                default:
                    // should not get here but just in case.....
                    override_state = OVERRIDE_STATE_DISABLED;
                    break;
            }
	    }
	}

}


/*******************************************************************************
 * @brief Get status of luminr driver interface
 * @param[in] void
 * @return bool true for ready and false for do not yet use.
 *******************************************************************************/
bool get_luminrop_ready_to_use(void){
    return luminr_op_ready;
}

/*******************************************************************************
 * @brief Get the latest brightness level
 * @param[in] void
 * @return uint8_t brightness leevl in %
 *******************************************************************************/
uint8_t get_luminrop_LampOutputBrightnessLevel(void)
{
	return ( last_output_level & (uint8_t)0x7F);
}
