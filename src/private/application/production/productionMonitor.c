/*
 * productionMonitor.c
 *
 *  Created on: 19 May 2021
 *      Author: benraiton
 */


/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "stdbool.h"
#include "stdint.h"
#include "string.h"
#include "stdio.h"
#include "productionMonitor.h"
#include "productionMonitor_impl.h"
#include "supplyMonitor.h"
#include "nwk_com.h"
#include "systick.h"
#include "gps.h"
#include "powerMonitor.h"
#include "compileUplink.h"
#include "debug.h"
#include "sysmon.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

#define PROD_KEY_CHECK_NUM_BYTES    20
#define PROD_WHITELIST_ENTRY_DELAY_MS (uint32_t)25000
#define PROD_NODE_INFO_LENGTH_BYTES 35
#define APPEUI  (const char *)"AAF77D2AF96B4A49"

static bool unitIsUnderTest = false;
static productionMonitorState prodMonState = PROD_IDLE;
static productionMonitorState prevProdMonState = PROD_IDLE;
static char radioDevEui[17]={0}; 
static uint32_t delayStateRefTimeMs = 0;
static bool receivedCalibrationReferenceReadings = false;
static bool receivedNodeInfo = false;
static char nodeInfoBuffer[PROD_NODE_INFO_LENGTH_BYTES];
static bool networkReadyForDevice = false;
static char keySectionStringBuffer[NODE_INFO_STRING_LENGTH] = {0}; // Declared here for Unt testing

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

void productionMonitor_checkIfUnitIsUnderTest(void)
{
	uint16_t i;

	for(i=0; i<PROD_KEY_CHECK_NUM_BYTES; i++)
	{
		if(0xff != readByteFromKeysSection(i))
		{
			unitIsUnderTest = false;
            return;
		}
	}
	unitIsUnderTest = true;
    return;
}

bool productionMonitor_getNetworkReadyForDevice(void)
{
    if(unitIsUnderTest == false)
    {
        return true;
    }
    else
    {
        return networkReadyForDevice;
    }
}

bool productionMonitor_getUnitIsUndertest(void)
{
    return unitIsUnderTest;
}

void productionMonitor_service(void)
{
    if(prevProdMonState != prodMonState)
    {
        prevProdMonState = prodMonState;
        debug_printf(PROD,"SM State => %d\r\n",prodMonState);
        productionMonitor_sendNewStateOverDali(prodMonState);
    }

    switch(prodMonState)
    {
        case PROD_IDLE:
            prodMonState = PROD_WAIT_FOR_12V_SUPPLY;
            break;

        case PROD_WAIT_FOR_12V_SUPPLY:
            if(supplyMonitor_get12vRailIsReady() == true)
            {
                debug_printf(PROD,"12 Supply Ready.\r\n");
                productionMonitor_enableDaliBusForComs();
                prodMonState = PROD_WAIT_FOR_DEVEUI;
            }
            break;

        case PROD_WAIT_FOR_DEVEUI:
            get_nwk_com_dev_eui(radioDevEui);
            if( strlen(radioDevEui) == 16 )
            {
                prodMonState = PROD_WHYTELIST_DELAY;
                productionMonitor_sendDevEuiOverDali(radioDevEui);
                debug_printf(PROD,"Sending DevEui %s\r\n", radioDevEui);
                delayStateRefTimeMs = get_systick_ms();
            }
            break;

        case PROD_WHYTELIST_DELAY:
            if((get_systick_ms() - delayStateRefTimeMs) > PROD_WHITELIST_ENTRY_DELAY_MS)
            {
                debug_printf(PROD,"Network should now be ready for join.\r\n");
                prodMonState = PROD_WAIT_FOR_GPS;
                networkReadyForDevice = true;
            }
            break;

        case PROD_WAIT_FOR_GPS:
            if( gps_verifyTimeStringReceptionIsDone() == true)
            {
                debug_printf(PROD,"GPS check done.\r\n");
                prodMonState = PROD_WAIT_FOR_PWR_CAL;
            }
            break;

        case PROD_WAIT_FOR_PWR_CAL:
            if( (powerMonitor_isCalibrationActive() == false) &&
                (get_sysmon_self_test_is_done() == true) )
            {
                debug_printf(PROD,"Power Calibration finished.\r\n");
                prodMonState = PROD_WAIT_FOR_REF_READINGS;
                compileUplink_ProductionTestReport();
            }
            break;

        case PROD_WAIT_FOR_REF_READINGS:
            if(receivedCalibrationReferenceReadings == true)
            {
                prodMonState = PROD_WAIT_FOR_NODE_INFO;
            }
            break;

        case PROD_WAIT_FOR_NODE_INFO:
            if(receivedNodeInfo == true)
            {
                if(productionMonitor_checkAndSaveNodeInfo(nodeInfoBuffer) == true)
                {
                    prodMonState = PROD_NODE_READY;
                }
                else
                {
                    prodMonState = PROD_ERROR;
                }
            }
            break;

        case PROD_NODE_READY:
            compileUplink_ProductionTestingDone();
            prodMonState = PROD_DONE;
            break;

        case PROD_DONE:
            break;

        default:
            prodMonState = PROD_ERROR;
    }
    return;
}

void productionMonitor_calibrateReadings(PowerCalcDataset * refReadings)
{
    powerMonitor_applyCalibReadings(refReadings);
    receivedCalibrationReferenceReadings = true;
    return;
}

void productionMonitor_receivedNodeInfo(nodeInfoDataset * nodeInfoSourceBuffer)
{
    char * ptrOutString = keySectionStringBuffer;
    char devEui[16] = {0};
    uint8_t i;

    //__________________________________________________________________
    // Serial Number 6 char
    for(i=0; i<sizeof nodeInfoSourceBuffer->SerialNumberString; i++)
    {
        *ptrOutString++ = nodeInfoSourceBuffer->SerialNumberString[i];
    }
    // Null separator
    *ptrOutString++ = '\0';
    //__________________________________________________________________
    // Default APPEUI - 8 bytes so 16 HEX characters
    sprintf(ptrOutString, "%s", APPEUI);
    ptrOutString += 2 * sizeof (APPEUI);
    // Null separator
    *ptrOutString++ = '\0';
    //__________________________________________________________________
    // DEVEUI 8 bytes so 16 HEX characters
    get_nwk_com_dev_eui(devEui);
    for(i=0; i<16; i++)
    {
        *ptrOutString++ = devEui[i];
    }
    // Null separator
    *ptrOutString++ = '\0';
    //__________________________________________________________________
    // APPKEY from Cloud via Test Manager - 16 bytes so 32 HEX characters
    for(i=0; i<sizeof nodeInfoSourceBuffer->AppKeyString; i++)
    {
        *ptrOutString++ = nodeInfoSourceBuffer->AppKeyString[i];
    }
    // Null separator
    *ptrOutString++ = '\0';
    //__________________________________________________________________
    // Part Number as Fxxxx (i.e. F6810)
    sprintf(ptrOutString, "F%04d", nodeInfoSourceBuffer->partNumber);
    ptrOutString += 5;
    // Null separator
    *ptrOutString++ = '\0';
    //__________________________________________________________________
    // Hardware Revisions
    *ptrOutString++ = nodeInfoSourceBuffer->powerBoardVersion;
    // Null separator    
    *ptrOutString++ = '\0';
    *ptrOutString++ = nodeInfoSourceBuffer->mainBoardVersion;
    // Null separator    
    *ptrOutString++ = '\0';
    *ptrOutString++ = nodeInfoSourceBuffer->radioBoardVersion;
    // Null separator    
    *ptrOutString++ = '\0';
    *ptrOutString++ = nodeInfoSourceBuffer->assemblyVersion;
    //__________________________________________________________________
    // Final separator
    // Null separator    
    *ptrOutString++ = '\0';

    productionMonitor_checkAndSaveNodeInfo(keySectionStringBuffer);
    receivedNodeInfo = true;
    return;
}
