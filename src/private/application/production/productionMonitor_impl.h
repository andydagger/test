/*
 * productionMonitor_impl.h
 *
 *  Created on: 19 May 2021
 *      Author: benraiton
 */

#ifndef PRIVATE_APPLICATION_PRODUCTION_PRODUCTIONMONITOR_IMPL_H_
#define PRIVATE_APPLICATION_PRODUCTION_PRODUCTIONMONITOR_IMPL_H_


#include "stdbool.h"
#include "stdint.h"
#include "productionMonitor.h"

/*****************************************************************************
 * Definitions
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*****************************************************************************
 * Read byte direct from flash - position starting from 1
 ****************************************************************************/
uint8_t readByteFromKeysSection(uint8_t position);
bool productionMonitor_checkAndSaveNodeInfo(char * nodeInfoSourceBuffer);
void productionMonitor_sendDevEuiOverDali(char * devEuiString);
void productionMonitor_enableDaliBusForComs(void);
void productionMonitor_sendNewStateOverDali(productionMonitorState newState);

#endif /* PRIVATE_APPLICATION_PRODUCTION_PRODUCTIONMONITOR_IMPL_H_ */
