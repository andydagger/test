/*
 * productionMonitor.h
 *
 *  Created on: 19 May 2021
 *      Author: benraiton
 */

#ifndef PRIVATE_APPLICATION_PRODUCTION_PRODUCTIONMONITOR_H_
#define PRIVATE_APPLICATION_PRODUCTION_PRODUCTIONMONITOR_H_

#include "config_dataset.h"
#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Definitions
 ****************************************************************************/

#define NODE_INFO_STRING_LENGTH 89

typedef enum __tag_productionMonitorState
{
	PROD_IDLE = 0,
	PROD_WAIT_FOR_12V_SUPPLY,
    PROD_WAIT_FOR_DEVEUI,
    PROD_WHYTELIST_DELAY,
    PROD_WAIT_FOR_GPS,
    PROD_WAIT_FOR_PWR_CAL,
    PROD_WAIT_FOR_REF_READINGS,
    PROD_WAIT_FOR_NODE_INFO,
    PROD_NODE_READY,
    PROD_DONE,
    PROD_ERROR
} productionMonitorState;

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void productionMonitor_checkIfUnitIsUnderTest(void);
bool productionMonitor_getUnitIsUndertest(void);
void productionMonitor_service(void);
void productionMonitor_calibrateReadings(PowerCalcDataset * refReadings);
void productionMonitor_receivedNodeInfo(nodeInfoDataset * nodeInfoSourceBuffer);
bool productionMonitor_getNetworkReadyForDevice(void);

#endif /* PRIVATE_APPLICATION_PRODUCTION_PRODUCTIONMONITOR_H_ */
