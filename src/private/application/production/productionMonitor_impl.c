/*
 * productionMonitor_impl.c
 *
 *  Created on: 19 May 2021
 *      Author: benraiton
 */

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "stdbool.h"
#include "stdint.h"
#include "string.h"
#include "stdlib.h"
#include "productionMonitor.h"
#include "productionMonitor_impl.h"
#include "common.h"
#include "hw.h"
#include "DaliAL.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

#define PROD_KEY_CHECK_NUM_BYTES    20
#define _KEYS_FLASH_ADDR (uint32_t)KEYS_ADDRESS
#define _KEYS_FLASH_SECTOR (uint32_t)(_KEYS_FLASH_ADDR>>12)

#define START_OF_APP_EUI    7
#define APP_EUI_LENGTH      16
#define START_OF_DEV_EUI    24
#define DEV_EUI_LENGTH      16
#define START_OF_APP_KEY    41
#define APP_KEY_LENGTH       32

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/


/*****************************************************************************
 * Public functions
 ****************************************************************************/

uint8_t readByteFromKeysSection(uint8_t position)
{
    if(position == 0) return 0xFF;
    uint8_t * keysSection = (uint8_t*) (KEYS_ADDRESS);
    return  keysSection[position - 1];
}

bool productionMonitor_checkAndSaveNodeInfo(char * nodeInfoSourceBuffer)
{
    uint8_t i;

    // Check all Null characters are present
    if( (nodeInfoSourceBuffer[6] != '\0') ||
        (nodeInfoSourceBuffer[23] != '\0') ||
        (nodeInfoSourceBuffer[40] != '\0') ||
        (nodeInfoSourceBuffer[73] != '\0') ||
        (nodeInfoSourceBuffer[79] != '\0') ||
        (nodeInfoSourceBuffer[81] != '\0') ||
        (nodeInfoSourceBuffer[83] != '\0') ||
        (nodeInfoSourceBuffer[85] != '\0') ||
        (nodeInfoSourceBuffer[87] != '\0'))   return false;

    // Check DEVEUI only contains valid characters
    for(i=START_OF_APP_EUI; i<(START_OF_APP_EUI + APP_EUI_LENGTH); i++)
    {
        if( ((nodeInfoSourceBuffer[i] < '0') || (nodeInfoSourceBuffer[i] > 'F')) ||
            ((nodeInfoSourceBuffer[i] > '9') && (nodeInfoSourceBuffer[i] < 'A')) )
            return false;
    }

    // Check DEVEUI only contains valid characters
    for(i=START_OF_DEV_EUI; i<(START_OF_DEV_EUI + DEV_EUI_LENGTH); i++)
    {
        if( ((nodeInfoSourceBuffer[i] < '0') || (nodeInfoSourceBuffer[i] > 'F')) ||
            ((nodeInfoSourceBuffer[i] > '9') && (nodeInfoSourceBuffer[i] < 'A')) )
            return false;
    }

    // Check APPKEY only contains valid characters
    for(i=START_OF_APP_KEY; i<(START_OF_APP_KEY + APP_KEY_LENGTH); i++)
    {
        if( ((nodeInfoSourceBuffer[i] < '0') || (nodeInfoSourceBuffer[i] > 'F')) ||
            ((nodeInfoSourceBuffer[i] > '9') && (nodeInfoSourceBuffer[i] < 'A')) )
            return false;
    }


    hw_writeToInternalFlashSingleSector(    KEYS_ADDRESS, 
                                            (uint32_t *)nodeInfoSourceBuffer,
                                            (uint32_t)NODE_INFO_STRING_LENGTH);
    return true;
}

void productionMonitor_sendDevEuiOverDali(char * devEuiString)
{
    uint8_t i;
    uint8_t nextByte = 0;
    char local_str[3] = {0};
    if(devEuiString == NULL) return;
    if(strlen(devEuiString) != 16) return;
    for(i=0; i<8; i++)
    {
        strncpy(local_str, devEuiString, 2);
        nextByte = (uint8_t) strtol(local_str, NULL, 16);
        devEuiString += 2;
        DaliAL_directArcPowerControl(nextByte, NULL);
    }
    return;
}

void productionMonitor_enableDaliBusForComs(void)
{
	an_driver_set_an_smooth(IO_LOW);
	an_driver_disable_pwm_pin(); // pin set high by default
	dali_set_psu(IO_HIGH);
    dali_out(true); // set DALI line high
    return;
}

void productionMonitor_sendNewStateOverDali(productionMonitorState newState)
{
    DaliAL_setFadeTime((uint8_t)newState, NULL);
    return;
}
