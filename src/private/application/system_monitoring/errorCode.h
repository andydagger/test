/*
 * errorCode.h
 *
 *  Created on: 22 Sep 2020
 *      Author: benraiton
 */

#ifndef PRIVATE_APPLICATION_SYSTEM_MONITORING_ERRORCODE_H_
#define PRIVATE_APPLICATION_SYSTEM_MONITORING_ERRORCODE_H_

#include "stdint.h"
#include "stdbool.h"

#define NO_FAULT        0x00
#define SYSTEM_FAULT    0x01
#define HARD_FAULT      0x01
#define MALLOC_FAULT    0x02
#define REQ_SYS_RESET   0x03
#define STACK_OVERFLOW  0x02
#define TASK_TIME_OUT   0x03
#define NWK_ERROR       0x04



void errorCode_storeCode(uint8_t idByte, uint8_t valueByte);
void errorCode_getCode(uint8_t* p_idByte, uint8_t* p_valueByte);
void errorCode_eraseCode();

#endif /* PRIVATE_APPLICATION_SYSTEM_MONITORING_ERRORCODE_H_ */
