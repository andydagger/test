/*
 * errorCode.c
 *
 *  Created on: 22 Sep 2020
 *      Author: benraiton
 */

#include "errorCode.h"
#include "config_eeprom.h"
#include "NonVolatileMemory.h"
#include "NVMData.h"

void errorCode_storeCode(uint8_t idByte, uint8_t valueByte)
{
    NVM_errorCode_t errorCode;

    NVMData_readErrorCode( &errorCode);

    errorCode.id    = idByte;
    errorCode.value = valueByte;

    NVMData_writeErrorCode( &errorCode);
}

void errorCode_eraseCode()
{
    NVM_errorCode_t errorCode;

    NVMData_readErrorCode( &errorCode);

    errorCode.id    = 0;
    errorCode.value = 0;

    NVMData_writeErrorCode( &errorCode);
}

void errorCode_getCode(uint8_t* p_idByte, uint8_t* p_valueByte)
{
    NVM_errorCode_t errorCode;

    NVMData_readErrorCode( &errorCode);

    *p_idByte    = errorCode.id;
    *p_valueByte = errorCode.value;
}
