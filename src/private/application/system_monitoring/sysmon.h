/*
==========================================================================
 Name        : sysmon.h
 Project     : pcore
 Path        : /pcore/src/private/application/system_monitoring/sysmon.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 4 Sep 2017
 Description :
==========================================================================
*/

#ifndef PRIVATE_APPLICATION_SYSTEM_MONITORING_SYSMON_H_
#define PRIVATE_APPLICATION_SYSTEM_MONITORING_SYSMON_H_

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/
#include "config_queues.h"
//#include "Tilt_status.h"
#include <stdbool.h>
#include <stdint.h>

/*
 * It could take 13 min for NWK task to return if waiting for
 * duty-cycle availability.
 */
// TODO: Change network task to return when waiting for duty-cycle.
#define TASK_EXE_TIMEOUT_SEC      (uint32_t)(60*35)

//----------------------------------------------------------------------------
// NWK PROTOCOL: 01.62.00 < FIRMWARE VERSION  < 02.00.00
//----------------------------------------------------------------------------

//-------------------------     FAULTS FLAGS     --------------------------
enum fault_flags{
    NO_POWER_DATA, // 0 Self Test
    NO_VALID_CALENDAR,
    CORRUPT_BILLING_INFO,
    ACCEL_NOT_RESPONDING, // 3 Self Test
    DALI_FAULT,
    LAMP_FAULT,
    DRIVER_FAULT,
    LMP_SHORT_CIRCUIT,
    LMP_OPEN_CIRCUIT,
    LMP_LOAD_INCREASE,
    LMP_LOAD_DECREASE,
    CUR_PROTECTOR_ACTIVE,
    THERMAL_SHUTDOWN,
    THERMAL_OVERLOAD,
    REF_MEAS_FAILED,
    ARC_LEVEL_NOT_DELIVERED,
    BLUETOOTH_DISCONNECTED,
    COLUMN_NOT_VERTICAL,
    DOOR_IS_OPEN,
    HIGH_COLUMN_TEMP_DETECTED,
    LORA_MODULE_NOT_RESPONDING,
    GPS_MODULE_NOT_RESPONDING, // 21 Self Test
    NO_GPS_LOCK,
    BLUETOOTH_HW_NOT_RESPONDING,
    EXT_MEM_NOT_RESPONDING, // 24 Self Test
    LAMP_CANNOT_DIM,
    NO_LOAD_DETECTED, 
    PWR_ST_FAILED,
    UNUSED2,
    SUPPLY_12V_RAIL_ERROR, // 29 Self Test
    MOUNTING_DRIVE,
    POWER_FAILURE
};
//                       30        20        10         0
//                       10987654321098765432109876543210
#define SELF_TEST_MASK 0b00100001001000000000000000001001
#define STARTING_MASK  0b00000000000000000000000000000000
#define NODE_ONE_MASK  0b00000000111001010000000000000000

#define GPS_NO_LOCK_POWER_OFF_SEC   (uint32_t)86400 // 24hrs

//-----------------------------     LORAWAN     ------------------------------
enum lorawan_error_flags{
    MSG_RX_INVALID,
    SEND_MESSAGE_TIMEOUT,
    MCAST_SESSION_CHANGE,
    REJOIN_REQ,
    DATA_LENGTH,
    KEYS_INIT,
    MCAST_KEYS_INIT,
    TX_MAC_ERR,
    NO_RESPONSE,
    MAC_RX_MISSED,
    FORCE_ENABLE_ER,
    MAC_RESUME_ER,
    TX_SM_STATE_OUT_OF_BOUND,
    RX_MSG_QU_FULL,
    FRAME_CNTR,
    INVALID_RESP
};

//-----------------------------     DALI     ------------------------------
// TODO: will need onset and recovery message type
enum dali_error_flags{
   NO_FAULT_FLAG = 0,
   DALI_FAULT_FLAG = 1 << 4,
   LAMP_FAULT_FLAG = 1 << 5,
   DRIVER_FAULT_FLAG = 1 << 6,
   SHORT_CIRCUIT_FLAG = 1 << 7,
   OPEN_CIRCUIT_FLAG = 1 << 8,
   LOAD_INCREASED_FLAG = 1 << 9,
   LOAD_DECREASED_FLAG = 1 << 10,
   CURRENT_PROTECT_FLAG = 1 << 11,
   THERMAL_SHUT_DOWN_FLAG = 1 << 12,
   THERMAL_OVERLOAD_FLAG = 1 << 13,
   REF_MEASUREMENT_FAILED = 1 << 14,
   READ_BACK_DIFF_FLAG = 1 << 15,
};

//-----------------------------     FLASH     ------------------------------

enum flash_error_flags{
    FLASH_SPI_READ,
    FLASH_SPI_READ_TX_RX_COUNT,
    FLASH_SPI_READ_ER_FLAG,
    ERASE_SECTOR,
    GET_STATUS,
    SEND_WR_EN,
    WRITE_DATA_PTR,
    WRITE_DATA,
    FLASH_SPI_WRITE_ER_FLAG,
    SEND_ER_ALL_CMD
};

//-----------------------------     PATCH RX     -------------------------

enum patch_rx_error_flags{
    PATCH_MSG_ID,
    PARSE_PATCH_CRC_NULL,
    PARSE_PATCH_LENGTH_NULL,
    PARSE_PATCH_LENGTH_OVER,
    PARSE_PATCH_PKTS_NULL,
    PARSE_PATCH_PKTS_OVER,
    PARSE_FILE_SIZE_NULL,
    PARSE_FILE_SIZE_OVER,
    PARSE_FILE_CRC_NULL,
    REMOVE_PATCH_FILE,
    OPEN_PATCH_RX,
    EXPAND_PATCH,
    WRITE_PATCH_INFO,
    LOAD_MCAST_NWK_JOB,
    PKT_NUM_OVER,
    PATCH_WRITE_OVERFLOW,
    SCROLL_PATCH_RX,
    WRITE_PATCH,
    CLOSE_PATCH_RX,
    CRC_PATCH_SIZE,
    CRC_PATCH_SCROLL,
    CRC_PATCH_READ,
    PATCH_CRC,
    LOAD_RPL_NWK_JOB,
    RPL_DELAY,
    RPL_RESET_SCROLL,
    RPL_RESET_ERASE,
    RPL_SET_PCP_NUMBER,
    RPL_UPDATE_SCROLL,
    RPL_UPDATE_READ,
    RPL_UPDATE_WRITE,
    RPL_CHECK_SCROLL
};

//-----------------------------     PATCH PROCESS     -------------------------

enum patch_process_error_flags{
    NEW_NAME,
    PATCH_PTR,
    SPARE_PTR,
    OPEN_OLD,
    OPEN_PATCH,
    SCROLL_PATCH,
    READ_PATCH,
    PATCH_INFO,
    REMOVE_NEW,
    CREATE_NEW,
    EXPAND_NEW,
    CLOSE_FILES,
    INS_CPY_UNCOMPLETE,
    NEW_FILE_CRC_FAIL,
    BUILD_CODE_MISMATCH,
    SOURCE_OVERFLOW,
    TARGET_OVERFLOW,
    CLOSE_OLD,
    CLOSE_PATCH,
    READ_FILE,
    SCROLL_FILE,
    WRITE_FILE,
    NEW_FILE_SIZE,
    CRC_SCROLL,
    CRC_READ,
    READ_PATCH_CRC_NULL,
    READ_PATCH_LENGTH_NULL,
    READ_PATCH_LENGTH_OVER,
    READ_PATCH_LENGTH_FILE,
    READ_FILE_CRC,
    READ_FILE_LENGTH_NULL,
    READ_FILE_LENGTH_OVER,
    READ_PATCH_SIZE,
    PATCH_SIZE_MISMATCH,
    IM_DELETE,
    RENAME_IM,
    IM_CLEAR_LOG,
    IM_OPEN_LOG,
    IM_WRITE_LOG,
    IM_CLOSE_LOG
};


//-----------------------------     NWK     -------------------------

enum nwk_error_flags{

    RX_MSG_TYPE_NOT_RECOGNISED,
    SYS_RESET_INVALID_FW_VER
};

//-----------------------------     LUMINR     -------------------------

enum luminr_op_error_flags{

	AN_DC_MAX,
	AN_DC_MIN
};

//-----------------------------     CALENDAR     -------------------------

enum cal_process_error_flags{
    OPEN_CAL_FILE,
    READ_CAL_FILE,
	EMPTY_CAL_FILE,
	VERSION_CAL_FILE,
	FILE_REF_CAL_FILE,
	FILE_LEN_CAL_FILE_NULL,
	FILE_LEN_CAL_FILE_MIN,
	FILE_LEN_CAL_FILE_MAX,
    PROFILE_SECTION_LEN_MIN,
    PROFILE_SECTION_LEN_MAX,
	PROFILE_LEN_CAL_FILE,
    PROFILE_LEN_CAL_FILE_MIN,
    PROFILE_LEN_CAL_FILE_MAX,
    PROFILE_ID_ERROR_MAX,
    PROFILE_SP_SRC_INVALID,
    PROFILE_SP_THRESH_MIN,
    PROFILE_SP_THRESH_MAX,
    PROFILE_SP_THRESH_CONFLICT,
    PROFILE_SP_LAMP_OUTPUT_MAX,
    PROFILE_EXCEPT_LENGTH_MAX,
    PROFILE_EXCEPT_PAYLOAD_ERROR,
    PROFILE_EXCEPT_PAYLOAD_NULL,
    PROFILE_NOT_FOUND
};

//-----------------------------   FILE SYS ERRORS     -------------------------

enum file_sys_error_flags{
    FS_INIT_FLASH,
    FS_ERASE_FLASH,
    FS_REMOVING_OLD_FILE,
    FS_CREATING_OLD_FILE,
    FS_WRITING_OLD_FILE,
    FS_SEEK_OLD_FILE,
    FS_TRUNC_OLD_FILE,
    FS_CLOSE_OLD_FILE,
    FS_OLD_IM_LENGTH_NULL,
    FS_IM_LENGTH_NULL,
    FS_OPEN_IM_FILE,
    FS_SEEK_IM_FILE,
    FS_READ_IM_FILE,
    FS_CLOSE_IM_FILE,
    FS_PREP_SECTOR_FOR_ERASE,
    FS_ERASE_SECTOR,
    FS_PREP_SECTOR_FOR_WRITE,
    FS_RAM_TO_FLASH,
    FS_DELETE_NEW_IMAGE,
    FS_DELETE_OLD_IMAGE,
    FS_RENAME_NEW_IMAGE,
    FS_OPEN_LOG,
    FS_SEEK_LOG,
    FS_READ_LOG,
    FS_WRITE_LOG,
    FS_CLOSE_LOG,
    FS_DELETE_LOG,
    FS_LOG_CORRUPT,
    FS_BOOT_ATTEMPTS,
    FS_OPEN_OLD_IM_FILE,
    FS_REV_OLD_IM_LENGTH_NULL,
    FS_CLOSE_OLD_IM_FILE,
    FS_REVERTING,
    FS_UPDATING,
    FS_DELETING_NEW
};

//-----------------------------     GPS     -------------------------

enum gps_error_flags{
    GPS_ALL_OK,
    GPS_HW_NOT_FOUND,
    GPS_COORDS_NOT_FOUND
};

//-----------------------------     TILT     -------------------------
enum tilt_error_flags{
  TILT_ALL_OK,
  TILT_IMPACT_EVENT,
  TILT_RECOVERY_EVENT,
  TILT_HW_NO_RESP

};

//-----------------------------     PWR     -------------------------

enum pwr_error_flags{
    LAST_GASP_DETECTED,
    LAST_GASP_RECOVERED,
    NO_POWER_MEASUREMENTS,
    POWER_MEASUREMENTS_OK
};

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void sysmon_run(void);
void sysmon_assert_called(void);

void HardFault_Handler(void);
// void vApplicationMallocFailedHook(void);
//void vApplicationStackOverflowHook(TaskHandle_t xTask, char *pcTaskName );
void sysmon_report_error(fw_module_id stack_id, uint32_t error_code);
void set_sysmon_fault(uint32_t fault_code);
void clear_sysmon_fault(uint32_t fault_code);
bool get_sysmon_self_test_is_done(void);;
bool get_sysmon_fault_status( uint32_t fault_code);
void sysmon_update_dali_faults(uint16_t dali_status);

void sysmon_format_disk_and_reset(void);
void sysmon_system_reset(void);
uint32_t get_sysmon_fault_flags(void);
void set_sysmon_delay_reset(uint32_t delay_sec);

void sysmon_set_task_exe_timeout(task_id id);
void sysmon_disable_task_exe_timeout(task_id id);
void sysmon_check_tasks_watchdog(void);

void vStatusUplinkCallBack(void);
void sysmon_loadStatusPayload(bool applyDelay);
void sysmon_status_uplink_enable(void);
void sysmon_status_uplink_disable(void);

void clear_sysmon_last_gasp_flag(void);
void set_sysmon_last_gasp_flag(void);
bool get_sysmon_last_gasp_is_active(void);
void sysmon_check_last_gasp_timeout(void);

#endif /* PRIVATE_APPLICATION_SYSTEM_MONITORING_SYSMON_H_ */
