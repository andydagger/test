/*
==========================================================================
 Name        : sysmon.c
 Project     : pcore
 Path        : /pcore/src/private/application/system_monitoring/sysmon.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 4 Sep 2017
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/
#include "stdbool.h"
#include "stdint.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "sysmon.h"
#include "lorawan.h"
#include "debug.h"
#include "systick.h"
#include "common.h"
#include "flash.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "FreeRTOSMacros.h"
#include "config_tasks.h"
#include "rtc_procx.h"

#include "gps.h"
#include "config_timers.h"
#include "hw.h"
#include "config_lorawan.h"
#include "errorCode.h"

#include "compileUplink.h"
#include "nwk_com.h"
#include "productionMonitor.h"
#include "luminr_tek.h"
#include "im_main.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

static uint32_t fault_flags = (uint32_t)STARTING_MASK;
static uint32_t delayed_forced_reset_time_out_sec = (uint32_t)-1;

typedef struct
{
    bool enabled[NUM_TASKS];
    uint32_t ref_time_sec[NUM_TASKS];
}rtos_tasks_wdt;

static rtos_tasks_wdt sysmon_tasks_wdt;

static bool b_initial_uplink_sent = false;
static uint32_t self_test_flags = (uint32_t)SELF_TEST_MASK;

static bool b_last_gasp_status = false;
static uint32_t last_gasp_trigger_ref_sec = 0;

typedef struct
{
    fw_module_id stack_id;
    uint32_t error_code;
    uint32_t flag_position;
    bool b_set_flag;
    bool b_force_reset;
    uint32_t reset_delay_sec;
}error_to_fault_conv;


/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
extern QueueHandle_t xQuNwkCom; // vTaskNwkCom

extern TimerHandle_t xTimer[NUM_TIMERS];

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void sysmon_boot_up_sequence(void);
static void sysmon_send_initial_msg(void);

static void sysmon_self_test(uint32_t report_flag);
static void sysmon_gps_no_lock_time_out(void);

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Process error from queue msg
 * @param[in] void
 * @return void
 *******************************************************************************/
void sysmon_run(void){
    static uint32_t prev_fault_flags = (uint32_t)STARTING_MASK;
    bool b_fault_msg_priority = LOW_PRIORITY;
    uint8_t faultUplinkQoSLevel = QOS_0;

    // kick external watchdog
    hw_kick_watchdog();

    // Report fault flags if any flag changed status.
    if( prev_fault_flags != fault_flags){
        prev_fault_flags = fault_flags;
        debug_printf(SYSMON, "New Error Word: 0x%08lX\r\n", fault_flags);

        // Change to high-priority if power-failure active
        if( fault_flags & ( 1 << POWER_FAILURE) ){
            b_fault_msg_priority = HIGH_PRIORITY;
            faultUplinkQoSLevel = QOS_1;
        }

        if(true == b_initial_uplink_sent){
            compileUplink_Fault(b_fault_msg_priority, faultUplinkQoSLevel, fault_flags);
        }else{
            // Only monitor changes in faults flags AND start reporting
            // changes AFTER the initial uplink has been transmitted.
        }
    }

    // Service boot-up sequence until initial uplink sent
    if( false == b_initial_uplink_sent ){
        sysmon_boot_up_sequence();
    }

    // Check if node needs a forced hard power-cycle
    if( get_systick_sec() > delayed_forced_reset_time_out_sec){
        sysmon_system_reset();
    }

    // Service GPS nop lock time-out
    sysmon_gps_no_lock_time_out();

    return;
}

/*******************************************************************************
 * @brief Stack error reporting
 * @param[in] void
 * @return void
 *******************************************************************************/
void sysmon_report_error(fw_module_id stack_id, uint32_t error_code){

	// Only print error message - decoding matrix not being used
	debug_printf(	SYSMON,
					"Error: StackId | ErrCode = %d | %d\r\n",
					stack_id,
					error_code );
	return;
}


/*******************************************************************************
 * @brief Set fault flag
 * @param[in] void
 * @return void
 *******************************************************************************/
void set_sysmon_fault(uint32_t fault_code){
    sysmon_self_test(fault_code);

    // Only debug printf when flag changes value
    if(get_sysmon_fault_status(fault_code) == false)
        debug_printf(SYSMON, "Set Flag %lu\r\n", fault_code);

    // Some faults are mapped on Node One Build Code F6810
    if( 0 == strncmp(BUILD_CODE, "F681", 4) ){
        // If this is a NODE1 hardware ignore GPS and Bluetooth faults
        if( (1<<fault_code) & NODE_ONE_MASK){
            debug_printf(SYSMON, "NODE1 > Ignore Flag %lu !!\r\n", fault_code);
            return;
        }
    }

    // All faults processed on NODE2 build
    fault_flags |= (1<<fault_code);

    return;
}

/*******************************************************************************
 * @brief Clear fault flag
 * @param[in] void
 * @return void
 *******************************************************************************/
void clear_sysmon_fault(uint32_t fault_code){
    sysmon_self_test(fault_code);

    // Only debug printf when flag changes value
    if(get_sysmon_fault_status(fault_code) == true)
        debug_printf(SYSMON, "Clear Flag %lu\r\n", fault_code);

    fault_flags &= ~(1<<fault_code);
    return;
}

/*******************************************************************************
 * @brief get fault flag status
 * @param[in] uint32_t fault code
 * @return bool
 *******************************************************************************/
bool get_sysmon_fault_status( uint32_t fault_code)
{
    if( fault_flags & (1 << fault_code))
    {
    	return true;
    }

    return false;
}

/*******************************************************************************
 * @brief Clear fault flag
 * @param[in] void
 * @return void
 *******************************************************************************/
void sysmon_update_dali_faults(uint16_t dali_status){
    // shift dali status
    uint32_t dali_status_section = ((uint32_t)dali_status) & 0x0000FFF8;
    // overwrite dali section only
    fault_flags = dali_status_section | (fault_flags & 0xFFFF001F);
    return;
}

/*******************************************************************************
 * @brief Check if all required stacks have reported on their test
 * @param[in] void
 * @return bool true for all tests done
 *******************************************************************************/
bool get_sysmon_self_test_is_done(void){
    return (0x00000000 == self_test_flags) ? true : false;
}

/*******************************************************************************
 * @brief Check if all required stacks have reported on their test
 * @param[in] void
 * @return bool true for all tests done
 *******************************************************************************/
static void sysmon_self_test(uint32_t report_flag){
    if(false == get_sysmon_self_test_is_done()){
        // clear corresponding flag
        self_test_flags &= ~(1<<report_flag);
    }
    return;
}

/*******************************************************************************
 * @brief Turn off gps hw if no lock for more than 24hrs
 * @param[in] void
 * @return bool true for all tests done
 *******************************************************************************/
static void sysmon_gps_no_lock_time_out(void){
    static bool b_gps_lock_timed_out = false;
    // if already processed just return
    if(true == b_gps_lock_timed_out){
        return;
    }

    // Have we timed-out?
    if( get_systick_sec() > GPS_NO_LOCK_POWER_OFF_SEC ){
        b_gps_lock_timed_out = true;

        // timed out and no lock > turn off
        if( fault_flags & (1<<NO_GPS_LOCK) ){
            debug_printf(SYSMON, "Powering off GPS Hardware.\r\n");
            gps_disable_module();
        }

    }

    return;
}

/*******************************************************************************
 * @brief Hard Fault Handler
 * @param[in] void
 * @return void
 *******************************************************************************/
void HardFault_Handler(void)
{
    taskENTER_CRITICAL();
    errorCode_storeCode(SYSTEM_FAULT, HARD_FAULT);
    debug_print_instant(SYSMON, "****** HARD FAULT ******\r\n");
    while(1);
}

/*******************************************************************************
 * @brief Malloc error catch routine
 * @param[in] void
 * @return void
 *******************************************************************************/
void vApplicationMallocFailedHook(void)
{
    taskENTER_CRITICAL();
    errorCode_storeCode(SYSTEM_FAULT, MALLOC_FAULT);
    debug_print_instant(SYSMON, "Failed Malloc Hook\r\n");
    while(1);
}

/*******************************************************************************
 * @brief FreeRTOS stack overflow hook
 * @param[in] void
 * @return void
 *******************************************************************************/
void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName)
{

    /* Run time stack overflow checking is performed if
       configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
       function is called if a stack overflow is detected. */
    taskENTER_CRITICAL();
    errorCode_storeCode(STACK_OVERFLOW, (uint8_t)pcTaskName[0]);
    debug_print_instant(SYSMON, "\r\n");
    debug_print_instant(SYSMON, "\r\n");
    debug_print_instant(SYSMON, "Stack Overflow  Hook on:\r\n");
    debug_print_instant(SYSMON, pcTaskName);
    debug_print_instant(SYSMON, "\r\n");
    while(1);
}

/*******************************************************************************
 * @brief This will wipe the whole flash content
 * @param[in] void
 * @return void
 *******************************************************************************/
void sysmon_format_disk_and_reset(void){
    //---------------------------------------------------------
    vTaskSuspendAll();
    debug_printf(SYSMON, "Reset Drive...\r\n");
    flash_disk_initialise();
    flash_wipe_memory();
    xTaskResumeAll();
    //---------------------------------------------------------
    debug_printf(SYSMON, "Reset System in 60 sec.\r\n");
    set_sysmon_delay_reset(60);
    return;
}

/*******************************************************************************
 * @brief This will force a reset by the external watchdog
 * @param[in] void
 * @return void
 *******************************************************************************/
void sysmon_system_reset(void){
    errorCode_storeCode(SYSTEM_FAULT, REQ_SYS_RESET);
    // Save session info before reseting node
    nwk_com_save_session_info();
    // Stop RTOS and force external wdt to time-out
    taskENTER_CRITICAL();
    debug_print_instant(SYSMON, "Saved Session Info.\r\n");
    debug_print_instant(SYSMON, "Reset System...\r\n");
    while(1);
    return;
}

void set_sysmon_delay_reset(uint32_t delay_sec){
    delayed_forced_reset_time_out_sec = get_systick_sec() + delay_sec;
    return;
}

/*******************************************************************************
 * @brief Return the System Monitoring 32 bit fault flags
 * @param[in] void
 * @return uint32_t fault flags
 *******************************************************************************/
uint32_t get_sysmon_fault_flags(void){
    return fault_flags;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

/*******************************************************************************
 * @brief Enable and capture time ref for task exe watchdog
 * @param[in] void
 * @return void
 *******************************************************************************/
void sysmon_set_task_exe_timeout(task_id id){
    sysmon_tasks_wdt.ref_time_sec[id] = get_systick_sec();
    sysmon_tasks_wdt.enabled[id] = true;
    return;
}

/*******************************************************************************
 * @brief Disable task exe watchdog
 * @param[in] void
 * @return void
 *******************************************************************************/
void sysmon_disable_task_exe_timeout(task_id id){
    sysmon_tasks_wdt.enabled[id] = false;
    return;
}

/*******************************************************************************
 * @brief Check all tasks exe watchdog
 * @param[in] void
 * @return void
 *******************************************************************************/
void sysmon_check_tasks_watchdog(void){
    uint8_t id = 0;
    uint32_t elapsed_time = 0;
    vTaskSuspendAll();
    do{
        elapsed_time = get_systick_sec() - sysmon_tasks_wdt.ref_time_sec[id];
        if( ( elapsed_time > (uint32_t)TASK_EXE_TIMEOUT_SEC)
                && (sysmon_tasks_wdt.enabled[id] == true)){
            taskENTER_CRITICAL();
            errorCode_storeCode(TASK_TIME_OUT, id);                        
            debug_print_instant(    SYSMON,
                                    "Task %d freeze >> force reboot...\r\n",
                                    id);
            while(1);
        }
    }while(++id < TASK_SYS_MON);

    xTaskResumeAll();
    return;
}

/*******************************************************************************
 * @brief Select boot-up sequence depending on whether or not the node
 * is in a commissioned state
 * @param[in] void
 * @return void
 *******************************************************************************/
static void sysmon_boot_up_sequence(void){
    // DO not send INITIAL uplink if unit is under production test.
    if(productionMonitor_getUnitIsUndertest() == false)
    {
        if( (get_sysmon_self_test_is_done() == true) &&
            (get_luminr_dataset_is_ready() == true) ){
            sysmon_send_initial_msg();
            b_initial_uplink_sent = true;
        }        
    }
    return;
}

/*******************************************************************************
 * @brief Compile and send INITIAL uplink
 * @param[in] void
 * @return true if sent to nwk task queue successfully
 *******************************************************************************/
static void sysmon_send_initial_msg(void){
    compileUplink_Initial();
    return;
}

/*******************************************************************************
 * @brief transmits 30 min payload as unacked uplink
 * @param[in] void
 * @return void
 *******************************************************************************/
void vStatusUplinkCallBack(void){
    sysmon_loadStatusPayload(true);
}

void sysmon_loadStatusPayload(bool applyDelay)
{
    compileUplink_Status(applyDelay);
}

/*******************************************************************************
 * @brief Enable periodic status uplinks
 * @param[in] void
 * @return void
 *******************************************************************************/
void sysmon_status_uplink_enable(void){
    // Start the timer (auto-repeat)
    xTimerStart(xTimer[STATUS_UPLINK], 0);
    return;
}

/*******************************************************************************
 * @brief Disable periodic status uplinks
 * @param[in] void
 * @return void
 *******************************************************************************/
void sysmon_status_uplink_disable(void){
    // Stop the timer
    xTimerStop(xTimer[STATUS_UPLINK], 0);
    return;
}


void clear_sysmon_last_gasp_flag(void){
    b_last_gasp_status = false;
    return;
}

void set_sysmon_last_gasp_flag(void){
    b_last_gasp_status = true;
    last_gasp_trigger_ref_sec = get_systick_sec();
    return;
}

bool get_sysmon_last_gasp_is_active(void){
    return b_last_gasp_status;
}

/*******************************************************************************
 * @brief service last gasp timeout when acrive
 * @param[in] void
 * @return void
 *******************************************************************************/
void sysmon_check_last_gasp_timeout(void){
    // if last gasp is not active just return false
    if(true == b_last_gasp_status ){
        // See if the node power came back before the super-cap died
        if( (get_systick_sec() - last_gasp_trigger_ref_sec) > LAST_GASP_TIME_OUT_SEC){
            // resume normal operation if it's been more than 2 minutes
            debug_printf(SYSMON, "Resuming normal nwk operation.\r\n");
            sysmon_report_error(PWR, LAST_GASP_RECOVERED);
        }
    }
    return;
}
