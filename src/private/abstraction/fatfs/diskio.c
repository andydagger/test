/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2016        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "stdbool.h"
#include "stdint.h"
#include "diskio.h"		/* FatFs lower layer API */
#include "flash.h"
#include "debug.h"
#include "systick.h"
#include "common.h"
#include "stddef.h"


typedef struct
{
    DRESULT res;
    const char * result_string;
}dresult_struct;

/*******************************************************************************
 * @brief Returns the drive status
 * @param[in] Physical drive nmuber to identify the drive - ignored
 * @return DSTATUS contains STA_PROTECT and STA_NOINIT flag status
 *******************************************************************************/
DSTATUS disk_status ( BYTE pdrv ) // doc = "pdrv always zero at single drive system"
{

	DSTATUS disk_status_res = STA_ALLOK;
	BYTE flash_status = 0x00;

	// if get status failed return not-initialised
	if( flash_disk_status(&flash_status) == false ){
//	    debug_printf(FLASH, "disk_status = %d => NO_INIT\r\n", STA_NOINIT);
	    return STA_NOINIT;
	}else{}

	// Process returned status byte

	if( flash_status && FLASH_STATUS_BITS_WEL ){
	    // Device is write enabled
	    // Leave STA_PROTECT flag clear
	}else{
	    // Device is not write-enabled
        disk_status_res |= STA_PROTECT;
	}

	if( flash_status && FLASH_STATUS_BITS_INIT ){
        // Device was initialised
	    // Leave STA_NOINIT clear
	}else{
        // Device not yet initialised
	    disk_status_res |= STA_NOINIT;
	}

//	debug_printf(FLASH, "disk_status = %d\r\n", disk_status_res);
	return disk_status_res;
}

/*******************************************************************************
 * @brief Initialises the storage device and put it ready to generic read/write
 * @param[in] Physical drive nmuber to identify the drive
 * @return Clear STA_NOINIT flag when successfully initialised
 *******************************************************************************/
// doc = "pdrv always zero at single drive system"
DSTATUS disk_initialize ( BYTE pdrv )
{
    debug_printf(FLASH, "disk_initialize\r\n");
    return ( flash_disk_initialise() == true ) ? STA_ALLOK : STA_NOINIT;

}

/*******************************************************************************
 * @brief Called to read data from the sector(s) of storage device
 * @param[in] See below
 * @return Read operation result OK, ERROR, PARERR, NOTRDY
 *******************************************************************************/
DRESULT disk_read (
	BYTE pdrv,		/* doc = "pdrv always zero at single drive system */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{

    debug_printf(FLASH, "disk_read(0x%08x, %lu, %lu)\r\n", buff, sector, count);

    // check for validity of given parameters
	if( ( sector + count ) > FLASH_N_SECTORS){
	    return RES_PARERR;
	}else{}

	// check buff address is valid
	if( buff == NULL){
	    return RES_PARERR;
	}else{}

	// TODO: check buffer won't overflow => RES_PARERR


	// First check that the flah has been initialised
	if(get_flash_is_initialised() != true){
	    return RES_NOTRDY;
	}else{}

	// instruct flash read
    if( flash_disk_read( (uint8_t *)buff, sector, count) == true){
        return RES_OK;
    }else{
        return RES_ERROR;
    }
}

/*******************************************************************************
 * @brief Called to write data to the sector(s) of storage device
 * @param[in] See below
 * @return Read operation result OK, ERROR, PARERR, NOTRDY
 *******************************************************************************/
DRESULT disk_write (
	BYTE pdrv,			// doc = "pdrv always zero at single drive system"
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{

    debug_printf(FLASH, "disk_write(0x%08x, %lu, %lu)\r\n", buff, sector, count);

    // check for validity of given parameters
    if( ( sector + count ) > FLASH_N_SECTORS){
        return RES_PARERR;
    }else{}

    // check buff address is valid
    if( buff == NULL){
        return RES_PARERR;
    }else{}

    // First check that the flah has been initialised
    if(get_flash_is_initialised() != true){
        return RES_NOTRDY;
    }else{}

    // instruct flash read
    if( flash_disk_write( (uint8_t *)buff, sector, count) == true){
        return RES_OK;
    }else{
        return RES_ERROR;
    }

}



/*******************************************************************************
 * @brief Called to control device specific features and miscellaneous functions
 *        other than generic read/write
 * @param[in] See below
 * @return Operation result OK, ERROR, PARERR, NOTRDY
 *******************************************************************************/
DRESULT disk_ioctl (
	BYTE pdrv,		// doc = "pdrv always zero at single drive system"
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{

    debug_printf(FLASH, "disk_ioctl(%lu)\r\n", cmd);

    //BYTE flash_ioctl_op_status = FLASH_ERR;
    DRESULT ioctl_op_result = RES_NOTRDY;

    switch(cmd){

    case CTRL_SYNC:
        ioctl_op_result = RES_OK; // blocking write routine implemente
        break;

    case GET_SECTOR_COUNT:
        *(DWORD*)buff = (DWORD)FLASH_N_SECTORS;
        ioctl_op_result = RES_OK;
        break;

    case GET_BLOCK_SIZE:
        *(DWORD*)buff = (DWORD)1; // set as unknown as the block size is variable
        ioctl_op_result = RES_OK;
        break;

    default:
        ioctl_op_result = RES_PARERR;
        break;
    }

    /*
     * Not Implemented:
     * GET_SECTOR_COUNT as FF_USE_MKFS = 0
     * GET_SECTOR_SIZE as FF_MAX_SS = FF_MIN_SS
     * GET_BLOCK_SIZE as FF_USE_MKFS = 0
     * CTRL_TRIM as FF_USE_TRIM  = 0
     */

    return ioctl_op_result;
}

/*******************************************************************************
 * @brief Get the RTC current date/time stamp
 * @param[in] void
 * @return DWORD time_stamp
 *******************************************************************************/
DWORD get_fattime (void)
{
    DWORD time_stamp = 0;
    uint32_t hr, min, sec, msec;
    /*
     * Doc format instructions:
     * bit31:25 Year origin from the 1980 (0..127, e.g. 37 for 2017)
     * bit24:21 Month (1..12)
     * bit20:16 Day of the month(1..31)
     * bit15:11 Hour (0..23)
     * bit10:5 Minute (0..59)
     * bit4:0 Second / 2 (0..29, e.g. 25 for 50)
     */

    // Work out the timestamp
    msec = get_systick_ms();
    hr = msec / 3600000;
    msec -= hr * 3600000;
    min = msec / 60000;
    msec -= min * 60000;
    sec = msec / 1000;
    msec -= sec * 1000;

    time_stamp |= (DWORD) (38 << 25); // load year
    time_stamp |= (DWORD) (1 << 21); // load month
    time_stamp |= (DWORD) (31 << 16); // load day
    time_stamp |= (DWORD) (hr << 11); // load hour
    time_stamp |= (DWORD) (min << 5); // load min
    time_stamp |= (DWORD) ( (sec/2) & 0x1F ); // load sec


    return time_stamp;
}
