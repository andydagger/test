/*
==========================================================================
 Name        : luminr_tek.c
 Project     : pcore
 Path        : /pcore/src/private/abstraction/luminr_tek.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 14 Apr 2018
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include "stdint.h"
#include "stdbool.h"
#include "stdio.h"
#include <string.h>
#include "lorawan.h"
#include "lorawanhw.h"
#include "common.h"
#include "systick.h"
#include "debug.h"
#include "DaliController.h"
#include "DaliAL.h"
#include "luminr_tek.h"
#include "sysmon.h"
#include "hw.h"
#include "config_dali.h"
#include "adc_procx.h"
#include "config_an_driver.h"
#include "otot.h"
#include "ztot.h"
#include "config_dataset.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "FreeRTOSMacros.h"
#include "FreeRTOSCommonHooks.h"
#include "config_queues.h"
#include "relay.h"


/*****************************************************************************
 * Private functions
 ****************************************************************************/

#define DRIVER_SEARCH_TO_SEC (uint32_t)20

static void detect_lamp_driver_tek(void);

static bool luminr_tek_dali_init(void);
static void luminr_tek_dali_service(void);
static bool luminr_tek_dali_update_lamp_output(uint8_t level);

static bool luminr_tek_ztot_init(void);
static void luminr_tek_ztot_service(void);
static bool luminr_tek_ztot_update_lamp_output(uint8_t level);

static bool luminr_tek_otot_init(void);
static void luminr_tek_otot_service(void);
static bool luminr_tek_otot_update_lamp_output(uint8_t level);

static void boot_up_sequence(uint32_t startTime);

//----------------------------------------------------------------
//           BOOT_UP SEQUENCE TIMING PARAMETERS
//----------------------------------------------------------------
#define BOOT_UP_UNKNOWN_DURATION_MS     (uint32_t)(30*1000)
#define BOOT_UP_ON_DURATION_MS          (uint32_t)(30*1000)
#define BOOT_UP_OFF_DURATION_MS         (uint32_t)(5*60*1000)
#define PRODUCTION_CALIBRATION_DELAY_MS (uint32_t)(30*60*1000)
//----------------------------------------------------------------

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static const struct_luminr_tek_routing update_lamp_output_matrix[] =
{
        { LAMP_TEK_SWITCH,      luminr_tek_otot_init,
                                luminr_tek_otot_service,
                                luminr_tek_otot_update_lamp_output},
		{ LAMP_TEK_DALI, 		luminr_tek_dali_init,
								luminr_tek_dali_service,
								luminr_tek_dali_update_lamp_output},
		{ LAMP_TEK_ZERO_TO_TEN, luminr_tek_ztot_init,
								luminr_tek_ztot_service,
								luminr_tek_ztot_update_lamp_output},
		{ LAMP_TEK_ONE_TO_TEN, 	luminr_tek_otot_init,
								luminr_tek_otot_service,
								luminr_tek_otot_update_lamp_output},
};

static uint8_t lamp_tek = LAMP_TEK_ZERO_TO_TEN;

static LuminrDataset luminr_dataset = {0};

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
extern QueueHandle_t xQuTxData;

/*****************************************************************************
 * Public functions
 ****************************************************************************/

//-----------------------------------------------------------------------------
//								INITIALISE
//-----------------------------------------------------------------------------

static void boot_up_sequence(uint32_t startTime)
{
    // Lamp should be kept ON for BOOT_UP_ON_DURATION_MS
    do
    {
        FreeRTOSDelay(100);
        luminr_tek_service();
    }
    while((get_systick_ms() - startTime) < BOOT_UP_ON_DURATION_MS);

    if( lamp_tek == LAMP_TEK_DALI)
    {
        debug_printf(LUMINR, "Driver Found Stay off for 5min\r\n");
        luminr_tek_update_lamp_output((uint8_t)0); 
    }
    else
    {
        debug_printf(LUMINR, "Driver NOT Found Stay on for 5min\r\n");
        luminr_tek_update_lamp_output((uint8_t)100);
    }

    // Lamp should be kept OFF for BOOT_UP_OFF_DURATION_MS if DALI found
    do
    {
        FreeRTOSDelay(100);
        luminr_tek_service();
    }
    while((get_systick_ms() - startTime) < BOOT_UP_ON_DURATION_MS + BOOT_UP_OFF_DURATION_MS);
}

/*******************************************************************************
 * @brief Initialise the luminr stack and interface
 * @param[in] void
 * @return true if executed successfully
 *******************************************************************************/
bool luminr_tek_init(void){
    uint32_t refTimeStartDaliDriverSearchMs = 0;

    // need to wait before attempting to operate the relay
    FreeRTOSDelay(BOOT_UP_UNKNOWN_DURATION_MS);

//    debug_printf(LUMINR, "ST not passed = %s\r\n",(get_pwr_calc_power_self_test_not_completed())?"true":"false");
//    debug_printf(LUMINR, "ZPC not performed = %s\r\n",(get_pwr_calc_power_zero_point_cal_not_performed())?"true":"false");
//
//    // Check if in production test
//    // Only valid if both self-test AND zero-point-cal not yet executed
//    if( (true == get_pwr_calc_power_self_test_not_completed()) &&
//        (true == get_pwr_calc_power_zero_point_cal_not_performed()) ){
//        // Set Power Board Self Test Failed
//        debug_printf(LUMINR, "Applying 30min delay.\r\n");
//        set_sysmon_fault(PWR_ST_FAILED);
//        // Keep relay open for log delay
//        relay_update_wait(RELAY_OPEN);
//        FreeRTOSDelay(PRODUCTION_CALIBRATION_DELAY_MS);
//    }else{
//        // STM8 does not need zero-cal so can start run-time code
//        clear_sysmon_fault(PWR_ST_FAILED);
//    }

    // Close the relay so that the driver can be detected
    relay_update_wait(RELAY_CLOSE);
    FreeRTOSDelay(1000);

    // Detects the driver tek
    refTimeStartDaliDriverSearchMs = get_systick_ms();
    debug_printf(LUMINR, "Starting Driver Detect...\r\n");
    detect_lamp_driver_tek();

    boot_up_sequence(refTimeStartDaliDriverSearchMs);

    // Set luminr dataset as ready
    luminr_dataset.b_data_is_ready = true;

    // Start with o/p at 0%
    luminr_tek_update_lamp_output((uint8_t)0);

    // init functions called as part of detect algorithm

	return true;
}

/*******************************************************************************
 * @brief Get Luminr Dataset Content
 * @param[in] ptr to target dataset
 * @return void
 *******************************************************************************/
void get_luminr_tek_dataset(LuminrDataset * dataset){
    // Prevent the RTOS kernel swapping out the task whilst
    // the dataset is being retrieved.
    vTaskSuspendAll();
    memcpy( (uint8_t *) dataset,
            (uint8_t *) &luminr_dataset,
            sizeof luminr_dataset);
    // The operation is complete.  Restart the RTOS kernel.
    xTaskResumeAll();
    return;
}

/*******************************************************************************
 * @brief Get Luminr Dataset Status
 * @param[in] void
 * @return bool true for ready to b eread
 *******************************************************************************/
bool get_luminr_dataset_is_ready(void){
    return luminr_dataset.b_data_is_ready;
}


/*******************************************************************************
 * @brief Detects the lamp driver technology
 * @param[in] void
 * @return void
 *******************************************************************************/
static void detect_lamp_driver_tek(void){

	// Attempt to find dali driver
	if( luminr_tek_dali_init() == true){
		lamp_tek = LAMP_TEK_DALI;
		debug_printf(LUMINR, "DALI driver connected!\r\n");
		luminr_dataset.info.driver_and_lamp_type = lamp_tek;
		ZaghaController_getDataset(&luminr_dataset);
		return;
	}else{}

	// checking fro 0-to-10 driver
	luminr_tek_otot_init();
	an_driver_set_pwm_dc(100);
	an_driver_start_pwm();

	// Wait for capacitor to charge up
	FreeRTOSDelay(10000);

	// if analog input shows high value 1 to 10 is valid
	if( get_adc_procx_an_driver_in_av_volt() > ANALOG_IN_DETECT_TH_VOLT){
		luminr_tek_otot_init();
		lamp_tek = LAMP_TEK_ONE_TO_TEN;
		luminr_dataset.info.driver_and_lamp_type = lamp_tek;
		debug_printf(LUMINR, "1-to-10 driver connected!\r\n");
	}else{
		// must be 0 to 10 driver
		luminr_tek_ztot_init();
		lamp_tek = LAMP_TEK_ZERO_TO_TEN;
		luminr_dataset.info.driver_and_lamp_type = lamp_tek;
		debug_printf(LUMINR, "0-to-10 driver connected!\r\n");
	}
	return;
}

/*******************************************************************************
 * @brief Initialise dali interface
 * @param[in] void
 * @return true if driver was found
 *******************************************************************************/
static bool luminr_tek_dali_init(void){
	uint32_t startOfDriverSearchSec = 0;
	// set o/p pins as required
	an_driver_set_an_smooth(IO_LOW);
	an_driver_disable_pwm_pin(); // pin set high by default
	dali_set_psu(IO_HIGH);
    dali_out(true); // set DALI line high

	// wait for 5 sec for Driver to start
	FreeRTOSDelay(5000);

    // this needs to be called before initialise driver with more
    // than 0% to set state machine to relay on
    ZaghaController_updateLampOutput(100);

    // Wait to find driver
    startOfDriverSearchSec = get_systick_sec();
    while(ZaghaController_init() == false)
    {
        // Free Up RTOS fo debug and other tasks
        FreeRTOSDelay(100);
    	// Driver search time-out
    	if((get_systick_sec() - startOfDriverSearchSec) > DRIVER_SEARCH_TO_SEC)
        {
    		debug_printf(LUMINR, "No DALI driver detecetd!\r\n");
    		return false;
        }
    }

    debug_printf(LUMINR, "DALI driver found!!\r\n");
    return true;
}

/*******************************************************************************
 * @brief Initialise zero-to-ten interface
 * @param[in] void
 * @return true if driver was found
 *******************************************************************************/
static bool luminr_tek_ztot_init(void){
	// set o/p pins as required
	an_driver_enable_pwm_pin();
	dali_set_psu(IO_HIGH);
	dali_out(IO_HIGH);
	an_driver_set_an_smooth(IO_HIGH);
	return true;
}

/*******************************************************************************
 * @brief Initialise one-to-ten interface
 * @param[in] void
 * @return true if driver was found
 *******************************************************************************/
static bool luminr_tek_otot_init(void){
	// set o/p pins as required
	an_driver_enable_pwm_pin();
	dali_set_psu(IO_LOW);
	dali_out(IO_HIGH);
	an_driver_set_an_smooth(IO_HIGH);
	return true;
}

//-----------------------------------------------------------------------------
//								SERVICE
//-----------------------------------------------------------------------------

/*******************************************************************************
 * @brief Service luminr driver interface
 * @param[in] void
 * @return true if executed successfully
 *******************************************************************************/
void luminr_tek_service(void){

    // service relay pulse
    relay_service_pulse();

	// call appropriate service function
    if(lamp_tek != LAMP_TEK_SWITCH){
    	return (*update_lamp_output_matrix[lamp_tek].func_service)();
    }else{}

	return;
}

/*******************************************************************************
 * @brief Service dali stack
 * @param[in] void
 * @return void
 *******************************************************************************/
static void luminr_tek_dali_service(void){
    ZaghaController_service();
	return;
}


/*******************************************************************************
 * @brief Service zero-to-ten stack
 * @param[in] void
 * @return void
 *******************************************************************************/
static void luminr_tek_ztot_service(void){
	static uint8_t tick_count = 0;
	if(++tick_count > 50){
		tick_count = 0;
		ztot_service();
	}else{}

	return;
}

/*******************************************************************************
 * @brief Service one-to-ten stack
 * @param[in] void
 * @return void
 *******************************************************************************/
static void luminr_tek_otot_service(void){
	static uint8_t tick_count = 0;
	if(++tick_count > 50){
		tick_count = 0;
		otot_service();
	}
	return;
}

//-----------------------------------------------------------------------------
//								UPDATE OUTPUT
//-----------------------------------------------------------------------------

/*******************************************************************************
 * @brief Process brightness change
 * @param[in] void
 * @return true if executed successfully
 *******************************************************************************/
bool luminr_tek_update_lamp_output(uint8_t level){
	uint8_t lamp_op = 0;
	lamp_op = (level&0x7F);

    // first activate the relay accordingly
    if(lamp_op > 0){
        relay_update(RELAY_CLOSE);
    }else{
        relay_update(RELAY_OPEN);
    }

    if(lamp_tek != LAMP_TEK_SWITCH){
        debug_printf(LUMINR, "Update output to: %d %%\r\n", lamp_op);
        // call the corresponding function
        return (*update_lamp_output_matrix[lamp_tek].func_update)(lamp_op);
    }else{
        debug_printf(LUMINR, "Luminr Tek still unknown!\r\n");
        return false;
    }
}


/*******************************************************************************
 * @brief Pass brightness level to dali stack
 * @param[in] void
 * @return true if passed to dali stack and false if no driver connected.
 *******************************************************************************/
static bool luminr_tek_dali_update_lamp_output(uint8_t level){
	// TODO: report error if out of range returned
	return ZaghaController_updateLampOutput(level);

}

/*******************************************************************************
 * @brief Pass brightness level to sero-to-ten stack
 * @param[in] void
 * @return true if executed successfully
 *******************************************************************************/
static bool luminr_tek_ztot_update_lamp_output(uint8_t level){
	return set_ztot_lamp_output(level);
}

/*******************************************************************************
 * @brief Pass brightness level to one-to-ten stack
 * @param[in] void
 * @return true if executed successfully
 *******************************************************************************/
static bool luminr_tek_otot_update_lamp_output(uint8_t level){
	return set_otot_lamp_output(level);
}



