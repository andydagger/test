/*
==========================================================================
 Name        : luminr_tek.h
 Project     : pcore
 Path        : /pcore/src/private/abstraction/luminr_tek.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 14 Apr 2018
 Description : 
==========================================================================
*/

#ifndef PRIVATE_ABSTRACTION_LUMINR_TEK_H_
#define PRIVATE_ABSTRACTION_LUMINR_TEK_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "config_dataset.h"
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
enum lamp_tek_types{
	LAMP_TEK_SWITCH,
	LAMP_TEK_DALI,
	LAMP_TEK_ZERO_TO_TEN,
	LAMP_TEK_ONE_TO_TEN
};

typedef struct
{
    uint8_t tek_type;
    bool (*func_init)(void);
    void (*func_service)(void);
    bool (*func_update)(uint8_t level);
}struct_luminr_tek_routing;

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
bool luminr_tek_init(void);
void luminr_tek_service(void);
bool luminr_tek_update_lamp_output(uint8_t level);

void get_luminr_tek_dataset(LuminrDataset * dataset);
bool get_luminr_dataset_is_ready(void);

#endif /* PRIVATE_ABSTRACTION_LUMINR_TEK_H_ */
