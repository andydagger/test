/*
==========================================================================
 Name        : powerMonitor_impl.h
 Project     : pcore
 Path        : /pcore/src/private/abstraction/power/powerMonitor_impl.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 16th June 2021
 Description : Power monitor abstraction implementation layer defines etc
==========================================================================
*/

/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PRIVATE_ABSTRACTION_POWER_POWERMONITOR_IMPL_H_
#define PRIVATE_ABSTRACTION_POWER_POWERMONITOR_IMPL_H_

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#include "powerMonitor.h"

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public Variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

powerMonitor_HWType powerMonitor_detectHW( void);
void powerMonitor_resetDataset( PowerCalcDataset * dataset_ptr);
void powerMonitor_resultHandler(powerMonitor_Result result);

/*------------------------------- End of File --------------------------------*/
#endif /* PRIVATE_ABSTRACTION_POWER_POWERMONITOR_IMPL_H_ */
