/*
==========================================================================
 Name        : powerMonitor.h
 Project     : pcore
 Path        : /pcore/src/private/abstraction/power/powerMonitor.h
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 6th April 2021
 Description : Power monitor abstraction layer defines etc
==========================================================================
*/

/* Define to prevent recursive inclusion -----------------------------------*/
#ifndef PRIVATE_ABSTRACTION_POWER_POWERMONITOR_H_
#define PRIVATE_ABSTRACTION_POWER_POWERMONITOR_H_

#include "config_dataset.h"
#include "stdint.h"
#include "stdbool.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

typedef enum _Tag_PowerMonitorHW
{
    POWER_MONITOR_HW_UNKNOWN = 0,
    POWER_MONITOR_HW_STM8,
	POWER_MONITOR_HW_MICROCHIP,
	POWER_MONITOR_HW_ADI_ADE9153A,
	POWER_MONITOR_HW_MAX
} powerMonitor_HWType;

typedef enum _Tag_PowerMonitorResult
{
	POWER_MONITOR_RESULT_IDLE = 0,
	POWER_MONITOR_RESULT_OK,
	POWER_MONITOR_RESULT_HW_NOT_DETECTED,
	POWER_MONITOR_RESULT_TIMEOUT,
	POWER_MONITOR_RESULT_DATA_ERROR,
	POWER_MONITOR_RESULT_PARAM_ERROR,
	POWER_MONITOR_RESULT_POWER_FAIL,
	POWER_MONITOR_RESULT_POWER_RESTORE,
	POWER_MONITOR_RESULT_CALIBRATION_ACTIVE,
	POWER_MONITOR_RESULT_CALIBRATION_FAILED,
	POWER_MONITOR_RESULT_MAX
} powerMonitor_Result;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
#define powerMonitor_uartISR UART1_IRQHandler


/*****************************************************************************
 * Public Variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

powerMonitor_HWType powerMonitor_getHWType( void);
void powerMonitor_init( void);
void powerMonitor_initMeasurement( void);
void powerMonitor_service( void);
powerMonitor_Result powerMonitor_getDataset( PowerCalcDataset * dataset_ptr);
powerMonitor_Result powerMonitor_calibrate( void);
bool powerMonitor_isCalibrationActive( void);
powerMonitor_Result powerMonitor_getCalibReadings( PowerCalcDataset * dataset_ptr);
powerMonitor_Result powerMonitor_applyCalibReadings(PowerCalcDataset * meter_dataset_ptr );

/*------------------------------- End of File --------------------------------*/
#endif /* PRIVATE_ABSTRACTION_POWER_POWERMONITOR_H_ */
