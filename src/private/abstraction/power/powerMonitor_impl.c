/*
==========================================================================
 Name        : powerMonitor_impl.c
 Project     : pcore
 Path        : /pcore/src/private/abstraction/power/powerMonitor_impl.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 16th June 2021
 Description : Power monitor abstraction implementation layer
==========================================================================
*/

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "debug.h"
#include "FreeRTOS.h"
#include "task.h"
#include "config_dataset.h"
#include "sysmon.h"
#include "powerMonitor.h"
#include "stm8Metering.h"
#include "microchipMetering.h"
#include "ade9153aMetering.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
 // error counters for debug
static uint32_t timeout_error_counter = 0;
static uint32_t data_error_counter = 0;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Power Monitor Abstract implementation Layer Function
 *        Detects which metering HW is present
 *
 * @param[in] void
 *
 * @return    powerMonitor_HWType - the found HW
 *
 *******************************************************************************/
powerMonitor_HWType powerMonitor_detectHW( void)
{
	timeout_error_counter = 0;
	data_error_counter = 0;

    if( ade9153aMetering_detectHW() == POWER_MONITOR_RESULT_OK)
    {
        debug_printf( PWR, "ADI ADE9153A Metering Detected\r\n");

        return POWER_MONITOR_HW_ADI_ADE9153A;
    }

    if( microchipMetering_detectHW() == POWER_MONITOR_RESULT_OK)
	{
        debug_printf( PWR, "Microchip Metering Detected\r\n");

        return POWER_MONITOR_HW_MICROCHIP;
	}

    // if none of the others it must be stm8
    debug_printf( PWR, "STM8 Metering Detected\r\n");

	return POWER_MONITOR_HW_STM8;
}

/*******************************************************************************
 * @brief Power Monitor Abstract implementation Layer Function
 *       Resets the power calculation dataset
 *
 * @param[in] PowerCalcDataset * pointer to the dataset
 *
 * @return    void
 *
 *******************************************************************************/
void powerMonitor_resetDataset( PowerCalcDataset * dataset_ptr)
{
    if( dataset_ptr == NULL)
    {
        return;
    }

    // default the calibration dataset
    dataset_ptr->accumulatedEnergy_20WhrUnits = 0;
    dataset_ptr->instantPowerFactor = 0.0F;
    dataset_ptr->instantIrms  = 0.0F;
    dataset_ptr->instantPower = 0.0F;
    dataset_ptr->instantVrms  = 0.0F;
    dataset_ptr->vrmsAverage  = 0.0F;
    dataset_ptr->vrmsMax      = 0.0F;
    // put a high value in so the value gets updated on 1st update
    dataset_ptr->vrmsMin      = 300.0F;
}

/*******************************************************************************
 * @brief Power Monitor Abstract implementation Layer Function
 *       procesess the result of the metering service routine
 *
 * @param[in] powerMonitor_Result result of the mettering service
 *
 * @return    void
 *
 *******************************************************************************/
void powerMonitor_resultHandler( powerMonitor_Result result)
{
	switch (result)
	{
		case POWER_MONITOR_RESULT_IDLE:
			break;

		case POWER_MONITOR_RESULT_OK:
			// turn off the no power data if required
			clear_sysmon_fault(NO_POWER_DATA);

			// turn off the power fail flag if required
			if (get_sysmon_fault_status(POWER_FAILURE))
			{
				clear_sysmon_fault(POWER_FAILURE);

				clear_sysmon_last_gasp_flag();
			}
			break;

		case POWER_MONITOR_RESULT_TIMEOUT:
			timeout_error_counter++;

			set_sysmon_fault(NO_POWER_DATA);

			debug_printf(PWR, "powerMonitor_resultHandler() : Timeout Error\r\n");
			break;

		case POWER_MONITOR_RESULT_DATA_ERROR:
			data_error_counter++;

			clear_sysmon_fault(NO_POWER_DATA);

			debug_printf(PWR, "powerMonitor_resultHandler() : Data Error\r\n");
			break;

		case POWER_MONITOR_RESULT_POWER_FAIL:
			// turn off the no power data if required
			clear_sysmon_fault(NO_POWER_DATA);

			set_sysmon_fault(POWER_FAILURE);
			set_sysmon_last_gasp_flag();

			debug_printf(PWR, "powerMonitor_resultHandler() : Power Fail\r\n");
			break;

		case POWER_MONITOR_RESULT_POWER_RESTORE:
			// turn off the no power data if required
			clear_sysmon_fault(NO_POWER_DATA);

			clear_sysmon_fault(POWER_FAILURE);
			clear_sysmon_last_gasp_flag();

			debug_printf(PWR, "powerMonitor_resultHandler() : Power Restore\r\n");
			break;

		default:
			break;
	}
}

/*------------------------------ End of File -------------------------------*/
