/*
==========================================================================
 Name        : powerMonitor.c
 Project     : pcore
 Path        : /pcore/src/private/abstraction/power/powerMonitor.c
 Author      : Jeff Barker
 Copyright   : Lucy Zodion Ltd
 Created     : 6th April 2021
 Description : Power monitor abstraction layer
==========================================================================
*/

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "debug.h"
#include "FreeRTOS.h"
#include "task.h"
#include "config_dataset.h"
#include "sysmon.h"
#include "powerMonitor.h"
#include "powerMonitor_impl.h"
#include "stm8Metering.h"
#include "stm8Metering_calibration.h"
#include "microchipMetering.h"
#include "microchipMetering_calibration.h"
#include "ade9153aMetering.h"
#include "ade9153aMetering_calibration.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static powerMonitor_HWType hwType = POWER_MONITOR_HW_UNKNOWN;
// data set used by metering implementation via a pointer
static PowerCalcDataset powerDataset;
// data set used by calibration metering implementation via a pointer
static PowerCalcDataset calibPowerDataset;

// init in bss so its true if checked before powerMonitor_calibrate() is called
static bool isCalibrationActive = true;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/


/*******************************************************************************
 * @brief Power Monitor Abstract Layer get HW type
 *
 * @param[in] void
 *
 * @return    powerMonitor_HWType
 *
 *******************************************************************************/
powerMonitor_HWType powerMonitor_getHWType( void)
{
	return hwType;
}

/*******************************************************************************
 * @brief Power Monitor Abstract Layer Init Function
 *        Called from the vTaskSensorLfp() task before entering the task loop
 *        Detects if the microchip HW is present and processes accordingly
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void powerMonitor_init( void)
{
	hwType = powerMonitor_detectHW();

	switch( hwType)
	{
		case POWER_MONITOR_HW_STM8:
			stm8Metering_init();
			break;

		case POWER_MONITOR_HW_MICROCHIP:
			microchipMetering_init();
			break;

		case POWER_MONITOR_HW_ADI_ADE9153A:
            ade9153aMetering_init();
		    break;

		default:
			break;
	}
}

/*******************************************************************************
 * @brief Power Monitor Abstract Layer Init measurement Function
 *        Called from the vTaskSensorLfp() task before entering the task loop
 *        Initialises measurement variables, calibration gains etc
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void powerMonitor_initMeasurement( void)
{
	powerMonitor_resetDataset( &powerDataset);

	switch( hwType)
	{
		case POWER_MONITOR_HW_STM8:
			stm8Metering_initMeasurement( &powerDataset);
			break;

		case POWER_MONITOR_HW_MICROCHIP:
			microchipMetering_initMeasurement( &powerDataset);
			break;

		case POWER_MONITOR_HW_ADI_ADE9153A:
            ade9153aMetering_initMeasurement( &powerDataset);
            break;

        default:
			break;
	}
}


/*******************************************************************************
 * @brief Power Monitor Abstract Layer service function
 *        Called from the vTaskSensorLfp() task every 100ms
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void powerMonitor_service( void)
{
	powerMonitor_Result result = POWER_MONITOR_RESULT_IDLE;

	switch( hwType)
	{
		case POWER_MONITOR_HW_STM8:
			result = stm8Metering_service();
			break;

		case POWER_MONITOR_HW_MICROCHIP:
			result = microchipMetering_service();
			break;

        case POWER_MONITOR_HW_ADI_ADE9153A:
            result = ade9153aMetering_service();
            break;

        default:
			break;
	}

	powerMonitor_resultHandler( result);

}


/*******************************************************************************
 * @brief Power Monitor Abstract Layer UART ISR function
 *        This function is mapped to the UART1 IRQ handler in "config_pinout.h"
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void powerMonitor_uartISR( void)
{
	switch( hwType)
	{
		case POWER_MONITOR_HW_STM8:
			stm8Metering_uartISR();
			break;

		case POWER_MONITOR_HW_MICROCHIP:
			microchipMetering_uartISR();
			break;

        case POWER_MONITOR_HW_ADI_ADE9153A:
            ade9153aMetering_uartISR();
            break;

        default:
			break;
	}
}

/*******************************************************************************
 * @brief Power Monitor Abstract Layer interrupt pin ISR function
 *        This function needs to be mapped to a PIN_INTx_IRQn
 *
 * @param[in] void
 *
 * @return    void
 *
 *******************************************************************************/
void powerMonitor_interruptPinISR( void)
{
    switch( hwType)
    {
        case POWER_MONITOR_HW_STM8:
            // stm8 does not have event interrupts
            break;

        case POWER_MONITOR_HW_MICROCHIP:
            microchipMetering_interruptPinISR();
            break;

        case POWER_MONITOR_HW_ADI_ADE9153A:
            ade9153aMetering_interruptPinISR();
            break;

        default:
            break;
    }
}


/*******************************************************************************
 * @brief Power Monitor Abstract Layer get data set function
 *
 * @param[in] PowerCalcDataset * - pointr to the dataset to copy to
 *
 * @return    powerMonitor_Result - the result of the copy
 *
 *******************************************************************************/
powerMonitor_Result powerMonitor_getDataset( PowerCalcDataset * dataset_ptr)
{
    if( dataset_ptr == NULL)
    {
        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }
	// Prevent the RTOS kernel swapping out the task whilst
	// the dataset is being retrieved.
	vTaskSuspendAll();

	memcpy( (void*) dataset_ptr,
			(const void*) &powerDataset,
			sizeof(PowerCalcDataset));

	// The operation is complete.  Restart the RTOS kernel.
	xTaskResumeAll();

	return POWER_MONITOR_RESULT_OK;
}

/*******************************************************************************
 * @brief Power Monitor Abstract Layer calibrate function
 *
 * @param[in] void
 *
 * @return    powerMonitor_Result - success of the calibration
 *
 *******************************************************************************/
powerMonitor_Result powerMonitor_calibrate( void)
{
	powerMonitor_Result result = POWER_MONITOR_RESULT_IDLE;

	powerMonitor_resetDataset( &calibPowerDataset);

	switch( hwType)
	{
		case POWER_MONITOR_HW_STM8:
		    debug_printf( PWR, "Start STM8 Calibration\r\n");

		    result = stm8Metering_calibrate( &calibPowerDataset);
			break;

		case POWER_MONITOR_HW_MICROCHIP:
            debug_printf( PWR, "Start Microchip Calibration\r\n");

            result = microchipMetering_calibrate( &calibPowerDataset);
			break;

        case POWER_MONITOR_HW_ADI_ADE9153A:
            debug_printf( PWR, "Start ADI ADE9153A Calibration\r\n");

            result = ade9153aMetering_calibrate( &calibPowerDataset);
            break;

        default:
			break;
	}

    isCalibrationActive = false;

    if( result != POWER_MONITOR_RESULT_OK)
	{
        set_sysmon_fault( PWR_ST_FAILED);

        debug_printf( PWR, "Calibration Failed\r\n");
	}
	else
	{
		clear_sysmon_fault( PWR_ST_FAILED);
	}

	return result;
}

/*******************************************************************************
 * @brief Power Monitor Abstract Layer is calibration active function
 *
 * @param[in] void
 *
 * @return    bool - TRUE if calibration active, otherwise FALSE
 *
 *******************************************************************************/
bool powerMonitor_isCalibrationActive( void)
{
	return isCalibrationActive;
}

/*******************************************************************************
 * @brief Power Monitor Abstract Layer copy of power calc dataset
 *
 * @param[in] PowerCalcDataset * dataset pointer
 *
 * @return    powerMonitor_Result - result of calibrating the readings
 *
 *******************************************************************************/
powerMonitor_Result powerMonitor_getCalibReadings( PowerCalcDataset * dataset_ptr)
{
    if( dataset_ptr == NULL)
    {
        debug_printf( PWR, "Get Calib Readings NULL ptr\r\n");

        return POWER_MONITOR_RESULT_PARAM_ERROR;
    }

    // Prevent the RTOS kernel swapping out the task whilst
    // the dataset is being retrieved.
    vTaskSuspendAll();

    memcpy( (void*) dataset_ptr,
            (const void*) &calibPowerDataset,
            sizeof(PowerCalcDataset));

    // The operation is complete.  Restart the RTOS kernel.
    xTaskResumeAll();

    return POWER_MONITOR_RESULT_OK;
}

/*******************************************************************************
 * @brief Power Monitor Abstract Layer apply calib readings
 *
 * @param[in] PowerCalcDataset * pointer to the dataset received from the meter
 *
 * @return    powerMonitor_Result - result of calibrating the readings
 *
 *******************************************************************************/
powerMonitor_Result powerMonitor_applyCalibReadings( PowerCalcDataset * meter_dataset_ptr)
{
	powerMonitor_Result result = POWER_MONITOR_RESULT_OK;
	
	switch (hwType)
	{
		case POWER_MONITOR_HW_STM8:
		    result = stm8Metering_applyCalibReadings( meter_dataset_ptr);
		    break;

		case POWER_MONITOR_HW_MICROCHIP:
			break;

		case POWER_MONITOR_HW_ADI_ADE9153A:
			break;

		default:
			break;
	}

	return result;
}

/*------------------------------ End of File -------------------------------*/
