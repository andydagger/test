/*
==========================================================================
 Name        : nwk_tek.h
 Project     : pcore
 Path        : /pcore/src/private/abstraction/network/nwk_tek.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 23 May 2018
 Description : 
==========================================================================
*/

#ifndef PRIVATE_ABSTRACTION_NETWORK_NWK_TEK_H_
#define PRIVATE_ABSTRACTION_NETWORK_NWK_TEK_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

bool nwk_tek_init(bool * allowNetworkSessionRecovery);
bool nwk_tek_connect(bool restoringSession);
bool nwk_tek_send_msg(char * p_msg_to_send, bool ack_is_required);
char * nwk_tek_read_msg(void);
bool nwk_tek_update_broabcast_status(bool mcastRequestedState);
void nwk_tek_save_session_info();
bool get_nwk_tek_rx_string_is_waiting(void);
bool nwk_tek_optimise_settings();

void nwk_tek_trx_isr(void);
uint16_t get_nwk_tek_fw_version();

void nwk_tek_checkForStringEnd();
void get_nwk_tek_dev_eui(char * p_devEuiString);
#endif /* PRIVATE_ABSTRACTION_NETWORK_NWK_TEK_H_ */
