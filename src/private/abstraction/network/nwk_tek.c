/*
==========================================================================
 Name        : nwk_tek.c
 Project     : pcore
 Path        : /pcore/src/private/abstraction/network/nwk_tek.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 23 May 2018
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "nwk_tek.h"
#include "lorawan.h"
#include "lorawaninit.h"
#include "lorawanhw.h"
#include "loraDriver.h"
#include "Murata.h"
#include "LorawanStatusFlags.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static bool radioTekIsMicrochip = false; // Start with Murata by default.

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
bool nwk_tek_init(bool * allowNetworkSessionRecovery)
{
    if(radioTekIsMicrochip == false)
    {
        // Attempt to initialise on Murata Tek
        if( true == Murata_Init(_EU868, allowNetworkSessionRecovery)){
            return true;
        }else{
            radioTekIsMicrochip = true; 
        }
    }
    // Attempt to initialise on Microchip
    if(true == lorawan_init(allowNetworkSessionRecovery))
    {
        // Latch as Microchip tek.
        radioTekIsMicrochip = true;
        return true;
    }
    return false;
}

bool nwk_tek_connect(bool restoringSession)
{
    if(radioTekIsMicrochip){
        return lorawan_join_network(restoringSession);
    }else{
        // Session restore not yet supported
        return Murata_JoinNetwork(_EU868, restoringSession);
    }
}

bool nwk_tek_send_msg(char * p_msg_to_send, bool ack_is_required)
{
    if(radioTekIsMicrochip){
        return lorawan_send_msg(p_msg_to_send, ack_is_required);
    }else{
        switch( Murata_SendMsg(p_msg_to_send, ack_is_required))
        {
            case CMD_SUCCESSFUL:    
                return true; 
                break;
            case CMD_FAILED:        
                return false; 
                break;
            case POOR_LINK:
                LorawanStatusFlags_SetModuleSettingsNeedOptimising();         
                return false; 
                break;
            case LOST_CONNECTION: 
                LorawanStatusFlags_ClearJoinedFlag();
                return false; 
                break;
            case MOD_NOT_RESP: 
                LorawanStatusFlags_SetModuleNeedsInitialising();
                return false; 
                break;
            default: 
                return false;   
        }        
    }
}

char * nwk_tek_read_msg(void)
{
    if(radioTekIsMicrochip){
        return lorawan_read_msg();
    }else{
        return Murata_ReadMsg();   
    }     
}

bool nwk_tek_update_broabcast_status(bool mcastRequestedState)
{
    if(radioTekIsMicrochip){
        return lorawan_update_mcast_state(mcastRequestedState);
    }else{
        return Murata_UpdateMcastState(mcastRequestedState); 
    }     
}

void nwk_tek_save_session_info()
{
    if(radioTekIsMicrochip){
        lorawan_save_session_info();
    }else{
        Murata_SaveSessionInfo();     
    }
    return;
}

bool get_nwk_tek_rx_string_is_waiting(void)
{
    if(radioTekIsMicrochip){
        return get_lorawanhw_rx_string_waiting();
    }else{
        return Murata_getDownlinkPayloadIsWaiting();  
    }
}

bool nwk_tek_optimise_settings()
{
    if(radioTekIsMicrochip){
        return lorawan_optimise_settings();
    }else{
        return Murata_SetBestSettings();
    }
}

uint16_t get_nwk_tek_fw_version()
{
    if(radioTekIsMicrochip){
        return get_lorawaninit_fw_version();
    }else{
        return Murata_GetModuleFwVersion();    
    }
}

void nwk_tek_trx_isr(void)
{
    if(radioTekIsMicrochip){
        lorawanhw_trx_isr();
    }else{
        loraDriver_trx_isr_murata(); 
    }
    return;      
}

void nwk_tek_checkForStringEnd()
{
    if(radioTekIsMicrochip){
        // Nope
    }else{
        Murata_checkForStringEnd(); 
    }
    return;  
}

void get_nwk_tek_dev_eui(char * p_devEuiString)
{
     if(radioTekIsMicrochip){
        get_lorawan_dev_eui(p_devEuiString);
    }else{
        Murata_getDevEUi(p_devEuiString);    
    }   
    return;
}