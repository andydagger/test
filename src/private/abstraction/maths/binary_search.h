/*!
	\file 		binary_search.h
	\author 	s.arthur
	\date 		25/08/2021
	\copyright 	Lucy Zodion Ltd.
*/

#ifndef SRC_PRIVATE_ABSTRACTION_MATHS_BINARY_SEARCH_H
#define SRC_PRIVATE_ABSTRACTION_MATHS_BINARY_SEARCH_H

#include <stdint.h>
/*
    Return values:
        -1  if search_array[index] <  ref
         0  if search_array[index] == ref
        +1  if search_array[index]  > ref

    Example:
        return search_array[index] - ref;
*/
typedef int32_t(*binary_search_check_t)(void *search_array, uint32_t index, void *ref);


int32_t binary_search(void *search_array,
                      void *ref,
                      uint32_t element_count,
                      binary_search_check_t check_func);

/*------------------------------ End of File -------------------------------*/
#endif // SRC_PRIVATE_ABSTRACTION_MATHS_BINARY_SEARCH_H
