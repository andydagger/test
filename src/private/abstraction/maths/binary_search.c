/*!
	\file 		binary_search.c
	\author 	s.arthur
	\date 		25/08/2021
	\copyright 	Lucy Zodion Ltd.
*/

#include <stdint.h>
#include "chip.h"
#include "binary_search.h"

//*****************************************************************************
// Examples of the implmentation for int and uint 8, 16 and 32 including the 
// search check functions can be found in test_binary_search.c
// The search check function must validate the search_array and ref pointers
//*****************************************************************************


/*!
    \brief      Perform a generic binary search for a reference value using
                successive approximation.
                array element values must be in ascending order

    \param      *search_array   Pointer to a sorted array of elements to search
    \param      *ref            Pointer to the reference object to find
    \param      element_count   Number of elements in search array

    \param      check_func      A function pointer to a search comparison function, capable of
                                operating on the search array and reference object

    \return     The index of the found value, or -1 if not found
*/
int32_t binary_search(void *search_array,
                      void *ref,
                      uint32_t element_count,
                      binary_search_check_t check_func) {

    uint32_t first_idx;
    uint32_t last_idx;
    uint32_t middle_idx;
    int32_t  result;

    // validate the input params
    // the binary search check function must validate the search_array and ref pointers
    if((element_count == 0) ||
       (check_func    == NULL)) {
        // Invalid param found
        return -1;
    }

    // catch an array size of 1
    if (element_count == 1)
    {
        middle_idx = 0;
        // we only need to check one element
        result = check_func(search_array, middle_idx, ref);
    }
    else
    {
        first_idx = 0;
        last_idx  = element_count - 1;

        while (first_idx <= last_idx) {

            middle_idx = (first_idx + last_idx) / 2;

            result = check_func(search_array, middle_idx, ref);

            if (result < 0) {
                // Ref value is larger than the array element
                first_idx = middle_idx + 1;
            }
            else if (result == 0) {
                // found the value in the array
                break;
            }
            else {
                // ref value is smaller than the array element
                last_idx = middle_idx - 1;
            }
        }
    }

    if (result == 0) {
        // value found return the index associated with it
        return (int32_t)middle_idx;
    }

    // nothing found
    return -1;
}


