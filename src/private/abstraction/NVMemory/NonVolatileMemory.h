/*
 * NonVolatileMemory.h
 *
 *  Created on: 20 Apr 2021
 *      Author: benraiton
 */

#ifndef PRIVATE_ABSTRACTION_NVMEMORY_NONVOLATILEMEMORY_H_
#define PRIVATE_ABSTRACTION_NVMEMORY_NONVOLATILEMEMORY_H_


typedef enum
{
    NVM_RESULT_OK = 0,
    NVM_RESULT_ERROR_CRC,
    NVM_RESULT_ERROR_VERSION,
    NVM_RESULT_ERROR_MUTEX,
    NVM_RESULT_MAX

} NVM_result_t;


/******************************************************************************
 * Public functions
 *****************************************************************************/
NVM_result_t NVM_initialise( void);
NVM_result_t NVM_read( uint32_t address, uint8_t * dataPtr, uint32_t length);
void NVM_write( uint32_t address, uint8_t * dataPtr, uint32_t length);


#endif /* PRIVATE_ABSTRACTION_NVMEMORY_NONVOLATILEMEMORY_H_ */

/*------------------------------ End of File -------------------------------*/
