/*
 * NonVolatileMemory.c
 *
 *  Created on: 20 Apr 2021
 *      Author: benraiton
 */

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "debug.h"
#include "config_eeprom.h"
#include "config_queues.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "hw.h"
#include "CRC16.h"
#include "NonVolatileMemory.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

// NVM access semaphore
static SemaphoreHandle_t         nvmLock;

// the RTOS ticks are 1ms
// In the LPC15xx datasheet page 70 it specifies 64 bytes prog. time of 2.9ms
// allowing for task switching we use a timeout of 10ms as we don't know the
// priority of the task that is writing the flash.
#define NVM_LOCK_TIMEOUT_TICK    (TickType_t)10

/*****************************************************************************
 * Private constants
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief Called on power-up or reset before starting FreeRTOS, initialises
 *        the semaphore.
 *
 * @param[in] void
 *
 * @return    NVM_result_t - the result of the initialisation
 *
 *******************************************************************************/
NVM_result_t NVM_initialise( void)
{
    NVM_result_t result = NVM_RESULT_OK;
    // NVM is a shared resource so needs protection against
    // simultaneous task access
    nvmLock = xSemaphoreCreateMutex();

    if( nvmLock == NULL)
    {
        // the init function is called before the RTOS is running
        // so we can just call the instant print func.
        debug_print_instant(NVM, "Mutex not created\r\n");

        result = NVM_RESULT_ERROR_MUTEX;
    }

    return result;
}


/*******************************************************************************
 * @brief reads a structure from NVM and validates the CRC
 *
 * @param[in] uint32_t  - address to read
 *            uint8_t * - pointer to the memory to read data into
 *            uint32_t  - how many bytes to read
 *
 * @return    NVM_result_t - result of the read
 *
 *******************************************************************************/
NVM_result_t NVM_read( uint32_t address, uint8_t * dataPtr, uint32_t length)
{
    NVM_result_t result = NVM_RESULT_OK;
    uint16_t     calcCRC;
    uint16_t     crc;

    if( nvmLock != NULL)
    {
        // simultaneous task eeprom write protection
        if( xSemaphoreTake( nvmLock, NVM_LOCK_TIMEOUT_TICK) == pdFALSE)
        {
            debug_printf(NVM, "Read mutex timeout\r\n");
        }
    }

    // HW abstracted function in HW.c
    eepromReadByte( address, dataPtr, length);

    // must have enough bytes in the array to be able to place the crc
    if( length > sizeof(uint16_t))
    {
        // all structs have a crc16 at the end of the struct.
        calcCRC = CRC16_calculate( NVM_CRC16_SEED,
                                   dataPtr,
                                   length - sizeof(uint16_t));

        // last 2 bytes are the CRC16
        crc = ((uint16_t)dataPtr[ length - 2] << 8) |
              ((uint16_t)dataPtr[ length - 1]);

        // validate crc
        if( crc != calcCRC)
        {
            debug_printf(NVM, "Read CRC error\r\n");

            result = NVM_RESULT_ERROR_CRC;
        }
    }

    if( nvmLock != NULL)
    {
        xSemaphoreGive( nvmLock);
    }

    return result;
}

/*******************************************************************************
 * @brief writes a structure to NVM including calculating its CRC
 *
 * @param[in] uint32_t  - address to write
 *            uint8_t * - pointer to the memory to write data from
 *            uint32_t  - how many bytes to write
 *
 * @return    void
 *
 *******************************************************************************/
void NVM_write( uint32_t address, uint8_t * dataPtr, uint32_t length)
{
    uint16_t crc;

    if( nvmLock != NULL)
    {
        // simultaneous task eeprom write protection
        if( xSemaphoreTake( nvmLock, NVM_LOCK_TIMEOUT_TICK) == pdFALSE)
        {
            debug_printf(NVM, "Write mutex timeout\r\n");
        }
    }

    // must have enough bytes in the array to be able to place the crc
    if( length > sizeof(uint16_t))
    {
        // all structs have a crc16 at the end of the struct.
        crc = CRC16_calculate( NVM_CRC16_SEED,
                               dataPtr,
                               length - sizeof(uint16_t));

        // last 2 bytes are the CRC16
        dataPtr[ length - 2] = (uint8_t)(crc >> 8);
        dataPtr[ length - 1] = (uint8_t)crc;
    }

    // HW abstracted function in HW.c
    eepromWriteByte( address, dataPtr, length);

    if( nvmLock != NULL)
    {
        xSemaphoreGive( nvmLock);
    }
}

/*------------------------------ End of File -------------------------------*/

