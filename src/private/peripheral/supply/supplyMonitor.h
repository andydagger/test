/*
 * supplyMonitor.h
 *
 *  Created on: 19 May 2021
 *      Author: benraiton
 */

#ifndef PRIVATE_PERIPHERAL_SUPPLY_SUPPLYMONITOR_H_
#define PRIVATE_PERIPHERAL_SUPPLY_SUPPLYMONITOR_H_

#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Definitions
 ****************************************************************************/
typedef enum __tag_supplyStatus
{
	SUPPLY_IS_OK = 0,
	SUPPLY_DID_NOT_REACH_TH,
    SUPPLY_MIN_TOO_LOW,
    SUPPLY_MAX_TOO_HIGH,
    SUPPLY_AV_TOO_LOW,
    SUPPLY_AV_TOO_HIGH
} supplyStatus;
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*****************************************************************************
 * Check routine of the 12V supply rail: Min, max, Average
 ****************************************************************************/
supplyStatus supplyMonitor_check12VRail(void);

/*****************************************************************************
 * Get flag of 12V readiness
 ****************************************************************************/
bool supplyMonitor_get12vRailIsReady(void);

#endif /* PRIVATE_PERIPHERAL_SUPPLY_SUPPLYMONITOR_H_ */
