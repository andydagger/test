/*
 * supplyMonitor_impl.c
 *
 *  Created on: 20 May 2021
 *      Author: benraiton
 */

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "supplyMonitor.h"
#include "supplyMonitor_impl.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

float supplyMonitor_getMin(float * sampleTable, uint8_t nSamples)
{
    float smallestSample = 30.0F;
    do
    {
        nSamples--;
        if(sampleTable[nSamples] < smallestSample)
        {
            smallestSample = sampleTable[nSamples];
        }
    } while (nSamples > 0);
    
    return smallestSample;
}

float supplyMonitor_getMax(float * sampleTable, uint8_t nSamples)
{
    float highestSample = 0.0F;
    do
    {
        nSamples--;
        if(sampleTable[nSamples] > highestSample)
        {
            highestSample = sampleTable[nSamples];
        }
    } while (nSamples > 0);
    
    return highestSample;
}

float supplyMonitor_getAv(float * sampleTable, uint8_t nSamples)
{
    float numberOfSamples = (float)nSamples;
    float sampleSum = 0.0F;
     do
    {
        nSamples--;
        sampleSum += sampleTable[nSamples];
    } while (nSamples > 0);   

    return (sampleSum/numberOfSamples);
}