/*
 * supplyMonitor_impl.h
 *
 *  Created on: 20 May 2021
 *      Author: benraiton
 */

#ifndef PRIVATE_PERIPHERAL_SUPPLY_SUPPLYMONITOR_IMPL_H_
#define PRIVATE_PERIPHERAL_SUPPLY_SUPPLYMONITOR_IMPL_H_

#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Definitions
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
float supplyMonitor_getMin(float * sampleTable, uint8_t nSamples);
float supplyMonitor_getMax(float * sampleTable, uint8_t nSamples);
float supplyMonitor_getAv(float * sampleTable, uint8_t nSamples);

#endif /* PRIVATE_PERIPHERAL_SUPPLY_SUPPLYMONITOR_IMPL_H_ */
