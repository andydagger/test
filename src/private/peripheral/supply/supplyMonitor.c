/*
 * supplyMonitor.c
 *
 *  Created on: 19 May 2021
 *      Author: benraiton
 */


/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "supplyMonitor.h"
#include "supplyMonitor_impl.h"
#include "adc_procx.h"
#include "FreeRTOS.h"
#include "FreeRTOSCommonHooks.h"
#include "sysmon.h"
#include "systick.h"
#include "debug.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define MIN_TO_START_DELAY_V            11.0F
#define CHARGE_TIME_OUT_MS              (uint32_t)20000
#define DELAY_TO_STABALISE_MS           (uint32_t)3000
#define ANALYSIS_PERIOD_MS              (uint32_t)5000
#define SAMPLING_GAP_MS                 (uint32_t)250
#define NUMBER_OF_SAMPLES               (uint8_t)(ANALYSIS_PERIOD_MS/SAMPLING_GAP_MS)

#define MIN_LIMIT                       11.5F
#define MAX_LIMIT                       12.5F
#define AV_MIN                          11.6F
#define AV_MAX                          12.2F

static bool supply12vRailIsReady = false;
static uint8_t sampleId = 0;
static float sampleTable[NUMBER_OF_SAMPLES];

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

supplyStatus supplyMonitor_check12VRail(void)
{
    uint32_t refTimeStartCharge = get_systick_ms();
    float samplesMin = 0.0F;
    float samplesMax = 0.0F;
    float samplesAv = 0.0F;
    float sample = 0.0F;

    debug_printf(PROD,"Checking 12V line...\r\n");

    // Wait 12V rail to cross threshold
    do
    {
        if( (get_systick_ms() - refTimeStartCharge) > CHARGE_TIME_OUT_MS)
        {
            return SUPPLY_DID_NOT_REACH_TH;
        }
        sample = get_adc_procx_p_detect_volt();
        debug_printf(   PROD, "12V line = %d mV\r\n", 
                        (uint16_t)(1000*sample));
        FreeRTOSDelay(SAMPLING_GAP_MS);
    } 
    while (sample < MIN_TO_START_DELAY_V);

    debug_printf(   PROD, "12V line > Vth = %d mV\r\n", 
                    (uint16_t)(1000*MIN_TO_START_DELAY_V));
    
    // Wait for it to stabalise
    FreeRTOSDelay(DELAY_TO_STABALISE_MS);
    debug_printf( PROD, "12V line now stable, check min/max/av...\r\n");

    // Sample for fix period
    do
    {
        sample = get_adc_procx_p_detect_volt();
        debug_printf(   PROD, "12V line = %d mV\r\n", 
                        (uint16_t)(1000*sample));
        sampleTable[sampleId++] = sample;
        FreeRTOSDelay(SAMPLING_GAP_MS);
    } 
    while (sampleId < NUMBER_OF_SAMPLES);
    
    // Process Samples
    samplesMin = supplyMonitor_getMin(sampleTable, sampleId);
    samplesMax = supplyMonitor_getMax(sampleTable, sampleId);
    samplesAv = supplyMonitor_getAv(sampleTable, sampleId);

    debug_printf(   PROD, "12V line Min/Max/Av = %d/%d/%d mV\r\n", 
                    (uint16_t)(1000*samplesMin),
                    (uint16_t)(1000*samplesMax),
                    (uint16_t)(1000*samplesAv));

    // Check against limits
    if(samplesMax > MAX_LIMIT)  return SUPPLY_MAX_TOO_HIGH;
    if(samplesMin < MIN_LIMIT)  return SUPPLY_MIN_TOO_LOW;
    if(samplesAv > AV_MAX)      return SUPPLY_AV_TOO_HIGH;
    if(samplesAv < AV_MIN)      return SUPPLY_AV_TOO_LOW;

    supply12vRailIsReady = true;
        
    return SUPPLY_IS_OK;
}

bool supplyMonitor_get12vRailIsReady(void)
{
    return supply12vRailIsReady;
}
