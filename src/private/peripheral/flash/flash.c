/*
==========================================================================
 Name        : flash.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/flash/flash.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 11 Jan 2018
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/
#include "chip.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "flash.h"
#include "config_pinout.h"
#include "config_flash.h"
#include "systick.h"
#include "sysmon.h"
#include "common.h"
#include "debug.h"
#include "spi_15xx.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "FreeRTOSMacros.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
/* Interrupt error code (used as semaphore) */
static volatile uint8_t flash_xfer_status;
/* SPI Transfer Setup */
static SPI_DATA_SETUP_T XferSetup;

static bool b_flash_is_initialised = false;
static uint8_t flash_status = 0;



/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static bool erase_sector(uint32_t sector_to_erase);
static bool write_page(uint8_t ** p_p_source, uint32_t * page_address);

static bool flash_send_opcode(uint8_t opcode);
static bool flash_send_get_status(uint8_t * status);
static bool flash_send_sector_erase(uint32_t address);

static bool flash_spi_read( uint32_t start_address, uint8_t *rx_data, uint32_t length);
static bool flash_spi_write( uint32_t start_address, uint8_t *tx_data, uint32_t length);

static void flash_update_status_bit(void);

static void SPI_Send_Data_RxIgnore(LPC_SPI_T *pSPI, SPI_DATA_SETUP_T *pXfSetup);
static void SPI_Receive_Data(LPC_SPI_T *pSPI, SPI_DATA_SETUP_T *pXfSetup);
static void SPI_Send_Data(LPC_SPI_T *pSPI,
                          SPI_DATA_SETUP_T *pXfSetup);

static void report_error(uint32_t error_code);

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * @brief  Erase the whole flash memory content
 * @param[in] void
 * @return void
 *******************************************************************************/
bool flash_wipe_memory(void){
    uint8_t flash_hw_status = FLASH_STATUS_BUSY;

    // send write enable opcode
    catch_error_report( SEND_ER_ALL_CMD,
                        flash_send_opcode( XFER_ERASE_ALL ));

    // wait for hw to be ready
    while (flash_hw_status & FLASH_STATUS_BUSY) {
        catch_error_report( GET_STATUS,
                            flash_send_get_status(&flash_hw_status));
    }
    return true;
}

/*******************************************************************************
 * @brief  Initialises the storage device and put it ready to generic read/write
 * @param[in] void
 * @return return true when done or false otherwise
 *******************************************************************************/
bool flash_disk_initialise(void){

    b_flash_is_initialised = false;

    // Need to execute WREN before writing to page
    catch_error_fail( flash_send_opcode( XFER_WRITE_EN ) );

    catch_error_fail( flash_send_opcode( XFER_GLOB_UNLOCK ) );

    // Enable Reset
    catch_error_fail( flash_send_opcode( XFER_RESET_EN ) );
    // Execute Reset
    catch_error_fail( flash_send_opcode( XFER_RESET ) );

    catch_error_fail( flash_send_opcode( XFER_WRITE_EN ) );

    // Update stack flag
    b_flash_is_initialised = true ;

    return true;
}

/*******************************************************************************
 * @brief  Returns the flash status
 * @param[in] ptr to status value
 * @return true if status acquired successfully
 *******************************************************************************/
bool flash_disk_status(uint8_t * status){

    // get hw status
    catch_error_report( GET_STATUS, flash_send_get_status(&flash_status));

    // return stack flash status.
    flash_update_status_bit();

    *status = flash_status;

    return true;
}

/*******************************************************************************
 * @brief  Reads flash sector(s)
 * @param[in] ptr to target data location, strat and number of sectors to read.
 * @return uint8_t read operation status
 *******************************************************************************/
bool flash_disk_read(uint8_t * buff, uint32_t start_sector, uint16_t n_sectors){

    uint32_t bytes_to_read = flash_nsectors_to_nbytes(n_sectors);
    uint32_t sector_start_address = flash_calc_sector_address(start_sector);
    bool b_flash_read_success = false;

    b_flash_read_success = flash_spi_read(   sector_start_address,
                                            buff,
                                            bytes_to_read);

    return b_flash_read_success;
}


/*******************************************************************************
 * @brief  Return flash initialised flag
 * @param[in] void
 * @return bool true if flash has been initialised
 *******************************************************************************/
bool get_flash_is_initialised(void){
    return b_flash_is_initialised;
}

/*******************************************************************************
 * @brief  Write to flash sector(s)
 * @param[in] ptr to source data, start and number of sectors to write
 * @return uint8_t write operation status
 *******************************************************************************/
bool flash_disk_write( uint8_t * buff, uint32_t start_sector, uint16_t n_sectors){
    uint32_t sector_being_erased = start_sector;
    uint32_t page_being_written =   flash_calc_sector_address(start_sector);
    uint32_t last_page_to_write =   flash_calc_sector_address(  start_sector
                                                                + n_sectors)
                                    - PAGE_SIZE_BYTES;
    uint8_t **p_p_data = &buff;

    // erase all required sectors
    while( sector_being_erased < (start_sector + n_sectors)) {
        catch_error_fail( erase_sector( sector_being_erased ));
        sector_being_erased++;
    }

    // write all pages
    while(page_being_written <= last_page_to_write){
        catch_error_fail( write_page( p_p_data, &page_being_written) );
    }

    return true;
}


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*******************************************************************************
 * @brief  Erase given sector
 * @param[in] uint32_t sector number
 * @return true if sector was erased successfully
 *******************************************************************************/
static bool erase_sector(uint32_t sector_to_erase){
    uint8_t flash_hw_status = FLASH_STATUS_BUSY;

    // send write enable opcode
    catch_error_report( SEND_WR_EN,
                        flash_send_opcode( XFER_WRITE_EN ));


    // Send sector erase instruction
    catch_error_report( ERASE_SECTOR,
                        flash_send_sector_erase( flash_calc_sector_address(
                                            sector_to_erase)));
    // Wait until hw no longer busy
    while (flash_hw_status & FLASH_STATUS_BUSY) {
        catch_error_report( GET_STATUS,
                            flash_send_get_status(&flash_hw_status));
    }

    return true;
}

/*******************************************************************************
 * @brief  write page content
 * @param[in] uint32_t page address
 * @return true if data was written successfiully
 *******************************************************************************/
static bool write_page(uint8_t ** p_p_source, uint32_t * page_address){
    static uint8_t tx_data[PAGE_SIZE_BYTES];
    uint16_t p = 0;
    uint8_t flash_hw_status = FLASH_STATUS_BUSY;

    // send write enable opcode
    catch_error_report( SEND_WR_EN,
                        flash_send_opcode( XFER_WRITE_EN ));

    // load data to be written
    for(p = 0; p < PAGE_SIZE_BYTES; p++){
        catch_test_report(WRITE_DATA_PTR, *p_p_source == NULL);
        tx_data[p] = **p_p_source;
        (*p_p_source)++;
    }

    catch_error_report( WRITE_DATA, flash_spi_write(    *page_address,
                                                        tx_data,
                                                        PAGE_SIZE_BYTES));

    // Wait until hw no longer busy
    while (flash_hw_status & FLASH_STATUS_BUSY) {
        catch_error_report( GET_STATUS,
                            flash_send_get_status(&flash_hw_status));
    }

    // increent page address for next time
    *page_address += PAGE_SIZE_BYTES;

    // page data written successfully
    return true;
}
/*******************************************************************************
 * @brief Report error to sysmon task
 * @param[in] void
 * @return void
 *******************************************************************************/
static void report_error(uint32_t error_code){
#ifdef COMPILE_IMAGE
    sysmon_report_error(FLASH, error_code);
#else
    debug_printf(FLASH, "ERR %lu", error_code);
#endif
    return;
}


/*******************************************************************************
 * @brief  Send simple cmd to flash
 * @param[in] uint8_t cmd to send
 * @return void
 *******************************************************************************/
static bool flash_send_opcode(uint8_t opcode){
    uint8_t tx_data[1] = {opcode};
    uint8_t rx_data[1];
    // Setup Transfer structure, this data should be retained for the entire transmission
    XferSetup.pTx = tx_data;  // Transmit Buffer
    XferSetup.pRx = rx_data; // Receive Buffer
    XferSetup.DataSize = 8; // Data format in bits
    XferSetup.Length = 1;  // Total frame length

    // Assert only SSEL0
    XferSetup.ssel =  SPI_FLASH_TXCTL_SSEL;
    XferSetup.TxCnt = 0;
    XferSetup.RxCnt = 0;

    // make sure spi routine did send single byte
    if( (Chip_SPI_RWFrames_Blocking(SPI_FLASH_IF, &XferSetup) == sizeof(tx_data)) ){
        return true;
    }else{
        return false;
    }
}

/*******************************************************************************
 * @brief  Get flash hw status byte
 * @param[in] void
 * @return uint8_t status byte
 *******************************************************************************/
static bool flash_send_get_status(uint8_t * status){
    uint8_t tx_data[2] = {XFER_STATUS, 0x00};
    uint8_t rx_data[2];
    // Setup Transfer structure, this data should be retained for the entire transmission
    XferSetup.pTx = tx_data;  // Transmit Buffer
    XferSetup.pRx = rx_data; // Receive Buffer
    XferSetup.DataSize = 8; // Data format in bits
    XferSetup.Length = 2;  // Total frame length

    // Assert only SSEL0
    XferSetup.ssel =  SPI_FLASH_TXCTL_SSEL;
    XferSetup.TxCnt = 0;
    XferSetup.RxCnt = 0;

    // make sure spi routine did send single byte
    if( (Chip_SPI_RWFrames_Blocking(SPI_FLASH_IF, &XferSetup) == sizeof(tx_data)) ){
        *status = rx_data[1];
        return true;
    }else{
        *status = 0x00;
        return false;
    }
}

/*******************************************************************************
 * @brief  Erase single sector
 * @param[in] uint32_t sector start address
 * @return void
 *******************************************************************************/
static bool flash_send_sector_erase(uint32_t address){
    uint8_t tx_data[4];
    uint8_t rx_data[4];

    // Setup Transfer structure, this data should be retained for the entire transmission
    XferSetup.pTx = tx_data;  // Transmit Buffer
    XferSetup.pRx = rx_data; // Receive Buffer
    XferSetup.DataSize = 8; // Data format in bits
    XferSetup.Length = 4;  // Total frame length

    // Assert only SSEL0
    XferSetup.ssel =  SPI_FLASH_TXCTL_SSEL;
    XferSetup.TxCnt = 0;
    XferSetup.RxCnt = 0;

    tx_data[0] = XFER_SECTOR_ERASE;
    tx_data[1] = (uint8_t)(address>>16);
    tx_data[2] = (uint8_t)(address>>8);
    tx_data[3] = (uint8_t)(address);

    // make sure spi routine did send single byte
    if( (Chip_SPI_RWFrames_Blocking(SPI_FLASH_IF, &XferSetup) == sizeof(tx_data)) ){
        return true;
    }else{
        return false;
    }
}

/*******************************************************************************
 * @brief spi read routine
 * @param[in] start address, target ptr for read data, length of data to read
 * @return true if read op was successful
 *******************************************************************************/
static bool flash_spi_read(    uint32_t start_address,
                                uint8_t *rx_data,
                                uint32_t length)
{
    uint32_t Status;

    // Setup Transfer structure, this data should be retained for the entire transmission
    XferSetup.pTx = NULL;  // Transmit Buffer
    XferSetup.pRx = rx_data; // Receive Buffer
    XferSetup.DataSize = 8; // Data format in bits
    XferSetup.Length = length;  // Total frame length

    // Assert only SSEL0
    XferSetup.ssel =  SPI_FLASH_TXCTL_SSEL;

    /* Clear status */
    Chip_SPI_ClearStatus(   SPI_FLASH_IF,
                            SPI_STAT_CLR_RXOV |
                            SPI_STAT_CLR_TXUR |
                            SPI_STAT_CLR_SSA |
                            SPI_STAT_CLR_SSD |
                            SPI_STAT_FORCE_EOT);

    Chip_SPI_SetControlInfo(    SPI_FLASH_IF,
                                XferSetup.DataSize,
                                XferSetup.ssel | SPI_TXCTL_EOF);

    XferSetup.TxCnt = 0;
    XferSetup.RxCnt = 0;

    // Send Op Code
    flash_wait_for_spi_tx() ;
    flash_send_mid_byte(XFER_READ);
    flash_wait_for_spi_rx();
    Chip_SPI_ReceiveFrame(SPI_FLASH_IF);

    // Send Address if required
    flash_wait_for_spi_tx() ;
    flash_send_mid_byte((uint8_t)(start_address>>16));
    flash_wait_for_spi_rx();
    Chip_SPI_ReceiveFrame(SPI_FLASH_IF);

    flash_wait_for_spi_tx() ;
    flash_send_mid_byte((uint8_t)(start_address>>8));
    flash_wait_for_spi_rx();
    Chip_SPI_ReceiveFrame(SPI_FLASH_IF);

    flash_wait_for_spi_tx() ;
    flash_send_mid_byte((uint8_t)(start_address));
    flash_wait_for_spi_rx();
    Chip_SPI_ReceiveFrame(SPI_FLASH_IF);


    while ((XferSetup.TxCnt < XferSetup.Length) ||
           (XferSetup.RxCnt < XferSetup.Length)) {

        Status = Chip_SPI_GetStatus(SPI_FLASH_IF);

        // In case of TxReady
        if ((Status & SPI_STAT_TXRDY) && (XferSetup.TxCnt < XferSetup.Length)) {
            SPI_Send_Data(SPI_FLASH_IF, &XferSetup);
        }

        // In case of Rx ready
        if ((Status & SPI_STAT_RXRDY) && (XferSetup.RxCnt < XferSetup.Length)) {
            SPI_Receive_Data(SPI_FLASH_IF, &XferSetup);
        }
    }

    catch_test_report(  FLASH_SPI_READ_TX_RX_COUNT,
                        XferSetup.TxCnt != XferSetup.RxCnt );

    catch_test_report(  FLASH_SPI_READ_ER_FLAG,
                        Chip_SPI_GetStatus(SPI_FLASH_IF)
                        & (SPI_STAT_RXOV | SPI_STAT_TXUR));


    // spi read operation successful
    return true;
}

/*******************************************************************************
 * @brief spi write routine
 * @param[in] start address, source ptr for data to write, length of data
 * @return true if write op was successful
 *******************************************************************************/
static bool flash_spi_write(   uint32_t start_address,
                        uint8_t *tx_data,
                        uint32_t length)
{
    flash_send_opcode( XFER_WRITE_EN );

    // Setup Transfer structure, this data should be retained for the entire transmission
    XferSetup.pTx = tx_data;  // Transmit Buffer
    XferSetup.pRx = NULL; // Receive Buffer
    XferSetup.DataSize = 8; // Data format in bits
    XferSetup.Length = length;  // Total frame length

    // Assert only SSEL0
    XferSetup.ssel =  SPI_FLASH_TXCTL_SSEL;
    XferSetup.TxCnt = 0;
    XferSetup.RxCnt = 0;

    /* Clear status */
    Chip_SPI_ClearStatus(   SPI_FLASH_IF,
                            SPI_STAT_CLR_RXOV |
                            SPI_STAT_CLR_TXUR |
                            SPI_STAT_CLR_SSA |
                            SPI_STAT_CLR_SSD |
                            SPI_STAT_FORCE_EOT);

    Chip_SPI_SetControlInfo(    SPI_FLASH_IF,
                                XferSetup.DataSize,
                                XferSetup.ssel |
                                SPI_TXCTL_EOF |
                                SPI_TXCTL_RXIGNORE);

    XferSetup.TxCnt = 0;
    XferSetup.RxCnt = 0;

    // Send Op Code
    flash_wait_for_spi_tx() ;
    flash_send_mid_byte(XFER_PAGE_WRITE);

    // Send Address if required
    flash_wait_for_spi_tx() ;
    flash_send_mid_byte((uint8_t)(start_address>>16));

    flash_wait_for_spi_tx() ;
    flash_send_mid_byte((uint8_t)(start_address>>8));

    flash_wait_for_spi_tx() ;
    flash_send_mid_byte((uint8_t)(start_address));

    do
    {

        flash_wait_for_spi_tx();
        SPI_Send_Data_RxIgnore(SPI_FLASH_IF, &XferSetup);
    }
    while ( XferSetup.TxCnt < XferSetup.Length );


    catch_test_report(  FLASH_SPI_WRITE_ER_FLAG,
                        Chip_SPI_GetStatus(SPI_FLASH_IF)
                        & (SPI_STAT_RXOV | SPI_STAT_TXUR));

    // spi write operation successful
    return true;
}


/*******************************************************************************
 * @brief Insert the stack init flag into hw status
 * @param[in] void
 * @return void
 *******************************************************************************/
static void flash_update_status_bit(void){
    /*
     * As bit 6 is unused in the hw status byte:
     * it is used by the flash stack to inform upper stacks whether or not the
     * flash hw has been initialised
     */
    if(b_flash_is_initialised){
        flash_status = flash_status || FLASH_STATUS_BITS_INIT;
    }else{
        flash_status = flash_status && !FLASH_STATUS_BITS_INIT;
    }
    return;
}
/*******************************************************************************
 * @brief Static funstions cpied from lpc15xx library
 *******************************************************************************/

static void SPI_Send_Data_RxIgnore(LPC_SPI_T *pSPI,
                                   SPI_DATA_SETUP_T *pXfSetup)
{
    if (pXfSetup->TxCnt == (pXfSetup->Length - 1)) {
        Chip_SPI_SendLastFrame_RxIgnore(pSPI, pXfSetup->pTx[pXfSetup->TxCnt], pXfSetup->DataSize, pXfSetup->ssel);
    }
    else {
        Chip_SPI_SendMidFrame(pSPI, pXfSetup->pTx[pXfSetup->TxCnt]);
    }

    pXfSetup->TxCnt++;
}

static void SPI_Receive_Data(LPC_SPI_T *pSPI,
                             SPI_DATA_SETUP_T *pXfSetup)
{
    pXfSetup->pRx[pXfSetup->RxCnt] = Chip_SPI_ReceiveFrame(pSPI);
    pXfSetup->RxCnt++;
}

static void SPI_Send_Data(LPC_SPI_T *pSPI,
                          SPI_DATA_SETUP_T *pXfSetup)
{
    if (pXfSetup->TxCnt == (pXfSetup->Length - 1)) {
        Chip_SPI_SendLastFrame(pSPI, pXfSetup->pTx[pXfSetup->TxCnt], pXfSetup->DataSize, pXfSetup->ssel);
    }
    else {
        Chip_SPI_SendMidFrame(pSPI, pXfSetup->pTx[pXfSetup->TxCnt]);
    }

    pXfSetup->TxCnt++;
}
/*******************************************************************************
 *
 *******************************************************************************/


