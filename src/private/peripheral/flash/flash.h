/*
==========================================================================
 Name        : flash.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/flash/flash.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 11 Jan 2018
 Description :
==========================================================================
*/

#ifndef PRIVATE_PERIPHERAL_FLASH_FLASH_H_
#define PRIVATE_PERIPHERAL_FLASH_FLASH_H_

/*****************************************************************************
 * Include files / Definitions (for headers)
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
// flash SST26VF016B 16Mbit >> 512x4kB
#define FLASH_N_SECTORS 512
#define PAGE_SIZE_BYTES 256


/*
FLASH_STATUS_BITS SST26VF016B

0 BUSY Write operation status
1 = Internal Write operation is in progress
0 = No internal Write operation is in progress

1 WEL Write-Enable Latch status
1 = Device is write-enabled
0 = Device is not write-enabled

2 WSE Write Suspend-Erase status
1 = Erase suspended
0 = Erase is not suspended

3 WSP Write Suspend-Program status
1 = Program suspended
0 = Program is not suspended

4 WPLD Write Protection Lock-Down status
1 = Write Protection Lock-Down enabled
0 = Write Protection Lock-Down disabled

5 SEC1
1 = Security ID space locked
0 = Security ID space not locked

6 RES Reserved for future use

7 BUSY Write operation status
1 = Internal Write operation is in progress
0 = No internal Write operation is in progress
 */
#define FLASH_STATUS_BITS_BUSY1     (uint8_t)(1 << 0)
#define FLASH_STATUS_BITS_WEL       (uint8_t)(1 << 1)
#define FLASH_STATUS_BITS_WSE       (uint8_t)(1 << 2)
#define FLASH_STATUS_BITS_WSP       (uint8_t)(1 << 3)
#define FLASH_STATUS_BITS_WPLD      (uint8_t)(1 << 4)
#define FLASH_STATUS_BITS_SEC       (uint8_t)(1 << 5)
#define FLASH_STATUS_BITS_INIT      (uint8_t)(1 << 6) /* added for flash init status*/
#define FLASH_STATUS_BITS_BUSY2     (uint8_t)(1 << 7)

#define FLASH_STATUS_BUSY (FLASH_STATUS_BITS_BUSY1 | FLASH_STATUS_BITS_BUSY2)

enum FLASH_XFER_STATUS{
    FLASH_XFER_IDLE = 0,
    FLASH_XFER_DONE,
    FLASH_XFER_BUSY,
    FLASH_XFER_ERR
};

typedef enum
{
    FLASH_STATE_IDLE,
    FLASH_STATE_ERASE_SEND,
    FLASH_STATE_ERASE_WAIT,
    FLASH_STATE_WRITE_SEND,
    FLASH_STATE_WRITE_WAIT
}statetype_flash_write;

// 4096 bytes per sector
#define flash_nsectors_to_nbytes(x) ( (((uint32_t)x) << 12) & 0xFFFFFF)
#define flash_calc_sector_address(x) ( (((uint32_t)x) << 12) & 0xFFFFFF)

// FLASH CODES
#define XFER_READ 0x03
#define XFER_STATUS 0x05
#define XFER_RESET_EN 0x66
#define XFER_RESET 0x99
#define XFER_SECTOR_ERASE 0x20
#define XFER_PAGE_WRITE 0x02
#define XFER_WRITE_EN 0x06
#define XFER_GLOB_UNLOCK 0x98
#define XFER_ERASE_ALL 0xC7

#define flash_wait_for_spi_tx() while( !( Chip_SPI_GetStatus(SPI_FLASH_IF) & SPI_STAT_TXRDY ) )
#define flash_wait_for_spi_rx() while( !( Chip_SPI_GetStatus(SPI_FLASH_IF) & SPI_STAT_RXRDY ) )
#define flash_send_mid_byte(x) Chip_SPI_SendMidFrame(SPI_FLASH_IF, x)

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
bool flash_wipe_memory(void);

// API for FatFs
bool flash_disk_initialise(void);
bool flash_disk_status(uint8_t * status);
bool flash_disk_read(uint8_t * buff, uint32_t start_sector, uint16_t n_sectors);
bool flash_disk_write( uint8_t * buff, uint32_t start_sector, uint16_t n_sectors);
bool get_flash_is_initialised(void);

#endif /* PRIVATE_PERIPHERAL_FLASH_FLASH_H_ */

