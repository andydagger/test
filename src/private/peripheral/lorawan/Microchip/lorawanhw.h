/*
==========================================================================
 Name        : lorawanhw.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/lorawanhw.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 11 Jul 2017
 Description :
==========================================================================
*/

#ifndef PERIPHERAL_LORAWAN_LORAWANHW_H_
#define PERIPHERAL_LORAWAN_LORAWANHW_H_

#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Definitions
 ****************************************************************************/


/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
typedef enum{
    NONE,
    OK,
    INVALID_PARAM,
    KEYS_NOT_INIT,
    MCAST_RE_KEY,
    NO_FREE_CH,
    SILENT,
    BUSY,
    MAC_PAUSED,
    DENIED,
    ACCEPTED,
    NOT_JOINED,
    FRM_CNTR_ERR,
    INVALID_DATA_LENGTH,
    MAC_TX_OK,
    MAC_RX,
    MAC_ERR,
    NOT_RECOGNISED
}hw_resp_type;

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void lorawanhw_tx_isr_supervisor(char *p_msg);
hw_resp_type get_lorawanhw_resp_status(void);
void lorawanhw_fetch_resp(char * p_to_target_buffer);
bool get_lorawanhw_rx_payload(char * p_rx_payload_buffer);
bool get_lorawanhw_rx_string_waiting(void);
void clear_lorawanhw_rx_string_waiting(void);
void set_lorawanhw_rx_string_waiting(void);
void lorawanhw_trx_isr(void);

#endif /* PERIPHERAL_LORAWAN_LORAWANHW_H_ */
