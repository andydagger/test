/*
==========================================================================
 Name        : lorawanmac.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/lorawanmac.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 11 Jul 2017
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "string.h"
#include "config_lorawan.h"
#include "lorawan.h"
#include "lorawanmac.h"
#include "lorawanhw.h"
#include "common.h"
#include "systick.h"
#include "debug.h"
#include "sysmon.h"
#include "FreeRTOS.h"
#include "FreeRTOSCommonHooks.h"
#include "led.h"
#include "nwk_com.h"
#include "errorCode.h"
#include "LorawanStatusFlags.h"

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void lorawanmac_load_primary_resp_timeout(void);
static void lorawanmac_load_primary_slow_resp_timeout(void);
static void lorawanmac_load_secondary_resp_timeout(void);
static bool resp_wait_timed_out(void);
static bool lorawanmac_process_response(hw_resp_type);
// individual state functions
static bool lorawanmac_tx_cmd_state_idle(void);
static bool lorawanmac_tx_cmd_state_send_cmd(void);
static bool lorawanmac_tx_cmd_state_wait_for_response(void);
// individual responses functions
static bool resp_was_none(void);
static bool resp_was_ok(void);
static bool resp_was_invalid_param(void);
static bool resp_was_keys_not_init(void);
static bool resp_was_mcast_re_key(void);
static bool resp_was_no_free_ch(void);
static bool resp_was_silent(void);
static bool resp_was_busy(void);
static bool resp_was_mac_paused(void);
static bool resp_was_denied(void);
static bool resp_was_accepted(void);
static bool resp_was_not_joined(void);
static bool resp_was_frm_cntr_err(void);
static bool resp_was_invalid_data_length(void);
static bool resp_was_mac_tx_ok(void);
static bool resp_was_mac_rx(void);
static bool resp_was_mac_err(void);
static bool resp_was_not_recognised(void);

static void report_error(uint32_t error_code);
/*
mac join abp +0ms
ok +8ms
accepted +28ms
>> freertos delay cannot be greater than 10ms or second resp may overwrite first one.
*/
#define freeUpRtos() FreeRTOSDelay(10)
#define reSendDelay() FreeRTOSDelay(5000)
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
// declare the list of states: statetype
typedef enum
{
    TX_IDLE,
    SEND_CMD,
    WAIT_FOR_RESPONSE
}statetype_lorawanmac_tx;
// create the state-machine: smtype
typedef struct
{
    statetype_lorawanmac_tx current_state;
    bool (*func)(void);
    char * p_state_name_string;
}smtype_lorawanmac_tx;
// declare the state to function routing
static const smtype_lorawanmac_tx sm_lorawanmac_tx_sm[] =
{
    { TX_IDLE, lorawanmac_tx_cmd_state_idle, "> SEND MESSAGE > TX IDLE\r\n"},
    { SEND_CMD, lorawanmac_tx_cmd_state_send_cmd, "> SEND MESSAGE > SEND CMD\r\n"},
    { WAIT_FOR_RESPONSE, lorawanmac_tx_cmd_state_wait_for_response, "> SEND MESSAGE > WAIT FOR RESPONSE\r\n"}
};
// response struct
typedef struct
{
    hw_resp_type rx_resp;
    bool (*func)(void);
    char * p_resp_string;
}lorawanmac_transport_struct;
// response matrix
static const lorawanmac_transport_struct lorawanmac_transport_resp_actions[] =
{
    { NONE, resp_was_none, " ** no response so far **\r\n"},
    { OK, resp_was_ok, "** response was: ok **\r\n"},
    { INVALID_PARAM, resp_was_invalid_param, "** response was: invalid param **\r\n"},
    { KEYS_NOT_INIT, resp_was_keys_not_init, "** response was: keys not init **\r\n"},
    { MCAST_RE_KEY, resp_was_mcast_re_key, "** response was mcast re key **\r\n"},
    { NO_FREE_CH, resp_was_no_free_ch, "** response was: no fee channel **\r\n"},
    { SILENT, resp_was_silent, "** response was: silent **\r\n"},
    { BUSY, resp_was_busy, "** response was: busy **\r\n"},
    { MAC_PAUSED, resp_was_mac_paused, "** response was: paused **\r\n"},
    { DENIED, resp_was_denied, "** response was: denied **\r\n"},
    { ACCEPTED, resp_was_accepted, "** response was: accepted **\r\n"},
    { NOT_JOINED, resp_was_not_joined, "** response was:  ot joined **\r\n"},
    { FRM_CNTR_ERR, resp_was_frm_cntr_err, "** response was: frane counter error **\r\n"},
    { INVALID_DATA_LENGTH, resp_was_invalid_data_length, "** response was: invalid data length **\r\n"},
    { MAC_TX_OK, resp_was_mac_tx_ok, "** response was:  mac tx ok **\r\n"},
    { MAC_RX, resp_was_mac_rx, "** response was:  mac rx **\r\n"},
    { MAC_ERR, resp_was_mac_err, "** response was:  mac err **\r\n"},
    { NOT_RECOGNISED, resp_was_not_recognised, "** response was not recognised **\r\n"}
};

static statetype_lorawanmac_tx lorawanmac_tx_cmd_state = TX_IDLE;
static char *p_tx_data;
static bool b_dual_resp_expected;

typedef struct{
    uint32_t reference_tick_count;
    uint32_t timeout;
}delay_timer;

static delay_timer resp_ms_timer = {0};
static char mac_buffer[DOWN_MSG_PAYLOAD_ASCII_WITH_TERM];
static uint32_t reSendAttemptsDurToDutyCycle = 0;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * Initialises resp timer for primary response timeout
 *******************************************************************************/
static void lorawanmac_load_primary_slow_resp_timeout(void){
    resp_ms_timer.reference_tick_count = get_systick_ms();
    resp_ms_timer.timeout = MCHP_PRIMARY_SLOW_RESP_TIMEOUT_MS;
    return;
}

/*******************************************************************************
 * Initialises resp timer for primary response timeout
 *******************************************************************************/
static void lorawanmac_load_primary_resp_timeout(void){
    resp_ms_timer.reference_tick_count = get_systick_ms();
    resp_ms_timer.timeout = MCHP_PRIMARY_RESP_TIMEOUT_MS;
    return;
}

/*******************************************************************************
 * Initialises resp timer for secondary response timeout
 *******************************************************************************/
static void lorawanmac_load_secondary_resp_timeout(void){
    resp_ms_timer.reference_tick_count = get_systick_ms();
    resp_ms_timer.timeout = MCHP_SECONDARY_RESP_TIMEOUT_MS;
    return;
}

/*******************************************************************************
 * Check if the resp timer has timed out
 *******************************************************************************/
static bool resp_wait_timed_out(void){
    uint32_t elapsed_ms = (uint32_t)(get_systick_ms()  - resp_ms_timer.reference_tick_count);
    if(elapsed_ms >= resp_ms_timer.timeout){
        return true;
    }else{
        return false;
    }
}

/*******************************************************************************
 * Works out how to act on local and calling sm depending on response
 *******************************************************************************/
static bool lorawanmac_process_response(hw_resp_type resp){

    catch_error_fail( (*lorawanmac_transport_resp_actions[resp].func)() );

    if( resp > NONE ){
        debug_printf(LORAMAC, lorawanmac_transport_resp_actions[resp].p_resp_string);
    }

    return true;
}

/*******************************************************************************
 * Returns true only once the mchp replied OK to the given cmd
 *******************************************************************************/
bool lorawanmac_send_cmd_ok_only(char *p_cmd){

    if(p_cmd == NULL) return false;

    // send the command first
    lorawanhw_tx_isr_supervisor(p_cmd);
    lorawanmac_load_primary_resp_timeout();

    do{
        freeUpRtos();
        // return fail if moduel is not responding
        catch_test_report(  NO_RESPONSE,
                            resp_wait_timed_out() == true);

        if(get_lorawanhw_rx_string_waiting() == true){
            if(get_lorawanhw_resp_status() == OK){
                return true;
            }else{
                report_error(INVALID_RESP);
                return false;
            }
        }
    }while(1);

    return false;
}

/*******************************************************************************
 * Returns true only once the mchp replied OK to the given cmd
 *******************************************************************************/
bool lorawanmac_send_cmd_ok_only_slow(char *p_cmd){

    if(p_cmd == NULL) return false;

    // send the command first
    lorawanhw_tx_isr_supervisor(p_cmd);
    lorawanmac_load_primary_slow_resp_timeout();

    do{
        freeUpRtos();
        // return fail if moduel is not responding
        catch_test_report(  NO_RESPONSE,
                            resp_wait_timed_out() == true);

        if(get_lorawanhw_rx_string_waiting() == true){
            if(get_lorawanhw_resp_status() == OK){
                return true;
            }else{
                report_error(INVALID_RESP);
                return false;
            }
        }
    }while(1);

    return false;
}

/*******************************************************************************
 * Sends a get cmd and then waits for a response
 *******************************************************************************/
bool lorawanmac_send_query(char * p_get_cmd, char * p_res){

    if(p_get_cmd == NULL) return false;
    if(p_res == NULL) return false;

    // send the command first
    lorawanhw_tx_isr_supervisor(p_get_cmd);
    lorawanmac_load_primary_slow_resp_timeout();

    do{
        freeUpRtos();
        // return fail if moduel is not responding
        catch_test_report(  NO_RESPONSE,
                            resp_wait_timed_out() == true);
        if(get_lorawanhw_rx_string_waiting() == true)
        {
            lorawanhw_fetch_resp(p_res);
            return true;
        }                    
    }while(1);  

    return false;
}

/*******************************************************************************
 * Services the state-machine
 *******************************************************************************/
bool lorawanmac_send_cmd(char *p_cmd){
    p_tx_data = p_cmd; // copy ptr to tx data for local use.
    b_dual_resp_expected = true; // tx_cmd always need sdual response
    uint32_t send_cmd_start_time_sec = get_systick_sec();

    // always start at TX_SEND when called
    lorawanmac_tx_cmd_state = SEND_CMD;
    reSendAttemptsDurToDutyCycle = 0;

    do{
        catch_test_report(  TX_SM_STATE_OUT_OF_BOUND,
                            lorawanmac_tx_cmd_state
                            >= matrix_n_rows(sm_lorawanmac_tx_sm) );

        // call sm sate function
        catch_error_fail(
                (*sm_lorawanmac_tx_sm[lorawanmac_tx_cmd_state].func)()
                        );

        // In case of ime-out sending an uplink reset the module.
        if((get_systick_sec() - send_cmd_start_time_sec) > LORAWAN_SEND_CMD_TIMEOUT_SEC )
        {
            /* 
                If the module still does not respond during the initialising sequence
                the task will timeout and lead to a full system reboot.
            */
           if( reSendAttemptsDurToDutyCycle < 1 )
           {
                LorawanStatusFlags_SetModuleNeedsInitialising();
           }
            report_error(SEND_MESSAGE_TIMEOUT);
            errorCode_storeCode(NWK_ERROR, 0x01);
            return false;
        }

    }while(lorawanmac_tx_cmd_state != TX_IDLE);

    return true;
}


/*******************************************************************************
 * Always move to SEND_CMD
 *******************************************************************************/
static bool lorawanmac_tx_cmd_state_idle(void){
    lorawanmac_tx_cmd_state = SEND_CMD;
    return true;
}

/*******************************************************************************
 * Passes the data to the tx ISR
 *******************************************************************************/
static bool lorawanmac_tx_cmd_state_send_cmd(void){
    lorawanhw_tx_isr_supervisor(p_tx_data);
    lorawanmac_load_primary_resp_timeout();
    lorawanmac_tx_cmd_state = WAIT_FOR_RESPONSE;
    return true;
}

/*******************************************************************************
 * Wait for resp from mchp module and process accordingly
 *******************************************************************************/
static bool lorawanmac_tx_cmd_state_wait_for_response(void){
    freeUpRtos();
    // If module hasn't responded return to SEND_CMD to resend
    if( resp_wait_timed_out() == true){
        lorawanmac_tx_cmd_state = SEND_CMD;
        reSendDelay();
    }else{ // if response was received work out what to do next
        catch_error_fail( lorawanmac_process_response(
                            get_lorawanhw_resp_status() ) );
    }
    return true;
}

/*******************************************************************************
 * Allfunctions below will act depending on received response
 *******************************************************************************/

static bool resp_was_none(void){
    //NOOP
    return true;
}

static bool resp_was_ok(void){
    if(b_dual_resp_expected == true){
        lorawanmac_load_secondary_resp_timeout();
    }else{
        lorawanmac_tx_cmd_state = TX_IDLE;
    }
    return true;
}

static bool resp_was_invalid_param(void){
    lorawanmac_tx_cmd_state = SEND_CMD;
    reSendDelay();
    return true;
}

static bool resp_was_keys_not_init(void){
    // TODO: This will result in being out of sync
    // with LNS >> RESET MODULE.
    report_error(KEYS_INIT);
    // reset hw module
    LorawanStatusFlags_SetModuleNeedsInitialising();
    lorawanmac_tx_cmd_state = TX_IDLE;
    return false;
}

static bool resp_was_mcast_re_key(void){
    // TODO: This will result in being out of sync
    // with LNS >> end mcast session.
    report_error(MCAST_KEYS_INIT);
    LorawanStatusFlags_SetMcastSessionShouldBeActive(false);
    lorawanmac_tx_cmd_state = TX_IDLE;
    return false;
}

static bool resp_was_no_free_ch(void){
    lorawanmac_tx_cmd_state = SEND_CMD;
    reSendDelay();
    reSendAttemptsDurToDutyCycle++;
    return true;
}

static bool resp_was_silent(void){
    // Should not happen
    LorawanStatusFlags_SetModuleNeedsInitialising();
    lorawanmac_tx_cmd_state = TX_IDLE;
    return false;
}

static bool resp_was_busy(void){
    lorawanmac_tx_cmd_state = SEND_CMD;
    reSendDelay();
    return true;
}

static bool resp_was_mac_paused(void){
    // SHould not happen
    LorawanStatusFlags_SetModuleNeedsInitialising();
    lorawanmac_tx_cmd_state = TX_IDLE;
    return false;
}

static bool resp_was_denied(void){
    LorawanStatusFlags_ClearJoinedFlag();
    lorawanmac_tx_cmd_state = TX_IDLE;
    return true;
}

static bool resp_was_accepted(void){
    lorawanmac_tx_cmd_state = TX_IDLE;
    LorawanStatusFlags_SetJoinedFlag();
    return true;
}

static bool resp_was_not_joined(void){
    LorawanStatusFlags_SetModuleNeedsInitialising();
    lorawanmac_tx_cmd_state = TX_IDLE;
    return false;
}

static bool resp_was_frm_cntr_err(void){
    report_error(FRAME_CNTR);
    // reset hw module
    LorawanStatusFlags_SetModuleNeedsInitialising();
    lorawanmac_tx_cmd_state = TX_IDLE;
    return false;
}

static bool resp_was_invalid_data_length(void){
    report_error(DATA_LENGTH);
    lorawanmac_tx_cmd_state = TX_IDLE;
    return false;
}

static bool resp_was_mac_tx_ok(void){
    lorawanmac_tx_cmd_state = TX_IDLE;
    return true;
}

static bool resp_was_mac_rx(void){
    lorawanmac_tx_cmd_state = TX_IDLE;
    set_lorawanhw_rx_string_waiting();
    return true;
}

static bool resp_was_mac_err(void){
    /*
     * According to Microchip:
     * - ACK not received for confirmed transmission
     * - Received packet Fcnt gap is greater than maxFCntGap allowed
     * - Radio transmission failure
     * >> RESET AND REJOIN
     */
    report_error(TX_MAC_ERR);
    LorawanStatusFlags_SetModuleSettingsNeedOptimising();
    lorawanmac_tx_cmd_state = TX_IDLE;
    return false;
}

static bool resp_was_not_recognised(void){
    lorawanmac_tx_cmd_state = SEND_CMD;
    reSendDelay();
    return true;
}

/*******************************************************************************
 * Fetch data-rate setting
 *******************************************************************************/
uint32_t get_lorawanmac_dr(void){
    uint32_t data_rate = 0;
    // fetch data-rate value from module
    mac_buffer[0] = '\0';
    do{
        lorawanmac_send_query("mac get dr\r\n", mac_buffer);
    }
    while( '\0' == mac_buffer[0] );

    // Convert to decimal
    data_rate = lz_ascii_to_uint32_ascii(mac_buffer);
    debug_printf( LORAMAC, "DR = %lu\r\n", data_rate);
    // should never be > 5
    if(data_rate > 5) data_rate = 5;

    // should be from 0 to 5
    return data_rate;
}

/*******************************************************************************
 * Fetch power index setting setting
 *******************************************************************************/
uint32_t get_lorawanmac_pwridx(void){
    uint32_t power_index = 1;
    // fetch data-rate value from module
    mac_buffer[0] = '\0';
    do{
        lorawanmac_send_query("mac get pwridx\r\n", mac_buffer);
    }
    while( '\0' == mac_buffer[0] );

    // Convert to decimal
    power_index = lz_ascii_to_uint32_ascii(mac_buffer);
    debug_printf( LORAMAC, "PWR IDX = %lu\r\n", power_index);
    // should never be > 5
    if(power_index > 5) power_index = 5;

    // should be from 1 o 5 from 868 band
    return power_index;
}


/*******************************************************************************
 * Report error to sysmon task
 * @param[in] error code
 * @return void
 *******************************************************************************/
static void report_error(uint32_t error_code){
	sysmon_report_error(LORA, error_code);
    return;
}

/***************************** EOF ******************************************/
