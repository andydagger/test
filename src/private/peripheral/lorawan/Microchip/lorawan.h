/*
==========================================================================
 Name        : lorawan.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/lorawan.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 11 Jul 2017
 Description :
==========================================================================
*/

#ifndef PERIPHERAL_LORAWAN_LORAWAN_H_
#define PERIPHERAL_LORAWAN_LORAWAN_H_

#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Definitions
 ****************************************************************************/
#define NEW_CHANNEL_REQ     (1<<11)
#define LINK_ADR_REQ_PWR    (1<<12)
#define LINK_ADR_REQ_NDREP  (1<<13)
#define DUTY_CYCLE_REQ      (1<<14)
#define RX_PARAM_SETUP_REQ  (1<<15)
#define RX_TIMING_SETUP_REQ (1<<16)

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

bool lorawan_init(bool * allowNetworkSessionRecovery);
bool lorawan_join_network(bool restoringSession);
char * lorawan_read_msg(void);
bool lorawan_send_msg(char * p_msg_to_send, bool ack_is_required);
bool lorawan_update_mcast_state(bool mcastRequestedState);
void lorawan_save_session_info(void);
bool lorawan_optimise_settings(void);
void get_lorawan_dev_eui(char * p_devEuiString);

#endif /* PERIPHERAL_LORAWAN_LORAWAN_H_ */


