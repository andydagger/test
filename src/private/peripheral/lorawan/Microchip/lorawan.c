/*
==========================================================================
 Name        : lorawan.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/lorawan.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 11 Jul 2017
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "stdbool.h"
#include "stdint.h"
#include "string.h"
#include "stdio.h"
#include "lorawan.h"
#include "config_lorawan.h"
#include "lorawanmac.h"
#include "lorawanhw.h"
#include "lorawaninit.h"
#include "common.h"
#include "systick.h"
#include "debug.h"
#include "sysmon.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "FreeRTOSMacros.h"
#include "FreeRTOSCommonHooks.h"
#include "LorawanStatusFlags.h"

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void lorawan_rst_mcast_cntr(void);

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

// used by tx and rx but only one at a time.
static char trx_buffer[UP_MSG_MICROCHIP_STRING_MAX];

static void report_error(uint32_t error_code);
static void lorawan_join_duty_cycle_handler(void);

static uint8_t join_dr = 5;
static uint8_t pre_join_dr = 0;
static uint8_t attempts_at_current_dr = 1;
static uint32_t time_of_prev_join_request = (uint32_t)0xFFFF;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
extern QueueHandle_t xQuRxMsg; // vTaskRxMsg
extern QueueHandle_t xQuTxData; // vTaskTxData

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * Calls the lorawan init sm, and, when done, moves on to joining
 *******************************************************************************/
bool lorawan_init(bool * allowNetworkSessionRecovery){
    return lorawaninit_exe(allowNetworkSessionRecovery);
}


/*******************************************************************************
 * Sends join request to the network until time-out
 *******************************************************************************/
bool lorawan_join_network(bool restoringSession){

    // Skip if rstoring previous session.
    if( false == restoringSession ){
        // Even with intelligent use of DR always leave
        // INTER_JOIN_REQ_DELAY_SEC between each request
        if(     (get_systick_sec() - time_of_prev_join_request)
                > INTER_JOIN_REQ_DELAY_SEC ){
            time_of_prev_join_request = get_systick_sec();
        }else{
            return false;
        }

        // Join-Request DR optimisation
        lorawan_join_duty_cycle_handler();

        // join status flag set within lorawanmac layer
        if(lorawanmac_send_cmd("mac join otaa\r\n") == true)
        {
            // only increment attempt counter if accepted or denied was received
            attempts_at_current_dr++;
        }
        else
        {
            return false;
        }
    }

    // If last attempt return Denied return false
    if(LorawanStatusFlags_GetJoinedFlag() == false)
    {
        return false;
    }

    // "mac set upctr 1" - ABP Fix to stop the module going into a
    // defailt class-A sate of sleep after rejecting a Join Accept aimed
    // at another node
    lorawanmac_send_cmd("mac join abp\r\n");

    debug_printf(LORA, "Joined !\r\n");

    return true;

}

/*******************************************************************************
 * Manages Join-Requests duty-cycles
 *******************************************************************************/
static void lorawan_join_duty_cycle_handler(void){
    char set_dr_str[15] = {0};

    // Every JOIN_REQ_AT_SAME_DR_LIMIT attempts at same DR:
    if(attempts_at_current_dr > (uint8_t)JOIN_REQ_AT_SAME_DR_LIMIT ){
        attempts_at_current_dr = 1;
        // lower data-rate, down to 0
        if( join_dr > 0){
            join_dr--;
        }else{
            // if attempts failed on 0 try again at higher DR
            join_dr = 5;
        }
    }

    // check if module needs dr adjusting
    if( join_dr != pre_join_dr){
        pre_join_dr = join_dr;
        sprintf(set_dr_str, "mac set dr %d\r\n", join_dr);
        lorawanmac_send_cmd_ok_only(set_dr_str);
    }

}
/*******************************************************************************
 * Copies the received payload to a local buffer returns prt to it if valid
 *******************************************************************************/
char * lorawan_read_msg(void){

    // if payload valid return ptr to local buffer
    if( true == get_lorawanhw_rx_payload(trx_buffer) ){
        return trx_buffer;
    }else{
        report_error(MSG_RX_INVALID);
        return NULL;
    }
}

/*******************************************************************************
 * Post Uplink rountine to trigger mock uplink with LinkADRAns
 *******************************************************************************/
static void postUplinkCheckForMacResp()
{
    uint32_t macStatus = 0;
    /*
        This post uplink routine is required to insure the LinkADRAns
        is transmitted immediately following reception of a LinkADRReq
        to avoid it being lost (known bug in RC20).
        Byte4 / Bit1: NbRep updated (‘1’ – updated via LinkADRReq MAC command)
        Byte4 / Bit0: Output power updated (‘1’ – updated via LinkADRReq MAC command)
        Also applied to all other MACReq flags.
    */
   //----------------------------------------
   // Wait to allow for Req arriving in RX2
    FreeRTOSDelay(3000);
   //----------------------------------------

    if( lorawanmac_send_query("mac get status\r\n", trx_buffer) ==  true)
        macStatus = lz_char_to_uint32_ascii(trx_buffer);
    else
        macStatus = 0;

    if( macStatus   & (uint32_t)( NEW_CHANNEL_REQ
                                | LINK_ADR_REQ_PWR
                                | LINK_ADR_REQ_NDREP
                                | DUTY_CYCLE_REQ
                                | RX_PARAM_SETUP_REQ
                                | RX_TIMING_SETUP_REQ))
    {
        debug_printf(LORA, ">>> MAC Resp Pending!!!\r\n");
        FreeRTOSDelay(1000);
        lorawanmac_send_cmd("mac tx uncnf 3 00000000\r\n");
    }
}

/*******************************************************************************
 * Passes app payload down to host mac layer for transmission
 *******************************************************************************/
bool lorawan_send_msg(char * p_msg_to_send, bool ack_is_required){

    if(p_msg_to_send == NULL) return false;
    if(strlen(p_msg_to_send) == 0) return false; 
    if(strlen(p_msg_to_send) > UP_MSG_PAYLOAD_ASCII_NO_TERM) return false; 

    if( ack_is_required == true){
        strcpy(trx_buffer, "mac tx cnf 1 ");
    }else{
        strcpy(trx_buffer, "mac tx uncnf 1 ");
    }
    strcat(trx_buffer, p_msg_to_send);
    strcat(trx_buffer, "\r\n");

    // return success flag from called function
    bool res =  lorawanmac_send_cmd(trx_buffer);

    if(res == true)
    {
        postUplinkCheckForMacResp();
    }

    return res;
}

/*******************************************************************************
 * Processes mcast session start-exit requests
 *******************************************************************************/
bool lorawan_update_mcast_state(bool mcastRequestedState){
    char mcast_str[20] = {0};

    if(mcastRequestedState == true){
        strcpy(mcast_str, "mac set mcast on\r\n");
        // Before initiating a new ncast session: reset dlc
        lorawan_rst_mcast_cntr();
    }else{
        strcpy(mcast_str, "mac set mcast off\r\n");
    }

    catch_error_report( MCAST_SESSION_CHANGE,
                        lorawanmac_send_cmd_ok_only(mcast_str));

    return true;
}

/********************************************************************************
 * Reset mcast downlink counter
 *******************************************************************************/
static void lorawan_rst_mcast_cntr(void){
	lorawanmac_send_cmd_ok_only("mac set mcastdnctr 0\r\n");
    return;
}

/*******************************************************************************
 * Save lorawan session info
 * @param[in] void
 * @return void
 *******************************************************************************/
void lorawan_save_session_info(void){
    debug_printf( LORA, "Save session info.\r\n");
    lorawanmac_send_cmd_ok_only_slow("mac save\r\n");
    return;
}

/*******************************************************************************
 * Optimise settings or trigger rejoin accordingly
 *******************************************************************************/
bool lorawan_optimise_settings(void){
    uint32_t data_rate = 0;
    uint32_t power_index = 0;
    // If DR = 0 and PWR_IDX = 1 already
    // force re-join
    // else force optimal parameters
    data_rate = get_lorawanmac_dr();
    power_index = get_lorawanmac_pwridx();
    if( ( 0 == data_rate ) && ( 1 == power_index ) ){
        // reset hw module
    	return false;
    }else{
        lorawanmac_send_cmd_ok_only("mac set pwridx 1\r\n");
        lorawanmac_send_cmd_ok_only("mac set dr 0\r\n");
    }
    return true;
}

/*******************************************************************************
 * Fetch DEVEUI from init stack and return true if available
 *******************************************************************************/
void get_lorawan_dev_eui(char * p_devEuiString)
{
    get_lorawaninit_hweui(p_devEuiString);
    return;
}

/*******************************************************************************
 * Report error to sysmon task
 *******************************************************************************/
static void report_error(uint32_t error_code){
    sysmon_report_error(LORA, error_code);
    return;
}


/***************************** EOF ******************************************/





