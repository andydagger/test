/*
==========================================================================
 Name        : lorawaninit.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/lorawaninit.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 13 Jul 2017
 Description :
==========================================================================
*/

/********************************************************************************
 * Include files
 ****************************************************************************/
#include "stdint.h"
#include "stdbool.h"
#include "stdio.h"
#include "string.h"
#include "config_lorawan.h"
#include "config_network.h"
#include "hw.h"
#include "lorawan.h"
#include "lorawanmac.h"
#include "common.h"
#include "debug.h"
#include "FreeRTOS.h"
#include "FreeRTOSCommonHooks.h"
#include "LorawanStatusFlags.h"
#include "productionMonitor.h"

/********************************************************************************
 * Private functions
 ****************************************************************************/
static void lorawan_init_stack(void);
static void lorawan_reset_module(void);
static void lorawan_activate_coms(void);
static void setup_module(void);
static void store_fw_version(void);
static void store_dev_eui(void);

/********************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define RESET_RETRIES_LIMIT 3
#define RESET_RETRY_DELAY_MS 2000   

static uint8_t lorawaninit_last_setting_sent = 0;
static char mchp_hwstr[DOWN_MSG_MICROCHIP_STRING_MAX];
static const char * const lorawaninit_setup_cmdlist[][2] ={
        { "mac set mcastdevaddr ", MCAST_DEV_ADDR },
        { "mac set mcastappskey ", MCAST_APPSKEY },
        { "mac set mcastnwkskey ", MCAST_NWKSKEY },
        { "mac set appskey ", APPSKEY },
        { "mac set nwkskey ", NWKSKEY },
        { "mac set devaddr ", DEV_ADDR },
        { "mac set pwridx 1", "" },
        { "mac set dr 0", "" },
        { "mac set adr on", "" },
        { "mac set retx ", UP_ACK_RETRIES },
        { "mac set linkchk ", LINK_CHECK_PERIOD },
        { "mac set rxdelay1 ", RECEIVE_DELAY1 },
        { "mac set ar on", "" },
        { "mac set rx2 ", RX2_DR_AND_FREQ },
        { "mac set sync 34", "" },
        { "mac set upctr 0", "" },
        { "mac set dnctr 0", "" },
        { "mac set ch dcycle 0 302", "" }, //302
        { "mac set ch dcycle 1 302", "" },
        { "mac set ch dcycle 2 302", "" },
        { "mac set ch drrange 0 0 5", "" },
        { "mac set ch drrange 1 0 5", "" },
        { "mac set ch drrange 2 0 5", "" },
        { "mac set ch status 0 on", "" },
        { "mac set ch status 1 on", "" },
        { "mac set ch status 2 on", "" },
        { "mac set class c", "" },
};

static uint16_t module_fw_version = 0;
static char hwEui[NWK_HW_EUI_LENGTH + 1] = {0};
static bool devEuiReadyForReading = false;
/*****************************************************************************
 * Public types/enumerations/variables
 *******************************************************************************/

/******************************************************************************
 * Public functions
 *******************************************************************************/

/*****************************************************************************
 * GENERAL FUNCTIONS
 ****************************************************************************/

/********************************************************************************
 * @brief Initialise lower stacks
 * @param[in] void
 * @return void
 *******************************************************************************/
static void lorawan_init_stack(void){
    // TODO: if needed
    return;
}

static void lorawan_reset_module(void){
    lorawan_disable_module();
    FreeRTOSDelay(500);
    lorawan_enable_module();
    FreeRTOSDelay(1000);
	return;
}

/********************************************************************************
 * @brief Initialise serial port
 * @param[in] void
 * @return void
 *******************************************************************************/
static void lorawan_activate_coms(void){
    lorawan_set_baudrate(57600);
	lorawan_reset_module();
    lorawan_irq_enable();
    lorawan_rx_disable();
    return;
}

void get_lorawaninit_hweui(char * p_str){
    if(devEuiReadyForReading)
    {
        strncpy(p_str, hwEui,sizeof hwEui);
    }
}

/*******************************************************************************
 * @brief Extracts the module fw version and RC number from returned string
 * @param[in] void
 * @return void
 *******************************************************************************/
static void store_fw_version(void){

    // 012345678901234567890
    // RN2483 1.0.5 RC20 Oct 09 2018 16:36:17
    uint8_t va = mchp_hwstr[7] - 0x30;
    uint8_t vb = mchp_hwstr[9] - 0x30;
    uint8_t vc = mchp_hwstr[11] - 0x30;
    uint8_t rc = (mchp_hwstr[15] - 0x30) * 10 +(mchp_hwstr[16] - 0x30) ;

    module_fw_version = va*100 + vb*10 + vc;
    module_fw_version = module_fw_version << 8;
    module_fw_version = module_fw_version + rc;
    return;
}

/*******************************************************************************
 * @brief Stores the module DEVEUI locally
 * @param[in] void
 * @return void
 *******************************************************************************/
static void store_dev_eui(void){
    strncpy(hwEui, mchp_hwstr, NWK_HW_EUI_LENGTH);
    return;
}

/*******************************************************************************
 * @brief Returns the module fw version info
 * @param[in] void
 * @return uint16_t of concatenated fw version info
 *******************************************************************************/
uint16_t get_lorawaninit_fw_version(void){
    return module_fw_version;
}

/*******************************************************************************
 * @brief lorawan init state-machine servicing
 * @param[in] void
 * @return bool true for initialising is ongoing
 *******************************************************************************/
bool lorawaninit_exe(bool * allowNetworkSessionRecovery){
//    static char * p_hweui_str = mchp_hwstr;
    uint32_t upfcntr = 0;
    uint8_t retry = 0;

    // initialise stack and hw
    lorawan_init_stack();
    lorawan_activate_coms();
    

    // send soft reset cmd until response starts with RN2483
    // or retries expired.
    mchp_hwstr[0] = '\0';
    lorawan_reset_module();
    lorawanmac_send_query("sys reset\r\n", mchp_hwstr);
    while(strncmp("RN2483", mchp_hwstr, 6) != 0)
    {
        if(++retry >= RESET_RETRIES_LIMIT) return false;
        FreeRTOSDelay(RESET_RETRY_DELAY_MS);
        mchp_hwstr[0] = '\0';
        lorawan_reset_module();
        lorawanmac_send_query("sys reset\r\n", mchp_hwstr);
    }
    store_fw_version();

    // read back DEVEUI
    retry = 0;
    while( false == lorawanmac_send_query("mac get deveui\r\n", mchp_hwstr))
    {
        if(++retry >= RESET_RETRIES_LIMIT) return false;
        FreeRTOSDelay(RESET_RETRY_DELAY_MS);
    }
    store_dev_eui();

    /*
    * Only restore session on first call from boot-up
    * Do not allow session-restore from run-time.
    */
    if( *allowNetworkSessionRecovery == true)
    {
        // If session info already stored set as joined and return
        // First fetch the uplink counter
        mchp_hwstr[0] = '\0';
        do{
            lorawanmac_send_query("mac get upctr\r\n", mchp_hwstr);
        }
        while( '\0' == mchp_hwstr[0] );

        // Convert to decimal
        upfcntr = lz_ascii_to_uint32_ascii(mchp_hwstr);
        debug_printf( LORA, "UpcFCnt = %lu\r\n", upfcntr);

        // Check if null = no session info
        if( upfcntr > LORAWAN_RESTORE_SESSION_UPLINK_COUNT_THRESHOLD )
        {
            debug_printf( LORA, "Use existing session info: no join required.\r\n");
            return true;
        }
        else
        {
            // Indicate that session was not restored > join will be required.
            *allowNetworkSessionRecovery = false;
        }
    }

    // send full reset cmd until response starts with RN2483
    mchp_hwstr[0] = '\0';
    do{
    	lorawan_reset_module();
    	lorawanmac_send_query("sys factoryRESET\r\n", mchp_hwstr);
    }
    while( strncmp("RN2483", mchp_hwstr, 6) != 0);

    // send reset in 868 mode cmd
    while(lorawanmac_send_cmd_ok_only("mac reset 868\r\n") != true);
    lorawaninit_last_setting_sent = 0;

    // send config
    setup_module();

    return true;
}

/*******************************************************************************
 * @brief Sends a series of initialisation cmd to the mchp module
 * @param[in] void
 * @return void
 *******************************************************************************/
static void setup_module(void){
    static char current_setup_str[UP_MSG_PAYLOAD_ASCII_WITH_TERM];

    // Use basic APPKEY and APPEUI if no credentials available.
    if(productionMonitor_getUnitIsUndertest() == true)
    {
        // Use module HWEUI as DEVEUI
        sprintf(current_setup_str, "mac set deveui %s\r\n", hwEui);
        // wait for deveui to be applied successfully
        while(lorawanmac_send_cmd_ok_only(current_setup_str) != true);
        sprintf(current_setup_str, "mac set appeui %s\r\n", PROD_APP_EUI);
        while(lorawanmac_send_cmd_ok_only(current_setup_str) != true);
        sprintf(current_setup_str, "mac set appkey %s\r\n", PROD_APP_KEY);
        while(lorawanmac_send_cmd_ok_only(current_setup_str) != true);
    }
    else
    {
        // Use module Flash for DEVEUI
        sprintf(current_setup_str, "mac set deveui %s\r\n", DEV_EUI);
        // wait for deveui to be applied successfully
        while(lorawanmac_send_cmd_ok_only(current_setup_str) != true);
        sprintf(current_setup_str, "mac set appeui %s\r\n", APP_EUI);
        while(lorawanmac_send_cmd_ok_only(current_setup_str) != true);
        sprintf(current_setup_str, "mac set appkey %s\r\n", APP_KEY);
        while(lorawanmac_send_cmd_ok_only(current_setup_str) != true);
    }

    do{
        // concatenate cmd string and argument
        strcpy( current_setup_str,
                lorawaninit_setup_cmdlist[lorawaninit_last_setting_sent][0]);
        strcat( current_setup_str,
                lorawaninit_setup_cmdlist[lorawaninit_last_setting_sent][1]);
        strcat(current_setup_str, "\r\n");
        // wait for module to ok cmd
        while(lorawanmac_send_cmd_ok_only(current_setup_str) != true);
        // move on to next cmd
        lorawaninit_last_setting_sent++;


        // ,oop until all cmds in the list have been sent
    }while( lorawaninit_last_setting_sent
            < matrix_n_rows(lorawaninit_setup_cmdlist) );

    lorawanmac_send_cmd_ok_only_slow("mac save\r\n");

    return;
}

/************************************* EOF *************************************/
