/*
==========================================================================
 Name        : lorawanhw.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/lorawanhw.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 11 Jul 2017
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "stdint.h"
#include "stdbool.h"
#include "string.h"
#include "config_lorawan.h"
#include "config_pinout.h"
#include "hw.h"
#include "lorawanhw.h"
#include "common.h"
#include "debug.h"
#include "sysmon.h"

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void lorawanhw_tx_isr(void);
static void lorawanhw_rx_isr(void);
static void wipe_rx_buffer(void);

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static volatile char rx_isr_buffer[DOWN_MSG_MICROCHIP_STRING_MAX];
static volatile uint16_t n_char_received = 0;
static volatile bool b_received_string_waiting = false;

static volatile char tx_isr_buffer[UP_MSG_MICROCHIP_STRING_MAX];
static volatile uint16_t n_char_transmitted = 0;

typedef struct{
    hw_resp_type rx_resp;
    char *string_content;
    uint8_t string_length;
}listtype_resp_list;

static const listtype_resp_list resp_list[] =
{
        { NONE, "" , 0 },
        { OK, "ok", 2 },
        { INVALID_PARAM,"invalid_param", 13 },
        { KEYS_NOT_INIT, "keys_not_init", 13 },
        { MCAST_RE_KEY, "mcast_re_key", 12 },
        { NO_FREE_CH, "no_free_ch", 10 },
        { SILENT, "silent", 6 },
        { BUSY, "busy", 4 },
        { MAC_PAUSED, "mac_paused", 10 },
        { DENIED, "denied", 6 },
        { ACCEPTED, "accepted", 8 },
        { NOT_JOINED, "not_joined", 10 },
        { FRM_CNTR_ERR, "frame_counter_err_rejoin_needed", 31 },
        { INVALID_DATA_LENGTH, "invalid_data_length", 19 },
        { MAC_TX_OK, "mac_tx_ok", 9 },
        { MAC_RX, "mac_rx", 6 },
        { MAC_ERR, "mac_err", 7 },
        { NOT_RECOGNISED, "", 0}
};

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/


/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 * Prepares hw interface for isr driven tx sequence
 *******************************************************************************/
void lorawanhw_tx_isr_supervisor(char *p_msg){
    if(p_msg == NULL) return;
    if(strlen(p_msg) > UP_MSG_MICROCHIP_STRING_MAX) return;
    
    strncpy( (char *)tx_isr_buffer, p_msg, UP_MSG_MICROCHIP_STRING_MAX);

    debug_printf(LORA_TX, (char *)tx_isr_buffer);

    // disable rx and prepare for next response
    lorawan_rx_disable();
    lorawan_rx_byte(); // clear pending byte
    wipe_rx_buffer();
    b_received_string_waiting = false;

    // Initialise tx variables
    n_char_transmitted = 0;
    // Enable tx
    lorawan_tx_enable();

    return;
}

/*******************************************************************************
 * Once a full string has been received from the module,
 * establish string id else return NONE
 *******************************************************************************/
hw_resp_type get_lorawanhw_resp_status(void){
    uint8_t resp_id = NONE;
    uint8_t string_id = NONE;

    if( b_received_string_waiting ){
        // look for a string match
        for(string_id = OK; string_id < NOT_RECOGNISED; string_id++){
            if( strncmp( resp_list[string_id].string_content,
            		(char *)rx_isr_buffer, resp_list[string_id].string_length) == 0){
                resp_id = string_id;
                break;
            }else{
                resp_id = NOT_RECOGNISED;
            }
        }
        clear_lorawanhw_rx_string_waiting();
    }else{
        resp_id = NONE;
    }

    if(resp_id > NONE){
        debug_printf(LORAMAC, "%s\r\n", resp_list[resp_id].string_content);
    }
    return resp_id;
}


/*******************************************************************************
 * Used by the upper stack to retrieve response to a mac get cmd
 *******************************************************************************/
void lorawanhw_fetch_resp(char * p_to_target_buffer){
    if(p_to_target_buffer == NULL) return;
    strncpy( p_to_target_buffer, (char *)rx_isr_buffer, DOWN_MSG_MICROCHIP_STRING_MAX );
    wipe_rx_buffer();
    return;
}

/*******************************************************************************
 * Used by the upper stack to retrieve received app payload
 *******************************************************************************/
bool get_lorawanhw_rx_payload(char * p_rx_payload_buffer){
    bool b_valid_rx_payload = false;
    char * p_to_rx_buffer = NULL;

    if(p_rx_payload_buffer == NULL) return false;

    // Check rx payload integrity
    if(strncmp( resp_list[MAC_RX].string_content, (char *)rx_isr_buffer,
            resp_list[MAC_RX].string_length) == 0){
        
        p_to_rx_buffer = (char *)rx_isr_buffer; // point to rx buffer start
        p_to_rx_buffer = strchr( p_to_rx_buffer, ' '); // point to first space char
        if(p_to_rx_buffer != NULL)
        {
            // MCHP fw 1.0.5 CR2 corrected the missing ' ' after mac_rx
            p_to_rx_buffer = strchr( p_to_rx_buffer + 1, ' '); // point to second space char
            if(p_to_rx_buffer != NULL)
            {
                // point to first char of received payload
                p_to_rx_buffer++; 

                // last char was previously forced to \0 for safety
                strncpy( p_rx_payload_buffer, p_to_rx_buffer, DOWN_MSG_PAYLOAD_ASCII_WITH_TERM );
                b_valid_rx_payload = true;
            }

        }

    }else{
        b_valid_rx_payload = false;
    }
    wipe_rx_buffer();
    clear_lorawanhw_rx_string_waiting();
    return b_valid_rx_payload;
}

/*******************************************************************************
 * Used by the upper stack to check if a class c payload was received
 *******************************************************************************/
bool get_lorawanhw_rx_string_waiting(void){
    return b_received_string_waiting;
}

/*******************************************************************************
 * Used by the upper stack to clear flag
 *******************************************************************************/
void clear_lorawanhw_rx_string_waiting(void){
    b_received_string_waiting = false;
    lorawan_rx_enable();
    return;
}
/*******************************************************************************
 * Used by the upper stack to set flag
 *******************************************************************************/
void set_lorawanhw_rx_string_waiting(void){
    b_received_string_waiting = true;

    /*
     * Replace the lastbyte with the terminatin char \0 so
     * that the str is always safely terminated even if more
     * than LORAWAN_STRING_MAX_LENGTH bytes are received
     */
    rx_isr_buffer[DOWN_MSG_MICROCHIP_STRING_MAX - 1] = '\0';

    lorawan_rx_disable();
    return;
}

/*******************************************************************************
 * WIpes rx irq buffer
 *******************************************************************************/
static void wipe_rx_buffer(void){
    uint16_t i = 0;
    n_char_received = 0;
    // Wipe rx irq buffer
    for(i = 0 ; i < (DOWN_MSG_MICROCHIP_STRING_MAX - 1); i++){
        rx_isr_buffer[i] = 0;
    }
    return;
}

/*******************************************************************************
 * lorawan serial port interrupt routine
 *******************************************************************************/
void lorawanhw_trx_isr(void){
    uint32_t intsrc = 0;

    // Read out irq sources
    intsrc = lorawan_get_int_status();
    // work out if tx or rx isr should be called
    if( lorawan_was_rx_isr(intsrc))
    {
        lorawanhw_rx_isr();
    }
    else if(lorawan_was_tx_isr(intsrc))
    {
        lorawanhw_tx_isr();
    }else{
        // NOOP: incorrect IRQ source
    }

    return;
}

 /*******************************************************************************
  * tx interrupt routine
  *******************************************************************************/
static void lorawanhw_tx_isr(void){
     char * p_next_byte;
     p_next_byte = (char *)(tx_isr_buffer + n_char_transmitted); // point to next byte
     lorawan_tx_byte(*p_next_byte); // parse byte to mapped serial port
     if( *p_next_byte == LORAWAN_END_CHAR ){ // if this was the end char
         lorawan_tx_disable(); // end tx
         lorawan_rx_enable(); // now enable rx for response
     }else{
         n_char_transmitted++; // increment n of char sent
     }
     if(n_char_transmitted >= UP_MSG_MICROCHIP_STRING_MAX)
     {
         lorawan_tx_disable(); // end tx
         lorawan_rx_enable(); // now enable rx for response
     }
     return;
 }


 /*******************************************************************************
  * rx interrupt routine
  *******************************************************************************/
 static void lorawanhw_rx_isr(void){
     char * p_next_byte;

     p_next_byte = (char *)(rx_isr_buffer + n_char_received); // point to next byte location
     *p_next_byte = lorawan_rx_byte(); // read out received byte
     if(*p_next_byte == LORAWAN_END_CHAR){ // if this was the last char
         set_lorawanhw_rx_string_waiting(); // use to inform rest of stack
         n_char_received = 0; // reset position for next reception
         debug_printf(LORA_RX, (char *)rx_isr_buffer);
     }else{
         if(n_char_received < DOWN_MSG_MICROCHIP_STRING_MAX )
         {
             n_char_received++;
         }
         else
         {
            n_char_received = 0;
            rx_isr_buffer[0] = '\0';
            set_lorawanhw_rx_string_waiting();
         }
     }
     return;
 }


 /***************************** EOF ******************************************/
