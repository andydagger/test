/*
==========================================================================
 Name        : lorawanmac.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/lorawanmac.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 11 Jul 2017
 Description :
==========================================================================
*/

#ifndef PERIPHERAL_LORAWAN_LORAWANMAC_H_
#define PERIPHERAL_LORAWAN_LORAWANMAC_H_

/*****************************************************************************
 * Definitions
 ****************************************************************************/
#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
bool lorawanmac_send_cmd(char *p_cmd);
bool lorawanmac_send_cmd_ok_only(char *p_cmd);
bool lorawanmac_send_cmd_ok_only_slow(char *p_cmd);
bool lorawanmac_get_value( char * p_get_cmd, char * p_res);
bool lorawanmac_send_query( char * p_get_cmd, char * p_res);

uint32_t get_lorawanmac_dr(void);
uint32_t get_lorawanmac_pwridx(void);

#endif /* PERIPHERAL_LORAWAN_LORAWANMAC_H_ */
