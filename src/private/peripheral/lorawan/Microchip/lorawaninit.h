/*
==========================================================================
 Name        : lorawaninit.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/lorawaninit.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 13 Jul 2017
 Description :
==========================================================================
*/

#ifndef PERIPHERAL_LORAWAN_LORAWANINIT_H_
#define PERIPHERAL_LORAWAN_LORAWANINIT_H_

/*****************************************************************************
 * Definitions
 ****************************************************************************/
#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
bool lorawaninit_exe(bool * allowNetworkSessionRecovery);

void get_lorawaninit_hweui(char * p_str);
uint16_t get_lorawaninit_fw_version(void);

#endif /* PERIPHERAL_LORAWAN_LORAWANINIT_H_ */
