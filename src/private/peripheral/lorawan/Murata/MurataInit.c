/*
==========================================================================
 Name        : MurataInit.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/MurataInit.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 10 Oct 2019
 Description : 
==========================================================================
*/


/********************************************************************************
 * Include files
 ****************************************************************************/
#include "stdint.h"
#include "stdbool.h"
#include "string.h"
#include "config_lorawan.h"
#include "config_network.h"
#include "common.h"
#include "debug.h"
#include "Murata.h"
#include "MurataInit.h"
#include "MurataInit_impl.h"
#include "productionMonitor.h"

/********************************************************************************
 * Private functions
 ****************************************************************************/

/********************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define SETUP_LIST_ITEMS 5

static setupCommandString moduleInitSetupCommandList[] ={
        { "AT+APPKEY=", APP_KEY },
        { "AT+SLEEP=", "0" }, // Sleep Mode Disabled
        { "AT+DFORMAT=", "1" }, // Payload format is hex
        { "AT+MODE=", "1" }, // OTAA
        { "AT+CLASS=", "2" } // CLASS C
};
static setupCommandString moduleInitSetupCommandListUnderTest[] ={
        { "AT+APPKEY=", PROD_APP_KEY },
        { "AT+SLEEP=", "0" }, // Sleep Mode Disabled
        { "AT+DFORMAT=", "1" }, // Payload format is hex
        { "AT+MODE=", "1" }, // OTAA
        { "AT+CLASS=", "2" } // CLASS C
};

static uint16_t module_fw_version = 0;
static char hwEui[NWK_HW_EUI_LENGTH + 1] = {0};
static bool devEuiReadyForReading = false;
/*****************************************************************************
 * Public types/enumerations/variables
 *******************************************************************************/

/******************************************************************************
 * Public functions
 *******************************************************************************/

/*******************************************************************************
 * 
 *******************************************************************************/
uint16_t MutataInit_GetFwVersion(void)
{
    return module_fw_version;
}

void MurataInit_getDevEUi(char * p_devEuiString)
{
    if(devEuiReadyForReading)
    {
        strncpy(p_devEuiString, hwEui,sizeof hwEui);
    }
    return;
}

/*******************************************************************************
 * 
 *******************************************************************************/
bool MurataInit_Run(uint8_t freqband, bool * allowNetworkSessionRecovery)
{
    uint8_t currentFreqBand = 0;
    char firmwareVersionString[40] = {0};
    uint32_t upCounter = 0;

    resetModule();
    activateComs();

    catch_test_fail( softReboot() == false);
    catch_test_fail( fetchModuleCurrentFreqBand(&currentFreqBand) == false );

    upCounter = fetchUpFrameCounter();
    debug_printf(LORAINIT, "Up = %d \r\n", upCounter);

    if( (*allowNetworkSessionRecovery) && (upCounter > LORAWAN_RESTORE_SESSION_UPLINK_COUNT_THRESHOLD))
    {
        debug_printf(LORAINIT, "Restore Session \r\n");
        return true;
    }
    else
    {
        // Indicate that session was not restored > join will be required.
        *allowNetworkSessionRecovery = false;
    }

    catch_test_fail( updateModuleFrequencyBand( currentFreqBand, freqband) == false );
    catch_test_fail( waitForModuleToBeReady() == false );
    catch_test_fail( getFwVersion(firmwareVersionString) == false );
    module_fw_version =  parseFwVersion(firmwareVersionString);

    if(freqband == _AS923)
    {
        catch_test_fail( applyDwellTime() == false );
    }

    readBackDevEui(hwEui, NWK_HW_EUI_LENGTH);
    devEuiReadyForReading = true;
    debug_printf( LORAINIT, "LORA DEVEUI = %s\r\n", hwEui);

    if(productionMonitor_getUnitIsUndertest() == false)
    {
        catch_test_fail( false == setupModule(moduleInitSetupCommandList, SETUP_LIST_ITEMS) );
    }
    else
    {
        catch_test_fail( false == setupModule(moduleInitSetupCommandListUnderTest, SETUP_LIST_ITEMS) );
    }

    return true;
}


/************************************* EOF *************************************/
