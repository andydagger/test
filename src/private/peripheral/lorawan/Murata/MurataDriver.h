#ifndef MURATA_DRIVER_H
#define MURATA_DRIVER_H

#include "stdint.h"
#include "stdbool.h"

bool MurataDriver_getFullStringReceived();
void MurataDriver_checkForStringEnd();
bool MurataDriver_sendString(char *p_msg);
void MurataDriver_txIsr();
void MurataDriver_rxIsr();
char * MurataDriver_getReceivedStringBuffer();
void MurataDriver_resetAndEnableDownlinkReceiver();


#endif
