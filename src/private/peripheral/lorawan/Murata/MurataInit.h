/*
==========================================================================
 Name        : MurataInit.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/MurataInit.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 10 Oct 2019
 Description : 
==========================================================================
*/

#ifndef PRIVATE_PERIPHERAL_LORAWAN_MURATAINIT_H_
#define PRIVATE_PERIPHERAL_LORAWAN_MURATAINIT_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "stdbool.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/


/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
bool MurataInit_Run(uint8_t freqband, bool * allowNetworkSessionRecovery);
uint16_t MutataInit_GetFwVersion(void);
void MurataInit_getDevEUi(char * p_devEuiString);
#endif /* PRIVATE_PERIPHERAL_LORAWAN_MURATAINIT_H_ */
