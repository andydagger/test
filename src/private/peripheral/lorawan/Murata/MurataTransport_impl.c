/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "MurataTransport_impl.h"
#include "MurataTransport.h"
#include "config_network.h"
#include "string.h"
#include "common.h"
#include "debug.h"
/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

#define MAX_PAYLOAD_LENGTH 51

typedef struct{
    murataResponseCode rx_resp;
    char *string_content;
    uint8_t string_length;
}listtype_resp_list;

static const listtype_resp_list resp_list[] =
{
        // { NO_RESP, "" , 0 },
        { RESP_OK,               "+OK", 3 },         // Success
        //---------------------------------------------------------------------
        // +ERR=?
        { INVALID_CMD,    "+ERR=-1\r", 8 },     // Command is unknown
        { INVALID_CMD,    "+ERR=-2\r", 8 },     // The number of parameter is invalid.
        { INVALID_CMD,    "+ERR=-3\r", 8 },     // The content of parameter is invalid.
                                                // Failed to restore to factory new state.
        { NOT_CONNECTED,       "+ERR=-5\r", 8 },     // Device is not in LoRaWAN network yet
                                                // Device is already in LoRaW AN network.
        { ISBUSY,             "+ERR=-7\r", 8 },     // LoRa MAC is busy in transmission.
                                                // The version of the firmware to be updated is the same with the current one.
                                                // Firmware information is not set.
                                                // Error occurred when write/read flash memory.
                                                // Failed to update firmware.
        { INVALID_CMD,    "+ERR=-12\r", 9 },    // The length of payload exceeds maximum value.
        { INVALID_CMD,    "+ERR=-13\r", 9 },    // Command only supported on ABP mode.
        { INVALID_CMD,    "+ERR=-14\r", 9 },    // Command only supported on OTAA mode.
        { INVALID_CMD,    "+ERR=-15\r", 9 },    // The band configuration is not supported by the device.
        { INVALID_CMD,    "+ERR=-16\r", 9 },    // Power value exceeds the range allowed by the power mode.
        { INVALID_CMD,    "+ERR=-17\r", 9 },    // Command is unusable under current band configuration.
        { NO_DUTY_CYCLE,       "+ERR=-18\r", 9 },    // TX is not allowed due to duty cycle limits.
        { NO_DUTY_CYCLE,       "+ERR=-19\r", 9 },    // No channel is available for TX due to LBT limits or error parameters.
                                                // Multiple LinkCheckReq command duplication in queue.
        //---------------------------------------------------------------------
        // EVENT=??
        { REBOOT_OK,        "+EVENT=0,0\r", 11}, // Module reboots successfully.
        // Below set as NO_RESP so as not to act on state machine in TL
        { RESET_OK,        "+EVENT=0,1\r", 11}, // Module restores to factory new successfully.
                                                // Module enters bootloader mode
        { JOIN_REJECTED,   "+EVENT=1,0\r", 11},    // Module doesn't join LoRa network.
        { JOIN_ACCEPT,         "+EVENT=1,1\r", 11},    // Module joins LoRa network successfully.
                                                // The link between modem and gateway is lost.
                                                // The link between modem and gateway is connected.
        { CNF_RETRY,             "+EVENT=2,2\r", 11}, // The modem doesn’t receive ACK for confirmed uplink message And will retransmission.
        //---------------------------------------------------------------------
        // OTHER
        { ACK,        "+ACK\r", 5 },        // This message is used to indicate the ACK message from concentrator.
        { NOACK,          "+NOACK\r", 7 },       // This message is used to indicate that modem doesn’t receive ACK message from concentrator.
        { RECV_MSG,     "+RECV=",   6}  // Received downlink payload

};

/*******************************************************************************
    ...
*******************************************************************************/
murataResponseCode decodeReceivedString(char * p_receivedString)
{
    murataResponseCode responeCode = CORRUPT_RESP;
    murataResponseCode string_id = NO_RESP;

    debug_printf( LORAMAC, "Decode string = %s\r\n", p_receivedString);


    if( p_receivedString == NULL) return PTR_ERR;

    if(strlen(p_receivedString) == 0)
    {
        return NO_RESP;
    }
    
    for(string_id = NO_RESP; string_id < matrix_n_rows(resp_list); string_id++){
        if( strncmp( resp_list[string_id].string_content, p_receivedString, resp_list[string_id].string_length) == 0){
            responeCode = resp_list[string_id].rx_resp;
            break;
        }else{
            responeCode = CORRUPT_RESP;
        }
    }
    debug_printf( LORAMAC, "Resp Code = %d\r\n", responeCode);

    return responeCode;
}

/*******************************************************************************
    ...
*******************************************************************************/
bool extractReceivedDownlinkPayload(char * p_receivedString, char * p_downlinkPayload)
{
    uint8_t payload_bytes_to_receive = 0;
    char * p_to_rx_buffer = p_receivedString;

    if( p_receivedString == NULL) return false;
    if( p_downlinkPayload == NULL) return false;

    // point to first comma
    p_to_rx_buffer = strchr( p_to_rx_buffer, ',');

    // return if ',' cannot be found
    if( p_to_rx_buffer == NULL) return false;

    // point to first char after comma
    p_to_rx_buffer++;

    // convert from ascii to decimal the number of payload bytes
    // +RECV=<port>,<length>\r\n\r\n
    payload_bytes_to_receive = *p_to_rx_buffer - 0x30;
    p_to_rx_buffer++;
    // Length > 9
    if('\r' != *p_to_rx_buffer ){
        payload_bytes_to_receive = payload_bytes_to_receive * 10;
        payload_bytes_to_receive += *p_to_rx_buffer - 0x30;
        p_to_rx_buffer++;
    }

    //--------------------------------------------------
    // ASCII encoded as string still at this stage.
    payload_bytes_to_receive *= 2;
    //--------------------------------------------------

    // Reject if payload too long - length x2 due to sring format
    if( payload_bytes_to_receive > (2*DOWN_MSG_PAYLOAD)) return false;
    payload_bytes_to_receive++; // Add one NULL char

    // Copy payload across
    p_to_rx_buffer += 4; // Skip \r\n\r\n
    strncpy(p_downlinkPayload, p_to_rx_buffer, payload_bytes_to_receive);

    return true;
}
