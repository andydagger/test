/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "MurataTransport_stateMachine.h"
#include "MurataTransport_impl.h"
#include "MurataDriver.h"
#include "systick.h"
#include "FreeRTOSCommonHooks.h"
#include "debug.h"
#include "config_lorawan.h"

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define RESEND_DELAY_MS 5000
#define MAX_RESEND_CMD_ATTEMPTS 5

static uint8_t sendCmdAttempts = 0;
/*******************************************************************************
    ...
*******************************************************************************/
murataTransportStates serviceMurataTransportStateMachine(   murataTransportStates currentState, 
                                                            murataResponseCode driverCode,
                                                            bool dualResponseExpected,
                                                            char * p_cmdString,
                                                            cmdResult * p_cmdSuccess)
{

    switch(currentState)
    {
        //---------------------------------------------------------------------
        case TL_IDLE:
            sendCmdAttempts = 0;
            return TL_SEND;
        break;

        case TL_SEND:
            sendCmdAttempts++;
            if(sendCmdAttempts > MAX_RESEND_CMD_ATTEMPTS){
                *p_cmdSuccess = MOD_NOT_RESP;
                return TL_IDLE;
            }
        	debug_printf( LORAMAC, "Send Cmd: %s\n", p_cmdString);
            // TODO: Add logic for return value false
            MurataDriver_sendString(p_cmdString);
            return TL_WAIT_FOR_INITIAL_RESP;
        break;

        //---------------------------------------------------------------------
        case TL_WAIT_FOR_INITIAL_RESP:
            if( driverCode == RESP_OK )
            {
                if(dualResponseExpected == true)
                {
                	MurataDriver_resetAndEnableDownlinkReceiver();
                    return TL_WAIT_FOR_SECOND_RESP;
                }
                else
                {
                	*p_cmdSuccess = CMD_SUCCESSFUL;
                    return TL_IDLE;
                }
            }
            else if( driverCode == INVALID_CMD )
			{
				return TL_SEND;
			}
            else if( driverCode == NOT_CONNECTED )
            {
                *p_cmdSuccess = LOST_CONNECTION;
                return TL_IDLE;
            }
            else if( (driverCode == NO_DUTY_CYCLE) || ( driverCode == ISBUSY ))
            {
            	MurataDriver_resetAndEnableDownlinkReceiver();
                return TL_RESEND_DELAY;
            }
            else if( driverCode == CORRUPT_RESP )
            {
            	return TL_SEND;
            }
            else
            {
            	return TL_WAIT_FOR_INITIAL_RESP;
            }
        break;

        //---------------------------------------------------------------------
        case TL_WAIT_FOR_SECOND_RESP:
            if( 	(driverCode == ACK) ||
            		(driverCode == JOIN_ACCEPT) ||
					(driverCode == RESET_OK) ||
					(driverCode == REBOOT_OK))
            {
                *p_cmdSuccess = CMD_SUCCESSFUL;
                return TL_IDLE;
            }
            else if( driverCode == CNF_RETRY )
            {
                //--------------------------------------------
                // Renable rx and carry on waiting.
                MurataDriver_resetAndEnableDownlinkReceiver();
                return TL_WAIT_FOR_SECOND_RESP;
                //--------------------------------------------
            }
            else if( driverCode == NOACK )
            {
                *p_cmdSuccess = POOR_LINK;
                return TL_IDLE;
            }
            else if( driverCode == JOIN_REJECTED )
            {
                *p_cmdSuccess = CMD_FAILED;
                return TL_IDLE;
            }
            else if( driverCode == CORRUPT_RESP )
            {
            	MurataDriver_resetAndEnableDownlinkReceiver();
            	return TL_WAIT_FOR_SECOND_RESP;
            }
            else
            {
            	return TL_WAIT_FOR_SECOND_RESP;
            }
        break;

        //---------------------------------------------------------------------
        case TL_RESEND_DELAY:
            FreeRTOSDelay(RESEND_DELAY_MS);
            return TL_SEND;
        break;

        //---------------------------------------------------------------------
        default:
        	return TL_IDLE;
        break;
    }

}

/*******************************************************************************
    ...
*******************************************************************************/

bool serviceStateMachineTimeOut(	murataTransportStates * currentState,
                                    uint32_t * startOfCommand)
{

	if(*currentState == TL_WAIT_FOR_INITIAL_RESP)
	{
		if( (get_systick_ms() - *startOfCommand ) > INITIAL_RESP_TO_MS)
		{
			debug_printf( LORAMAC, "TO Initial Resp\r\n");
			*currentState = TL_SEND;
            //--------------------------------------------------
            // Reset reference time for retry
            *startOfCommand = get_systick_ms();
            //--------------------------------------------------
			return true;
		}
	}
	else if(*currentState == TL_WAIT_FOR_SECOND_RESP)
	{
		if( (get_systick_ms() - *startOfCommand ) > SECOND_RESP_TO_MS)
		{
			debug_printf( LORAMAC, "TO Second Resp\r\n");
			*currentState = TL_IDLE;
			return true;
		}
    }
	else if(*currentState == TL_SEND)
	{
		*startOfCommand = get_systick_ms();
		return false;
	}
return false;
}

/*******************************************************************************
    ...
*******************************************************************************/
bool checkForOverAllSendOpTimeOut(uint32_t transferReferenceTimeMs)
{
    uint32_t elapsedTimeSinceStartSec =  (get_systick_ms() - transferReferenceTimeMs) / 1000;
    return ( elapsedTimeSinceStartSec > LORAWAN_SEND_CMD_TIMEOUT_SEC) ? true : false;
}
