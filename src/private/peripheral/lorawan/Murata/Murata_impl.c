/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "Murata_impl.h"
#include "Murata.h"
#include "config_network.h"
#include "MurataTransport.h"
#include "stdio.h"

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static uint8_t prevJoinDR = 99;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/


/*******************************************************************************
 *
 *******************************************************************************/
bool joinDutyCycleHandler(  uint8_t * joinDR, 
                            uint8_t * attemptsAtCurrentDr, 
                            uint8_t freqBand){

    // First call shall start with DR5 and return true                            
    if( prevJoinDR == 99)
    {
        *joinDR = 5;
    }

    if( freqBand != _AS923)
    {
        // Every JOIN_REQ_AT_SAME_DR_LIMIT attempts at same DR:
        if((*attemptsAtCurrentDr)++ > (uint8_t)JOIN_REQ_AT_SAME_DR_LIMIT )
        {
            *attemptsAtCurrentDr = 1;
            // lower data-rate, down to 0 and the back up to 5
            if( *joinDR > 0)
            {
                (*joinDR)--;
            }
            else
            {
                *joinDR = 5;
            }
            
        }
    }
    else
    {
        // AS923 JR always on DR2
        *joinDR = 2;
    }

    // check if module needs dr adjusting
    if( *joinDR != prevJoinDR)
    {
        prevJoinDR = *joinDR;
        return true;
    }
    else
    {
        return false;
    }    
}


/*******************************************************************************
 *
 *******************************************************************************/
 void updateDataRate(uint8_t newDataRate)
 {
    char set_dr_str[15] = {0};
    sprintf(set_dr_str, "AT+DR=%d\r", newDataRate);
    MurataTransport_sendCommand(set_dr_str, false, NULL, 0);
    return;
 }

/*******************************************************************************
 *
 *******************************************************************************/
void bandSpecificPostJoinRoutine(uint8_t freqBand)
{
    if( freqBand == _AS923 )
    {
        /*
            Send mock uplink to enable Dwell-time disabling sequence
            betweem NS and LoRa module.
            This shall be the first uplink sent.
        */
        do
        {
            // Make sure Dwell disable uplink is sent with DR2
            MurataTransport_sendCommand("AT+DR=2\r", false, NULL, 0);
        }
        while( MurataTransport_sendCommand("AABBCCDD", true, NULL, 0) == false);
    }
    return;
}

/*******************************************************************************
 *
 *******************************************************************************/
uint8_t getDataRate(void)
{
    // AT+DR?\r
    // +OK=0\r\n\r\n
    // resp 0 to 5
    char responseBuffer[1] = {0};
    if(MurataTransport_sendCommand("AT+DR?\r", false, responseBuffer, 1) == CMD_SUCCESSFUL)
    {
        return (uint8_t)(responseBuffer[0] - 0x30 );
    }
    else
    {
        return 99;
    }
}

uint8_t getPowerIndex(void)
{
    // AT+RFPOWER?\r
    // +OK=0,1\r\n\r\n
    // resp 0 to 7
    char responseBuffer[3] = {0};
    if(MurataTransport_sendCommand("AT+RFPOWER?\r", false, responseBuffer, 3) == CMD_SUCCESSFUL)
    {
        return (uint8_t)(responseBuffer[2] - 0x30 );
    }
    else
    {
        return 99;
    }
}
