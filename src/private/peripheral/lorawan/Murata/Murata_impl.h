#ifndef MURATA_IMPL_H
#define MURATA_IMPL_H

#include "stdint.h"
#include "stdbool.h"

bool joinDutyCycleHandler(  uint8_t * joinDR, 
                            uint8_t * attemptsAtCurrentDr, 
                            uint8_t freqBand);

void updateDataRate(uint8_t newDataRate);
void bandSpecificPostJoinRoutine(uint8_t freqBand);

uint8_t getDataRate(void);
uint8_t getPowerIndex(void);

#endif