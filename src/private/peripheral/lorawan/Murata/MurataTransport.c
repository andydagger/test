/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "MurataTransport.h"
#include "MurataTransport_impl.h"
#include "MurataTransport_stateMachine.h"
#include "MurataDriver.h"
#include "config_lorawan.h"
#include "systick.h"
#include "string.h"
#include "debug.h"
/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define MAX_RESP_LENGTH_BYTES NETWORK_PACKET_LENGTH

static murataTransportStates murataTransportCurrentState = TL_IDLE;

/*******************************************************************************
    ...
*******************************************************************************/
cmdResult MurataTransport_sendCommand(   char * p_command, 
                                    bool secondResponseIsExpected,
                                    char * p_responseData ,
                                    uint8_t respLengthBytes)
{

    if( p_command == NULL) return CMD_FAILED;
    if( respLengthBytes > DOWN_MSG_PAYLOAD) return CMD_FAILED;

    uint32_t transferReferenceTimeMs = get_systick_ms();
    uint32_t transferStartTimeMs = transferReferenceTimeMs;
    murataResponseCode responseCode = NO_RESP;
    cmdResult cmdRes = CMD_FAILED;
    char * p_resp;

    murataTransportCurrentState = TL_IDLE;   

    do{

		if(MurataDriver_getFullStringReceived() == true)
		{
            p_resp = MurataDriver_getReceivedStringBuffer();
            responseCode = decodeReceivedString( p_resp );
		}

        murataTransportCurrentState =   serviceMurataTransportStateMachine(
                                            murataTransportCurrentState,
                                            responseCode,
                                            secondResponseIsExpected,
                                            p_command,
                                            &cmdRes
                                            );
        responseCode = NO_RESP;
        
        if( true == serviceStateMachineTimeOut(	&murataTransportCurrentState,
        										&transferReferenceTimeMs))
        {
        	cmdRes = CMD_FAILED;
        }

        if(checkForOverAllSendOpTimeOut(transferStartTimeMs) == true)
        {
            murataTransportCurrentState = TL_IDLE; 
            cmdRes = MOD_NOT_RESP;
        }
        
    }while( murataTransportCurrentState != TL_IDLE);

    // Was transfer successful
    if( cmdRes == CMD_SUCCESSFUL )
    {
        // Was a response expected
        if( p_responseData != NULL )
        {
            strncpy(p_responseData, &p_resp[4], respLengthBytes);
            // p_responseData[respLengthBytes] = 0;
        }
    }

    MurataDriver_resetAndEnableDownlinkReceiver();

    return cmdRes;
}                                    

/*******************************************************************************
    ...
*******************************************************************************/
bool MurataTransport_getDownlinkPayloadIsWaiting()
{
    return MurataDriver_getFullStringReceived();
}

/*******************************************************************************
    ...
*******************************************************************************/
bool MurataTransport_getDownlinkPayload(char * callerBuffer)
{
    bool res;
    char * rxPayloadDriverBuffer = MurataDriver_getReceivedStringBuffer();
    res = extractReceivedDownlinkPayload(rxPayloadDriverBuffer, callerBuffer);
    MurataDriver_resetAndEnableDownlinkReceiver();
    return res;
}