
/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "chip.h"
#include "stdbool.h"
#include "stdint.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "config_lorawan.h"
#include "common.h"
#include "systick.h"
#include "debug.h"
#include "sysmon.h"
#include "FreeRTOSCommonHooks.h"
#include "MurataInit.h"

#include "Murata.h"
#include "Murata_impl.h"
#include "MurataTransport.h"
#include "MurataDriver.h"

/*****************************************************************************
 * Private functions
 ****************************************************************************/
static void report_error(uint32_t error_code);

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define MCAST_RETRY_DELAY 2000
// used by tx and rx but only one at a time.
static char trx_buffer[UP_MSG_MICROCHIP_STRING_MAX];


/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
static uint32_t time_of_prev_join_request = 0;
static uint8_t join_dr = 5;
static uint8_t attempts_at_current_dr = 0;

/*******************************************************************************
 * 
 *******************************************************************************/
bool Murata_Init(uint8_t freqBand, bool * allowNetworkSessionRecovery)
{
    // Murata stack does not yet support session restore so flag always false
    catch_error_fail( MurataInit_Run(freqBand, allowNetworkSessionRecovery) );   
    // Initialise Variables
    time_of_prev_join_request = 0xFFFF;
    join_dr = 5;
    attempts_at_current_dr = 0;
    return true;
}

/*******************************************************************************
 * 
 *******************************************************************************/
bool Murata_JoinNetwork(uint8_t freqBand, bool restoringSession)
{
    uint32_t currentTimeSec = get_systick_sec();

    if(!restoringSession)
    {
        // Even with intelligent use of DR always leave
        // INTER_JOIN_REQ_DELAY_SEC between each request
        if( (currentTimeSec - time_of_prev_join_request) > INTER_JOIN_REQ_DELAY_SEC )
        {
            time_of_prev_join_request = currentTimeSec;
        }else{
            return false;
        }

        // Join-Request DR optimisation
        if( joinDutyCycleHandler(&join_dr, &attempts_at_current_dr, freqBand) == true)
        {
            updateDataRate(join_dr);
        }

        if( MurataTransport_sendCommand("AT+JOIN\r", true, NULL, 0) != CMD_SUCCESSFUL)
        {
            return false;
        }
    }

    bandSpecificPostJoinRoutine(freqBand);

    return true;

}

/*******************************************************************************
    ...
*******************************************************************************/
bool Murata_getDownlinkPayloadIsWaiting()
{
    return MurataTransport_getDownlinkPayloadIsWaiting();
}

/*******************************************************************************
    ...
*******************************************************************************/
static void eraseTrxBuffer()
{
    uint8_t i = 0;
    for(i = 0 ; i < (UP_MSG_MICROCHIP_STRING_MAX - 1); i++){
        trx_buffer[i] = 0;
    }
    return;    
}

/*******************************************************************************
    ...
 *******************************************************************************/
char * Murata_ReadMsg(void){

    eraseTrxBuffer();

    // if payload valid return ptr to local buffer
    if( true == MurataTransport_getDownlinkPayload(trx_buffer) ){
        return trx_buffer;
    }else{
        report_error(MSG_RX_INVALID);
        return NULL;
    }
}


/*******************************************************************************
 * 
 *******************************************************************************/
bool Murata_UpdateMcastState(bool mcastReqState){
    uint8_t retry = 0;

    if( true == mcastReqState ){
        sprintf(trx_buffer, "AT+MCAST=%s\r", MCAST_INFO_ON);
        // Before initiating a new ncast session: reset dlc
    }else{
        sprintf(trx_buffer, "AT+MCAST=%s\r", MCAST_INFO_OFF);
    }

    while(MurataTransport_sendCommand(trx_buffer, false, NULL, 0) != CMD_SUCCESSFUL)
    {
        FreeRTOSDelay(MCAST_RETRY_DELAY);
        if(++retry > 4) return false;
    }

    return true;
}

/*******************************************************************************
 *
 *******************************************************************************/
cmdResult Murata_SendMsg(char * p_msg_to_send, bool ack_is_required){
    uint16_t str_len = strlen(p_msg_to_send)/2;

    // null pointer fail-safe
    if( NULL == p_msg_to_send)
    {
        return CMD_FAILED;
    }

    // Uplink message length fail-safe
    if( str_len > UP_MSG_PAYLOAD)
    {
        return CMD_FAILED;
    }

    if( true == ack_is_required)
    {
        // Termination \r\n just for debugging
        sprintf(    trx_buffer, "AT+CTX %d\r%s",
                    str_len,
                    p_msg_to_send);
        // For confirmed: OK then ACK or NOACK
        return MurataTransport_sendCommand(trx_buffer, true, NULL, 0);
    }else
    {
        // Termination \r\n just for debugging
        sprintf(    trx_buffer, "AT+UTX %d\r%s",
                    str_len,
                    p_msg_to_send);
        // For unconfirmed only OK response
        return MurataTransport_sendCommand(trx_buffer, false, NULL, 0);
    }
}

/*******************************************************************************
 *
 *******************************************************************************/
void Murata_SaveSessionInfo(void){
    debug_printf( LORA, "Save session info - Not Available!\r\n");
    return;
}

/*******************************************************************************
 * 
 *******************************************************************************/
void Murata_FullHwReset(void){
    debug_printf( LORA, "Reset Module.\r\n");
    // Expects OK then EVENT=0,1 >> MAC_TX_OK
    while(false == MurataTransport_sendCommand("AT+FACNEW\r", true, NULL, 0));
    return;
}

/*******************************************************************************
 * 
 *******************************************************************************/
uint16_t Murata_GetModuleFwVersion(void){
    return MutataInit_GetFwVersion();
}

/*******************************************************************************
 * 
 *******************************************************************************/
bool Murata_SetBestSettings()
{
    uint32_t data_rate = 0;
    uint32_t power_index = 0;
    // If DR = 0 and PWR_IDX = 1 already
    // force re-join
    // else force optimal parameters
    data_rate = getDataRate();
    power_index = getPowerIndex();
    if( ( 0 == data_rate ) && ( 1 == power_index ) ){
        // reset hw module
    	return false;
    }else{
        MurataTransport_sendCommand("AT+DR=0\r", false, NULL, 0);
        MurataTransport_sendCommand("AT+RFPOWER=0,1\r", false, NULL, 0);
    }
    return true;    
}


/*******************************************************************************
 * 
 *******************************************************************************/
static void report_error(uint32_t error_code)
{
    sysmon_report_error(LORA, error_code);
    return;
}

void Murata_checkForStringEnd()
{
    MurataDriver_checkForStringEnd();
    return;
}

void Murata_getDevEUi(char * p_devEuiString)
{
    MurataInit_getDevEUi(p_devEuiString);
    return;
}


/***************************** EOF ******************************************/
