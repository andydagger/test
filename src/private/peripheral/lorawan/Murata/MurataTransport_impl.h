
#ifndef MURATA_TRANSPORT_IMPL_H
#define MURATA_TRANSPORT_IMPL_H

#include "stdint.h"
#include "stdbool.h"

typedef enum{
    NO_RESP,
    RESP_OK,
    INVALID_CMD,
    NO_DUTY_CYCLE,
    ISBUSY,
    JOIN_REJECTED,
    JOIN_ACCEPT,
    NOT_CONNECTED,
    ACK,
    NOACK,
    REBOOT_OK,
    RESET_OK,
    CORRUPT_RESP,
    CNF_RETRY,
    RECV_MSG,
    PTR_ERR
}murataResponseCode;


murataResponseCode decodeReceivedString(char * p_receivedString);
bool extractReceivedDownlinkPayload(char * p_receivedString, char * p_downlinkPayload);

#endif