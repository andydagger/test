
/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "loraDriver.h"
#include "hw.h"
// #include "stdint.h"
// #include "nwk_tek.h"
#include "MurataDriver.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

void loraDriver_trx_isr_murata()
{
    uint32_t intsrc = 0;

    // Read out irq sources
    intsrc = loraDriver_GetIrqStatus();
    // work out if tx or rx isr should be called
    if( lorawan_was_rx_isr(intsrc)){
        MurataDriver_rxIsr();
    }else if(lorawan_was_tx_isr(intsrc)){
        MurataDriver_txIsr();
    }else{
        // NOOP: incorrect IRQ source
    }
}

void loraDriver_SetBaudrate(uint32_t baudRate)
{
    lorawan_set_baudrate(baudRate);
}

void loraDriver_TxByte(uint8_t c)
{
    lorawan_tx_byte(c);
}

uint32_t loraDriver_RxByte()
{
    return lorawan_rx_byte();
}

uint32_t loraDriver_GetIrqStatus()
{
    return lorawan_get_int_status();
}

void loraDriver_TxEnable()
{
    lorawan_tx_enable();
}

void loraDriver_TxDisable()
{
    lorawan_tx_disable();
}

void loraDriver_RxEnable()
{
    lorawan_rx_enable();
}

void loraDriver_RxDisable()
{
    lorawan_rx_disable();
}

void loraDriver_IrqEnable()
{
    lorawan_irq_enable();
}

void loraDriver_IrqDisable()
{
    lorawan_irq_disable();
}

void loraDriver_DisableModule()
{
    lorawan_disable_module();
}

void loraDriver_EnableModule()
{
    lorawan_enable_module();
}
