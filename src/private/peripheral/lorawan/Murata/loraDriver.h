
#ifndef PUBLIC_DRIVER_LORADRIVER_H_
#define PUBLIC_DRIVER_LORADRIVER_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "stdbool.h"
#include "stdint.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void loraDriver_SetBaudrate(uint32_t baudRate);
void loraDriver_TxByte(uint8_t c);
uint32_t loraDriver_RxByte();
uint32_t loraDriver_GetIrqStatus();
void loraDriver_TxEnable();
void loraDriver_TxDisable();
void loraDriver_RxEnable();
void loraDriver_RxDisable();
void loraDriver_IrqEnable();
void loraDriver_IrqDisable();
void loraDriver_DisableModule();
void loraDriver_EnableModule();
void loraDriver_trx_isr_murata();

#endif /* PUBLIC_DRIVER_LORADRIVER_H_ */
