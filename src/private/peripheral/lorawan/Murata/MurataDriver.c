/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "MurataDriver.h"
#include "config_network.h"
#include "loraDriver.h"
#include "string.h"
#include "debug.h"
/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
volatile static uint8_t charactersReceived = 0;
volatile static bool fullStringReceived = false;
static volatile uint8_t prevCharacterReceived = 0;
volatile static uint8_t charactersSent = 0;
volatile static char trxBuffer[UP_MSG_MICROCHIP_STRING_MAX];

/*******************************************************************************
    ...
*******************************************************************************/
static void eraseTrxBuffer()
{
    uint8_t i = 0;
    for(i = 0 ; i < (UP_MSG_MICROCHIP_STRING_MAX - 1); i++){
        trxBuffer[i] = 0;
    }
    return;    
}

/*******************************************************************************
    ...
*******************************************************************************/
bool MurataDriver_getFullStringReceived()
{
    return fullStringReceived;
}

/*******************************************************************************
    ...
*******************************************************************************/
void MurataDriver_checkForStringEnd()
{ 

	if(fullStringReceived == true)
	{
		return;
	}

    if(charactersReceived == 0)
    {
        fullStringReceived = false;
        return;
    }
    if(prevCharacterReceived == charactersReceived)
    {
        loraDriver_RxDisable();
        fullStringReceived = true;
    }
    else
    {
        prevCharacterReceived = charactersReceived;
        fullStringReceived = false;
    }
    return; 
}  

/*******************************************************************************
    ...
*******************************************************************************/
bool MurataDriver_sendString(char *p_msg)
{
    if( strlen(p_msg) > ( UP_MSG_MICROCHIP_STRING_MAX - 1) )
    {
        return false;
    }

    eraseTrxBuffer();
    strncpy( (char *)trxBuffer, p_msg, strlen(p_msg));
    loraDriver_RxDisable();
    charactersReceived = 0;
    prevCharacterReceived = 0;
    fullStringReceived = false;
    
    charactersSent = 0;
    loraDriver_TxEnable();

    return true;
}

/*******************************************************************************
    ...
*******************************************************************************/
void MurataDriver_txIsr()
{
    if( trxBuffer[charactersSent] == 0)
    {
        loraDriver_TxDisable();
        MurataDriver_resetAndEnableDownlinkReceiver();
        return;
    }
    loraDriver_TxByte(trxBuffer[charactersSent++]);
    return;
}

/*******************************************************************************
    ...
*******************************************************************************/
void MurataDriver_rxIsr()
{
    trxBuffer[charactersReceived++] = loraDriver_RxByte();
    if( charactersReceived >= DOWN_MSG_MICROCHIP_STRING_MAX)
    {
        loraDriver_RxDisable();
    }
    return;
}

/*******************************************************************************
    ...
*******************************************************************************/
char * MurataDriver_getReceivedStringBuffer()
{
    return (char *)trxBuffer;
}

/*******************************************************************************
    ...
*******************************************************************************/
void MurataDriver_resetAndEnableDownlinkReceiver()
{
    charactersReceived = 0;
    prevCharacterReceived = 0;
    fullStringReceived = false;
    eraseTrxBuffer();
    loraDriver_RxByte();
    loraDriver_RxEnable();
    return;
}

