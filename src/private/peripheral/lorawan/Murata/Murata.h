/*
==========================================================================
 Name        : Murata.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/lorawan/Murata.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 9 Oct 2019
 Description :
==========================================================================
*/

#ifndef PRIVATE_PERIPHERAL_LORAWAN_MURATA_H_
#define PRIVATE_PERIPHERAL_LORAWAN_MURATA_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "stdbool.h"
#include "MurataTransport.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
enum MURATA_BAND_LIST
{
    _AS923,
    _AU915,
    _RFU1,
    _RFU2,
    _RFU3,
    _EU868,
    _KR920,
    _IN865,
    _US915,
    _US915H // code only supports single digit
};

#define RESPONSE_LENGTH 50

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
bool Murata_Init(uint8_t freqBand, bool * allowNetworkSessionRecovery);
bool Murata_JoinNetwork(uint8_t freqBand, bool restoringSession);
bool Murata_getDownlinkPayloadIsWaiting();

char * Murata_ReadMsg();
cmdResult Murata_SendMsg(char * p_msg_to_send, bool ack_is_required);
bool Murata_UpdateMcastState(bool mcastReqState);
uint16_t Murata_GetModuleFwVersion();
void Murata_SaveSessionInfo();
void Murata_FullHwReset();
bool Murata_SetBestSettings();
void Murata_checkForStringEnd();
void Murata_getDevEUi(char * p_devEuiString);
#endif /* PRIVATE_PERIPHERAL_LORAWAN_MURATA_H_ */
