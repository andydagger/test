#ifndef MURATA_TRANSPORT_H
#define MURATA_TRANSPORT_H

#include "stdint.h"
#include "stdbool.h"

typedef enum{
    CMD_SUCCESSFUL,
    CMD_FAILED,
    POOR_LINK,
    LOST_CONNECTION,
    MOD_NOT_RESP
}cmdResult;

cmdResult MurataTransport_sendCommand(   char * p_command, 
                                    bool secondResponseIsExpected,
                                    char * p_responseData,
                                    uint8_t respLengthBytes );

bool MurataTransport_getDownlinkPayloadIsWaiting();
bool MurataTransport_getDownlinkPayload(char * callerBuffer);

#endif