
/*****************************************************************************
 * Include files
 ****************************************************************************/
#include "MurataInit_impl.h"
#include "common.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "Murata.h"
#include "config_network.h"
#include "loraDriver.h"
#include "FreeRTOS.h"
#include "FreeRTOSCommonHooks.h"
#include "MurataTransport.h"

/*****************************************************************************
 * Private functions
 ****************************************************************************/


/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define RESET_RETRIES_LIMIT 3
#define RESET_RETRY_DELAY_MS 2000   
#define READY_RETRIES_LIMIT 5
#define READY_RETRY_DELAY_MS 1000   
#define DWELL_RETRIES_LIMIT 3
#define DWELL_RETRY_DELAY_MS 1000   
#define FW_VERSION_STRING_LENGTH 6

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*******************************************************************************
 *
 *******************************************************************************/
bool getFwVersion(char * respString)
{
    if( MurataTransport_sendCommand("AT+VER?\r", 
                                    false, 
                                    respString, 
                                    FW_VERSION_STRING_LENGTH) != CMD_SUCCESSFUL)
        return false;
    else
        return true;
}

/*******************************************************************************
 *
 *******************************************************************************/
uint16_t parseFwVersion(char * respString)
{
    uint16_t module_fw_version = 0;
    
    // Expected Format
    // 1.1.03,Dec 20 2018 15:00:56

    uint8_t va = *respString - 0x30;
    respString += 2;
    uint8_t vb = *respString - 0x30;
    respString += 2;
    uint8_t vc = (uint8_t)(respString[0] - 0x30)*10 + (uint8_t)(respString[1] - 0x30);

    if(va > 9) return 0;
    if(vb > 9) return 0;
    if(vc > 99) return 0;

    module_fw_version = va*10 + vb;
    module_fw_version = module_fw_version << 8;
    module_fw_version = module_fw_version + vc;

    return module_fw_version;
}

/*******************************************************************************
 *
 *******************************************************************************/
void resetModule()
{
    loraDriver_DisableModule();
    FreeRTOSDelay(500);
    loraDriver_EnableModule();
    FreeRTOSDelay(1000);
    return;
}

/*******************************************************************************
 *
 *******************************************************************************/
void activateComs()
{
    loraDriver_SetBaudrate(19200);
    loraDriver_IrqEnable();
    loraDriver_RxDisable();
    return;
}

/*******************************************************************************
 * 
 *******************************************************************************/
bool setupModule(setupCommandString * setupCmdList, uint8_t numberOfSettings)
{
    char current_setup_str[UP_MSG_MICROCHIP_STRING_MAX];
    uint8_t settingNumber = 0;

    do{
        sprintf(    current_setup_str, "%s%s\r",
                    setupCmdList[settingNumber].commandString,
                    setupCmdList[settingNumber].valueString);
        if( MurataTransport_sendCommand(current_setup_str, false, NULL, 0) != CMD_SUCCESSFUL)
        {
            return false;
        }            
    }while( ++settingNumber < numberOfSettings );

    return true;
}

/*******************************************************************************
 * 
 *******************************************************************************/
bool softReboot()
{
    uint8_t retry = 0;
    while(MurataTransport_sendCommand("AT+REBOOT\r", true, NULL, 0) != CMD_SUCCESSFUL)
    {
        FreeRTOSDelay(RESET_RETRY_DELAY_MS);
        if(++retry >= RESET_RETRIES_LIMIT) return false;
    }
    return true;
}

/*******************************************************************************
 * 
 *******************************************************************************/
bool fetchModuleCurrentFreqBand(uint8_t * currentFreqBand)
{
    char bandResp[15];
    if( MurataTransport_sendCommand("AT+BAND?\r", false, bandResp, 1) != CMD_SUCCESSFUL)
        return false;
    else
    {
        *currentFreqBand = bandResp[0] - 0x30;
        return true;
    }
    return true;
}

/*******************************************************************************
 * 
 *******************************************************************************/
bool updateModuleFrequencyBand(uint8_t currentFreqBand, uint8_t requiredFreqband)
{
    if(currentFreqBand > _US915H) return false;
    if(requiredFreqband > _US915H) return false;
    if( currentFreqBand != requiredFreqband )
    {
        char bandCmd[15];
        sprintf(bandCmd, "AT+BAND=%d\r", requiredFreqband );
        if(MurataTransport_sendCommand(bandCmd, true, NULL, 0) != CMD_SUCCESSFUL)
            return false;
        else
            return true;
        
    }
    return true;
}

/*******************************************************************************
 * 
 *******************************************************************************/
bool waitForModuleToBeReady()
{
    uint8_t retry = 0;
    while( MurataTransport_sendCommand("AT\r", false, NULL, 0) != CMD_SUCCESSFUL)
    {
        FreeRTOSDelay(READY_RETRY_DELAY_MS);
        if(++retry >= READY_RETRIES_LIMIT) return false;
    }
    return true;
}

/*******************************************************************************
 * 
 *******************************************************************************/
bool applyDwellTime()
{
    uint8_t retry = 0;
    while(MurataTransport_sendCommand("AT+DWELL=1,1\r", false, NULL, 0) != CMD_SUCCESSFUL)
    {
        FreeRTOSDelay(DWELL_RETRY_DELAY_MS);
        if(++retry >= DWELL_RETRIES_LIMIT) return false;
    }
    return true;
}

/*******************************************************************************
 * 
 *******************************************************************************/
void readBackDevEui(char * respString, uint8_t length)
{

    MurataTransport_sendCommand("AT+DEVEUI?\r", false, respString, length);
    return;
}
/*******************************************************************************
 * 
 *******************************************************************************/
uint32_t fetchUpFrameCounter()
{
    char counterResp[22]={0};
    uint8_t x = 0;
    uint32_t counterValue = 0;

    if( MurataTransport_sendCommand("AT+FRMCNT?\r", false, counterResp, 21) == CMD_SUCCESSFUL)
        { 
            while(counterResp[x] != ',')
            {
                counterValue += (counterResp[x] - 0x30);
                x++;
                if(counterResp[x] != ',')
                {
                    counterValue = counterValue * 10;
                }
            }
        }
    return counterValue;
}
