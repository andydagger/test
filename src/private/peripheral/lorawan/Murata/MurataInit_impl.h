#ifndef MURATA_INIT_IMPL_H
#define MURATA_INIT_IMPL_H

#include "stdint.h"
#include "stdbool.h"

typedef struct
{
    const char * const commandString;
    const char * const valueString;
}setupCommandString;

bool getFwVersion(char * respString);
uint16_t parseFwVersion(char * respString);
void resetModule();
void activateComs();
bool setupModule(setupCommandString * setupCmdList, uint8_t numberOfSettings);
bool softReboot();
bool fetchModuleCurrentFreqBand(uint8_t * currentFreqBand);
bool updateModuleFrequencyBand(uint8_t currentFreqBand, uint8_t requiredFreqband);
bool waitForModuleToBeReady();
bool applyDwellTime();
void readBackDevEui(char * respString, uint8_t length);
uint32_t fetchUpFrameCounter();
#endif