#ifndef MURATA_TRANSPORT_STATE_MACHINE_H
#define MURATA_TRANSPORT_STATE_MACHINE_H


#include "stdint.h"
#include "stdbool.h"
#include "MurataTransport_impl.h"
#include "MurataTransport.h"

#define INITIAL_RESP_TO_MS  350
#define SECOND_RESP_TO_MS  (15*60*1000)

typedef enum{
    TL_IDLE,
    TL_SEND,
    TL_WAIT_FOR_INITIAL_RESP,
    TL_WAIT_FOR_SECOND_RESP,
    TL_RESEND_DELAY
}murataTransportStates;

murataTransportStates serviceMurataTransportStateMachine(   murataTransportStates currentState, 
                                                            murataResponseCode driverCode,
                                                            bool dualResponseExpected,
                                                            char * p_cmdString,
                                                            cmdResult * p_cmdSuccess);
bool serviceStateMachineTimeOut(	murataTransportStates * currentState,
													uint32_t * startOfCommand);
bool checkForOverAllSendOpTimeOut(uint32_t transferReferenceTimeMs);
#endif
