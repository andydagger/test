/*
 * LIS2DH12_config.h
 *
 *  Created on: 30 Nov 2018
 *      Author: robwoodhouse
 */

#ifndef LIS2DH12_CONFIG_H_
#define LIS2DH12_CONFIG_H_

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

// Plus 2K with some margin
#define LIS2DH12_CONFIG_XYZ_UPPER_BOUND                     2200

// Minus 2K with some margin
#define LIS2DH12_CONFIG_XYZ_LOWER_BOUND                     -2200
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#endif /* LIS2DH12_CONFIG_H_ */
