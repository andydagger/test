/*
 * LIS2DH12.c
 *
 *  Created on: 30 Nov 2018
 *      Author: robwoodhouse
 */

#include "LIS2DH12.h"
#include "LIS2DH12_config.h"
#include "chip.h"
#include "config_pinout.h"
#include "debug.h"
#include <stdio.h>
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#define _LIS2DH12_READ_SINGLE_NUM_BYTES                          7
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static void initPinMux();
static void initSPI();

static LIS2DH12_readStatus checkZeros(uint8_t* pData);
static LIS2DH12_readStatus checkRange(LIS2DH12_output* pOutput);

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
static SPI_DATA_SETUP_T XferSetup;

static uint8_t tx_data[32];
static uint8_t rx_data[32];
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void LIS2DH12_initSPI()
{
	initPinMux();
	initSPI();
}
//------------------------------------------------------------------------------
uint32_t LIS2DH12_readByteRegWait(const uint8_t regAddr, uint8_t* pData)
{
	uint8_t txData[2];
	uint8_t rxData[2];

	txData[0] = regAddr;
	txData[0] |= 0x80;

	// Setup Transfer structure, this data should be retained for the entire transmission
	XferSetup.pTx = txData;  // Transmit Buffer
	XferSetup.pRx = rxData; // Receive Buffer
	XferSetup.DataSize = 8; // Data format in bits
	XferSetup.Length = 2;  // Total frame length

	// Assert only SSEL0
	XferSetup.ssel =  SPI_FLASH_TXCTL_SSEL;
	XferSetup.TxCnt = 0;
	XferSetup.RxCnt = 0;

	uint32_t ret = Chip_SPI_RWFrames_Blocking(SPI_ACC_IF, &XferSetup);

	*pData = rxData[1];

	return ret;
}
//------------------------------------------------------------------------------
uint32_t LIS2DH12_writeByteRegWait(const uint8_t regAddr, const uint8_t data)
{
	uint8_t txData[2];
	uint8_t rxData[2];

	txData[0] = regAddr;
	txData[0] &= (~0x80);
	txData[0] |= 0x40;

	txData[1] = data;

	// Setup Transfer structure, this data should be retained for the entire transmission
	XferSetup.pTx = txData;  // Transmit Buffer
	XferSetup.pRx = rxData; // Receive Buffer
	XferSetup.DataSize = 8; // Data format in bits
	XferSetup.Length = 2;  // Total frame length

	// Assert only SSEL0
	XferSetup.ssel =  SPI_FLASH_TXCTL_SSEL;
	XferSetup.TxCnt = 0;
	XferSetup.RxCnt = 0;

	uint32_t ret = Chip_SPI_RWFrames_Blocking(SPI_ACC_IF, &XferSetup);

	return ret;
}
//------------------------------------------------------------------------------
LIS2DH12_readStatus LIS2DH12_readSingleOutputWait(LIS2DH12_output* pOutput)
{
  tx_data[0] = 0xe8;

  // Setup Transfer structure, this data should be retained for the entire transmission
  XferSetup.pTx = tx_data;  // Transmit Buffer
  XferSetup.pRx = rx_data; // Receive Buffer
  XferSetup.DataSize = 8; // Data format in bits
  XferSetup.Length = _LIS2DH12_READ_SINGLE_NUM_BYTES;  // Total frame length

  // Assert only SSEL0
  XferSetup.ssel =  SPI_FLASH_TXCTL_SSEL;
  XferSetup.TxCnt = 0;
  XferSetup.RxCnt = 0;

  int ret = Chip_SPI_RWFrames_Blocking(SPI_ACC_IF, &XferSetup);

  if(_LIS2DH12_READ_SINGLE_NUM_BYTES == ret)
  {
    // The correct number of bytes have been transferred - so continue
  }
  else
  {
    // The correct number of bytes have NOT been transferred
    // - so return now with appropriate return code.

    return LIS2DH12_READ_BTYE_COUNT_MISMATCH;
  }

  //--------------------------------------
  // Zeros check of raw data

  LIS2DH12_readStatus readStatus = checkZeros(&rx_data[1]);

  if(LIS2DH12_READ_OK == readStatus)
  {
    // 1 or more raw bytes is NOT zero - so continue
  }
  else
  {
    // All raw bytes are zero
    // - so return now with appropriate return code.
    return readStatus;
  }
  //--------------------------------------

  // Raw acceleration data is 12 bit left justified little endian,
  // i.e., least significant byte is at first (at lower index).
  pOutput->X = 0;
  pOutput->X = rx_data[1];
  pOutput->X |= (rx_data[2] << 8);
  pOutput->X = pOutput->X >> 4;

  pOutput->Y = 0;
  pOutput->Y = rx_data[3];
  pOutput->Y |= (rx_data[4] << 8);
  pOutput->Y = pOutput->Y >> 4;

  pOutput->Z = 0;
  pOutput->Z = rx_data[5];
  pOutput->Z |= (rx_data[6] << 8);
  pOutput->Z = pOutput->Z >> 4;
  //--------------------------------------

  // Range check
  readStatus = checkRange(pOutput);

  if(LIS2DH12_READ_OK == readStatus)
  {
    // All values are in valid range - so continue
  }
  else
  {
    // 1 or more values is out of range
    // - so return now with appropriate return code.
    return readStatus;
  }
  //--------------------------------------

  return LIS2DH12_READ_OK;
}

//------------------------------------------------------------------------------
void LIS2DH12_debugRegPrint()
{
	uint8_t regAddr;
	uint8_t regData;

	//------------------------
	debug_printf(1, "\r\n");

	regAddr = 0x0f;
	LIS2DH12_readByteRegWait(regAddr, &regData);
	debug_printf(1, "regData [0x%02x]: 0x%02x\r\n", regAddr, regData);
	//------------------------
	regAddr = 0x20;
	LIS2DH12_readByteRegWait(regAddr, &regData);
	debug_printf(1, "regData [0x%02x]: 0x%02x\r\n", regAddr, regData);
	//------------------------
	regAddr = 0x21;
	LIS2DH12_readByteRegWait(regAddr, &regData);
	debug_printf(1, "regData [0x%02x]: 0x%02x\r\n", regAddr, regData);
	//------------------------
	regAddr = 0x22;
	LIS2DH12_readByteRegWait(regAddr, &regData);
	debug_printf(1, "regData [0x%02x]: 0x%02x\r\n", regAddr, regData);
	//------------------------
	regAddr = 0x23;
	LIS2DH12_readByteRegWait(regAddr, &regData);
	debug_printf(1, "regData [0x%02x]: 0x%02x\r\n", regAddr, regData);
	//------------------------
	regAddr = 0x24;
	LIS2DH12_readByteRegWait(regAddr, &regData);
	debug_printf(1, "regData [0x%02x]: 0x%02x\r\n", regAddr, regData);
	//------------------------
	regAddr = 0x25;
	LIS2DH12_readByteRegWait(regAddr, &regData);
	debug_printf(1, "regData [0x%02x]: 0x%02x\r\n", regAddr, regData);
	//------------------------
	regAddr = 0x26;
	LIS2DH12_readByteRegWait(regAddr, &regData);
	debug_printf(1, "regData [0x%02x]: 0x%02x\r\n", regAddr, regData);
	//------------------------
	regAddr = 0x27;
	LIS2DH12_readByteRegWait(regAddr, &regData);
	debug_printf(1, "regData [0x%02x]: 0x%02x\r\n", regAddr, regData);
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

static void initPinMux()
{
	Chip_SWM_MovablePortPinAssign(GPIO_ACC_SPI_SDI_SWM, GPIO_ACC_SPI_SDI_PORT, GPIO_ACC_SPI_SDI_PIN);
	Chip_SWM_MovablePortPinAssign(GPIO_ACC_SPI_SDO_SWM, GPIO_ACC_SPI_SDO_PORT, GPIO_ACC_SPI_SDO_PIN);
	Chip_SWM_MovablePortPinAssign(GPIO_ACC_SPI_SPC_SWM, GPIO_ACC_SPI_SPC_PORT, GPIO_ACC_SPI_SPC_PIN);
	Chip_SWM_MovablePortPinAssign(GPIO_ACC_SPI_CS_SWM, GPIO_ACC_SPI_CS_PORT, GPIO_ACC_SPI_CS_PIN);
}
//------------------------------------------------------------------------------
static void initSPI()
{
	SPI_CFG_T			spiCfg;
	SPI_DELAY_CONFIG_T	spiDelayCfg;

	//
	// Disable the SPI before configuring it.
	//
	Chip_SPI_Disable(SPI_ACC_IF);

	//
	// Initialize SPI Block.
	//
	Chip_SPI_Init(SPI_ACC_IF);

	//
	// Set SPI Config register.
	//
	spiCfg.ClkDiv = 3;						// Set Clock divider to maximum
	spiCfg.Mode = SPI_MODE_MASTER;				// Enable Master Mode
	spiCfg.ClockMode = SPI_CLOCK_MODE0;			// Enable Mode 0
	spiCfg.DataOrder = SPI_DATA_MSB_FIRST;		// Transmit MSB first
	spiCfg.SSELPol = (SPI_CFG_SPOL0_LO | SPI_CFG_SPOL1_LO | SPI_CFG_SPOL2_LO | SPI_CFG_SPOL3_LO);	// Slave select polarity is active low
	Chip_SPI_SetConfig(SPI_ACC_IF, &spiCfg);

	//
	// Set Delay register.
	//
	spiDelayCfg.PreDelay = 2;
	spiDelayCfg.PostDelay = 2;
	spiDelayCfg.FrameDelay = 1;
	spiDelayCfg.TransferDelay = 2;
	Chip_SPI_DelayConfig(SPI_ACC_IF, &spiDelayCfg);


	//
	// Enable the SPI.
	//
	Chip_SPI_Enable(SPI_ACC_IF);
}

//------------------------------------------------------------------------------
static LIS2DH12_readStatus checkZeros(uint8_t* pData)
{
  uint8_t i;
  bool allZeros = true;

  for(i=0; i<6; i++)
  {
    if(0 != pData[i])
    {
      allZeros = false;
      break;
    }
  }

  if(true == allZeros)
  {
      // All raw data bytes are zero
    return LIS2DH12_READ_ALL_ZEROS;
  }
  else
  {
    return LIS2DH12_READ_OK;
  }
}
//------------------------------------------------------------------------------
static LIS2DH12_readStatus checkRange(LIS2DH12_output* pOutput)
{
  if(pOutput->X > LIS2DH12_CONFIG_XYZ_UPPER_BOUND)
  {
      return LIS2DH12_READ_OVERRANGE;
  }

  if(pOutput->X < LIS2DH12_CONFIG_XYZ_LOWER_BOUND)
  {
      return LIS2DH12_READ_OVERRANGE;
  }

  if(pOutput->Y > LIS2DH12_CONFIG_XYZ_UPPER_BOUND)
  {
      return LIS2DH12_READ_OVERRANGE;
  }

  if(pOutput->Y < LIS2DH12_CONFIG_XYZ_LOWER_BOUND)
  {
      return LIS2DH12_READ_OVERRANGE;
  }

  if(pOutput->Z > LIS2DH12_CONFIG_XYZ_UPPER_BOUND)
  {
      return LIS2DH12_READ_OVERRANGE;
  }

  if(pOutput->Z < LIS2DH12_CONFIG_XYZ_LOWER_BOUND)
  {
      return LIS2DH12_READ_OVERRANGE;
  }

  return LIS2DH12_READ_OK;
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void LIS2DH12_output_sprint(LIS2DH12_output* _this, char* str)
{
  uint8_t i = 0;

  i += sprintf(&str[i], "{%d,", _this->X);

  i += sprintf(&str[i], "%d,", _this->Y);

  i += sprintf(&str[i], "%d},", _this->Z);
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
