/*
 * IhalTilt.h
 *
 *  Created on: 30 Nov 2018
 *      Author: robwoodhouse
 *
 *  Hardware Abstraction Layer (HAL) for tilt interface implemented
 *  with LIS2DH12 Accelerometer IC.
 */

#ifndef IHALTILT_H_
#define IHALTILT_H_

#include <stdint.h>
#include "LIS2DH12.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
typedef LIS2DH12_output IhalTilt_output;

typedef enum
{
  IHALTILT_READ_OK = 0,
  IHALTILT_READ_BTYE_COUNT_MISMATCH,
  IHALTILT_READ_ALL_ZEROS,
  IHALTILT_READ_OVERRANGE

} IhalTilt_readStatus;

//typedef LIS2DH12_readStatus IhalTilt_readStatus;
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Initialise IhalTilt module.
void IhalTilt_init(const uint32_t mode);
//------------------------------------------------------------------------------
// Returns resolution of IhalTilt hardware.
// Unit is K / g. (1K = 1024)
//
// Example:
// 12 bit accelerometer has a full scale deflection of +- 2g.
// therefore +1g = +1024
// and -1g = -1024.
// The resolution is therefore 1K/g.
//
uint32_t IhalTilt_getResolution();
//------------------------------------------------------------------------------
// Reads a single sample set (i.e. all axes - X, Y & Z)
// from the tilt hardware (accelerometer)
IhalTilt_readStatus IhalTilt_readSingleWait(IhalTilt_output* pOutput);
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

#endif /* IHALTILT_H_ */
