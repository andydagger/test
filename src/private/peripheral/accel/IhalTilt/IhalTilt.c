/*
 * IhalTilt.c
 *
 *  Created on: 30 Nov 2018
 *      Author: robwoodhouse
 */

#include "IhalTilt.h"

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
void IhalTilt_init(const uint32_t mode)
{
  //FastSwTimer_delay_mS(100);

  LIS2DH12_initSPI();

  //------------------------------------------

  uint8_t regAddr;

  // CTRL_REG1
  regAddr = 0x20;
  LIS2DH12_writeByteRegWait(regAddr, 0x37); // 25 Hz
  //------------------------
  // CTRL_REG2
  regAddr = 0x21;
  LIS2DH12_writeByteRegWait(regAddr, 0x00);
  //------------------------
  // CTRL_REG3
  regAddr = 0x22;
  LIS2DH12_writeByteRegWait(regAddr, 0x00);
  //------------------------
  // CTRL_REG4
  regAddr = 0x23;
  LIS2DH12_writeByteRegWait(regAddr, 0x08);
  //------------------------
  // CTRL_REG5
  regAddr = 0x24;
  LIS2DH12_writeByteRegWait(regAddr, 0x00);
  //------------------------
  // CTRL_REG6
  regAddr = 0x25;
  LIS2DH12_writeByteRegWait(regAddr, 0x00);
  //------------------------
  // REFERENCE
  regAddr = 0x26;
  LIS2DH12_writeByteRegWait(regAddr, 0x00);
  //------------------------
  // INT1_THS
  regAddr = 0x32;
  LIS2DH12_writeByteRegWait(regAddr, 0x00);
  //------------------------
  // INT1_DUR
  regAddr = 0x33;
  LIS2DH12_writeByteRegWait(regAddr, 0x00);
  //------------------------
  // INT1_CFG
  regAddr = 0x30;
  LIS2DH12_writeByteRegWait(regAddr, 0x00);
  //------------------------
  // INT2_THS
  regAddr = 0x36;
  LIS2DH12_writeByteRegWait(regAddr, 0x00);
  //------------------------
  // INT2_DUR
  regAddr = 0x37;
  LIS2DH12_writeByteRegWait(regAddr, 0x00);
  //------------------------
  // INT2_CFG
  regAddr = 0x34;
  LIS2DH12_writeByteRegWait(regAddr, 0x00);
  //------------------------
  // CTRL_REG5
  regAddr = 0x24;
  LIS2DH12_writeByteRegWait(regAddr, 0x80);

  //------------------------------------------
//  LIS2DH12_debugRegPrint();

  //FastSwTimer_delay_mS(100);
}
//------------------------------------------------------------------------------
uint32_t IhalTilt_getResolution()
{
  return 1;
}
//------------------------------------------------------------------------------
IhalTilt_readStatus IhalTilt_readSingleWait(IhalTilt_output* pOutput)
{
	return (IhalTilt_readStatus) LIS2DH12_readSingleOutputWait(pOutput);
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
