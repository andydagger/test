/*
==========================================================================
 Name        : DaliTL_impl.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliTL_impl.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 6 Nov 2019
 Description : 
==========================================================================
*/

#ifndef PRIVATE_PERIPHERAL_DALI_DALITL_IMPL_H_
#define PRIVATE_PERIPHERAL_DALI_DALITL_IMPL_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "DaliTL.h"
typedef enum
{
    DALITL_STATE_IDLE = 0,
    DALITL_STATE_TX_WAIT,
    DALITL_STATE_TWICE_DELAY,
    DALITL_STATE_REPLY_WAIT
} DALITL_STATE;

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/*****************************************************************************
 * Returns the current state of the state machine
 ****************************************************************************/
DALITL_STATE DaliTL_getState(void);

/*****************************************************************************
 * Sets the current state of the state machine
 ****************************************************************************/
void DaliTL_setState(DALITL_STATE state);

/*****************************************************************************
 * Makes a local copy of the request structure
 ****************************************************************************/
void DaliTL_setRequest(DaliTL_request* p_request);
/*****************************************************************************
 * Returns the local copy of the request structur
 ****************************************************************************/
DaliTL_request DaliTL_getRequest(void);

/*****************************************************************************
 * State machine - Idle state behaviour
 ****************************************************************************/
DaliTL_status DaliTL_Idle(void);

/*****************************************************************************
 * State machine - TxWait state behaviour
 ****************************************************************************/
DaliTL_status DaliTL_TxWait(void);

/*****************************************************************************
 * State machine - Send Twice state behaviour
 ****************************************************************************/
DaliTL_status DaliTL_SendTwiceDelay(void);

/*****************************************************************************
 * State machine - Rx state behaviour
 ****************************************************************************/
DaliTL_status DaliTL_RxWait(void);

/*****************************************************************************
 * State machine - perform a transition to Idle
 ****************************************************************************/
void DaliTL_transitionToIdle(void);

/*****************************************************************************
 * State machine - perform a transition to send twice
 ****************************************************************************/
void DaliTL_transitionToSendTwice(void);

/*****************************************************************************
 * State machine - perform a transition to reply wait
 ****************************************************************************/
void DaliTL_transitionToReplyWait(void);

/*****************************************************************************
 * State machine - perform a transition from send twice to tx wait
 ****************************************************************************/
void DaliTL_transitionSendTwiceToTxWait(void);

#endif /* PRIVATE_PERIPHERAL_DALI_DALITL_IMPL_H_ */
