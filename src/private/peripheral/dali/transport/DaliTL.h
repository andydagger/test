/*
==========================================================================
 Name        : DaliTL.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliTL.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 22 Oct 2019
 Description : 
==========================================================================
*/

#ifndef PRIVATE_PERIPHERAL_DALI_DALITL_H_
#define PRIVATE_PERIPHERAL_DALI_DALITL_H_

#include <stdint.h>
#include <stdbool.h>

typedef enum {
	DALITL_STATUS_DONE = 0,
	DALITL_STATUS_BUSY,
	DALITL_STATUS_TIMEOUT,
	DALITL_STATUS_CORRUPT,
} DaliTL_status;

typedef struct {
	uint8_t address;
	uint8_t txByte;         // byte to be transmitted
	bool b_answerExpected;  // whether or not a reply (i.e. answer) is expected.
	bool b_sendTwice;       // If true then repeat transfer
	bool b_24bit;			// If true use the 24bit encoding
	uint8_t instance;		// instance byte for a 24 bit frame
} DaliTL_request;

typedef enum {
    DALI_TL_RET_OK = 0,
    DALI_TL_RET_ERR
} DALI_TL_RET;

// Starts a transfer
DALI_TL_RET DaliTL_startXfer(DaliTL_request* p_request);

// Runs the DaliTL object, i.e., gives the DaliTL layer run time.
// Returns status of transfer.
DaliTL_status DaliTL_run(void);

// Runs the result of the transfer
uint8_t DaliTL_getResponse(void);


#endif /* PRIVATE_PERIPHERAL_DALI_DALITL_H_ */
