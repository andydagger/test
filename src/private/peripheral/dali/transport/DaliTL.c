/*
==========================================================================
 Name        : DaliTL.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliTL.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 22 Oct 2019
 Description : 
==========================================================================
*/

#include <stdint.h>
#include <stdbool.h>

#include "DaliTL.h"
#include "DaliTL_impl.h"
#include "DaliTx.h"
#include "DaliRx.h"
#include "systick.h"
#include "common.h"

#define MAX_FRAME_LENGTH				3
#define DALI_COMMS_TIMEOUT_MS			200

typedef DaliTL_status(*StateFunction)(void);
StateFunction g_stateFunction[4] = {
		&DaliTL_Idle,
		&DaliTL_TxWait,
		&DaliTL_SendTwiceDelay,
		&DaliTL_RxWait
};

uint32_t localTimerInMs = 0;

// Start Dali transfer.
DALI_TL_RET DaliTL_startXfer(DaliTL_request* p_request)
{
	if (p_request == NULL) {
		return DALI_TL_RET_ERR;
	}
	if (p_request->b_answerExpected && p_request->b_sendTwice) {
		return DALI_TL_RET_ERR;
	}

	uint8_t frame[MAX_FRAME_LENGTH] = {0};

	if (p_request->b_24bit) {
		frame[0] = p_request->address;
		frame[1] = p_request->instance;
		frame[2] = p_request->txByte;
	}
	else {
		frame[0] = p_request->address;
		frame[1] = p_request->txByte;
		frame[2] = 0;
	}

	DaliTL_setRequest(p_request);
	DaliTx_reset();
	DaliTx_start(frame, p_request->b_24bit);
	DaliTL_setState(DALITL_STATE_TX_WAIT);

	localTimerInMs = get_systick_ms();
	return DALI_TL_RET_OK;
}

// Run DaliTL module.
DaliTL_status DaliTL_run(void) {
	if (get_systick_ms() - localTimerInMs >= DALI_COMMS_TIMEOUT_MS) 	{
		return DALITL_STATUS_TIMEOUT;
	}
	return g_stateFunction[DaliTL_getState()]();
}

uint8_t DaliTL_getResponse(void) {
    return DaliRx_getResponse();
}
