/*
==========================================================================
 Name        : DaliTL_impl.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliTL_impl.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 6 Nov 2019
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "DaliTL_impl.h"
#include "DaliTx.h"
#include "DaliRx.h"
#include "systick.h"
#include <string.h>
#include "debug.h"

#define SEND_TWICE_SETTLING_TIME			10
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
DaliTL_request localRequest;
DALITL_STATE localState = DALITL_STATE_IDLE;
uint32_t localTimer = 0;
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
void dali_debug_print_rx_error(DALI_RX_STATUS rxStatus)
{
	const char* rxError = "";
	switch (rxStatus)
	{
	case DALI_RX_DONE:
		rxError = "DALI_RX_DONE";
		break;
	case DALI_RX_STOP:
		rxError = "DALI_RX_STOP";
		break;
	case DALI_RX_TIMEOUT:
		rxError = "DALI_RX_TIMEOUT";
		break;
	case DALI_RX_SETTLING_TIME_ERROR:
		rxError = "DALI_RX_SETTLING_TIME_ERROR";
		break;
	case DALI_RX_TOO_MANY_EDGES:
		rxError = "DALI_RX_TOO_MANY_EDGES";
		break;
	case DALI_RX_PULSE_LENGTH_ERROR:
		rxError = "DALI_RX_PULSE_LENGTH_ERROR";
		break;
	case DALI_RX_NOT_ENOUGH_PULSES:
		rxError = "DALI_RX_NOT_ENOUGH_PULSES";
		break;
	case DALI_RX_ENCODING_ERROR:
		rxError = "DALI_RX_ENCODING_ERROR";
		break;
	case DALI_RX_BUSY:
		rxError = "DALI_RX_BUSY";
		break;
	}

	debug_printf(DALI, "Error: %s\r\n", rxError);
}
/*****************************************************************************
 * Public functions
 ****************************************************************************/
DALITL_STATE DaliTL_getState(void)
{
	return localState;
}

void DaliTL_setState(DALITL_STATE state)
{
	localState = state;
}

void DaliTL_setRequest(DaliTL_request* p_request)
{
	memcpy(&localRequest, p_request, sizeof(DaliTL_request));
}

DaliTL_request DaliTL_getRequest(void)
{
	return localRequest;
}

DaliTL_status DaliTL_Idle(void)
{
	return DALITL_STATUS_BUSY;
}

DaliTL_status DaliTL_TxWait(void)
{
	DaliTX_status txStatus = DaliTx_getStatus();
	if (DALI_TX_DONE == txStatus)
	{
		if (true == localRequest.b_sendTwice)
		{
			DaliTL_transitionToSendTwice();
		}
		else if (true == localRequest.b_answerExpected)
		{
			DaliTL_transitionToReplyWait();
		}
		else
		{
			DaliTL_transitionToIdle();
			return DALITL_STATUS_DONE;
		}
	}
	return DALITL_STATUS_BUSY;
}

DaliTL_status DaliTL_SendTwiceDelay(void)
{
	if ((get_systick_ms() - localTimer) >= SEND_TWICE_SETTLING_TIME)
	{
		DaliTL_transitionSendTwiceToTxWait();
	}
	return DALITL_STATUS_BUSY;
}

DaliTL_status DaliTL_RxWait(void) {
	DaliTL_status status_tl = DALITL_STATUS_BUSY;

	DALI_RX_STATUS status_rx = DaliRx_getStatus();

	if (status_rx == DALI_RX_STOP) {
		status_rx = DaliRx_process();

	    if (status_rx == DALI_RX_DONE) {
	    	debug_printf(DALI, "<< 0x%02X\r\n", DaliRx_getResponse());
	    }
	}

	switch (status_rx) {
		case DALI_RX_DONE: {
			DaliTL_transitionToIdle();
			status_tl = DALITL_STATUS_DONE;
			break;
		}

		case DALI_RX_BUSY: {
			status_tl = DALITL_STATUS_BUSY;
			break;
		}

		case DALI_RX_SETTLING_TIME_ERROR:
		case DALI_RX_TOO_MANY_EDGES:
		case DALI_RX_PULSE_LENGTH_ERROR:
		case DALI_RX_NOT_ENOUGH_PULSES:
		case DALI_RX_ENCODING_ERROR: {
			DaliTL_transitionToIdle();
			status_tl = DALITL_STATUS_CORRUPT;
			break;
		}

		default: {
			DaliTL_transitionToIdle();
			dali_debug_print_rx_error(status_rx);
			status_tl = DALITL_STATUS_TIMEOUT;
			break;
		}
	}

	return status_tl;
}

void DaliTL_transitionToIdle(void)
{
	DaliRx_reset();
	DaliTx_reset();
	DaliTL_setState(DALITL_STATE_IDLE);
}

void DaliTL_transitionSendTwiceToTxWait(void)
{

	localRequest.b_sendTwice = false;
	DaliTL_startXfer(&localRequest);
	DaliTL_setState(DALITL_STATE_TX_WAIT);
}

void DaliTL_transitionToSendTwice(void)
{
	localTimer = get_systick_ms();
	DaliTL_setState(DALITL_STATE_TWICE_DELAY);
}

void DaliTL_transitionToReplyWait(void)
{
	DaliRx_start();
	DaliTL_setState(DALITL_STATE_REPLY_WAIT);
}
