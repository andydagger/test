/*
==========================================================================
 Name        : DaliTx.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliTx.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 6 Nov 2019
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include <DaliHW.h>
#include "DaliTx.h"
#include "DaliTx_impl.h"
#include "common.h"
#include "FreeRTOSCommonHooks.h"
#include "debug.h"
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
#define TX_IDLE_SETTLING_TIME_MS 20
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

bool DaliTx_start(uint8_t * p_frame, uint8_t b_is24BitFrame)
{
    if(p_frame == NULL) return false;
    if (b_is24BitFrame)
    {
    	debug_printf(DALI, ">> 0x%02X%02X%02X\r\n", p_frame[0], p_frame[1], p_frame[2]);
    }
    else
    {
    	debug_printf(DALI, ">> 0x%02X%02X\r\n", p_frame[0], p_frame[1]);
    }
    DaliTx_encodeAndLoadFrame(p_frame, b_is24BitFrame);
    DaliTx_setTxMode();
    DaliTx_resetIsr();
    FreeRTOSDelay(TX_IDLE_SETTLING_TIME_MS);
    DaliTx_startTimer();
    return true;
}

DaliTX_status DaliTx_getStatus(void)
{
    return DaliTx_getTxIsrStatus();
}

void DaliTx_reset(void)
{
    DaliTx_disableTimer();
    DaliTx_setTxMode();
    return;
}

void DaliTx_ISR(void)
{
    DaliTx_run();
}
