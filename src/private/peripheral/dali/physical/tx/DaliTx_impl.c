/*
==========================================================================
 Name        : DaliTx_impl.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliTx_impl.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 7 Nov 2019
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "DaliHW.h"
#include "DaliTx_impl.h"

#define DALI_TX_FREQ    2400
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static DaliTX_status transmitStatus = DALI_TX_DONE;

// ISR Variables
static bool encodedBitFrame[25] = {0};
static uint8_t bitsToSend = 0;
static volatile uint8_t bitsSent = 0;
static volatile uint8_t stopBitTickCount = 0;
static volatile bool b_invertDataBit = false;
static volatile bool b_sendStartBit = false;
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

//--------------------------------------------------------------------
void DaliTx_setTxMode(void)
{
    DaliHw_SetOutputTo(DALI_TX_HIGH);
    return;
}

//--------------------------------------------------------------------
void DaliTx_startTimer(void)
{
    DaliHw_startTimer(2400);
}

//--------------------------------------------------------------------
void DaliTx_disableTimer(void)
{
    DaliHw_StopTimer();
}

//--------------------------------------------------------------------
void DaliTx_encodeAndLoadFrame( uint8_t * p_frameData, bool b_isFrame24Bit)
{
    uint8_t bit;
    uint8_t encodedBits = 0;
    uint8_t bytesToEncode = (b_isFrame24Bit == true) ? 3 : 2;
    bitsToSend = bytesToEncode * 8 + 1; // add one for start bit

    // Clear ISR buffer
    for(bit = 0; bit < 24; bit++) encodedBitFrame[bit] = 0;

    // Place start bit
    encodedBitFrame[encodedBits++] = 1;

    // Encode frame bytes into bits MSB first
    do{
        for(bit = 8; bit > 0; bit--)
        {
            encodedBitFrame[encodedBits++] =
                                ((*p_frameData) & (1<<(bit-1))) ? 1 : 0;
        }
        p_frameData++;
    } while(--bytesToEncode > 0);
}

//--------------------------------------------------------------------
bool * DaliTx_getEncodedBitsFramePtr(void)
{
    return encodedBitFrame;
}


//--------------------------------------------------------------------
DaliTX_status DaliTx_getTxIsrStatus(void)
{
    return transmitStatus;
}

//--------------------------------------------------------------------
void DaliTx_resetIsr(void)
{
    transmitStatus = DALI_TX_BUSY;
    b_invertDataBit = true;
    bitsSent = 0;
    stopBitTickCount = 0;
}

//--------------------------------------------------------------------
void DaliTx_run(void)
{
    if( transmitStatus == DALI_TX_DONE )
    {
        return;
    }
    // If we have sent all the bits
    // Keep o/p high for > 2400us = 6 x (1/2.4Khz)
    if( bitsSent >= bitsToSend)
    {
        DaliHw_SetOutputTo(true);
        if(++stopBitTickCount >= 6)
        {
            transmitStatus = DALI_TX_DONE;
            DaliHw_StopTimer();
            stopBitTickCount = 10; // Avoid wrap-round
        }
        return;
    }

    // Send bits
    if( b_invertDataBit == true )
    {
        // Adjust output to guarantee edge encoding
        DaliHw_SetOutputTo(!encodedBitFrame[bitsSent]);
    }
    else
    {
        // Create edge encoding
        DaliHw_SetOutputTo(encodedBitFrame[bitsSent]);
        bitsSent++;
    }

    // Toggle Manchester encoding gate
    b_invertDataBit = !b_invertDataBit;
}

