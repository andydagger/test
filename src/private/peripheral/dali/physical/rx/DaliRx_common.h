/*
==========================================================================
 Name        : DaliRx_common.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/Rx/DaliRx_common.h
 Author      : Chris BSW
 Copyright   : Lucy Zodion Ltd
 Created     : 21 Nov 2019
 Description :
==========================================================================
*/
#ifndef PRIVATE_PERIPHERAL_DALI_RX_DALIRX_COMMON_H_
#define PRIVATE_PERIPHERAL_DALI_RX_DALIRX_COMMON_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "DaliRx.h"
#include <stdint.h>
#include <stdbool.h>

#define MAXIMUM_EDGES			20
#define MAXIMUM_EDGE_TIMINGS	(MAXIMUM_EDGES - 1)
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
extern uint32_t DaliRx_tickCount;
extern uint32_t DaliRx_tickCountSinceLastEdge;
extern uint32_t DaliRx_frameMaximumTickCount;	//TBC
extern uint32_t DaliRx_stopConditionTickCount;
extern uint32_t DaliRx_minimumSettlingTime;	//3ms@100kHz
extern uint32_t DaliRx_minimumHalfBitTicks;
extern uint32_t DaliRx_maximumHalfBitTicks;
extern uint32_t DaliRx_edgeTimings[MAXIMUM_EDGE_TIMINGS];
extern uint32_t DaliRx_edgeTimingIndex;
extern bool DaliRx_startFlag;

extern DALI_RX_STATUS DaliRx_status;

extern bool DaliRx_pinState;
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/


#endif /* PRIVATE_PERIPHERAL_DALI_RX_DALIRX_COMMON_H_ */
