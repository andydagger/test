/*
==========================================================================
 Name        : DaliRx.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliRx.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 6 Nov 2019
 Description : 
==========================================================================
*/

#ifndef PRIVATE_PERIPHERAL_DALI_DALIRX_C_
#define PRIVATE_PERIPHERAL_DALI_DALIRX_C_

#include <DaliHW.h>
#include "DaliRx.h"
#include "DaliRx_impl.h"
#include "DaliRx_common.h"
#include "debug.h"
/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/

#define MINIMUM_PULSE_COUNT 				9
#define BIT_MSB								0x80
#define MAXIMUM_EDGE_INDEX					18

#define _CHECK_PULSE_WIDTH_HALF(pulse) 									\
		if(checkPulseWidthHalf(pulse) == DALI_RX_PULSE_LENGTH_ERROR)	\
		{return DALI_RX_PULSE_LENGTH_ERROR;}

#define _CHECK_PULSE_WIDTH(pulse) 										\
		if(checkPulseWidth(pulse) == DALI_RX_PULSE_LENGTH_ERROR)		\
		{return DALI_RX_PULSE_LENGTH_ERROR;}

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
uint8_t localResponse;
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/
bool pulseInRange(uint32_t pulseWidth, uint32_t minimum, uint32_t maximum)
{
	return (pulseWidth >= minimum) &&
				(pulseWidth <= maximum);
}

bool pulseInHalfRange(uint32_t pulseWidth)
{
	return pulseInRange(pulseWidth, DaliRx_minimumHalfBitTicks,
			DaliRx_maximumHalfBitTicks);
}

bool pulseInFullRange(uint32_t pulseWidth)
{
	return pulseInRange(pulseWidth, 2 * DaliRx_minimumHalfBitTicks,
			2 * DaliRx_maximumHalfBitTicks);
}

DALI_RX_STATUS checkPulseWidthHalf(uint32_t pulseWidth)
{
	if(!pulseInHalfRange(pulseWidth))
	{
		return DALI_RX_PULSE_LENGTH_ERROR;
	}
	return DALI_RX_DONE;
}

DALI_RX_STATUS checkPulseWidth(uint32_t pulseWidth)
{
	if(!pulseInHalfRange(pulseWidth) && !pulseInFullRange(pulseWidth))
	{
		return DALI_RX_PULSE_LENGTH_ERROR;
	}
	return DALI_RX_DONE;
}

DALI_RX_STATUS validatePulses()
{
	if (DaliRx_edgeTimingIndex < MINIMUM_PULSE_COUNT)
	{
		return DALI_RX_NOT_ENOUGH_PULSES;
	}
	if (DaliRx_edgeTimingIndex % 2 == 0)	//Even
	{
		//start on high, end on hight
		//even number of edges -> odd number of pulses
		return DALI_RX_ENCODING_ERROR;
	}
	//Start pulse
	_CHECK_PULSE_WIDTH_HALF(DaliRx_edgeTimings[0]);

	//Subsequent pulses
	for (uint32_t i = 1; i < DaliRx_edgeTimingIndex; ++i)
	{
		_CHECK_PULSE_WIDTH(DaliRx_edgeTimings[i]);
	}
	return DALI_RX_DONE;
}

static inline bool isDecodingEdge(uint8_t edgeIndex)
{
	return edgeIndex % 2 == 0;
}

static inline bool isRisingEdge(uint8_t pulseIndex)
{
	return pulseIndex % 2 == 0;
}

static inline void setBit(uint8_t bit)
{
	localResponse |= bit;
}

static inline void decodeAndShift(uint8_t edgeIndex, uint8_t pulseIndex, uint8_t* p_bit)
{
	if (isDecodingEdge(edgeIndex))
	{
		if (isRisingEdge(pulseIndex))
		{
			setBit(*p_bit);
		}
		*p_bit >>= 1;
	}
}

DALI_RX_STATUS decodePulses(void)
{
	localResponse = 0;
	uint8_t edgeIndex = 0;
	uint8_t bit = BIT_MSB;
	bool expectHalfPulse = false;

	for (uint8_t pulseIndex = 1; pulseIndex < DaliRx_edgeTimingIndex; ++pulseIndex)
	{
		if (pulseInHalfRange(DaliRx_edgeTimings[pulseIndex]))
		{
			//If we are expecting a half pulse, we now don't care if it is full
			//If we are not expecting a half pulse, we now expect another
			//this ensure that there are always two half pulses
			expectHalfPulse = !expectHalfPulse;
			edgeIndex++;
		}
		else if (expectHalfPulse)
		{
			//invalid - this is a full pulse and we expected a half pulse
			return DALI_RX_ENCODING_ERROR;
		}
		else
		{
			//full pulse
			edgeIndex += 2;
		}
		if (edgeIndex > MAXIMUM_EDGE_INDEX)
		{
			//Too long
			return DALI_RX_ENCODING_ERROR;
		}
		decodeAndShift(edgeIndex, pulseIndex, &bit);
	}
	if (bit != 0)
	{
		//Too short
		return DALI_RX_ENCODING_ERROR;
	}
	return DALI_RX_DONE;
}
/*****************************************************************************
 * Public functions
 ****************************************************************************/
void DaliRx_start(void)
{
	DaliRx_TimerISR_reset();
	DaliRx_InputEdgeISR_reset();
	DaliRx_EnableTimerISR(true);
}

DALI_RX_STATUS DaliRx_getStatus(void)
{
	return DaliRx_status;
}

void DaliRx_reset(void)
{
	DaliRx_EnableTimerISR(false);
	DaliHw_SetOutputTo(true);
}

DALI_RX_STATUS DaliRx_process(void)
{
	DaliRx_status = validatePulses();
	if (DaliRx_status == DALI_RX_DONE)
	{
		DaliRx_status = decodePulses();
	}
	return DaliRx_status;
}

uint8_t DaliRx_getResponse(void)
{
	return localResponse;
}

void DaliRx_TimerISR(void)
{
	DaliRx_TimerISR_run();
}

#endif /* PRIVATE_PERIPHERAL_DALI_DALIRX_C_ */
