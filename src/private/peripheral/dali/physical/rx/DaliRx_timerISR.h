/*
==========================================================================
 Name        : DaliRx_timerISR.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/Rx/DaliRx_timerISR.h
 Author      : Chris BSW
 Copyright   : Lucy Zodion Ltd
 Created     : 21 Nov 2019
 Description :
==========================================================================
*/
#ifndef PRIVATE_PERIPHERAL_DALI_RX_DALIRX_TIMERISR_H_
#define PRIVATE_PERIPHERAL_DALI_RX_DALIRX_TIMERISR_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void DaliRx_EnableTimerISR(bool b_enable);

void DaliRx_TimerISR_reset(void);

void DaliRx_TimerISR_run(void);

uint32_t DaliRx_ticksSinceLastEdge(void);

#endif /* PRIVATE_PERIPHERAL_DALI_RX_DALIRX_TIMERISR_H_ */
