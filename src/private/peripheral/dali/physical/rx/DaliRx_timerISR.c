/*
==========================================================================
 Name        : DaliRx_timerISR.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/Rx/DaliRx_timerISR.c
 Author      : Chris BSW
 Copyright   : Lucy Zodion Ltd
 Created     : 21 Nov 2019
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include <DaliHW.h>
#include "DaliRx_timerISR.h"
#include "DaliRx_common.h"
#include "DaliRx_edgeISR.h"

#define DALI_RX_TIMER_FREQ          100000

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
void DaliRx_EnableTimerISR(bool b_enable)
{
    if( b_enable == true)
    {
        DaliHw_startTimer(DALI_RX_TIMER_FREQ);
    }else
    {
        DaliHw_StopTimer();
    }
}


void DaliRx_TimerISR_reset(void)
{
    DaliRx_status = DALI_RX_BUSY;
	DaliRx_tickCount = 0;
	DaliRx_tickCountSinceLastEdge = 0;
}

void DaliRx_TimerISR_run(void)
{
    if(DaliRx_status != DALI_RX_BUSY)
    {
        return;
    }
    bool currentDaliPinState = DaliHw_ReadInput();
    if (currentDaliPinState != DaliRx_pinState)
    {
    	DaliRx_InputEdgeISR_run();
    	DaliRx_pinState = currentDaliPinState;
    }
	++DaliRx_tickCount;
	++DaliRx_tickCountSinceLastEdge;
	if (DaliRx_tickCount >= DaliRx_frameMaximumTickCount)
	{
		DaliRx_status = DALI_RX_TIMEOUT;
		DaliRx_reset();
	}
	else if ((DaliRx_tickCountSinceLastEdge >= DaliRx_stopConditionTickCount)
	        && (DaliRx_startFlag == true) )
	{
		DaliRx_status = DALI_RX_STOP;
		DaliRx_reset();
	}
}

uint32_t DaliRx_ticksSinceLastEdge(void)
{
	uint32_t output = DaliRx_tickCountSinceLastEdge;
	DaliRx_tickCountSinceLastEdge = 0;
	return output;
}
