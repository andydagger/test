/*
==========================================================================
 Name        : DaliRx_edgeISR.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/Rx/DaliRx_edgeISR.h
 Author      : Chris BSW
 Copyright   : Lucy Zodion Ltd
 Created     : 21 Nov 2019
 Description :
==========================================================================
*/
#ifndef PRIVATE_PERIPHERAL_DALI_RX_DALIRX_EDGEISR_H_
#define PRIVATE_PERIPHERAL_DALI_RX_DALIRX_EDGEISR_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

void DaliRx_InputEdgeISR_reset(void);

void DaliRx_InputEdgeISR_run(void);


#endif /* PRIVATE_PERIPHERAL_DALI_RX_DALIRX_EDGEISR_H_ */
