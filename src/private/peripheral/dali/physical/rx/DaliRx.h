/*
==========================================================================
 Name        : DaliRx.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliRx.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 6 Nov 2019
 Description : 
==========================================================================
*/
#ifndef PRIVATE_PERIPHERAL_DALI_DALIRX_H_
#define PRIVATE_PERIPHERAL_DALI_DALIRX_H_

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
typedef enum {
	DALI_RX_DONE,
	DALI_RX_STOP,
	DALI_RX_TIMEOUT,
	DALI_RX_SETTLING_TIME_ERROR,
	DALI_RX_TOO_MANY_EDGES,
	DALI_RX_PULSE_LENGTH_ERROR,
	DALI_RX_NOT_ENOUGH_PULSES,
	DALI_RX_ENCODING_ERROR,
	DALI_RX_BUSY
} DALI_RX_STATUS;
/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

void DaliRx_start(void);
DALI_RX_STATUS DaliRx_getStatus(void);
DALI_RX_STATUS DaliRx_process(void);
uint8_t DaliRx_getResponse(void);
void DaliRx_reset(void);

void DaliRx_InputEdgeISR(void);
void DaliRx_TimerISR(void);

#endif /* PRIVATE_PERIPHERAL_DALI_DALIRX_H_ */
