/*
 * DaliRx_impl.h
 *
 *  Created on: 7 Nov 2019
 *      Author: LZ.RED.ENG
 */

#ifndef PRIVATE_PERIPHERAL_DALI_DALIRX_IMPL_H_
#define PRIVATE_PERIPHERAL_DALI_DALIRX_IMPL_H_


/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "DaliRx.h"
#include "DaliRx_edgeISR.h"
#include "DaliRx_timerISR.h"

#endif /* PRIVATE_PERIPHERAL_DALI_DALIRX_IMPL_H_ */
