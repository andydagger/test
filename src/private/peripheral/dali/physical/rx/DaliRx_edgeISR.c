/*
==========================================================================
 Name        : DaliRx_edgeISR.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/Rx/DaliRx_edgeISR.c
 Author      : Chris BSW
 Copyright   : Lucy Zodion Ltd
 Created     : 21 Nov 2019
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include <DaliHW.h>
#include "DaliRx_edgeISR.h"
#include "DaliRx_common.h"
#include "DaliRx_timerISR.h"
#include "memory.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

void DaliRx_InputEdgeISR_reset(void)
{
	DaliRx_startFlag = false;
	DaliRx_pinState = DaliHw_ReadInput();
	memset(DaliRx_edgeTimings, 0, sizeof(DaliRx_edgeTimings));
}


void DaliRx_InputEdgeISR_run(void)
{
    if(DaliRx_status != DALI_RX_BUSY)
    {
        return;
    }
	if (DaliRx_startFlag)
	{
		if (DaliRx_edgeTimingIndex == MAXIMUM_EDGE_TIMINGS)
		{
			//Buffer overrun
			DaliRx_status = DALI_RX_TOO_MANY_EDGES;
			DaliRx_reset();

			return;
		}
		DaliRx_edgeTimings[DaliRx_edgeTimingIndex++] =
				DaliRx_ticksSinceLastEdge();
	}
	else if (DaliRx_ticksSinceLastEdge() < DaliRx_minimumSettlingTime)
	{
		DaliRx_status = DALI_RX_SETTLING_TIME_ERROR;
		DaliRx_reset();
	}
	else
	{
	    DaliRx_tickCountSinceLastEdge = 0;
		DaliRx_edgeTimingIndex = 0;
		DaliRx_startFlag = true;
	}
}
