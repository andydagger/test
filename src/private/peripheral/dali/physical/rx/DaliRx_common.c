/*
==========================================================================
 Name        : DaliRx_common.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/Rx/DaliRx_common.h
 Author      : Chris BSW
 Copyright   : Lucy Zodion Ltd
 Created     : 21 Nov 2019
 Description :
==========================================================================
*/
#include "DaliRx_common.h"

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
uint32_t DaliRx_tickCount = 0;
uint32_t DaliRx_tickCountSinceLastEdge = 0;
uint32_t DaliRx_frameMaximumTickCount = 15000;	//TBC
uint32_t DaliRx_stopConditionTickCount = 240;
uint32_t DaliRx_minimumSettlingTime = 300;	//3ms@100kHz
uint32_t DaliRx_minimumHalfBitTicks = 33;
uint32_t DaliRx_maximumHalfBitTicks = 50;
uint32_t DaliRx_edgeTimings[MAXIMUM_EDGE_TIMINGS] = {0};
uint32_t DaliRx_edgeTimingIndex = 0;
bool DaliRx_startFlag = false;

DALI_RX_STATUS DaliRx_status = DALI_RX_DONE;

bool DaliRx_pinState = false;
/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/
