/*
 * DaliAL_p.h
 *
 *  Created on: 24 Oct 2019
 *      Author: LZ.RED.ENG
 */

#ifndef PRIVATE_PERIPHERAL_DALI_DALIAL_IMPL_H_
#define PRIVATE_PERIPHERAL_DALI_DALIAL_IMPL_H_

#include "DaliAL.h"
#include "DaliTL.h"

extern uint8_t dali_faults;

dali_bus_status_t DaliAL_processRequest(DaliTL_request *request, uint8_t *resp);

#endif /* PRIVATE_PERIPHERAL_DALI_DALIAL_IMPL_H_ */
