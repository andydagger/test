/*
==========================================================================
 Name        : DaliAL.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliAL_impl.c
 Author      : Chris Burdett-Smith-Whitting
 Copyright   : Lucy Zodion Ltd
 Created     : 23 Oct 2019
 Description :
==========================================================================
*/

#include <stdint.h>

#include "DaliAL_impl.h"
#include "sysmon.h"

uint8_t dali_faults = 0;

/*!
	\brief 		Process and send a single DALI packet
	\note 		Blocking
*/
dali_bus_status_t DaliAL_processRequest(DaliTL_request *request, uint8_t *resp) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DALI_TL_RET error_tl = DaliTL_startXfer(request);

	if (error_tl != DALI_TL_RET_OK) {
		return status;
	}

	DaliTL_status status_tl;
	do {
		status_tl = DaliTL_run();

		if (status_tl == DALITL_STATUS_TIMEOUT) {
			if (!request->b_24bit && request->b_answerExpected) {
				++dali_faults;

				if (dali_faults == 3) {
					set_sysmon_fault(DALI_FAULT);
				}
			}

			status = DALI_BUS_STATUS_TIMEOUT;
			break;
		}
		else if (status_tl == DALITL_STATUS_CORRUPT) {
			status = DALI_BUS_STATUS_CORRUPT;
			break;
		}
	} while (status_tl != DALITL_STATUS_DONE);

	if (status_tl == DALITL_STATUS_DONE) {
		if (resp != NULL) {
			*resp = DaliTL_getResponse();
		}

		if (!request->b_24bit && request->b_answerExpected) {
			dali_faults = 0;
			clear_sysmon_fault(DALI_FAULT);
		}

		status = DALI_BUS_STATUS_OK;
	}

	return status;
}
