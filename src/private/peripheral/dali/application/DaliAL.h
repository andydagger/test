/*
==========================================================================
 Name        : DaliAL.h
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliAL.h
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 22 Oct 2019
 Description : 
==========================================================================
*/

#ifndef PRIVATE_PERIPHERAL_DALI_DALIAL_H_
#define PRIVATE_PERIPHERAL_DALI_DALIAL_H_

#include <stdint.h>

#include "DaliAL_db.h"

#define DALI_ADDRESS_BROADCAST 				0xFF
#define DALI_ADDRESS_BROADCAST_UNADDRESSED 	0xFE

/*
	TODO: Special transitionary macro.

	For use in application-layer functions that are being migrated to use
	DALI addresses instead of broadcast, but without the underlying framework
	to use properly.

	Has no special meaning to underlying functions, is simply the zero-address.
*/
#define DALI_ADDRESS_IGNORE 0

#define DALI_MAX_ADDRESS_LONG 	((uint32_t) 0x00FFFFFF)
#define DALI_MAX_ADDRESS_SHORT 	((uint8_t) 0xFF)

#define DALI_INSTANCE_BROADCAST 		0xFF
#define DALI_INSTANCE_DEVICE 			0xFE
#define DALI_INSTANCE_BROADCAST_FOI 	0xFD
#define DALI_INSTANCE_FOD 				0xFC

#define DALI_MASK 	((uint8_t) 0xFF)

typedef enum {
	DALI_AL_RET_OK = 0,
	DALI_AL_RET_ERR
} DALI_AL_RET;

typedef enum dali_address_mode {
	DALI_ADDRESS_MODE_BROADCAST = 0,
	DALI_ADDRESS_MODE_SHORT,
	DALI_ADDRESS_MODE_GROUP,
	DALI_ADDRESS_MODE_SPECIAL,
	DALI_ADDRESS_MODE_MANUAL,
} dali_address_mode_t;

typedef enum dali_al_status {
	DALI_BUS_STATUS_OK = 0,

	DALI_BUS_STATUS_BUSY,
	DALI_BUS_STATUS_TIMEOUT,
	DALI_BUS_STATUS_CORRUPT,
	DALI_BUS_STATUS_INVALID,

	DALI_BUS_STATUS_MAX,
} dali_bus_status_t;

typedef enum dali_device_type {
	DALI_DEVICE_TYPE_NONE 	= 0,

	DALI_DEVICE_TYPE_LED 	= 6,

	DALI_DEVICE_TYPE_MASK 	= 0xFF
} dali_device_type_t;

// TODO: Populate from spec
typedef enum dali_instance_type {
	DALI_INSTANCE_TYPE_GENERIC 	= 0,
	DALI_INSTANCE_TYPE_SENSOR 	= 3,

	DALI_INSTANCE_TYPE_INVALID 	= 32
} dali_instance_type_t;

// TODO: Populate from spec
typedef enum dali_quiescence_mode {
	DALI_QUIESCENCE_MODE_UNKNOWN = 0xFF,
} dali_quiescence_mode_t;

typedef enum dali_driver_status {
	DALI_DRIVER_STATUS_NONE 					= 0x00,
	DALI_DRIVER_STATUS_CONTROL_GEAR_FAILURE 	= 0x01,
	DALI_DRIVER_STATUS_LAMP_FAILURE 			= 0x02,
	DALI_DRIVER_STATUS_LAMP_ON 					= 0x04,
	DALI_DRIVER_STATUS_LIMIT_ERROR 				= 0x08,
	DALI_DRIVER_STATUS_FADE_RUNNING 			= 0x10,
	DALI_DRIVER_STATUS_RESET_STATE 				= 0x20,
	DALI_DRIVER_STATUS_SHORT_ADDRRESS 			= 0x40,
	DALI_DRIVER_STATUS_POWER_CYCLE_SEEN 		= 0x80
} dali_driver_status_t;

typedef enum dali_driver_failure_status {
	DALI_DRIVER_FAILURE_STATUS_NONE  							= 0x00,
	DALI_DRIVER_FAILURE_STATUS_SHORT_CIRCUIT 					= 0x01,
	DALI_DRIVER_FAILURE_STATUS_OPEN_CIRCUIT 					= 0x02,
	DALI_DRIVER_FAILURE_STATUS_LOAD_DECREASE 					= 0x04,
	DALI_DRIVER_FAILURE_STATUS_LOAD_INCREASE 					= 0x08,
	DALI_DRIVER_FAILURE_STATUS_CURRENT_PROTECTOR_ACTIVE 		= 0x10,
	DALI_DRIVER_FAILURE_STATUS_THERMAL_SHUTDOWN 				= 0x20,
	DALI_DRIVER_FAILURE_STATUS_THERMAL_OVERLOAD_LIGHT_REDUCED 	= 0x40,
	DALI_DRIVER_FAILURE_STATUS_REFERENCE_MEASUREMENT_FAILED 	= 0x80
} dali_driver_failure_status_t;

typedef enum dali_dimming_curve {
	DALI_DIMMING_CURVE_LOGARITHMIC 	= 0,
	DALI_DIMMING_CURVE_LINEAR 		= 1,
	DALI_DIMMING_CURVE_UNKNOWN,
} dali_dimming_curve_t;

typedef enum dali_lightsource {
	DALI_LIGHTSOURCE_FLUO 			= 0,
	DALI_LIGHTSOURCE_HID 			= 2,
	DALI_LIGHTSOURCE_LV_HAL 		= 3,
	DALI_LIGHTSOURCE_INCANDESCENT 	= 4,
	DALI_LIGHTSOURCE_LED 			= 6,
	DALI_LIGHTSOURCE_OLED 			= 7,
	DALI_LIGHTSOURCE_NONE 			= 254
} dali_lightsource_t;

dali_bus_status_t DaliAL_command_tx(uint8_t address, dali_command_t command, dali_address_mode_t mode, uint8_t *payload_tx, uint8_t *payload_rx);

dali_bus_status_t DaliAL_command_tx_broadcast(dali_command_t command, uint8_t *payload_tx);
dali_bus_status_t DaliAL_command_tx_short(uint8_t address, dali_command_t command, uint8_t *payload_tx, uint8_t *payload_rx);
dali_bus_status_t DaliAL_command_tx_special(dali_command_t command, uint8_t *payload_tx, uint8_t *payload_rx);

uint8_t DaliAL_encode_address(uint8_t address, uint8_t template, dali_address_mode_t mode);

bool DaliAL_compare(dali_bus_status_t *bus);
void DaliAL_dataTransferRegister0(uint8_t data, dali_bus_status_t *bus);
void DaliAL_dataTransferRegister1(uint8_t data, dali_bus_status_t *bus);
void DaliAL_dataTransferRegister2(uint8_t data, dali_bus_status_t *bus);
void DaliAL_directArcPowerControl(uint8_t level, dali_bus_status_t *bus);
void DaliAL_disableApplicationController(dali_bus_status_t *bus);
void DaliAL_disableInstance(uint8_t instance, dali_bus_status_t *bus);
void DaliAL_enableApplicationController(dali_bus_status_t *bus);
void DaliAL_enableDeviceType(dali_device_type_t device_type, dali_bus_status_t *bus);
void DaliAL_enableWriteMemory(uint8_t address, dali_bus_status_t *bus);
void DaliAL_initialise(uint8_t device, dali_bus_status_t *bus);
void DaliAL_off(uint8_t address, dali_bus_status_t *bus);
void DaliAL_onAndStepUp(uint8_t address, dali_bus_status_t *bus);
void DaliAL_ping(dali_bus_status_t *bus);
void DaliAL_programShortAddress(uint8_t address, dali_bus_status_t *bus);
uint8_t DaliAL_queryActualLevel(uint8_t address, dali_bus_status_t *bus);
bool DaliAL_queryApplicationControllerEnabled(uint8_t address, dali_bus_status_t *bus);
bool DaliAL_queryControlGearPresent(uint8_t address, dali_bus_status_t *bus);
dali_device_type_t DaliAL_queryDeviceType(uint8_t address, dali_bus_status_t *bus);
dali_dimming_curve_t DaliAL_queryDimmingCurve(uint8_t address, dali_device_type_t device_type, dali_bus_status_t *bus);
uint8_t DaliAL_queryDriverVersionNumber(uint8_t address, dali_bus_status_t *bus);
uint8_t DaliAL_queryExtendedVersionNumber(uint8_t address, dali_bus_status_t *bus);
dali_driver_failure_status_t DaliAL_queryFailureStatus(uint8_t address, dali_device_type_t device_type, dali_bus_status_t *bus);
uint8_t DaliAL_queryInputValue(uint8_t address, uint8_t instance, dali_bus_status_t *bus);
uint8_t DaliAL_queryInputValueLatch(uint8_t address, uint8_t instance, dali_bus_status_t *bus);
dali_instance_type_t DaliAL_queryInstanceType(uint8_t address, uint8_t instance, dali_bus_status_t *bus);
dali_lightsource_t DaliAL_queryLightSourceType(uint8_t address, dali_bus_status_t *bus);
dali_device_type_t DaliAL_queryNextDeviceType(uint8_t address, dali_bus_status_t *bus);
uint8_t DaliAL_queryNumberOfInstances(uint8_t address, dali_bus_status_t *bus);
uint8_t DaliAL_queryPhysicalMinimum(uint8_t address, dali_bus_status_t *bus);
dali_quiescence_mode_t DaliAL_queryQuiescentMode(uint8_t address, dali_bus_status_t *bus);
uint8_t DaliAL_querySensorVersionNumber(uint8_t address, dali_bus_status_t *bus);
dali_driver_status_t DaliAL_queryStatus(uint8_t address, dali_bus_status_t *bus);
void DaliAL_randomise(dali_bus_status_t *bus);
uint8_t DaliAL_readMemoryLocation(uint8_t address, dali_bus_status_t *bus);
void DaliAL_reset(dali_bus_status_t *bus);
void DaliAL_selectDimmingCurve(dali_device_type_t device_type, dali_dimming_curve_t curve, dali_bus_status_t *bus);
void DaliAL_setEventFilter(uint8_t instance, uint32_t event_filter, dali_bus_status_t *bus);
void DaliAL_setFadeTime(uint8_t fade_time, dali_bus_status_t *bus);
void DaliAL_setSearchAddress(uint32_t address, dali_bus_status_t *bus);
void DaliAL_setShortAddress(uint8_t address, uint8_t new_address, dali_bus_status_t *bus);
void DaliAL_startQuiescentMode(dali_bus_status_t *bus);
void DaliAL_stopQuiescentMode(dali_bus_status_t *bus);
void DaliAL_terminate(dali_bus_status_t *bus);
uint8_t DaliAL_verifyShortAddress(uint8_t address, dali_bus_status_t *bus);
void DaliAL_withdraw(dali_bus_status_t *bus);
void DaliAL_writeMemoryLocation(uint8_t payload, dali_bus_status_t *bus);

#endif /* PRIVATE_PERIPHERAL_DALI_DALIAL_H_ */
