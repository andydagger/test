/*
==========================================================================
 Name        : DaliAL.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/Dali/DaliAL.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 22 Oct 2019
 Description : 
==========================================================================
*/

#include <stdint.h>
#include <stdbool.h>

#include "DaliAL.h"
#include "DaliAL_impl.h"
#include "DaliAL_db.h"
#include "DaliTL.h"
#include "common.h"

#define MAX_FADE_TIME				15
#define MAX_EVENT_FILTER			0x00FFFFFF

/*!
	\brief 		Construct and perform round-trip transmission of a single DALI frame

	\param 		address 	Address of target.
							If using SHORT addressing mode, range 0-63.
							If using GROUP addressing mode, range 0-16.
							If using BROADCAST addressing mode, ignored.
							If using SPECIAL addressing mode, ignored.
							If using MANUAL addressing mode, range 0-255.

	\param 		address 	Real device address
	\param 		command 	Control command code
	\param 		mode 		Addressing mode
	\param 		payload 	1-byte tx buffer

	\return 	Optional 1-byte raw return value
*/
dali_bus_status_t DaliAL_command_tx(
	uint8_t 					address,
	dali_command_t 				command,
	dali_address_mode_t 		mode,
	uint8_t 					*payload_tx,
	uint8_t 					*payload_rx
) {
	dali_bus_status_t ret = DALI_BUS_STATUS_INVALID;

	const struct dali_msg *template = NULL;

	do {
		if (!DaliAL_db_fetch_template(command, &template, 0)) {
			break;
		}

		if (template == NULL) {
			break;
		}

		struct dali_msg msg = *template;

		msg.address = DaliAL_encode_address(address, template->address, mode);

		if (msg.size == DALI_PACKET_FF16) {
			if (msg.flags.selector) {
				msg.address &= ~0x01;
			}
			else {
				msg.address |= 0x01;
			}

			// 16-bit FF writes pack opcode as opcode + payload
			if (payload_tx != NULL) {
				msg.opcode += *payload_tx;
			}
		}
		else if (msg.size == DALI_PACKET_FF24) {
			msg.address |= 0x01;
		}
		else {
			break;
		}

		DaliTL_request request = {
			.address 			= msg.address,
			.instance 			= msg.instance,
			.txByte 			= msg.opcode,
			.b_answerExpected 	= msg.flags.has_response,
			.b_24bit 			= (msg.size == DALI_PACKET_FF24),
			.b_sendTwice 		= msg.flags.send_twice
		};

		if (!msg.flags.has_response) {
			payload_rx = NULL;
		}

		DALI_AL_RET ret_al = DaliAL_processRequest(&request, payload_rx);

		if (ret_al == DALI_AL_RET_OK) {
			ret = DALI_BUS_STATUS_OK;
		}
		else {
			ret = DALI_BUS_STATUS_INVALID;
		}
	} while (0);

	return ret;
}

/*!
	\brief 		Encode a real integer device address into a DALI raw byte address

	\details 	Different addressing modes encode bits in different ways.

				16-bit packets:
					Short addressing: 	0b0xxxxxx0
					Group addressing: 	0b10xxxxx0
					Broadcast: 			0b11111111

	\param 		address 	Device address
	\param 		template 	Default address from template (kept for SPECIAL)
	\param 		mode 		Addressing mode

	\return 	Encoded byte address
*/
uint8_t DaliAL_encode_address(uint8_t address, uint8_t template, dali_address_mode_t mode) {
	uint8_t encoded = template;

	switch (mode) {
		case DALI_ADDRESS_MODE_SHORT: {
			encoded = (address & 0x3F) << 1;
			encoded &= 0b01111110;
			encoded |= 0b00000000;
			break;
		}
		case DALI_ADDRESS_MODE_GROUP: {
			encoded = (address & 0x1F) << 1;
			encoded &= 0b00111110;
			encoded |= 0b10000000;
			break;
		}
		case DALI_ADDRESS_MODE_BROADCAST: {
			encoded = DALI_ADDRESS_BROADCAST;
			break;
		}
		case DALI_ADDRESS_MODE_SPECIAL: {
			break;
		}
		case DALI_ADDRESS_MODE_MANUAL: {
			encoded = address;
			break;
		}
		default: {
			break;
		}
	}

	return encoded;
}

/*!
	\brief 		Transmit a round-trip DALI packet using SHORT addressing
*/
dali_bus_status_t DaliAL_command_tx_short(uint8_t address, dali_command_t command, uint8_t *payload_tx, uint8_t *payload_rx) {
	return DaliAL_command_tx(address, command, DALI_ADDRESS_MODE_SHORT, payload_tx, payload_rx);
}

/*!
	\brief 		Transmit a round-trip DALI packet using BROADCAST addressing
*/
dali_bus_status_t DaliAL_command_tx_broadcast(dali_command_t command, uint8_t *payload_tx) {
	return DaliAL_command_tx(DALI_ADDRESS_IGNORE, command, DALI_ADDRESS_MODE_BROADCAST, payload_tx, NULL);
}

/*!
	\brief 		Transmit a round-trip DALI packet using SPECIAL addressing
*/
dali_bus_status_t DaliAL_command_tx_special(dali_command_t command, uint8_t *payload_tx, uint8_t *payload_rx) {
	return DaliAL_command_tx(DALI_ADDRESS_IGNORE, command, DALI_ADDRESS_MODE_SPECIAL, payload_tx, NULL);
}

/*!
	\brief 		Sends a command to Query the device status
*/
dali_driver_status_t DaliAL_queryStatus(uint8_t address, dali_bus_status_t *bus) {
	dali_driver_status_t driver_status = DALI_DRIVER_STATUS_NONE;

	uint8_t raw = 0;

	dali_bus_status_t bus_status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_STATUS,
		NULL,
		&raw
	);

	if (bus_status == DALI_BUS_STATUS_OK) {
		driver_status = raw;
	}

	if (bus != NULL) {
		*bus = bus_status;
	}

	return driver_status;
}

/*!
	\brief 		Sends a DAPC command with the new level
*/
void DaliAL_directArcPowerControl(uint8_t level, dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_broadcast(
		DALI_COMMAND_DIRECT_ARC_POWER_CONTROL,
		&level
	);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Send a command to reset all of the device's variables
*/
void DaliAL_reset(dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_broadcast(DALI_COMMAND_RESET, NULL);

	if (status == DALI_BUS_STATUS_OK) {
		delay_ms(350);
	}

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to set the fade time in seconds (15 seconds max)
*/
void DaliAL_setFadeTime(uint8_t fade_time, dali_bus_status_t *bus) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	do {
		if (fade_time > MAX_FADE_TIME) {
			break;
		}

		DaliAL_dataTransferRegister0(fade_time, &status);
		if (status != DALI_BUS_STATUS_OK) {
			break;
		}

		status = DaliAL_command_tx_broadcast(DALI_COMMAND_SET_FADE_TIME, NULL);
	} while (0);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to query the device type
*/
dali_device_type_t DaliAL_queryDeviceType(uint8_t address, dali_bus_status_t *bus) {
	dali_device_type_t device_type = DALI_DEVICE_TYPE_NONE;

	uint8_t raw = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_DEVICE_TYPE,
		NULL,
		&raw
	);

	if (status == DALI_BUS_STATUS_OK) {
		device_type = raw;
	}

	if (bus != NULL) {
		*bus = status;
	}

	return device_type;
}

/*!
	\brief 		Sends a command to query the next device type
*/
dali_device_type_t DaliAL_queryNextDeviceType(uint8_t address, dali_bus_status_t *bus) {
	dali_device_type_t device_type = DALI_DEVICE_TYPE_NONE;

	uint8_t raw = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_NEXT_DEVICE_TYPE,
		NULL,
		&raw
	);

	if (status == DALI_BUS_STATUS_OK) {
		device_type = raw;
	}

	if (bus != NULL) {
		*bus = status;
	}

	return device_type;
}

/*!
	\brief 		Sends a command to query the minimum lighting level
*/
uint8_t DaliAL_queryPhysicalMinimum(uint8_t address, dali_bus_status_t *bus) {
	uint8_t physical_min = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_PHYSICAL_MINIMUM,
		NULL,
		&physical_min
	);

	if (bus != NULL) {
		*bus = status;
	}

	return physical_min;
}

/*!
	\brief 		Sends a command to query the light source type
*/
dali_lightsource_t DaliAL_queryLightSourceType(uint8_t address, dali_bus_status_t *bus) {
	dali_lightsource_t source = DALI_LIGHTSOURCE_NONE;

	uint8_t raw = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_LIGHT_SOURCE_TYPE,
		NULL,
		&raw
	);

	if (status == DALI_BUS_STATUS_OK) {
		source = raw;
	}

	if (bus != NULL) {
		*bus = status;
	}

	return source;
}

/*!
	\brief 		Sends a command to query the current lighting level
*/
uint8_t DaliAL_queryActualLevel(uint8_t address, dali_bus_status_t *bus) {
	uint8_t level = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_ACTUAL_LEVEL,
		NULL,
		&level
	);

	if (bus != NULL) {
		*bus = status;
	}

	return level;
}

/*!
	\brief 		Sends a command to query the driver version number
*/
uint8_t DaliAL_queryDriverVersionNumber(uint8_t address, dali_bus_status_t *bus) {
	uint8_t driver_version = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_DRIVER_VERSION_NUMBER,
		NULL,
		&driver_version
	);

	if (bus != NULL) {
		*bus = status;
	}

	return driver_version;
}

/*!
	\brief 		Sends a command to query the sensor version number
*/
uint8_t DaliAL_querySensorVersionNumber(uint8_t address, dali_bus_status_t *bus) {
	uint8_t sensor_version = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_SENSOR_VERSION_NUMBER,
		NULL,
		&sensor_version
	);

	if (bus != NULL) {
		*bus = status;
	}

	return sensor_version;
}

/*!
	\brief 		Sends a command to query the extended version number
*/
uint8_t DaliAL_queryExtendedVersionNumber(uint8_t address, dali_bus_status_t *bus) {
	uint8_t extended_version = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_EXTENDED_VERSION_NUMBER,
		NULL,
		&extended_version
	);

	if (bus != NULL) {
		*bus = status;
	}

	return extended_version;
}

/*!
	\brief 		Sends a command to query the dimming curve
*/
dali_dimming_curve_t DaliAL_queryDimmingCurve(uint8_t address, dali_device_type_t device_type, dali_bus_status_t *bus) {
	dali_dimming_curve_t curve = DALI_DIMMING_CURVE_UNKNOWN;

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	do {
		DaliAL_enableDeviceType(device_type, &status);
		if (status != DALI_BUS_STATUS_OK) {
			break;
		}

		uint8_t raw = 0;

		status = DaliAL_command_tx_short(address, DALI_QUERY_DIMMING_CURVE, NULL, &raw);

		if (status == DALI_BUS_STATUS_OK) {
			curve = raw;
		}
	} while (0);

	if (bus != NULL) {
		*bus = status;
	}

	return curve;
}

/*!
	\brief 		Sends a command to select the dimming curve
*/
void DaliAL_selectDimmingCurve(dali_device_type_t device_type, dali_dimming_curve_t curve, dali_bus_status_t *bus) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	do {
		if (curve >= DALI_DIMMING_CURVE_UNKNOWN) {
			break;
		}

		DaliAL_dataTransferRegister0(curve, &status);
		if (status != DALI_BUS_STATUS_OK) {
			break;
		}

		DaliAL_enableDeviceType(device_type, &status);
		if (status != DALI_BUS_STATUS_OK) {
			break;
		}

		status = DaliAL_command_tx_broadcast(DALI_COMMAND_SELECT_DIMMING_CURVE, NULL);
	} while (0);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to enable a particular device type
*/
void DaliAL_enableDeviceType(dali_device_type_t device_type, dali_bus_status_t *bus) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	if (device_type < 254) {
		uint8_t raw = device_type;

		status = DaliAL_command_tx_special(DALI_COMMAND_ENABLE_DEVICE_TYPE, &raw, NULL);
	}

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to indicate presence
*/
void DaliAL_ping(dali_bus_status_t *bus) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	status = DaliAL_command_tx_special(DALI_COMMAND_PING, NULL, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to set the value of the DTR0 register
*/
void DaliAL_dataTransferRegister0(uint8_t data, dali_bus_status_t *bus) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	status = DaliAL_command_tx_special(DALI_COMMAND_DTR0, &data, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to set the value of the DTR1 register
*/
void DaliAL_dataTransferRegister1(uint8_t data, dali_bus_status_t *bus) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	status = DaliAL_command_tx_special(DALI_COMMAND_DTR1, &data, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to set the value of the DTR2 register
*/
void DaliAL_dataTransferRegister2(uint8_t data, dali_bus_status_t *bus) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	status = DaliAL_command_tx_special(DALI_COMMAND_DTR2, &data, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to enable write mode (must be called prior to any write)
*/
void DaliAL_enableWriteMemory(uint8_t address, dali_bus_status_t *bus) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	status = DaliAL_command_tx_short(address, DALI_COMMAND_ENABLE_WRITE_MEMORY, NULL, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to write data to memory bank (DTR1), and address (DTR0)
*/
void DaliAL_writeMemoryLocation(uint8_t payload, dali_bus_status_t *bus) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	status = DaliAL_command_tx_special(DALI_COMMAND_WRITE_MEMORY_LOCATION_NOREPLY, &payload, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to read data from memory bank (DTR1), and address (DTR0)
*/
uint8_t DaliAL_readMemoryLocation(uint8_t address, dali_bus_status_t *bus) {
	uint8_t data = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_COMMAND_READ_MEMORY_LOCATION,
		NULL,
		&data
	);

	if (bus != NULL) {
		*bus = status;
	}

	return data;
}

/*!
	\brief 		Sends a command to Query the device failure status
*/
dali_driver_failure_status_t DaliAL_queryFailureStatus(uint8_t address, dali_device_type_t device_type, dali_bus_status_t *bus) {
	dali_driver_failure_status_t failure_status = DALI_DRIVER_FAILURE_STATUS_NONE;

	dali_bus_status_t bus_status = DALI_BUS_STATUS_INVALID;

	do {
		DaliAL_enableDeviceType(device_type, &bus_status);
		if (bus_status != DALI_BUS_STATUS_OK) {
			break;
		}

		uint8_t raw = 0;

		bus_status = DaliAL_command_tx_short(
			address,
			DALI_QUERY_FAILURE_STATUS,
			NULL,
			&raw
		);

		if (bus_status == DALI_BUS_STATUS_OK) {
			failure_status = raw;
		}
	} while (0);

	if (bus != NULL) {
		*bus = bus_status;
	}

	return failure_status;
}

/*!
	\brief 		Sends a command to Query the number of instances
*/
uint8_t DaliAL_queryNumberOfInstances(uint8_t address, dali_bus_status_t *bus) {
	uint8_t instances = 0;

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	status = DaliAL_command_tx_short(address, DALI_QUERY_NUMBER_INSTANCES, NULL, &instances);

	if (bus != NULL) {
		*bus = status;
	}

	return instances;
}

/*!
	\brief 		Sends a command to Query the instance type
*/
dali_instance_type_t DaliAL_queryInstanceType(uint8_t address, uint8_t instance, dali_bus_status_t *bus) {
	dali_instance_type_t instance_type = DALI_INSTANCE_TYPE_GENERIC;

	uint8_t raw = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(address, DALI_QUERY_INSTANCE_TYPE, NULL, &raw);

	if (status == DALI_BUS_STATUS_OK) {
		instance_type = raw;
	}

	if (bus != NULL) {
		*bus = status;
	}

	return instance_type;
}

/*!
	\brief 		Sends a command to set the instance event filter
*/
void DaliAL_setEventFilter(uint8_t instance, uint32_t event_filter, dali_bus_status_t *bus) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	do {
		if (event_filter > MAX_EVENT_FILTER) {
			break;
		}

		uint8_t event_filter_bytes[3] = {
			[0] = (event_filter >> 0) & 0xFF,
			[1] = (event_filter >> 8) & 0xFF,
			[2] = (event_filter >> 16) & 0xFF,
		};

		DaliAL_dataTransferRegister0(event_filter_bytes[0], &status);
		if (status != DALI_BUS_STATUS_OK) {
			break;
		}

		DaliAL_dataTransferRegister1(event_filter_bytes[1], &status);
		if (status != DALI_BUS_STATUS_OK) {
			break;
		}

		DaliAL_dataTransferRegister2(event_filter_bytes[2], &status);
		if (status != DALI_BUS_STATUS_OK) {
			break;
		}

		status = DaliAL_command_tx_broadcast(DALI_COMMAND_SET_EVENT_FILTER, NULL);
	} while (0);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to disable an instance
*/
void DaliAL_disableInstance(uint8_t instance, dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_broadcast(DALI_COMMAND_DISABLE_INSTANCE, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to Query the input value
*/
uint8_t DaliAL_queryInputValue(uint8_t address, uint8_t instance, dali_bus_status_t *bus) {
	uint8_t input_value = 0;

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	status = DaliAL_command_tx_short(address, DALI_QUERY_INPUT_VALUE, NULL, &input_value);

	if (bus != NULL) {
		*bus = status;
	}

	return input_value;
}

/*!
	\brief 		Sends a command to Query the latched input value
*/
uint8_t DaliAL_queryInputValueLatch(uint8_t address, uint8_t instance, dali_bus_status_t *bus) {
	uint8_t input_value = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_INPUT_VALUE_LATCH,
		NULL,
		&input_value
	);

	if (bus != NULL) {
		*bus = status;
	}

	return input_value;
}

/*!
	\brief 		Sends a command to enable the application controller
*/
void DaliAL_enableApplicationController(dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_broadcast(
		DALI_COMMAND_ENABLE_APP_CONTROLLER,
		NULL
	);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to disable the application controller
*/
void DaliAL_disableApplicationController(dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_broadcast(
		DALI_COMMAND_DISABLE_APP_CONTROLLER,
		NULL
	);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to query the application controller enabled status
*/
bool DaliAL_queryApplicationControllerEnabled(uint8_t address, dali_bus_status_t *bus) {
	bool is_enabled = false;

	uint8_t ret = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_APP_CONTROLLER_ENABLED,
		NULL,
		&ret
	);

	if (status == DALI_BUS_STATUS_OK) {
		is_enabled = (ret == DALI_MASK);
	}
	else if (status == DALI_BUS_STATUS_TIMEOUT) {
		is_enabled = false;
	}

	if (bus != NULL) {
		*bus = status;
	}

	return is_enabled;
}

/*!
	\brief 		Sends a command to start the quiescent mode
*/
void DaliAL_startQuiescentMode(dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_broadcast(DALI_COMMAND_START_QUIESCENT_MODE, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to stop the quiescent mode
*/
void DaliAL_stopQuiescentMode(dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_broadcast(DALI_COMMAND_STOP_QUIESCENT_MODE, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Sends a command to query the quiescent mode
*/
dali_quiescence_mode_t DaliAL_queryQuiescentMode(uint8_t address, dali_bus_status_t *bus) {
	dali_quiescence_mode_t quiescence_mode = DALI_QUIESCENCE_MODE_UNKNOWN;

	uint8_t raw = 0;

	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_QUIESCENT_MODE,
		NULL,
		&raw
	);

	if (status == DALI_BUS_STATUS_OK) {
		quiescence_mode = raw;
	}

	if (bus != NULL) {
		*bus = status;
	}

	return quiescence_mode;
}

/*!
	\brief 		Preload a full 24-bit DALI address for searching
*/
void DaliAL_setSearchAddress(uint32_t address, dali_bus_status_t *bus) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	if (address > DALI_MAX_ADDRESS_LONG) {
		address = DALI_MAX_ADDRESS_LONG;
	}

	uint8_t address_bytes[3] = {
		[2] = (address >> 16) & 0xFF,
		[1] = (address >>  8) & 0xFF,
		[0] = (address >>  0) & 0xFF
	};

	do {
		status = DaliAL_command_tx_special(DALI_COMMAND_SEARCHADDRH, &address_bytes[2], NULL);
		if (status != DALI_BUS_STATUS_OK) {
			break;
		}

		status = DaliAL_command_tx_special(DALI_COMMAND_SEARCHADDRM, &address_bytes[1], NULL);
		if (status != DALI_BUS_STATUS_OK) {
			break;
		}

		status = DaliAL_command_tx_special(DALI_COMMAND_SEARCHADDRL, &address_bytes[0], NULL);
	} while (0);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Randomise all connected 24-bit DALI addresses
*/
void DaliAL_randomise(dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_special(DALI_COMMAND_RANDOMISE, NULL, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Trigger the device initialisation state
*/
void DaliAL_initialise(uint8_t device, dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_special(DALI_COMMAND_INITIALISE, &device, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

/*!
	\brief 		Compare the search address to all devices real address

	\return 	True if any connected device is less-than-or-equal to the stored value
				of the device-local parameter 'searchAddress'.
*/
bool DaliAL_compare(dali_bus_status_t *bus) {
	bool match = false;

	uint8_t ret = 0;
	dali_bus_status_t status = DaliAL_command_tx_special(DALI_COMMAND_COMPARE, NULL, &ret);

	if (status == DALI_BUS_STATUS_OK) {
		match = (ret == DALI_MASK);
	}
	else if (status == DALI_BUS_STATUS_TIMEOUT) {
		match = false;
	}

	if (bus != NULL) {
		*bus = status;
	}

	return match;
}

void DaliAL_withdraw(dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_special(DALI_COMMAND_WITHDRAW, NULL, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

void DaliAL_terminate(dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_special(DALI_COMMAND_TERMINATE, NULL, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

void DaliAL_programShortAddress(uint8_t address, dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_special(
		DALI_COMMAND_PROGRAM_SHORT_ADDRESS,
		&address,
		NULL
	);

	if (bus != NULL) {
		*bus = status;
	}
}

bool DaliAL_queryControlGearPresent(uint8_t address, dali_bus_status_t *bus) {
	bool is_present = false;

	uint8_t raw = 0;
	dali_bus_status_t status = DaliAL_command_tx_short(
		address,
		DALI_QUERY_CONTROL_GEAR_PRESENT,
		NULL,
		&raw
	);

	if (status == DALI_BUS_STATUS_OK) {
		is_present = (raw == DALI_MASK);
	}
	else if (status == DALI_BUS_STATUS_TIMEOUT) {
		is_present = false;
	}

	if (bus != NULL) {
		*bus = status;
	}

	return is_present;
}

/*!
	\brief 		Verify the existence of a device at the given address
	\return 	The number of devices at the given address
*/
uint8_t DaliAL_verifyShortAddress(uint8_t address, dali_bus_status_t *bus) {
	uint8_t count = false;

	uint8_t raw = 0;

	dali_bus_status_t status = DaliAL_command_tx_special(
		DALI_COMMAND_VERIFY_SHORT_ADDRESS,
		&address,
		&raw
	);

	if (status == DALI_BUS_STATUS_OK) {
		count = (raw == DALI_MASK) ? 1 : 0;
	}
	else if (status == DALI_BUS_STATUS_TIMEOUT) {
		count = 0;
	}
	else if (status == DALI_BUS_STATUS_CORRUPT) {
		count = 2;
	}

	if (bus != NULL) {
		*bus = status;
	}

	return count;
}

void DaliAL_setShortAddress(uint8_t address, uint8_t new_address, dali_bus_status_t *bus) {
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	if (address == DALI_ADDRESS_BROADCAST) {
		status = DaliAL_command_tx_broadcast(
			DALI_COMMAND_SET_SHORT_ADDRESS,
			&new_address
		);
	}
	else {
		status = DaliAL_command_tx_short(
			address,
			DALI_COMMAND_SET_SHORT_ADDRESS,
			&new_address,
			NULL
		);
	}

	if (bus != NULL) {
		*bus = status;
	}
}

void DaliAL_onAndStepUp(uint8_t address, dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_short(address, DALI_COMMAND_ON_AND_STEP_UP, NULL, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}

void DaliAL_off(uint8_t address, dali_bus_status_t *bus) {
	dali_bus_status_t status = DaliAL_command_tx_short(address, DALI_COMMAND_OFF, NULL, NULL);

	if (bus != NULL) {
		*bus = status;
	}
}
