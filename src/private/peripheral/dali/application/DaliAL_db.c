/*!
	\file 		DaliAL_db.c
	\author 	s.arthur
	\date 		04/08/2021
	\copyright 	Lucy Zodion Ltd.
*/

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "DaliAL_db.h"

/*
	DALI specification messages. 16-bit forward frames.
	Derived from [BSI 62386-102:2014], [BSI 62386-107:2014]
*/
static const struct dali_msg dali_command_16[DALI_COMMAND_MAX] = {
	[DALI_QUERY_ACTUAL_LEVEL] = {
		.opcode 		= 0xA0,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_CONTROL_GEAR_PRESENT] = {
		.opcode 		= 0x91,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_DEVICE_TYPE] = {
		.opcode 		= 0x99,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_DIMMING_CURVE] = {
		.opcode 		= 0xEE,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_DRIVER_VERSION_NUMBER] = {
		.opcode 		= 0x97,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_EXTENDED_VERSION_NUMBER] = {
		.opcode 		= 0xFF,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_FAILURE_STATUS] = {
		.opcode 		= 0xF1,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_LIGHT_SOURCE_TYPE] = {
		.opcode 		= 0x9F,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= true,
				[1] 			= true,
				[2] 			= true
			}
		}
	},
	[DALI_QUERY_NEXT_DEVICE_TYPE] = {
		.opcode 		= 0xA7,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_PHYSICAL_MINIMUM] = {
		.opcode 		= 0x9A,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_STATUS] = {
		.opcode 		= 0x90,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},

	[DALI_COMMAND_COMPARE] = {
		.address 		= 0xA9,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_DIRECT_ARC_POWER_CONTROL] = {
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= true,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_DTR0] = {
		.address 		= 0xA3,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= true,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_DTR1] = {
		.address 		= 0xC3,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= true,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_DTR2] = {
		.address 		= 0xC5,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= true
			}
		}
	},
	[DALI_COMMAND_ENABLE_DEVICE_TYPE] = {
		.address 		= 0xC1,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_ENABLE_WRITE_MEMORY] = {
		.opcode 		= 0x81,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= true,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_INITIALISE] = {
		.address 		= 0xA5,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= true,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_OFF] = {
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_ON_AND_STEP_UP] = {
		.opcode 		= 0x08,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_PING] = {
		.address 		= 0xAD,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_PROGRAM_SHORT_ADDRESS] = {
		.address 		= 0xB7,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_RANDOMISE] = {
		.address 		= 0xA7,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= true,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_READ_MEMORY_LOCATION] = {
		.opcode 		= 0xC5,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= true,
				[1] 			= true,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_RESET] = {
		.opcode 		= 0x20,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= true,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_SEARCHADDRH] = {
		.address 		= 0xB1,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_SEARCHADDRM] = {
		.address 		= 0xB3,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_SEARCHADDRL] = {
		.address 		= 0xB5,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_SELECT_DIMMING_CURVE] = {
		.opcode 		= 0xE3,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_SET_FADE_TIME] = {
		.opcode 		= 0x2E,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= true,
			.selector 		= false,
			.dtr = {
				[0] 			= true,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_SET_SHORT_ADDRESS] = {
		.opcode 		= 0x80,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= true,
			.selector 		= false,
			.dtr = {
				[0] 			= true,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_TERMINATE] = {
		.address 		= 0xA1,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_VERIFY_SHORT_ADDRESS] = {
		.address 		= 0xB9,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_WITHDRAW] = {
		.address 		= 0xAB,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_WRITE_MEMORY_LOCATION_NOREPLY] = {
		.address 		= 0xC9,
		.opcode 		= 0x00,
		.size 			= DALI_PACKET_FF16,
		.flags = {
			.has_response 	= false,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= true,
				[1] 			= true,
				[2] 			= false
			}
		}
	}
};

/*
	DALI specification messages. 24-bit forward frames.
	Derived from [BSI 62386-103:2014]
*/
static const struct dali_msg dali_command_24[DALI_COMMAND_MAX] = {
	[DALI_QUERY_APP_CONTROLLER_ENABLED] = {
		.instance 		= 0xFE,
		.opcode 		= 0x3D,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_INPUT_VALUE] = {
		.instance 		= 0xFE,
		.opcode 		= 0x8C,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_INPUT_VALUE_LATCH] = {
		.instance 		= 0xFE,
		.opcode 		= 0x8D,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_INSTANCE_TYPE] = {
		.instance 		= 0xFE,
		.opcode 		= 0x80,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_NUMBER_INSTANCES] = {
		.instance 		= 0xFE,
		.opcode 		= 0x35,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_QUIESCENT_MODE] = {
		.instance 		= 0xFE,
		.opcode 		= 0x40,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_QUERY_SENSOR_VERSION_NUMBER] = {
		.instance 		= 0xFE,
		.opcode 		= 0x34,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= true,
			.send_twice 	= false,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},

	[DALI_COMMAND_DISABLE_APP_CONTROLLER] = {
		.instance 		= 0xFE,
		.opcode 		= 0x17,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= false,
			.send_twice 	= true,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_DISABLE_INSTANCE] = {
		.instance 		= 0xFE,
		.opcode 		= 0x63,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= false,
			.send_twice 	= true,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_ENABLE_APP_CONTROLLER] = {
		.instance 		= 0xFE,
		.opcode 		= 0x16,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= false,
			.send_twice 	= true,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_SET_EVENT_FILTER] = {
		.instance 		= 0xFE,
		.opcode 		= 0x68,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= false,
			.send_twice 	= true,
			.selector 		= false,
			.dtr = {
				[0] 			= true,
				[1] 			= true,
				[2] 			= true
			}
		}
	},
	[DALI_COMMAND_START_QUIESCENT_MODE] = {
		.instance 		= 0xFE,
		.opcode 		= 0x1D,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= false,
			.send_twice 	= true,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	},
	[DALI_COMMAND_STOP_QUIESCENT_MODE] = {
		.instance 		= 0xFE,
		.opcode 		= 0x1E,
		.size 			= DALI_PACKET_FF24,
		.flags = {
			.has_response 	= false,
			.send_twice 	= true,
			.selector 		= false,
			.dtr = {
				[0] 			= false,
				[1] 			= false,
				[2] 			= false
			}
		}
	}
};

/*!
	\brief 		Fetch a known DALI command message template from the database
*/
bool DaliAL_db_fetch_template(enum dali_command command, const struct dali_msg **msg, uint16_t opt) {
	bool ret = false;
	const struct dali_msg *ptr = NULL;

	do {
		if (command <= DALI_COMMAND_NONE || command >= DALI_COMMAND_MAX) {
			break;
		}

		ptr = &dali_command_16[command];

		if (ptr->size != DALI_PACKET_NONE) {
			break;
		}

		ptr = &dali_command_24[command];

		if (ptr->size != DALI_PACKET_NONE) {
			break;
		}
	} while (0);

	if (ptr != NULL) {
		if (msg != NULL) {
			*msg = ptr;
		}

		ret = true;
	}

	return ret;
}

/*!
	\brief 		Validate internal database integrity
*/
bool DaliAL_db_test_integrity(void) {
	bool ret = true;

	// No entries in database
	if (DALI_COMMAND_MAX <= DALI_COMMAND_NONE) {
		ret = false;
	}

	for (enum dali_command i = DALI_COMMAND_NONE; i < DALI_COMMAND_MAX; i++) {
		// Cross-contamination of frames sizes
		if (dali_command_16[i].size == DALI_PACKET_FF24) {
			ret = false;
			break;
		}
		if (dali_command_24[i].size == DALI_PACKET_FF16) {
			ret = false;
			break;
		}
	}

	return ret;
}
