/*
==========================================================================
 Name        : otot.c
 Project     : pcore
 Path        : /pcore/src/private/peripheral/luminr/otot.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 16 Apr 2018
 Description : 
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "otot.h"
#include "hw.h"
#include "config_an_driver.h"
#include "sysmon.h"
#include "common.h"
#include "debug.h"
#include "adc_procx.h"


/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static float target_ana_volt = 10;
static float brightness_to_volt_coeff = 0.09;
static float volt_to_dc_coeff = 11.11;
static float an_op_min_vol = 1;
static float corr_coef = 0.5;
static uint8_t last_dc_entry = 100;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

void otot_service(void){
	int8_t dc_offset = 0;

	// calculate error in measured volts
	float error = target_ana_volt - get_adc_procx_pwm_fb_av_volt();
	// calculate DC offset required in %
	if(error < 0){
		error = - error;
		dc_offset = (uint8_t)(error * corr_coef * volt_to_dc_coeff);
//		debug_printf(LUMINR, "DC corr = %d - %d %%\r\n", last_dc_entry, dc_offset);
		last_dc_entry = (last_dc_entry > dc_offset) ?
						last_dc_entry - dc_offset :
						0;
	}else{
		dc_offset = (uint8_t)(error * corr_coef * volt_to_dc_coeff);
//		debug_printf(LUMINR, "DC corr = %d + %d %%\r\n", last_dc_entry, dc_offset);
		last_dc_entry = last_dc_entry + dc_offset;
		last_dc_entry = (last_dc_entry > 100) ?
						100 :
						last_dc_entry;
	}

	an_driver_set_pwm_dc(last_dc_entry);
	return;
}

bool set_otot_lamp_output(uint8_t level){
	//	an_driver_set_pwm_dc(level);
		target_ana_volt = (float)(level * brightness_to_volt_coeff + an_op_min_vol);
		//last_dc_entry = (uint8_t)(target_ana_volt * volt_to_dc_coeff);
		//an_driver_set_pwm_dc(last_dc_entry);
		debug_printf(LUMINR, "Update: BR %d %% | Van = %lu V\r\n",
				level,
				(uint32_t)(100*target_ana_volt));
		return true;
}
