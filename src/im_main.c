/*
==========================================================================
 Name        : im_main.c
 Project     : pcore
 Path        : /pcore/src/im_main.c
 Author      : Ben Raiton
 Copyright   : Lucy Zodion Ltd
 Created     : 23 May 2018
 Description :
==========================================================================
*/

/*****************************************************************************
 * Include files / definitions
 ****************************************************************************/
#include "chip.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "FreeRTOSMacros.h"
#include "FreeRTOSCommonHooks.h"
#include "debug.h"
#include "ff.h"
#include "config_queues.h"
#include "config_tasks.h"
#include "diskio.h"
#include "flash.h"
#include "hw.h"
#include "led.h"
#include "nwk_com.h"
#include "common.h"
#include "systick.h"
#include "luminrop.h"
#include "msg_rx.h"
#include "patch_rx.h"
#include "patch_process.h"
#include "sysmon.h"
#include "config_timers.h"
#include "calendar.h"
#include "Tilt.h"
#include "config_lorawan.h"
#include "file_sys.h"
#include "solar_clock.h"
#include "config_fatfs.h"
#include "nwk_handler.h"
#include "msg_handler.h"
#include "productionMonitor.h"
#include "photo.h"
#include "powerMonitor.h"
#include "supplyMonitor.h"
#include "config_eeprom.h"
#include "NonVolatileMemory.h"
#include "NVMData.h"
// temp
#include "luminr_tek.h"
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/
static TaskHandle_t vTaskLuminrOp_Handle;
static TaskHandle_t vTaskCalendar_Handle;
static TaskHandle_t vTaskPatching_Handle;
static TaskHandle_t vTaskNwkCom_Handle;
static TaskHandle_t vTaskSensorLfp_Handle;
static TaskHandle_t vTaskSysMon_Handle;
static TaskHandle_t vTaskDebug_Handle;

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/
// extern queue handles
extern QueueHandle_t xQuDebug; 		// vTaskDebug
extern QueueHandle_t XQuPatchData; 	// vTaskPatching
extern QueueHandle_t xQuNwkCom; // vTaskNwkCom
extern QueueHandle_t xQuLuminrOp; 	// vTaskLuminrOp

// timer handles
TimerHandle_t xTimer[NUM_TIMERS];

/*****************************************************************************
 * Private functions
 ****************************************************************************/

// RTOS Tasks Objects
static void vTaskLuminrOp(void *pvParameters);
static void vTaskCalendar(void *pvParameters);
static void vTaskPatching(void *pvParameters);
static void vTaskNwkCom(void *pvParameters);
static void vTaskSensorLfp(void *pvParameters);
static void vTaskSysMon(void *pvParameters);
static void vTaskDebug(void *pvParameters);


// RTOS generate functions
static void xTaskCreateWithDebug(   TaskFunction_t pxTaskCode,
									const char * const pcName,
									const uint16_t usStackDepth,
									void * const pvParameters,
									UBaseType_t uxPriority,
									TaskHandle_t * const pxCreatedTask);

static QueueHandle_t xQueueCreateWithDebug( const UBaseType_t uxQueueLength,
                                            const UBaseType_t uxItemSize,
                                            const char * p_queue_name);

static void create_status_uplink_timer(void);


// RTOS Timers
typedef struct{
    uint32_t timer_id;
    uint32_t period;
    bool b_auto_reload;
    void (*func)(void);
}timer_info_struct;

static timer_info_struct timer_info[]=
{
    {BCAST_TIME_OUT, BCAST_TIME_OUT_MS, pdFALSE, vBroadcastTimeOutCallBack},
    {STATUS_UPLINK,  0,                 pdTRUE,  vStatusUplinkCallBack}
};

/*****************************************************************************
 * Public functions
 ****************************************************************************/

void vTimerCallback( TimerHandle_t pxTimer )
{
    uint32_t timer_id = 0;
     // TODO: Optionally do something if the pxTimer parameter is NULL.
     //configASSERT( pxTimer );

     // Call call-back-function attached to triggering timer.
    timer_id = ( int32_t ) pvTimerGetTimerID( pxTimer );
    debug_printf(MAIN, "Timer ID = %lu\r\n", timer_id);

    (*timer_info[timer_id].func)();
}

void image_main_new(void){

    hw_kick_watchdog();
    led_on(GREEN);
    led_off(RED);

    debug_print_instant(MAIN, "\r\n");
    debug_print_instant(MAIN, "\r\n");
    debug_print_instant(MAIN, "************************************************\r\n");

    //-------------------------------------
    // Production Boot Up Sequence Start
    productionMonitor_checkIfUnitIsUnderTest();

    if(productionMonitor_getUnitIsUndertest())
    {
        NVMData_restoreAll();
    }
    else
    {
        // read and validate structures in EEprom
        NVMData_initialise();
    }
    //-------------------------------------


    hw_kick_watchdog();
    led_on(GREEN);
    led_off(RED);

    debug_print_instant( MAIN, "Device: %s\r\n", DEV_SER);
    debug_print_instant( MAIN, "Build Code: %s\r\n", BUILD_CODE);
    debug_print_instant( MAIN, "Image:\r\n");
    debug_print_instant( MAIN, "%s\r\n", FW_VERSION_MAJOR);
    debug_print_instant( MAIN, "%s\r\n", FW_VERSION_MINOR);
    debug_print_instant( MAIN, "%s\r\n", FW_VERSION_TEST);

    rtc_procx_SetDstIsApplicable(true);


    // Attempt to mount the fat drive
    bool driveMountedOk = file_sys_mount_fatfs_drive();

    debug_print_instant(    MAIN,
                            "Config Total Heap Size: %lu B.\r\n",
                            (uint32_t)configTOTAL_HEAP_SIZE);

    debug_print_instant(    MAIN,
                            "RTOS available heap size: %lu B.\r\n",
                            (uint32_t)xPortGetFreeHeapSize());

    //------------------------------------------------------
    //                  CREATE RTOS TOMERS
    //------------------------------------------------------

    // Create Broadcast Session Time Out
    xTimer[BCAST_TIME_OUT] = xTimerCreate(  "Timer",
                                            timer_info[BCAST_TIME_OUT].period,
                                            timer_info[BCAST_TIME_OUT].b_auto_reload,
                                            ( void * ) BCAST_TIME_OUT,
                                            vTimerCallback);

    create_status_uplink_timer();


    debug_print_instant(    MAIN,
                            "Timer task created. Unused Heap %lu B.\r\n",
                            (uint32_t)xPortGetFreeHeapSize());

	//------------------------------------------------------
	// 					CREATE RTOS QUEUES
	//------------------------------------------------------

	// Create Debug Queue
	xQuDebug = xQueueCreateWithDebug( 	QU_DEPTH_DEBUG,
										sizeof(QuDebugMsg),
										"xQuDebug");

	// Create Patch Data Queue
	XQuPatchData = xQueueCreateWithDebug( 	QU_DEPTH_PATCH_DATA,
										    sizeof(QuPatchDataMsg),
										    "xQuPatchData");

	// Create Tx Msg Queue
	xQuNwkCom = xQueueCreateWithDebug( 	QU_DEPTH_NWK_COM,
										sizeof(QuNwkComMsg),
										"xQuNwkCom");
    // Create Luminr Op Queue
	xQuLuminrOp = xQueueCreateWithDebug(    QU_DEPTH_LUMINR_OP,
                                            sizeof(QuLuminrOpMsg),
                                            "xQuLuminrOpMsg");

	// Print out string showing remaining free RTOS heap size
    debug_print_instant(    MAIN,
                            "Queues created. Unused Heap %lu B.\r\n",
                            (uint32_t)xPortGetFreeHeapSize());

    hw_kick_watchdog();

	//------------------------------------------------------
	// 					CREATE RTOS TASKS
	//------------------------------------------------------

    // Create Task LuminrOp
    xTaskCreateWithDebug(   vTaskLuminrOp,
                            (char *)"A_LUMIN",
                            STACKSIZE_LUMINROP,
                            NULL,
                            TASK_PRIORITY_LUMINROP,
                            &vTaskLuminrOp_Handle);
if(driveMountedOk)
{
    // Create Task Calendar
    xTaskCreateWithDebug(   vTaskCalendar,
                            (char *)"B_CAL",
                            STACKSIZE_CALENDAR,
                            NULL,
                            TASK_PRIORITY_CALENDAR,
                            &vTaskCalendar_Handle);

    // Create Task Patch Rx
    xTaskCreateWithDebug(   vTaskPatching,
                            (char *)"C_P_RX",
                            STACKSIZE_PATCHING,
                            NULL,
                            TASK_PRIORITY_PATCHING,
                            &vTaskPatching_Handle);
}
    // Create Task Nwk Com
    xTaskCreateWithDebug(   vTaskNwkCom,
                            (char *)"D_NWK",
                            STACKSIZE_NWK_COM,
                            NULL,
                            TASK_PRIORITY_NWK_COM,
                            &vTaskNwkCom_Handle);

    // Create Task Power Calc
    xTaskCreateWithDebug(   vTaskSensorLfp,
                            (char *)"E_PWR",
                            STACKSIZE_SENSOR_LFP,
                            NULL,
                            TASK_PRIORITY_SENSOR_LFP,
                            &vTaskSensorLfp_Handle);

    // Create Task Debug
    xTaskCreateWithDebug(   vTaskDebug,
                            (char *)"F_DEBG",
                            STACKSIZE_DEBUG,
                            NULL,
                            TASK_PRIORITY_DEBUG,
                            &vTaskDebug_Handle);

    // Create Task Sys Mon
    xTaskCreateWithDebug(   vTaskSysMon,
                            (char *)"G_SYSM",
                            STACKSIZE_SYS_MON,
                            NULL,
                            TASK_PRIORITY_SYS_MON,
                            &vTaskSysMon_Handle);

    // Print out string showing remaining free RTOS heap size
    debug_print_instant(    MAIN,
                            "Tasks created. Unused Heap %lu B.\r\n",
                            (uint32_t)xPortGetFreeHeapSize());

    debug_print_instant(    MAIN,
                            "Idle Task =  %lu B.\r\n",
                            (uint32_t)configMINIMAL_STACK_SIZE*4);

    debug_print_instant(    MAIN,
                            "Timer Task =  %lu B.\r\n",
                            (uint32_t)configTIMER_TASK_STACK_DEPTH*4);

    debug_print_instant(    MAIN,
                            "Scheduler =  236 B.\r\n");

    // Print out string showing final free RTOS heap size
    debug_print_instant(    MAIN,
                            "Forecast Unallocated Heap = %ld\r\n",
                            (uint32_t)xPortGetFreeHeapSize()-236-
                                    (configMINIMAL_STACK_SIZE*4)-
                                    (configTIMER_TASK_STACK_DEPTH*4));

    hw_kick_watchdog();

	//------------------------------------------------------
	// 						START RTOS
	//------------------------------------------------------

    vTaskStartScheduler();

    // SHould never reach this point!!
    for(;;);
    return;

}


/*******************************************************************************
 * @brief sets the timer period in the timer_info table
 *
 * @param[in] uint32_t - timer id
 *            uint32_t - timer period in ms
 *
 * @return void
 *
 *******************************************************************************/
void im_main_set_timer_period( uint32_t timer_id, uint32_t timer_period_ms)
{
    // range check the timer id
    if( timer_id < RTOS_TIMERS_MAX) {

        timer_info[timer_id].period = timer_period_ms;
        // Change the timer period
        xTimerChangePeriod(xTimer[timer_id], (TickType_t)timer_period_ms, CHANGE_TIMER_BLOCK_TICKS);
    }
}

/*******************************************************************************
 * @brief returns the status uplink timer period in the timer_info table
 *
 * @param[in]  uint32_t - timer id
 *
 * @return     uint32_t - status period in ms
 *
 *******************************************************************************/
uint32_t im_main_get_timer_period( uint32_t timer_id)
{
    // range check the timer id
    if( timer_id < RTOS_TIMERS_MAX) {

        return timer_info[timer_id].period;
    }

    // when out of range timer id return zero
    return 0;
}



/*******************************************************************************
 * @brief TODO:
 * @param[in] void
 * @return void
 *******************************************************************************/
static void vTaskLuminrOp(void *pvParameters) {
    static TickType_t xLastWakeTime;
    static QuLuminrOpMsg luminr_op_msg;

    if( productionMonitor_getUnitIsUndertest() == true)
    {
    //----------------------------------------------------------
    // Use this task to run the productionMonitor
    //----------------------------------------------------------
        xLastWakeTime = xTaskGetTickCount();
        for (;;)
        {
            vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(100));
            productionMonitor_service();
        }
    //----------------------------------------------------------
    }
    else
    {
    //----------------------------------------------------------
        // Initialise Luminr interface
        sysmon_set_task_exe_timeout(TASK_LUMINR);
        luminrop_init();
        sysmon_disable_task_exe_timeout(TASK_LUMINR);

        xLastWakeTime = xTaskGetTickCount();
        for (;;)
        {
            sysmon_disable_task_exe_timeout(TASK_LUMINR);
            // Need to service the luminr stack every 100 ms
            vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(100));
            sysmon_set_task_exe_timeout(TASK_LUMINR);
            
            // check if new lamp output level msg waiting
            if ( xQueueReceive( xQuLuminrOp, &luminr_op_msg, 0 ) == pdTRUE)
            {
                luminrop_process_msg( luminr_op_msg );
            } 
            else
            {
            }

            luminrop_service_stack();
        
        }
    //----------------------------------------------------------
    }
}

/*******************************************************************************
 * @brief TODO:
 * @param[in] void
 * @return void
 *******************************************************************************/
static void vTaskCalendar(void *pvParameters) {
    static TickType_t xLastWakeTime;
    bool bGetSolar = false;

    xLastWakeTime = xTaskGetTickCount();
    for (;;)
    {
        sysmon_disable_task_exe_timeout(TASK_CAL);
        vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(100));
        sysmon_set_task_exe_timeout(TASK_CAL);

        /* Only allow Calendar to control the Lamp Output if the Luminar init is complete */
        if ( true == get_luminrop_ready_to_use() )
        {
			bGetSolar = cal_get_calc_solar_clock();

			/* Shall we get the New Solar Clock Calc for the day */
			if ( true == bGetSolar )
			{
				/* Calculate the Sunset and Sunrise Time */
				solar_main();

				/* Clear calc Solar Clock flag */
				cal_set_calc_solar_clock(false);
			}
			else
			{
				/* Run the Main Scheduling task */
				calendar_check_sp();
			}
        }
    }
}

/*******************************************************************************
 * @brief TODO:
 * @param[in] void
 * @return void
 *******************************************************************************/
static void vTaskPatching(void *pvParameters) {
    static QuPatchDataMsg rx_data_msg;
    static bool b_patch_pro_ok = false;

    // Validate Firmware Update: Delete new image file if present
    sysmon_set_task_exe_timeout(TASK_PATCHING);
    patch_process_validate_new_image_file();
    sysmon_disable_task_exe_timeout(TASK_PATCHING);

    for (;;)
    {
        sysmon_disable_task_exe_timeout(TASK_PATCHING);
        // BLOCK until message received on XQuPatchData
        if ( xQueueReceive( XQuPatchData, &rx_data_msg, portMAX_DELAY ) == pdTRUE) {
            sysmon_set_task_exe_timeout(TASK_PATCHING);
                if (patch_rx_process_msg(rx_data_msg) == false){
                    // if an error occured kill the download process
                    patch_rx_remote_kill();
                }
                if(true == get_patch_rx_patch_is_pending() ){
                    // Apply corresponding patch
                    debug_printf(MAIN, "Apply patch: %s\r\n", get_patch_rx_file_name());
                    b_patch_pro_ok = patch_process_exe(get_patch_rx_file_name());
                    // Execute corresponding termination sequence
                    if( strcmp( get_patch_rx_file_name(),
                                (const char *)FW_PATCH_FILE_NAME) == 0){
                        patch_process_finalise_fw_update(b_patch_pro_ok);
                    }else if(   strcmp( get_patch_rx_file_name(),
                                (const char *)CAL_PATCH_FILE_NAME) == 0){
                        patch_process_finalise_cal_update(b_patch_pro_ok);
                    }
                }
        }
    }
}

/*******************************************************************************
 * @brief TODO:
 * @param[in] void
 * @return void
 *******************************************************************************/
static void vTaskNwkCom(void *pvParameters) {
    static QuNwkComMsg nwk_com_msg;
    static TickType_t xLastWakeTime;

    msg_handler_init();

    xLastWakeTime = xTaskGetTickCount();

   for (;;)
   {
        sysmon_disable_task_exe_timeout(TASK_NWK_COM);
        // Wake up task every 100ms
        vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(100));
        sysmon_set_task_exe_timeout(TASK_NWK_COM);

        // Check if a msg is waiting on xQuNwkCom
        if ( xQueueReceive( xQuNwkCom, &nwk_com_msg, 0 ) == pdTRUE){
            msg_handler_load(nwk_com_msg);
        }else{}

        // Service Msg Handler SM
        msg_handler_run();

        // Service Network Handler SM
        nwk_handler_run();

   }

}

/*******************************************************************************
 * @brief TODO:
 * @param[in] void
 * @return void
 *******************************************************************************/
static void vTaskSensorLfp(void *pvParameters)
{
    static TickType_t xLastWakeTime;
    static char g_s[64];

    //--------------------------------
    static Tilt_status tilt_status = 0;
    static Tilt_status prev_tilt_status = 0;
    //--------------------------------

    if( supplyMonitor_check12VRail() != SUPPLY_IS_OK )
    {
        debug_printf(PROD,"12V line error\r\n");
        set_sysmon_fault(SUPPLY_12V_RAIL_ERROR);
    }
    else
    {
        debug_printf(PROD,"12V line ok\r\n");
        clear_sysmon_fault(SUPPLY_12V_RAIL_ERROR);
    }

    sysmon_set_task_exe_timeout(TASK_SENSOR_LFP);
    // Mark bluetooth as disconnected as stack not yet implemented
    set_sysmon_fault(BLUETOOTH_DISCONNECTED);

    Tilt_ctor(Tilt_getRef(), 0, g_s);

    // Init power monitor HW etc
    powerMonitor_init();

    // init the power monitor measurements and calcs
    powerMonitor_initMeasurement();

    photo_init();

    sysmon_disable_task_exe_timeout(TASK_SENSOR_LFP);

    // if under test launch the calibration
    // if detected the ADI ADE9153A -> this has auto calibration,
    // so must be called every time after power-on
    if(( productionMonitor_getUnitIsUndertest() == true) ||
       ( powerMonitor_getHWType() == POWER_MONITOR_HW_ADI_ADE9153A))
    {
        // this code blocks until calibration finishes or fails
        powerMonitor_calibrate();
    }

    xLastWakeTime = xTaskGetTickCount();

    for (;;)
    {
        sysmon_disable_task_exe_timeout(TASK_SENSOR_LFP);
        vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(100));
        sysmon_set_task_exe_timeout(TASK_SENSOR_LFP);
        // check id data is waiting to be processed
        powerMonitor_service();
        // Service photo sensor
        photo_service();

        tilt_status = Tilt_run(Tilt_getRef());

        if( prev_tilt_status != tilt_status)
        {
          prev_tilt_status = tilt_status;
          float tiltAngle = Tilt_getTilt(Tilt_getRef());
          debug_printf( SENSOR,
                        "Tilt Status Change: %d. Angle = %d\r\n",
                        (uint8_t)tilt_status,
                        (uint8_t)tiltAngle);
          // process new tilt status
          switch( tilt_status ){
          case TILT_STATUS_NULL:
                // TODO: ??
              break;
          case TILT_STATUS_OK:
                clear_sysmon_fault(ACCEL_NOT_RESPONDING);
              break;
          case TILT_STATUS_IMPACT_EVENT:
                set_sysmon_fault(COLUMN_NOT_VERTICAL);
              break;
          case TILT_STATUS_RECOVERY_EVENT:
              clear_sysmon_fault(COLUMN_NOT_VERTICAL);
              break;
          case TILT_STATUS_ERROR:
                set_sysmon_fault(ACCEL_NOT_RESPONDING);
              break;
          }
      }

    }
}

/*******************************************************************************
 * @brief Debug print out
 * @param[in] void
 * @return void
 *******************************************************************************/
static void vTaskDebug(void *pvParameters) {
    static QuDebugMsg debug_msg;

    for (;;)
    {
        sysmon_disable_task_exe_timeout(TASK_DEBUG);
        // BLOCK until message waiting in queue
        if ( xQueueReceive( xQuDebug, &debug_msg, portMAX_DELAY ) != pdTRUE) {
            // NOOP: wait for message
        } else {
            // Print oldest message sent
            sysmon_set_task_exe_timeout(TASK_DEBUG);
            debug_print_queue(debug_msg);
        }
    }
}

/*******************************************************************************
 * @brief TODO:
 * @param[in] void
 * @return void
 *******************************************************************************/
static void vTaskSysMon(void *pvParameters) {
    static TickType_t xLastWakeTime;

    xLastWakeTime = xTaskGetTickCount();
    for (;;)
    {
        vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(100));
        led_off(GREEN);

        sysmon_run();

        if( true == get_sysmon_last_gasp_is_active()){
            sysmon_check_last_gasp_timeout();
        }
        // check if any running task got stuck
        sysmon_check_tasks_watchdog();

    }
}

/*******************************************************************************
 * @brief Create a task with debugging information
 * @param[in] void
 * @return void
 *******************************************************************************/
static void xTaskCreateWithDebug(   TaskFunction_t pxTaskCode,
									const char * const pcName,
									const uint16_t usStackDepth,
									void * const pvParameters,
									UBaseType_t uxPriority,
									TaskHandle_t * const pxCreatedTask){

    char rtos_build_debug_str[75];
    BaseType_t task_create_res = pdPASS;

    snprintf(    rtos_build_debug_str, sizeof rtos_build_debug_str,
                "Creating task: %s (code %lu) (Stack: %lu).....",
                pcName,
                (uint32_t)pxTaskCode,
                (uint32_t)(usStackDepth*4));
    debug_print_instant(MAIN, rtos_build_debug_str);

    task_create_res = xTaskCreate(  pxTaskCode,
                                    pcName,
                                    usStackDepth,
                                    pvParameters,
                                    uxPriority,
                                    pxCreatedTask);

    // Check if task was created successfully
    if( pdPASS != task_create_res){
        debug_print_instant(MAIN, "failed!\r\n");
        while(1);
    }else{
        debug_print_instant(MAIN, "ok\r\n");
    }

    return;
}

/*******************************************************************************
 * @brief Create a queue with debugging information
 * @param[in] void
 * @return void
 *******************************************************************************/
static QueueHandle_t xQueueCreateWithDebug( const UBaseType_t uxQueueLength,
                                            const UBaseType_t uxItemSize,
                                            const char * p_queue_name){

    char rtos_build_debug_str[75];
    QueueHandle_t created_queue_handle = NULL;

    snprintf(    rtos_build_debug_str, sizeof rtos_build_debug_str,
                "Creating queue: %s (%lu B).....",
                p_queue_name,
                (uint32_t)(uxQueueLength*uxItemSize));
    debug_print_instant(MAIN, rtos_build_debug_str);

    created_queue_handle = xQueueCreate(uxQueueLength, uxItemSize);

    // Check if task was created successfully
    if( created_queue_handle == NULL){
        debug_print_instant(MAIN, "failed!\r\n");
        while(1);
    }else{
        debug_print_instant(MAIN, "ok\r\n");
    }

    return created_queue_handle;

}

/*******************************************************************************
 * @brief Creates the uplink status period timer using the status period
 *        stored in EEprom
 *
 * @param[in] void
 *
 * @return void
 *******************************************************************************/
static void create_status_uplink_timer(void)
{
    NVM_RuntimeParams_t  runtimeParams = { 0 };
    // read the runtime params which contains the status period
    NVMData_readRuntimeParams(&runtimeParams);

    // if failed to read or the value is invalid default it
    if( runtimeParams.status_period == 0)
    {
        runtimeParams.status_period = NVM_DEFAULT_STATUS_PERIOD;
    }

    // status period is in minutes so need to * 60 to get seconds * 1000 to get ms
    timer_info[STATUS_UPLINK].period = runtimeParams.status_period * (1000*60);

    debug_print_instant(MAIN, "Uplink timer status period: %d min\r\n",runtimeParams.status_period);

    xTimer[STATUS_UPLINK] = xTimerCreate( "Timer",
                                           timer_info[STATUS_UPLINK].period,
                                           timer_info[STATUS_UPLINK].b_auto_reload,
                                           (void *) STATUS_UPLINK,
                                           vTimerCallback);
}

