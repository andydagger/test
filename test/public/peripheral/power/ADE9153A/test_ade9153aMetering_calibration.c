#include "unity.h"
#include "ade9153aMetering_calibration.c"

#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_productionMonitor.h"
#include "mock_powerMonitor.h"
#include "mock_ade9153aMetering.h"
#include "mock_ade9153aMetering_impl.h"
#include "mock_ade9153aMetering_transport.h"
#include "mock_ade9153aMetering_dataProcess.h"
#include "mock_ade9153aMetering_calibDataProcess.h"
#include "mock_ade9153aMetering_calibStartStop.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// NULL pointer test
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrate_NullPointer()
{
    _DEBUG_OUT_TEST_NAME
    
    powerMonitor_Result result = ade9153aMetering_calibrate(NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// test for calibration Current channel timeout
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrate_CurrentChannelTimeout()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;
    
    ade9153a_calibrateCurrentChannel_ExpectAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);

    powerMonitor_Result result = ade9153aMetering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// test for calibration Voltage channel timeout
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrate_VoltageTimeout()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;

    ade9153a_calibrateCurrentChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);
    
    ade9153a_calibrateVoltageChannel_ExpectAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);

    powerMonitor_Result result = ade9153aMetering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// test for calibration - Read calibration register timeout
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrateReadCalRegs_Timeout()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;

    ade9153a_calibrateCurrentChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calibrateVoltageChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_readAutoCalRegisters_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);

    powerMonitor_Result result = ade9153aMetering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// test for calibration - Apply Calib Gains timeout
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrate_ApplyGainsTimeout()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;

    ade9153a_calibrateCurrentChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calibrateVoltageChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_readAutoCalRegisters_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    
    ade9153a_applyCalibrationGains_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);

    powerMonitor_Result result = ade9153aMetering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// test for calibration - OK - under test
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrate_UnderTestOK()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;

    ade9153a_calibrateCurrentChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calibrateVoltageChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_readAutoCalRegisters_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_applyCalibrationGains_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);

    ade9153a_calibrationReadValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    powerMonitor_Result result = ade9153aMetering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// test for calibration - OK - not under test
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrate_NotUnderTestOK()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;

    ade9153a_calibrateCurrentChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calibrateVoltageChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_readAutoCalRegisters_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_applyCalibrationGains_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    productionMonitor_getUnitIsUndertest_ExpectAndReturn(false);

    powerMonitor_Result result = ade9153aMetering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}


/*------------------------------ End of File -------------------------------*/

