#include "unity.h"
#include "ade9153aMetering.c"
#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_powerMonitor.h"
#include "mock_ade9153aMetering_impl.h"
#include "mock_ade9153aMetering_transport.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_ade9153aMetering_init()
// Test flag and last sample time being initialised
//-----------------------------------------------------------------
void test_ade9153aMetering_init()
{
    _DEBUG_OUT_TEST_NAME

    get_systick_sec_ExpectAndReturn(0x100);

    ade9153a_init_Expect();
    
    ade9153a_transportInit_Expect();

    ade9153aMetering_init();

    TEST_ASSERT_EQUAL_UINT32( 0x100, ade9153a_last_sample_time);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_initMeasurement()
// 
//-----------------------------------------------------------------
void test_ade9153aMetering_initMeasurement()
{
    _DEBUG_OUT_TEST_NAME

    PowerCalcDataset dataset;
    
    ade9153a_initMeasurement_Expect(&dataset);

    ade9153aMetering_initMeasurement(&dataset);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_detectHW()
// result OK
//-----------------------------------------------------------------
void test_ade9153aMetering_detectHW_ResultOK()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_hardware_detect_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    powerMonitor_Result result = ade9153aMetering_detectHW();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_uartISR()
// 
//-----------------------------------------------------------------
void test_ade9153aMetering_uartISR()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_uartISR_Expect();

    ade9153aMetering_uartISR();
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_service()
// test sample time idle state
//-----------------------------------------------------------------
void test_ade9153aMetering_serviceIdle()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;

    ade9153a_last_sample_time = 0;

    get_systick_sec_ExpectAndReturn(ADE9153A_INSTANT_DATA_WAIT_TIME - 1);

    result = ade9153aMetering_service();

    TEST_ASSERT_EQUAL_UINT32(0, ade9153a_last_sample_time);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_IDLE, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_service()
// test sample time
//-----------------------------------------------------------------
void test_ade9153aMetering_serviceSampleTime()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    
    ade9153a_last_sample_time = 0;
 
    get_systick_sec_ExpectAndReturn(ADE9153A_INSTANT_DATA_WAIT_TIME + 1);
    
    ade9153a_readAndProcessInstantData_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    result = ade9153aMetering_service();

    TEST_ASSERT_EQUAL_UINT32(ADE9153A_INSTANT_DATA_WAIT_TIME + 1, ade9153a_last_sample_time);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

/*------------------------------ End of File -------------------------------*/

