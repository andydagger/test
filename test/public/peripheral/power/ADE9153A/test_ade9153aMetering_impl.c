#include "unity.h"
#include "ade9153aMetering_impl.c"

#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_relay.h"
#include "mock_hw.h"
#include "mock_nwk_com.h"
#include "mock_CRC16.h"
#include "mock_powerMonitor.h"
#include "mock_ade9153aMetering.h"
#include "mock_ade9153aMetering_dataProcess.h"
#include "mock_ade9153aMetering_transport.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}


void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_stm8_init()
// Make sure vas are initialised correctly, baud rate set and uart irq enabled
//-----------------------------------------------------------------
void test_ade9153a_init()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset localDataset = { 0 };

    ad_dataset_ptr = &localDataset;

    ade9153a_init();

    TEST_ASSERT_EQUAL_PTR(NULL, ad_dataset_ptr);
}


//-----------------------------------------------------------------
// void test_ade9153a_initMeasurement()
// 
//-----------------------------------------------------------------
void test_ade9153a_initMeasurement()
{
    _DEBUG_OUT_TEST_NAME

    PowerCalcDataset dataset = {0};
    // ADE9153A_REG_AI_PGAGAIN
    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    // ADE9153A_REG_CONFIG0
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    // ADE9153A_REG_CONFIG1
    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_CONFIG2
    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_CONFIG3
    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_ACCMODE
    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_VLEVEL
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_ZX_CFG
    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_ACT_NL_LVL
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_REACT_NL_LVL
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_APP_NL_LVL
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_VDIV_RSMALL
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_COMPMODE
    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_EGY_TIME
    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_TEMP_CFG
    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_SWELL_LVL_32
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_SWELL_CYC_16
    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_DIP_LVL_32
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_DIP_CYC_16
    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_OI_LVL_32
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    // ADE9153A_REG_AIGAIN
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_EP_CFG
    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_MASK
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_initMeasurement(&dataset);

    TEST_ASSERT_EQUAL_PTR(&dataset, ad_dataset_ptr);
}


//-----------------------------------------------------------------
// void test_ade9153a_hardware_detect()
// tests if the address of the calibration vals is correct
//-----------------------------------------------------------------
void test_ade9153a_hardware_detectResultOK()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    uint32_t version = ADE9153A_VERSION_PRODUCT_VAL;

    ade9153a_writeRegister_16_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_readRegister_32_ExpectAnyArgsAndReturn( POWER_MONITOR_RESULT_OK);

    result = ade9153a_hardware_detect();
    
    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_HW_NOT_DETECTED, result);
}

//-----------------------------------------------------------------
// void ade9153a_readAndProcessInstantData()
// Test with invalid response from read instant reg vals
//-----------------------------------------------------------------
void test_ade9153a_readAndProcessInstantData_TimeoutReadingInstantData()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result     result;

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    result = ade9153a_readAndProcessInstantData();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void ade9153a_readAndProcessInstantData()
// Test with ade9153a_calcInstantValues() returning param error
//-----------------------------------------------------------------
void test_ade9153a_readAndProcessInstantData_CalcInstantValsParamError()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result     result;

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_PARAM_ERROR);
    
    result = ade9153a_readAndProcessInstantData();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void ade9153a_readAndProcessInstantData()
// Test with ade9153a_addValuesToDataset() returing param error
//-----------------------------------------------------------------
void test_ade9153a_readAndProcessInstantData_AddValsToDatasetParamError()
{
    _DEBUG_OUT_TEST_NAME

        powerMonitor_Result     result;

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_addValuesToDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_PARAM_ERROR);

    result = ade9153a_readAndProcessInstantData();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void ade9153a_readAndProcessInstantData()
// Test with no errors
//-----------------------------------------------------------------
void test_ade9153a_readAndProcessInstantData_ProcessedOK()
{
    _DEBUG_OUT_TEST_NAME

        powerMonitor_Result     result;

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_addValuesToDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    result = ade9153a_readAndProcessInstantData();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

/*------------------------------ End of File -------------------------------*/

