#include "unity.h"
#include "ade9153aMetering_calibDataProcess.c"

#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_productionMonitor.h"
#include "mock_powerMonitor.h"
#include "mock_ade9153aMetering.h"
#include "mock_ade9153aMetering_impl.h"
#include "mock_ade9153aMetering_transport.h"
#include "mock_ade9153aMetering_calibStartStop.h"
#include "mock_ade9153aMetering_dataProcess.h"
#include "mock_ade9153aMetering_calibration.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}


void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_ade9153a_calibrationReadValues()
// NULL pointer test
//-----------------------------------------------------------------
void test_ade9153_calibrationReadValues_NullPointer()
{
    _DEBUG_OUT_TEST_NAME
    
    powerMonitor_Result result = ade9153a_calibrationReadValues(NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_calibrationReadValues()
// Read Instant Regs Timeout
//-----------------------------------------------------------------
void test_ade9153_calibrationReadValues_ReadInstantRegsTimeout()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataSet = { 0 };

    FreeRTOSDelay_ExpectAnyArgs();
    
    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    powerMonitor_Result result = ade9153a_calibrationReadValues(&dataSet);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_calibrationReadValues()
// calc instant vals param error
//-----------------------------------------------------------------
void test_ade9153_calibrationReadValues_CalcInstantValsError()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataSet = { 0 };

    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_PARAM_ERROR);

    powerMonitor_Result result = ade9153a_calibrationReadValues(&dataSet);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_calibrationReadValues()
// add vals to dataset param error
//-----------------------------------------------------------------
void test_ade9153_calibrationReadValues_AddValsToDatasetError()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataSet = { 0 };

    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_addValuesToDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_PARAM_ERROR);

    powerMonitor_Result result = ade9153a_calibrationReadValues(&dataSet);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_calibrationReadValues()
// loop result OK
//-----------------------------------------------------------------
void test_ade9153a_calibrationReadValues_LoopResultOK()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataSet = { 0 };

    // 1st loop
    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_addValuesToDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    // 2nd loop
    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_addValuesToDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    // 3rd loop
    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_addValuesToDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    // 4th loop
    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_addValuesToDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    // 5th loop
    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_addValuesToDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    // 6th loop
    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_addValuesToDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    // 7th loop
    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_addValuesToDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    // 8th loop
    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_readInstantRegisterValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_addValuesToDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    
    powerMonitor_Result result = ade9153a_calibrationReadValues(&dataSet);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_calibrateVoltageChannel()
// result OK
//-----------------------------------------------------------------
void test_ade9153a_calibrateVoltageChannel_ResultOK()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_startCalibration_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_stopCalibration_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    powerMonitor_Result result = ade9153a_calibrateVoltageChannel();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_calibrateVoltageChannel()
// result Start calib timeout
//-----------------------------------------------------------------
void test_ade9153a_calibrateVoltageChannel_StartCalibTimeout()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_startCalibration_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    powerMonitor_Result result = ade9153a_calibrateVoltageChannel();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_calibrateVoltageChannel()
// result Stop calib timeout
//-----------------------------------------------------------------
void test_ade9153a_calibrateVoltageChannel_StopCalibTimeout()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_startCalibration_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_stopCalibration_ExpectAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    powerMonitor_Result result = ade9153a_calibrateVoltageChannel();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_calibrateCurrenteChannel()
// result OK
//-----------------------------------------------------------------
void test_ade9153a_calibrateCurrentChannel_ResultOK()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_startCalibration_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_stopCalibration_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    powerMonitor_Result result = ade9153a_calibrateCurrentChannel();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_calibrateCurrentChannel()
// result Start calib timeout
//-----------------------------------------------------------------
void test_ade9153a_calibrateCurrentChannel_StartCalibTimeout()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_startCalibration_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    powerMonitor_Result result = ade9153a_calibrateCurrentChannel();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_calibrateCurrentChannel()
// result Stop calib timeout
//-----------------------------------------------------------------
void test_ade9153a_calibrateCurrentChannel_StopCalibTimeout()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_startCalibration_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    FreeRTOSDelay_ExpectAnyArgs();

    ade9153a_stopCalibration_ExpectAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    powerMonitor_Result result = ade9153a_calibrateCurrentChannel();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_readAutoCalRegisters()
// NULL pointer test
//-----------------------------------------------------------------
void test_ade9153a_readAutoCalRegisters_NULLPointer()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result = ade9153a_readAutoCalRegisters(NULL);
 
    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_readAutoCalRegisters()
// timeout reading AICC register
//-----------------------------------------------------------------
void test_ade9153a_readAutoCalRegisters_TimeoutReading_AICC()
{
    _DEBUG_OUT_TEST_NAME
    ade9153a_autoCalRegs calRegs;
    
    // ADE9153A_REG_MS_ACAL_AICC
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);
    
    powerMonitor_Result result = ade9153a_readAutoCalRegisters(&calRegs);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_readAutoCalRegisters()
// timeout reading AICERT register
//-----------------------------------------------------------------
void test_ade9153a_readAutoCalRegisters_TimeoutReading_AICERT()
{
    _DEBUG_OUT_TEST_NAME
    ade9153a_autoCalRegs calRegs;

    // ADE9153A_REG_MS_ACAL_AICC
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_MS_ACAL_AICERT
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    powerMonitor_Result result = ade9153a_readAutoCalRegisters(&calRegs);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_readAutoCalRegisters()
// timeout reading AVCC register
//-----------------------------------------------------------------
void test_ade9153a_readAutoCalRegisters_TimeoutReading_AVCC()
{
    _DEBUG_OUT_TEST_NAME
    ade9153a_autoCalRegs calRegs;

    // ADE9153A_REG_MS_ACAL_AICC
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_MS_ACAL_AICERT
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    //ADE9153A_REG_MS_ACAL_AVCC
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    powerMonitor_Result result = ade9153a_readAutoCalRegisters(&calRegs);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_readAutoCalRegisters()
// timeout reading AVCERT register
//-----------------------------------------------------------------
void test_ade9153a_readAutoCalRegisters_TimeoutReading_AVCERT()
{
    _DEBUG_OUT_TEST_NAME
    ade9153a_autoCalRegs calRegs;

    // ADE9153A_REG_MS_ACAL_AICC
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_MS_ACAL_AICERT
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_MS_ACAL_AVCC
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_MS_ACAL_AVCERT
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    powerMonitor_Result result = ade9153a_readAutoCalRegisters(&calRegs);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_readAutoCalRegisters()
// timeout reading AVCERT register
//-----------------------------------------------------------------
void test_ade9153a_readAutoCalRegisters_ResultOK()
{
    _DEBUG_OUT_TEST_NAME
    ade9153a_autoCalRegs calRegs;

    // ADE9153A_REG_MS_ACAL_AICC
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_MS_ACAL_AICERT
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_MS_ACAL_AVCC
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_MS_ACAL_AVCERT
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    powerMonitor_Result result = ade9153a_readAutoCalRegisters(&calRegs);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}


//-----------------------------------------------------------------
// void test_ade9153a_applyCalibrationGains()
// NULL pointer test
//-----------------------------------------------------------------
void test_ade9153a_applyCalibrationGains_NULLPointer()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result = ade9153a_applyCalibrationGains(NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_applyCalibrationGains()
// timeout writing AIGAIN register
//-----------------------------------------------------------------
void test_ade9153a_applyCalibrationGains_TimeoutWriting_AIGAIN()
{
    _DEBUG_OUT_TEST_NAME
    ade9153a_autoCalRegs calRegs;

    // ADE9153A_REG_AIGAIN
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    powerMonitor_Result result = ade9153a_applyCalibrationGains(&calRegs);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_applyCalibrationGains()
// timeout writing AVGAIN register
//-----------------------------------------------------------------
void test_ade9153a_applyCalibrationGains_TimeoutWriting_AVGAIN()
{
    _DEBUG_OUT_TEST_NAME
    ade9153a_autoCalRegs calRegs;

    // ADE9153A_REG_AIGAIN
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_AVGAIN
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    powerMonitor_Result result = ade9153a_applyCalibrationGains(&calRegs);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_applyCalibrationGains()
// Result OK
//-----------------------------------------------------------------
void test_ade9153a_applyCalibrationGains_ResultOK()
{
    _DEBUG_OUT_TEST_NAME
    ade9153a_autoCalRegs calRegs;

    // ADE9153A_REG_AIGAIN
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_AVGAIN
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    powerMonitor_Result result = ade9153a_applyCalibrationGains(&calRegs);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

#if 0

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// test for calibration Current And Voltage timeout
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrate_CurrentTimeout()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;
    
    ade9153a_calibrateCurrentChannel_ExpectAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);

    powerMonitor_Result result = ade9153aMetering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// test for calibration Voltage timeout
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrate_VoltageTimeout()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;

    ade9153a_calibrateCurrentChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);
    
    ade9153a_calibrateVoltageChannel_ExpectAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);

    powerMonitor_Result result = ade9153aMetering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// test for calibration - Read calibration register timeout
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrateReadCalRegs_Timeout()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;

    ade9153a_calibrateCurrentChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calibrateVoltageChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_readAutoCalRegisters_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);

    powerMonitor_Result result = ade9153aMetering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// test for calibration - Apply Calib Gains timeout
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrate_ApplyGainsTimeout()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;

    ade9153a_calibrateCurrentChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calibrateVoltageChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_readAutoCalRegisters_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    
    ade9153a_applyCalibrationGains_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);

    powerMonitor_Result result = ade9153aMetering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// test for calibration - OK - under test
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrate_UnderTestOK()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;

    ade9153a_calibrateCurrentChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calibrateVoltageChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_readAutoCalRegisters_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_applyCalibrationGains_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);

    ade9153a_calibrationReadValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    powerMonitor_Result result = ade9153aMetering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_calibrate()
// test for calibration - OK - not under test
//-----------------------------------------------------------------
void test_ade9153aMetering_calibrate_NotUnderTestOK()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;

    ade9153a_calibrateCurrentChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_calibrateVoltageChannel_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_readAutoCalRegisters_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    ade9153a_applyCalibrationGains_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    productionMonitor_getUnitIsUndertest_ExpectAndReturn(false);

    powerMonitor_Result result = ade9153aMetering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}


//-----------------------------------------------------------------
// void test_ade9153a_stopCalibration()
// test ade9153a_stopCalibration() 
//-----------------------------------------------------------------
void test_ade9153a_stopCalibration()
{
    _DEBUG_OUT_TEST_NAME
 
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    powerMonitor_Result result = ade9153a_stopCalibration();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_startCalibration()
// test with invalid register value 
//-----------------------------------------------------------------
void test_ade9153a_startCalibration_InvalidCalRegValue()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result = ade9153a_startCalibration(0);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_startCalibration()
// test with voltage channel cal timeout reading register
//-----------------------------------------------------------------
void test_ade9153a_startCalibration_CalRegReadTimeout()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    powerMonitor_Result result = ade9153a_startCalibration(ADE9153A_RUN_AUTOCAL_AV);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_startCalibration()
// test timeout on 1st pass 
//-----------------------------------------------------------------
void test_ade9153a_startCalibration_TimeoutWaitingMSureReady()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    get_systick_ms_ExpectAndReturn(0);

    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_ms_ExpectAndReturn(ADE9153A_MSURE_READY_TIMEOUT+1);

    powerMonitor_Result result = ade9153a_startCalibration(ADE9153A_RUN_AUTOCAL_AV);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_startCalibration()
// test loop test with timeout 
//-----------------------------------------------------------------
void test_ade9153a_startCalibration_LoopTimeoutWaitingMSureReady()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    get_systick_ms_ExpectAndReturn(0);

    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_ms_ExpectAndReturn(12);

    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_ms_ExpectAndReturn(ADE9153A_MSURE_READY_TIMEOUT + 1);

    powerMonitor_Result result = ade9153a_startCalibration(ADE9153A_RUN_AUTOCAL_AV);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}





//-----------------------------------------------------------------
// void test_stm8Metering_calibrate()
// test for calibration sample timeout
//-----------------------------------------------------------------
void test_stm8Metering_calibrateSampleTimeout()
{
    _DEBUG_OUT_TEST_NAME
        PowerCalcDataset dataset;
    // Start time init
    get_systick_sec_ExpectAndReturn(0);
    get_systick_sec_ExpectAndReturn(0);

    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(STM8_CALIBRATION_SAMPLE_TIMEOUT + 2);

    stm8_isMessageReceived_ExpectAndReturn(false);

    powerMonitor_Result result = stm8Metering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_stm8Metering_calibrate()
// test the state machine and exiting the cal with a pass
//-----------------------------------------------------------------
void test_stm8Metering_calibrateProcessWithPass()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset = { 0 };
    uint8_t rx_buffer[16];
    stm8_CalibCoeff calibCoeffs;
    
    // Start time init
    get_systick_sec_ExpectAndReturn(0);
    get_systick_sec_ExpectAndReturn(0);

    /* 1st time rounf loop */
    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(2);

    stm8_isMessageReceived_ExpectAndReturn(true);

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    relay_update_wait_ExpectAnyArgs();
    
    stm8_sendResponse_ExpectAnyArgs();
    
    /* 2nd time round loop */
    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(4);

    stm8_isMessageReceived_ExpectAndReturn(true);

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getZeroPointCalibStatus_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    /* 3rd time round loop */
    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(6);

    stm8_isMessageReceived_ExpectAndReturn(true);

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_processCalibSamples_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_updateDataset_ExpectAnyArgs();

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    powerMonitor_Result result = stm8Metering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
    TEST_ASSERT_EQUAL_UINT32(STM8_CALIB_STATE_FINISHED, stm8_state);
}

//-----------------------------------------------------------------
// void test_stm8Metering_calibrate()
// test the state machine and exiting the cal with a fail
//-----------------------------------------------------------------
void test_stm8Metering_calibrateProcessWithFail()
{
    _DEBUG_OUT_TEST_NAME
        PowerCalcDataset dataset = { 0 };
    uint8_t rx_buffer[16];
    stm8_CalibCoeff calibCoeffs;

    // Start time init
    get_systick_sec_ExpectAndReturn(0);
    get_systick_sec_ExpectAndReturn(0);

    /* 1st time rounf loop */
    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(2);

    stm8_isMessageReceived_ExpectAndReturn(true);

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    /* 2nd time round loop */
    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(4);

    stm8_isMessageReceived_ExpectAndReturn(true);

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getZeroPointCalibStatus_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    /* 3rd time round loop */
    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(6);

    stm8_isMessageReceived_ExpectAndReturn(true);

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_processCalibSamples_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_CALIBRATION_FAILED);

    stm8_updateDataset_ExpectAnyArgs();

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    powerMonitor_Result result = stm8Metering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_CALIBRATION_FAILED, result);
    TEST_ASSERT_EQUAL_UINT32(STM8_CALIB_STATE_FAILED, stm8_state);
}




//-----------------------------------------------------------------
// void test_stm8_calibProcessRxMessage()
// test the zero point cal moves on to wait for cal
//-----------------------------------------------------------------
void test_stm8_calibProcessRxMessage_ZeroPointCal()
{
    _DEBUG_OUT_TEST_NAME
    uint8_t rx_buffer[16];
    stm8_CalibCoeff calibCoeffs;

    stm8_state = STM8_CALIB_STATE_ZERO_POINT_CAL;

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    powerMonitor_Result result = stm8_calibProcessRxMessage();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_CALIBRATION_ACTIVE, result);
    TEST_ASSERT_EQUAL_UINT32(STM8_CALIB_STATE_WAIT_ZERO_POINT_CAL, stm8_state);
}

//-----------------------------------------------------------------
// void test_stm8_calibProcessRxMessage()
// test the zero point cal wait ends and closes the relay
//-----------------------------------------------------------------
void test_stm8_calibProcessRxMessage_ZeroPointCalFinished()
{
    _DEBUG_OUT_TEST_NAME
        uint8_t rx_buffer[16];
    stm8_CalibCoeff calibCoeffs;

    stm8_state = STM8_CALIB_STATE_WAIT_ZERO_POINT_CAL;

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getZeroPointCalibStatus_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    powerMonitor_Result result = stm8_calibProcessRxMessage();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_CALIBRATION_ACTIVE, result);
    TEST_ASSERT_EQUAL_UINT32(STM8_CALIB_STATE_CALIB_CALC, stm8_state);
}

//-----------------------------------------------------------------
// void test_stm8_calibProcessRxMessage()
// process rx message with a checksum error
//-----------------------------------------------------------------
void test_stm8_calibProcessRxMessage_ChecksumError()
{
    _DEBUG_OUT_TEST_NAME
        uint8_t rx_buffer[16];
    stm8_CalibCoeff calibCoeffs;

    stm8_state = STM8_CALIB_STATE_WAIT_ZERO_POINT_CAL;

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_DATA_ERROR);

    powerMonitor_Result result = stm8_calibProcessRxMessage();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_DATA_ERROR, result);
    TEST_ASSERT_EQUAL_UINT32(STM8_CALIB_STATE_WAIT_ZERO_POINT_CAL, stm8_state);
}

#endif

/*------------------------------ End of File -------------------------------*/

