#include "unity.h"
#include "ade9153aMetering_calibStartStop.c"

#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_productionMonitor.h"
#include "mock_powerMonitor.h"
#include "mock_ade9153aMetering.h"
#include "mock_ade9153aMetering_impl.h"
#include "mock_ade9153aMetering_transport.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_ade9153a_stopCalibration()
// test ade9153a_stopCalibration() 
//-----------------------------------------------------------------
void test_ade9153a_stopCalibration()
{
    _DEBUG_OUT_TEST_NAME
 
    ade9153a_writeRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    powerMonitor_Result result = ade9153a_stopCalibration();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_startCalibration()
// test with invalid register value 
//-----------------------------------------------------------------
void test_ade9153a_startCalibration_InvalidCalRegValue()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result = ade9153a_startCalibration(0);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_startCalibration()
// test with voltage channel cal timeout reading register
//-----------------------------------------------------------------
void test_ade9153a_startCalibration_CalRegReadTimeout()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    powerMonitor_Result result = ade9153a_startCalibration(ADE9153A_RUN_AUTOCAL_AV);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_startCalibration()
// test timeout on 1st pass 
//-----------------------------------------------------------------
void test_ade9153a_startCalibration_TimeoutWaitingMSureReady()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    get_systick_ms_ExpectAndReturn(0);

    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_ms_ExpectAndReturn(ADE9153A_MSURE_READY_TIMEOUT+1);

    powerMonitor_Result result = ade9153a_startCalibration(ADE9153A_RUN_AUTOCAL_AV);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_startCalibration()
// test loop test with timeout 
//-----------------------------------------------------------------
void test_ade9153a_startCalibration_LoopTimeoutWaitingMSureReady()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    get_systick_ms_ExpectAndReturn(0);

    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_ms_ExpectAndReturn(12);

    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_ms_ExpectAndReturn(ADE9153A_MSURE_READY_TIMEOUT + 1);

    powerMonitor_Result result = ade9153a_startCalibration(ADE9153A_RUN_AUTOCAL_AV);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}


/*------------------------------ End of File -------------------------------*/

