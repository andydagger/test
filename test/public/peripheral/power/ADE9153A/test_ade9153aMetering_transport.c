#include "unity.h"
#include "ade9153aMetering_transport.c"

#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_relay.h"
#include "mock_hw.h"
#include "mock_nwk_com.h"
#include "mock_CRC16.h"
#include "mock_powerMonitor.h"
#include "mock_ade9153aMetering.h"
#include "mock_ade9153aMetering_dataProcess.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_ade9153a_transportInit()
// Make sure vas are initialised correctly, baud rate set and uart irq enabled
//-----------------------------------------------------------------
void test_ade9153a_transportInit()
{
    _DEBUG_OUT_TEST_NAME

    ad_comms_tx_index = 10;
    ad_comms_rx_index = 10;
    ad_comms_rx_size = 10;
    ad_comms_tx_size = 10;

    hw_setup_power_monitor_uart_params_Expect(ADE9153A_UART_BAUD_RATE, ADE9153A_UART_COMM_PARAMS);

    hw_power_monitor_uart_irq_enable_Expect();

    ade9153a_transportInit();

    TEST_ASSERT_EQUAL_UINT8(0, ad_comms_tx_size);
    TEST_ASSERT_EQUAL_UINT8(0, ad_comms_rx_size);
    TEST_ASSERT_EQUAL_UINT8(0, ad_comms_tx_index);
    TEST_ASSERT_EQUAL_UINT8(0, ad_comms_rx_index);
}

//-----------------------------------------------------------------
// void test_ade9153aMetering_uartISR()
// Test data byte received path - not reached buffer length
//-----------------------------------------------------------------
void test_ade9153a_uartISR_RXD()
{
    _DEBUG_OUT_TEST_NAME
    ad_comms_rx_index = 0;
    ad_comms_rx_size = 1;
    ad_rx_buffer[0] = 0;

    hw_power_monitor_uart_get_int_status_ExpectAndReturn(UART_STAT_RXRDY);

    hw_power_monitor_uart_rx_byte_ExpectAndReturn(0x5A);

    ade9153a_uartISR();

    TEST_ASSERT_EQUAL_UINT8(0x5A, ad_rx_buffer[0]);
}


//-----------------------------------------------------------------
// void test_ade9153a_uartISR()
// Test rxd byte is not put in buffer when size reached
//-----------------------------------------------------------------
void test_ade9153a_uartISR_RXD_SizeLimit()
{
    _DEBUG_OUT_TEST_NAME

    ad_rx_buffer[0] = 0;
    ad_rx_buffer[1] = 0;
    ad_comms_rx_index = 0;
    ad_comms_rx_size = 1;

    hw_power_monitor_uart_get_int_status_ExpectAndReturn(UART_STAT_RXRDY);

    hw_power_monitor_uart_rx_byte_ExpectAndReturn(0x5A);

    ade9153a_uartISR();

    hw_power_monitor_uart_get_int_status_ExpectAndReturn(UART_STAT_RXRDY);

    hw_power_monitor_uart_rx_byte_ExpectAndReturn(0xA5);

    ade9153a_uartISR();

    TEST_ASSERT_EQUAL_UINT8(0x5A, ad_rx_buffer[0]);
    TEST_ASSERT_EQUAL_UINT8(0x00, ad_rx_buffer[1]);
}

//-----------------------------------------------------------------
// void test_ade9153a_uartISR()
// Test data byte RXD but not diabling the TX IRQ
//-----------------------------------------------------------------
void test_ade9153a_uartISR_TXD_NotEndOfMessage()
{
    _DEBUG_OUT_TEST_NAME
    ad_tx_buffer[0] = 0x5A;
    ad_comms_tx_index = 0;
    ad_comms_tx_size = 2;

    hw_power_monitor_uart_get_int_status_ExpectAndReturn(UART_STAT_TXRDY);

    hw_power_monitor_uart_tx_byte_ExpectAnyArgs();

    ade9153a_uartISR();

    TEST_ASSERT_EQUAL_UINT8(1, ad_comms_tx_index);
}


//-----------------------------------------------------------------
// void test_ade9153a_uartISR()
// Test data byte TXD and test for IRQ being disable when sent all bytes
//-----------------------------------------------------------------
void test_ade9153a_uartISR_RXD_TXDEndOfMessage()
{
    _DEBUG_OUT_TEST_NAME
    ad_comms_tx_index = 0;
    ad_comms_tx_size = 1;

    hw_power_monitor_uart_get_int_status_ExpectAndReturn(UART_STAT_TXRDY);

    hw_power_monitor_uart_tx_byte_ExpectAnyArgs();

    hw_power_monitor_uart_tx_disable_Expect();

    ade9153a_uartISR();

    TEST_ASSERT_EQUAL_UINT8(1, ad_comms_tx_index);
}

//-----------------------------------------------------------------
// void test_ade9153a_readRegister_16()
// Test return value with NULL data ptr
//-----------------------------------------------------------------
void test_ade9153a_readRegister_16_NullDataPtr()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;

    result = ade9153a_readRegister_16(ADE9153A_REG_CFMODE_16, NULL);

    TEST_ASSERT_EQUAL_UINT32( POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_readRegister_16()
// Test the contents of the TX message
//-----------------------------------------------------------------
void test_ade9153a_readRegister_16_TxSendMessageContents()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    uint16_t value;

    ad_tx_buffer[ADE9153A_TX_IDX_CMD0] = 0;
    ad_tx_buffer[ADE9153A_TX_IDX_CMD1] = 0;

    hw_power_monitor_uart_tx_enable_Expect();
    get_systick_ms_ExpectAndReturn(0);
    FreeRTOSDelay_ExpectAnyArgs();
    get_systick_ms_ExpectAndReturn(26);

    result = ade9153a_readRegister_16(ADE9153A_REG_CFMODE_16, &value);

#define CMD16_MSB_TEST_VAL  (((ADE9153A_REG_CFMODE_16 << 4) | ADE9153A_CMD_CHECKSUM | ADE9153A_CMD_READ) >> 8)
#define CMD16_LSB_TEST_VAL  (((ADE9153A_REG_CFMODE_16 << 4) | ADE9153A_CMD_CHECKSUM | ADE9153A_CMD_READ) & 0xFF)

    TEST_ASSERT_EQUAL_UINT8(CMD16_MSB_TEST_VAL, ad_tx_buffer[ADE9153A_TX_IDX_CMD0]);
    TEST_ASSERT_EQUAL_UINT8(CMD16_LSB_TEST_VAL, ad_tx_buffer[ADE9153A_TX_IDX_CMD1]);
    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_readRegister_32()
// Test return value with NULL data ptr
//-----------------------------------------------------------------
void test_ade9153a_readRegister_32_NullDataPtr()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;

    result = ade9153a_readRegister_32(ADE9153A_REG_AVRMS_32, NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_readRegister_32()
// Test the contents of the TX message
//-----------------------------------------------------------------
void test_ade9153a_readRegister_32_TxSendMessageContents()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    uint32_t value;

    ad_tx_buffer[ADE9153A_TX_IDX_CMD0] = 0;
    ad_tx_buffer[ADE9153A_TX_IDX_CMD1] = 0;

    hw_power_monitor_uart_tx_enable_Expect();
    get_systick_ms_ExpectAndReturn(0);
    FreeRTOSDelay_ExpectAnyArgs();
    get_systick_ms_ExpectAndReturn(26);

    result = ade9153a_readRegister_32(ADE9153A_REG_AVRMS_32, &value);

#define CMD32_MSB_TEST_VAL  (((ADE9153A_REG_AVRMS_32 << 4) | ADE9153A_CMD_CHECKSUM | ADE9153A_CMD_READ) >> 8)
#define CMD32_LSB_TEST_VAL  (((ADE9153A_REG_AVRMS_32 << 4) | ADE9153A_CMD_CHECKSUM | ADE9153A_CMD_READ) & 0xFF)

    TEST_ASSERT_EQUAL_UINT8(CMD32_MSB_TEST_VAL, ad_tx_buffer[ADE9153A_TX_IDX_CMD0]);
    TEST_ASSERT_EQUAL_UINT8(CMD32_LSB_TEST_VAL, ad_tx_buffer[ADE9153A_TX_IDX_CMD1]);
    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_writeRegister_16()
// Test the contents of the TX message
//-----------------------------------------------------------------
void test_ade9153a_writeRegister_16_TxSendMessageContents()
{
    _DEBUG_OUT_TEST_NAME

        powerMonitor_Result result;
    uint16_t value = 0x1234;

    memset((void*)&ad_tx_buffer, 0, sizeof(ad_tx_buffer));

    CRC16_calculate_ExpectAnyArgsAndReturn(0x5AA5);
    hw_power_monitor_uart_tx_enable_Expect();
    get_systick_ms_ExpectAndReturn(0);
    FreeRTOSDelay_ExpectAnyArgs();
    get_systick_ms_ExpectAndReturn(26);

    result = ade9153a_writeRegister_16(ADE9153A_REG_COMPMODE_16, value);

#define CMD16_W_MSB_TEST_VAL  (((ADE9153A_REG_COMPMODE_16 << 4) | ADE9153A_CMD_CHECKSUM) >> 8)
#define CMD16_W_LSB_TEST_VAL  (((ADE9153A_REG_COMPMODE_16 << 4) | ADE9153A_CMD_CHECKSUM) & 0xFF)

    TEST_ASSERT_EQUAL_UINT8(CMD16_W_MSB_TEST_VAL, ad_tx_buffer[ADE9153A_TX_IDX_CMD0]);
    TEST_ASSERT_EQUAL_UINT8(CMD16_W_LSB_TEST_VAL, ad_tx_buffer[ADE9153A_TX_IDX_CMD1]);
    TEST_ASSERT_EQUAL_UINT8(0x12, ad_tx_buffer[ADE9153A_TX_IDX_DATA0]);
    TEST_ASSERT_EQUAL_UINT8(0x34, ad_tx_buffer[ADE9153A_TX_IDX_DATA1]);
    TEST_ASSERT_EQUAL_UINT8(0x5A, ad_tx_buffer[ADE9153A_TX_IDX_CRC_MSB_16]);
    TEST_ASSERT_EQUAL_UINT8(0xA5, ad_tx_buffer[ADE9153A_TX_IDX_CRC_LSB_16]);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_writeRegister_32()
// Test the contents of the TX message
//-----------------------------------------------------------------
void test_ade9153a_writeRegister_32_TxSendMessageContents()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    uint32_t value = 0x12345678;

    memset((void*)&ad_tx_buffer, 0, sizeof(ad_tx_buffer));

    CRC16_calculate_ExpectAnyArgsAndReturn(0x5AA5);
    hw_power_monitor_uart_tx_enable_Expect();
    get_systick_ms_ExpectAndReturn(0);
    FreeRTOSDelay_ExpectAnyArgs();
    get_systick_ms_ExpectAndReturn(26);

    result = ade9153a_writeRegister_32(ADE9153A_REG_AIGAIN_32, value);

#define CMD32_W_MSB_TEST_VAL  (((ADE9153A_REG_AIGAIN_32 << 4) | ADE9153A_CMD_CHECKSUM) >> 8)
#define CMD32_W_LSB_TEST_VAL  (((ADE9153A_REG_AIGAIN_32 << 4) | ADE9153A_CMD_CHECKSUM) & 0xFF)

    TEST_ASSERT_EQUAL_UINT8(CMD32_W_MSB_TEST_VAL, ad_tx_buffer[ADE9153A_TX_IDX_CMD0]);
    TEST_ASSERT_EQUAL_UINT8(CMD32_W_LSB_TEST_VAL, ad_tx_buffer[ADE9153A_TX_IDX_CMD1]);
    TEST_ASSERT_EQUAL_UINT8(0x12, ad_tx_buffer[ADE9153A_TX_IDX_DATA0]);
    TEST_ASSERT_EQUAL_UINT8(0x34, ad_tx_buffer[ADE9153A_TX_IDX_DATA1]);
    TEST_ASSERT_EQUAL_UINT8(0x56, ad_tx_buffer[ADE9153A_TX_IDX_DATA2]);
    TEST_ASSERT_EQUAL_UINT8(0x78, ad_tx_buffer[ADE9153A_TX_IDX_DATA3]);
    TEST_ASSERT_EQUAL_UINT8(0x5A, ad_tx_buffer[ADE9153A_TX_IDX_CRC_MSB_32]);
    TEST_ASSERT_EQUAL_UINT8(0xA5, ad_tx_buffer[ADE9153A_TX_IDX_CRC_LSB_32]);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}


//-----------------------------------------------------------------
// void test_sendMessage()
// Test the vars contents and function calls
//-----------------------------------------------------------------
void test_ade9153a_sendMessage()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    // invalidate the vars for the compare
    ad_comms_tx_index = 0xFF;
    ad_comms_rx_index = 0xFF;

    ad_comms_tx_size = 0xFF;
    ad_comms_rx_size = 0xFF;

    hw_power_monitor_uart_tx_enable_Expect();
    get_systick_ms_ExpectAndReturn(0);
    FreeRTOSDelay_ExpectAnyArgs();
    get_systick_ms_ExpectAndReturn(26);

    result = ade9153a_sendMessage(ADE9153A_READ_TX_SIZE, ADE9153A_READ_RX_SIZE_32);

    TEST_ASSERT_EQUAL_UINT8(0, ad_comms_tx_index);
    TEST_ASSERT_EQUAL_UINT8(0, ad_comms_rx_index);
    TEST_ASSERT_EQUAL_UINT8(ADE9153A_READ_TX_SIZE, ad_comms_tx_size);
    TEST_ASSERT_EQUAL_UINT8(ADE9153A_READ_RX_SIZE_32, ad_comms_rx_size);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

/*------------------------------ End of File -------------------------------*/
