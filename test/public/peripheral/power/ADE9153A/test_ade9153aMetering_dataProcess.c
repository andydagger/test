#include "unity.h"
#include "ade9153aMetering_dataProcess.c"

#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_relay.h"
#include "mock_hw.h"
#include "mock_nwk_com.h"
#include "mock_powerMonitor.h"
#include "mock_ade9153aMetering.h"
#include "mock_ade9153aMetering_impl.h"
#include "mock_ade9153aMetering_transport.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_ade9153a_addValuesToDataset()
// instant vals pointer = NULL
//-----------------------------------------------------------------
void test_ade9153a_addValuesToDataset_InstantValNullPtr()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset datasetVals = { 0 };

    powerMonitor_Result result = ade9153a_addValuesToDataset(NULL, &datasetVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_addValuesToDataset()
// PowerCalcDataset pointer = NULL
//-----------------------------------------------------------------
void test_ade9153a_addValuesToDataset_PowerCalcDatasetNullPtr()
{
    _DEBUG_OUT_TEST_NAME
    ade9153a_instantValues instantVals = { 0 };

    powerMonitor_Result result = ade9153a_addValuesToDataset( &instantVals, NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_addValuesToDataset()
// Test the values calculated
//-----------------------------------------------------------------
void test_ade9153a_addValuesToDataset_CalcValues()
{
    _DEBUG_OUT_TEST_NAME
    ade9153a_instantValues instantVals = { 0 };
    PowerCalcDataset datasetVals = { 0 };

    instantVals.voltage = 240000;
    instantVals.current = 1000;
    instantVals.power = 1200;
    instantVals.powerFactor = 12;
    instantVals.energy = 40000;

    datasetVals.vrmsMin = 900;

    vTaskSuspendAll_Expect();
    xTaskResumeAll_ExpectAndReturn(pdTRUE);

    powerMonitor_Result result = ade9153a_addValuesToDataset(&instantVals, &datasetVals);

    if (result == POWER_MONITOR_RESULT_OK)
    {
        // test for vrmsMin, vrms average and accum energy
        instantVals.voltage = 230000;
        instantVals.current = 1000;
        instantVals.power = 1200;
        instantVals.powerFactor = 12;
        instantVals.energy = 40000;

        vTaskSuspendAll_Expect();
        xTaskResumeAll_ExpectAndReturn(pdTRUE);

        result = ade9153a_addValuesToDataset(&instantVals, &datasetVals);
    }


    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);

    TEST_ASSERT_EQUAL_FLOAT(230, datasetVals.instantVrms);
    TEST_ASSERT_EQUAL_FLOAT(1, datasetVals.instantIrms);
    TEST_ASSERT_EQUAL_FLOAT(1.2, datasetVals.instantPower);
    TEST_ASSERT_EQUAL_FLOAT(12, datasetVals.instantPowerFactor);
    TEST_ASSERT_EQUAL_FLOAT(4, datasetVals.accumulatedEnergy_20WhrUnits);
    TEST_ASSERT_EQUAL_FLOAT(240, datasetVals.vrmsMax);
    TEST_ASSERT_EQUAL_FLOAT(230, datasetVals.vrmsMin);
    TEST_ASSERT_EQUAL_FLOAT(235, datasetVals.vrmsAverage);
}

//-----------------------------------------------------------------
// void test_ade9153a_calcInstantValues()
// test raw null pointer return value
//-----------------------------------------------------------------
void test_ade9153a_calcInstantValues_RawNullPtr()
{
    _DEBUG_OUT_TEST_NAME
    ade9153a_instantValues instantVals = { 0 };

    powerMonitor_Result result;
        
    result = ade9153a_calcInstantValues(NULL, &instantVals);
 
    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_calcInstantValues()
// test instant vals null pointer return value
//-----------------------------------------------------------------
void test_ade9153a_calcInstantValues_InstantValsNullPtr()
{
    _DEBUG_OUT_TEST_NAME
    
    ade9153a_rawValues rawVals = { 0 };

    powerMonitor_Result result;

    result = ade9153a_calcInstantValues(&rawVals, NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_calcInstantValues()
// test the calculation values
//-----------------------------------------------------------------
void test_ade9153a_calcInstantValues_TestCalcs()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_rawValues rawVals = { 0 };
    ade9153a_instantValues instantVals = { 0 };

    powerMonitor_Result result;

    rawVals.voltageRMS = 17896;
    rawVals.currentRMS = 1194;
    rawVals.activePower = 796;
    rawVals.activeEnergy = 46603371;
    rawVals.powerFactor = 1610612736;

    result = ade9153a_calcInstantValues(&rawVals, &instantVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);

    TEST_ASSERT_EQUAL_FLOAT(240004.2, instantVals.voltage);
    TEST_ASSERT_EQUAL_FLOAT(1000.799, instantVals.current);
    TEST_ASSERT_EQUAL_FLOAT(1200.959, instantVals.power);
    TEST_ASSERT_EQUAL_FLOAT(12, instantVals.powerFactor);
    TEST_ASSERT_EQUAL_FLOAT(40000, instantVals.energy);
}


//-----------------------------------------------------------------
// void test_ade9153a_calcInstantValsAndAddValstoDataset()
// test the instant val calcs and adding vals to dataset
//-----------------------------------------------------------------
void test_ade9153a_calcInstantValsAndAddValstoDataset()
{
    _DEBUG_OUT_TEST_NAME

    ade9153a_rawValues rawVals = { 0 };
    ade9153a_instantValues instantVals = { 0 };
    PowerCalcDataset datasetVals = { 0 };

    powerMonitor_Result result;

    datasetVals.vrmsMin = 900;

    rawVals.voltageRMS = 17896;
    rawVals.currentRMS = 1194;
    rawVals.activePower = 796;
    rawVals.activeEnergy = 46603371;
    rawVals.powerFactor = 1610612736;

    vTaskSuspendAll_Expect();
    xTaskResumeAll_ExpectAndReturn(pdTRUE);

    result = ade9153a_calcInstantValues(&rawVals, &instantVals);

    if (result == POWER_MONITOR_RESULT_OK)
    {
        result = ade9153a_addValuesToDataset(&instantVals, &datasetVals);
    }

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);

    TEST_ASSERT_EQUAL_FLOAT(240004.2, instantVals.voltage);
    TEST_ASSERT_EQUAL_FLOAT(1000.799, instantVals.current);
    TEST_ASSERT_EQUAL_FLOAT(1200.959, instantVals.power);
    TEST_ASSERT_EQUAL_FLOAT(12, instantVals.powerFactor);
    TEST_ASSERT_EQUAL_FLOAT(40000, instantVals.energy);

    TEST_ASSERT_EQUAL_FLOAT(240.0042, datasetVals.instantVrms);
    TEST_ASSERT_EQUAL_FLOAT(1.000799, datasetVals.instantIrms);
    TEST_ASSERT_EQUAL_FLOAT(1.200959, datasetVals.instantPower);
    TEST_ASSERT_EQUAL_FLOAT(12, datasetVals.instantPowerFactor);
    TEST_ASSERT_EQUAL_FLOAT(2, datasetVals.accumulatedEnergy_20WhrUnits);
    TEST_ASSERT_EQUAL_FLOAT(240.0042, datasetVals.vrmsMax);
    TEST_ASSERT_EQUAL_FLOAT(240.0042, datasetVals.vrmsMin);
    TEST_ASSERT_EQUAL_FLOAT(240.0042, datasetVals.vrmsAverage);
}

//-----------------------------------------------------------------
// void test_ade9153a_readInstantRegisterValues()
// test raw vals null pointer return value
//-----------------------------------------------------------------
void test_ade9153a_readInstantRegisterValues_RawValsNullPtr()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;

    result = ade9153a_readInstantRegisterValues(NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_readInstantRegisterValues()
// test return value of sucessful read
//-----------------------------------------------------------------
void test_ade9153a_readInstantRegisterValues_SuccessfulRead()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    ade9153a_rawValues rawVals = { 0 };

    // ADE9153A_REG_AVRMS
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_AIRMS
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
     // ADE9153A_REG_APF
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_AWATT
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_AWATTHR_HI
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    
    result = ade9153a_readInstantRegisterValues(&rawVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}


//-----------------------------------------------------------------
// void test_ade9153a_readInstantRegisterValues()
// test return value of ERROR reading AVRMS
//-----------------------------------------------------------------
void test_ade9153a_readInstantRegisterValues_AVRMS_ReadFail()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    ade9153a_rawValues rawVals = { 0 };

    // ADE9153A_REG_AVRMS
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    result = ade9153a_readInstantRegisterValues(&rawVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_readInstantRegisterValues()
// test return value of ERROR reading AIRMS
//-----------------------------------------------------------------
void test_ade9153a_readInstantRegisterValues_AIRMS_ReadFail()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    ade9153a_rawValues rawVals = { 0 };

    // ADE9153A_REG_AVRMS
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_AIRMS
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    result = ade9153a_readInstantRegisterValues(&rawVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_ade9153a_readInstantRegisterValues()
// test return value of ERROR reading APF
//-----------------------------------------------------------------
void test_ade9153a_readInstantRegisterValues_APF_ReadFail()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    ade9153a_rawValues rawVals = { 0 };

    // ADE9153A_REG_AVRMS
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_AIRMS
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_APF
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    result = ade9153a_readInstantRegisterValues(&rawVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}
//-----------------------------------------------------------------
// void test_ade9153a_readInstantRegisterValues()
// test return value of ERROR reading AWATT
//-----------------------------------------------------------------
void test_ade9153a_readInstantRegisterValues_AWATT_ReadFail()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    ade9153a_rawValues rawVals = { 0 };

    // ADE9153A_REG_AVRMS
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_AIRMS
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_APF
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_AWATT
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    result = ade9153a_readInstantRegisterValues(&rawVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}
//-----------------------------------------------------------------
// void test_ade9153a_readInstantRegisterValues()
// test return value of ERROR reading AWATTHR_HI
//-----------------------------------------------------------------
void test_ade9153a_readInstantRegisterValues_AWATTHR_HI_ReadFail()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    ade9153a_rawValues rawVals = { 0 };

    // ADE9153A_REG_AVRMS
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_AIRMS
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_APF
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_AWATT
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    // ADE9153A_REG_AWATTHR_HI
    ade9153a_readRegister_32_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_TIMEOUT);

    result = ade9153a_readInstantRegisterValues(&rawVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

/*------------------------------ End of File -------------------------------*/

