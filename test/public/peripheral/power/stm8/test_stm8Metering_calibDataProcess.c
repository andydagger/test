#include "unity.h"
#include "stm8Metering_calibDataProcess.c"

#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_relay.h"
#include "mock_powerMonitor.h"
#include "mock_stm8Metering.h"
#include "mock_stm8Metering_impl.h"
#include "mock_stm8Metering_dataProcess.h"
#include "mock_stm8Metering_calibration.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_stm8Metering_calibrate()
// NULL pointer test
//-----------------------------------------------------------------
void test_stm8_getZeroPointCalibStatus_NullPointer()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result = stm8_getZeroPointCalibStatus(NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_processCalibSamples()
// test with null samples pointer
//-----------------------------------------------------------------
void test_stm8_processCalibSamples_NullSamplesPointer()
{
    _DEBUG_OUT_TEST_NAME
    stm8_instantValues instantVals;

    powerMonitor_Result result = stm8_processCalibSamples(NULL, &instantVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_processCalibSamples()
// test with null instant values pointer
//-----------------------------------------------------------------
void test_stm8_processCalibSamples_NullInstantValsPointer()
{
    _DEBUG_OUT_TEST_NAME
    stm8_samples samples;

    powerMonitor_Result result = stm8_processCalibSamples(&samples, NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_insertNewSample()
// test with null samples pointer
//-----------------------------------------------------------------
void test_stm8_insertNewSample_NullSamplesPointer()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result = stm8_insertNewSample( NULL, 0.1F);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_checkSampleMargins()
// test for not enough samples
//-----------------------------------------------------------------
void test_stm8_checkSampleMargins_NotEnoughSanples()
{
    _DEBUG_OUT_TEST_NAME
    stm8_calibSample samples = { 0 };

    samples.sample_count = 1;

    bool retVal = stm8_checkSampleMargins(&samples);

    TEST_ASSERT_FALSE(retVal);
}

//-----------------------------------------------------------------
// void test_stm8_checkSampleMargins()
// test result within limits
//-----------------------------------------------------------------
void test_stm8_checkSampleMargins_WithinLimits()
{
    _DEBUG_OUT_TEST_NAME

    stm8_calibSample samples = { 0 };

    samples.sample_count = STM8_REQUIRED_SAMPLE_COUNT;

    samples.samples[0] = 240.0F;
    samples.samples[1] = 240.0F;
    samples.samples[2] = 240.0F;
    samples.samples[3] = 240.0F;
    samples.samples[4] = 240.0F;
    samples.samples[5] = 240.0F;
    samples.samples[6] = 240.0F;
    samples.samples[7] = 240.0F;
    samples.samples[8] = 240.0F;
    samples.samples[9] = 240.0F;

    bool retVal = stm8_checkSampleMargins(&samples);

    TEST_ASSERT_TRUE(retVal);
}

//-----------------------------------------------------------------
// void test_stm8_checkSampleMargins()
// test result outside limits
//-----------------------------------------------------------------
void test_stm8_checkSampleMargins_OutsideLimits()
{
    _DEBUG_OUT_TEST_NAME

    stm8_calibSample samples = { 0 };

    samples.sample_count = STM8_REQUIRED_SAMPLE_COUNT;

    samples.samples[0] = 249.0F;
    samples.samples[1] = 240.0F;
    samples.samples[2] = 240.0F;
    samples.samples[3] = 240.0F;
    samples.samples[4] = 240.0F;
    samples.samples[5] = 240.0F;
    samples.samples[6] = 240.0F;
    samples.samples[7] = 240.0F;
    samples.samples[8] = 240.0F;
    samples.samples[9] = 231.0F;

    bool retVal = stm8_checkSampleMargins(&samples);

    TEST_ASSERT_FALSE(retVal);
}

//-----------------------------------------------------------------
// void test_stm8_validateSampleLimits()
// test with samples NULL pointer
//-----------------------------------------------------------------
void test_stm8_validateSampleLimits_NullPointer()
{
    _DEBUG_OUT_TEST_NAME

    uint8_t result = stm8_validateSampleLimits(NULL);

    TEST_ASSERT_EQUAL_UINT8(STM8_LIMITS_RESULT_ERROR_POINTER,result);
}

//-----------------------------------------------------------------
// void test_stm8_validateSampleLimits()
// test with volts out of range
//-----------------------------------------------------------------
void test_stm8_validateSampleLimits_VoltsOutOfRange()
{
    _DEBUG_OUT_TEST_NAME

    stm8_samples samples = { 0 };

    samples.current.calib_stored = CURRENT_CALIB_MAX;
    samples.volts.calib_stored = VOLT_CALIB_MAX+1.0F;
    samples.power.calib_stored = POWER_CALIB_MAX;

    uint8_t result = stm8_validateSampleLimits(&samples);

    TEST_ASSERT_EQUAL_UINT8(STM8_LIMITS_RESULT_ERROR_VOLTS,result);
}

//-----------------------------------------------------------------
// void test_stm8_validateSampleLimits()
// test with current out of range
//-----------------------------------------------------------------
void test_stm8_validateSampleLimits_CurrentOutOfRange()
{
    _DEBUG_OUT_TEST_NAME

    stm8_samples samples = { 0 };

    samples.current.calib_stored = CURRENT_CALIB_MAX + 0.1F;
    samples.volts.calib_stored = VOLT_CALIB_MAX - 1.0F;
    samples.power.calib_stored = POWER_CALIB_MAX - 1.0F;

    uint8_t result = stm8_validateSampleLimits(&samples);

    TEST_ASSERT_EQUAL_UINT8(STM8_LIMITS_RESULT_ERROR_CURRENT,result);
}

//-----------------------------------------------------------------
// void test_stm8_validateSampleLimits()
// test with power out of range
//-----------------------------------------------------------------
void test_stm8_validateSampleLimits_PowerOutOfRange()
{
    _DEBUG_OUT_TEST_NAME

    stm8_samples samples = { 0 };

    samples.current.calib_stored = CURRENT_CALIB_MAX;
    samples.volts.calib_stored = VOLT_CALIB_MAX;
    samples.power.calib_stored = POWER_CALIB_MAX + 1.0F;

    uint8_t result = stm8_validateSampleLimits(&samples);

    TEST_ASSERT_EQUAL_UINT8(STM8_LIMITS_RESULT_ERROR_POWER,result);
}

//-----------------------------------------------------------------
// void test_stm8_validateSampleLimits()
// test with all values in range
//-----------------------------------------------------------------
void test_stm8_validateSampleLimits_AllValuesInRange()
{
    _DEBUG_OUT_TEST_NAME

    stm8_samples samples = { 0 };

    samples.current.calib_stored = CURRENT_CALIB_MAX;
    samples.volts.calib_stored = VOLT_CALIB_MAX - 1.0F;
    samples.power.calib_stored = POWER_CALIB_MAX - 1.0F;

    uint8_t result = stm8_validateSampleLimits(&samples);

    TEST_ASSERT_EQUAL_UINT8(STM8_LIMITS_RESULT_OK,result);
}

//-----------------------------------------------------------------
// void test_stm8_calculateCoefficients()
// test with NULL meter data pointer
//-----------------------------------------------------------------
void test_stm8_calculateCoefficients_MeterDataPointerNull()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    PowerCalcDataset dataset = { 0 };

    result = stm8_calculateCoefficients(NULL, &dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_calculateCoefficients()
// test with NULL calib datset pointer
//-----------------------------------------------------------------
void test_stm8_calculateCoefficients_CalibDataPointerNull()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    PowerCalcDataset dataset = { 0 };

    result = stm8_calculateCoefficients(&dataset, NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_calculateCoefficients()
// test calculations
//-----------------------------------------------------------------
void test_stm8_calculateCoefficients()
{
    _DEBUG_OUT_TEST_NAME

    stm8_CalibCoeff coeffs = { 0 };
    PowerCalcDataset meterDataset = { 0 };
    PowerCalcDataset calibDataset = { 0 };
    powerMonitor_Result result;

    meterDataset.instantIrms = 2;
    meterDataset.instantVrms = 240;
    meterDataset.instantPower = 4;

    calibDataset.instantIrms = 1;
    calibDataset.instantVrms = 240;
    calibDataset.instantPower = 2;

    stm8_getCalibCoeffs_ExpectAndReturn(&coeffs);

    result = stm8_calculateCoefficients(&calibDataset, &meterDataset);

    TEST_ASSERT_EQUAL_FLOAT(1, coeffs.voltage);
    TEST_ASSERT_EQUAL_FLOAT(2, coeffs.current);
    TEST_ASSERT_EQUAL_FLOAT(2, coeffs.power);
    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_stm8_updateDataset()
// test values transferred to dataset
//-----------------------------------------------------------------
void test_stm8_updateDataset_xferTest()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset = { 0 };
    stm8_samples samples;

    samples.current.calib_stored = 1.0F;
    samples.volts.calib_stored = 240.0F;
    samples.power.calib_stored = 2.2F;

    stm8_updateDataset(&dataset, &samples);

    TEST_ASSERT_EQUAL_FLOAT(1.0F, dataset.instantIrms);
    TEST_ASSERT_EQUAL_FLOAT(240.0F, dataset.instantVrms);
    TEST_ASSERT_EQUAL_FLOAT(2.2F, dataset.instantPower);
}

//-----------------------------------------------------------------
// void test_stm8_updateDataset()
// test values not transferred to dataset - dataset ptr null
//-----------------------------------------------------------------
void test_stm8_updateDataset_DatasetPointerNull()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset = { 0 };
    stm8_samples samples;

    samples.current.calib_stored = 1.0F;
    samples.volts.calib_stored = 240.0F;
    samples.power.calib_stored = 2.2F;

    stm8_updateDataset(NULL, &samples);

    TEST_ASSERT_EQUAL_FLOAT(0.0F, dataset.instantIrms);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, dataset.instantVrms);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, dataset.instantPower);
}

//-----------------------------------------------------------------
// void test_stm8_updateDataset()
// test values not transferred to dataset - samples ptr null
//-----------------------------------------------------------------
void test_stm8_updateDataset_SamplesPointerNull()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset = { 0 };

    stm8_updateDataset(&dataset, NULL);

    TEST_ASSERT_EQUAL_FLOAT(0.0F, dataset.instantIrms);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, dataset.instantVrms);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, dataset.instantPower);
}

/*------------------------------ End of File -------------------------------*/
