#include "unity.h"
#include "stm8Metering_dataProcess.c"

#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_relay.h"
#include "mock_hw.h"
#include "mock_nwk_com.h"
#include "mock_powerMonitor.h"
#include "mock_stm8Metering.h"
#include "mock_stm8Metering_impl.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}


//-----------------------------------------------------------------
// void test_stm8_calcInstantValues()
// Raw pointer NULL
//-----------------------------------------------------------------
void test_stm8_calcInstantValues_RawPtrNull()
{
    _DEBUG_OUT_TEST_NAME
    stm8_CalibCoeff calibVals;
    stm8_instantValues instantVals;

    powerMonitor_Result result = stm8_calcInstantValues(NULL,
                                                        &calibVals, 
                                                        &instantVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_calcInstantValues()
// Calib pointer NULL
//-----------------------------------------------------------------
void test_stm8_calcInstantValues_CalibPtrNull()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues rawVals;
    stm8_instantValues instantVals;

    powerMonitor_Result result = stm8_calcInstantValues(&rawVals,
                                                        NULL,
                                                        &instantVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_calcInstantValues()
// instant values pointer NULL
//-----------------------------------------------------------------
void test_stm8_calcInstantValues_InstantPtrNull()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues rawVals;
    stm8_CalibCoeff calibVals;

    powerMonitor_Result result = stm8_calcInstantValues(&rawVals,
                                                        &calibVals,
                                                        NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_calcInstantValues()
// Test the calulation
//-----------------------------------------------------------------
void test_stm8_calcInstantValues()
{
    _DEBUG_OUT_TEST_NAME

    stm8_rawValues rawVals = { 0 };
    stm8_CalibCoeff calibVals = { 0 };
    stm8_instantValues instantVals = { 0 };

    calibVals.current = 1.0F;
    calibVals.power = 1.0F;
    calibVals.voltage = 1.0F;

    rawVals.ci2 = 158341069;
    rawVals.cr  = 66666666;
    rawVals.cv2 = 615869502;

    powerMonitor_Result result = stm8_calcInstantValues(&rawVals,
                                                        &calibVals,
                                                        &instantVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
    TEST_ASSERT_EQUAL_FLOAT(240, instantVals.voltage);
    TEST_ASSERT_EQUAL_FLOAT(2, instantVals.current);
    TEST_ASSERT_EQUAL_FLOAT(100, instantVals.power);
}

//-----------------------------------------------------------------
// void test_stm8_processData()
// test raw null pointer return value
//-----------------------------------------------------------------
void test_stm8_processData_RawNullPtr()
{
    _DEBUG_OUT_TEST_NAME
    stm8_avgValues avgVals;
    stm8_instantValues instantVals;
    PowerCalcDataset dataset;
    float last_power;

    powerMonitor_Result result = stm8_processData(NULL,
                                                  &avgVals,
                                                  &instantVals,
                                                  &dataset,
                                                  &last_power);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_processData()
// test average vals null pointer return value
//-----------------------------------------------------------------
void test_stm8_processData_AverageValsNullPtr()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues rawVals;
    stm8_instantValues instantVals;
    PowerCalcDataset dataset;
    float last_power;

    powerMonitor_Result result = stm8_processData(&rawVals,
                                                  NULL,
                                                  &instantVals,
                                                  &dataset,
                                                  &last_power);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_processData()
// test instant vals null pointer return value
//-----------------------------------------------------------------
void test_stm8_processData_InstantValsNullPtr()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues rawVals;
    stm8_avgValues avgVals;
    PowerCalcDataset dataset;
    float last_power;

    powerMonitor_Result result = stm8_processData(&rawVals,
                                                  &avgVals,
                                                  NULL,
                                                  &dataset,
                                                  &last_power);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_processData()
// test dataset null pointer return value
//-----------------------------------------------------------------
void test_stm8_processData_DatasetNullPtr()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues rawVals;
    stm8_avgValues avgVals;
    stm8_instantValues instantVals;
    float last_power;

    powerMonitor_Result result = stm8_processData(&rawVals,
                                                  &avgVals,
                                                  &instantVals,
                                                  NULL,
                                                  &last_power);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_processData()
// test Last power null pointer return value
//-----------------------------------------------------------------
void test_stm8_processData_LastPowerNullPtr()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues rawVals;
    stm8_avgValues avgVals;
    stm8_instantValues instantVals;
    PowerCalcDataset dataset;

    powerMonitor_Result result = stm8_processData(&rawVals,
                                                  &avgVals,
                                                  &instantVals,
                                                  &dataset,
                                                  NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}


//-----------------------------------------------------------------
// void test_stm8_processData()
// power fail detect and test of return value
//-----------------------------------------------------------------
void test_stm8_processData_PowerLossDetect()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues rawVals = { 0 };
    stm8_avgValues avgVals = { 0 };
    stm8_instantValues instantVals = { 0 };
    PowerCalcDataset dataset = { 0 };
    float last_power = 0;

    rawVals.status = STM8_STATUS_POWER_LOSS_DETECTED;

    get_sysmon_last_gasp_is_active_ExpectAndReturn(false);

    powerMonitor_Result result = stm8_processData(&rawVals,
                                                  &avgVals,
                                                  &instantVals,
                                                  &dataset,
                                                  &last_power);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_POWER_FAIL, result);
}

//-----------------------------------------------------------------
// void test_stm8_processData()
// restore power detection test of return value
//-----------------------------------------------------------------
void test_stm8_processData_PowerRestoreDetect()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues rawVals = { 0 };
    stm8_avgValues avgVals = { 0 };
    stm8_instantValues instantVals = { 0 };
    PowerCalcDataset dataset = { 0 };
    float last_power = 0;

    get_sysmon_last_gasp_is_active_ExpectAndReturn(true);

    powerMonitor_Result result = stm8_processData(&rawVals,
                                                  &avgVals,
                                                  &instantVals,
                                                  &dataset,
                                                  &last_power);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_POWER_RESTORE, result);
}

//-----------------------------------------------------------------
// void test_stm8_processData()
// power limit detection test of return value
//-----------------------------------------------------------------
void test_stm8_processData_PowerLimitDetect()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues rawVals = { 0 };
    stm8_avgValues avgVals = { 0 };
    stm8_instantValues instantVals = { 0 };
    PowerCalcDataset dataset = { 0 };
    float last_power = 0;

    instantVals.power = 5001;

    get_sysmon_last_gasp_is_active_ExpectAndReturn(false);

    powerMonitor_Result result = stm8_processData(&rawVals,
                                                  &avgVals,
                                                  &instantVals,
                                                  &dataset,
                                                  &last_power);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_DATA_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_processData()
// average values being loaded test 
//-----------------------------------------------------------------
void test_stm8_processData_averageValsTest()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues rawVals = { 0 };
    stm8_avgValues avgVals = { 0 };
    stm8_instantValues instantVals = { 0 };
    PowerCalcDataset dataset = { 0 };
    float last_power = 0;

    instantVals.voltage = 240;
    instantVals.current = 200;
    instantVals.power = 100;

    get_sysmon_last_gasp_is_active_ExpectAndReturn(false);

    powerMonitor_Result result = stm8_processData(&rawVals,
                                                  &avgVals,
                                                  &instantVals,
                                                  &dataset,
                                                  &last_power);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);

    TEST_ASSERT_EQUAL_FLOAT(100, avgVals.power);
    TEST_ASSERT_EQUAL_FLOAT(200, avgVals.current);
    TEST_ASSERT_EQUAL_FLOAT(240, avgVals.volt);
    TEST_ASSERT_EQUAL_UINT8(1, avgVals.count);
}

//-----------------------------------------------------------------
// void test_stm8_processData()
// dataset values test 
//-----------------------------------------------------------------
void test_stm8_processData_datasetValsTest()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues rawVals = { 0 };
    stm8_avgValues avgVals = { 0 };
    stm8_instantValues instantVals = { 0 };
    PowerCalcDataset dataset = { 0 };
    float last_power = 0;

    instantVals.voltage = 240;
    instantVals.current = 200;
    instantVals.power = 100;

    avgVals.count = STM8_NO_POWER_SAMPLES - 1;
    avgVals.volt = 240 * (STM8_NO_POWER_SAMPLES - 1);
    avgVals.current = 200 * (STM8_NO_POWER_SAMPLES - 1);
    avgVals.power = 100 * (STM8_NO_POWER_SAMPLES - 1);

    dataset.vrmsMin = 350;
    dataset.vrmsMax = 0;

    rawVals.status = 0x500;


    get_sysmon_last_gasp_is_active_ExpectAndReturn(false);
    vTaskSuspendAll_Expect();
    xTaskResumeAll_ExpectAndReturn(0);

    powerMonitor_Result result = stm8_processData(&rawVals,
                                                  &avgVals,
                                                  &instantVals,
                                                  &dataset,
                                                  &last_power);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);

    TEST_ASSERT_EQUAL_FLOAT(100, dataset.instantPower);
    TEST_ASSERT_EQUAL_FLOAT(200, dataset.instantIrms);
    TEST_ASSERT_EQUAL_FLOAT(240, dataset.instantVrms);
    TEST_ASSERT_EQUAL_FLOAT(240, dataset.vrmsAverage);
    TEST_ASSERT_EQUAL_FLOAT(240, dataset.vrmsMax);
    TEST_ASSERT_EQUAL_FLOAT(240, dataset.vrmsMin);
    // make sure the avg vals have been reset
    TEST_ASSERT_EQUAL_UINT8(0, avgVals.count);
    TEST_ASSERT_EQUAL_FLOAT(0, avgVals.power);
    TEST_ASSERT_EQUAL_FLOAT(0, avgVals.current);
    TEST_ASSERT_EQUAL_FLOAT(0, avgVals.volt);
}

//-----------------------------------------------------------------
// void test_stm8_noiseShaping()
// Validate return val with NULL pointer
//-----------------------------------------------------------------
void test_stm8_noiseShaping_NullPointer()
{
    _DEBUG_OUT_TEST_NAME
    
    powerMonitor_Result result = stm8_noiseShaping(NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_noiseShaping()
// Validate return val with floor values < limits force 800mW
//-----------------------------------------------------------------
void test_stm8_noiseShaping_floorValues()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues values = { 0 };
    
    values.ci2 = STM8_CURRENT_OFFSET - 1;
    values.cr = STM8_REAL_POWER_OFFSET - 1;
    values.cv2 = STM8_REAL_POWER_VOLTAGE_THRESHOLD + 1;

    powerMonitor_Result result = stm8_noiseShaping(&values);

    TEST_ASSERT_EQUAL_UINT32(0, values.ci2);
    TEST_ASSERT_EQUAL_UINT32(((STM8_REPORTED_STANDBY_POWER + 1) / STM8_POWER_CONST / 1000), values.cr);
    TEST_ASSERT_EQUAL_UINT32(STM8_REAL_POWER_VOLTAGE_THRESHOLD + 1, values.cv2);
    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_stm8_noiseShaping()
// Validate return val with values above floor 
//-----------------------------------------------------------------
void test_stm8_noiseShaping_aboveFloorValues()
{
    _DEBUG_OUT_TEST_NAME
    stm8_rawValues values = { 0 };

    values.ci2 = STM8_CURRENT_OFFSET + 2;
    values.cr = (STM8_POWER_MINIMUM_THRESHOLD / STM8_POWER_CONST / 1000);
    values.cv2 = values.cr;

    powerMonitor_Result result = stm8_noiseShaping(&values);

    TEST_ASSERT_EQUAL_UINT32(2, values.ci2);

    TEST_ASSERT_EQUAL_UINT32(((STM8_REPORTED_STANDBY_POWER + 1) / STM8_POWER_CONST / 1000), values.cr);

    TEST_ASSERT_EQUAL_UINT32(2000000, values.cv2);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}




//-----------------------------------------------------------------
// void test_stm8_validateCheckum()
// Validate return val with NULL pointer
//-----------------------------------------------------------------
void test_stm8_validateCheckum_NullPointer()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result = stm8_validateCheckum(NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

//-----------------------------------------------------------------
// void test_stm8_validateCheckum()
// validate with a valid checksum
//-----------------------------------------------------------------
void test_stm8_validateCheckum_OK()
{
    _DEBUG_OUT_TEST_NAME

    stm8_rawValues rawVals;

    rawVals.cv2 = 0xAAAA0000;
    rawVals.ci2 = 0x0000AAAA;
    rawVals.cr = 0x55550000;
    rawVals.status = 0x00005555;
    rawVals.crc = 0xFFFFFFFF;

    powerMonitor_Result result = stm8_validateCheckum(&rawVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_stm8_validateCheckum()
// validate with a valid checksum
//-----------------------------------------------------------------
void test_stm8_validateCheckum_Error()
{
    _DEBUG_OUT_TEST_NAME

    stm8_rawValues rawVals = { 0 };
    rawVals.cv2 = 0xAAAA0000;

    powerMonitor_Result result = stm8_validateCheckum(&rawVals);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_DATA_ERROR, result);
}

/*------------------------------ End of File -------------------------------*/

