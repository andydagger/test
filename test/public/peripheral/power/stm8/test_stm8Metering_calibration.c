#include "unity.h"
#include "stm8Metering_calibration.c"

#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_relay.h"
#include "mock_powerMonitor.h"
#include "mock_stm8Metering.h"
#include "mock_stm8Metering_impl.h"
#include "mock_stm8Metering_dataProcess.h"
#include "mock_stm8Metering_calibDataProcess.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_stm8Metering_calibrate()
// NULL pointer test
//-----------------------------------------------------------------
void test_stm8Metering_calibrate_NullPointer()
{
    _DEBUG_OUT_TEST_NAME
    
    powerMonitor_Result result = stm8Metering_calibrate(NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);

}

//-----------------------------------------------------------------
// void test_stm8Metering_calibrate()
// test for calibration timeout
//-----------------------------------------------------------------
void test_stm8Metering_calibrateTimeout()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset;
    // Start time init
    get_systick_sec_ExpectAndReturn(0);
    get_systick_sec_ExpectAndReturn(0);

    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn( STM8_CALIBRATION_TIMEOUT + 2);

    powerMonitor_Result result = stm8Metering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_stm8Metering_calibrate()
// test for calibration sample timeout
//-----------------------------------------------------------------
void test_stm8Metering_calibrateSampleTimeout()
{
    _DEBUG_OUT_TEST_NAME
        PowerCalcDataset dataset;
    // Start time init
    get_systick_sec_ExpectAndReturn(0);
    get_systick_sec_ExpectAndReturn(0);

    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(STM8_CALIBRATION_SAMPLE_TIMEOUT + 2);

    stm8_isMessageReceived_ExpectAndReturn(false);

    powerMonitor_Result result = stm8Metering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_stm8Metering_calibrate()
// test the state machine and exiting the cal with a pass
//-----------------------------------------------------------------
void test_stm8Metering_calibrateProcessWithPass()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset = { 0 };
    uint8_t rx_buffer[16];
    stm8_CalibCoeff calibCoeffs;
    
    // Start time init
    get_systick_sec_ExpectAndReturn(0);
    get_systick_sec_ExpectAndReturn(0);

    /* 1st time rounf loop */
    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(2);

    stm8_isMessageReceived_ExpectAndReturn(true);

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    relay_update_wait_ExpectAnyArgs();
    
    stm8_sendResponse_ExpectAnyArgs();
    
    /* 2nd time round loop */
    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(4);

    stm8_isMessageReceived_ExpectAndReturn(true);

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getZeroPointCalibStatus_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    /* 3rd time round loop */
    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(6);

    stm8_isMessageReceived_ExpectAndReturn(true);

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_processCalibSamples_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_updateDataset_ExpectAnyArgs();

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    powerMonitor_Result result = stm8Metering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
    TEST_ASSERT_EQUAL_UINT32(STM8_CALIB_STATE_FINISHED, stm8_state);
}

//-----------------------------------------------------------------
// void test_stm8Metering_calibrate()
// test the state machine and exiting the cal with a fail
//-----------------------------------------------------------------
void test_stm8Metering_calibrateProcessWithFail()
{
    _DEBUG_OUT_TEST_NAME
        PowerCalcDataset dataset = { 0 };
    uint8_t rx_buffer[16];
    stm8_CalibCoeff calibCoeffs;

    // Start time init
    get_systick_sec_ExpectAndReturn(0);
    get_systick_sec_ExpectAndReturn(0);

    /* 1st time rounf loop */
    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(2);

    stm8_isMessageReceived_ExpectAndReturn(true);

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    /* 2nd time round loop */
    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(4);

    stm8_isMessageReceived_ExpectAndReturn(true);

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getZeroPointCalibStatus_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    /* 3rd time round loop */
    FreeRTOSDelay_ExpectAnyArgs();

    get_systick_sec_ExpectAndReturn(6);

    stm8_isMessageReceived_ExpectAndReturn(true);

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_processCalibSamples_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_CALIBRATION_FAILED);

    stm8_updateDataset_ExpectAnyArgs();

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    powerMonitor_Result result = stm8Metering_calibrate(&dataset);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_CALIBRATION_FAILED, result);
    TEST_ASSERT_EQUAL_UINT32(STM8_CALIB_STATE_FAILED, stm8_state);
}




//-----------------------------------------------------------------
// void test_stm8_calibProcessRxMessage()
// test the zero point cal moves on to wait for cal
//-----------------------------------------------------------------
void test_stm8_calibProcessRxMessage_ZeroPointCal()
{
    _DEBUG_OUT_TEST_NAME
    uint8_t rx_buffer[16];
    stm8_CalibCoeff calibCoeffs;

    stm8_state = STM8_CALIB_STATE_ZERO_POINT_CAL;

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    powerMonitor_Result result = stm8_calibProcessRxMessage();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_CALIBRATION_ACTIVE, result);
    TEST_ASSERT_EQUAL_UINT32(STM8_CALIB_STATE_WAIT_ZERO_POINT_CAL, stm8_state);
}

//-----------------------------------------------------------------
// void test_stm8_calibProcessRxMessage()
// test the zero point cal wait ends and closes the relay
//-----------------------------------------------------------------
void test_stm8_calibProcessRxMessage_ZeroPointCalFinished()
{
    _DEBUG_OUT_TEST_NAME
        uint8_t rx_buffer[16];
    stm8_CalibCoeff calibCoeffs;

    stm8_state = STM8_CALIB_STATE_WAIT_ZERO_POINT_CAL;

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getCalibCoeffs_ExpectAndReturn(&calibCoeffs);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_getZeroPointCalibStatus_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    relay_update_wait_ExpectAnyArgs();

    stm8_sendResponse_ExpectAnyArgs();

    powerMonitor_Result result = stm8_calibProcessRxMessage();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_CALIBRATION_ACTIVE, result);
    TEST_ASSERT_EQUAL_UINT32(STM8_CALIB_STATE_CALIB_CALC, stm8_state);
}

//-----------------------------------------------------------------
// void test_stm8_calibProcessRxMessage()
// process rx message with a checksum error
//-----------------------------------------------------------------
void test_stm8_calibProcessRxMessage_ChecksumError()
{
    _DEBUG_OUT_TEST_NAME
        uint8_t rx_buffer[16];
    stm8_CalibCoeff calibCoeffs;

    stm8_state = STM8_CALIB_STATE_WAIT_ZERO_POINT_CAL;

    stm8_resetMessageReceived_Expect();

    stm8_getRXBuffer_ExpectAndReturn(rx_buffer);

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_DATA_ERROR);

    powerMonitor_Result result = stm8_calibProcessRxMessage();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_DATA_ERROR, result);
    TEST_ASSERT_EQUAL_UINT32(STM8_CALIB_STATE_WAIT_ZERO_POINT_CAL, stm8_state);
}
/*------------------------------ End of File -------------------------------*/

