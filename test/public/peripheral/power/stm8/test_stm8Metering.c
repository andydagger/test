#include "unity.h"
#include "stm8Metering.c"

#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_powerMonitor.h"
#include "mock_stm8Metering_impl.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_stm8Metering_init()
// Test flag and last sample time being initialised
//-----------------------------------------------------------------
void test_stm8Metering_init()
{
    _DEBUG_OUT_TEST_NAME

    b_power_sampling_fault_latch = true;

    stm8_init_Expect();

    get_systick_sec_ExpectAndReturn(0x100);

    stm8Metering_init();

    TEST_ASSERT_EQUAL_UINT32( 0x100, stm8_last_sample_time);

    TEST_ASSERT_FALSE( b_power_sampling_fault_latch);
}

//-----------------------------------------------------------------
// void test_stm8Metering_initMeasurement()
// 
//-----------------------------------------------------------------
void test_stm8Metering_initMeasurement()
{
    _DEBUG_OUT_TEST_NAME

    PowerCalcDataset dataset;
    
    stm8_initMeasurement_Expect(&dataset);

    stm8Metering_initMeasurement(&dataset);
}

//-----------------------------------------------------------------
// void test_stm8Metering_uartISR()
// 
//-----------------------------------------------------------------
void test_stm8Metering_uartISR()
{
    _DEBUG_OUT_TEST_NAME

    stm8_uartISR_Expect();

    stm8Metering_uartISR();
}

//-----------------------------------------------------------------
// void test_stm8Metering_service()
// test message timeout with last gasp not active
//-----------------------------------------------------------------
void test_stm8Metering_serviceTimeoutLastGaspNotActive()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;
    
    stm8_last_sample_time = 0;
    b_power_sampling_fault_latch = false;

    stm8_isMessageReceived_ExpectAndReturn(false);

    get_systick_sec_ExpectAndReturn(STM8_POWER_SAMPLE_TIMEOUT_SEC + 1);

    get_sysmon_last_gasp_is_active_ExpectAndReturn(false);

    result = stm8Metering_service();

    TEST_ASSERT_TRUE(b_power_sampling_fault_latch);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_TIMEOUT, result);
}

//-----------------------------------------------------------------
// void test_stm8Metering_service()
// test message timeout with last gasp not active, fault latch true
//-----------------------------------------------------------------
void test_stm8Metering_serviceTimeoutLastGaspNotActiveFaultLatchTrue()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;

    stm8_last_sample_time = 0;
    b_power_sampling_fault_latch = true;

    stm8_isMessageReceived_ExpectAndReturn(false);

    get_systick_sec_ExpectAndReturn(STM8_POWER_SAMPLE_TIMEOUT_SEC + 1);

    get_sysmon_last_gasp_is_active_ExpectAndReturn(false);

    result = stm8Metering_service();

    TEST_ASSERT_TRUE(b_power_sampling_fault_latch);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_IDLE, result);
}

//-----------------------------------------------------------------
// void test_stm8Metering_service()
// test message timeout with last gasp active
//-----------------------------------------------------------------
void test_stm8Metering_serviceTimeoutLastGaspActive()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;

    stm8_last_sample_time = 0;
    b_power_sampling_fault_latch = false;

    stm8_isMessageReceived_ExpectAndReturn(false);

    get_systick_sec_ExpectAndReturn(STM8_POWER_SAMPLE_TIMEOUT_SEC + 1);

    get_sysmon_last_gasp_is_active_ExpectAndReturn(true);

    result = stm8Metering_service();

    TEST_ASSERT_FALSE(b_power_sampling_fault_latch);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_IDLE, result);
}

//-----------------------------------------------------------------
// void test_stm8Metering_service()
// test message received
//-----------------------------------------------------------------
void test_stm8Metering_serviceMessageReceived()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_Result result;

    stm8_last_sample_time = 0;
    b_power_sampling_fault_latch = true;

    stm8_isMessageReceived_ExpectAndReturn(true);

    get_systick_sec_ExpectAndReturn(0x100);

    stm8_processReceivedMessage_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    result = stm8Metering_service();

    TEST_ASSERT_FALSE(b_power_sampling_fault_latch);

    TEST_ASSERT_EQUAL_UINT32(0x100, stm8_last_sample_time);
    
    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

/*------------------------------ End of File -------------------------------*/

