#include "unity.h"
#include "stm8Metering_impl.c"

#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_config_eeprom.h"
#include "mock_sysmon.h"
#include "mock_relay.h"
#include "mock_hw.h"
#include "mock_nwk_com.h"
#include "mock_NonVolatileMemory.h"
#include "mock_NVMData.h"
#include "mock_powerMonitor.h"
#include "mock_stm8Metering_dataProcess.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_stm8_init()
// Make sure vas are initialised correctly, baud rate set and uart irq enabled
//-----------------------------------------------------------------
void test_stm8_init()
{
    _DEBUG_OUT_TEST_NAME

    stm8_message_received = true;
    stm8_comms_tx_message_count = 10;
    stm8_comms_tx_index = 10;
    stm8_comms_rx_index = 10;
    prev_byte_ms_tick_count = 0;

    b_power_self_test_not_completed = true;
    b_stm8_lorawan_joined = true;

    get_systick_ms_ExpectAndReturn(0x100);

    hw_setup_power_monitor_uart_params_Expect(STM8_UART_BAUD_RATE, STM8_UART_COMM_PARAMS);

    hw_power_monitor_uart_irq_enable_Expect();

    stm8_init();

    TEST_ASSERT_FALSE(stm8_message_received);
    TEST_ASSERT_EQUAL_UINT8(0, stm8_comms_tx_message_count);
    TEST_ASSERT_EQUAL_UINT8(0, stm8_comms_tx_index);
    TEST_ASSERT_EQUAL_UINT8(0, stm8_comms_rx_index);
 
    TEST_ASSERT_EQUAL_UINT32( 0x100, prev_byte_ms_tick_count);

    TEST_ASSERT_FALSE(b_power_self_test_not_completed);
    TEST_ASSERT_FALSE(b_stm8_lorawan_joined);
}

//-----------------------------------------------------------------
// void test_stm8_initMeasurement()
// 
//-----------------------------------------------------------------
void test_stm8_initMeasurement()
{
    _DEBUG_OUT_TEST_NAME
    
    PowerCalcDataset dataset;
 
    b_have_power_data = true;

    last_power_sample = 10.0F;

    flags.data_stm = 10;

    // reset the averaging vars.
    averagingValues.count = 10;
    averagingValues.volt = 10.0F;
    averagingValues.current = 10.0F;
    averagingValues.power = 10.0F;

    NVMData_readMeteringCalibCoef_ExpectAnyArgsAndReturn( NVM_RESULT_OK);

    stm8_initMeasurement(&dataset);

    TEST_ASSERT_EQUAL_PTR(&dataset, dataset_ptr);
    TEST_ASSERT_FALSE(b_have_power_data);

    TEST_ASSERT_EQUAL_UINT32(0, flags.data_stm);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, last_power_sample);

    TEST_ASSERT_EQUAL_UINT8(0, averagingValues.count);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, averagingValues.volt);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, averagingValues.current);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, averagingValues.power);
}

//-----------------------------------------------------------------
// void test_stm8Metering_uartISR()
// Test data byte received path - no timeout, not reached buffer length
//-----------------------------------------------------------------
void test_stm8_uartISR_RXD()
{
    _DEBUG_OUT_TEST_NAME
    prev_byte_ms_tick_count = 0;
    stm8_message_received = false;
    stm8_comms_rx_index = 0;

    hw_power_monitor_uart_get_int_status_ExpectAndReturn(UART_STAT_RXRDY);

    hw_power_monitor_uart_rx_byte_ExpectAndReturn(0x5A);

    get_systick_ms_ExpectAndReturn(STM8_INTER_BYTE_MAX_DELAY_MS);

    get_systick_ms_ExpectAndReturn(STM8_INTER_BYTE_MAX_DELAY_MS);

    stm8_uartISR();

    TEST_ASSERT_EQUAL_UINT8(0x5A, stm8_rx_buffer[0]);
    TEST_ASSERT_EQUAL_UINT32(STM8_INTER_BYTE_MAX_DELAY_MS, prev_byte_ms_tick_count);
}

//-----------------------------------------------------------------
// void test_stm8Metering_uartISR()
// Test with a message already received
//-----------------------------------------------------------------
void test_stm8_uartISR_RXD_messageAlreadyRXD()
{
    _DEBUG_OUT_TEST_NAME
    prev_byte_ms_tick_count = 0;
    stm8_message_received = true;
    stm8_comms_rx_index = 0;

    hw_power_monitor_uart_get_int_status_ExpectAndReturn(UART_STAT_RXRDY);

    hw_power_monitor_uart_rx_byte_ExpectAndReturn(0x5A);

    get_systick_ms_ExpectAndReturn(STM8_INTER_BYTE_MAX_DELAY_MS);

    stm8_uartISR();
}

//-----------------------------------------------------------------
// void test_stm8Metering_uartISR()
// Test data byte received path - RX buffer full no message to send
//-----------------------------------------------------------------
void test_stm8_uartISR_RXD_bufferFullNoTXD()
{
    _DEBUG_OUT_TEST_NAME
    prev_byte_ms_tick_count = 0;
    stm8_message_received = false;
    stm8_comms_rx_index = STM8_RX_BUFF_SIZE - 1;
    stm8_comms_tx_message_count = 0;
    stm8_comms_tx_index = 10;

    hw_power_monitor_uart_get_int_status_ExpectAndReturn(UART_STAT_RXRDY);

    hw_power_monitor_uart_rx_byte_ExpectAndReturn(0x5A);

    get_systick_ms_ExpectAndReturn(STM8_INTER_BYTE_MAX_DELAY_MS);

    get_systick_ms_ExpectAndReturn(STM8_INTER_BYTE_MAX_DELAY_MS);

    stm8_uartISR();

    TEST_ASSERT_EQUAL_UINT8(0, stm8_comms_rx_index);
    TEST_ASSERT_EQUAL_UINT8(0, stm8_comms_tx_index);
    TEST_ASSERT_EQUAL_UINT8(0x5A, stm8_rx_buffer[STM8_RX_BUFF_SIZE - 1]);
    TEST_ASSERT_TRUE(stm8_message_received);
    TEST_ASSERT_EQUAL_UINT32(STM8_INTER_BYTE_MAX_DELAY_MS, prev_byte_ms_tick_count);
}


//-----------------------------------------------------------------
// void test_stm8Metering_uartISR()
// Test data byte received path - RX buffer full start message TXD
//-----------------------------------------------------------------
void test_stm8_uartISR_RXD_bufferFullStartTXD()
{
    _DEBUG_OUT_TEST_NAME
    prev_byte_ms_tick_count = 0;
    stm8_message_received = false;
    stm8_comms_rx_index = STM8_RX_BUFF_SIZE - 1;
    stm8_comms_tx_message_count = 0;
    stm8_comms_tx_index = 10;
    stm8_comms_tx_message_count = 4;

    hw_power_monitor_uart_get_int_status_ExpectAndReturn(UART_STAT_RXRDY);

    hw_power_monitor_uart_rx_byte_ExpectAndReturn(0x5A);

    get_systick_ms_ExpectAndReturn(STM8_INTER_BYTE_MAX_DELAY_MS);

    get_systick_ms_ExpectAndReturn(STM8_INTER_BYTE_MAX_DELAY_MS);

    hw_power_monitor_uart_tx_enable_Expect();

    stm8_uartISR();

    TEST_ASSERT_EQUAL_UINT8(0, stm8_comms_rx_index);
    TEST_ASSERT_EQUAL_UINT8(0x5A, stm8_rx_buffer[STM8_RX_BUFF_SIZE - 1]);
    TEST_ASSERT_TRUE(stm8_message_received);
    TEST_ASSERT_EQUAL_UINT32(STM8_INTER_BYTE_MAX_DELAY_MS, prev_byte_ms_tick_count);

    TEST_ASSERT_EQUAL_UINT8(0, stm8_comms_tx_index);
    TEST_ASSERT_EQUAL_UINT8(3, stm8_comms_tx_message_count);
}

//-----------------------------------------------------------------
// void test_stm8Metering_uartISR()
// Test data byte TXD path
//-----------------------------------------------------------------
void test_stm8_uartISR_TXD()
{
    _DEBUG_OUT_TEST_NAME

    stm8_comms_tx_index = 0;
    
    hw_power_monitor_uart_get_int_status_ExpectAndReturn(UART_STAT_TXRDY);

    hw_power_monitor_uart_tx_byte_ExpectAnyArgs();

    stm8_uartISR();

    TEST_ASSERT_EQUAL_UINT8(1, stm8_comms_tx_index);
}

//-----------------------------------------------------------------
// void test_stm8Metering_uartISR()
// Test data byte TXD path
//-----------------------------------------------------------------
void test_stm8_uartISR_TXD_BufferEnd()
{
    _DEBUG_OUT_TEST_NAME

    stm8_comms_tx_index = STM8_TX_BUFF_SIZE-1;

    hw_power_monitor_uart_get_int_status_ExpectAndReturn(UART_STAT_TXRDY);

    hw_power_monitor_uart_tx_byte_ExpectAnyArgs();

    hw_power_monitor_uart_tx_disable_Expect();

    stm8_uartISR();

    TEST_ASSERT_EQUAL_UINT8(0, stm8_comms_tx_index);
}


//-----------------------------------------------------------------
// void test_stm8_getRXBuffer()
// tests the address of the rx buffer returned is correct
//-----------------------------------------------------------------
void test_stm8_getRXBuffer()
{
    _DEBUG_OUT_TEST_NAME
    
    uint8_t* data_ptr = NULL;

    data_ptr = stm8_getRXBuffer();

    TEST_ASSERT_EQUAL_PTR(&stm8_rx_buffer, data_ptr);
}

//-----------------------------------------------------------------
// void test_stm8_getCalibCoeffs()
// tests if the address of the calibration vals is correct
//-----------------------------------------------------------------
void test_stm8_getCalibCoeffs()
{
    _DEBUG_OUT_TEST_NAME

    stm8_CalibCoeff* calib_ptr = NULL;

    calib_ptr = stm8_getCalibCoeffs();

    TEST_ASSERT_EQUAL_PTR(&calibCoeffs, calib_ptr);
}

//-----------------------------------------------------------------
// void test_stm8_txMessage()
// 
//-----------------------------------------------------------------
void test_stm8_txMessage()
{
    _DEBUG_OUT_TEST_NAME

    uint32_t dataValue = 0x12345678;

    stm8_comms_tx_index = 10;
    stm8_comms_tx_message_count = 0;

    stm8_txMessage(&dataValue);

    TEST_ASSERT_EQUAL_UINT8(0, stm8_comms_tx_index);
    TEST_ASSERT_EQUAL_UINT8(NO_OF_STM8_TX, stm8_comms_tx_message_count);
    // this check is endian specific
    TEST_ASSERT_EQUAL_UINT8(0x78, stm8_tx_buffer[STM8_CRC_POS]);
    TEST_ASSERT_EQUAL_UINT8(0x56, stm8_tx_buffer[STM8_CRC_POS + 1]);
    TEST_ASSERT_EQUAL_UINT8(0x34, stm8_tx_buffer[STM8_CRC_POS + 2]);
    TEST_ASSERT_EQUAL_UINT8(0x12, stm8_tx_buffer[STM8_CRC_POS + 3]);
    TEST_ASSERT_EQUAL_UINT8(0x78, stm8_tx_buffer[STM8_FLAGS_POS]);
    TEST_ASSERT_EQUAL_UINT8(0x56, stm8_tx_buffer[STM8_FLAGS_POS + 1]);
    TEST_ASSERT_EQUAL_UINT8(0x34, stm8_tx_buffer[STM8_FLAGS_POS + 2]);
    TEST_ASSERT_EQUAL_UINT8(0x12, stm8_tx_buffer[STM8_FLAGS_POS + 3]);
}

//-----------------------------------------------------------------
// void test_stm8_isMessageReceived()
// test true is returned
//-----------------------------------------------------------------
void test_stm8_isMessageReceivedAsTrue()
{
    _DEBUG_OUT_TEST_NAME
    
    stm8_message_received = true;

    bool retVal = stm8_isMessageReceived();

    TEST_ASSERT_TRUE(retVal);
}

//-----------------------------------------------------------------
// void test_stm8_isMessageReceived()
// test false is returned
//-----------------------------------------------------------------
void test_stm8_isMessageReceivedAsFalse()
{
    _DEBUG_OUT_TEST_NAME

    stm8_message_received = false;

    bool retVal = stm8_isMessageReceived();

    TEST_ASSERT_FALSE(retVal);
}

//-----------------------------------------------------------------
// void test_stm8_resetMessageReceived()
// test false is returned
//-----------------------------------------------------------------
void test_stm8_resetMessageReceived()
{
    _DEBUG_OUT_TEST_NAME

    stm8_message_received = true;
    stm8_comms_rx_index = 10;

    stm8_resetMessageReceived();

    TEST_ASSERT_FALSE(stm8_message_received);
    TEST_ASSERT_EQUAL_UINT8(0, stm8_comms_rx_index);
}

//-----------------------------------------------------------------
// void test_stm8_sendResponse()
// test for byte data_3 being zero
//-----------------------------------------------------------------
void test_stm8_sendResponseData3_Zero()
{
    _DEBUG_OUT_TEST_NAME
    
    b_stm8_lorawan_joined = false;
    
    b_get_relay_state_ExpectAndReturn(true);

    get_nwk_com_is_connected_ExpectAndReturn(true);

    stm8_sendResponse(STM8_REQUEST_NONE);
   
    TEST_ASSERT_EQUAL_UINT8(0, flags.byte.data_3);

    TEST_ASSERT_TRUE(b_stm8_lorawan_joined);
}

//-----------------------------------------------------------------
// void test_stm8_sendResponse()
// test for byte data_3 being STM8_LOAD_RELAY_CLOSED
//-----------------------------------------------------------------
void test_stm8_sendResponseData3_relayClosed()
{
    _DEBUG_OUT_TEST_NAME

    b_stm8_lorawan_joined = false;

    b_get_relay_state_ExpectAndReturn(false);

    get_nwk_com_is_connected_ExpectAndReturn(true);

    stm8_sendResponse(STM8_REQUEST_NONE);

    TEST_ASSERT_EQUAL_UINT8(STM8_LOAD_RELAY_CLOSED, flags.byte.data_3);
    
    TEST_ASSERT_TRUE(b_stm8_lorawan_joined);
}

//-----------------------------------------------------------------
// void test_stm8_sendResponse()
// test for byte data_3 being STM8_LIGHT_RED_LED
//-----------------------------------------------------------------
void test_stm8_sendResponseData3_redLed()
{
    _DEBUG_OUT_TEST_NAME

    b_stm8_lorawan_joined = false;

    b_get_relay_state_ExpectAndReturn(true);

    get_nwk_com_is_connected_ExpectAndReturn(false);

    stm8_sendResponse(STM8_REQUEST_NONE);

    TEST_ASSERT_EQUAL_UINT8(STM8_LIGHT_RED_LED, flags.byte.data_3);
    
    TEST_ASSERT_FALSE(b_stm8_lorawan_joined);
}

//-----------------------------------------------------------------
// void test_stm8_sendResponse()
// test for byte data_3 being STM8_RESET_DISCARD_CALIB
//-----------------------------------------------------------------
void test_stm8_sendResponseData3_zeroCal()
{
    _DEBUG_OUT_TEST_NAME

    b_stm8_lorawan_joined = false;

    b_get_relay_state_ExpectAndReturn(true);

    get_nwk_com_is_connected_ExpectAndReturn(true);

    stm8_sendResponse(STM8_REQUEST_ZERO_CAL);

    TEST_ASSERT_EQUAL_UINT8(STM8_RESET_DISCARD_CALIB, flags.byte.data_3);

    TEST_ASSERT_TRUE(b_stm8_lorawan_joined);
}

//-----------------------------------------------------------------
// void test_stm8_sendResponse()
// test for byte data_3 being all flags set
//-----------------------------------------------------------------
void test_stm8_sendResponseData3_AllFlagsSet()
{
    _DEBUG_OUT_TEST_NAME

    b_stm8_lorawan_joined = false;

    b_get_relay_state_ExpectAndReturn(false);

    get_nwk_com_is_connected_ExpectAndReturn(false);

    stm8_sendResponse(STM8_REQUEST_ZERO_CAL);

    TEST_ASSERT_EQUAL_UINT8(STM8_LOAD_RELAY_CLOSED|STM8_LIGHT_RED_LED|STM8_RESET_DISCARD_CALIB, flags.byte.data_3);

    TEST_ASSERT_FALSE(b_stm8_lorawan_joined);
}

//-----------------------------------------------------------------
// void test_stm8_processReceivedMessage()
// 
//-----------------------------------------------------------------
void test_stm8_processReceivedMessage()
{
    _DEBUG_OUT_TEST_NAME
    powerMonitor_Result result;
 
    //stm8_resetMessageReceived_Expect();

    stm8_validateCheckum_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_noiseShaping_ExpectAnyArgsAndReturn( POWER_MONITOR_RESULT_OK);

    b_get_relay_state_ExpectAndReturn(false);

    get_nwk_com_is_connected_ExpectAndReturn(false);

    //stm8_sendResponse_Expect(STM8_REQUEST_NONE);

    stm8_calcInstantValues_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    stm8_processData_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);

    result = stm8_processReceivedMessage();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_stm8_selfTestNotCompleted()
//  test the return value is true
//-----------------------------------------------------------------
void test_stm8_selfTestNotCompletedAsTrue()
{
    _DEBUG_OUT_TEST_NAME

    b_power_self_test_not_completed = true;

    bool retVal = stm8_selfTestNotCompleted();

    TEST_ASSERT_TRUE(retVal);
}

//-----------------------------------------------------------------
// void test_stm8_selfTestNotCompleted()
//  test the return value is false
//-----------------------------------------------------------------
void test_stm8_selfTestNotCompletedAsFalse()
{
    _DEBUG_OUT_TEST_NAME

    b_power_self_test_not_completed = false;

    bool retVal = stm8_selfTestNotCompleted();

    TEST_ASSERT_FALSE(retVal);
}

//-----------------------------------------------------------------
// void test_stm8_getPowerCalcPowerSample()
// 
//-----------------------------------------------------------------
void test_stm8_getPowerCalcPowerSample()
{
    _DEBUG_OUT_TEST_NAME

    float localValue;

    last_power_sample = 1234.56F;

    localValue = stm8_getPowerCalcPowerSample();

    TEST_ASSERT_EQUAL_FLOAT(1234.56F, localValue);
}

//-----------------------------------------------------------------
// void test_stm8_getPowerVersionFlags()
// 
//-----------------------------------------------------------------
void test_stm8_getPowerVersionFlags()
{
    _DEBUG_OUT_TEST_NAME
    stm8_status localStatus;

    status_data.status = 0x12345678;
   
    vTaskSuspendAll_Expect();
    xTaskResumeAll_ExpectAndReturn(0);
    
    stm8_getPowerVersionFlags( (uint8_t*)&localStatus);

    TEST_ASSERT_EQUAL_UINT32(0x12345678, localStatus.status);
}

//-----------------------------------------------------------------
// void test_stm8_getFirmwareVersion()
// 
//-----------------------------------------------------------------
void test_stm8_getFirmwareVersion()
{
    _DEBUG_OUT_TEST_NAME
    
    uint16_t ver = 0;

    status_data.bytes.major = 0x12;
    status_data.bytes.minor = 0x34;

    vTaskSuspendAll_Expect();
    xTaskResumeAll_ExpectAndReturn(0);

    ver = stm8_getFirmwareVersion();

    TEST_ASSERT_EQUAL_UINT16(0x1234, ver);
}


/*------------------------------ End of File -------------------------------*/

