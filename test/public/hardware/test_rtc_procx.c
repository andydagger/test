#include "unity.h"
#include "stdbool.h"
#include "FreeRTOS.h"
#include "rtc_procx.h"
#include "mock_task.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{

}

void tearDown(void)
{

}

void setDateTimeStruct( RTCTime * p_TestStruct,
                        uint32_t RTC_Sec,
                        uint32_t RTC_Min,
                        uint32_t RTC_Hour,
                        uint32_t RTC_Mday,
                        uint32_t RTC_Mon,
                        uint32_t RTC_Year,
                        uint32_t RTC_Wday,
                        uint32_t RTC_Yday)
{
    p_TestStruct->RTC_Sec = RTC_Sec;
    p_TestStruct->RTC_Min = RTC_Min;
    p_TestStruct->RTC_Hour = RTC_Hour;
    p_TestStruct->RTC_Mday = RTC_Mday;
    p_TestStruct->RTC_Mon = RTC_Mon;
    p_TestStruct->RTC_Year = RTC_Year;
    p_TestStruct->RTC_Yday = RTC_Yday;
}

void test_basic_set_and_read_back_counter(void)
{
_DEBUG_OUT_TEST_NAME
        rtc_procx_SetCounter(1568204202);
        TEST_ASSERT_EQUAL_UINT32(1568204202, rtc_procx_GetCounter());
        rtc_procx_SetCounter(0);
        TEST_ASSERT_EQUAL_UINT32(0, rtc_procx_GetCounter());
}

void test_counter_increment_and_read_back(void)
{
_DEBUG_OUT_TEST_NAME
        rtc_procx_SetCounter(1568204202);
        rtc_procx_IncCounter();
        TEST_ASSERT_EQUAL_UINT32(1568204202+1, rtc_procx_GetCounter());
}

void test_datetime_struct_day_increment(void)
{
_DEBUG_OUT_TEST_NAME
        RTCTime test_struct;

        rtc_procx_SetDstIsApplicable(true);

        rtc_procx_SetCounter(1568204202);
        TEST_ASSERT_EQUAL_UINT32(1568204202, rtc_procx_GetCounter());

        test_struct = rtc_procx_GetTime();
        test_struct.RTC_Mday += 1;
        rtc_procx_SetTime(test_struct);

        TEST_ASSERT_EQUAL_UINT32(1568290602, rtc_procx_GetCounter());


}

/* All Epoch/timestamps have been validated, date: 20/09/2019 */
void test_conversionWithoutDst(void)
{
_DEBUG_OUT_TEST_NAME
    RTCTime test_struct;
    uint32_t test_unix;

    rtc_procx_SetDstIsApplicable(false);

    /* 1568204202 Epoch is: Wednesday, 11 September 2019 12:16:42 (GMT) */
    rtc_procx_SetCounter(1568204202);
    TEST_ASSERT_EQUAL_UINT32(1568204202, rtc_procx_GetCounter());

    test_struct = rtc_procx_GetTime();
    test_struct.RTC_Mday += 1;
    rtc_procx_SetTime(test_struct);
    /* Thursday, 12 September 2019 12:16:42 (GMT) */
    TEST_ASSERT_EQUAL_UINT32(1568290602, rtc_procx_GetCounter());

    /* YEAR 2019: */
    /* 12:00:00 01-01-2019 TUES     1546344000 */
    test_unix = 1546344000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 01, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/01/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 01-02-2019 FRI  1549022400 */
    test_unix = 1549022400;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 2, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/02/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 28-02-2019 THU  1551355200  Test leap year */
    test_unix = 1551355200;
    setDateTimeStruct( &test_struct, 00, 00, 12, 28, 2, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/02/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 29-02-2019   1551441600 Test leap year - This date should not exist */
    /* EPOCH should return: 12:00:00 01-03-2019 */
    test_unix = 1551441600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 29, 2, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    // Note: Use STRNE (not equal) as this date should not exist. 2019 is not a leap year
    // TEST_ASSERT_EQUAL_STRING_MESSAGE("29/02/2019 12:00:00", getRtcAsString(), "Expected to fail!");
    // Should equal the following day:
    TEST_ASSERT_EQUAL_STRING("01/03/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 01-03-2019 FRI  1551441600 */
    test_unix = 1551441600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 3, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/03/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 30-03-2019 SAT  1553947200 */
    test_unix = 1553947200;
    setDateTimeStruct( &test_struct, 00, 00, 12, 30, 3, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("30/03/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 00:00:00 31-03-2019  1553990400*/
    test_unix = 1553990400;
    setDateTimeStruct( &test_struct, 00, 00, 00, 31, 3, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/03/2019 00:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 01:00:00 31-03-2019 SUN  1553994000 (DST would normally start) */
    test_unix = 1553994000;
    setDateTimeStruct( &test_struct, 00, 00, 01, 31, 3, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/03/2019 01:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 02:00:00 31-03-2019 SUN  1553997600 */
    test_unix = 1553997600;
    setDateTimeStruct( &test_struct, 00, 00, 02, 31, 3, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/03/2019 02:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 31-05-2019 FRI 1559304000 */
    test_unix = 1559304000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 31, 5, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/05/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

////////////////////////////////////////////////////////////////

    /* GMT 11:00:00 31-07-2019 WED */
    test_unix = 1564570800;
    setDateTimeStruct( &test_struct, 00, 00, 11, 31, 7, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/07/2019 11:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* GMT 11:00:00 26-10-2019 SAT  1572087600  Day before DST */
    test_unix = 1572087600;
    setDateTimeStruct( &test_struct, 00, 00, 11, 26, 10, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("26/10/2019 11:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* GMT 01:59:59 27-10-2019 SUN  1572137999 */
    test_unix = 1572141599;
    setDateTimeStruct( &test_struct, 59, 59, 01, 27, 10, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("27/10/2019 01:59:59", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 02:00:00 27-10-2019 SUN  1572141600  DST ends */
    test_unix = 1572141600;
    setDateTimeStruct( &test_struct, 00, 00, 02, 27, 10, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("27/10/2019 02:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* GMT 12:00:00 27-10-2019 SUN  1572177600 */
    test_unix = 1572177600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 27, 10, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("27/10/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 31-12-2019 TUES 1577793600 */
    test_unix = 1577793600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 31, 12, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/12/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 2020 - Leap year */
    /* 12:00:00 01-01-2020 WED  1577880000 */
    test_unix = 1577880000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 01, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/01/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 28-02-2020 FRI  1582891200 */
    test_unix = 1582891200;
    setDateTimeStruct( &test_struct, 00, 00, 12, 28, 02, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/02/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 29-02-2020 SAT  1582977600      Test for leap year - this day SHOULD exist */
    test_unix = 1582977600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 29, 02, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("29/02/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 01-03-2020 SUN  1583064000 */
    test_unix = 1583064000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 03, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/03/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 28-03-2020 SAT  1585396800 */
    test_unix = 1585396800;
    setDateTimeStruct( &test_struct, 00, 00, 12, 28, 03, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/03/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 29-03-2020 SUN  1585483200      DST starts */
    test_unix = 1585483200;
    setDateTimeStruct( &test_struct, 00, 00, 12, 29, 03, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("29/03/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* GMT 12:00:00 24-10-2020 SAT  1603540800 */
    test_unix = 1603540800;
    setDateTimeStruct( &test_struct, 00, 00, 12, 24, 10, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("24/10/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 25-10-2020 SUN  1603627200      DST stops */
    test_unix = 1603627200;
    setDateTimeStruct( &test_struct, 00, 00, 12, 25, 10, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("25/10/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());
    /////////

    /* GMT 12:00:00 28-02-2021 MON  1614513600 - non leap year*/
    test_unix = 1614513600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 28, 02, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/02/2021 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* GMT 00:00:00 01-03-2021 MON  1614556800 - non leap year*/
    test_unix = 1614556800;
    setDateTimeStruct( &test_struct, 00, 00, 00, 01, 03, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/03/2021 00:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* GMT 12:00:00 01-03-2021 MON  1614600000 - non leap year*/
    test_unix = 1614600000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 03, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/03/2021 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* GMT 12:00:00 29-02-2024 FRI  1709208000 - Leap year */
    test_unix = 1709208000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 29, 02, (2024), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("29/02/2024 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* GMT 00:00:00 01-03-2024 FRI  1709251200 - Leap year */
    test_unix = 1709251200;
    setDateTimeStruct( &test_struct, 00, 00, 00, 01, 03, (2024), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/03/2024 00:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* GMT 12:00:00 01-03-2024 FRI  1709294400 - Leap year */
    test_unix = 1709294400;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 03, (2024), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/03/2024 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

}




/* All EPOCH and timestamps in this function have been checked for */
/* correctness as of: 20/09/2019            */
void test_conversionWithDst(void){
    RTCTime test_struct;
    uint32_t test_unix;

    rtc_procx_SetDstIsApplicable(true);

    /* YEAR 2019: */
    /* 12:00:00 01-01-2019 TUES     1546344000 */
    test_unix = 1546344000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 01, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/01/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 01-02-2019 FRI  1549022400 */
    test_unix = 1549022400;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 2, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/02/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 28-02-2019 THU  1551355200  Test leap year */
    test_unix = 1551355200;
    setDateTimeStruct( &test_struct, 00, 00, 12, 28, 2, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/02/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 29-02-2019   1551441600 This date should not exist */
    test_unix = 1551441600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 29, 2, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    // Note: Use STRNE (not equal) as this date should not exist. 2019 is not a leap year
    // TEST_ASSERT_EQUAL_STRING_MESSAGE("29/02/2019 12:00:00", getRtcAsString(), "Expected to fail!");
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 01-03-2019 FRI  1551441600 */
    test_unix = 1551441600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 3, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/03/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 30-03-2019 SAT  1553947200 */
//    test_unix = 1553947200;
    test_unix = 1553943600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 30, 3, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("30/03/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* One second before DST starts */
    /* GMT: 00:59:59 31-03-2019 SUN  1553993999 */
//    test_unix = 1553993999;
    test_unix = 1553990399;
    setDateTimeStruct( &test_struct, 59, 59, 0, 31, 3, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/03/2019 00:59:59", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* DST starts */
    /* GMT time: 01:00:00 31-03-2019 SUN    1553994000 */
    /* After DST applied:  02:00:00 31-03-2019 SUN  1553994000 */
    test_unix = 1553994000;
    setDateTimeStruct( &test_struct, 00, 00, 02, 31, 3, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/03/2019 02:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* One second after DST has begun: */
    /* 02:00:01 31-03-2019 SUN  1553994001 */
    test_unix = 1553994001;
    setDateTimeStruct( &test_struct, 01, 00, 02, 31, 3, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/03/2019 02:00:01", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* DST 13:00:00 31-05-2019 FRI */
    test_unix = 1559304000;
    setDateTimeStruct( &test_struct, 00, 00, 13, 31, 5, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/05/2019 13:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* DST 12:00:00 31-07-2019 WED */
    test_unix = 1564570800;
    setDateTimeStruct( &test_struct, 00, 00, 12, 31, 7, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/07/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 26-10-2019 SAT  1572087600  Day before DST */
    test_unix = 1572087600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 26, 10, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("26/10/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 01:59:59 27-10-2019 SUN  1572137999  DST ends */
    test_unix = 1572141599;
    setDateTimeStruct( &test_struct, 59, 59, 01, 27, 10, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("27/10/2019 01:59:59", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 02:00:00 27-10-2019 SUN  1572141600  DST ends */
    test_unix = 1572141600;
    setDateTimeStruct( &test_struct, 00, 00, 02, 27, 10, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("27/10/2019 02:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 02:00:01 27-10-2019 SUN  1572141601  DST ends */
    test_unix = 1572141601;
    setDateTimeStruct( &test_struct, 01, 00, 02, 27, 10, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("27/10/2019 02:00:01", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 27-10-2019 SUN  1572177600 */
    test_unix = 1572177600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 27, 10, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("27/10/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 28-10-2019 MON  1572264000 */
    test_unix = 1572264000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 28, 10, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/10/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 31-12-2019 TUES 1577793600 */
    test_unix = 1577793600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 31, 12, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/12/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 2020 - Leap year */
    /* 12:00:00 01-01-2020 WED  1577880000 */
    test_unix = 1577880000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 01, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/01/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 28-02-2020 FRI  1582891200 */
    test_unix = 1582891200;
    setDateTimeStruct( &test_struct, 00, 00, 12, 28, 02, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/02/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 29-02-2020 SAT  1582977600      Test for leap year - this day SHOULD exist */
    test_unix = 1582977600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 29, 02, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("29/02/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 01-03-2020 SUN  1583064000 */
    test_unix = 1583064000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 03, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/03/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 28-03-2020 SAT  1585396800 */
    test_unix = 1585396800;
    setDateTimeStruct( &test_struct, 00, 00, 12, 28, 03, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/03/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 13:00:00 29-03-2020 SUN  1585483200      DST starts */
    test_unix = 1585483200;
    setDateTimeStruct( &test_struct, 00, 00, 13, 29, 03, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("29/03/2020 13:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 13:00:00 24-10-2020 SAT  1603540800 */
    test_unix = 1603540800;
    setDateTimeStruct( &test_struct, 00, 00, 13, 24, 10, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("24/10/2020 13:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 25-10-2020 SUN  1603627200      DST stops */
    test_unix = 1603627200;
    setDateTimeStruct( &test_struct, 00, 00, 12, 25, 10, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("25/10/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 31-12-2020 THU  1609416000 */
    test_unix = 1609416000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 31, 12, (2020), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/12/2020 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    // YEAR 2021:
    /* 12:00:00 01-01-2021 FRI  1609502400 */
    test_unix = 1609502400;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 01, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/01/2021 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 02-01-2021 SAT  1609588800 */
    test_unix = 1609588800;
    setDateTimeStruct( &test_struct, 00, 00, 12, 02, 01, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("02/01/2021 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 01-02-2021 MON  1612180800 */
    test_unix = 1612180800;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 02, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/02/2021 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 28-02-2021 SUN  1614513600 */
    test_unix = 1614513600;
    setDateTimeStruct( &test_struct, 00, 00, 12, 28, 02, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/02/2021 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* Try a (non existent) leap year date: */
    /* 12:00:00 29-02-2021  1614600000  Should not exist */
    /* Should return:  12:00:00 01-03-2021 */
    test_unix = 1614600000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 29, 02, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    // Note: Using STRNE as this date should not exist:
    // TEST_ASSERT_EQUAL_STRING_MESSAGE("29/02/2021 12:00:00", getRtcAsString(), "Expected to fail!");

    // Should return the following:
    TEST_ASSERT_EQUAL_STRING("01/03/2021 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 01-03-2021 MON  1614600000 */
    test_unix = 1614600000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 03, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/03/2021 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 24-03-2021 WED  1616587200 */
    test_unix = 1616587200;
    setDateTimeStruct( &test_struct, 00, 00, 12, 24, 03, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("24/03/2021 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 00:00:00 28-03-2021 SUN  1616889600 */
    test_unix = 1616889600;
    setDateTimeStruct( &test_struct, 00, 00, 00, 28, 03, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/03/2021 00:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 00:59:59 28-03-2021 SUN  1616893199  - 1sec before DST starts */
    test_unix = 1616893199;
    setDateTimeStruct( &test_struct, 59, 59, 00, 28, 03, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/03/2021 00:59:59", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 02:00:00 28-03-2021 SUN  1616893200  DST starts */
    test_unix = 1616893200;
    setDateTimeStruct( &test_struct, 00, 00, 02, 28, 03, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/03/2021 02:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 02:01:00 28-03-2021 SUN  1616893260  1min after DST has started */
    test_unix = 1616893260;
    setDateTimeStruct( &test_struct, 00, 01, 02, 28, 03, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/03/2021 02:01:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 03:00:00 28-03-2021 SUN  1616896800 */
    test_unix = 1616896800;
    setDateTimeStruct( &test_struct, 00, 00, 03, 28, 03, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("28/03/2021 03:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 31-03-2021 WED  1617188400 */
    test_unix = 1617188400;
    setDateTimeStruct( &test_struct, 00, 00, 12, 31, 03, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/03/2021 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    /* 12:00:00 01-04-2021 THU  1617274800 */
    test_unix = 1617274800;
    setDateTimeStruct( &test_struct, 00, 00, 12, 01, 04, (2021), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("01/04/2021 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

}

void test_struct_conversion(void)
{
_DEBUG_OUT_TEST_NAME
    RTCTime test_struct;
    uint32_t test_unix;

    rtc_procx_SetDstIsApplicable(false);

    test_unix = 1553947200;
    setDateTimeStruct( &test_struct, 00, 00, 12, 30, 3, (2019), 0, 0);

    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("30/03/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());

    test_unix = 1559304000;
    setDateTimeStruct( &test_struct, 00, 00, 12, 31, 5, (2019), 0, 0);
    rtc_procx_SetTime(test_struct);
    test_struct = rtc_procx_GetTime();
    TEST_ASSERT_EQUAL_STRING("31/05/2019 12:00:00", getRtcAsString());
    TEST_ASSERT_EQUAL_UINT32(test_unix, rtc_procx_GetCounter());
}
