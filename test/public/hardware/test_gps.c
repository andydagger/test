#include "unity.h"
#include "gps.c"

#include "stdbool.h"
#include "stdint.h"
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "mock_debug.h"
#include "mock_sysmon.h"
#include "mock_FreeRTOS.h"
#include "mock_task.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_common.h"
#include "mock_config_dataset.h"
#include "mock_hw.h"
#include "mock_rtc_procx.h"
#include "mock_systick.h"


#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
    memset((char *)gprmc_data,0x00,sizeof gprmc_data);
}
void tearDown(void)
{

}

//-----------------------------------------------------------------
// bool gps_verifyTimeStringReceptionIsDone(void);
//-----------------------------------------------------------------
void test_verifyTimeStringReception_waitForHardwareToBootUp()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_ExpectAndReturn(GPS_HW_SEARCH_TIMEOUT_PROD_MS - 1);
    TEST_ASSERT_FALSE(gps_verifyTimeStringReceptionIsDone());
}
void test_verifyTimeStringReception_NoResponse()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_ExpectAndReturn(GPS_HW_SEARCH_TIMEOUT_PROD_MS + 1);
    set_sysmon_fault_Expect(GPS_MODULE_NOT_RESPONDING);
    TEST_ASSERT_TRUE(gps_verifyTimeStringReceptionIsDone());
}
void test_verifyTimeStringReception_allowDelayForTimeString()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(GPS_HW_TIME_STRING_TIMEOUT_PROD_MS - 1);
    gprmc_data[1] = 'G';
    TEST_ASSERT_FALSE(gps_verifyTimeStringReceptionIsDone());
}
void test_verifyTimeStringReception_timeStringReceptionTimeOut()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(GPS_HW_TIME_STRING_TIMEOUT_PROD_MS + 1);
    gprmc_data[1] = 'G';
    clear_sysmon_fault_Expect(GPS_HW_NOT_FOUND);
    set_sysmon_fault_Expect(NO_GPS_LOCK);
    TEST_ASSERT_TRUE(gps_verifyTimeStringReceptionIsDone());
}
void test_verifyTimeStringReception_receivedValidTimeString()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(GPS_HW_TIME_STRING_TIMEOUT_PROD_MS - 1);
    gprmc_data[1] = 'G';
    gprmc_data[GPS_GPRMC_FIRST_HOUR_DIGIT_POS] = '1';
    clear_sysmon_fault_Expect(GPS_HW_NOT_FOUND);
    clear_sysmon_fault_Expect(NO_GPS_LOCK);
    TEST_ASSERT_TRUE(gps_verifyTimeStringReceptionIsDone());
}