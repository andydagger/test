#include "unity.h"
#include "stdbool.h"
#include "DaliRx.h"
#include "mock_DaliRx_timerISR.h"
#include "mock_DaliRx_edgeISR.h"
#include "mock_DaliHW.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

TEST_FILE("DaliRx.c");

void setUp(void)
{
    
}
void tearDown(void)
{

}

//declared extern in DaliRx_impl.h
//needs to be defined for tests
DALI_RX_STATUS DaliRx_status = DALI_RX_BUSY;
uint32_t DaliRx_edgeTimings[19];
uint32_t DaliRx_edgeTimingIndex;
uint32_t DaliRx_maximumHalfBitTicks;
uint32_t DaliRx_minimumHalfBitTicks;

/*******************************************************************************
    void DaliRx_start(void);
*******************************************************************************/
void test_start_setsHardwareCorrectly()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_TimerISR_reset_Expect();
    DaliRx_InputEdgeISR_reset_Expect();
    DaliRx_EnableTimerISR_Expect(true);
    
    DaliRx_start();
}

/*******************************************************************************
    DALI_RX_STATUS DaliRx_getStatus(void);
*******************************************************************************/
void test_getStatus_returnsTheImplLayerStatus()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_status = DALI_RX_TIMEOUT;
    TEST_ASSERT_EQUAL_INT(DALI_RX_TIMEOUT, DaliRx_getStatus());
}

void test_getStatus_returnsTheImplLayerStatus_DifferentValue()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_status = DALI_RX_STOP;
    TEST_ASSERT_EQUAL_INT(DALI_RX_STOP, DaliRx_getStatus());
}

/*******************************************************************************
    void DaliRx_reset(void);
*******************************************************************************/
void test_start_disablesTheISRs()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_EnableTimerISR_Expect(false);
    DaliHw_SetOutputTo_Expect(true);
    
    DaliRx_reset();
}

/*******************************************************************************
    DALI_RX_STATUS DaliRx_process(void);
*******************************************************************************/
void test_process_returnsAnErrorIfTheFirstPulseIsLessThanOneHalfPeriod()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_edgeTimingIndex = 11;    //Set to a valid number of pulses
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 300;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_PULSE_LENGTH_ERROR, DaliRx_process());
}

void test_process_returnsAnErrorIfTheFirstPulseIsMoreThanOneHalfPeriod()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_edgeTimingIndex = 11;    //Set to a valid number of pulses
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 520;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_PULSE_LENGTH_ERROR, DaliRx_process());
}


void test_process_returnsAnErrorIfTheSecondPulseIsLessThanOnePeriod()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_edgeTimingIndex = 11;    //Set to a valid number of pulses
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 600;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_PULSE_LENGTH_ERROR, DaliRx_process());
}

void test_process_returnsAnErrorIfTheSecondPulseIsMoreThanOnePeriod()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_edgeTimingIndex = 11;    //Set to a valid number of pulses
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 1020;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_PULSE_LENGTH_ERROR, DaliRx_process());
}

void test_process_returnsAnErrorIfThereAreLessThan9Pulses()
{
_DEBUG_OUT_TEST_NAME
    //9 indicates the miminmum number of pulses
    //This is start bit followed by pattern 01010101
    DaliRx_edgeTimingIndex = 7; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 800;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_NOT_ENOUGH_PULSES, DaliRx_process());
}

void test_process_returnsDoneIfAllPulsesAreWithinRange()
{
_DEBUG_OUT_TEST_NAME

    //This pattern corrresponds to (s= start bit,R = rising edge, F = falling edge)
    //SRFFRFFRF

    DaliRx_edgeTimingIndex = 13; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 400;
    DaliRx_edgeTimings[2] = 400;
    DaliRx_edgeTimings[3] = 800;
    DaliRx_edgeTimings[4] = 400;
    DaliRx_edgeTimings[5] = 400;
    DaliRx_edgeTimings[6] = 800;
    DaliRx_edgeTimings[7] = 800;
    DaliRx_edgeTimings[8] = 400;
    DaliRx_edgeTimings[9] = 400;
    DaliRx_edgeTimings[10] = 800;
    DaliRx_edgeTimings[11] = 800;
    DaliRx_edgeTimings[12] = 400;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_DONE, DaliRx_process());
}


void test_process_setsTheRxStatusToDoneIfProcessSuccessful()
{
_DEBUG_OUT_TEST_NAME

    //This pattern corrresponds to (s= start bit,R = rising edge, F = falling edge)
    //SRFFRFFRF

    DaliRx_edgeTimingIndex = 13; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 400;
    DaliRx_edgeTimings[2] = 400;
    DaliRx_edgeTimings[3] = 800;
    DaliRx_edgeTimings[4] = 400;
    DaliRx_edgeTimings[5] = 400;
    DaliRx_edgeTimings[6] = 800;
    DaliRx_edgeTimings[7] = 800;
    DaliRx_edgeTimings[8] = 400;
    DaliRx_edgeTimings[9] = 400;
    DaliRx_edgeTimings[10] = 800;
    DaliRx_edgeTimings[11] = 800;
    DaliRx_edgeTimings[12] = 400;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_DONE, DaliRx_process());
    TEST_ASSERT_EQUAL_INT(DALI_RX_DONE, DaliRx_status);
}


void test_process_returnsErrorIfAPulseIsOutsideRange()
{
_DEBUG_OUT_TEST_NAME

    //This pattern corrresponds to (s= start bit,R = rising edge, F = falling edge)
    //SRFFRFFRF

    DaliRx_edgeTimingIndex = 13; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 400;
    DaliRx_edgeTimings[2] = 400;
    DaliRx_edgeTimings[3] = 800;
    DaliRx_edgeTimings[4] = 400;
    DaliRx_edgeTimings[5] = 400;
    DaliRx_edgeTimings[6] = 600;        //This value is not allowed
    DaliRx_edgeTimings[7] = 800;
    DaliRx_edgeTimings[8] = 400;
    DaliRx_edgeTimings[9] = 400;
    DaliRx_edgeTimings[10] = 800;
    DaliRx_edgeTimings[11] = 800;
    DaliRx_edgeTimings[12] = 400;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_PULSE_LENGTH_ERROR, DaliRx_process());
}


void test_process_returnsErrorIfAPulseIsOutsideRange_differentIndex()
{
_DEBUG_OUT_TEST_NAME

    //This pattern corrresponds to (s= start bit,R = rising edge, F = falling edge)
    //SRFFRFFRF

    DaliRx_edgeTimingIndex = 13; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 400;
    DaliRx_edgeTimings[2] = 400;
    DaliRx_edgeTimings[3] = 800;
    DaliRx_edgeTimings[4] = 400;
    DaliRx_edgeTimings[5] = 400;
    DaliRx_edgeTimings[6] = 400; 
    DaliRx_edgeTimings[7] = 800;
    DaliRx_edgeTimings[8] = 400;
    DaliRx_edgeTimings[9] = 400;
    DaliRx_edgeTimings[10] = 1040;
    DaliRx_edgeTimings[11] = 800;
    DaliRx_edgeTimings[12] = 400;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_PULSE_LENGTH_ERROR, DaliRx_process());
}

void test_process_setsTheOutputByteBasedOnEdgeData()
{
_DEBUG_OUT_TEST_NAME

    //This pattern corrresponds to (s= start bit,R = rising edge, F = falling edge)
    //SRFFRFFRF
    //As rising = 1, falling = 0, this is 10010010 or 0x92

    DaliRx_edgeTimingIndex = 13; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 400;
    DaliRx_edgeTimings[2] = 400;
    DaliRx_edgeTimings[3] = 800;
    DaliRx_edgeTimings[4] = 400;
    DaliRx_edgeTimings[5] = 400;
    DaliRx_edgeTimings[6] = 800;
    DaliRx_edgeTimings[7] = 800;
    DaliRx_edgeTimings[8] = 400;
    DaliRx_edgeTimings[9] = 400;
    DaliRx_edgeTimings[10] = 800;
    DaliRx_edgeTimings[11] = 800;
    DaliRx_edgeTimings[12] = 400;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_DONE, DaliRx_process());
    TEST_ASSERT_EQUAL_HEX8(0x92, DaliRx_getResponse());
}

void test_process_setsTheOutputByteBasedOnEdgeData_DifferentValue()
{
_DEBUG_OUT_TEST_NAME

    //This pattern corrresponds to (s= start bit,R = rising edge, F = falling edge)
    //SFFRRRRRF
    //As rising = 1, falling = 0, this is 00111110 or 0x3E

    DaliRx_edgeTimingIndex = 15; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 800;
    DaliRx_edgeTimings[2] = 400;
    DaliRx_edgeTimings[3] = 400;
    DaliRx_edgeTimings[4] = 800;
    DaliRx_edgeTimings[5] = 400;
    DaliRx_edgeTimings[6] = 400;
    DaliRx_edgeTimings[7] = 400;
    DaliRx_edgeTimings[8] = 400;
    DaliRx_edgeTimings[9] = 400;
    DaliRx_edgeTimings[10] = 400;
    DaliRx_edgeTimings[11] = 400;
    DaliRx_edgeTimings[12] = 400;
    DaliRx_edgeTimings[13] = 800;
    DaliRx_edgeTimings[14] = 400;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_DONE, DaliRx_process());
    TEST_ASSERT_EQUAL_HEX8(0x3E, DaliRx_getResponse());
}

void test_process_setsTheOutputByteBasedOnEdgeData_DifferentValue2()
{
_DEBUG_OUT_TEST_NAME

    //This pattern corrresponds to (s= start bit,R = rising edge, F = falling edge)
    //SRFRFFFFR
    //As rising = 1, falling = 0, this is 10100001 or 0xA1

    DaliRx_edgeTimingIndex = 13; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 400;
    DaliRx_edgeTimings[2] = 400;
    DaliRx_edgeTimings[3] = 800;
    DaliRx_edgeTimings[4] = 800;
    DaliRx_edgeTimings[5] = 800;
    DaliRx_edgeTimings[6] = 400;
    DaliRx_edgeTimings[7] = 400;
    DaliRx_edgeTimings[8] = 400;
    DaliRx_edgeTimings[9] = 400;
    DaliRx_edgeTimings[10] = 400;
    DaliRx_edgeTimings[11] = 400;
    DaliRx_edgeTimings[12] = 800;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_DONE, DaliRx_process());
    TEST_ASSERT_EQUAL_HEX8(0xA1, DaliRx_getResponse());
}

void test_process_setsTheOutputByteBasedOnEdgeData_0xFF()
{
_DEBUG_OUT_TEST_NAME

    //This pattern corrresponds to (s= start bit,R = rising edge, F = falling edge)
    //SRRRRRRRR
    //As rising = 1, falling = 0, this is 11111111 or 0xFF

    DaliRx_edgeTimingIndex = 19; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 400;
    DaliRx_edgeTimings[2] = 400;
    DaliRx_edgeTimings[3] = 400;
    DaliRx_edgeTimings[4] = 400;
    DaliRx_edgeTimings[5] = 400;
    DaliRx_edgeTimings[6] = 400;
    DaliRx_edgeTimings[7] = 400;
    DaliRx_edgeTimings[8] = 400;
    DaliRx_edgeTimings[9] = 400;
    DaliRx_edgeTimings[10] = 400;
    DaliRx_edgeTimings[11] = 400;
    DaliRx_edgeTimings[12] = 400;
    DaliRx_edgeTimings[13] = 400;
    DaliRx_edgeTimings[14] = 400;
    DaliRx_edgeTimings[15] = 400;
    DaliRx_edgeTimings[16] = 400;
    DaliRx_edgeTimings[17] = 400;
    DaliRx_edgeTimings[18] = 400;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_DONE, DaliRx_process());
    TEST_ASSERT_EQUAL_HEX8(0xFF, DaliRx_getResponse());
}

void test_process_setsTheOutputByteBasedOnEdgeData_0x00()
{
_DEBUG_OUT_TEST_NAME

    //This pattern corrresponds to (s= start bit,R = rising edge, F = falling edge)
    //SFFFFFFFF
    //As rising = 1, falling = 0, this is 00000000 or 0x00

    DaliRx_edgeTimingIndex = 17; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 800;
    DaliRx_edgeTimings[2] = 400;
    DaliRx_edgeTimings[3] = 400;
    DaliRx_edgeTimings[4] = 400;
    DaliRx_edgeTimings[5] = 400;
    DaliRx_edgeTimings[6] = 400;
    DaliRx_edgeTimings[7] = 400;
    DaliRx_edgeTimings[8] = 400;
    DaliRx_edgeTimings[9] = 400;
    DaliRx_edgeTimings[10] = 400;
    DaliRx_edgeTimings[11] = 400;
    DaliRx_edgeTimings[12] = 400;
    DaliRx_edgeTimings[13] = 400;
    DaliRx_edgeTimings[14] = 400;
    DaliRx_edgeTimings[15] = 400;
    DaliRx_edgeTimings[16] = 400;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_DONE, DaliRx_process());
    TEST_ASSERT_EQUAL_HEX8(0x00, DaliRx_getResponse());
}

void test_process_returnsAnErrorIfTheSignalEndsLow()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_edgeTimingIndex = 16; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 400;
    DaliRx_edgeTimings[2] = 800;
    DaliRx_edgeTimings[3] = 400;
    DaliRx_edgeTimings[4] = 400;
    DaliRx_edgeTimings[5] = 400;
    DaliRx_edgeTimings[6] = 400;
    DaliRx_edgeTimings[7] = 400;
    DaliRx_edgeTimings[8] = 400;
    DaliRx_edgeTimings[9] = 400;
    DaliRx_edgeTimings[10] = 400;
    DaliRx_edgeTimings[11] = 400;
    DaliRx_edgeTimings[12] = 400;
    DaliRx_edgeTimings[13] = 800;
    DaliRx_edgeTimings[14] = 800;
    DaliRx_edgeTimings[15] = 800;

    //N.B. Whilst this is invalid due to the timings, investigation shows that
    //Any even number of transitions is invalid -> ending on a low signal
    //This is because you must start on a high, and end on a high, therefore
    //an even number of transitions -> an odd number of pulses
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_ENCODING_ERROR, DaliRx_process());

}
void test_process_returnsAnErrorIfThereIsAnInvalidEncoding()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_edgeTimingIndex = 13; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 400;
    DaliRx_edgeTimings[2] = 800;
    DaliRx_edgeTimings[3] = 400;
    DaliRx_edgeTimings[4] = 400;
    DaliRx_edgeTimings[5] = 800;
    DaliRx_edgeTimings[6] = 400;
    DaliRx_edgeTimings[7] = 400;
    DaliRx_edgeTimings[8] = 400;
    DaliRx_edgeTimings[9] = 400;
    DaliRx_edgeTimings[10] = 400;
    DaliRx_edgeTimings[11] = 800;
    DaliRx_edgeTimings[12] = 800;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_ENCODING_ERROR, DaliRx_process());

}

void test_process_returnsAnErrorIfThereIsAnInvalidEncoding_DifferentValues()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_edgeTimingIndex = 11; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 800;
    DaliRx_edgeTimings[2] = 800;
    DaliRx_edgeTimings[3] = 800;
    DaliRx_edgeTimings[4] = 400;
    DaliRx_edgeTimings[5] = 800;
    DaliRx_edgeTimings[6] = 400;
    DaliRx_edgeTimings[7] = 800;
    DaliRx_edgeTimings[8] = 800;
    DaliRx_edgeTimings[9] = 800;
    DaliRx_edgeTimings[10] = 400;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_ENCODING_ERROR, DaliRx_process());
}

void test_process_returnsAnErrorIfThereAreTooFewBits()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_edgeTimingIndex = 13; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 800;
    DaliRx_edgeTimings[2] = 400;
    DaliRx_edgeTimings[3] = 400;
    DaliRx_edgeTimings[4] = 400;
    DaliRx_edgeTimings[5] = 400;
    DaliRx_edgeTimings[6] = 800;
    DaliRx_edgeTimings[7] = 800;
    DaliRx_edgeTimings[8] = 400;
    DaliRx_edgeTimings[9] = 400;
    DaliRx_edgeTimings[10] = 400;
    DaliRx_edgeTimings[11] = 400;
    DaliRx_edgeTimings[12] = 400;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_ENCODING_ERROR, DaliRx_process());
}

void test_process_returnsAnErrorIfThereAreTooManyBits()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_edgeTimingIndex = 11; 
    DaliRx_minimumHalfBitTicks = 333;   //333us
    DaliRx_maximumHalfBitTicks = 500;   //500us
    DaliRx_edgeTimings[0] = 400;
    DaliRx_edgeTimings[1] = 800;
    DaliRx_edgeTimings[2] = 800;
    DaliRx_edgeTimings[3] = 800;
    DaliRx_edgeTimings[4] = 800;
    DaliRx_edgeTimings[5] = 800;
    DaliRx_edgeTimings[6] = 800;
    DaliRx_edgeTimings[7] = 800;
    DaliRx_edgeTimings[8] = 800;
    DaliRx_edgeTimings[9] = 800;
    DaliRx_edgeTimings[10] = 400;
    
    TEST_ASSERT_EQUAL_INT(DALI_RX_ENCODING_ERROR, DaliRx_process());
}