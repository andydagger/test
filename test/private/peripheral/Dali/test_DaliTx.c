#include "unity.h"
#include "stdbool.h"
#include "DaliTx.h"
#include "mock_DaliTx_impl.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_debug.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

TEST_FILE("DaliTx.c");

/*******************************************************************************
    Setup and Expected values.
*******************************************************************************/

void setUp(void)
{
    debugprint_Ignore();

}

void tearDown(void)
{

}

/*******************************************************************************
    void DaliTx_start(uint8_t * p_frame, uint8_t b_is24BitFrame)
*******************************************************************************/

void test_start_AllCallsInOrder16Bit()
{
    _DEBUG_OUT_TEST_NAME

    uint8_t frameData[2] = {0x01, 0x02};
    bool b_is24BitFrame = false;
    DaliTx_encodeAndLoadFrame_Expect(frameData, b_is24BitFrame);
    DaliTx_setTxMode_Expect();
    DaliTx_resetIsr_Expect();
    FreeRTOSDelay_Expect(20);
    DaliTx_startTimer_Expect();
    TEST_ASSERT_TRUE(DaliTx_start(frameData, b_is24BitFrame));
}

void test_start_CallsInOrder24Bit()
{
    _DEBUG_OUT_TEST_NAME

    uint8_t frameData[3] = {0x01, 0x02, 0x03};
    bool b_is24BitFrame = true;
    DaliTx_encodeAndLoadFrame_Expect(frameData, b_is24BitFrame);
    DaliTx_setTxMode_Expect();
    DaliTx_resetIsr_Expect();
    FreeRTOSDelay_Expect(20);
    DaliTx_startTimer_Expect();
    TEST_ASSERT_TRUE(DaliTx_start(frameData, b_is24BitFrame));
}

void test_start_RejectedFrameNullPointer()
{
    _DEBUG_OUT_TEST_NAME

    TEST_ASSERT_FALSE(DaliTx_start(NULL, false));
}

/*******************************************************************************
    void DaliTx_reset(void)
*******************************************************************************/


void test_reset_AllCallsInOrder()
{
    _DEBUG_OUT_TEST_NAME

    DaliTx_disableTimer_Expect();
    DaliTx_setTxMode_Expect();
    
    DaliTx_reset();
}