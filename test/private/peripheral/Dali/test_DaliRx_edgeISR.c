#include "unity.h"
#include "stdbool.h"
#include "DaliRx_edgeISR.h"

#include "mock_DaliRx.h"
#include "mock_DaliHW.h"
#include "mock_DaliRx_timerISR.h"
#include "DaliRx_common.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

TEST_FILE("DaliRx_impl.c");

void setUp(void)
{
    
}
void tearDown(void)
{

}

void resetEdgeTimings(void)
{
    for (uint32_t i = 0; i < 19; ++i)
    {
        DaliRx_edgeTimings[i] = 0;
    }
    DaliRx_edgeTimingIndex = 0;
}

/*******************************************************************************
    void DaliRx_InputEdgeISR_run(void);
*******************************************************************************/
void test_inputEdgeISR_StoresTheTickLengthOfTheFirstPulse()
{
    resetEdgeTimings();

    DaliRx_status = DALI_RX_BUSY;
    //We have not seen a start pulse yet
    DaliRx_startFlag = false; 
    //Must be greater than 5.5ms, therefore 5500 @1Mz
    DaliRx_minimumSettlingTime = 5500;
    
    DaliRx_ticksSinceLastEdge_ExpectAndReturn(5500);
    DaliRx_InputEdgeISR_run();

    //Start flag is now true
    TEST_ASSERT_TRUE(DaliRx_startFlag);

    DaliRx_ticksSinceLastEdge_ExpectAndReturn(412);
    
    DaliRx_InputEdgeISR_run();

    TEST_ASSERT_EQUAL_UINT32(412, DaliRx_edgeTimings[0]);
}

void test_inputEdgeISR_StoresTheTickLengthOfTheFirstPulse_DifferentValue()
{
    resetEdgeTimings();
    DaliRx_status = DALI_RX_BUSY;

    DaliRx_startFlag = true;
    DaliRx_ticksSinceLastEdge_ExpectAndReturn(291);
    DaliRx_InputEdgeISR_run();

    TEST_ASSERT_EQUAL_UINT32(291, DaliRx_edgeTimings[0]);
}

void test_inputEdgeISR_StoresTheTickLengthOfSubsequentPulses()
{
    resetEdgeTimings();

    DaliRx_status = DALI_RX_BUSY;
    DaliRx_startFlag = true;
    DaliRx_ticksSinceLastEdge_ExpectAndReturn(291);
    DaliRx_InputEdgeISR_run();
    DaliRx_ticksSinceLastEdge_ExpectAndReturn(240);
    DaliRx_InputEdgeISR_run();
    DaliRx_ticksSinceLastEdge_ExpectAndReturn(319);
    DaliRx_InputEdgeISR_run();

    TEST_ASSERT_EQUAL_UINT32(291, DaliRx_edgeTimings[0]);
    TEST_ASSERT_EQUAL_UINT32(240, DaliRx_edgeTimings[1]);
    TEST_ASSERT_EQUAL_UINT32(319, DaliRx_edgeTimings[2]);
}

void test_inputEdgeISR_HasProtectionIfThereAreMoreThanTheMaximumNumberOfPulses()
{
    resetEdgeTimings();

    DaliRx_status = DALI_RX_BUSY;
    DaliRx_edgeTimingIndex = 19;
    DaliRx_startFlag = true;
    DaliRx_reset_Expect();
    DaliRx_InputEdgeISR_run();

    TEST_ASSERT_EQUAL_UINT32(19, DaliRx_edgeTimingIndex);
    TEST_ASSERT_EQUAL(DALI_RX_TOO_MANY_EDGES, DaliRx_status);
}

void test_inputEdgeISR_ReportsAnErrorIfThereIsMoreThanTheMinimumSettlingTimeOnStart()
{
    DaliRx_status = DALI_RX_BUSY;
    resetEdgeTimings();
    DaliRx_minimumSettlingTime = 5500;
    DaliRx_ticksSinceLastEdge_ExpectAndReturn(5000);   //i.e. less than 5.5ms (5500 ticks)
    DaliRx_startFlag = false;
    DaliRx_reset_Expect();
    DaliRx_InputEdgeISR_run();
 
    TEST_ASSERT_EQUAL(DALI_RX_SETTLING_TIME_ERROR, DaliRx_status);
}