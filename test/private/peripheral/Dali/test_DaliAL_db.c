#include <stdint.h>

#include "unity.h"

#include "DaliAL_db.h"

TEST_FILE("DaliAL_db.c");

void setUp(void) {
	;
}

void tearDown(void) {
	;
}

void test_DaliAL_db_fetch_template_invalid(void) {
	bool ret = false;

	ret = DaliAL_db_fetch_template(DALI_COMMAND_NONE, NULL, 0);
	TEST_ASSERT_EQUAL(false, ret);

	ret = DaliAL_db_fetch_template(DALI_COMMAND_MAX, NULL, 0);
	TEST_ASSERT_EQUAL(false, ret);
}

void test_DaliAL_db_fetch_template_valid_ff16(void) {
	const struct dali_msg *msg = NULL;

	bool ret = DaliAL_db_fetch_template(DALI_QUERY_DEVICE_TYPE, &msg, 0);
	TEST_ASSERT_EQUAL(true, ret);
	TEST_ASSERT_NOT_NULL(msg);

	TEST_ASSERT_EQUAL(DALI_PACKET_FF16, msg->size);
	TEST_ASSERT_EQUAL(0x99, msg->opcode);
	TEST_ASSERT_EQUAL(true, msg->flags.has_response);
	TEST_ASSERT_EQUAL(false, msg->flags.send_twice);
	TEST_ASSERT_EQUAL(false, msg->flags.selector);
}

void test_DaliAL_db_fetch_template_valid_ff24(void) {
	const struct dali_msg *msg = NULL;

	bool ret = DaliAL_db_fetch_template(DALI_QUERY_QUIESCENT_MODE, &msg, 0);
	TEST_ASSERT_EQUAL(true, ret);
	TEST_ASSERT_NOT_NULL(msg);

	TEST_ASSERT_EQUAL(DALI_PACKET_FF24, msg->size);
	TEST_ASSERT_EQUAL(0x40, msg->opcode);
	TEST_ASSERT_EQUAL(0xFE, msg->instance);
	TEST_ASSERT_EQUAL(true, msg->flags.has_response);
	TEST_ASSERT_EQUAL(false, msg->flags.send_twice);
	TEST_ASSERT_EQUAL(false, msg->flags.selector);
}

void test_DaliAL_db_test_integrity(void) {
	TEST_ASSERT_TRUE(DaliAL_db_test_integrity());
}
