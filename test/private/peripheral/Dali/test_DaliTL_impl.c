#include "unity.h"
#include "stdbool.h"
#include "DaliTL_impl.h"
#include "mock_DaliTL.h"
#include "mock_DaliTx.h"
#include "mock_DaliRx.h"
#include "mock_systick.h"
#include "mock_debug.h"
#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

TEST_FILE("DaliTL_impl.c");

/*******************************************************************************
    Setup and Expected values.
*******************************************************************************/

void setUp(void)
{
    debug_print_instant_Ignore();
    debugprint_Ignore();
}

void tearDown(void)
{

}

/*******************************************************************************
    void DaliTL_Idle(void);
*******************************************************************************/

void test_idle_ReturnsBusy()
{
_DEBUG_OUT_TEST_NAME
    DaliTL_setState(DALITL_STATE_IDLE);

    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_Idle());
    TEST_ASSERT_EQUAL_INT(DALITL_STATE_IDLE, DaliTL_getState());
}


/*******************************************************************************
    void DaliTL_TxWait(void);
*******************************************************************************/

void test_txWait_queriesTheTxStatus()
{
_DEBUG_OUT_TEST_NAME
  
    DaliTL_setState(DALITL_STATE_TX_WAIT);
    DaliTx_getStatus_ExpectAndReturn(DALI_TX_BUSY);

    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_TxWait());
    TEST_ASSERT_EQUAL_INT(DALITL_STATE_TX_WAIT, DaliTL_getState());
}

void test_txWait_entersTheTwiceStateIfTxReportsDoneAndRequestIsSendTwice()
{
_DEBUG_OUT_TEST_NAME
    DaliTL_setState(DALITL_STATE_TX_WAIT);
    DaliTL_request request = {.b_sendTwice = true };
    DaliTL_setRequest(&request);
    DaliTx_getStatus_ExpectAndReturn(DALI_TX_DONE);
    get_systick_ms_IgnoreAndReturn(0);

    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_TxWait());
    TEST_ASSERT_EQUAL_INT(DALITL_STATE_TWICE_DELAY, DaliTL_getState());
}

void test_txWait_entersTheIdleStateIfTxReportsDoneAndRequestIsNoAnswer()
{
_DEBUG_OUT_TEST_NAME
    DaliTL_setState(DALITL_STATE_TX_WAIT);
    DaliTL_request request = {.b_sendTwice = false, .b_answerExpected = false };
    DaliTL_setRequest(&request);
    DaliTx_getStatus_ExpectAndReturn(DALI_TX_DONE);
    DaliRx_reset_Expect();
    DaliTx_reset_Expect();

    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_DONE, DaliTL_TxWait());
    TEST_ASSERT_EQUAL_INT(DALITL_STATE_IDLE, DaliTL_getState());
}

void test_txWait_entersTheReplyWaitStateTxReportsDoneAndRequestIsAnswer()
{
_DEBUG_OUT_TEST_NAME
    DaliTL_setState(DALITL_STATE_TX_WAIT);
    DaliTL_request request = {.b_sendTwice = false, .b_answerExpected = true };
    DaliTL_setRequest(&request);
    DaliTx_getStatus_ExpectAndReturn(DALI_TX_DONE);
    DaliRx_start_Expect();

    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_TxWait());
    TEST_ASSERT_EQUAL_INT(DALITL_STATE_REPLY_WAIT, DaliTL_getState());
}


/*******************************************************************************
    void DaliTL_SendTwiceDelay(void);
*******************************************************************************/

void test_sendTwice_waitsForAMinimumSettlingTime()
{
_DEBUG_OUT_TEST_NAME
  
    get_systick_ms_ExpectAndReturn(0);
    DaliTL_transitionToSendTwice();
    get_systick_ms_ExpectAndReturn(10);
    DaliTL_startXfer_ExpectAnyArgsAndReturn(DALI_TL_RET_OK);

    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_SendTwiceDelay());
    TEST_ASSERT_EQUAL_INT(DALITL_STATE_TX_WAIT, DaliTL_getState());
}

void test_sendTwice_waitsForAMinimumSettlingTime_withOffset()
{
_DEBUG_OUT_TEST_NAME
  
    get_systick_ms_ExpectAndReturn(153);
    DaliTL_transitionToSendTwice();
    get_systick_ms_ExpectAndReturn(163);
    DaliTL_startXfer_ExpectAnyArgsAndReturn(DALI_TL_RET_OK);

    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_SendTwiceDelay());
    TEST_ASSERT_EQUAL_INT(DALITL_STATE_TX_WAIT, DaliTL_getState());
}


void test_sendTwice_doesNotTransitionUntil10MsHaveElapsed()
{
_DEBUG_OUT_TEST_NAME
  
    get_systick_ms_ExpectAndReturn(0);
    DaliTL_transitionToSendTwice();
    get_systick_ms_ExpectAndReturn(9);

    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_SendTwiceDelay());
    TEST_ASSERT_EQUAL_INT(DALITL_STATE_TWICE_DELAY, DaliTL_getState());
}

void test_sendTwice_doesNotHaveTheSendTwiceFlagSetAfterTransition()
{
_DEBUG_OUT_TEST_NAME
  
    get_systick_ms_ExpectAndReturn(0);
    DaliTL_transitionToSendTwice();
    get_systick_ms_ExpectAndReturn(30);

    DaliTL_startXfer_ExpectAnyArgsAndReturn(DALI_TL_RET_OK);
    DaliTL_request request = {.b_sendTwice = true };
    DaliTL_setRequest(&request);
    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_SendTwiceDelay());
    TEST_ASSERT_EQUAL_INT(DALITL_STATE_TX_WAIT, DaliTL_getState());
    TEST_ASSERT_FALSE(DaliTL_getRequest().b_sendTwice);
}



/*******************************************************************************
    void DaliTL_ReplyWait(void);
*******************************************************************************/

void test_replyWait_queriesTheRxStatus()
{
_DEBUG_OUT_TEST_NAME
  
    DaliTL_setState(DALITL_STATE_REPLY_WAIT);
    DaliRx_getStatus_ExpectAndReturn(DALI_RX_BUSY);

    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_RxWait());
    TEST_ASSERT_EQUAL_INT(DALITL_STATE_REPLY_WAIT, DaliTL_getState());
}

void test_replyWait_returnsDoneIfRxIsDone()
{
_DEBUG_OUT_TEST_NAME
  
    DaliTL_setState(DALITL_STATE_REPLY_WAIT);
    DaliRx_getStatus_ExpectAndReturn(DALI_RX_DONE);
    DaliRx_reset_Expect();
    DaliTx_reset_Expect();

    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_DONE, DaliTL_RxWait());
    TEST_ASSERT_EQUAL_INT(DALITL_STATE_IDLE, DaliTL_getState());
}

