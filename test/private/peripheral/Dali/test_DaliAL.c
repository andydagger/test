#include <stdbool.h>

#include "unity.h"

#include "DaliAL.h"

#include "DaliAL_db.h"

#include "mock_DaliAL_impl.h"
#include "mock_common.h"

#define _DEBUG_OUT_TEST_NAME()    printf(" %s", __func__)

TEST_FILE("DaliAL.c");

void setUp(void) {
	delay_ms_Ignore();
}

void tearDown(void) {

}

DaliTL_request expectedQueryStatusRequest = {
	.address = 0x7F,
	.txByte = 0x90,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedDAPCRequest = {
	.address = 0xFE,
	.txByte = 0x00,
	.b_answerExpected = false,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedResetRequest = {
	.address = 0x7F,
	.txByte = 0x20,
	.b_answerExpected = false,
	.b_sendTwice = true,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedDTR0Request = {
	.address = 0xA3,
	.txByte = 0x00,
	.b_answerExpected = false,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedDTR024BitRequest = {
	.address = 0xC1,
	.txByte = 0x00,
	.b_answerExpected = false,
	.b_sendTwice = false,
	.b_24bit = true,
	.instance = 0x30
};

DaliTL_request expectedDTR1Request = {
	.address = 0xC3,
	.txByte = 0x00,
	.b_answerExpected = false,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};


DaliTL_request expectedDTR124BitRequest = {
	.address = 0xC1,
	.txByte = 0x00,
	.b_answerExpected = false,
	.b_sendTwice = false,
	.b_24bit = true,
	.instance = 0x31
};

DaliTL_request expectedDTR2Request = {
	.address = 0xC5,
	.txByte = 0x00,
	.b_answerExpected = false,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedDTR224BitRequest = {
	.address = 0xC1,
	.txByte = 0x00,
	.b_answerExpected = false,
	.b_sendTwice = false,
	.b_24bit = true,
	.instance = 0x32
};

DaliTL_request expectedSetFadeRequest = {
	.address = 0xFF,
	.txByte = 0x2E,
	.b_answerExpected = false,
	.b_sendTwice = true,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedDeviceTypeRequest = {
	.address = 0x7F,
	.txByte = 0x99,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedNextDeviceTypeRequest = {
	.address = 0x7F,
	.txByte = 0xA7,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedPhysicalMinimumRequest = {
	.address = 0x7F,
	.txByte = 0x9A,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedLightSourceTypeRequest = {
	.address = 0x7F,
	.txByte = 0x9F,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedActualLevelRequest = {
	.address = 0x7F,
	.txByte = 0xA0,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedDriverVersionNumberRequest = {
	.address = 0x7F,
	.txByte = 0x97,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedSensorVersionNumberRequest = {
	.address = 0x7F,
	.txByte = 0x34,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedExtendedVersionNumberRequest = {
	.address = 0x7F,
	.txByte = 0xFF,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedEnableDeviceTypeRequest = {
	.address = 0xC1,
	.txByte = 0x00,
	.b_answerExpected = false,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedPingRequest = {
	.address = 0xAD,
	.txByte = 0x00,
	.b_answerExpected = false,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedEnableWriteMemoryRequest = {
	.address = 0x7F,
	.txByte = 0x81,
	.b_answerExpected = false,
	.b_sendTwice = true,
	.b_24bit = false,
	.instance = 0x00
};


DaliTL_request expectedWriteMemoryLocationRequest = {
	.address = 0xC9,
	.txByte = 0x00,
	.b_answerExpected = false,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};


DaliTL_request expectedReadMemoryLocationRequest = {
	.address = 0x7F,
	.txByte = 0xC5,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedReadMemoryLocation24BitRequest = {
	.address = 0x7F,
	.txByte = 0x3C,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedQueryFailureStatusRequest = {
	.address = 0x7F,
	.txByte = 0xF1,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedQueryNumberOfInstancesRequest = {
	.address = 0x7F,
	.txByte = 0x35,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedQueryInstanceTypeRequest = {
	.address = 0x7F,
	.txByte = 0x80,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = true,
	.instance = 0xFE
};


DaliTL_request expectedSetEventFilterRequest = {
	.address = 0xFF,
	.txByte = 0x68,
	.b_answerExpected = false,
	.b_sendTwice = true,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedDisableInstanceRequest = {
	.address = 0xFF,
	.txByte = 0x63,
	.b_answerExpected = false,
	.b_sendTwice = true,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedQueryInputValueRequest = {
	.address = 0x7F,
	.txByte = 0x8C,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedQueryInputValueLatchRequest = {
	.address = 0x7F,
	.txByte = 0x8D,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedEnableApplicationControllerRequest = {
	.address = 0xFF,
	.txByte = 0x16,
	.b_answerExpected = false,
	.b_sendTwice = true,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedDisableApplicationControllerRequest = {
	.address = 0xFF,
	.txByte = 0x17,
	.b_answerExpected = false,
	.b_sendTwice = true,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedQueryApplicationControllerEnabledRequest = {
	.address = 0x7F,
	.txByte = 0x3D,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedStartQuiescentModeRequest = {
	.address = 0xFF,
	.txByte = 0x1D,
	.b_answerExpected = false,
	.b_sendTwice = true,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedStopQuiescentModeRequest = {
	.address = 0xFF,
	.txByte = 0x1E,
	.b_answerExpected = false,
	.b_sendTwice = true,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedQueryQuiescentModeRequest = {
	.address = 0x7F,
	.txByte = 0x40,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = true,
	.instance = 0xFE
};

DaliTL_request expectedQueryDimmingCurveRequest = {
	.address = 0x7F,
	.txByte = 0xEE,
	.b_answerExpected = true,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

DaliTL_request expectedSelectDimmingCurveRequest = {
	.address = 0xFF,
	.txByte = 0xE3,
	.b_answerExpected = false,
	.b_sendTwice = false,
	.b_24bit = false,
	.instance = 0x00
};

/*******************************************************************************
	DALI_AL_RET DaliAL_queryStatus(uint8_t * p_retVal);
*******************************************************************************/

void test_queryStatus_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0xE6;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedQueryStatusRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryStatus(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryStatus_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0x49;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedQueryStatusRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryStatus(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryStatus_nullptr()
{
	_DEBUG_OUT_TEST_NAME();
	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	uint8_t ret = DaliAL_queryStatus(DALI_ADDRESS_BROADCAST, NULL);

	TEST_ASSERT_EQUAL_UINT8(DALI_DRIVER_STATUS_NONE, ret);
}

void test_queryStatus_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryStatus(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_directArcPowerControl(uint8_t level);
*******************************************************************************/

void test_directArcPowerControl_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	DaliTL_request request = expectedDAPCRequest;

	request.txByte = 0x56;

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&request,
		1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_directArcPowerControl(request.txByte, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_directArcPowerControl_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	DaliTL_request request = expectedDAPCRequest;

	request.txByte = 0x8E;

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&request,
		1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_directArcPowerControl(request.txByte, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_directArcPowerControl_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_directArcPowerControl(0x00, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_setFadeTime(uint8_t fadeTime);
*******************************************************************************/
void test_setFadeTime_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t fade_time = 0x0E;

	DaliTL_request request_dtr0;

	request_dtr0 = expectedDTR0Request;
	request_dtr0.txByte = fade_time;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request_dtr0, 1, NULL, 0, DALI_AL_RET_OK);
	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedSetFadeRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_setFadeTime(fade_time, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_setFadeTime_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t fade_time = 0x06;

	DaliTL_request request_dtr0 = expectedDTR0Request;
	request_dtr0.txByte = fade_time;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request_dtr0, 1, NULL, 0, DALI_AL_RET_OK);
	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedSetFadeRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_setFadeTime(fade_time, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_setFadeTime_ReturnsAnErrorIfDTR0Fails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliTL_request request_dtr0 = expectedDTR0Request;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request_dtr0, 1, NULL, 0, DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_setFadeTime(0x00, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

void test_setFadeTime_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();
	//DTR0 call retuns successfully, Second call i.e. Setfade fails
	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_setFadeTime(0x00, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

void test_setFadeTime_ReturnsAnErrorIfFadeTimeIsGreaterThan15()
{
	_DEBUG_OUT_TEST_NAME();
	//DTR0 call retuns successfully, Second call i.e. Setfade fails
	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_setFadeTime(16, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_queryDeviceType(uint8_t * p_retVal);
*******************************************************************************/

void test_queryDeviceType_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0xE6;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedDeviceTypeRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryDeviceType(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryDeviceType_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0x49;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedDeviceTypeRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryDeviceType(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryDeviceType_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryDeviceType(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryDeviceType_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_queryDeviceType(DALI_ADDRESS_BROADCAST, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}


/*******************************************************************************
	DALI_AL_RET DaliAL_queryNextDeviceType(uint8_t * p_retVal);
*******************************************************************************/

void test_queryNextDeviceType_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0xE6;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedNextDeviceTypeRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryNextDeviceType(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryNextDeviceType_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0x49;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedNextDeviceTypeRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryNextDeviceType(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryNextDeviceType_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryNextDeviceType(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryNextDeviceType_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_queryNextDeviceType(DALI_ADDRESS_BROADCAST, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}


/*******************************************************************************
	DALI_AL_RET DaliAL_queryPhysicalMinimum(uint8_t * p_retVal);
*******************************************************************************/

void test_queryPhysicalMinimum_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0xE6;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedPhysicalMinimumRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryPhysicalMinimum(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryPhysicalMinimum_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0x49;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedPhysicalMinimumRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryPhysicalMinimum(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryPhysicalMinimum_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryPhysicalMinimum(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryPhysicalMinimum_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryPhysicalMinimum(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_queryLightSourceType(uint8_t * p_retVal);
*******************************************************************************/

void test_queryLightSourceType_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0xE6;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedLightSourceTypeRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryLightSourceType(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryLightSourceType_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0x49;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedLightSourceTypeRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryLightSourceType(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryLightSourceType_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryLightSourceType(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryLightSourceType_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryLightSourceType(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_queryActualLevel(uint8_t * p_retVal);
*******************************************************************************/

void test_queryActualLevel_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0xE6;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedActualLevelRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryActualLevel(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryActualLevel_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0x49;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedActualLevelRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryActualLevel(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryActualLevel_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryActualLevel(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryActualLevel_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryActualLevel(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_queryDriverVersionNumber(uint8_t * p_retVal);
*******************************************************************************/

void test_queryDriverVersionNumber_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0xE6;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedDriverVersionNumberRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryDriverVersionNumber(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryDriverVersionNumber_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0x45;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedDriverVersionNumberRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryDriverVersionNumber(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryDriverVersionNumber_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryDriverVersionNumber(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryDriverVersionNumber_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryDriverVersionNumber(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}



/*******************************************************************************
	DALI_AL_RET DaliAL_querySensorVersionNumber(uint8_t * p_retVal);
*******************************************************************************/

void test_querySensorVersionNumber_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0xE6;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedSensorVersionNumberRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_querySensorVersionNumber(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_querySensorVersionNumber_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0x45;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedSensorVersionNumberRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_querySensorVersionNumber(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_querySensorVersionNumber_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_querySensorVersionNumber(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_querySensorVersionNumber_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_querySensorVersionNumber(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_queryExtendedVersionNumber(uint8_t * p_retVal);
*******************************************************************************/

void test_queryExtendedVersionNumber_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0xE6;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedExtendedVersionNumberRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryExtendedVersionNumber(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryExtendedVersionNumber_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0x45;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedExtendedVersionNumberRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryExtendedVersionNumber(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryExtendedVersionNumber_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryExtendedVersionNumber(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryExtendedVersionNumber_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryExtendedVersionNumber(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_enableDeviceType(uint8_t data);
*******************************************************************************/
void test_enableDeviceType_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	DaliTL_request request = expectedEnableDeviceTypeRequest;

	request.txByte = 0x3A;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_enableDeviceType(request.txByte, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_enableDeviceType_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	DaliTL_request request = expectedEnableDeviceTypeRequest;

	request.txByte = 0x92;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_enableDeviceType(request.txByte, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}


void test_enableDeviceType_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_enableDeviceType(0x00, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}


void test_enableDeviceType_ReturnsAnErrorIfTheValueIs254()
{
	_DEBUG_OUT_TEST_NAME();
	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_enableDeviceType(254, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

void test_enableDeviceType_ReturnsAnErrorIfTheValueIsMASK()
{
	_DEBUG_OUT_TEST_NAME();
	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_enableDeviceType(0xFF, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_ping(void);
*******************************************************************************/

void test_ping_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	//We do not care about p_retVal as this command does not expect a response
	//The NULL value indicates we do not care
	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedPingRequest,
		1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_ping(&status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_ping_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();
	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_ping(&status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_queryFailureStatus(uint8_t * p_retVal);
*******************************************************************************/

void test_queryFailureStatus_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t expected = 0xE9;
	expectedEnableDeviceTypeRequest.txByte = 6;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedEnableDeviceTypeRequest, 1, NULL, 0, DALI_AL_RET_OK);
	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedQueryFailureStatusRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	dali_driver_failure_status_t ret = DaliAL_queryFailureStatus(DALI_ADDRESS_BROADCAST, 6, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_HEX8(expected, ret);
}

void test_queryFailureStatus_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t expected = 0x47;
	expectedEnableDeviceTypeRequest.txByte = 12;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedEnableDeviceTypeRequest, 1, NULL, 0, DALI_AL_RET_OK);
	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedQueryFailureStatusRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	dali_driver_failure_status_t ret = DaliAL_queryFailureStatus(DALI_ADDRESS_BROADCAST, 12, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL(expected, ret);
}

void test_queryFailureStatus_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryFailureStatus(DALI_ADDRESS_BROADCAST, 0, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryFailureStatus_ReturnsAnErrorIfEnableDeviceTypeFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryFailureStatus(DALI_ADDRESS_BROADCAST, 0, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

void test_queryFailureStatus_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);
	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryFailureStatus(DALI_ADDRESS_BROADCAST, 0, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}


/*******************************************************************************
	DALI_AL_RET DaliAL_queryNumberOfInstances(uint8_t * p_retVal);
*******************************************************************************/

void test_queryNumberOfInstances_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0xE6;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedQueryNumberOfInstancesRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryNumberOfInstances(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryNumberOfInstances_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0x49;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&expectedQueryNumberOfInstancesRequest, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryNumberOfInstances(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryNumberOfInstances_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryNumberOfInstances(DALI_ADDRESS_BROADCAST, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryNumberOfInstances_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	uint8_t ret = 0;
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_queryNumberOfInstances(DALI_ADDRESS_BROADCAST, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_queryInstanceType(uint8_t instance, uint8_t * p_rNULLl);
*******************************************************************************/

void test_queryInstanceType_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = DALI_INSTANCE_TYPE_GENERIC;

	DaliTL_request request = expectedQueryInstanceTypeRequest;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	dali_instance_type_t ret = DaliAL_queryInstanceType(DALI_ADDRESS_BROADCAST, request.instance, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL(DALI_INSTANCE_TYPE_GENERIC, ret);
}

void test_queryInstanceType_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryInstanceType(DALI_ADDRESS_BROADCAST, 1, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryInstanceType_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	uint8_t ret = 0;
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_queryInstanceType(DALI_ADDRESS_BROADCAST, 1, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_setEventFilter(uint8_t instance, uint32_t filter);
*******************************************************************************/

void test_setEventFilter_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint32_t filter = 0x00563412;

	DaliTL_request request = expectedSetEventFilterRequest;
	request.instance = 0xFE;

	DaliTL_request request_dtr0 = expectedDTR0Request;
	DaliTL_request request_dtr1 = expectedDTR1Request;
	DaliTL_request request_dtr2 = expectedDTR2Request;

	request_dtr0.txByte = (filter >> 0) & 0xFF;
	request_dtr1.txByte = (filter >> 8) & 0xFF;
	request_dtr2.txByte = (filter >> 16) & 0xFF;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request_dtr0, 1, NULL, 0, DALI_AL_RET_OK);
	DaliAL_processRequest_ExpectWithArrayAndReturn(&request_dtr1, 1, NULL, 0, DALI_AL_RET_OK);
	DaliAL_processRequest_ExpectWithArrayAndReturn(&request_dtr2, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_setEventFilter(0, filter, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_setEventFilter_ReturnsAnErrorIfEventFilterExceeds24bits()
{
	_DEBUG_OUT_TEST_NAME();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_setEventFilter(4, 0x01000000, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_disableInstance(uint8_t instance);
*******************************************************************************/

void test_disableInstance_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedDisableInstanceRequest,
		1, NULL, 0, DALI_AL_RET_OK
	);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_disableInstance(12, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_disableInstance_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedDisableInstanceRequest,
		1, NULL, 0, DALI_AL_RET_ERR
	);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_disableInstance(4, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_queryInputValue(uint8_t instance, uint8_t * p_retVal);
*******************************************************************************/

void test_queryInputValue_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0xEB;

	DaliTL_request request = expectedQueryInputValueRequest;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryInputValue(DALI_ADDRESS_BROADCAST, 12, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryInputValue_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0x49;

	DaliTL_request request = expectedQueryInputValueRequest;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryInputValue(DALI_ADDRESS_BROADCAST, 24, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryInputValue_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryInputValue(DALI_ADDRESS_BROADCAST, 1, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryInputValue_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_queryInputValue(DALI_ADDRESS_BROADCAST, 1, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_queryInputValueLatch(uint8_t instance, uint8_t * p_retVal);
*******************************************************************************/

void test_queryInputValueLatch_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0xEB;
	const uint8_t instance = 12;

	DaliTL_request request = expectedQueryInputValueLatchRequest;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryInputValueLatch(DALI_ADDRESS_BROADCAST, instance, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryInputValueLatch_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t expected = 0x49;

	DaliTL_request request = expectedQueryInputValueLatchRequest;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryInputValueLatch(DALI_ADDRESS_BROADCAST, 24, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryInputValueLatch_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryInputValueLatch(DALI_ADDRESS_BROADCAST, 1, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryInputValueLatch_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	uint8_t ret = 0;
	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_queryInputValueLatch(DALI_ADDRESS_BROADCAST, 1, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_enableApplicationController();
*******************************************************************************/

void test_enableApplicationController_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedEnableApplicationControllerRequest,
		1, NULL, 0, DALI_AL_RET_OK
	);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_enableApplicationController(&status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}


void test_enableApplicationController_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedEnableApplicationControllerRequest,
		1, NULL, 0, DALI_AL_RET_ERR
	);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_enableApplicationController(&status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_disableApplicationController();
*******************************************************************************/

void test_disableApplicationController_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedDisableApplicationControllerRequest,
		1, NULL, 0, DALI_AL_RET_OK
	);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_disableApplicationController(&status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}


void test_disableApplicationController_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedDisableApplicationControllerRequest,
		1, NULL, 0, DALI_AL_RET_ERR
	);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_disableApplicationController(&status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}


/*******************************************************************************
	DALI_AL_RET DaliAL_queryApplicationControllerEnabled(uint8_t * p_retVal);
*******************************************************************************/

void test_queryApplicationControllerEnabled_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t expected = 0x00;

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedQueryApplicationControllerEnabledRequest,
		1, NULL, 0, DALI_AL_RET_OK
	);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	bool ret = DaliAL_queryApplicationControllerEnabled(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL(false, ret);
}

void test_queryApplicationControllerEnabled_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t expected = 0xFF;

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedQueryApplicationControllerEnabledRequest,
		1, NULL, 0, DALI_AL_RET_OK
	);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	bool ret = DaliAL_queryApplicationControllerEnabled(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(true, ret);
}

void test_queryApplicationControllerEnabled_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryApplicationControllerEnabled(DALI_ADDRESS_BROADCAST, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryApplicationControllerEnabled_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t expected = 0x49;

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedQueryApplicationControllerEnabledRequest,
		1, NULL, 0, DALI_AL_RET_ERR
	);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	uint8_t ret = DaliAL_queryApplicationControllerEnabled(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_startQuiescentMode();
*******************************************************************************/

void test_startQuiescentMode_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedStartQuiescentModeRequest,
		1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_startQuiescentMode(&status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}


void test_startQuiescentMode_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedStartQuiescentModeRequest,
		1, NULL, 0, DALI_AL_RET_ERR);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_startQuiescentMode(&status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_stopQuiescentMode();
*******************************************************************************/

void test_stopQuiescentMode_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedStopQuiescentModeRequest,
		1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_stopQuiescentMode(&status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}


void test_stopQuiescentMode_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedStopQuiescentModeRequest,
		1, NULL, 0, DALI_AL_RET_ERR);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_stopQuiescentMode(&status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}


/*******************************************************************************
	DALI_AL_RET DaliAL_queryQuiescentMode(uint8_t * p_retVal);
*******************************************************************************/

void test_queryQuiescentMode_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t expected = 0xEB;

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedQueryQuiescentModeRequest,
		1, NULL, 0, DALI_AL_RET_OK
	);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	uint8_t ret = DaliAL_queryQuiescentMode(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryQuiescentMode_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t expected = 0x49;

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedQueryQuiescentModeRequest,
		1, NULL, 0, DALI_AL_RET_OK
	);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	uint8_t ret = DaliAL_queryQuiescentMode(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_UINT8(expected, ret);
}

void test_queryQuiescentMode_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryQuiescentMode(DALI_ADDRESS_BROADCAST, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryQuiescentMode_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t expected = 0x49;

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedQueryQuiescentModeRequest,
		1, NULL, 0, DALI_AL_RET_ERR
	);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	uint8_t ret = DaliAL_queryQuiescentMode(DALI_ADDRESS_BROADCAST, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_queryDimmingCurve(uint8_t deviceType, uint8_t * p_retVal);
*******************************************************************************/

void test_queryDimmingCurve_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t expected = 0xE9;
	expectedEnableDeviceTypeRequest.txByte = 9;

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedEnableDeviceTypeRequest,
		1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedQueryDimmingCurveRequest,
		1, NULL, 0, DALI_AL_RET_OK
	);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryDimmingCurve(DALI_ADDRESS_BROADCAST, 9, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_HEX8(0xE9, ret);
}

void test_queryDimmingCurve_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t expected = 0x47;
	expectedEnableDeviceTypeRequest.txByte = 14;

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedEnableDeviceTypeRequest,
		1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	DaliAL_processRequest_ExpectWithArrayAndReturn(
		&expectedQueryDimmingCurveRequest,
		1, NULL, 0, DALI_AL_RET_OK
	);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	uint8_t ret = DaliAL_queryDimmingCurve(DALI_ADDRESS_BROADCAST, 14, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
	TEST_ASSERT_EQUAL_HEX8(0x47, ret);
}

void test_queryDimmingCurve_nullptr()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryDimmingCurve(DALI_ADDRESS_BROADCAST, 10, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_queryDimmingCurve_ReturnsAnErrorIfEnableDeviceTypeFails()
{
	_DEBUG_OUT_TEST_NAME();
	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryDimmingCurve(DALI_ADDRESS_BROADCAST, 15, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

void test_queryDimmingCurve_ReturnsAnErrorIfProcessRequestFails()
{
	_DEBUG_OUT_TEST_NAME();
	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);
	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_ERR);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_queryDimmingCurve(DALI_ADDRESS_BROADCAST, 23, &status);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

/*******************************************************************************
	DALI_AL_RET DaliAL_selectDimmingCurve(uint8_t deviceType, uint8_t data);
*******************************************************************************/
void test_selectDimmingCurve_SendsTheCorrectValuesToTheTransportLayer()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t curve 		= 0;
	uint8_t device_type = 13;

	DaliTL_request request_dtr0 = expectedDTR0Request;
	DaliTL_request request_edt 	= expectedEnableDeviceTypeRequest;
	DaliTL_request request_sdc 	= expectedSelectDimmingCurveRequest;

	request_dtr0.txByte = curve;
	request_edt.txByte 	= device_type;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request_dtr0, 1, NULL, 0, DALI_AL_RET_OK);
	DaliAL_processRequest_ExpectWithArrayAndReturn(&request_edt, 1, NULL, 0, DALI_AL_RET_OK);
	DaliAL_processRequest_ExpectWithArrayAndReturn(&request_sdc, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_selectDimmingCurve(device_type, curve, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_selectDimmingCurve_SendsTheCorrectValuesToTheTransportLayer_2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t curve 		= 0;
	uint8_t device_type = 17;

	DaliTL_request request_dtr0 = expectedDTR0Request;
	DaliTL_request request_edt 	= expectedEnableDeviceTypeRequest;
	DaliTL_request request_sdc 	= expectedSelectDimmingCurveRequest;

	request_dtr0.txByte = curve;
	request_edt.txByte 	= device_type;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request_dtr0, 1, NULL, 0, DALI_AL_RET_OK);
	DaliAL_processRequest_ExpectWithArrayAndReturn(&request_edt, 1, NULL, 0, DALI_AL_RET_OK);
	DaliAL_processRequest_ExpectWithArrayAndReturn(&request_sdc, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;
	DaliAL_selectDimmingCurve(device_type, curve, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_OK, status);
}

void test_selectDimmingCurve_ReturnsAnErrorIfDimmingCurveIsGreaterThan1()
{
	_DEBUG_OUT_TEST_NAME();
	//DTR0 call retuns successfully, Second call i.e. Setfade fails
	DaliAL_processRequest_IgnoreAndReturn(DALI_AL_RET_OK);

	dali_bus_status_t status = DALI_BUS_STATUS_INVALID;

	DaliAL_selectDimmingCurve(3, 2, &status);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_INVALID, status);
}

void test_DaliAL_encode_address_short_zero(void) {
	uint8_t address = DaliAL_encode_address(0, 0, DALI_ADDRESS_MODE_SHORT);
	TEST_ASSERT_EQUAL_UINT8(0x00, address);
}

void test_DaliAL_encode_address_short_packed(void) {
	uint8_t address = DaliAL_encode_address(23, 0xAA, DALI_ADDRESS_MODE_SHORT);
	TEST_ASSERT_EQUAL_UINT8(0x2E, address);
}

void test_DaliAL_encode_address_short_out_of_range(void) {
	uint8_t address = DaliAL_encode_address(64, 0xFF, DALI_ADDRESS_MODE_SHORT);
	TEST_ASSERT_EQUAL_UINT8(0x00, address);
}

void test_DaliAL_encode_address_broadcast(void) {
	uint8_t address = DaliAL_encode_address(0, 0, DALI_ADDRESS_MODE_BROADCAST);
	TEST_ASSERT_EQUAL_UINT8(0xFF, address);
}

void test_DaliAL_encode_address_special_zero(void) {
	uint8_t address = DaliAL_encode_address(0, 0x00, DALI_ADDRESS_MODE_SPECIAL);
	TEST_ASSERT_EQUAL_UINT8(0x00, address);
}

void test_DaliAL_encode_address_special_packed(void) {
	uint8_t address = DaliAL_encode_address(23, 0xAA, DALI_ADDRESS_MODE_SPECIAL);
	TEST_ASSERT_EQUAL_UINT8(0xAA, address);
}

void test_DaliAL_encode_address_special_out_of_range(void) {
	uint8_t address = DaliAL_encode_address(64, 0xFF, DALI_ADDRESS_MODE_SPECIAL);
	TEST_ASSERT_EQUAL_UINT8(0xFF, address);
}

void test_DaliAL_encode_address_group_zero(void) {
	uint8_t address = DaliAL_encode_address(0, 0x00, DALI_ADDRESS_MODE_GROUP);
	TEST_ASSERT_EQUAL_UINT8(0x80, address);
}

void test_DaliAL_encode_address_group_packed(void) {
	uint8_t address = DaliAL_encode_address(23, 0xAA, DALI_ADDRESS_MODE_GROUP);
	TEST_ASSERT_EQUAL_UINT8(0xAE, address);
}

void test_DaliAL_encode_address_group_out_of_range(void) {
	uint8_t address = DaliAL_encode_address(64, 0xFF, DALI_ADDRESS_MODE_GROUP);
	TEST_ASSERT_EQUAL_UINT8(0x80, address);
}

void test_DaliAL_command_tx_bad_command(void) {
	DALI_AL_RET ret;

	ret = DaliAL_command_tx(0, DALI_COMMAND_NONE, DALI_ADDRESS_MODE_SHORT, NULL, NULL);
	TEST_ASSERT_EQUAL(DALI_BUS_STATUS_INVALID, ret);

	ret = DaliAL_command_tx(0, DALI_COMMAND_NONE, DALI_ADDRESS_MODE_SPECIAL, NULL, NULL);
	TEST_ASSERT_EQUAL(DALI_BUS_STATUS_INVALID, ret);

	ret = DaliAL_command_tx(0, DALI_COMMAND_NONE, DALI_ADDRESS_MODE_BROADCAST, NULL, NULL);
	TEST_ASSERT_EQUAL(DALI_BUS_STATUS_INVALID, ret);

	ret = DaliAL_command_tx(0, DALI_COMMAND_NONE, DALI_ADDRESS_MODE_GROUP, NULL, NULL);
	TEST_ASSERT_EQUAL(DALI_BUS_STATUS_INVALID, ret);
}

void test_DaliAL_command_tx_short_ff16_nselector(void) {
	DaliTL_request request = expectedQueryStatusRequest;

	request.address = 0x01;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request, 1, NULL, 0, DALI_AL_RET_OK);

	dali_bus_status_t ret = DaliAL_command_tx(0, DALI_QUERY_STATUS, DALI_ADDRESS_MODE_SHORT, NULL, NULL);
	TEST_ASSERT_EQUAL(DALI_BUS_STATUS_OK, ret);
}

void test_DaliAL_command_tx_short_ff16_selector(void) {
	DaliTL_request request = expectedDAPCRequest;

	request.address = 0x00;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request, 1, NULL, 0, DALI_AL_RET_OK);

	dali_bus_status_t ret = DaliAL_command_tx(0, DALI_COMMAND_DIRECT_ARC_POWER_CONTROL, DALI_ADDRESS_MODE_SHORT, NULL, NULL);
	TEST_ASSERT_EQUAL(DALI_BUS_STATUS_OK, ret);
}

void test_DaliAL_command_tx_broadcast_ff16_dapc(void) {
	DaliTL_request request = expectedDAPCRequest;

	request.address = 0xFE;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request, 1, NULL, 0, DALI_AL_RET_OK);

	dali_bus_status_t ret = DaliAL_command_tx(0, DALI_COMMAND_DIRECT_ARC_POWER_CONTROL, DALI_ADDRESS_MODE_BROADCAST, NULL, NULL);
	TEST_ASSERT_EQUAL(DALI_BUS_STATUS_OK, ret);
}

void test_DaliAL_command_tx_short_ff16_response(void) {
	DaliTL_request request = expectedActualLevelRequest;

	request.address = 0x0F;

	uint8_t expected_response = 0xA1;

	DaliAL_processRequest_ExpectWithArrayAndReturn(&request, 1, NULL, 0, DALI_AL_RET_OK);

	DaliAL_processRequest_IgnoreArg_resp();
	DaliAL_processRequest_ReturnThruPtr_resp(&expected_response);

	uint8_t response = 0x00;

	dali_bus_status_t ret = DaliAL_command_tx(7, DALI_QUERY_ACTUAL_LEVEL, DALI_ADDRESS_MODE_SHORT, NULL, &response);
	TEST_ASSERT_EQUAL(DALI_BUS_STATUS_OK, ret);

	TEST_ASSERT_EQUAL_UINT8(expected_response, response);
}
