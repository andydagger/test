#include "unity.h"
#include "stdbool.h"
#include "stdio.h"
#include "DaliTx_impl.h"
#include "mock_DaliHW.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

TEST_FILE("DaliTx_impl.c");

/*******************************************************************************
    Setup and Expected values.
*******************************************************************************/

void setUp(void)
{

}

void tearDown(void)
{

}


/*******************************************************************************
    void DaliTx_encodeAndLoadFrame(uint8_t * p_frameData, bool b_isFrame24Bit)
*******************************************************************************/

void test_encodeAndLoadFrame_16Bit()
{
_DEBUG_OUT_TEST_NAME

    uint8_t testByteFrame[2] = {0x12, 0x34};
    bool b_isFrame24Bit = false;
    uint8_t endocedBitFrameExpected[25] = {1, 0,0,0,1,0,0,1,0, 0,0,1,1,0,1,0,0, 0,0,0,0,0,0,0,0};
    DaliTx_encodeAndLoadFrame(testByteFrame, b_isFrame24Bit);   
    TEST_ASSERT_EQUAL_UINT8_ARRAY(endocedBitFrameExpected, DaliTx_getEncodedBitsFramePtr(), 25);
}


void test_encodeAndLoadFrame_IgnoreThirdByte()
{
_DEBUG_OUT_TEST_NAME

    uint8_t testByteFrame[3] = {0x12, 0x34, 0x56};
    bool b_isFrame24Bit = false;
    uint8_t endocedBitFrameExpected[25] = {1, 0,0,0,1,0,0,1,0, 0,0,1,1,0,1,0,0, 0,0,0,0,0,0,0,0};
    DaliTx_encodeAndLoadFrame(testByteFrame, b_isFrame24Bit);   
    TEST_ASSERT_EQUAL_UINT8_ARRAY(endocedBitFrameExpected, DaliTx_getEncodedBitsFramePtr(), 25);
}

void test_encodeAndLoadFrame_24Bit()
{
_DEBUG_OUT_TEST_NAME

    uint8_t testByteFrame[3] = {0x12, 0x34, 0x56};
    bool b_isFrame24Bit = true;
    uint8_t endocedBitFrameExpected[25] = {1, 0,0,0,1,0,0,1,0, 0,0,1,1,0,1,0,0, 0,1,0,1,0,1,1,0};
    DaliTx_encodeAndLoadFrame(testByteFrame, b_isFrame24Bit);  
    TEST_ASSERT_EQUAL_UINT8_ARRAY(endocedBitFrameExpected, DaliTx_getEncodedBitsFramePtr(), 25);
}

void test_encodeAndLoadFrame_ClearBetweenTransmit()
{
_DEBUG_OUT_TEST_NAME

    uint8_t testByteFrame[3] = {0x12, 0x34, 0x56};
    uint8_t endocedBitFrameExpected[25] = {1, 0,0,0,1,0,0,1,0, 0,0,1,1,0,1,0,0, 0,0,0,0,0,0,0,0};
    DaliTx_encodeAndLoadFrame(testByteFrame, true);  
    DaliTx_encodeAndLoadFrame(testByteFrame, false);  
    TEST_ASSERT_EQUAL_UINT8_ARRAY(endocedBitFrameExpected, DaliTx_getEncodedBitsFramePtr(), 25);
}

/*******************************************************************************
    void DaliTx_run(void)
*******************************************************************************/

void test_ISR16bit()
{
_DEBUG_OUT_TEST_NAME

    uint8_t isrCalls;
    uint8_t testByteFrame[2] = {0x12, 0x34};
    uint8_t daliOutputStates[40] = { 0,1 , /* Start Bit */
                                    1,0 , 1,0 , 1,0 , 0,1 , 1,0 , 1,0 , 0,1 , 1,0 ,  /* Byte 1 */
                                    1,0 , 1,0 , 0,1 , 0,1 , 1,0 , 0,1 , 1,0 , 1,0 ,  /* Byte 2 */
                                    1,1 , 1,1 , 1,1}; /* Stop Bit */
    DaliTx_encodeAndLoadFrame(testByteFrame, false);
    DaliTx_resetIsr();
    for(isrCalls = 0; isrCalls < 40; isrCalls++)
    {
        DaliHw_SetOutputTo_Expect(daliOutputStates[isrCalls]);
        if(isrCalls == 39) DaliHw_StopTimer_Expect();
        TEST_ASSERT_EQUAL_INT(DALI_TX_BUSY, DaliTx_getTxIsrStatus());
        DaliTx_run();
    }
    TEST_ASSERT_EQUAL_INT(DALI_TX_DONE, DaliTx_getTxIsrStatus());
}

void test_ISR24bit()
{
_DEBUG_OUT_TEST_NAME

    uint8_t isrCalls;
    uint8_t testByteFrame[3] = {0x12, 0x34, 0x56};
    uint8_t daliOutputStates[56] = { 0,1 , /* Start Bit */
                                    1,0 , 1,0 , 1,0 , 0,1 , 1,0 , 1,0 , 0,1 , 1,0 ,  /* Byte 1 */
                                    1,0 , 1,0 , 0,1 , 0,1 , 1,0 , 0,1 , 1,0 , 1,0 ,  /* Byte 2 */
                                    1,0 , 0,1 , 1,0 , 0,1 , 1,0 , 0,1 , 0,1 , 1,0 ,  /* Byte 3 */
                                    1,1 , 1,1 , 1,1}; /* Stop Bit */
    DaliTx_encodeAndLoadFrame(testByteFrame, true);
    DaliTx_resetIsr();
    for(isrCalls = 0; isrCalls < 56; isrCalls++)
    {
        DaliHw_SetOutputTo_Expect(daliOutputStates[isrCalls]);
        if(isrCalls == 55) DaliHw_StopTimer_Expect();
        TEST_ASSERT_EQUAL_INT(DALI_TX_BUSY, DaliTx_getTxIsrStatus());
        DaliTx_run();
    }
    TEST_ASSERT_EQUAL_INT(DALI_TX_DONE, DaliTx_getTxIsrStatus());
}

void test_ISR24bit_2()
{
_DEBUG_OUT_TEST_NAME

    uint8_t isrCalls;
    uint8_t testByteFrame[3] = {0x92, 0x34, 0x56};
    uint8_t daliOutputStates[56] = { 0,1 , /* Start Bit */
                                    0,1 , 1,0 , 1,0 , 0,1 , 1,0 , 1,0 , 0,1 , 1,0 ,  /* Byte 1 */
                                    1,0 , 1,0 , 0,1 , 0,1 , 1,0 , 0,1 , 1,0 , 1,0 ,  /* Byte 2 */
                                    1,0 , 0,1 , 1,0 , 0,1 , 1,0 , 0,1 , 0,1 , 1,0 ,  /* Byte 3 */
                                    1,1 , 1,1 , 1,1}; /* Stop Bit */
    DaliTx_encodeAndLoadFrame(testByteFrame, true);
    DaliTx_resetIsr();
    for(isrCalls = 0; isrCalls < 56; isrCalls++)
    {
        DaliHw_SetOutputTo_Expect(daliOutputStates[isrCalls]);
        if(isrCalls == 55) DaliHw_StopTimer_Expect();
        TEST_ASSERT_EQUAL_INT(DALI_TX_BUSY, DaliTx_getTxIsrStatus());
        DaliTx_run();
    }
    TEST_ASSERT_EQUAL_INT(DALI_TX_DONE, DaliTx_getTxIsrStatus());
}

void test_ISR24bit_3()
{
_DEBUG_OUT_TEST_NAME

    uint8_t isrCalls;
    uint8_t testByteFrame[3] = {0x92, 0x34, 0x57};
    uint8_t daliOutputStates[56] = { 0,1 , /* Start Bit */
                                    0,1 , 1,0 , 1,0 , 0,1 , 1,0 , 1,0 , 0,1 , 1,0 ,  /* Byte 1 */
                                    1,0 , 1,0 , 0,1 , 0,1 , 1,0 , 0,1 , 1,0 , 1,0 ,  /* Byte 2 */
                                    1,0 , 0,1 , 1,0 , 0,1 , 1,0 , 0,1 , 0,1 , 0,1 ,  /* Byte 3 */
                                    1,1 , 1,1 , 1,1}; /* Stop Bit */
    DaliTx_encodeAndLoadFrame(testByteFrame, true);
    DaliTx_resetIsr();
    for(isrCalls = 0; isrCalls < 56; isrCalls++)
    {
        DaliHw_SetOutputTo_Expect(daliOutputStates[isrCalls]);
        if(isrCalls == 55) DaliHw_StopTimer_Expect();
        TEST_ASSERT_EQUAL_INT(DALI_TX_BUSY, DaliTx_getTxIsrStatus());
        DaliTx_run();
    }
    TEST_ASSERT_EQUAL_INT(DALI_TX_DONE, DaliTx_getTxIsrStatus());
}