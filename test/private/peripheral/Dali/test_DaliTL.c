#include "unity.h"
#include "stdbool.h"
#include "DaliTL.h"
#include "mock_DaliTL_impl.h"
#include "mock_DaliTx.h"
#include "mock_DaliRx.h"
#include "mock_systick.h"
#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

TEST_FILE("DaliTL.c");

/*******************************************************************************
    Setup and Expected values.
*******************************************************************************/

void setUp(void)
{

}

void tearDown(void)
{

}

/*******************************************************************************
    DALI_TL_RET DaliTL_startXfer(DaliTL_request* p_request);
*******************************************************************************/

void test_startXfer_SendsTheFrameToTheTxLayer()
{
_DEBUG_OUT_TEST_NAME
    DaliTL_request request = {
        .address = 0x12,
	    .txByte = 0x34,
	    .b_answerExpected = false,
        .b_sendTwice = false,
        .b_24bit = false,
        .instance = 0
    };
	
    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_setRequest_Ignore();
    DaliTL_setState_Ignore();
    uint8_t expectedData[2] = {0x12, 0x34};
    DaliTx_reset_Ignore();
    DaliTx_start_ExpectWithArrayAndReturn(expectedData, 2, false, true);

    TEST_ASSERT_EQUAL_INT(DALI_TL_RET_OK, DaliTL_startXfer(&request));
}

void test_startXfer_SendsTheFrameToTheTxLayer_DifferentValues()
{
_DEBUG_OUT_TEST_NAME
    DaliTL_request request = {
        .address = 0x4E,
	    .txByte = 0x92,
	    .b_answerExpected = false,
        .b_sendTwice = false,
        .b_24bit = false,
        .instance = 0
    };
	
    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_setRequest_Ignore();
    DaliTL_setState_Ignore();
    uint8_t expectedData[2] = {0x4E, 0x92};
    DaliTx_reset_Ignore();
    DaliTx_start_ExpectWithArrayAndReturn(expectedData, 2, false, true);

    TEST_ASSERT_EQUAL_INT(DALI_TL_RET_OK, DaliTL_startXfer(&request));
}

void test_startXfer_SendsTheFrameToTheTxLayer_24BitCommand()
{
_DEBUG_OUT_TEST_NAME
    DaliTL_request request = {
        .address = 0xFF,
	    .txByte = 0xE4,
	    .b_answerExpected = false,
        .b_sendTwice = false,
        .b_24bit = true,
        .instance = 0x48
    };

    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_setRequest_Ignore();
    DaliTL_setState_Ignore();
    DaliTx_reset_Ignore();
    uint8_t expectedData[3] = {0xFF, 0x48, 0xE4};
    DaliTx_start_ExpectWithArrayAndReturn(expectedData, 3, true, true);

    TEST_ASSERT_EQUAL_INT(DALI_TL_RET_OK, DaliTL_startXfer(&request));
}

void test_startXfer_EntersTheTxWaitState()
{
_DEBUG_OUT_TEST_NAME
    DaliTL_request request = {
        .address = 0xFF,
	    .txByte = 0xE4,
	    .b_answerExpected = false,
        .b_sendTwice = false,
        .b_24bit = true,
        .instance = 0x48
    };
    
    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_setRequest_Ignore();
    DaliTL_setState_Expect(DALITL_STATE_TX_WAIT);

    DaliTx_reset_Ignore();
    DaliTx_start_IgnoreAndReturn(true);

    TEST_ASSERT_EQUAL_INT(DALI_TL_RET_OK, DaliTL_startXfer(&request));
}


void test_startXfer_ResetsTheTxLayer()
{
_DEBUG_OUT_TEST_NAME
    DaliTL_request request = {
        .address = 0xFF,
	    .txByte = 0xE4,
	    .b_answerExpected = false,
        .b_sendTwice = false,
        .b_24bit = true,
        .instance = 0x48
    };
    
    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_setRequest_Ignore();
    DaliTL_setState_Ignore();
    DaliTx_reset_Expect();
    DaliTx_start_ExpectAnyArgsAndReturn(true);

    TEST_ASSERT_EQUAL_INT(DALI_TL_RET_OK, DaliTL_startXfer(&request));
}

void test_startXfer_MakesALocalCopyOfTheRequest()
{
_DEBUG_OUT_TEST_NAME
    DaliTL_request request = {
        .address = 0x12,
	    .txByte = 0x34,
	    .b_answerExpected = false,
        .b_sendTwice = false,
        .b_24bit = false,
        .instance = 0
    };

    get_systick_ms_IgnoreAndReturn(0);
	DaliTL_setRequest_ExpectWithArray(&request, 1);
    DaliTL_setState_Ignore();
    //N.B. this is 0x13 beacuse bit is set to indicate standard command
    uint8_t expectedData[2] = {0x13, 0x34};
    DaliTx_reset_Ignore();
    DaliTx_start_IgnoreAndReturn(true);

    TEST_ASSERT_EQUAL_INT(DALI_TL_RET_OK, DaliTL_startXfer(&request));
}

void test_startXfer_ReturnsAnErrorIfRequestIsNull()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_EQUAL_INT(DALI_TL_RET_ERR, DaliTL_startXfer(NULL));
}

void test_startXfer_ReturnsAnErrorIfBothSendTwiceAndAnswerExpectedSet()
{
_DEBUG_OUT_TEST_NAME
    DaliTL_request request = {
        .address = 0x12,
	    .txByte = 0x34,
	    .b_answerExpected = true,
        .b_sendTwice = true,
        .b_24bit = false,
        .instance = 0
    };

    TEST_ASSERT_EQUAL_INT(DALI_TL_RET_ERR, DaliTL_startXfer(&request));
}

/*******************************************************************************
    DaliTL_status DaliTL_run(void);
*******************************************************************************/
void test_run_callsTheIdleFunctionIfStateIsIdle()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);

    DaliTL_getState_IgnoreAndReturn(DALITL_STATE_IDLE);

    DaliTL_Idle_ExpectAndReturn(DALITL_STATUS_BUSY);
    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_run());
}

void test_run_callsTheIdleFunctionIfStateIsIdle_DifferentStatus()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_getState_IgnoreAndReturn(DALITL_STATE_IDLE);

    DaliTL_Idle_ExpectAndReturn(DALITL_STATUS_TIMEOUT);
    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_TIMEOUT, DaliTL_run());
}

void test_run_callsTheTxWaitFunctionIfStateIsTxWait()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_getState_IgnoreAndReturn(DALITL_STATE_TX_WAIT);

    DaliTL_TxWait_ExpectAndReturn(DALITL_STATUS_BUSY);
    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_run());
}

void test_run_callsTheTxWaitFunctionIfStateIsTxWait_DifferentStatus()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_getState_IgnoreAndReturn(DALITL_STATE_TX_WAIT);

    DaliTL_TxWait_ExpectAndReturn(DALITL_STATUS_TIMEOUT);
    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_TIMEOUT, DaliTL_run());
}

void test_run_callsTheSendTwiceFunctionIfStateIsSendTwice()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_getState_IgnoreAndReturn(DALITL_STATE_TWICE_DELAY);

    DaliTL_SendTwiceDelay_ExpectAndReturn(DALITL_STATUS_BUSY);
    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_run());
}

void test_run_callsTheSendTwiceFunctionIfStateIsSendTwice_DifferentStatus()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_getState_IgnoreAndReturn(DALITL_STATE_TWICE_DELAY);

    DaliTL_SendTwiceDelay_ExpectAndReturn(DALITL_STATUS_TIMEOUT);
    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_TIMEOUT, DaliTL_run());
}

void test_run_callsTheRxWaitFunctionIfStateIsRxWait()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_getState_IgnoreAndReturn(DALITL_STATE_REPLY_WAIT);

    DaliTL_RxWait_ExpectAndReturn(DALITL_STATUS_BUSY);
    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_BUSY, DaliTL_run());
}

void test_run_callsTheRxWaitFunctionIfStateIsRxWait_DifferentStatus()
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_getState_IgnoreAndReturn(DALITL_STATE_REPLY_WAIT);

    DaliTL_RxWait_ExpectAndReturn(DALITL_STATUS_TIMEOUT);
    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_TIMEOUT, DaliTL_run());
}

void test_run_returnTimeout()
{
_DEBUG_OUT_TEST_NAME

    DaliTL_request request = {
        .address = 0x12,
	    .txByte = 0x34,
	    .b_answerExpected = false,
        .b_sendTwice = false,
        .b_24bit = false,
        .instance = 0
    };

	DaliTL_setRequest_ExpectWithArray(&request, 1);
    DaliTL_setState_Ignore();
    DaliTx_reset_Ignore();
    DaliTx_start_IgnoreAndReturn(true);

    get_systick_ms_IgnoreAndReturn(0);
    DaliTL_startXfer(&request);
    
    get_systick_ms_IgnoreAndReturn(200);

    TEST_ASSERT_EQUAL_INT(DALITL_STATUS_TIMEOUT, DaliTL_run());
}

/*******************************************************************************
    uint8_t DaliTL_getResponse(void);
*******************************************************************************/

void test_getResponse_returnsTheRxResponse()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_getResponse_ExpectAndReturn(0x49);
    TEST_ASSERT_EQUAL_UINT8(0x49, DaliTL_getResponse());
}


void test_getResponse_returnsTheRxResponse_DifferentValue()
{
_DEBUG_OUT_TEST_NAME
    DaliRx_getResponse_ExpectAndReturn(0xE3);
    TEST_ASSERT_EQUAL_UINT8(0xE3, DaliTL_getResponse());
}
