#include "unity.h"
#include "stdbool.h"
#include "DaliAL_impl.h"
#include "mock_DaliTL.h"
#include "mock_sysmon.h"

#define DEBUG_OUT_TEST_NAME()    printf(" %s",__func__)

TEST_FILE("DaliAL_impl.c");

/*******************************************************************************
	Setup and Expected values.
*******************************************************************************/

void setUp(void) {
	dali_faults = 0;
}

void tearDown(void) {

}

/*******************************************************************************
	DALI_AL_RET DaliAL_processRequest(
		DaliTL_request* request,
		uint8_t* p_retVal););
*******************************************************************************/

void test_processRequest_SendsTheCorrectCallsToTheTransportLayer(void) {
	DEBUG_OUT_TEST_NAME();

	//We do not care about the data, as we are simply testing that the pointers
	//are passed to the DaliTL
	DaliTL_request expectedRequest = {.b_answerExpected = false};
	uint8_t expectedResponse;

	//Compare TL object used with the AL's TL object, and compare the data inside the request
	DaliTL_startXfer_ExpectAndReturn(&expectedRequest, DALI_TL_RET_OK);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_DONE);
	DaliTL_getResponse_ExpectAndReturn(expectedResponse);

	DaliAL_processRequest(&expectedRequest, &expectedResponse);
}

void test_processRequest_KeepsCallingRunUntilTheStatusIsDone(void) {
	DEBUG_OUT_TEST_NAME();

	//We do not care about the data, as we are simply testing that the pointers
	//are passed to the DaliTL
	DaliTL_request expectedRequest = {.b_answerExpected = false};
	uint8_t expectedResponse;

	//Compare TL object used with the AL's TL object, and compare the data inside the request
	DaliTL_startXfer_ExpectAndReturn(&expectedRequest, DALI_TL_RET_OK);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_BUSY);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_BUSY);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_BUSY);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_BUSY);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_BUSY);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_DONE);
	DaliTL_getResponse_ExpectAndReturn(expectedResponse);

	DaliAL_processRequest(&expectedRequest, &expectedResponse);
}

void test_processRequest_ReturnsCorruptOnCollision(void) {
	DEBUG_OUT_TEST_NAME();

	DaliTL_request request = {
		.b_answerExpected = true
	};

	DaliTL_startXfer_ExpectAndReturn(&request, DALI_TL_RET_OK);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_CORRUPT);

	uint8_t response;
	dali_bus_status_t status = DaliAL_processRequest(&request, &response);

	TEST_ASSERT_EQUAL(DALI_BUS_STATUS_CORRUPT, status);
}

void test_processRequest_ClearsTheSysmonFlagIfStatusIsDone(void) {
	DEBUG_OUT_TEST_NAME();

	//We do not care about the data, as we are simply testing that the pointers
	//are passed to the DaliTL
	DaliTL_request expectedRequest = {.b_24bit = false, .b_answerExpected = true};
	uint8_t expectedResponse;

	//Compare TL object used with the AL's TL object, and compare the data inside the request
	DaliTL_startXfer_ExpectAndReturn(&expectedRequest, DALI_TL_RET_OK);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_BUSY);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_BUSY);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_BUSY);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_BUSY);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_BUSY);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_DONE);
	DaliTL_getResponse_ExpectAndReturn(expectedResponse);

	clear_sysmon_fault_Expect(DALI_FAULT);

	DaliAL_processRequest(&expectedRequest, &expectedResponse);
}

void test_processRequest_returnsAnErrorIfTransportLayerReturnsTimeout(void) {
	DEBUG_OUT_TEST_NAME();

	dali_faults = 0;
	//We do not care about the data, as we are simply testing that the pointers
	//are passed to the DaliTL
	DaliTL_request expectedRequest = {.b_answerExpected = false};
	uint8_t expectedResponse;


	//Compare TL object used with the AL's TL object, and compare the data inside the request
	DaliTL_startXfer_ExpectAndReturn(&expectedRequest, DALI_TL_RET_OK);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_TIMEOUT);

	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_TIMEOUT, DaliAL_processRequest(&expectedRequest, &expectedResponse));
}

void test_processRequest_setsSysmonFlagIfDaliTLFails3Times(void) {
	DEBUG_OUT_TEST_NAME();

	dali_faults = 0;

	//We do not care about the data, as we are simply testing that the pointers
	//are passed to the DaliTL
	DaliTL_request expectedRequest = {.b_24bit = false, .b_answerExpected = true};
	uint8_t expectedResponse;


	//Compare TL object used with the AL's TL object, and compare the data inside the request
	DaliTL_startXfer_ExpectAndReturn(&expectedRequest, DALI_TL_RET_OK);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_TIMEOUT);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_TIMEOUT, DaliAL_processRequest(&expectedRequest, &expectedResponse));
	DaliTL_startXfer_ExpectAndReturn(&expectedRequest, DALI_TL_RET_OK);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_TIMEOUT);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_TIMEOUT, DaliAL_processRequest(&expectedRequest, &expectedResponse));
	DaliTL_startXfer_ExpectAndReturn(&expectedRequest, DALI_TL_RET_OK);
	DaliTL_run_ExpectAndReturn(DALITL_STATUS_TIMEOUT);
	set_sysmon_fault_Expect(DALI_FAULT);
	TEST_ASSERT_EQUAL_INT(DALI_BUS_STATUS_TIMEOUT, DaliAL_processRequest(&expectedRequest, &expectedResponse));
}
