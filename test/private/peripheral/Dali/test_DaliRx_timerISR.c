#include "unity.h"
#include "stdbool.h"
#include "DaliRx_timerISR.h"
#include "mock_DaliRx.h"
#include "mock_DaliRx_edgeISR.h"
#include "mock_DaliHW.h"
#include "DaliRx_common.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

TEST_FILE("DaliRx_impl.c");

void setUp(void)
{
    
}
void tearDown(void)
{

}

/*******************************************************************************
    void DaliRx_TimerISR_run(void);
*******************************************************************************/
void test_timerISR_incrementsTheTickCount()
{
    DaliRx_status = DALI_RX_BUSY;
    DaliRx_tickCount = 0;
    DaliHw_ReadInput_IgnoreAndReturn(true);
    
    DaliRx_pinState = true;
    DaliRx_TimerISR_run();
    
    TEST_ASSERT_EQUAL(1, DaliRx_tickCount);
}

void test_timerISR_incrementsTheTickCount_MultipleTimes()
{
    DaliRx_tickCount = 0;
    DaliHw_ReadInput_IgnoreAndReturn(true);
    DaliRx_pinState = true;
    DaliRx_TimerISR_run();
    DaliRx_TimerISR_run();
    DaliRx_TimerISR_run();
    DaliRx_TimerISR_run();
    DaliRx_TimerISR_run();
    DaliRx_TimerISR_run();
    TEST_ASSERT_EQUAL(6, DaliRx_tickCount);
}

void test_timerISR_incrementsTheTickCountSinceLastEdge()
{
    DaliRx_tickCountSinceLastEdge = 0;
    DaliHw_ReadInput_IgnoreAndReturn(true);
    DaliRx_pinState = true;
    DaliRx_TimerISR_run();
    TEST_ASSERT_EQUAL(1, DaliRx_tickCountSinceLastEdge);
}

void test_timerISR_incrementsTheTickCountSinceLastEdge_MultipleTimes()
{
    DaliRx_tickCountSinceLastEdge = 0;
    DaliHw_ReadInput_IgnoreAndReturn(true);
    DaliRx_pinState = true;
    DaliRx_TimerISR_run();
    DaliRx_TimerISR_run();
    DaliRx_TimerISR_run();
    DaliRx_TimerISR_run();
    DaliRx_TimerISR_run();
    DaliRx_TimerISR_run();
    TEST_ASSERT_EQUAL(6, DaliRx_tickCountSinceLastEdge);
}


void test_timerISR_setsTheRxTimeoutConditionIfTheTickCountIsMoreThanMaximum()
{
    DaliRx_tickCount = 100000;
    DaliRx_frameMaximumTickCount = 100000;

    DaliRx_status = DALI_RX_BUSY;
    DaliHw_ReadInput_IgnoreAndReturn(true);
    DaliRx_reset_Expect();
    DaliRx_TimerISR_run();

    TEST_ASSERT_EQUAL(DALI_RX_TIMEOUT, DaliRx_status);
}

void test_timerISR_timeoutConditionIsNotSetIfTheTickCountIsLessThanMaximum()
{
    DaliRx_tickCount = 99998;   //100000 - 2 
    //N.B. ISR does increment before check
    DaliRx_frameMaximumTickCount = 100000;

    DaliRx_status = DALI_RX_BUSY;
    DaliHw_ReadInput_IgnoreAndReturn(true);
    DaliRx_pinState = true;
    DaliRx_TimerISR_run();

    TEST_ASSERT_EQUAL(DALI_RX_BUSY, DaliRx_status);
}


void test_timerISR_stopConditionSetIfTimeSinceLastEdgeIsMoreThanThreshold()
{
    DaliRx_tickCount = 0;
    DaliRx_startFlag = true;
    DaliRx_tickCountSinceLastEdge = 2400;   
    DaliRx_stopConditionTickCount = 2400;

    DaliRx_status = DALI_RX_BUSY;
    DaliHw_ReadInput_IgnoreAndReturn(true);
    DaliRx_reset_Expect();
    DaliRx_pinState = true;
    DaliRx_TimerISR_run();

    TEST_ASSERT_EQUAL(DALI_RX_STOP, DaliRx_status);
}

void test_timerISR_callsTheEdgeISROnFallingEdge()
{
     DaliRx_status = DALI_RX_BUSY;
    DaliRx_tickCount = 0;
    DaliRx_tickCountSinceLastEdge = 0;
    DaliHw_ReadInput_IgnoreAndReturn(true);
    
    DaliRx_pinState = false;
    DaliRx_InputEdgeISR_run_Expect();
    DaliRx_TimerISR_run();
}


void test_timerISR_callsTheEdgeISROnRisingEdge()
{
     DaliRx_status = DALI_RX_BUSY;
    DaliRx_tickCount = 0;
    DaliRx_tickCountSinceLastEdge = 0;
    DaliHw_ReadInput_IgnoreAndReturn(false);
    
    DaliRx_pinState = true;
    DaliRx_InputEdgeISR_run_Expect();
    DaliRx_TimerISR_run();
}

/*******************************************************************************
    uint32_t DaliRx_ticksSinceLastEdge(void);
*******************************************************************************/
void test_ticksSinceLastEdge_returnsTheCorrectTickCount()
{
    DaliRx_tickCountSinceLastEdge = 1947;

    TEST_ASSERT_EQUAL_UINT32(1947, DaliRx_ticksSinceLastEdge());
}

void test_ticksSinceLastEdge_returnsTheCorrectTickCount_DifferentValue()
{
    DaliRx_tickCountSinceLastEdge = 402;

    TEST_ASSERT_EQUAL_UINT32(402, DaliRx_ticksSinceLastEdge());
}

void test_ticksSinceLastEdge_resetsTheLocalCounter()
{
    DaliRx_tickCountSinceLastEdge = 402;

    TEST_ASSERT_EQUAL_UINT32(402, DaliRx_ticksSinceLastEdge());

    TEST_ASSERT_EQUAL_UINT32(0, DaliRx_tickCountSinceLastEdge);
}