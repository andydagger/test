#include "unity.h"

#include "lorawaninit.c"

#include "mock_config_lorawan.h"
#include "mock_hw.h"
#include "mock_lorawan.h"
#include "mock_lorawanmac.h"
#include "mock_common.h"
#include "mock_debug.h"
#include "mock_FreeRTOS.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_LorawanStatusFlags.h"
#include "mock_productionMonitor.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

#define ALLOW_SESSION_RECOVERY true
#define FORCE_SESSION_RESET false

bool requestToRecoverSession = ALLOW_SESSION_RECOVERY;

void setUp(void)
{
    debugprint_Ignore();
}
void tearDown(void)
{

}

static void mockResetModule(){
    lorawan_disable_module_Expect();
    FreeRTOSDelay_Expect(500);
    lorawan_enable_module_Expect();
    FreeRTOSDelay_Expect(1000);    
}

static void mockActivateComs(){
    lorawan_set_baudrate_Expect(57600);
    mockResetModule();
    lorawan_irq_enable_Expect();
    lorawan_rx_disable_Expect();
}

//-----------------------------------------------------------------
void test_init_restoreSessionAtBootUp_ok(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    mockActivateComs();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    lorawanmac_send_query_ExpectAndReturn("mac get deveui\r\n", mchp_hwstr, true);
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("1122334455667788", 16);
    lorawanmac_send_query_ExpectAndReturn("mac get upctr\r\n", mchp_hwstr, true);
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("12", 12);
    lz_ascii_to_uint32_ascii_ExpectAndReturn("12", 12);
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_TRUE(requestToRecoverSession);
    TEST_ASSERT_EQUAL_STRING("1122334455667788", hwEui);
}
void test_init_restoreSessionAtBootUp_FailsToGetBootUpStringFirstTimeRound(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    mockActivateComs();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RRN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 39);
    FreeRTOSDelay_ExpectAnyArgs();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    lorawanmac_send_query_ExpectAndReturn("mac get deveui\r\n", mchp_hwstr, true);
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("1122334455667788", 16);
    lorawanmac_send_query_ExpectAndReturn("mac get upctr\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("12", 12);
    lz_ascii_to_uint32_ascii_ExpectAndReturn("12", 12);
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_TRUE(requestToRecoverSession);
}
void test_init_restoreSessionAtBootUp_FailsToBootUp_MaximumRetries(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    mockActivateComs();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    FreeRTOSDelay_ExpectAnyArgs();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    FreeRTOSDelay_ExpectAnyArgs();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    TEST_ASSERT_FALSE(lorawaninit_exe(&requestToRecoverSession));
}
void test_init_restoreSessionAtBootUp_failsToGetDevEuiFirstTimeRound(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    mockActivateComs();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    lorawanmac_send_query_ExpectAndReturn("mac get deveui\r\n", mchp_hwstr, false);
    FreeRTOSDelay_Expect(RESET_RETRY_DELAY_MS);
    lorawanmac_send_query_ExpectAndReturn("mac get deveui\r\n", mchp_hwstr, true);
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("1122334455667788", 16);
    lorawanmac_send_query_ExpectAndReturn("mac get upctr\r\n", mchp_hwstr, true);
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("12", 12);
    lz_ascii_to_uint32_ascii_ExpectAndReturn("12", 12);
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_TRUE(requestToRecoverSession);
}
void test_init_restoreSessionAtBootUp_failsToGetDevEuiMaxRetries(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    mockActivateComs();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    lorawanmac_send_query_ExpectAndReturn("mac get deveui\r\n", mchp_hwstr, false);
    FreeRTOSDelay_Expect(RESET_RETRY_DELAY_MS);
    lorawanmac_send_query_ExpectAndReturn("mac get deveui\r\n", mchp_hwstr, false);
    FreeRTOSDelay_Expect(RESET_RETRY_DELAY_MS);
    lorawanmac_send_query_ExpectAndReturn("mac get deveui\r\n", mchp_hwstr, false);
    TEST_ASSERT_FALSE(lorawaninit_exe(&requestToRecoverSession));
}

void test_init_restoreSessionAtBootUp_FrameCounterReadBackFailsFirstTimeRound(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    mockActivateComs();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    mchp_hwstr[0] = '2'; // shall be cleared before next query
    lorawanmac_send_query_ExpectAndReturn("mac get deveui\r\n", mchp_hwstr, true);
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("1122334455667788", 16);
    lorawanmac_send_query_ExpectAndReturn("mac get upctr\r\n", mchp_hwstr, false);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ExpectAndReturn("mac get upctr\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("12", 12);
    lz_ascii_to_uint32_ascii_ExpectAndReturn("12", 12);
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_TRUE(requestToRecoverSession);
}
//-----------------------------------------------------------------
static void mockStartUpWithLowFrameCounter(){
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    mockActivateComs();
    //
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    //
    lorawanmac_send_query_ExpectAndReturn("mac get deveui\r\n", mchp_hwstr, true);
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("AA22334455667788", 16);
    //
    lorawanmac_send_query_ExpectAndReturn("mac get upctr\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("9", 9);
    lz_ascii_to_uint32_ascii_ExpectAnyArgsAndReturn(9);
}
void test_init_bootUpOnLowFrameCounter_ok(){
_DEBUG_OUT_TEST_NAME
    mockStartUpWithLowFrameCounter();
    //
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys factoryRESET\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    //
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac reset 868\r\n", true);
    // setup module
    productionMonitor_getUnitIsUndertest_IgnoreAndReturn(false);
    lorawanmac_send_cmd_ok_only_IgnoreAndReturn(true);
    lorawanmac_send_cmd_ok_only_slow_IgnoreAndReturn(true);
    //
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_FALSE(requestToRecoverSession);
}
void test_init_bootUpOnLowFrameCounter_failsOnFactoryResetFirstTimeRoundNoResponse(){
_DEBUG_OUT_TEST_NAME
    mockStartUpWithLowFrameCounter();
    //
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys factoryRESET\r\n", mchp_hwstr, false);
    lorawanmac_send_query_IgnoreArg_p_res();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys factoryRESET\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    //
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac reset 868\r\n", true);
    // setup module
    productionMonitor_getUnitIsUndertest_IgnoreAndReturn(false);
    lorawanmac_send_cmd_ok_only_IgnoreAndReturn(true);
    lorawanmac_send_cmd_ok_only_slow_IgnoreAndReturn(true);
    //
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_FALSE(requestToRecoverSession);
}
void test_init_bootUpOnLowFrameCounter_failsOnFactoryResetFirstTimeRoundCorruptResp(){
_DEBUG_OUT_TEST_NAME
    mockStartUpWithLowFrameCounter();
    //
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys factoryRESET\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RRN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 39);
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys factoryRESET\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    //
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac reset 868\r\n", true);
    // setup module
    productionMonitor_getUnitIsUndertest_IgnoreAndReturn(false);
    lorawanmac_send_cmd_ok_only_IgnoreAndReturn(true);
    lorawanmac_send_cmd_ok_only_slow_IgnoreAndReturn(true);
    //
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_FALSE(requestToRecoverSession);
}
void test_init_bootUpOnLowFrameCounter_failsOn868ResetFirstTimeRound(){
_DEBUG_OUT_TEST_NAME
    mockStartUpWithLowFrameCounter();
    //
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys factoryRESET\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    //
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac reset 868\r\n", false);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac reset 868\r\n", true);
    // setup module
    productionMonitor_getUnitIsUndertest_IgnoreAndReturn(false);
    lorawanmac_send_cmd_ok_only_IgnoreAndReturn(true);
    lorawanmac_send_cmd_ok_only_slow_IgnoreAndReturn(true);
    //
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_FALSE(requestToRecoverSession);
}
//-----------------------------------------------------------------
void test_init_setupSequenceKeepsGoingUnitlAllOk(){
_DEBUG_OUT_TEST_NAME
    mockStartUpWithLowFrameCounter();
    //
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys factoryRESET\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    //
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac reset 868\r\n", true);
    // setup module
    productionMonitor_getUnitIsUndertest_IgnoreAndReturn(false);
    lorawanmac_send_cmd_ok_only_ExpectAnyArgsAndReturn(true);
    lorawanmac_send_cmd_ok_only_ExpectAnyArgsAndReturn(true);
    lorawanmac_send_cmd_ok_only_ExpectAnyArgsAndReturn(true);
    lorawanmac_send_cmd_ok_only_ExpectAnyArgsAndReturn(false);
    lorawanmac_send_cmd_ok_only_ExpectAnyArgsAndReturn(true);
    lorawanmac_send_cmd_ok_only_ExpectAnyArgsAndReturn(true);
    lorawanmac_send_cmd_ok_only_IgnoreAndReturn(true);
    lorawanmac_send_cmd_ok_only_slow_IgnoreAndReturn(true);
    //
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_FALSE(requestToRecoverSession);
}
void test_init_setupFailOnSaveIsNotBlocking(){
_DEBUG_OUT_TEST_NAME
    mockStartUpWithLowFrameCounter();
    //
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys factoryRESET\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    //
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac reset 868\r\n", true);
    // setup module
    productionMonitor_getUnitIsUndertest_IgnoreAndReturn(false);
    lorawanmac_send_cmd_ok_only_IgnoreAndReturn(true);
    lorawanmac_send_cmd_ok_only_slow_IgnoreAndReturn(false);
    //
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_FALSE(requestToRecoverSession);
}
void test_init_setupExecutesSavesAtEnd(){
_DEBUG_OUT_TEST_NAME
    mockStartUpWithLowFrameCounter();
    //
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys factoryRESET\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    //
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac reset 868\r\n", true);
    // setup module
    productionMonitor_getUnitIsUndertest_IgnoreAndReturn(false);
    uint8_t setupCommands = 30;
    do{
        lorawanmac_send_cmd_ok_only_ExpectAnyArgsAndReturn(true);
    }
    while(--setupCommands > 0);
    lorawanmac_send_cmd_ok_only_slow_ExpectAndReturn("mac save\r\n", true);
    //
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_FALSE(requestToRecoverSession);

}
void test_init_setupCorrectStringFormating(){
_DEBUG_OUT_TEST_NAME
    mockStartUpWithLowFrameCounter();
    //
    mockResetModule();
    hwEui[0] = 0;
    lorawanmac_send_query_ExpectAndReturn("sys factoryRESET\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    //
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac reset 868\r\n", true);
    // setup module
    productionMonitor_getUnitIsUndertest_ExpectAndReturn(false);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set deveui 1122334455667788\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set appeui 0102030405060708\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set appkey 01020304050607080102030405060708\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set mcastdevaddr 016fe84b\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set mcastappskey cd3311c0e3b15c838dc7468c0b9524d7\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set mcastnwkskey 0b6f6b0d48939e18da0af09d488c2539\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set appskey 11223344556677889900aabbccddeeff\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set nwkskey 12341234123412345678567856785678\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set devaddr 00001122\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set pwridx 1\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set dr 0\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set adr on\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set retx 6\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set linkchk 0\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set rxdelay1 1000\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ar on\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set rx2 0 869525000\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set sync 34\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set upctr 0\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set dnctr 0\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch dcycle 0 302\r\n", true); //302
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch dcycle 1 302\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch dcycle 2 302\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch drrange 0 0 5\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch drrange 1 0 5\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch drrange 2 0 5\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch status 0 on\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch status 1 on\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch status 2 on\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set class c\r\n", true);

    lorawanmac_send_cmd_ok_only_slow_ExpectAndReturn("mac save\r\n", false);
    //
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_FALSE(requestToRecoverSession);
}
void test_init_setupCorrectStringFormating_whenUnderTest(){
_DEBUG_OUT_TEST_NAME
    mockStartUpWithLowFrameCounter();
    //
    mockResetModule();
    hwEui[0] = 0;
    lorawanmac_send_query_ExpectAndReturn("sys factoryRESET\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    //
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac reset 868\r\n", true);
    // setup module
    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set deveui AA22334455667788\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set appeui 8877665544332211\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set appkey 11223344556677881122334455667788\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set mcastdevaddr 016fe84b\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set mcastappskey cd3311c0e3b15c838dc7468c0b9524d7\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set mcastnwkskey 0b6f6b0d48939e18da0af09d488c2539\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set appskey 11223344556677889900aabbccddeeff\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set nwkskey 12341234123412345678567856785678\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set devaddr 00001122\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set pwridx 1\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set dr 0\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set adr on\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set retx 6\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set linkchk 0\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set rxdelay1 1000\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ar on\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set rx2 0 869525000\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set sync 34\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set upctr 0\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set dnctr 0\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch dcycle 0 302\r\n", true); //302
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch dcycle 1 302\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch dcycle 2 302\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch drrange 0 0 5\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch drrange 1 0 5\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch drrange 2 0 5\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch status 0 on\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch status 1 on\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set ch status 2 on\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn( "mac set class c\r\n", true);

    lorawanmac_send_cmd_ok_only_slow_ExpectAndReturn("mac save\r\n", false);
    //
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_FALSE(requestToRecoverSession);
}
//-----------------------------------------------------------------
void test_init_calledWithoutAllowingSessionRestore(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = FORCE_SESSION_RESET;
    mockActivateComs();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    //
    lorawanmac_send_query_ExpectAndReturn("mac get deveui\r\n", mchp_hwstr, true);
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("1122334455667788", 16);
    //
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys factoryRESET\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    //
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac reset 868\r\n", true);
    // setup module
    productionMonitor_getUnitIsUndertest_IgnoreAndReturn(false);
    lorawanmac_send_cmd_ok_only_IgnoreAndReturn(true);
    lorawanmac_send_cmd_ok_only_slow_IgnoreAndReturn(false);
    //
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
}
//-----------------------------------------------------------------
void test_init_storesCorrectRadioFirmwareVersionRC20(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    mockActivateComs();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 RC20 Oct 09 2018 16:36:17", 38);
    lorawanmac_send_query_ExpectAndReturn("mac get deveui\r\n", mchp_hwstr, true);
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("1122334455667788", 16);
    lorawanmac_send_query_ExpectAndReturn("mac get upctr\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("12", 12);
    lz_ascii_to_uint32_ascii_ExpectAndReturn("12", 12);
    //
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_EQUAL((105*256 + 20), get_lorawaninit_fw_version()); 
}
void test_init_storesCorrectRadioFirmwareVersion105(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    mockActivateComs();
    mockResetModule();
    lorawanmac_send_query_ExpectAndReturn("sys reset\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("RN2483 1.0.5 Oct 31 2018 16:36:17", 33);
    lorawanmac_send_query_ExpectAndReturn("mac get deveui\r\n", mchp_hwstr, true);
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("1122334455667788", 16);
    lorawanmac_send_query_ExpectAndReturn("mac get upctr\r\n", mchp_hwstr, true);
    lorawanmac_send_query_IgnoreArg_p_res();
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("12", 12);
    lz_ascii_to_uint32_ascii_ExpectAndReturn("12", 12);
    //
    TEST_ASSERT_TRUE(lorawaninit_exe(&requestToRecoverSession));
    TEST_ASSERT_EQUAL(27032, get_lorawaninit_fw_version()); 
}
//-----------------------------------------------------------------

void test_get_hweui_notYetAvailable()
{
_DEBUG_OUT_TEST_NAME
    devEuiReadyForReading = false;
    char DevEUi[17] = {0};
    get_lorawaninit_hweui(DevEUi);
    TEST_ASSERT_EQUAL_STRING("", DevEUi);
}

void test_get_hweui_ready()
{
_DEBUG_OUT_TEST_NAME
    devEuiReadyForReading = true;
    char DevEUi[17] = {0};
    sprintf(hwEui, "1122334455667788");
    get_lorawaninit_hweui(DevEUi);
    TEST_ASSERT_EQUAL_STRING("1122334455667788", DevEUi);
}
