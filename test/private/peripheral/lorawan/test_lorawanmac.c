#include "unity.h"
#include "lorawanmac.c"

#include "mock_config_lorawan.h"
#include "mock_lorawan.h"
#include "mock_lorawanhw.h"
#include "mock_common.h"
#include "mock_systick.h"
#include "mock_debug.h"
#include "mock_sysmon.h"
#include "mock_FreeRTOS.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_led.h"
#include "mock_nwk_com.h"
#include "mock_errorCode.h"
#include "mock_LorawanStatusFlags.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

#define SET_APPKEY_CMD "mac set appkey 11223344556677881122334455667788\r\n"
#define QUERY_CMD "mac get status\r\n"
#define NWK_STACK_RTOS_FREEUP_PERIOD 10
#define mockReSendDelayExpect() FreeRTOSDelay_Expect(5000)

uint32_t runTime = 0;
char respBuffer[DOWN_MSG_MICROCHIP_STRING_MAX] = {0};

static void mockWipeRespBuffer(void){
    uint16_t i = 0;
    for(i = 0 ; i < (DOWN_MSG_MICROCHIP_STRING_MAX - 1); i++){
        respBuffer[i] = 0;
    }
    return;
}

void setUp(void)
{
    debugprint_Ignore();
    runTime = 0;
    mockWipeRespBuffer();
}
void tearDown(void)
{

}

//-----------------------------------------------------------------
void test_send_cmd_ok_only(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    TEST_ASSERT_TRUE(lorawanmac_send_cmd_ok_only(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_waitsUntilOkReceived(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    TEST_ASSERT_TRUE(lorawanmac_send_cmd_ok_only(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_returnsFalseOnTimeout(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Ignore();
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime = MCHP_PRIMARY_RESP_TIMEOUT_MS;
    get_systick_ms_ExpectAndReturn(runTime);
    sysmon_report_error_Expect(LORA, NO_RESPONSE);
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_returnsFalseOnNullPointer(){
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only(NULL));
}
void test_send_cmd_ok_only_returnsFalseOnNotRecognised(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(NOT_RECOGNISED);
    sysmon_report_error_Expect(LORA, INVALID_RESP);
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_returnsFalseOnInvalidParam(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(INVALID_PARAM);
    sysmon_report_error_Expect(LORA, INVALID_RESP);
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_returnsFalseOnNotJoined(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(NOT_JOINED);
    sysmon_report_error_Expect(LORA, INVALID_RESP);
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_returnsFalseOnMacErr(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_ERR);
    sysmon_report_error_Expect(LORA, INVALID_RESP);
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_returnsFalseOnNone(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(NONE);
    sysmon_report_error_Expect(LORA, INVALID_RESP);
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only(SET_APPKEY_CMD));
}
//-----------------------------------------------------------------
void test_send_cmd_ok_only_slow(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    TEST_ASSERT_TRUE(lorawanmac_send_cmd_ok_only_slow(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_slow_waitsUntilOkReceived(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += MCHP_PRIMARY_RESP_TIMEOUT_MS;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += MCHP_PRIMARY_RESP_TIMEOUT_MS;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += MCHP_PRIMARY_RESP_TIMEOUT_MS;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    TEST_ASSERT_TRUE(lorawanmac_send_cmd_ok_only_slow(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_slow_returnsFalseOnTimeout(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Ignore();
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime = MCHP_PRIMARY_SLOW_RESP_TIMEOUT_MS;
    get_systick_ms_ExpectAndReturn(runTime);
    sysmon_report_error_Expect(LORA, NO_RESPONSE);
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only_slow(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_slow_returnsFalseOnNullPointer(){
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only_slow(NULL));
}
void test_send_cmd_ok_only_slow_returnsFalseOnNotRecognised(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(NOT_RECOGNISED);
    sysmon_report_error_Expect(LORA, INVALID_RESP);
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only_slow(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_slow_returnsFalseOnInvalidParam(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(INVALID_PARAM);
    sysmon_report_error_Expect(LORA, INVALID_RESP);
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only_slow(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_slow_returnsFalseOnNotJoined(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(NOT_JOINED);
    sysmon_report_error_Expect(LORA, INVALID_RESP);
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only_slow(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_slow_returnsFalseOnMacErr(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_ERR);
    sysmon_report_error_Expect(LORA, INVALID_RESP);
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only_slow(SET_APPKEY_CMD));
}
void test_send_cmd_ok_only_slow_returnsFalseOnNone(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(SET_APPKEY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    get_lorawanhw_resp_status_ExpectAndReturn(NONE);
    sysmon_report_error_Expect(LORA, INVALID_RESP);
    TEST_ASSERT_FALSE(lorawanmac_send_cmd_ok_only_slow(SET_APPKEY_CMD));
}
//-----------------------------------------------------------------
void test_send_query(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(QUERY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    lorawanhw_fetch_resp_Expect(respBuffer);
    TEST_ASSERT_TRUE(lorawanmac_send_query(QUERY_CMD, respBuffer));
}
void test_send_query_responseCopiedAcrossOnSuccess(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(QUERY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    lorawanhw_fetch_resp_Expect(respBuffer);
    lorawanhw_fetch_resp_ReturnArrayThruPtr_p_to_target_buffer("01020304\r\n", NWK_STACK_RTOS_FREEUP_PERIOD);
    TEST_ASSERT_TRUE(lorawanmac_send_query(QUERY_CMD, respBuffer));
    TEST_ASSERT_EQUAL_STRING("01020304\r\n", respBuffer);
}
void test_send_query_waitsUntilRespReceived(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Expect(QUERY_CMD);
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += MCHP_PRIMARY_RESP_TIMEOUT_MS;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += MCHP_PRIMARY_RESP_TIMEOUT_MS;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += MCHP_PRIMARY_RESP_TIMEOUT_MS;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += MCHP_PRIMARY_RESP_TIMEOUT_MS;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    lorawanhw_fetch_resp_Expect(respBuffer);
    TEST_ASSERT_TRUE(lorawanmac_send_query(QUERY_CMD, respBuffer));
    TEST_ASSERT_EQUAL_STRING("", respBuffer);
}
void test_send_query_returnsFalseOnTimeout(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor_Ignore();
    get_systick_ms_ExpectAndReturn(runTime);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime);
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    runTime = MCHP_PRIMARY_SLOW_RESP_TIMEOUT_MS;
    get_systick_ms_ExpectAndReturn(runTime);
    sysmon_report_error_Expect(LORA, NO_RESPONSE);
    TEST_ASSERT_FALSE(lorawanmac_send_query(QUERY_CMD, respBuffer));
}
void test_send_query_returnsFalseOnNullCmdPointer(){
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(lorawanmac_send_query(NULL, respBuffer));
}
void test_send_query_returnsFalseOnNullRespPointer(){
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(lorawanmac_send_query(QUERY_CMD, NULL));
}
//-----------------------------------------------------------------
void test_send_cmd_sendUplink(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_ExpectAndReturn(runTime); // capture start time for send operation
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");
    get_systick_ms_ExpectAndReturn(runTime); // load primary resp time
    get_systick_sec_ExpectAndReturn(runTime/1000); // check send op TO

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD); runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime); // check resp TO
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    get_systick_ms_ExpectAndReturn(runTime); // load sec resp TO
    get_systick_sec_ExpectAndReturn(runTime/1000); // check send op TO

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD); runTime += 1000;
    get_systick_ms_ExpectAndReturn(runTime); // check resp TO
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_TX_OK);
    get_systick_sec_ExpectAndReturn(runTime/1000); // check send op TO

    TEST_ASSERT_TRUE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplinkFirstResponseTimeOut(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");
    get_systick_ms_ExpectAndReturn(runTime); // load primary resp time

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD); 
    get_systick_ms_ExpectAndReturn(runTime); // check resp TO
    get_lorawanhw_resp_status_ExpectAndReturn(NONE);

    runTime += MCHP_PRIMARY_RESP_TIMEOUT_MS;
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD); 
    get_systick_ms_ExpectAndReturn(runTime); // check resp TO

    mockReSendDelayExpect();
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");
    get_systick_ms_ExpectAndReturn(runTime); // load primary resp time

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD); 
    get_systick_ms_ExpectAndReturn(runTime); // check resp TO
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    get_systick_ms_ExpectAndReturn(runTime); // load sec resp TO

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD); runTime += 1000;
    get_systick_ms_ExpectAndReturn(runTime); // check resp TO
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_TX_OK);

    TEST_ASSERT_TRUE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplinkCorrectStatesOrder(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_TX_OK);

    TEST_ASSERT_TRUE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_secondRespTimeOutTriggersReSend(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0); 
    FreeRTOSDelay_Ignore();
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");
    get_systick_ms_ExpectAndReturn(runTime); // load primary resp time

    runTime += NWK_STACK_RTOS_FREEUP_PERIOD;
    get_systick_ms_ExpectAndReturn(runTime); // check resp TO
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    get_systick_ms_ExpectAndReturn(runTime); // load sec resp TO

    get_systick_ms_ExpectAndReturn(runTime); // check resp TO
    get_lorawanhw_resp_status_ExpectAndReturn(NONE);

    runTime += MCHP_SECONDARY_RESP_TIMEOUT_MS + 1;
    get_systick_ms_ExpectAndReturn(runTime); // check resp TO

    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");
    get_systick_ms_ExpectAndReturn(runTime); // load primary resp time

    get_systick_ms_ExpectAndReturn(runTime); // check resp TO
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    get_systick_ms_ExpectAndReturn(runTime); // load sec resp TO

    get_systick_ms_ExpectAndReturn(runTime); // check resp TO
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_TX_OK);

    TEST_ASSERT_TRUE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_oparationTimeOutFollowingOk(){
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    FreeRTOSDelay_Ignore();
    lorawanhw_tx_isr_supervisor_Ignore();
    
    get_systick_sec_ExpectAndReturn(runTime); // ref
    get_systick_sec_ExpectAndReturn(runTime);
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    get_systick_sec_ExpectAndReturn(runTime);
    get_lorawanhw_resp_status_ExpectAndReturn(NONE);
    get_systick_sec_ExpectAndReturn(runTime);
    get_lorawanhw_resp_status_ExpectAndReturn(NONE);
    runTime = LORAWAN_SEND_CMD_TIMEOUT_SEC;
    get_systick_sec_ExpectAndReturn(runTime);
    get_lorawanhw_resp_status_ExpectAndReturn(NONE);
    runTime = LORAWAN_SEND_CMD_TIMEOUT_SEC + 1;
    get_systick_sec_ExpectAndReturn(runTime);

    LorawanStatusFlags_SetModuleNeedsInitialising_Expect();
    sysmon_report_error_Expect(LORA, SEND_MESSAGE_TIMEOUT);
    errorCode_storeCode_Expect(NWK_ERROR, 0x01);

    TEST_ASSERT_FALSE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}

void test_send_cmd_sendUplink_oparationTimeOutDueToDutyCycle(){
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    FreeRTOSDelay_Ignore();
    lorawanhw_tx_isr_supervisor_Ignore();
    
    get_systick_sec_ExpectAndReturn(runTime); // ref
    get_systick_sec_ExpectAndReturn(runTime);
    get_lorawanhw_resp_status_ExpectAndReturn(NO_FREE_CH);
    get_systick_sec_ExpectAndReturn(runTime);

    get_systick_sec_ExpectAndReturn(runTime);
    get_lorawanhw_resp_status_ExpectAndReturn(NO_FREE_CH);
    get_systick_sec_ExpectAndReturn(runTime);

    get_systick_sec_ExpectAndReturn(runTime);
    get_lorawanhw_resp_status_ExpectAndReturn(NO_FREE_CH);
    runTime = LORAWAN_SEND_CMD_TIMEOUT_SEC;
    get_systick_sec_ExpectAndReturn(runTime);

    get_systick_sec_ExpectAndReturn(runTime);
    get_lorawanhw_resp_status_ExpectAndReturn(NO_FREE_CH);
    runTime = LORAWAN_SEND_CMD_TIMEOUT_SEC + 1;
    get_systick_sec_ExpectAndReturn(runTime);

    sysmon_report_error_Expect(LORA, SEND_MESSAGE_TIMEOUT);
    errorCode_storeCode_Expect(NWK_ERROR, 0x01);

    TEST_ASSERT_FALSE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_FirstRespIsInvalidParameters(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(INVALID_PARAM);
    mockReSendDelayExpect();

    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_TX_OK);

    TEST_ASSERT_TRUE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_FirstRespIsKeysNotInit(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(KEYS_NOT_INIT);
    sysmon_report_error_Expect(LORA, KEYS_INIT);
    LorawanStatusFlags_SetModuleNeedsInitialising_Expect();

    TEST_ASSERT_FALSE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_FirstRespIsMcastReKey(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(MCAST_RE_KEY);
    sysmon_report_error_Expect(LORA, MCAST_KEYS_INIT);
    LorawanStatusFlags_SetMcastSessionShouldBeActive_Expect(false);

    TEST_ASSERT_FALSE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_FirstRespIsNoFreeChannel(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(NO_FREE_CH);
    mockReSendDelayExpect();
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(NO_FREE_CH);
    mockReSendDelayExpect();
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_TX_OK);

    TEST_ASSERT_TRUE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_FirstRespIsSilent(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(SILENT);
    LorawanStatusFlags_SetModuleNeedsInitialising_Expect();

    TEST_ASSERT_FALSE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_FirstRespIsBusy(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(BUSY);
    mockReSendDelayExpect();
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(BUSY);
    mockReSendDelayExpect();
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_TX_OK);

    TEST_ASSERT_TRUE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_FirstRespIsMacPaused(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_PAUSED);
    LorawanStatusFlags_SetModuleNeedsInitialising_Expect();

    TEST_ASSERT_FALSE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_FirstRespIsDenied(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(DENIED);
    LorawanStatusFlags_ClearJoinedFlag_Expect();

    TEST_ASSERT_TRUE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_FirstRespIsAccepted(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(ACCEPTED);
    LorawanStatusFlags_SetJoinedFlag_Expect();

    TEST_ASSERT_TRUE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_FirstRespIsNotJoined(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(NOT_JOINED);
    LorawanStatusFlags_SetModuleNeedsInitialising_Expect();

    TEST_ASSERT_FALSE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_FirstRespIsFrameCounter(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(FRM_CNTR_ERR);
    sysmon_report_error_Expect(LORA, FRAME_CNTR);
    LorawanStatusFlags_SetModuleNeedsInitialising_Expect();

    TEST_ASSERT_FALSE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_SecRespIsDataLength(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    lorawanhw_tx_isr_supervisor_Ignore();
    FreeRTOSDelay_Ignore();

    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    get_lorawanhw_resp_status_ExpectAndReturn(INVALID_DATA_LENGTH);
    sysmon_report_error_Expect(LORA, DATA_LENGTH);

    TEST_ASSERT_FALSE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_SecRespIsMacTxOk(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    lorawanhw_tx_isr_supervisor_Ignore();
    FreeRTOSDelay_Ignore();

    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_TX_OK);

    TEST_ASSERT_TRUE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_SecRespIsMacRx(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    lorawanhw_tx_isr_supervisor_Ignore();
    FreeRTOSDelay_Ignore();

    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_RX);
    set_lorawanhw_rx_string_waiting_Expect();

    TEST_ASSERT_TRUE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_SecRespIsMacErr(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    lorawanhw_tx_isr_supervisor_Ignore();
    FreeRTOSDelay_Ignore();

    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_ERR);
    sysmon_report_error_Expect(LORA, TX_MAC_ERR);
    LorawanStatusFlags_SetModuleSettingsNeedOptimising_Expect();

    TEST_ASSERT_FALSE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
void test_send_cmd_sendUplink_FirstRespIsNotRecognised(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    get_systick_ms_IgnoreAndReturn(0);
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(NOT_RECOGNISED);
    mockReSendDelayExpect();
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(NOT_RECOGNISED);
    mockReSendDelayExpect();
    
    lorawanhw_tx_isr_supervisor_Expect("mac tx uncnf 1 01020304\r\n");

    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(OK);
    
    FreeRTOSDelay_Expect(NWK_STACK_RTOS_FREEUP_PERIOD);
    get_lorawanhw_resp_status_ExpectAndReturn(MAC_TX_OK);

    TEST_ASSERT_TRUE(lorawanmac_send_cmd("mac tx uncnf 1 01020304\r\n"));
}
//-----------------------------------------------------------------
void mock_SendQuery(char * query, char * response, uint8_t respLength){
    lorawanhw_tx_isr_supervisor_Expect(query);
    get_systick_ms_IgnoreAndReturn(0);
    FreeRTOSDelay_Ignore();
    get_lorawanhw_rx_string_waiting_IgnoreAndReturn(true);
    lorawanhw_fetch_resp_ExpectAnyArgs();
    lorawanhw_fetch_resp_IgnoreArg_p_to_target_buffer();
    lorawanhw_fetch_resp_ReturnArrayThruPtr_p_to_target_buffer(response, respLength);
}
void test_get_datarate_return1(){
_DEBUG_OUT_TEST_NAME
    mock_SendQuery("mac get dr\r\n", "1\r\n", 3);
    lz_ascii_to_uint32_ascii_ExpectAndReturn("1\r\n", 1);
    TEST_ASSERT_EQUAL(1, get_lorawanmac_dr());
}
void test_get_datarate_return0(){
_DEBUG_OUT_TEST_NAME
    mock_SendQuery("mac get dr\r\n", "0\r\n", 3);
    lz_ascii_to_uint32_ascii_ExpectAndReturn("0\r\n", 0);
    TEST_ASSERT_EQUAL(0, get_lorawanmac_dr());
}
void test_get_datarate_returnTooHighAValue(){
_DEBUG_OUT_TEST_NAME
    mock_SendQuery("mac get dr\r\n", "7\r\n", 3);
    lz_ascii_to_uint32_ascii_ExpectAndReturn("7\r\n", 7);
    TEST_ASSERT_EQUAL(5, get_lorawanmac_dr());
}
void test_get_pwr_index_return1(){
_DEBUG_OUT_TEST_NAME
    mock_SendQuery("mac get pwridx\r\n", "1\r\n", 3);
    lz_ascii_to_uint32_ascii_ExpectAndReturn("1\r\n", 1);
    TEST_ASSERT_EQUAL(1, get_lorawanmac_pwridx());
}
void test_get_pwr_index_return0(){
_DEBUG_OUT_TEST_NAME
    mock_SendQuery("mac get pwridx\r\n", "0\r\n", 3);
    lz_ascii_to_uint32_ascii_ExpectAndReturn("0\r\n", 0);
    TEST_ASSERT_EQUAL(0, get_lorawanmac_pwridx());
}
void test_get_pwr_index_returnTooHighAValue(){
_DEBUG_OUT_TEST_NAME
    mock_SendQuery("mac get pwridx\r\n", "7\r\n", 3);
    lz_ascii_to_uint32_ascii_ExpectAndReturn("7\r\n", 7);
    TEST_ASSERT_EQUAL(5, get_lorawanmac_pwridx());
}
//-----------------------------------------------------------------
