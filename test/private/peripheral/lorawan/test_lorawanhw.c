#include "unity.h"

#include "lorawanhw.c"

#include "mock_config_lorawan.h"
#include "mock_config_pinout.h"
#include "mock_hw.h"
#include "mock_common.h"
#include "mock_debug.h"
#include "mock_sysmon.h"


#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

#define TEST_STRING_50BYTES_RX "mac rx 123 0011223344556677889900112233445566778899001122334455667788990011223344556677889900112233445566778899\r\n"
#define TEST_STRING_50BYTES "mac tx uncnf 123 0011223344556677889900112233445566778899001122334455667788990011223344556677889900112233445566778899\r\n"
#define TEST_STRING_NO_TERM_CHAR "mac tx uncnf 123 001122334455667788990011223344556677889900112233445566778899001122334455667788990011223344556677889900"
#define TEST_STRING_51BYTES "Xmac tx uncnf 123 0011223344556677889900112233445566778899001122334455667788990011223344556677889900112233445566778899\r\n"

#define UART_STAT_RXRDY         (0x01 << 0)			/*!< Receiver ready */
#define UART_STAT_TXRDY         (0x01 << 2)			/*!< Transmitter ready for data */

void mockWipeRxBuffer(void){
    uint16_t i = 0;
    n_char_received = 0;
    // Wipe rx irq buffer
    for(i = 0 ; i < (DOWN_MSG_MICROCHIP_STRING_MAX - 1); i++){
        rx_isr_buffer[i] = 0;
    }
    return;
}
void mockWipeTxBuffer(void){
    uint16_t i = 0;
    n_char_transmitted = 0;
    // Wipe rx irq buffer
    for(i = 0 ; i < (UP_MSG_MICROCHIP_STRING_MAX - 1); i++){
        tx_isr_buffer[i] = 0;
    }
    return;
}

void setUp(void)
{
    debugprint_Ignore();
    mockWipeRxBuffer();
    mockWipeTxBuffer();
}
void tearDown(void)
{

}

//-----------------------------------------------------------------
void test_tx_isr_supervisor_msgLoadedIntoTxBuffer(){
_DEBUG_OUT_TEST_NAME
    lorawan_rx_disable_Expect();
    lorawan_rx_byte_ExpectAndReturn('R');
    lorawan_tx_enable_Expect();
    lorawanhw_tx_isr_supervisor("mac tx uncnf 1 010203040506070809\r\n");
    TEST_ASSERT_EQUAL_STRING("mac tx uncnf 1 010203040506070809\r\n", tx_isr_buffer);
}
void test_tx_isr_supervisor_recevedStringWaitingFlagCleared(){
_DEBUG_OUT_TEST_NAME
    b_received_string_waiting = true;
    //
    lorawan_rx_disable_Ignore();
    lorawan_rx_byte_IgnoreAndReturn('R');
    lorawan_tx_enable_Ignore();
    lorawanhw_tx_isr_supervisor("mac tx uncnf 1 010203040506070809\r\n");
    TEST_ASSERT_FALSE(b_received_string_waiting);
}
void test_tx_isr_supervisor_nOfTransmittedCharsReset(){
_DEBUG_OUT_TEST_NAME
    n_char_transmitted = 10;
    //
    lorawan_rx_disable_Ignore();
    lorawan_rx_byte_IgnoreAndReturn('R');
    lorawan_tx_enable_Ignore();
    lorawanhw_tx_isr_supervisor("mac tx uncnf 1 010203040506070809\r\n");
    TEST_ASSERT_EQUAL(0, n_char_transmitted);
}
void test_tx_isr_supervisor_rxBufferWiped(){
_DEBUG_OUT_TEST_NAME
    sprintf((char *)rx_isr_buffer, "MOCK DATA");
    //
    lorawan_rx_disable_Ignore();
    lorawan_rx_byte_IgnoreAndReturn('R');
    lorawan_tx_enable_Ignore();
    lorawanhw_tx_isr_supervisor("mac tx uncnf 1 010203040506070809\r\n");
    uint16_t i = 0;
    for(i = 0 ; i < (DOWN_MSG_MICROCHIP_STRING_MAX - 1); i++){
        TEST_ASSERT_EQUAL(0, rx_isr_buffer[i]);
    }
    
}
void test_tx_isr_supervisor_nullPointerCatch(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor(NULL);
}
void test_tx_isr_supervisor_maxPayloadLength(){
_DEBUG_OUT_TEST_NAME
    lorawan_rx_disable_Ignore();
    lorawan_rx_byte_IgnoreAndReturn('R');
    lorawan_tx_enable_Ignore();
    lorawanhw_tx_isr_supervisor(TEST_STRING_50BYTES);
    TEST_ASSERT_EQUAL_STRING(TEST_STRING_50BYTES, tx_isr_buffer);
}
void test_tx_isr_supervisor_payloadTooLargeIgnored(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_tx_isr_supervisor(TEST_STRING_51BYTES);
}
//-----------------------------------------------------------------
void mockValidRxStringTest(uint32_t expecetdreturnedCode, char * receivedString){
    b_received_string_waiting = true;
    strcpy((char *)rx_isr_buffer, receivedString);
    lorawan_rx_enable_Ignore();
    TEST_ASSERT_EQUAL(expecetdreturnedCode, get_lorawanhw_resp_status());
}

void test_get_resp_status_stringWaitingFlagCleared(){
_DEBUG_OUT_TEST_NAME
    b_received_string_waiting = true;
    lorawan_rx_enable_Ignore();
    TEST_ASSERT_EQUAL(NOT_RECOGNISED, get_lorawanhw_resp_status());
}
void test_get_resp_status_enableRxIsr(){
_DEBUG_OUT_TEST_NAME
    b_received_string_waiting = true;
    lorawan_rx_enable_Expect();
    TEST_ASSERT_EQUAL(NOT_RECOGNISED, get_lorawanhw_resp_status());
}
void test_get_resp_status_noStringWaiting(){
_DEBUG_OUT_TEST_NAME
    b_received_string_waiting = false;
    TEST_ASSERT_EQUAL(NONE, get_lorawanhw_resp_status());
}
void test_get_resp_status_notRecognised(){
_DEBUG_OUT_TEST_NAME
    b_received_string_waiting = true;
    lorawan_rx_enable_Ignore();
    TEST_ASSERT_EQUAL(NOT_RECOGNISED, get_lorawanhw_resp_status());
}
void test_get_resp_status_ok(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(OK, "ok\r\n");
}
void test_get_resp_status_invalidParam(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(INVALID_PARAM, "invalid_param\r\n");
}
void test_get_resp_status_keysNotInit(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(KEYS_NOT_INIT, "keys_not_init\r\n");
}
void test_get_resp_status_mcastReKey(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(MCAST_RE_KEY, "mcast_re_key\r\n");
}
void test_get_resp_status_noFreeCh(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(NO_FREE_CH, "no_free_ch\r\n");
}
void test_get_resp_status_silent(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(SILENT, "silent\r\n");
}
void test_get_resp_status_busy(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(BUSY, "busy\r\n");
}
void test_get_resp_status_macPaused(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(MAC_PAUSED, "mac_paused\r\n");
}
void test_get_resp_status_denied(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(DENIED, "denied\r\n");
}
void test_get_resp_status_accepted(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(ACCEPTED, "accepted\r\n");
}
void test_get_resp_status_notJoined(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(NOT_JOINED, "not_joined\r\n");
}
void test_get_resp_status_frameCounterErr(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(FRM_CNTR_ERR, "frame_counter_err_rejoin_needed\r\n");
}
void test_get_resp_status_InvalidDataLength(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(INVALID_DATA_LENGTH, "invalid_data_length\r\n");
}
void test_get_resp_status_macTxOk(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(MAC_TX_OK, "mac_tx_ok\r\n");
}
void test_get_resp_status_macRx(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(MAC_RX, "mac_rx\r\n");
}
void test_get_resp_status_macErr(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(MAC_ERR, "mac_err\r\n");
}
void test_get_resp_status_macTxOkCorrupt(){
_DEBUG_OUT_TEST_NAME
    mockValidRxStringTest(NOT_RECOGNISED, "mmac_tx_ok\r\n");
}
//-----------------------------------------------------------------
void test_fetch_response_copiesBuffer(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    sprintf((char *)rx_isr_buffer, "01020304\r\n");
    lorawanhw_fetch_resp(targetBuff);
    TEST_ASSERT_EQUAL_STRING("01020304\r\n", targetBuff);
}
void test_fetch_response_copiesMaxRxPayload(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    strncpy((char *)rx_isr_buffer, TEST_STRING_50BYTES_RX, DOWN_MSG_MICROCHIP_STRING_MAX);
    lorawanhw_fetch_resp(targetBuff);
    TEST_ASSERT_EQUAL_STRING(TEST_STRING_50BYTES_RX, targetBuff);
}
void test_fetch_response_wipesRxBuffer(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    sprintf((char *)rx_isr_buffer, "01020304\r\n");
    lorawanhw_fetch_resp(targetBuff);
    TEST_ASSERT_EQUAL_STRING("", rx_isr_buffer);
}
void test_fetch_responseNullTargetPointer(){
_DEBUG_OUT_TEST_NAME
    lorawanhw_fetch_resp(NULL);
}
//-----------------------------------------------------------------
void test_get_rx_payload_returnsTrueOnValidMsg(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    sprintf((char *)rx_isr_buffer, "mac_rx 1 01020304\r\n");
    lorawan_rx_enable_Ignore();
    TEST_ASSERT_TRUE(get_lorawanhw_rx_payload(targetBuff));
}
void test_get_rx_payload_extractsPayload(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    sprintf((char *)rx_isr_buffer, "mac_rx 1 01020304\r\n");
    lorawan_rx_enable_Ignore();
    get_lorawanhw_rx_payload(targetBuff);
    TEST_ASSERT_EQUAL_STRING("01020304\r\n", targetBuff);
}
void test_get_rx_payload_wipesRxBufferValidPayload(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    sprintf((char *)rx_isr_buffer, "mac_rx 1 01020304\r\n");
    lorawan_rx_enable_Ignore();
    get_lorawanhw_rx_payload(targetBuff);
    TEST_ASSERT_EQUAL_STRING("", rx_isr_buffer);
}
void test_get_rx_payload_wipesRxBufferCorruptPayload(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    sprintf((char *)rx_isr_buffer, "mmac_rx 1 01020304\r\n");
    lorawan_rx_enable_Ignore();
    get_lorawanhw_rx_payload(targetBuff);
    TEST_ASSERT_EQUAL_STRING("", rx_isr_buffer);
}
void test_get_rx_payload_wipesRxBufferMissingSPaceChar(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    sprintf((char *)rx_isr_buffer, "mac_rx1 01020304\r\n");
    lorawan_rx_enable_Ignore();
    get_lorawanhw_rx_payload(targetBuff);
    TEST_ASSERT_EQUAL_STRING("", rx_isr_buffer);
}
void test_get_rx_payload_clearsStringWaitingFlag_validMsg(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    b_received_string_waiting = true;
    sprintf((char *)rx_isr_buffer, "mac_rx 1 01020304\r\n");
    lorawan_rx_enable_Ignore();
    get_lorawanhw_rx_payload(targetBuff);
    TEST_ASSERT_FALSE(b_received_string_waiting);
}
void test_get_rx_payload_clearsStringWaitingFlag_coruptMsg(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    b_received_string_waiting = true;
    sprintf((char *)rx_isr_buffer, "mmac_rx 1 01020304\r\n");
    lorawan_rx_enable_Ignore();
    TEST_ASSERT_FALSE(get_lorawanhw_rx_payload(targetBuff));
    TEST_ASSERT_FALSE(b_received_string_waiting);
}
void test_get_rx_payload_enablesRxIsr_validMsg(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    sprintf((char *)rx_isr_buffer, "mac_rx 1 01020304\r\n");
    lorawan_rx_enable_Expect();
    get_lorawanhw_rx_payload(targetBuff);
}
void test_get_rx_payload_enablesRxIsr_corruptMsg(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    sprintf((char *)rx_isr_buffer, "mmac_rx 1 01020304\r\n");
    lorawan_rx_enable_Expect();
    TEST_ASSERT_FALSE(get_lorawanhw_rx_payload(targetBuff));
}
void test_get_rx_payload_enablesRxIsr_missingSpaceChar(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    sprintf((char *)rx_isr_buffer, "mac_rx1 01020304\r\n");
    lorawan_rx_enable_Expect();
    TEST_ASSERT_FALSE(get_lorawanhw_rx_payload(targetBuff));
}
void test_get_rx_payload_nullTargetPointer(){
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(get_lorawanhw_rx_payload(NULL));
}
void test_get_rx_payload_emptyPayload(){
_DEBUG_OUT_TEST_NAME
    char targetBuff[200] = {0};
    sprintf((char *)rx_isr_buffer, "mac_rx 1 \r\n");
    lorawan_rx_enable_Ignore();
    get_lorawanhw_rx_payload(targetBuff);
    TEST_ASSERT_EQUAL_STRING("\r\n", targetBuff);
}
//-----------------------------------------------------------------
void test_get_rx_payload_waiting(){
_DEBUG_OUT_TEST_NAME
    b_received_string_waiting = true;
    TEST_ASSERT_TRUE(get_lorawanhw_rx_string_waiting());
    b_received_string_waiting = false;
    TEST_ASSERT_FALSE(get_lorawanhw_rx_string_waiting());
}
void test_clear_rx_payload_waiting(){
_DEBUG_OUT_TEST_NAME
    b_received_string_waiting = true;
    lorawan_rx_enable_Expect();
    clear_lorawanhw_rx_string_waiting();
    TEST_ASSERT_FALSE(b_received_string_waiting);
}
void test_set_rx_payload_waiting(){
_DEBUG_OUT_TEST_NAME
    b_received_string_waiting = true;
    lorawan_rx_enable_Expect();
    clear_lorawanhw_rx_string_waiting();
    TEST_ASSERT_FALSE(b_received_string_waiting);
}
//-----------------------------------------------------------------
void test_tx_isr(){
_DEBUG_OUT_TEST_NAME
    sprintf((char *)tx_isr_buffer,"mac tx cnf 1 0102\r\n");
    n_char_transmitted = 0;

    lorawan_get_int_status_IgnoreAndReturn(UART_STAT_TXRDY);
    lorawan_was_rx_isr_IgnoreAndReturn(false);
    lorawan_was_tx_isr_IgnoreAndReturn(true);

    lorawan_tx_byte_Expect('m'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('a'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('c'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect(' '); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('t'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('x'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect(' '); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('c'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('n'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('f'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect(' '); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('1'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect(' '); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('0'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('1'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('0'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('2'); 
    lorawanhw_trx_isr();


    lorawan_tx_byte_Expect('\r'); 
    lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('\n'); 
    lorawan_tx_disable_Expect();
    lorawan_rx_enable_Expect();
    lorawanhw_trx_isr();
}
//-----------------------------------------------------------------
void test_tx_isr_incrementsCharSent_maxPayload(){
_DEBUG_OUT_TEST_NAME
    strncpy((char *)tx_isr_buffer, TEST_STRING_50BYTES, UP_MSG_MICROCHIP_STRING_MAX);
    n_char_transmitted = 0;
    uint8_t x = UP_MSG_MICROCHIP_STRING_MAX;

    lorawan_get_int_status_IgnoreAndReturn(UART_STAT_TXRDY);
    lorawan_was_rx_isr_IgnoreAndReturn(false);
    lorawan_was_tx_isr_IgnoreAndReturn(true);
    lorawan_tx_byte_Ignore(); 

    while(x-- > 1) lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('\n'); 
    lorawan_tx_disable_Expect();
    lorawan_rx_enable_Expect();
    lorawanhw_trx_isr();
    
}
//-----------------------------------------------------------------
void test_tx_isr_noTerminationCharBeforeMaxPayload(){
_DEBUG_OUT_TEST_NAME
    strncpy((char *)tx_isr_buffer, TEST_STRING_NO_TERM_CHAR, UP_MSG_MICROCHIP_STRING_MAX);
    n_char_transmitted = 0;
    uint8_t x = UP_MSG_MICROCHIP_STRING_MAX;

    lorawan_get_int_status_IgnoreAndReturn(UART_STAT_TXRDY);
    lorawan_was_rx_isr_IgnoreAndReturn(false);
    lorawan_was_tx_isr_IgnoreAndReturn(true);
    lorawan_tx_byte_Ignore(); 

    while(x-- > 1) lorawanhw_trx_isr();

    lorawan_tx_byte_Expect('0'); 
    lorawan_tx_disable_Expect();
    lorawan_rx_enable_Expect();
    lorawanhw_trx_isr();
    
}
//-----------------------------------------------------------------
void test_rx_isr(){
_DEBUG_OUT_TEST_NAME
    b_received_string_waiting = false;
    n_char_received = 0;
    lorawan_get_int_status_IgnoreAndReturn(UART_STAT_RXRDY);
    lorawan_was_rx_isr_IgnoreAndReturn(true);
    lorawan_was_tx_isr_IgnoreAndReturn(false);

    lorawan_rx_byte_ExpectAndReturn('1');
    lorawanhw_trx_isr();
    lorawan_rx_byte_ExpectAndReturn('2');
    lorawanhw_trx_isr();
    lorawan_rx_byte_ExpectAndReturn('3');
    lorawanhw_trx_isr();
    lorawan_rx_byte_ExpectAndReturn('\r');
    lorawanhw_trx_isr();
    lorawan_rx_byte_ExpectAndReturn('\n');
    lorawan_rx_disable_Expect();
    lorawanhw_trx_isr();
    TEST_ASSERT_TRUE(b_received_string_waiting);
}
void test_rx_isr_maxPayload(){
_DEBUG_OUT_TEST_NAME
    uint8_t x = DOWN_MSG_MICROCHIP_STRING_MAX;
    b_received_string_waiting = false;
    n_char_received = 0;
    lorawan_get_int_status_IgnoreAndReturn(UART_STAT_RXRDY);
    lorawan_was_rx_isr_IgnoreAndReturn(true);
    lorawan_was_tx_isr_IgnoreAndReturn(false);
    do
    {
        lorawan_rx_byte_ExpectAndReturn('1');
        lorawanhw_trx_isr();
    }while(--x > 2);
    lorawan_rx_byte_ExpectAndReturn('\r');
    lorawanhw_trx_isr();
    lorawan_rx_byte_ExpectAndReturn('\n');
    lorawan_rx_disable_Expect();
    lorawanhw_trx_isr();
}
void test_rx_isr_disableRxIsrWhenMsgTooLong(){
_DEBUG_OUT_TEST_NAME
    uint8_t x = DOWN_MSG_MICROCHIP_STRING_MAX;
    b_received_string_waiting = false;
    n_char_received = 0;
    lorawan_get_int_status_IgnoreAndReturn(UART_STAT_RXRDY);
    lorawan_was_rx_isr_IgnoreAndReturn(true);
    lorawan_was_tx_isr_IgnoreAndReturn(false);
    do
    {
        lorawan_rx_byte_ExpectAndReturn('1');
        lorawanhw_trx_isr();
    }while(--x > 0);
    lorawan_rx_byte_ExpectAndReturn('2');
    lorawan_rx_disable_Expect();
    lorawanhw_trx_isr();
    TEST_ASSERT_TRUE(b_received_string_waiting);
    TEST_ASSERT_EQUAL_STRING("", rx_isr_buffer);
}
//-----------------------------------------------------------------