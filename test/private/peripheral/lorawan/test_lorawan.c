#include "unity.h"
#include "lorawan.c"

#include "mock_config_lorawan.h"
#include "mock_lorawanmac.h"
#include "mock_lorawanhw.h"
#include "mock_lorawaninit.h"
#include "mock_common.h"
#include "mock_systick.h"
#include "mock_debug.h"
#include "mock_sysmon.h"
#include "mock_FreeRTOS.h"
#include "mock_task.h"
#include "mock_queue.h"
#include "mock_timers.h"
#include "mock_FreeRTOSMacros.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_LorawanStatusFlags.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

#define TEST_STRING_50BYTES "0011223344556677889900112233445566778899001122334455667788990011223344556677889900112233445566778899"
#define TEST_STRING_51BYTES "00112233445566778899001122334455667788990011223344556677889900112233445566778899001122334455667788990"
bool allowSessionRecovery = true;
uint32_t runTime = 0;

void setUp(void)
{
    debugprint_Ignore();

    join_dr = 5;
    pre_join_dr = 0;
    attempts_at_current_dr = 1;  
    time_of_prev_join_request = (uint32_t)0xFFFF;  

    runTime = 0;
}
void tearDown(void)
{

}

//-----------------------------------------------------------------
void test_init_okWithSessionRestore(){
_DEBUG_OUT_TEST_NAME
    allowSessionRecovery = true;
    lorawaninit_exe_ExpectAndReturn(&allowSessionRecovery, true);
    TEST_ASSERT_TRUE(lorawan_init(&allowSessionRecovery));
}
void test_init_failWithSessionRestore(){
_DEBUG_OUT_TEST_NAME
    allowSessionRecovery = true;
    lorawaninit_exe_ExpectAndReturn(&allowSessionRecovery, false);
    TEST_ASSERT_FALSE(lorawan_init(&allowSessionRecovery));
}
void test_init_okWithoutSessionRestore(){
_DEBUG_OUT_TEST_NAME
    allowSessionRecovery = false;
    lorawaninit_exe_ExpectAndReturn(&allowSessionRecovery, true);
    TEST_ASSERT_TRUE(lorawan_init(&allowSessionRecovery));
}
void test_init_failWithoutSessionRestore(){
_DEBUG_OUT_TEST_NAME
    allowSessionRecovery = false;
    lorawaninit_exe_ExpectAndReturn(&allowSessionRecovery, false);
    TEST_ASSERT_FALSE(lorawan_init(&allowSessionRecovery));
}
//-----------------------------------------------------------------
static void mockFailedAttemptsAtDataRate(uint8_t dr)
{
    char str[20];
    sprintf(str, "mac set dr %d\r\n", dr);
    get_systick_sec_IgnoreAndReturn(runTime);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn(str, true);
    lorawanmac_send_cmd_ExpectAnyArgsAndReturn(true);
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(lorawan_join_network(false));
    runTime += INTER_JOIN_REQ_DELAY_SEC + 1;
    get_systick_sec_IgnoreAndReturn(runTime);
    lorawanmac_send_cmd_ExpectAnyArgsAndReturn(true);
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(lorawan_join_network(false));
    runTime += INTER_JOIN_REQ_DELAY_SEC + 1;
    get_systick_sec_IgnoreAndReturn(runTime);
    lorawanmac_send_cmd_ExpectAnyArgsAndReturn(true);
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(lorawan_join_network(false));
    runTime += INTER_JOIN_REQ_DELAY_SEC + 1;
}
void test_join_okFirstTimeRound(){
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(10);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set dr 5\r\n", true);
    lorawanmac_send_cmd_ExpectAndReturn("mac join otaa\r\n", true);
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(true);
    lorawanmac_send_cmd_ExpectAndReturn("mac join abp\r\n", true);
    TEST_ASSERT_TRUE(lorawan_join_network(false));
}
void test_join_okIteratesThroughDataRatesBeforeJoining(){
_DEBUG_OUT_TEST_NAME
    runTime = INTER_JOIN_REQ_DELAY_SEC + 1;
    mockFailedAttemptsAtDataRate(5);
    mockFailedAttemptsAtDataRate(4);
    mockFailedAttemptsAtDataRate(3);
    mockFailedAttemptsAtDataRate(2);
    mockFailedAttemptsAtDataRate(1);
    mockFailedAttemptsAtDataRate(0);
    mockFailedAttemptsAtDataRate(5);
    mockFailedAttemptsAtDataRate(4);

    runTime += INTER_JOIN_REQ_DELAY_SEC + 1;
    get_systick_sec_IgnoreAndReturn(runTime);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set dr 3\r\n", true);
    lorawanmac_send_cmd_ExpectAndReturn("mac join otaa\r\n", true);
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(true);
    lorawanmac_send_cmd_ExpectAndReturn("mac join abp\r\n", true);
    TEST_ASSERT_TRUE(lorawan_join_network(false));
}

void test_join_doesNotSendRequestBeforeMinimumDelay()
{
_DEBUG_OUT_TEST_NAME
    get_systick_sec_IgnoreAndReturn(0);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set dr 5\r\n", true);
    lorawanmac_send_cmd_ExpectAndReturn("mac join otaa\r\n", false);
    lorawan_join_network(false);
    get_systick_sec_IgnoreAndReturn(1);
    TEST_ASSERT_FALSE(lorawan_join_network(false));
    get_systick_sec_IgnoreAndReturn(INTER_JOIN_REQ_DELAY_SEC + 2);
    lorawanmac_send_cmd_ExpectAndReturn("mac join otaa\r\n", true);
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(true);
    lorawanmac_send_cmd_ExpectAndReturn("mac join abp\r\n", true);
    TEST_ASSERT_TRUE(lorawan_join_network(false));
}

void test_join_restoringSession(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(true);
    lorawanmac_send_cmd_ExpectAndReturn("mac join abp\r\n", true);
    TEST_ASSERT_TRUE(lorawan_join_network(true));
}
//-----------------------------------------------------------------
static void mockPostUplinkNoMacCmd(){
    FreeRTOSDelay_Ignore();
    lorawanmac_send_query_IgnoreAndReturn(true);
    lz_char_to_uint32_ascii_IgnoreAndReturn(0);
}

void test_send_msg_confirmedOk(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_ExpectAndReturn("mac tx cnf 1 MSG\r\n", true);
    mockPostUplinkNoMacCmd();
    TEST_ASSERT_TRUE(lorawan_send_msg("MSG", true));
}
void test_send_msg_confirmedFailed(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_ExpectAnyArgsAndReturn(false);
    TEST_ASSERT_FALSE(lorawan_send_msg("MSG", true));
}
void test_send_msg_unconfirmedOk(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_ExpectAndReturn("mac tx uncnf 1 MSG\r\n", true);
    mockPostUplinkNoMacCmd();
    TEST_ASSERT_TRUE(lorawan_send_msg("MSG", false));
}
void test_send_msg_unconfirmedFailed(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_ExpectAnyArgsAndReturn(false);
    TEST_ASSERT_FALSE(lorawan_send_msg("MSG", false));
}
void test_send_msg_nullPointer(){
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(lorawan_send_msg(NULL, false));
}
void test_send_msg_emptyMsg(){
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(lorawan_send_msg("", false));
}
void test_send_msg_tooLongMsg(){
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(lorawan_send_msg(TEST_STRING_51BYTES, false));
}
void test_send_msg_maxPayloadLength(){
_DEBUG_OUT_TEST_NAME
    char buf[200];
    sprintf(buf, "mac tx cnf 1 %s\r\n",TEST_STRING_50BYTES);
    lorawanmac_send_cmd_ExpectAndReturn(buf, true);
    mockPostUplinkNoMacCmd();
    TEST_ASSERT_TRUE(lorawan_send_msg(TEST_STRING_50BYTES, true));
}

void test_send_msg_macStatusIsNull(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_IgnoreAndReturn(true);
    FreeRTOSDelay_Ignore();
    lorawanmac_send_query_IgnoreAndReturn(true);
    lz_char_to_uint32_ascii_IgnoreAndReturn(0);
    TEST_ASSERT_TRUE(lorawan_send_msg("MSG", true));
}
void test_send_msg_macStatusLinkAdrReqPwr(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_ExpectAndReturn("mac tx cnf 1 MSG\r\n", true);
    FreeRTOSDelay_Ignore();
    lorawanmac_send_query_IgnoreAndReturn(true);
    lz_char_to_uint32_ascii_IgnoreAndReturn(LINK_ADR_REQ_PWR);
    lorawanmac_send_cmd_ExpectAndReturn("mac tx uncnf 3 00000000\r\n", true);
    TEST_ASSERT_TRUE(lorawan_send_msg("MSG", true));
}
void test_send_msg_macStatusLinkAdrReqNBRep(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_ExpectAndReturn("mac tx cnf 1 MSG\r\n", true);
    FreeRTOSDelay_Ignore();
    lorawanmac_send_query_IgnoreAndReturn(true);
    lz_char_to_uint32_ascii_IgnoreAndReturn(LINK_ADR_REQ_NDREP);
    lorawanmac_send_cmd_ExpectAndReturn("mac tx uncnf 3 00000000\r\n", true);
    TEST_ASSERT_TRUE(lorawan_send_msg("MSG", true));
}
void test_send_msg_macStatusNewChannelReq(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_ExpectAndReturn("mac tx cnf 1 MSG\r\n", true);
    FreeRTOSDelay_Ignore();
    lorawanmac_send_query_IgnoreAndReturn(true);
    lz_char_to_uint32_ascii_IgnoreAndReturn(NEW_CHANNEL_REQ);
    lorawanmac_send_cmd_ExpectAndReturn("mac tx uncnf 3 00000000\r\n", true);
    TEST_ASSERT_TRUE(lorawan_send_msg("MSG", true));
}
void test_send_msg_macStatusCorrectSequence(){
_DEBUG_OUT_TEST_NAME
    char respBuf[UP_MSG_MICROCHIP_STRING_MAX]={0};
    lorawanmac_send_cmd_ExpectAndReturn("mac tx cnf 1 MSG\r\n", true);
    FreeRTOSDelay_Expect(3000);
    lorawanmac_send_query_ExpectAnyArgsAndReturn(true);
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("0102FF04\r\n", 10);
    lz_char_to_uint32_ascii_ExpectAndReturn("0102FF04\r\n 1 MSG\r\n", 0x0102FF04);
    FreeRTOSDelay_Expect(1000);
    lorawanmac_send_cmd_ExpectAndReturn("mac tx uncnf 3 00000000\r\n", true);
    TEST_ASSERT_TRUE(lorawan_send_msg("MSG", true));
}
void test_send_msg_MacStatusFailedIgnored(){
_DEBUG_OUT_TEST_NAME
    char respBuf[UP_MSG_MICROCHIP_STRING_MAX]={0};
    lorawanmac_send_cmd_ExpectAndReturn("mac tx cnf 1 MSG\r\n", true);
    FreeRTOSDelay_Expect(3000);
    lorawanmac_send_query_ExpectAnyArgsAndReturn(true);
    lorawanmac_send_query_ReturnArrayThruPtr_p_res("0102FF04\r\n", 10);
    lz_char_to_uint32_ascii_ExpectAndReturn("0102FF04\r\n 1 MSG\r\n", 0x0102FF04);
    FreeRTOSDelay_Expect(1000);
    lorawanmac_send_cmd_ExpectAndReturn("mac tx uncnf 3 00000000\r\n", false);
    TEST_ASSERT_TRUE(lorawan_send_msg("MSG", true));
}
//-----------------------------------------------------------------
void test_update_mcast_state_StartSessionOk(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set mcastdnctr 0\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set mcast on\r\n", true);
    TEST_ASSERT_TRUE(lorawan_update_mcast_state(true));  
}
void test_update_mcast_state_StartSessionFailedResetCounter(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set mcastdnctr 0\r\n", false);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set mcast on\r\n", true);
    TEST_ASSERT_TRUE(lorawan_update_mcast_state(true));   
}
void test_update_mcast_state_StartSessionFailedActivateSession(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set mcastdnctr 0\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set mcast on\r\n", false);
    sysmon_report_error_Expect(LORA, MCAST_SESSION_CHANGE);
    TEST_ASSERT_FALSE(lorawan_update_mcast_state(true));  
}

void test_update_mcast_state_EndSessionOk(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set mcast off\r\n", true);
    TEST_ASSERT_TRUE(lorawan_update_mcast_state(false));  
}
void test_update_mcast_state_EndSessionFailedDisableSession(){
_DEBUG_OUT_TEST_NAME
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set mcast off\r\n", false);
    sysmon_report_error_Expect(LORA, MCAST_SESSION_CHANGE);
    TEST_ASSERT_FALSE(lorawan_update_mcast_state(false));  
}
//-----------------------------------------------------------------
void test_save_session_ok(){
    lorawanmac_send_cmd_ok_only_slow_ExpectAndReturn("mac save\r\n", true);
    lorawan_save_session_info();
}
void test_save_session_failHasNoImpact(){
    lorawanmac_send_cmd_ok_only_slow_ExpectAndReturn("mac save\r\n", false);
    lorawan_save_session_info();
}
//-----------------------------------------------------------------
void test_optimise_settings_canBeOptimised1(){
_DEBUG_OUT_TEST_NAME
    get_lorawanmac_dr_ExpectAndReturn(3);
    get_lorawanmac_pwridx_ExpectAndReturn(1);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set pwridx 1\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set dr 0\r\n", true);
    TEST_ASSERT_TRUE(lorawan_optimise_settings());
}
void test_optimise_settings_canBeOptimised2(){
_DEBUG_OUT_TEST_NAME
    get_lorawanmac_dr_ExpectAndReturn(0);
    get_lorawanmac_pwridx_ExpectAndReturn(3);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set pwridx 1\r\n", true);
    lorawanmac_send_cmd_ok_only_ExpectAndReturn("mac set dr 0\r\n", true);
    TEST_ASSERT_TRUE(lorawan_optimise_settings());
}
void test_optimise_settings_alreadyOptimum(){
_DEBUG_OUT_TEST_NAME
    get_lorawanmac_dr_ExpectAndReturn(0);
    get_lorawanmac_pwridx_ExpectAndReturn(1);
    TEST_ASSERT_FALSE(lorawan_optimise_settings());
}
//-----------------------------------------------------------------
void test_get_lorawan_dev_eui()
{
_DEBUG_OUT_TEST_NAME
    char DevEUi[17]={0};
    get_lorawaninit_hweui_Expect(DevEUi);
    get_lorawan_dev_eui(DevEUi);    
}
