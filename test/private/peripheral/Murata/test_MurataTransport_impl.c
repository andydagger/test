#include "unity.h"
#include "MurataTransport_impl.c"
#include "string.h"
#include "mock_debug.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);


void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

/*******************************************************************************
    murataResponseCode decodeReceivedString(char * p_receivedString);
*******************************************************************************/

void test_decodeReceivedString_ExitEarlyOnNullPointer()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( PTR_ERR, decodeReceivedString(NULL));
}

void test_decodeReceivedString_EmptyBuffer()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( NO_RESP, decodeReceivedString(""));
}

void test_decodeReceivedString_Ok()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( RESP_OK, decodeReceivedString("+OK\r"));
}

void test_decodeReceivedString_CommandUnknown()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( INVALID_CMD, decodeReceivedString("+ERR=-1\r"));
}

void test_decodeReceivedString_NumberParametersInvalid()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( INVALID_CMD, decodeReceivedString("+ERR=-2\r"));
}

void test_decodeReceivedString_ParameterContantInvalid()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( INVALID_CMD, decodeReceivedString("+ERR=-3\r"));
}

void test_decodeReceivedString_NotConnected()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( NOT_CONNECTED, decodeReceivedString("+ERR=-5\r"));
}

void test_decodeReceivedString_Busy()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( ISBUSY, decodeReceivedString("+ERR=-7\r"));
}

void test_decodeReceivedString_PayloadlengthExceedsmaximum()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( INVALID_CMD, decodeReceivedString("+ERR=-12\r"));
}

void test_decodeReceivedString_CommandOnlySupportedInAbpMode()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( INVALID_CMD, decodeReceivedString("+ERR=-13\r"));
}

void test_decodeReceivedString_CommandOnlySupportedInOtaamode()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( INVALID_CMD, decodeReceivedString("+ERR=-14\r"));
}

void test_decodeReceivedString_BandConfigurationNotSupported()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( INVALID_CMD, decodeReceivedString("+ERR=-15\r"));
}

void test_decodeReceivedString_PowerValueOutOfRange()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( INVALID_CMD, decodeReceivedString("+ERR=-16\r"));
}

void test_decodeReceivedString_CommandUnusableUnderCurrentBand()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( INVALID_CMD, decodeReceivedString("+ERR=-17\r"));
}

void test_decodeReceivedString_NoDutyCycleAvailable()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( NO_DUTY_CYCLE, decodeReceivedString("+ERR=-18\r"));
}

void test_decodeReceivedString_LbTDutyCycleRestriction()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( NO_DUTY_CYCLE, decodeReceivedString("+ERR=-19\r"));
}

void test_decodeReceivedString_RebootOk()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( REBOOT_OK, decodeReceivedString("+EVENT=0,0\r"));
}

void test_decodeReceivedString_ResetOk()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( RESET_OK, decodeReceivedString("+EVENT=0,1\r"));
}

void test_decodeReceivedString_JoinReject()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( JOIN_REJECTED, decodeReceivedString("+EVENT=1,0\r"));
}

void test_decodeReceivedString_JoinAccept()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( JOIN_ACCEPT, decodeReceivedString("+EVENT=1,1\r"));
}

void test_decodeReceivedString_ConfirmedUplinkRetry()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( CNF_RETRY, decodeReceivedString("+EVENT=2,2\r"));
}

void test_decodeReceivedString_ConfirmedAcked()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( ACK, decodeReceivedString("+ACK\r"));
}

void test_decodeReceivedString_ConfirmedNotAcked()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8( NOACK, decodeReceivedString("+NOACK\r"));
}

void test_decodeReceivedString_ReceivedDownlink()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_EQUAL_UINT8( RECV_MSG, decodeReceivedString("+RECV="));
}

void test_decodeReceivedString_ReceivedCorruptString1()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_EQUAL_UINT8( CORRUPT_RESP, decodeReceivedString("ERR=-19"));
}

void test_decodeReceivedString_ReceivedCorruptString2()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_EQUAL_UINT8( CORRUPT_RESP, decodeReceivedString("*+ERR=-19"));
}

/*******************************************************************************
    bool extractReceivedDownlinkPayload(char * p_receivedString, 
                                        char * p_downlinkPayload);
*******************************************************************************/

void test_extractReceivedDownlinkPayload_LongPayloadCopiedAcross()
{
_DEBUG_OUT_TEST_NAME
    char callerBuffer[DOWN_MSG_MICROCHIP_STRING_MAX] = {0};
    extractReceivedDownlinkPayload("+RECV=2,12\r\n\r\nDOWNLINK_MSG", callerBuffer);
    TEST_ASSERT_EQUAL_STRING("DOWNLINK_MSG", callerBuffer);
}

void test_extractReceivedDownlinkPayload_ShortPayloadCopiedAcross()
{
_DEBUG_OUT_TEST_NAME
    char callerBuffer[DOWN_MSG_MICROCHIP_STRING_MAX] = {0};
    extractReceivedDownlinkPayload("+RECV=2,4\r\n\r\nDOWN", callerBuffer);
    TEST_ASSERT_EQUAL_STRING("DOWN", callerBuffer);
}

void test_extractReceivedDownlinkPayload_RejectPayloadGreaterThan51Bytes()
{
_DEBUG_OUT_TEST_NAME
    char callerBuffer[DOWN_MSG_MICROCHIP_STRING_MAX] = {0};
    TEST_ASSERT_FALSE(extractReceivedDownlinkPayload("+RECV=2,52\r\n\r\nDOWN", callerBuffer));
    TEST_ASSERT_EQUAL_STRING("", callerBuffer);
}

void test_extractReceivedDownlinkPayload_FailNoComma()
{
_DEBUG_OUT_TEST_NAME
    char callerBuffer[DOWN_MSG_MICROCHIP_STRING_MAX] = {0};
    TEST_ASSERT_FALSE(extractReceivedDownlinkPayload("+RECV=2/52\r\n\r\nDOWN", callerBuffer));
    TEST_ASSERT_EQUAL_STRING("", callerBuffer);
}

void test_extractReceivedDownlinkPayload_FailOnNullSourcePointer()
{
_DEBUG_OUT_TEST_NAME
    char callerBuffer[DOWN_MSG_MICROCHIP_STRING_MAX] = {0};
    TEST_ASSERT_FALSE(extractReceivedDownlinkPayload(NULL, callerBuffer));
    TEST_ASSERT_EQUAL_STRING("", callerBuffer);
}

void test_extractReceivedDownlinkPayload_FailOnNullTargetPointer()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(extractReceivedDownlinkPayload("+RECV=2,52\r\n\r\nDOWN", NULL));
}


