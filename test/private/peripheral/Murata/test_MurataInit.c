#include "unity.h"
#include "stdbool.h"
#include "MurataInit.c"
#include "mock_MurataInit_impl.h"
#include "mock_debug.h"
#include "mock_common.h"
#include "mock_config_lorawan.h"
#include "mock_config_network.h"
#include "mock_productionMonitor.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);
#define ALLOW_SESSION_RECOVERY true
#define FORCE_SESSION_RESET false

bool requestToRecoverSession = ALLOW_SESSION_RECOVERY;

void setUp(void)
{
    debugprint_Ignore();
    devEuiReadyForReading = false;
}
void tearDown(void)
{

}

/*******************************************************************************
   bool MurataInit_Run(uint8_t freqband);
*******************************************************************************/
void test_MurataInit_Run_CallsInCorrectOrder868()
{
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = FORCE_SESSION_RESET;
    resetModule_Expect();
    activateComs_Expect();
    softReboot_ExpectAndReturn(true);
    fetchModuleCurrentFreqBand_ExpectAnyArgsAndReturn(true);
    fetchUpFrameCounter_ExpectAndReturn(5);
    updateModuleFrequencyBand_ExpectAnyArgsAndReturn(true);
    waitForModuleToBeReady_ExpectAndReturn(true);
    getFwVersion_ExpectAnyArgsAndReturn(true);
    parseFwVersion_ExpectAnyArgsAndReturn(true);
    readBackDevEui_ExpectAnyArgs();
    productionMonitor_getUnitIsUndertest_ExpectAndReturn(false);
    setupModule_ExpectAnyArgsAndReturn(true);

    TEST_ASSERT_TRUE( MurataInit_Run(_EU868, &requestToRecoverSession) );
}
void test_MurataInit_Run_CallsInCorrectOrder868_notUndertest()
{
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = FORCE_SESSION_RESET;
    resetModule_Expect();
    activateComs_Expect();
    softReboot_ExpectAndReturn(true);
    fetchModuleCurrentFreqBand_ExpectAnyArgsAndReturn(true);
    fetchUpFrameCounter_ExpectAndReturn(5);
    updateModuleFrequencyBand_ExpectAnyArgsAndReturn(true);
    waitForModuleToBeReady_ExpectAndReturn(true);
    getFwVersion_ExpectAnyArgsAndReturn(true);
    parseFwVersion_ExpectAnyArgsAndReturn(true);
    readBackDevEui_ExpectAnyArgs();
    productionMonitor_getUnitIsUndertest_ExpectAndReturn(false);
    setupModule_ExpectAndReturn(moduleInitSetupCommandList, SETUP_LIST_ITEMS, true);

    TEST_ASSERT_TRUE( MurataInit_Run(_EU868, &requestToRecoverSession) );
}
void test_MurataInit_Run_CallsInCorrectOrder868_whenUndertest()
{
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = FORCE_SESSION_RESET;
    resetModule_Expect();
    activateComs_Expect();
    softReboot_ExpectAndReturn(true);
    fetchModuleCurrentFreqBand_ExpectAnyArgsAndReturn(true);
    fetchUpFrameCounter_ExpectAndReturn(5);
    updateModuleFrequencyBand_ExpectAnyArgsAndReturn(true);
    waitForModuleToBeReady_ExpectAndReturn(true);
    getFwVersion_ExpectAnyArgsAndReturn(true);
    parseFwVersion_ExpectAnyArgsAndReturn(true);
    readBackDevEui_ExpectAnyArgs();
    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);
    setupModule_ExpectAndReturn(moduleInitSetupCommandListUnderTest, SETUP_LIST_ITEMS, true);

    TEST_ASSERT_TRUE( MurataInit_Run(_EU868, &requestToRecoverSession) );
}
void test_MurataInit_Run_SetsDevEuiFlagWhenAvailable()
{
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = FORCE_SESSION_RESET;
    resetModule_Expect();
    activateComs_Expect();
    softReboot_ExpectAndReturn(true);
    fetchModuleCurrentFreqBand_ExpectAnyArgsAndReturn(true);
    fetchUpFrameCounter_ExpectAndReturn(5);
    updateModuleFrequencyBand_ExpectAnyArgsAndReturn(true);
    waitForModuleToBeReady_ExpectAndReturn(true);
    getFwVersion_ExpectAnyArgsAndReturn(true);
    parseFwVersion_ExpectAnyArgsAndReturn(true);
    readBackDevEui_ExpectAnyArgs();
    productionMonitor_getUnitIsUndertest_ExpectAndReturn(false);
    setupModule_ExpectAnyArgsAndReturn(true);

    MurataInit_Run(_EU868, &requestToRecoverSession);
    TEST_ASSERT_TRUE(devEuiReadyForReading);
}

void test_MurataInit_Run_CallsInCorrectOrder923()
{
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = FORCE_SESSION_RESET;
    resetModule_Expect();
    activateComs_Expect();
    softReboot_ExpectAndReturn(true);
    fetchModuleCurrentFreqBand_ExpectAnyArgsAndReturn(true);
    fetchUpFrameCounter_ExpectAndReturn(5);
    updateModuleFrequencyBand_ExpectAnyArgsAndReturn(true);
    waitForModuleToBeReady_ExpectAndReturn(true);
    getFwVersion_ExpectAnyArgsAndReturn(true);
    parseFwVersion_ExpectAnyArgsAndReturn(true);
    applyDwellTime_ExpectAndReturn(true);
    readBackDevEui_ExpectAnyArgs();
    productionMonitor_getUnitIsUndertest_ExpectAndReturn(false);
    setupModule_ExpectAnyArgsAndReturn(true);

    TEST_ASSERT_TRUE( MurataInit_Run(_AS923, &requestToRecoverSession) );
}

void test_MurataInit_Run_ChangeFrequencyBand()
{
_DEBUG_OUT_TEST_NAME
    uint8_t currentFreqBand = _EU868;
    requestToRecoverSession = FORCE_SESSION_RESET;
    resetModule_Ignore();
    activateComs_Ignore();
    softReboot_IgnoreAndReturn(true);
    fetchModuleCurrentFreqBand_ExpectAnyArgsAndReturn(true);
    fetchUpFrameCounter_ExpectAndReturn(5);
    fetchModuleCurrentFreqBand_ReturnThruPtr_currentFreqBand(&currentFreqBand);
    updateModuleFrequencyBand_ExpectAndReturn(_EU868, _AS923, true);
    waitForModuleToBeReady_IgnoreAndReturn(true);
    getFwVersion_IgnoreAndReturn(true);
    parseFwVersion_IgnoreAndReturn(true);
    applyDwellTime_IgnoreAndReturn(true);
    readBackDevEui_Ignore();
    productionMonitor_getUnitIsUndertest_IgnoreAndReturn(false);
    setupModule_IgnoreAndReturn(true);

    TEST_ASSERT_TRUE( MurataInit_Run(_AS923, &requestToRecoverSession) );
}

void test_MurataInit_Run_ReturnFalseOnFailure_softReboot()
{
_DEBUG_OUT_TEST_NAME
    uint8_t currentFreqBand = _EU868;
    requestToRecoverSession = FORCE_SESSION_RESET;
    resetModule_Ignore();
    activateComs_Ignore();
    softReboot_IgnoreAndReturn(false);

    TEST_ASSERT_FALSE( MurataInit_Run(_AS923, &requestToRecoverSession) );
}

void test_MurataInit_Run_ReturnFalseOnFailure_updateFrequency()
{
_DEBUG_OUT_TEST_NAME
    uint8_t currentFreqBand = _EU868;
    resetModule_Ignore();
    activateComs_Ignore();
    softReboot_IgnoreAndReturn(false);
    fetchModuleCurrentFreqBand_IgnoreAndReturn(true);
    fetchUpFrameCounter_IgnoreAndReturn(5);
    updateModuleFrequencyBand_IgnoreAndReturn(false);

    TEST_ASSERT_FALSE( MurataInit_Run(_AS923, &requestToRecoverSession) );
}


void test_MurataInit_Run_ReturnFalseOnFailure_applyDwellTime()
{
_DEBUG_OUT_TEST_NAME
    uint8_t currentFreqBand = _EU868;
    requestToRecoverSession = FORCE_SESSION_RESET;
    resetModule_Ignore();
    activateComs_Ignore();
    softReboot_IgnoreAndReturn(false);
    fetchModuleCurrentFreqBand_IgnoreAndReturn(true);
    fetchUpFrameCounter_IgnoreAndReturn(5);
    updateModuleFrequencyBand_IgnoreAndReturn(true);
    waitForModuleToBeReady_IgnoreAndReturn(true);
    getFwVersion_IgnoreAndReturn(true);
    parseFwVersion_IgnoreAndReturn(true);
    applyDwellTime_IgnoreAndReturn(false);

    TEST_ASSERT_FALSE( MurataInit_Run(_AS923, &requestToRecoverSession) );
}


void test_MurataInit_Run_ReturnFalseOnFailure_setupModule()
{
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = FORCE_SESSION_RESET;
    resetModule_Ignore();
    activateComs_Ignore();
    softReboot_IgnoreAndReturn(false);
    fetchModuleCurrentFreqBand_IgnoreAndReturn(true);
    fetchUpFrameCounter_IgnoreAndReturn(5);
    updateModuleFrequencyBand_IgnoreAndReturn(true);
    waitForModuleToBeReady_IgnoreAndReturn(true);
    getFwVersion_IgnoreAndReturn(true);
    parseFwVersion_IgnoreAndReturn(true);
    applyDwellTime_IgnoreAndReturn(true);
    readBackDevEui_Ignore();
    productionMonitor_getUnitIsUndertest_IgnoreAndReturn(false);
    setupModule_IgnoreAndReturn(false);

    TEST_ASSERT_FALSE( MurataInit_Run(_EU868, &requestToRecoverSession) );
}

void test_MurataInit_Run_RestoreSession()
{
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    resetModule_Ignore();
    activateComs_Ignore();
    softReboot_IgnoreAndReturn(true);
    fetchModuleCurrentFreqBand_ExpectAnyArgsAndReturn(true);
    fetchUpFrameCounter_ExpectAndReturn(LORAWAN_RESTORE_SESSION_UPLINK_COUNT_THRESHOLD + 1);
    TEST_ASSERT_TRUE( MurataInit_Run(_EU868, &requestToRecoverSession) );
    TEST_ASSERT_TRUE(requestToRecoverSession);
}

void test_MurataInit_Run_ForceSessionReset()
{
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = FORCE_SESSION_RESET;
    resetModule_Ignore();
    activateComs_Ignore();
    softReboot_IgnoreAndReturn(true);
    fetchModuleCurrentFreqBand_ExpectAnyArgsAndReturn(true);
    fetchUpFrameCounter_ExpectAndReturn(LORAWAN_RESTORE_SESSION_UPLINK_COUNT_THRESHOLD + 1);
    updateModuleFrequencyBand_IgnoreAndReturn(true);
    waitForModuleToBeReady_IgnoreAndReturn(true);
    getFwVersion_IgnoreAndReturn(true);
    parseFwVersion_IgnoreAndReturn(true);
    applyDwellTime_IgnoreAndReturn(true);
    readBackDevEui_Ignore();
    productionMonitor_getUnitIsUndertest_IgnoreAndReturn(false);
    setupModule_IgnoreAndReturn(true);
    TEST_ASSERT_TRUE( MurataInit_Run(_EU868, &requestToRecoverSession) );
    TEST_ASSERT_FALSE(requestToRecoverSession);
}

void test_MurataInit_Run_LowFrameCounterTriggersReset()
{
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    resetModule_Ignore();
    activateComs_Ignore();
    softReboot_IgnoreAndReturn(true);
    fetchModuleCurrentFreqBand_ExpectAnyArgsAndReturn(true);
    fetchUpFrameCounter_ExpectAndReturn(LORAWAN_RESTORE_SESSION_UPLINK_COUNT_THRESHOLD - 1);
    updateModuleFrequencyBand_IgnoreAndReturn(true);
    waitForModuleToBeReady_IgnoreAndReturn(true);
    getFwVersion_IgnoreAndReturn(true);
    parseFwVersion_IgnoreAndReturn(true);
    applyDwellTime_IgnoreAndReturn(true);
    readBackDevEui_Ignore();
    productionMonitor_getUnitIsUndertest_IgnoreAndReturn(false);
    setupModule_IgnoreAndReturn(true);
    TEST_ASSERT_TRUE( MurataInit_Run(_EU868, &requestToRecoverSession) );
    TEST_ASSERT_FALSE(requestToRecoverSession);
}
//---------------------------------------------------------------------------

void test_get_hweui_notYetAvailable()
{
_DEBUG_OUT_TEST_NAME
    devEuiReadyForReading = false;
    char DevEUi[17]={0};
    MurataInit_getDevEUi(DevEUi);
    TEST_ASSERT_EQUAL_STRING("", DevEUi);
}

void test_get_hweui_ready()
{
_DEBUG_OUT_TEST_NAME
    devEuiReadyForReading = true;
    char DevEUi[17]={0};
    sprintf(hwEui, "1122334455667788");
    MurataInit_getDevEUi(DevEUi);
    TEST_ASSERT_EQUAL_STRING("1122334455667788", DevEUi);
}