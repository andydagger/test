#include "unity.h"
#include "stdbool.h"
#include "MurataInit_impl.c"
#include "mock_Murata.h"
#include "mock_MurataTransport.h"
#include "mock_loraDriver.h"
#include "mock_FreeRTOSCommonHooks.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

static setupCommandString lorawaninit_setup_cmdlist[] ={
        { "AT+APPKEY=", "01020304050607080102030405060708" },
        { "AT+SLEEP=", "0" }, // Sleep Mode Disabled
        { "AT+DFORMAT=", "1" }, // Payload format is hex
        { "AT+MODE=", "1" }, // OTAA
        { "AT+CLASS=", "2" } // CLASS C
};

static uint8_t numberOfSettings = 5;


void setUp(void)
{

}
void tearDown(void)
{

}

/*******************************************************************************
    bool getFwVersion(char * respString)
*******************************************************************************/
void test_getFwversion_ParseResponseString()
{
_DEBUG_OUT_TEST_NAME
    char fwResp[15];
    MurataTransport_sendCommand_ExpectAndReturn("AT+VER?\r", false, fwResp, FW_VERSION_STRING_LENGTH, CMD_SUCCESSFUL);
    getFwVersion(fwResp);
}

void test_getFwversion_ReturnsTrueIfQuerySucceeded()
{
_DEBUG_OUT_TEST_NAME
    char fwResp[15];
    MurataTransport_sendCommand_IgnoreAndReturn(CMD_SUCCESSFUL);
    TEST_ASSERT_TRUE(getFwVersion(fwResp));
}

void test_getFwversion_ReturnsFalseIfQueryFailed()
{
_DEBUG_OUT_TEST_NAME
    char fwResp[15];
    MurataTransport_sendCommand_IgnoreAndReturn(CMD_FAILED);
    TEST_ASSERT_FALSE(getFwVersion(fwResp));
}
/*******************************************************************************
    uint16_t parseFwVersion(char * respString)
*******************************************************************************/

void test_parseFwVersion_CorrectVersion1()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_EQUAL_UINT16(2819, parseFwVersion("1.1.03,Dec 20 2018 15:00:56"));
}

void test_parseFwVersion_CorrectVersion2()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_EQUAL_UINT16(17187, parseFwVersion("6.7.35 Dec 20 2058"));
}

void test_parseFwVersion_ReturnNullIfFirstCharCorrupt()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_EQUAL_UINT16(0, parseFwVersion("A.7.35 Dec 20 2058"));
}

void test_parseFwVersion_ReturnNullIfSecondCharCorrupt()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_EQUAL_UINT16(0, parseFwVersion("1.X.35 Dec 20 2058"));
}

void test_parseFwVersion_ReturnNullIfThirdCharCorrupt()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_EQUAL_UINT16(0, parseFwVersion("1.7..5 Dec 20 2058"));
}

/*******************************************************************************
    bool setupModule(char setupCmdList[][2], uint8_t numberOfSettings)
*******************************************************************************/

void test_setupModule_ReturnsTrueWhenDone()
{
_DEBUG_OUT_TEST_NAME
    MurataTransport_sendCommand_ExpectAndReturn("AT+APPKEY=01020304050607080102030405060708\r", false, NULL, 0, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ExpectAndReturn("AT+SLEEP=0\r", false,NULL, 0, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ExpectAndReturn("AT+DFORMAT=1\r", false, NULL, 0, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ExpectAndReturn("AT+MODE=1\r", false, NULL, 0, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ExpectAndReturn("AT+CLASS=2\r", false, NULL, 0, CMD_SUCCESSFUL);
    TEST_ASSERT_TRUE(setupModule(lorawaninit_setup_cmdlist, numberOfSettings));
}

void test_setupModule_ReturnsFalseIfCmdFails()
{
_DEBUG_OUT_TEST_NAME
    MurataTransport_sendCommand_ExpectAndReturn("AT+APPKEY=01020304050607080102030405060708\r", false, NULL, 0, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ExpectAndReturn("AT+SLEEP=0\r", false,NULL, 0, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ExpectAndReturn("AT+DFORMAT=1\r", false, NULL, 0, CMD_FAILED);
    TEST_ASSERT_FALSE(setupModule(lorawaninit_setup_cmdlist, numberOfSettings));
}

void test_setupModule_ReturnsFalseIfResponseUnexpected()
{
_DEBUG_OUT_TEST_NAME
    MurataTransport_sendCommand_ExpectAndReturn("AT+APPKEY=01020304050607080102030405060708\r", false, NULL, 0, LOST_CONNECTION);
    TEST_ASSERT_FALSE(setupModule(lorawaninit_setup_cmdlist, numberOfSettings));
}

/*******************************************************************************
    bool softReboot();
*******************************************************************************/
void test_softReboot_SingleShotOk()
{
_DEBUG_OUT_TEST_NAME
    MurataTransport_sendCommand_ExpectAndReturn("AT+REBOOT\r", true, NULL, 0, CMD_SUCCESSFUL);
    TEST_ASSERT_TRUE(softReboot());
}

void test_softReboot_WithRetries()
{
_DEBUG_OUT_TEST_NAME
    MurataTransport_sendCommand_ExpectAndReturn("AT+REBOOT\r", true, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(RESET_RETRY_DELAY_MS);
    MurataTransport_sendCommand_ExpectAndReturn("AT+REBOOT\r", true, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(RESET_RETRY_DELAY_MS);
    MurataTransport_sendCommand_ExpectAndReturn("AT+REBOOT\r", true, NULL, 0, CMD_SUCCESSFUL);
     TEST_ASSERT_TRUE(softReboot());
}

void test_softReset_RetryUpToLimit()
{
_DEBUG_OUT_TEST_NAME
    uint8_t testRun = RESET_RETRIES_LIMIT;
    while(testRun-- > 0)
    {
        MurataTransport_sendCommand_ExpectAndReturn("AT+REBOOT\r", true, NULL, 0, CMD_FAILED);
        FreeRTOSDelay_Expect(RESET_RETRY_DELAY_MS);
    }
    TEST_ASSERT_FALSE(softReboot());
}

/*******************************************************************************
    uint8_t fetchModuleCurrentFreqBand();
*******************************************************************************/

void test_fetchModuleCurrentFreqBand_ParseEU868()
{
_DEBUG_OUT_TEST_NAME
    char bandResp[15];
    uint8_t currentFreqBand = 0;
    sprintf(bandResp, "%d", _EU868);
    MurataTransport_sendCommand_ExpectAndReturn("AT+BAND?\r", false, bandResp, 1, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_IgnoreArg_p_responseData();
    MurataTransport_sendCommand_ReturnThruPtr_p_responseData(&bandResp[0]);
    fetchModuleCurrentFreqBand(&currentFreqBand);
    TEST_ASSERT_EQUAL_UINT8(_EU868, currentFreqBand);
}

void test_fetchModuleCurrentFreqBand_ParseAS923()
{
_DEBUG_OUT_TEST_NAME
    char bandResp[15];
    uint8_t currentFreqBand = 0;
    sprintf(bandResp, "%d", _AS923);
    MurataTransport_sendCommand_ExpectAndReturn("AT+BAND?\r", false, bandResp, 1, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_IgnoreArg_p_responseData();
    MurataTransport_sendCommand_ReturnThruPtr_p_responseData(&bandResp[0]);
    fetchModuleCurrentFreqBand(&currentFreqBand);
    TEST_ASSERT_EQUAL_UINT8(_AS923, currentFreqBand);
}

void test_fetchModuleCurrentFreqBand_ReturnsTrueIfQuerySucceeded()
{
_DEBUG_OUT_TEST_NAME
    uint8_t currentFreqBand = 0;
    MurataTransport_sendCommand_IgnoreAndReturn(CMD_SUCCESSFUL);
    TEST_ASSERT_TRUE(fetchModuleCurrentFreqBand(&currentFreqBand));
}

void test_fetchModuleCurrentFreqBand_ReturnsFalseIfQueryFailed()
{
_DEBUG_OUT_TEST_NAME
    uint8_t currentFreqBand = 0;
    MurataTransport_sendCommand_IgnoreAndReturn(CMD_FAILED);
    TEST_ASSERT_FALSE(fetchModuleCurrentFreqBand(&currentFreqBand));
}


/*******************************************************************************
    bool updateModuleFrequencyBand(uint8_t currentFreqBand, uint8_t requiredFreqband);
*******************************************************************************/

void test_updateModuleFrequencyBand_RejectCurrentBndOutOfBound()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(updateModuleFrequencyBand((_US915H + 1), _AS923));
}

void test_updateModuleFrequencyBand_RejectRequiredBndOutOfBound()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(updateModuleFrequencyBand(_EU868, (_US915H + 1)));
}

void test_updateModuleFrequencyBand_SendCmdWithNewBandIfChangeRequired()
{
_DEBUG_OUT_TEST_NAME
    char bandChangeCmd[15];
    sprintf(bandChangeCmd, "AT+BAND=%d\r", _AS923 );
    MurataTransport_sendCommand_ExpectAndReturn(bandChangeCmd, true, NULL, 0, CMD_SUCCESSFUL);
    TEST_ASSERT_TRUE(updateModuleFrequencyBand(_EU868, _AS923));
}

void test_updateModuleFrequencyBand_ReturnsFalseIfBandChangeFailed()
{
_DEBUG_OUT_TEST_NAME
    char bandChangeCmd[15];
    sprintf(bandChangeCmd, "AT+BAND=%d\r", _AS923 );
    MurataTransport_sendCommand_ExpectAndReturn(bandChangeCmd, true, NULL, 0, CMD_FAILED);
    TEST_ASSERT_FALSE(updateModuleFrequencyBand(_EU868, _AS923));
}

/*******************************************************************************
    bool waitForModuleToBeReady();
*******************************************************************************/
void test_waitForModuleToBeReady_SingleShotOk()
{
_DEBUG_OUT_TEST_NAME
    MurataTransport_sendCommand_ExpectAndReturn("AT\r", false, NULL, 0, CMD_SUCCESSFUL);
    TEST_ASSERT_TRUE(waitForModuleToBeReady());
}

void test_waitForModuleToBeReady_WithRetries()
{
_DEBUG_OUT_TEST_NAME
    MurataTransport_sendCommand_ExpectAndReturn("AT\r", false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(READY_RETRY_DELAY_MS);
    MurataTransport_sendCommand_ExpectAndReturn("AT\r", false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(READY_RETRY_DELAY_MS);
    MurataTransport_sendCommand_ExpectAndReturn("AT\r", false, NULL, 0, CMD_SUCCESSFUL);
     TEST_ASSERT_TRUE(waitForModuleToBeReady());
}

void test_waitForModuleToBeReady_RetryUpToLimit()
{
_DEBUG_OUT_TEST_NAME
    uint8_t testRun = READY_RETRIES_LIMIT;
    while(testRun-- > 0)
    {
        MurataTransport_sendCommand_ExpectAndReturn("AT\r", false, NULL, 0, CMD_FAILED);
        FreeRTOSDelay_Expect(READY_RETRY_DELAY_MS);    
    }
    TEST_ASSERT_FALSE(waitForModuleToBeReady());
}

/*******************************************************************************
    bool applyDwellTime();
*******************************************************************************/
void test_applyDwellTime_SingleShotOk()
{
_DEBUG_OUT_TEST_NAME
    MurataTransport_sendCommand_ExpectAndReturn("AT+DWELL=1,1\r", false, NULL, 0, CMD_SUCCESSFUL);
    TEST_ASSERT_TRUE(applyDwellTime());
}

void test_applyDwellTime_WithRetries()
{
_DEBUG_OUT_TEST_NAME
    MurataTransport_sendCommand_ExpectAndReturn("AT+DWELL=1,1\r", false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(DWELL_RETRY_DELAY_MS);
    MurataTransport_sendCommand_ExpectAndReturn("AT+DWELL=1,1\r", false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(DWELL_RETRY_DELAY_MS);
    MurataTransport_sendCommand_ExpectAndReturn("AT+DWELL=1,1\r", false, NULL, 0, CMD_SUCCESSFUL);
     TEST_ASSERT_TRUE(applyDwellTime());
}

void test_applyDwellTime_RetryUpToLimit()
{
_DEBUG_OUT_TEST_NAME
    uint8_t testRun = DWELL_RETRIES_LIMIT;
    while(testRun-- > 0)
    {
        MurataTransport_sendCommand_ExpectAndReturn("AT+DWELL=1,1\r", false, NULL, 0, CMD_FAILED);
        FreeRTOSDelay_Expect(DWELL_RETRY_DELAY_MS);    
    }
    TEST_ASSERT_FALSE(applyDwellTime());
}

/*******************************************************************************
    uint32_t fetchUpFrameCounter();
*******************************************************************************/
void test_ffetchUpFrameCounters_Fail()
{
_DEBUG_OUT_TEST_NAME
    char counterResp[22]={0};
    uint32_t upCounter = 0;
    MurataTransport_sendCommand_ExpectAndReturn("AT+FRMCNT?\r", false, counterResp, 21, CMD_FAILED);
    TEST_ASSERT_EQUAL_UINT32(0, fetchUpFrameCounter());
}

void test_fetchUpFrameCounters_valuesA()
{
_DEBUG_OUT_TEST_NAME
    char counterResp[22]={0};
    uint32_t upCounter = 0;
    MurataTransport_sendCommand_ExpectAndReturn("AT+FRMCNT?\r", false, counterResp, 21, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ReturnArrayThruPtr_p_responseData("3,1", 3);
    TEST_ASSERT_EQUAL_UINT32(3, fetchUpFrameCounter());
}

void test_fetchUpFrameCounters_valuesB()
{
_DEBUG_OUT_TEST_NAME
    char counterResp[22]={0};
    uint32_t upCounter = 0;
    MurataTransport_sendCommand_ExpectAndReturn("AT+FRMCNT?\r", false, counterResp, 21, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ReturnArrayThruPtr_p_responseData("852,21", 6);
    TEST_ASSERT_EQUAL_UINT32(852, fetchUpFrameCounter());
}


void test_fetchUpFrameCounters_valuesMax()
{
_DEBUG_OUT_TEST_NAME
    char counterResp[22]={0};
    uint32_t upCounter = 0;
    MurataTransport_sendCommand_ExpectAndReturn("AT+FRMCNT?\r", false, counterResp, 21, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ReturnArrayThruPtr_p_responseData("4294967295,56325", 16);
    TEST_ASSERT_EQUAL_UINT32(4294967295, fetchUpFrameCounter());
}

void test_fetchUpFrameCounters_valuesNull()
{
_DEBUG_OUT_TEST_NAME
    char counterResp[22]={0};
    uint32_t upCounter = 0;
    MurataTransport_sendCommand_ExpectAndReturn("AT+FRMCNT?\r", false, counterResp, 21, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ReturnArrayThruPtr_p_responseData("0,0", 3);
    TEST_ASSERT_EQUAL_UINT32(0, fetchUpFrameCounter());
}