#include "unity.h"
#include "MurataTransport.c"
#include "mock_MurataTransport_impl.h"
#include "mock_MurataTransport_stateMachine.h"
#include "mock_MurataDriver.h"
#include "mock_systick.h"
#include "string.h"
#include "mock_debug.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

char respBuffer[DOWN_MSG_MICROCHIP_STRING_MAX];
char cmdBuffer[DOWN_MSG_MICROCHIP_STRING_MAX];

void setUp(void)
{
    uint8_t i = 0;
    for(i = 0 ; i < (DOWN_MSG_MICROCHIP_STRING_MAX - 1); i++){
        respBuffer[i] = 0;
        cmdBuffer[i] = 0;
    }
    debugprint_Ignore();
}

void tearDown(void)
{

}

/*******************************************************************************
cmdResult MurataTransport_sendCommand(   char * p_command, 
                                    bool secondResponseIsExpected,
                                    char * p_responseData,
                                    uint8_t respLengthBytes );;
*******************************************************************************/

void test_MurataTransport_sendCommand_RejectCommandWithNullPointer()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_EQUAL_UINT8( CMD_FAILED, MurataTransport_sendCommand(NULL, false, respBuffer, 0));
}

void test_MurataTransport_sendCommand_TimeOut()
{
_DEBUG_OUT_TEST_NAME
    MurataDriver_getFullStringReceived_IgnoreAndReturn(true);
    serviceMurataTransportStateMachine_IgnoreAndReturn(TL_IDLE);
    MurataDriver_getReceivedStringBuffer_IgnoreAndReturn(respBuffer);
    decodeReceivedString_IgnoreAndReturn(RESP_OK);
    get_systick_ms_IgnoreAndReturn(0);
    checkForOverAllSendOpTimeOut_IgnoreAndReturn(false);

    serviceStateMachineTimeOut_IgnoreAndReturn(true);
    MurataDriver_resetAndEnableDownlinkReceiver_Ignore();
    TEST_ASSERT_EQUAL_UINT8( CMD_FAILED, MurataTransport_sendCommand(cmdBuffer, true, respBuffer, 0));

}

void test_MurataTransport_moduleNotResponding()
{
_DEBUG_OUT_TEST_NAME
    MurataDriver_getFullStringReceived_IgnoreAndReturn(false);
    serviceMurataTransportStateMachine_IgnoreAndReturn(TL_WAIT_FOR_INITIAL_RESP);
    serviceStateMachineTimeOut_IgnoreAndReturn(false);
    get_systick_ms_IgnoreAndReturn(0);

    checkForOverAllSendOpTimeOut_ExpectAnyArgsAndReturn(false);
    checkForOverAllSendOpTimeOut_ExpectAnyArgsAndReturn(false);
    checkForOverAllSendOpTimeOut_ExpectAnyArgsAndReturn(false);
    checkForOverAllSendOpTimeOut_ExpectAnyArgsAndReturn(true);
    
    MurataDriver_resetAndEnableDownlinkReceiver_Ignore();
    TEST_ASSERT_EQUAL_UINT8( MOD_NOT_RESP, MurataTransport_sendCommand(cmdBuffer, true, respBuffer, 0));

}


void test_MurataTransport_FailedTransferReturnsFalse()
{
_DEBUG_OUT_TEST_NAME

    cmdResult cmdRes = CMD_FAILED;
    MurataDriver_getFullStringReceived_IgnoreAndReturn(true);
    MurataDriver_getReceivedStringBuffer_IgnoreAndReturn(respBuffer);
    decodeReceivedString_IgnoreAndReturn(RESP_OK);
    get_systick_ms_IgnoreAndReturn(0);
    checkForOverAllSendOpTimeOut_IgnoreAndReturn(false);

    serviceMurataTransportStateMachine_ExpectAnyArgsAndReturn(TL_IDLE);
    serviceMurataTransportStateMachine_ReturnThruPtr_p_cmdSuccess(&cmdRes);
    serviceStateMachineTimeOut_IgnoreAndReturn(false);
    MurataDriver_resetAndEnableDownlinkReceiver_Ignore();
    TEST_ASSERT_EQUAL_UINT8( CMD_FAILED, MurataTransport_sendCommand(cmdBuffer, true, NULL, 0));
}

void test_MurataTransport_TransferReturnsTrueNoRespExpected()
{
_DEBUG_OUT_TEST_NAME

    cmdResult cmdRes = CMD_SUCCESSFUL;
    MurataDriver_getFullStringReceived_IgnoreAndReturn(true);
    MurataDriver_getReceivedStringBuffer_IgnoreAndReturn(respBuffer);
    decodeReceivedString_IgnoreAndReturn(RESP_OK);
    get_systick_ms_IgnoreAndReturn(0);
    checkForOverAllSendOpTimeOut_IgnoreAndReturn(false);

    serviceMurataTransportStateMachine_ExpectAnyArgsAndReturn(TL_IDLE);
    serviceMurataTransportStateMachine_ReturnThruPtr_p_cmdSuccess(&cmdRes);
    serviceStateMachineTimeOut_IgnoreAndReturn(false);
    MurataDriver_resetAndEnableDownlinkReceiver_Ignore();
    TEST_ASSERT_EQUAL_UINT8( CMD_SUCCESSFUL, MurataTransport_sendCommand(cmdBuffer, true, NULL, 0));
}

void test_MurataTransport_TransferReturnsTrueFromGetCommand()
{
_DEBUG_OUT_TEST_NAME

    cmdResult cmdRes = CMD_SUCCESSFUL;
    sprintf(respBuffer, "+OK=1234\r\n\r\n");
    char buf[100]={0};
    MurataDriver_getFullStringReceived_IgnoreAndReturn(true);   
    MurataDriver_getReceivedStringBuffer_IgnoreAndReturn(respBuffer);
    decodeReceivedString_IgnoreAndReturn(RESP_OK);
    get_systick_ms_IgnoreAndReturn(0);
    checkForOverAllSendOpTimeOut_IgnoreAndReturn(false);

    serviceMurataTransportStateMachine_ExpectAnyArgsAndReturn(TL_WAIT_FOR_SECOND_RESP);
    serviceMurataTransportStateMachine_ExpectAnyArgsAndReturn(TL_IDLE);
    serviceMurataTransportStateMachine_ReturnThruPtr_p_cmdSuccess(&cmdRes);
    serviceStateMachineTimeOut_IgnoreAndReturn(false);
    MurataDriver_resetAndEnableDownlinkReceiver_Ignore();
    TEST_ASSERT_EQUAL_UINT8( CMD_SUCCESSFUL, MurataTransport_sendCommand(cmdBuffer, true, buf, 4));
    TEST_ASSERT_EQUAL_STRING( "1234", buf);
}

void test_MurataTransport_TransferReturnsFalseIfExpectedLengthIsTooLong()
{
_DEBUG_OUT_TEST_NAME

    char buf[100]={0};
    TEST_ASSERT_EQUAL_UINT8( CMD_FAILED, MurataTransport_sendCommand(cmdBuffer, true, buf, DOWN_MSG_PAYLOAD + 1));
}


void test_MurataTransport_TransferReturnsPoorLink()
{
_DEBUG_OUT_TEST_NAME

    cmdResult cmdRes = POOR_LINK;
    MurataDriver_getFullStringReceived_IgnoreAndReturn(true);
    MurataDriver_getReceivedStringBuffer_IgnoreAndReturn(respBuffer);
    decodeReceivedString_IgnoreAndReturn(RESP_OK);
    get_systick_ms_IgnoreAndReturn(0);
    checkForOverAllSendOpTimeOut_IgnoreAndReturn(false);

    serviceMurataTransportStateMachine_ExpectAnyArgsAndReturn(TL_IDLE);
    serviceMurataTransportStateMachine_ReturnThruPtr_p_cmdSuccess(&cmdRes);
    serviceStateMachineTimeOut_IgnoreAndReturn(false);
    MurataDriver_resetAndEnableDownlinkReceiver_Ignore();
    TEST_ASSERT_EQUAL_UINT8( POOR_LINK, MurataTransport_sendCommand(cmdBuffer, true, NULL, 0));
}

void test_MurataTransport_TransferReturnsLostConnection()
{
_DEBUG_OUT_TEST_NAME

    cmdResult cmdRes = LOST_CONNECTION;
    MurataDriver_getFullStringReceived_IgnoreAndReturn(true);
    MurataDriver_getReceivedStringBuffer_IgnoreAndReturn(respBuffer);
    decodeReceivedString_IgnoreAndReturn(RESP_OK);
    get_systick_ms_IgnoreAndReturn(0);
    checkForOverAllSendOpTimeOut_IgnoreAndReturn(false);

    serviceMurataTransportStateMachine_ExpectAnyArgsAndReturn(TL_IDLE);
    serviceMurataTransportStateMachine_ReturnThruPtr_p_cmdSuccess(&cmdRes);
    serviceStateMachineTimeOut_IgnoreAndReturn(false);
    MurataDriver_resetAndEnableDownlinkReceiver_Ignore();
    TEST_ASSERT_EQUAL_UINT8( LOST_CONNECTION, MurataTransport_sendCommand(cmdBuffer, true, NULL, 0));
}

void test_MurataTransport_TransferReturnsTrueNoRespExpected_CallsInCorrectOrder()
{
_DEBUG_OUT_TEST_NAME

    cmdResult cmdRes = CMD_SUCCESSFUL;
    get_systick_ms_ExpectAndReturn(0);
    MurataDriver_getFullStringReceived_ExpectAndReturn(true);
    MurataDriver_getReceivedStringBuffer_ExpectAndReturn(respBuffer);
    decodeReceivedString_ExpectAnyArgsAndReturn(RESP_OK);
    checkForOverAllSendOpTimeOut_IgnoreAndReturn(false);

    serviceMurataTransportStateMachine_ExpectAnyArgsAndReturn(TL_IDLE);
    serviceMurataTransportStateMachine_ReturnThruPtr_p_cmdSuccess(&cmdRes);
    serviceStateMachineTimeOut_ExpectAnyArgsAndReturn(false);
    MurataDriver_resetAndEnableDownlinkReceiver_Expect();
    TEST_ASSERT_EQUAL_UINT8( CMD_SUCCESSFUL, MurataTransport_sendCommand(cmdBuffer, true, NULL, 0));
}

/*******************************************************************************
bool MurataTransport_getDownlinkPayload(char * callerBuffer);
*******************************************************************************/

void test_MurataTransport_getDownlinkPayload_ReturnsFalseIfDecodingFailed()
{
_DEBUG_OUT_TEST_NAME

    MurataDriver_getReceivedStringBuffer_IgnoreAndReturn(respBuffer);
    extractReceivedDownlinkPayload_IgnoreAndReturn(false);
    MurataDriver_resetAndEnableDownlinkReceiver_Ignore();
    TEST_ASSERT_FALSE(MurataTransport_getDownlinkPayload(cmdBuffer));
}

void test_MurataTransport_getDownlinkPayload_ReturnsTrueIfDecodingOk()
{
_DEBUG_OUT_TEST_NAME

    MurataDriver_getReceivedStringBuffer_IgnoreAndReturn(respBuffer);
    extractReceivedDownlinkPayload_IgnoreAndReturn(true);
    MurataDriver_resetAndEnableDownlinkReceiver_Ignore();
    TEST_ASSERT_TRUE(MurataTransport_getDownlinkPayload(cmdBuffer));
}
