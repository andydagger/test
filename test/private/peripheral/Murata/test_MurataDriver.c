#include "unity.h"
#include "MurataDriver.c"
#include "mock_loraDriver.h"
#include "string.h"
#include "mock_debug.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

char commonTestBuffer[UP_MSG_MICROCHIP_STRING_MAX] = {0};

void setUp(void)
{
    sprintf(commonTestBuffer, "TEST_STRING\r\n");
    eraseTrxBuffer();
    debugprint_Ignore();
    fullStringReceived = false;
}

void tearDown(void)
{

}

static void Mock_resetAndEnableDownlinkReceiver()
{
    loraDriver_RxByte_IgnoreAndReturn('X');
    loraDriver_RxEnable_Ignore();    
}

/*******************************************************************************
    bool MurataDriver_getFullStringReceived();
    void MurataDriver_checkForStringEnd();
*******************************************************************************/
void test_getFullStringReceived_InitialisedAsFalse()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE( MurataDriver_getFullStringReceived() );
}

void test_getFullStringReceived_ReturnFalseIfNoCharacterReceived()
{
_DEBUG_OUT_TEST_NAME
    charactersReceived = 0;
    MurataDriver_checkForStringEnd();
    TEST_ASSERT_FALSE( MurataDriver_getFullStringReceived() );
}

void test_getFullStringReceived_ReturnFalseIfExtraCharacterReceived()
{
_DEBUG_OUT_TEST_NAME
    prevCharacterReceived = 3;
    charactersReceived = 4;
    MurataDriver_checkForStringEnd();
    TEST_ASSERT_FALSE( MurataDriver_getFullStringReceived() );
}

void test_getFullStringReceived_ReturnTrueIfNoCaractersReceivedSinceLastCall()
{
_DEBUG_OUT_TEST_NAME
    prevCharacterReceived = 3;
    charactersReceived = 3;
    loraDriver_RxDisable_Ignore();
    MurataDriver_checkForStringEnd();
    TEST_ASSERT_TRUE( MurataDriver_getFullStringReceived() );
}

void test_getFullStringReceived_DisablesRxIsrIfStringEndDetected()
{
_DEBUG_OUT_TEST_NAME
    prevCharacterReceived = 25;
    charactersReceived = 25;
    loraDriver_RxDisable_Expect();
    MurataDriver_checkForStringEnd();
    TEST_ASSERT_TRUE( MurataDriver_getFullStringReceived() );
}

void test_getFullStringReceived_UpdatesPreviousCharacterCount()
{
_DEBUG_OUT_TEST_NAME
    prevCharacterReceived = 3;
    charactersReceived = 4;
    MurataDriver_checkForStringEnd();
    MurataDriver_getFullStringReceived();
    TEST_ASSERT_EQUAL_UINT8(4, prevCharacterReceived);
}

void test_getFullStringReceived_InstantExitIfStringPending()
{
_DEBUG_OUT_TEST_NAME
    fullStringReceived = true;
    MurataDriver_checkForStringEnd();
}

/*******************************************************************************
    bool MurataDriver_sendString(char *p_msg);
*******************************************************************************/

void test_sendString_LengthOverLimit()
{
_DEBUG_OUT_TEST_NAME
    char testBuffer[UP_MSG_MICROCHIP_STRING_MAX + 2]={0};
    uint8_t i = 0;
    do
    {
        testBuffer[i++] = 'A';
    }while( i < (UP_MSG_MICROCHIP_STRING_MAX) );

    TEST_ASSERT_FALSE( MurataDriver_sendString(testBuffer) );    
}

void test_sendString_MsgCopiedIntoTrxBuffer()
{
_DEBUG_OUT_TEST_NAME
    loraDriver_RxDisable_Ignore();
    loraDriver_TxEnable_Ignore();
    MurataDriver_sendString((char *)"TEST_MSG_TO_SEND\r\n");
    TEST_ASSERT_EQUAL_STRING("TEST_MSG_TO_SEND\r\n",trxBuffer);
}

void test_sendString_TrxBufferErasedBeforeWrite()
{
_DEBUG_OUT_TEST_NAME
    loraDriver_RxDisable_Ignore();
    loraDriver_TxEnable_Ignore();
    MurataDriver_sendString((char *)"TEST_MSG_TO_SEND\r\n");
    MurataDriver_sendString((char *)"SHORTER_MSG\r\n");
    TEST_ASSERT_EQUAL_STRING("SHORTER_MSG\r\n",trxBuffer);
}

void test_sendString_RxIsrDisabledBeforeTxIsrEnabled()
{    
_DEBUG_OUT_TEST_NAME
    loraDriver_RxDisable_Expect();
    loraDriver_TxEnable_Expect();
    MurataDriver_sendString((char *)"TEST_MSG_TO_SEND\r\n");
}
void test_sendString_ClearscharactersReceived()
{
_DEBUG_OUT_TEST_NAME
    charactersReceived = 3;
    loraDriver_RxDisable_Ignore();
    loraDriver_TxEnable_Ignore();
    MurataDriver_sendString((char *)"TEST_MSG_TO_SEND\r\n");
    TEST_ASSERT_EQUAL_UINT8( 0, charactersReceived);
}
void test_sendString_ClearsprevCharacterReceived()
{
_DEBUG_OUT_TEST_NAME
    prevCharacterReceived = 3;
    loraDriver_RxDisable_Ignore();
    loraDriver_TxEnable_Ignore();
    MurataDriver_sendString((char *)"TEST_MSG_TO_SEND\r\n");
    TEST_ASSERT_EQUAL_UINT8( 0, prevCharacterReceived);
}
void test_sendString_ClearsfullStringReceived()
{
_DEBUG_OUT_TEST_NAME
    fullStringReceived = true;
    loraDriver_RxDisable_Ignore();
    loraDriver_TxEnable_Ignore();
    MurataDriver_sendString((char *)"TEST_MSG_TO_SEND\r\n");
    TEST_ASSERT_FALSE(fullStringReceived);
}

void test_sendString_ClearscharactersSentBeforeEnablingTxIsr()
{
_DEBUG_OUT_TEST_NAME
    charactersSent = 20;
    loraDriver_RxDisable_Ignore();
    loraDriver_TxEnable_Ignore();
    MurataDriver_sendString((char *)"TEST_MSG_TO_SEND\r\n");
    TEST_ASSERT_EQUAL_UINT8( 0, charactersSent);    
}

/*******************************************************************************
    void MurataDriver_TxIsr();
*******************************************************************************/

void test_txIsr_SendsCharatersInCorrectOrder()
{
_DEBUG_OUT_TEST_NAME
    sprintf( (char *)trxBuffer, "MSG_TO_SEND\r\n");
    uint8_t c = 0;
    do{
        loraDriver_TxByte_Expect(trxBuffer[c]);
        MurataDriver_txIsr();
    }while( c++ < 6);
}

void test_txIsr_StopsAtNullCharacter()
{
_DEBUG_OUT_TEST_NAME
    loraDriver_RxDisable_Ignore();
    loraDriver_TxEnable_Ignore();
    MurataDriver_sendString((char *)"TEST_MSG\r\n");
    uint8_t c = 0;
    do{
        loraDriver_TxByte_Expect(trxBuffer[c]);
        MurataDriver_txIsr();
    }while( ++c < 10);
    loraDriver_TxDisable_Expect();
    Mock_resetAndEnableDownlinkReceiver();
    MurataDriver_txIsr();
}

void test_txIsr_EnabledRxIsrAtNullCharacterAfterTxDisabled()
{
_DEBUG_OUT_TEST_NAME
    loraDriver_RxDisable_Ignore();
    loraDriver_TxEnable_Ignore();
    MurataDriver_sendString((char *)"TEST_MSG\r\n");
    uint8_t c = 0;
    do{
        loraDriver_TxByte_Expect(trxBuffer[c]);
        MurataDriver_txIsr();
    }while( ++c < 10);
    loraDriver_TxDisable_Expect();
    Mock_resetAndEnableDownlinkReceiver();
    MurataDriver_txIsr();
}


/*******************************************************************************
    void MurataDriver_rxIsr();
*******************************************************************************/

void test_rxIsr_LoadsReceivedCharactersInTrxBuffer()
{
_DEBUG_OUT_TEST_NAME

    charactersReceived = 0;
    uint8_t c = 0;
    do{
        loraDriver_RxByte_ExpectAndReturn(commonTestBuffer[c]);
        MurataDriver_rxIsr();
    }while( ++c < 11);
    TEST_ASSERT_EQUAL_STRING( "TEST_STRING", trxBuffer);
}

void test_rxIsr_RxIsrDisabledIfStringTooLong()
{
_DEBUG_OUT_TEST_NAME

    charactersReceived = DOWN_MSG_MICROCHIP_STRING_MAX - 2;
    
    loraDriver_RxByte_ExpectAndReturn('X');
    MurataDriver_rxIsr();

    loraDriver_RxByte_ExpectAndReturn('X');
    loraDriver_RxDisable_Expect();
    MurataDriver_rxIsr();
}


/*******************************************************************************
    void MurataDriver_resetAndEnableDownlinkReceiver();
*******************************************************************************/

void test_resetAndEnableDownlinkReceiver_EnableRxIsr()
{
_DEBUG_OUT_TEST_NAME
    loraDriver_RxByte_ExpectAndReturn('X');
    loraDriver_RxEnable_Expect();  
    MurataDriver_resetAndEnableDownlinkReceiver();
}

void test_resetAndEnableDownlinkReceiver_ClearscharactersReceived()
{
_DEBUG_OUT_TEST_NAME
    charactersReceived = 3;
    Mock_resetAndEnableDownlinkReceiver();
    MurataDriver_resetAndEnableDownlinkReceiver();
    TEST_ASSERT_EQUAL_UINT8( 0, charactersReceived);
}
void test_resetAndEnableDownlinkReceiver_ClearsprevCharacterReceived()
{
_DEBUG_OUT_TEST_NAME
    prevCharacterReceived = 3;
    Mock_resetAndEnableDownlinkReceiver();
    MurataDriver_resetAndEnableDownlinkReceiver();
    TEST_ASSERT_EQUAL_UINT8( 0, prevCharacterReceived);
}
void test_resetAndEnableDownlinkReceiver_ClearsfullStringReceived()
{
_DEBUG_OUT_TEST_NAME
    fullStringReceived = true;
    Mock_resetAndEnableDownlinkReceiver();
    MurataDriver_resetAndEnableDownlinkReceiver();
    TEST_ASSERT_FALSE(fullStringReceived);
}
void test_resetAndEnableDownlinkReceiver_ClearsTrxBuffer()
{
_DEBUG_OUT_TEST_NAME
    uint8_t i;
    char c = 0;
    sprintf((char *)trxBuffer, "SOME_TEXT\r\n");
    Mock_resetAndEnableDownlinkReceiver();
    MurataDriver_resetAndEnableDownlinkReceiver();
    for( i = 0; i < (sizeof(trxBuffer)-1); i++)
    {
        c = c | trxBuffer[i];
    }
    TEST_ASSERT_EQUAL_UINT8( 0, c);
}