#include "unity.h"
#include "Murata_impl.c"
#include "config_network.h"
#include "mock_MurataTransport.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

uint32_t time_of_prev_join_request = 0;
uint8_t join_dr = 5;
uint8_t attempts_at_current_dr = 0;

void setUp(void)
{
    time_of_prev_join_request = 0;
    join_dr = 5;
    attempts_at_current_dr = 0;

    prevJoinDR = 99;
}
void tearDown(void)
{

}

/*******************************************************************************
    void joinDutyCycleHandler(  uint8_t * joinDR, 
                                uint8_t * attemptsAtCurrentDr, 
                                uint8_t freqBand);
*******************************************************************************/

void test_joinDutyCycleHandler_AlwaysUseDR2ForAs923()
{
_DEBUG_OUT_TEST_NAME

    join_dr = 5;
    joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _AS923);
    TEST_ASSERT_EQUAL_UINT8( 2, join_dr);

}

void test_joinDutyCycleHandler_AlwaysUseDR2ForAs923EvenAfter5Calls()
{
_DEBUG_OUT_TEST_NAME

    attempts_at_current_dr = 5;
    join_dr = 5;
    joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _AS923);
    TEST_ASSERT_EQUAL_UINT8( 2, join_dr);

}

void test_joinDutyCycleHandler_StartWithDR5ForEu868()
{
_DEBUG_OUT_TEST_NAME

    join_dr = 0;
    joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _EU868);
    TEST_ASSERT_EQUAL_UINT8( 5, join_dr);
}

void test_joinDutyCycleHandler_ReturnsTrueAtFirstCall()
{
_DEBUG_OUT_TEST_NAME

    join_dr = 0;
    attempts_at_current_dr = 0;
    TEST_ASSERT_TRUE(joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _EU868));
}

void test_joinDutyCycleHandler_ReturnsFalseAtSecondCall()
{
_DEBUG_OUT_TEST_NAME

    prevJoinDR = 5;
    join_dr = 5;
    attempts_at_current_dr = 1;
    TEST_ASSERT_FALSE(joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _EU868));
}

void test_joinDutyCycleHandler_ReturnsTrueIfRequestLimitExceeded()
{
_DEBUG_OUT_TEST_NAME

    prevJoinDR = 5;
    join_dr = 5;
    attempts_at_current_dr = JOIN_REQ_AT_SAME_DR_LIMIT + 1;
    TEST_ASSERT_TRUE(joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _EU868));
}

void test_joinDutyCycleHandler_ReturnsFalseUpToRequestlimit()
{
_DEBUG_OUT_TEST_NAME

    prevJoinDR = 5;
    join_dr = 5;
    attempts_at_current_dr = JOIN_REQ_AT_SAME_DR_LIMIT;
    TEST_ASSERT_FALSE(joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _EU868));
}

void test_joinDutyCycleHandler_ResetAttemptsAtSameDRIfRequestLimitHit()
{
_DEBUG_OUT_TEST_NAME

    prevJoinDR = 5;
    join_dr = 5;
    attempts_at_current_dr = JOIN_REQ_AT_SAME_DR_LIMIT + 1;
    joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _EU868);
    TEST_ASSERT_EQUAL_UINT8( 1, attempts_at_current_dr);
}

void test_joinDutyCycleHandler_DropsTo4After5()
{
_DEBUG_OUT_TEST_NAME

    prevJoinDR = 5;
    join_dr = 5;
    attempts_at_current_dr = JOIN_REQ_AT_SAME_DR_LIMIT + 1;
    joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _EU868);
    TEST_ASSERT_EQUAL_UINT8( 4, join_dr);
}

void test_joinDutyCycleHandler_DropsTo3After4()
{
_DEBUG_OUT_TEST_NAME

    prevJoinDR = 4;
    join_dr = 4;
    attempts_at_current_dr = JOIN_REQ_AT_SAME_DR_LIMIT + 1;
    joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _EU868);
    TEST_ASSERT_EQUAL_UINT8( 3, join_dr);
}

void test_joinDutyCycleHandler_DropsTo2After3()
{
_DEBUG_OUT_TEST_NAME

    prevJoinDR = 3;
    join_dr = 3;
    attempts_at_current_dr = JOIN_REQ_AT_SAME_DR_LIMIT + 1;
    joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _EU868);
    TEST_ASSERT_EQUAL_UINT8( 2, join_dr);
}

void test_joinDutyCycleHandler_DropsTo1After2()
{
_DEBUG_OUT_TEST_NAME

    prevJoinDR = 2;
    join_dr = 2;
    attempts_at_current_dr = JOIN_REQ_AT_SAME_DR_LIMIT + 1;
    joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _EU868);
    TEST_ASSERT_EQUAL_UINT8( 1, join_dr);
}

void test_joinDutyCycleHandler_DropsTo0After1()
{
_DEBUG_OUT_TEST_NAME

    prevJoinDR = 1;
    join_dr = 1;
    attempts_at_current_dr = JOIN_REQ_AT_SAME_DR_LIMIT + 1;
    joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _EU868);
    TEST_ASSERT_EQUAL_UINT8( 0, join_dr);
}

void test_joinDutyCycleHandler_GoesBackUpTo5After0()
{
_DEBUG_OUT_TEST_NAME

    prevJoinDR = 0;
    join_dr = 0;
    attempts_at_current_dr = JOIN_REQ_AT_SAME_DR_LIMIT + 1;
    joinDutyCycleHandler( &join_dr, &attempts_at_current_dr, _EU868);
    TEST_ASSERT_EQUAL_UINT8( 5, join_dr);
}

/*******************************************************************************
     void updateDataRate(uint8_t newDataRate);
*******************************************************************************/
void test_updateDataRate_WithDR0()
{
_DEBUG_OUT_TEST_NAME
    MurataTransport_sendCommand_ExpectAndReturn("AT+DR=0\r", false, NULL, 0, CMD_SUCCESSFUL);
    updateDataRate(0);
}
void test_updateDataRate_WithDR5()
{
_DEBUG_OUT_TEST_NAME
    MurataTransport_sendCommand_ExpectAndReturn("AT+DR=5\r", false, NULL, 0, CMD_SUCCESSFUL);
    updateDataRate(5);
}

/*******************************************************************************
    uint8_t getDataRate(void);
*******************************************************************************/
void test_getDataRate_returns0()
{
_DEBUG_OUT_TEST_NAME
    char responseBuffer[10] = {0};
    MurataTransport_sendCommand_ExpectAndReturn("AT+DR?\r", false, responseBuffer, 1, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ReturnArrayThruPtr_p_responseData("0", 1);
    TEST_ASSERT_EQUAL_UINT8( 0, getDataRate());
}
void test_getDataRate_returns5()
{
_DEBUG_OUT_TEST_NAME
    char responseBuffer[10] = {0};
    MurataTransport_sendCommand_ExpectAndReturn("AT+DR?\r", false, responseBuffer, 1, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ReturnArrayThruPtr_p_responseData("5", 1);
    TEST_ASSERT_EQUAL_UINT8( 5, getDataRate());
}
void test_getDataRate_fails()
{
_DEBUG_OUT_TEST_NAME
    char responseBuffer[10] = {0};
    MurataTransport_sendCommand_ExpectAndReturn("AT+DR?\r", false, responseBuffer, 1, CMD_FAILED);
    TEST_ASSERT_EQUAL_UINT8( 99, getDataRate());
}

void test_getPowerIndex_returns1()
{
_DEBUG_OUT_TEST_NAME
    char responseBuffer[10] = {0};
    MurataTransport_sendCommand_ExpectAndReturn("AT+RFPOWER?\r", false, responseBuffer, 3, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ReturnArrayThruPtr_p_responseData("0,1", 3);
    TEST_ASSERT_EQUAL_UINT8( 1, getPowerIndex());
}
void test_getPowerIndex_returns7()
{
_DEBUG_OUT_TEST_NAME
    char responseBuffer[10] = {0};
    MurataTransport_sendCommand_ExpectAndReturn("AT+RFPOWER?\r", false, responseBuffer, 3, CMD_SUCCESSFUL);
    MurataTransport_sendCommand_ReturnArrayThruPtr_p_responseData("0,7", 3);
    TEST_ASSERT_EQUAL_UINT8( 7, getPowerIndex());
}
void test_getPowerIndex_fails()
{
_DEBUG_OUT_TEST_NAME
    char responseBuffer[10] = {0};
    MurataTransport_sendCommand_ExpectAndReturn("AT+RFPOWER?\r", false, responseBuffer, 3, CMD_FAILED);
    TEST_ASSERT_EQUAL_UINT8( 99, getPowerIndex());
}
/*******************************************************************************
    uint8_t getPowerIndex(void);
*******************************************************************************/


