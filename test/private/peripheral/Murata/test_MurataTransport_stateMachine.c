#include "unity.h"
#include "MurataTransport_stateMachine.c"
#include "mock_MurataDriver.h"
#include "string.h"
#include "config_network.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_debug.h"
#include "mock_systick.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

char cmdBuffer[UP_MSG_MICROCHIP_STRING_MAX];
cmdResult commandSuccess = CMD_FAILED;

void setUp(void)
{
    uint8_t i = 0;
    for(i = 0 ; i < (UP_MSG_MICROCHIP_STRING_MAX - 1); i++){
        cmdBuffer[i] = 0;
    }

    commandSuccess = CMD_FAILED;
    debugprint_Ignore();
    sendCmdAttempts = 0;
}

void tearDown(void)
{

}

/*******************************************************************************
    murataTransportStates serviceMurataTransportStateMachine(   murataTransportStates currentState,
                                                                murataResponseCode driverCode,
                                                                bool dualResponseExpected,
                                                                char * p_cmdString,
                                                                cmdResult * p_cmdSuccess);
*******************************************************************************/

void test_serviceMurataTransportStateMachine_IdleToSend()
{
_DEBUG_OUT_TEST_NAME
    sendCmdAttempts = 3;
    TEST_ASSERT_EQUAL_UINT8(TL_SEND, serviceMurataTransportStateMachine( TL_IDLE, NO_RESP, false, cmdBuffer, &commandSuccess));
    TEST_ASSERT_EQUAL_INT(0, sendCmdAttempts);
}

void test_serviceMurataTransportStateMachine_SendToInitialRespWait()
{
_DEBUG_OUT_TEST_NAME
    MurataDriver_sendString_IgnoreAndReturn(true);
    TEST_ASSERT_EQUAL_UINT8(TL_WAIT_FOR_INITIAL_RESP, serviceMurataTransportStateMachine( TL_SEND, NO_RESP, false, cmdBuffer, &commandSuccess));
}

void test_serviceMurataTransportStateMachine_exceedsSendComandsAttempts()
{
_DEBUG_OUT_TEST_NAME
    MurataDriver_sendString_IgnoreAndReturn(true);
    TEST_ASSERT_EQUAL_UINT8(TL_WAIT_FOR_INITIAL_RESP, serviceMurataTransportStateMachine( TL_SEND, NO_RESP, false, cmdBuffer, &commandSuccess));
    MurataDriver_sendString_IgnoreAndReturn(true);
    TEST_ASSERT_EQUAL_UINT8(TL_WAIT_FOR_INITIAL_RESP, serviceMurataTransportStateMachine( TL_SEND, NO_RESP, false, cmdBuffer, &commandSuccess));
    MurataDriver_sendString_IgnoreAndReturn(true);
    TEST_ASSERT_EQUAL_UINT8(TL_WAIT_FOR_INITIAL_RESP, serviceMurataTransportStateMachine( TL_SEND, NO_RESP, false, cmdBuffer, &commandSuccess));
    MurataDriver_sendString_IgnoreAndReturn(true);
    TEST_ASSERT_EQUAL_UINT8(TL_WAIT_FOR_INITIAL_RESP, serviceMurataTransportStateMachine( TL_SEND, NO_RESP, false, cmdBuffer, &commandSuccess));
    MurataDriver_sendString_IgnoreAndReturn(true);
    TEST_ASSERT_EQUAL_UINT8(TL_WAIT_FOR_INITIAL_RESP, serviceMurataTransportStateMachine( TL_SEND, NO_RESP, false, cmdBuffer, &commandSuccess));
    MurataDriver_sendString_IgnoreAndReturn(true);
    TEST_ASSERT_EQUAL_UINT8(TL_IDLE, serviceMurataTransportStateMachine( TL_SEND, NO_RESP, false, cmdBuffer, &commandSuccess));
    TEST_ASSERT_EQUAL_INT(commandSuccess, MOD_NOT_RESP);
}

void test_serviceMurataTransportStateMachine_InitialRespWaitToIdleOkOnly()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8(TL_IDLE, serviceMurataTransportStateMachine( TL_WAIT_FOR_INITIAL_RESP, RESP_OK, false, cmdBuffer, &commandSuccess));
}

void test_serviceMurataTransportStateMachine_InitialRespWaitToSendOkOnlyInvalidCmd()
{
_DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT8(TL_SEND, serviceMurataTransportStateMachine( TL_WAIT_FOR_INITIAL_RESP, INVALID_CMD, false, cmdBuffer, &commandSuccess));
}

void test_serviceMurataTransportStateMachine_InitialRespWaitToSecondResponse()
{
_DEBUG_OUT_TEST_NAME
    MurataDriver_resetAndEnableDownlinkReceiver_Expect();
    TEST_ASSERT_EQUAL_UINT8(TL_WAIT_FOR_SECOND_RESP, serviceMurataTransportStateMachine( TL_WAIT_FOR_INITIAL_RESP, RESP_OK, true, cmdBuffer, &commandSuccess));
}

void test_serviceMurataTransportStateMachine_InitialRespWaitToDelayWhenBusy()
{
_DEBUG_OUT_TEST_NAME
    MurataDriver_resetAndEnableDownlinkReceiver_Expect();
    TEST_ASSERT_EQUAL_UINT8(TL_RESEND_DELAY, serviceMurataTransportStateMachine( TL_WAIT_FOR_INITIAL_RESP, ISBUSY, true, cmdBuffer, &commandSuccess));
}

void test_serviceMurataTransportStateMachine_InitialRespWaitToDelayWhenNoDutyCycle()
{
_DEBUG_OUT_TEST_NAME
    MurataDriver_resetAndEnableDownlinkReceiver_Expect();
    TEST_ASSERT_EQUAL_UINT8(TL_RESEND_DELAY, serviceMurataTransportStateMachine( TL_WAIT_FOR_INITIAL_RESP, NO_DUTY_CYCLE, true, cmdBuffer, &commandSuccess));
}

void test_serviceMurataTransportStateMachine_InitialRespWaitToSendInvalidResp()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_EQUAL_UINT8(TL_SEND, serviceMurataTransportStateMachine( TL_WAIT_FOR_INITIAL_RESP, CORRUPT_RESP, false, cmdBuffer, &commandSuccess));
}

void test_serviceMurataTransportStateMachine_DelayToSendAgain()
{
_DEBUG_OUT_TEST_NAME
    FreeRTOSDelay_Ignore();
    TEST_ASSERT_EQUAL_UINT8(TL_SEND, serviceMurataTransportStateMachine( TL_RESEND_DELAY, NO_RESP, false, cmdBuffer, &commandSuccess));
}


void test_serviceMurataTransportStateMachine_SecondRespACKToIdle()
{
_DEBUG_OUT_TEST_NAME
    commandSuccess = CMD_FAILED;
    TEST_ASSERT_EQUAL_UINT8(TL_IDLE, serviceMurataTransportStateMachine( TL_WAIT_FOR_SECOND_RESP, ACK, true, cmdBuffer, &commandSuccess));
    TEST_ASSERT_EQUAL_UINT8(CMD_SUCCESSFUL, commandSuccess);
}

void test_serviceMurataTransportStateMachine_SecondRespNOACKForPoorLink()
{
_DEBUG_OUT_TEST_NAME
    commandSuccess = CMD_FAILED;
    TEST_ASSERT_EQUAL_UINT8(TL_IDLE, serviceMurataTransportStateMachine( TL_WAIT_FOR_SECOND_RESP, NOACK, true, cmdBuffer, &commandSuccess));
    TEST_ASSERT_EQUAL_UINT8(POOR_LINK, commandSuccess);
}

void test_serviceMurataTransportStateMachine_SecondRespInvalidToWait()
{
_DEBUG_OUT_TEST_NAME
    commandSuccess = CMD_SUCCESSFUL;
    MurataDriver_resetAndEnableDownlinkReceiver_Expect();
    TEST_ASSERT_EQUAL_UINT8(TL_WAIT_FOR_SECOND_RESP, serviceMurataTransportStateMachine( TL_WAIT_FOR_SECOND_RESP, CORRUPT_RESP, true, cmdBuffer, &commandSuccess));
}

void test_serviceMurataTransportStateMachine_SecondRespRemainInStateForRetries()
{
_DEBUG_OUT_TEST_NAME
    commandSuccess = CMD_SUCCESSFUL;
    MurataDriver_resetAndEnableDownlinkReceiver_Expect();
    TEST_ASSERT_EQUAL_UINT8(TL_WAIT_FOR_SECOND_RESP, serviceMurataTransportStateMachine( TL_WAIT_FOR_SECOND_RESP, CNF_RETRY, true, cmdBuffer, &commandSuccess));
    TEST_ASSERT_EQUAL_UINT8(CMD_SUCCESSFUL, commandSuccess);
}

void test_serviceMurataTransportStateMachine_SecondRespIsJoinAccept()
{
_DEBUG_OUT_TEST_NAME
    commandSuccess = CMD_FAILED;
    TEST_ASSERT_EQUAL_UINT8(TL_IDLE, serviceMurataTransportStateMachine( TL_WAIT_FOR_SECOND_RESP, JOIN_ACCEPT, true, cmdBuffer, &commandSuccess));
    TEST_ASSERT_EQUAL_UINT8(CMD_SUCCESSFUL, commandSuccess);
}

void test_serviceMurataTransportStateMachine_SecondRespIsJoinReject()
{
_DEBUG_OUT_TEST_NAME
    commandSuccess = CMD_SUCCESSFUL;
    TEST_ASSERT_EQUAL_UINT8(TL_IDLE, serviceMurataTransportStateMachine( TL_WAIT_FOR_SECOND_RESP, JOIN_REJECTED, true, cmdBuffer, &commandSuccess));
    TEST_ASSERT_EQUAL_UINT8(CMD_FAILED, commandSuccess);
}

void test_serviceMurataTransportStateMachine_FirstRespIsNotConnected()
{
_DEBUG_OUT_TEST_NAME
    commandSuccess = CMD_FAILED;
    TEST_ASSERT_EQUAL_UINT8(TL_IDLE, serviceMurataTransportStateMachine( TL_WAIT_FOR_INITIAL_RESP, NOT_CONNECTED, true, cmdBuffer, &commandSuccess));
    TEST_ASSERT_EQUAL_UINT8(LOST_CONNECTION, commandSuccess);
}

void test_serviceMurataTransportStateMachine_SecondRespIsJoinResetOk()
{
_DEBUG_OUT_TEST_NAME
    commandSuccess = CMD_FAILED;
    TEST_ASSERT_EQUAL_UINT8(TL_IDLE, serviceMurataTransportStateMachine( TL_WAIT_FOR_SECOND_RESP, RESET_OK, true, cmdBuffer, &commandSuccess));
    TEST_ASSERT_EQUAL_UINT8(CMD_SUCCESSFUL, commandSuccess);
}

void test_serviceMurataTransportStateMachine_SecondRespIsRebootOk()
{
_DEBUG_OUT_TEST_NAME
    commandSuccess = CMD_FAILED;
    TEST_ASSERT_EQUAL_UINT8(TL_IDLE, serviceMurataTransportStateMachine( TL_WAIT_FOR_SECOND_RESP, REBOOT_OK, true, cmdBuffer, &commandSuccess));
    TEST_ASSERT_EQUAL_UINT8(CMD_SUCCESSFUL, commandSuccess);
}

/*******************************************************************************
*******************************************************************************/

void test_serviceMurataTransportStateMachine_SendStateCallsDriverSendString()
{
_DEBUG_OUT_TEST_NAME
    MurataDriver_sendString_ExpectAndReturn(cmdBuffer, true);
    TEST_ASSERT_EQUAL_UINT8(TL_WAIT_FOR_INITIAL_RESP, serviceMurataTransportStateMachine( TL_SEND, RESP_OK, true, cmdBuffer, &commandSuccess));
}

void test_serviceMurataTransportStateMachine_DelayStateWaits()
{
_DEBUG_OUT_TEST_NAME
    FreeRTOSDelay_Expect(RESEND_DELAY_MS);
    TEST_ASSERT_EQUAL_UINT8(TL_SEND, serviceMurataTransportStateMachine( TL_RESEND_DELAY, RESP_OK, true, cmdBuffer, &commandSuccess));
}

/*******************************************************************************
    bool serviceStateMachineTimeOut(	murataTransportStates * currentState,
                                        uint32_t * startOfCommand);
*******************************************************************************/

void test_serviceStateMachineTimeOut_InitialRespWaitNoTimeOut()
{
_DEBUG_OUT_TEST_NAME
    uint32_t refTickMs = 0;
    murataTransportStates currentState = TL_WAIT_FOR_INITIAL_RESP;
    get_systick_ms_IgnoreAndReturn(INITIAL_RESP_TO_MS - 1);
    TEST_ASSERT_FALSE( serviceStateMachineTimeOut(&currentState, &refTickMs) );
    TEST_ASSERT_EQUAL_UINT8(TL_WAIT_FOR_INITIAL_RESP, currentState);
}

void test_serviceStateMachineTimeOut_InitialRespWaitWithTimeOut()
{
_DEBUG_OUT_TEST_NAME
    uint32_t refTickMs = 0;
    murataTransportStates currentState = TL_WAIT_FOR_INITIAL_RESP;
    get_systick_ms_IgnoreAndReturn(INITIAL_RESP_TO_MS + 1);
    TEST_ASSERT_TRUE( serviceStateMachineTimeOut(&currentState, &refTickMs) );
    TEST_ASSERT_EQUAL_UINT8(TL_SEND, currentState);
}

void test_serviceStateMachineTimeOut_SecondRespWaitNoTimeOut()
{
_DEBUG_OUT_TEST_NAME
    uint32_t refTickMs = 0;
    murataTransportStates currentState = TL_WAIT_FOR_SECOND_RESP;
    get_systick_ms_IgnoreAndReturn(INITIAL_RESP_TO_MS - 1);
    TEST_ASSERT_FALSE( serviceStateMachineTimeOut(&currentState, &refTickMs) );
    TEST_ASSERT_EQUAL_UINT8(TL_WAIT_FOR_SECOND_RESP, currentState);
}

void test_serviceStateMachineTimeOut_SecondRespWaitWithTimeOut()
{
_DEBUG_OUT_TEST_NAME
    uint32_t refTickMs = 0;
    murataTransportStates currentState = TL_WAIT_FOR_SECOND_RESP;
    get_systick_ms_IgnoreAndReturn(SECOND_RESP_TO_MS + 1);
    TEST_ASSERT_TRUE( serviceStateMachineTimeOut(&currentState, &refTickMs) );
    TEST_ASSERT_EQUAL_UINT8(TL_IDLE, currentState);
}

void test_serviceStateMachineTimeOut_RefreshRefTickCountAtSend()
{
_DEBUG_OUT_TEST_NAME
    uint32_t refTickMs = 0;
    murataTransportStates currentState = TL_SEND;
    get_systick_ms_ExpectAndReturn(21);
    serviceStateMachineTimeOut(&currentState, &refTickMs);
    TEST_ASSERT_EQUAL_UINT32(21, refTickMs);
}

/*******************************************************************************
    bool checkForOverAllSendOpTimeOut(uint32_t transferReferenceTimeMs);
*******************************************************************************/

void test_checkForOverAllSendOpTimeOut_notimeOut()
{
_DEBUG_OUT_TEST_NAME
    uint32_t t = 123456;
    get_systick_ms_ExpectAndReturn(t + (LORAWAN_SEND_CMD_TIMEOUT_SEC - 1) * 1000);
    TEST_ASSERT_FALSE(checkForOverAllSendOpTimeOut(t));
}
void test_checkForOverAllSendOpTimeOut_withtimeOut()
{
_DEBUG_OUT_TEST_NAME
    uint32_t t = 123456;
    get_systick_ms_ExpectAndReturn(t + (LORAWAN_SEND_CMD_TIMEOUT_SEC + 1) * 1000);
    TEST_ASSERT_TRUE(checkForOverAllSendOpTimeOut(t));
}

void test_checkForOverAllSendOpTimeOut_notimeOutWithRollOver()
{
_DEBUG_OUT_TEST_NAME
    uint32_t t = 0xFFFFFFFF - LORAWAN_SEND_CMD_TIMEOUT_SEC*1000/2;
    get_systick_ms_ExpectAndReturn(t + (LORAWAN_SEND_CMD_TIMEOUT_SEC - 1) * 1000);
    TEST_ASSERT_FALSE(checkForOverAllSendOpTimeOut(t));
}
void test_checkForOverAllSendOpTimeOut_withtimeOutWithRollOver()
{
_DEBUG_OUT_TEST_NAME
    uint32_t t = 0xFFFFFFFF - LORAWAN_SEND_CMD_TIMEOUT_SEC*1000/2;
    get_systick_ms_ExpectAndReturn(t + (LORAWAN_SEND_CMD_TIMEOUT_SEC + 1) * 1000);
    TEST_ASSERT_TRUE(checkForOverAllSendOpTimeOut(t));
}