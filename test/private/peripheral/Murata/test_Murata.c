#include "unity.h"
#include "stdbool.h"
#include "string.h"
#include "Murata.c"
#include "mock_MurataInit.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_debug.h"
#include "mock_systick.h"
#include "mock_common.h"
#include "mock_sysmon.h"
#include "mock_Murata_impl.h"
#include "mock_MurataTransport.h"
#include "mock_MurataDriver.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);
#define MCAST_ON_STRING "AT+MCAST=1,016FE84B,0B6F6B0D48939E18DA0AF09D488C2539,CD3311C0E3B15C838DC7468C0B9524D7\r"
#define MCAST_OFF_STRING "AT+MCAST=1,01010101,01010101010101010101010101010101,01010101010101010101010101010101\r"

char respBuffer[DOWN_MSG_MICROCHIP_STRING_MAX];
char cmdBuffer[UP_MSG_MICROCHIP_STRING_MAX];
bool allowSessionRecovery = true;

void setUp(void)
{
    time_of_prev_join_request = 0xFFFF;
    join_dr = 5;
    attempts_at_current_dr = 0;

    uint8_t i = 0;
    for(i = 0 ; i < (UP_MSG_MICROCHIP_STRING_MAX - 1); i++)
    {
        cmdBuffer[i] = 0;
    }
    for(i = 0 ; i < (DOWN_MSG_MICROCHIP_STRING_MAX - 1); i++)
    {
        respBuffer[i] = 0;
    }

    debugprint_Ignore();
}
void tearDown(void)
{

}
/*******************************************************************************
    bool Murata_Init();
*******************************************************************************/

void test_Init_ALlOk()
{
_DEBUG_OUT_TEST_NAME
    allowSessionRecovery = false;
    MurataInit_Run_ExpectAndReturn(_EU868, &allowSessionRecovery, true);
    TEST_ASSERT_TRUE(Murata_Init(_EU868, &allowSessionRecovery));
}
void test_Init_Fails()
{
_DEBUG_OUT_TEST_NAME
    allowSessionRecovery = false;
    MurataInit_Run_ExpectAndReturn(_EU868, &allowSessionRecovery, false);
    TEST_ASSERT_FALSE(Murata_Init(_EU868, &allowSessionRecovery));
}
void test_Init_ResetsAllVariables()
{
_DEBUG_OUT_TEST_NAME

    time_of_prev_join_request = 1;
    join_dr = 1;
    attempts_at_current_dr = 1;
    allowSessionRecovery = false;

    MurataInit_Run_ExpectAndReturn(_EU868, &allowSessionRecovery, true);
    TEST_ASSERT_TRUE(Murata_Init(_EU868, &allowSessionRecovery));

    TEST_ASSERT_EQUAL_UINT32( 0xFFFF, time_of_prev_join_request);
    TEST_ASSERT_EQUAL_UINT8( 5, join_dr);
    TEST_ASSERT_EQUAL_UINT8( 0, attempts_at_current_dr);
}

/*******************************************************************************
    bool Murata_JoinNetwork(uint8_t freqBand);
*******************************************************************************/

void test_Murata_JoinNetwork_SkipIfNotEnoughTimeElapsedSinceLastRequest()
{
_DEBUG_OUT_TEST_NAME

    time_of_prev_join_request = 0;
    get_systick_sec_ExpectAndReturn(INTER_JOIN_REQ_DELAY_SEC);
    TEST_ASSERT_FALSE(Murata_JoinNetwork(_EU868, false));
}

void test_Murata_JoinNetwork_FirstCallAlwaysExecute()
{
_DEBUG_OUT_TEST_NAME

    get_systick_sec_ExpectAndReturn(1000);
    joinDutyCycleHandler_IgnoreAndReturn(false);
    MurataTransport_sendCommand_IgnoreAndReturn(CMD_FAILED);
    TEST_ASSERT_FALSE(Murata_JoinNetwork(_EU868, false));
}

void test_Murata_JoinNetwork_ExecuteRequestIfEnoughTimeElapsed()
{
_DEBUG_OUT_TEST_NAME

    time_of_prev_join_request = 0;
    get_systick_sec_ExpectAndReturn(INTER_JOIN_REQ_DELAY_SEC+1);
    joinDutyCycleHandler_IgnoreAndReturn(false);
    MurataTransport_sendCommand_IgnoreAndReturn(CMD_FAILED);
    TEST_ASSERT_FALSE(Murata_JoinNetwork(_EU868, false));
}

void test_Murata_JoinNetwork_UpdatesJoinDrIfChanged()
{
_DEBUG_OUT_TEST_NAME
    
    join_dr = 4;
    attempts_at_current_dr = JOIN_REQ_AT_SAME_DR_LIMIT;
    get_systick_sec_ExpectAndReturn(0);
    joinDutyCycleHandler_IgnoreAndReturn(true);
    joinDutyCycleHandler_ReturnThruPtr_joinDR(&join_dr);
    updateDataRate_Expect(4);
    MurataTransport_sendCommand_IgnoreAndReturn(CMD_FAILED);
    TEST_ASSERT_FALSE(Murata_JoinNetwork(_EU868, false));
}


void test_Murata_JoinNetwork_ReturnsFalseIfCommandFailed()
{
_DEBUG_OUT_TEST_NAME

    time_of_prev_join_request = 0;
    get_systick_sec_ExpectAndReturn(INTER_JOIN_REQ_DELAY_SEC+1);
    joinDutyCycleHandler_IgnoreAndReturn(false);
    MurataTransport_sendCommand_IgnoreAndReturn(CMD_FAILED);
    TEST_ASSERT_FALSE(Murata_JoinNetwork(_EU868, false));
}

void test_Murata_JoinNetwork_ReturnsTrueIfCommandSucceeded()
{
_DEBUG_OUT_TEST_NAME

    time_of_prev_join_request = 0;
    get_systick_sec_ExpectAndReturn(INTER_JOIN_REQ_DELAY_SEC+1);
    joinDutyCycleHandler_IgnoreAndReturn(false);
    MurataTransport_sendCommand_IgnoreAndReturn(CMD_SUCCESSFUL);
    bandSpecificPostJoinRoutine_Ignore();
    TEST_ASSERT_TRUE(Murata_JoinNetwork(_EU868, false));
}

void test_Murata_JoinNetwork_PostRoutineCalledIfJoined()
{
_DEBUG_OUT_TEST_NAME

    time_of_prev_join_request = 0;
    get_systick_sec_ExpectAndReturn(INTER_JOIN_REQ_DELAY_SEC+1);
    joinDutyCycleHandler_IgnoreAndReturn(false);
    MurataTransport_sendCommand_IgnoreAndReturn(CMD_SUCCESSFUL);
    bandSpecificPostJoinRoutine_Expect(_EU868);
    TEST_ASSERT_TRUE(Murata_JoinNetwork(_EU868, false));
}

void test_Murata_JoinNetwork_SkipJoinRequestIfRestoringSession()
{
_DEBUG_OUT_TEST_NAME

    time_of_prev_join_request = 0;
    get_systick_sec_ExpectAndReturn(INTER_JOIN_REQ_DELAY_SEC+1);
    bandSpecificPostJoinRoutine_Expect(_EU868);
    TEST_ASSERT_TRUE(Murata_JoinNetwork(_EU868, true));
}

/*******************************************************************************
   char * Murata_ReadMsg();
*******************************************************************************/

void test_ReadMsg_AllOk()
{
_DEBUG_OUT_TEST_NAME
    char buffer[20];
    sprintf(buffer, "TEST MESSAGE\r\n");
    MurataTransport_getDownlinkPayload_ExpectAnyArgsAndReturn(true);
    MurataTransport_getDownlinkPayload_ReturnMemThruPtr_callerBuffer(buffer, 14);
    TEST_ASSERT_EQUAL_STRING("TEST MESSAGE\r\n", Murata_ReadMsg());
}

void test_ReadMsg_PayloadNotValid()
{
_DEBUG_OUT_TEST_NAME

    MurataTransport_getDownlinkPayload_IgnoreAndReturn(false);
    sysmon_report_error_Expect(LORA, MSG_RX_INVALID);
    TEST_ASSERT_EQUAL_STRING(NULL, Murata_ReadMsg());
}


// /*******************************************************************************
//    bool Murata_UpdateMcastState(bool mcastReqState);
// *******************************************************************************/

void test_UpdateMcastState_EnableMcastSession()
{
_DEBUG_OUT_TEST_NAME

    MurataTransport_sendCommand_ExpectAndReturn(MCAST_ON_STRING, false, NULL, 0, CMD_SUCCESSFUL);
    TEST_ASSERT_TRUE(Murata_UpdateMcastState(true));
}

void test_UpdateMcastState_DisableMcastSession()
{
_DEBUG_OUT_TEST_NAME

    MurataTransport_sendCommand_ExpectAndReturn(MCAST_OFF_STRING, false, NULL, 0, CMD_SUCCESSFUL);
    TEST_ASSERT_TRUE(Murata_UpdateMcastState(false));
}

void test_UpdateMcastState_EnableMcastSessionWithRetries()
{
_DEBUG_OUT_TEST_NAME

    MurataTransport_sendCommand_ExpectAndReturn(MCAST_ON_STRING, false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(MCAST_RETRY_DELAY);
    MurataTransport_sendCommand_ExpectAndReturn(MCAST_ON_STRING, false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(MCAST_RETRY_DELAY);
    MurataTransport_sendCommand_ExpectAndReturn(MCAST_ON_STRING, false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(MCAST_RETRY_DELAY);
    MurataTransport_sendCommand_ExpectAndReturn(MCAST_ON_STRING, false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(MCAST_RETRY_DELAY);
    MurataTransport_sendCommand_ExpectAndReturn(MCAST_ON_STRING, false, NULL, 0, CMD_SUCCESSFUL);
    TEST_ASSERT_TRUE(Murata_UpdateMcastState(true));
}

void test_UpdateMcastState_EnableMcastSessionWillRetry5Times()
{
_DEBUG_OUT_TEST_NAME

    MurataTransport_sendCommand_ExpectAndReturn(MCAST_ON_STRING, false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(MCAST_RETRY_DELAY);
    MurataTransport_sendCommand_ExpectAndReturn(MCAST_ON_STRING, false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(MCAST_RETRY_DELAY);
    MurataTransport_sendCommand_ExpectAndReturn(MCAST_ON_STRING, false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(MCAST_RETRY_DELAY);
    MurataTransport_sendCommand_ExpectAndReturn(MCAST_ON_STRING, false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(MCAST_RETRY_DELAY);
    MurataTransport_sendCommand_ExpectAndReturn(MCAST_ON_STRING, false, NULL, 0, CMD_FAILED);
    FreeRTOSDelay_Expect(MCAST_RETRY_DELAY);
    TEST_ASSERT_FALSE(Murata_UpdateMcastState(true));
}

/*******************************************************************************
   bool Murata_SendMsg(char * p_msg_to_send, bool ack_is_required);
*******************************************************************************/

void test_Murata_SendMsg_ReturnsFalseInstantlyPayloadTooLong()
{
_DEBUG_OUT_TEST_NAME
    uint8_t i = 0;
    for(i = 0 ; i < (UP_MSG_PAYLOAD + 1)*2; i++)
    {
        cmdBuffer[i] = 'A';
    }
    TEST_ASSERT_EQUAL_UINT8(CMD_FAILED, Murata_SendMsg(cmdBuffer, true));
}

void test_Murata_SendMsg_ConfirmedUplinkFormat()
{
_DEBUG_OUT_TEST_NAME
    uint8_t i = 0;
    char buffer[UP_MSG_MICROCHIP_STRING_MAX] = {0};
    for(i = 0 ; i < UP_MSG_PAYLOAD; i++)
    {
        cmdBuffer[i] = 'A';
    }
    sprintf(buffer, "AT+CTX 25\r%s", cmdBuffer);
    MurataTransport_sendCommand_ExpectAndReturn(buffer, true, NULL, 0, CMD_SUCCESSFUL);    
    TEST_ASSERT_EQUAL_UINT8(CMD_SUCCESSFUL, Murata_SendMsg(cmdBuffer, true));
}

void test_Murata_SendMsg_ConfirmedUplinkReturnsFailed()
{
_DEBUG_OUT_TEST_NAME

    MurataTransport_sendCommand_IgnoreAndReturn(CMD_FAILED);    
    TEST_ASSERT_EQUAL_UINT8(CMD_FAILED, Murata_SendMsg("MSG", true));
}


void test_Murata_SendMsg_UnconfirmedUplinkFormat()
{
_DEBUG_OUT_TEST_NAME
    uint8_t i = 0;
    char buffer[UP_MSG_MICROCHIP_STRING_MAX] = {0};
    for(i = 0 ; i < UP_MSG_PAYLOAD; i++)
    {
        cmdBuffer[i] = 'A';
    }
    sprintf(buffer, "AT+UTX 25\r%s", cmdBuffer);
    MurataTransport_sendCommand_ExpectAndReturn(buffer, false, NULL, 0, CMD_SUCCESSFUL);    
    TEST_ASSERT_EQUAL_UINT8(CMD_SUCCESSFUL, Murata_SendMsg(cmdBuffer, false));
}

void test_Murata_SendMsg_UnconfirmedUplinkReturnsFailed()
{
_DEBUG_OUT_TEST_NAME

    MurataTransport_sendCommand_IgnoreAndReturn(CMD_FAILED);    
    TEST_ASSERT_EQUAL_UINT8(CMD_FAILED, Murata_SendMsg("MSG", false));
}


void test_Murata_SendMsg_ConfirmedUplinkReturnsPoorLink()
{
_DEBUG_OUT_TEST_NAME

    MurataTransport_sendCommand_IgnoreAndReturn(POOR_LINK);    
    TEST_ASSERT_EQUAL_UINT8(POOR_LINK, Murata_SendMsg("MSG", true));
}


void test_Murata_SendMsg_ConfirmedUplinkReturnsLostConnection()
{
_DEBUG_OUT_TEST_NAME

    MurataTransport_sendCommand_IgnoreAndReturn(LOST_CONNECTION);    
    TEST_ASSERT_EQUAL_UINT8(LOST_CONNECTION, Murata_SendMsg("MSG", true));
}


/*******************************************************************************
   void Murata_FullHwReset();
*******************************************************************************/

void test_Murata_FullHwReset_SendsOutCorrectCmd()
{
_DEBUG_OUT_TEST_NAME

    MurataTransport_sendCommand_ExpectAndReturn("AT+FACNEW\r", true, NULL, 0, true);
    Murata_FullHwReset();     
}

void test_Murata_FullHwReset_KepstryingUntilSUccessful()
{
_DEBUG_OUT_TEST_NAME

    MurataTransport_sendCommand_ExpectAndReturn("AT+FACNEW\r", true, NULL, 0, false);
    MurataTransport_sendCommand_ExpectAndReturn("AT+FACNEW\r", true, NULL, 0, false);
    MurataTransport_sendCommand_ExpectAndReturn("AT+FACNEW\r", true, NULL, 0, false);
    MurataTransport_sendCommand_ExpectAndReturn("AT+FACNEW\r", true, NULL, 0, true);
    Murata_FullHwReset();     
}

/*******************************************************************************
   void Murata_FullHwReset();
*******************************************************************************/

void test_Murata_SetBestSettingsDataRateNotOptimum()
{
_DEBUG_OUT_TEST_NAME
    getDataRate_ExpectAndReturn(1);
    getPowerIndex_ExpectAndReturn(1);
    MurataTransport_sendCommand_ExpectAndReturn("AT+DR=0\r", false, NULL, 0, true);
    MurataTransport_sendCommand_ExpectAndReturn("AT+RFPOWER=0,1\r", false, NULL, 0, true);
    TEST_ASSERT_TRUE(Murata_SetBestSettings());
}
void test_Murata_SetBestSettingsPowerIndexNotOptimum()
{
_DEBUG_OUT_TEST_NAME
    getDataRate_ExpectAndReturn(0);
    getPowerIndex_ExpectAndReturn(3);
    MurataTransport_sendCommand_ExpectAndReturn("AT+DR=0\r", false, NULL, 0, true);
    MurataTransport_sendCommand_ExpectAndReturn("AT+RFPOWER=0,1\r", false, NULL, 0, true);
    TEST_ASSERT_TRUE(Murata_SetBestSettings());
}

void test_Murata_SetBestSettingsExecutesCorrectCommandsEvenWithFailures()
{
_DEBUG_OUT_TEST_NAME
    getDataRate_ExpectAndReturn(0);
    getPowerIndex_ExpectAndReturn(3);
    MurataTransport_sendCommand_ExpectAndReturn("AT+DR=0\r", false, NULL, 0, false);
    MurataTransport_sendCommand_ExpectAndReturn("AT+RFPOWER=0,1\r", false, NULL, 0, true);
    TEST_ASSERT_TRUE(Murata_SetBestSettings());
}

void test_Murata_SetBestSettingsAlreadyOptimum()
{
_DEBUG_OUT_TEST_NAME
    getDataRate_ExpectAndReturn(0);
    getPowerIndex_ExpectAndReturn(1);
    TEST_ASSERT_FALSE(Murata_SetBestSettings());
}

/*******************************************************************************
   void Murata_getDevEUi(char * p_devEuiString);
*******************************************************************************/


void test_getDevEUi()
{
_DEBUG_OUT_TEST_NAME
    char DevEUi[17]={0};
    MurataInit_getDevEUi_Expect(DevEUi);
    Murata_getDevEUi(DevEUi);    
}
