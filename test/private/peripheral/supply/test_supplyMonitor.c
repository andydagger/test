#include "unity.h"
#include "supplyMonitor.c"

#include "mock_supplyMonitor_impl.h"
#include "mock_adc_procx.h"
#include "mock_FreeRTOS.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_sysmon.h"
#include "mock_systick.h"
#include "mock_debug.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    supply12vRailIsReady = false;
    sampleId = 0;
    debugprint_Ignore();
}
void tearDown(void)
{

}

//-----------------------------------------------------------------
// bool supplyMonitor_get12vRailIsReady(void)
//-----------------------------------------------------------------

void test_get12vRailIsready_false(void)
{
_DEBUG_OUT_TEST_NAME
    supply12vRailIsReady = false;
    TEST_ASSERT_FALSE(supplyMonitor_get12vRailIsReady());
}

void test_get12vRailIsready_true(void)
{
_DEBUG_OUT_TEST_NAME
    supply12vRailIsReady = true;
    TEST_ASSERT_TRUE(supplyMonitor_get12vRailIsReady());
}

//-----------------------------------------------------------------
// void supplyMonitor_check12VRail(void)
//-----------------------------------------------------------------

void test_check12VRail_allGood(void)
{
_DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    // 12V line increasing slowly
    get_adc_procx_p_detect_volt_ExpectAndReturn(9.0F);
    FreeRTOSDelay_Expect(SAMPLING_GAP_MS);
    get_adc_procx_p_detect_volt_ExpectAndReturn(MIN_TO_START_DELAY_V + 0.1F);
    FreeRTOSDelay_Expect(SAMPLING_GAP_MS);
    // Wait for supply line to stabalise
    FreeRTOSDelay_Expect(DELAY_TO_STABALISE_MS);
    // Sampling routine
    sampleId = NUMBER_OF_SAMPLES - 1;
    get_adc_procx_p_detect_volt_ExpectAndReturn(12.0F);
    FreeRTOSDelay_Expect(SAMPLING_GAP_MS);
    // Check Min Max and Average against limits
    supplyMonitor_getMin_ExpectAnyArgsAndReturn(MIN_LIMIT + 0.1F);
    supplyMonitor_getMax_ExpectAnyArgsAndReturn(MAX_LIMIT - 0.1F);
    supplyMonitor_getAv_ExpectAnyArgsAndReturn(AV_MAX - 0.1F);

    TEST_ASSERT_EQUAL_INT(SUPPLY_IS_OK, supplyMonitor_check12VRail());
    TEST_ASSERT_TRUE(supply12vRailIsReady);
}


void test_check12VRail_correctNumberOfSamples(void)
{
    _DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    get_adc_procx_p_detect_volt_IgnoreAndReturn(12.0F);
    FreeRTOSDelay_Ignore();

    supplyMonitor_getMin_IgnoreAndReturn(12.0F);
    supplyMonitor_getMax_IgnoreAndReturn(12.0F);
    supplyMonitor_getAv_IgnoreAndReturn(12.0F);
   
    supplyMonitor_check12VRail();
    TEST_ASSERT_EQUAL_INT(NUMBER_OF_SAMPLES, sampleId);
    TEST_ASSERT_TRUE(supply12vRailIsReady);
}

void test_check12VRail_timesOutOnCrossingThreshold(void)
{
    _DEBUG_OUT_TEST_NAME
    get_adc_procx_p_detect_volt_IgnoreAndReturn(9.0F);
    FreeRTOSDelay_Ignore();
    get_systick_ms_ExpectAndReturn(0);
    get_systick_ms_ExpectAndReturn(CHARGE_TIME_OUT_MS - 1);
    get_systick_ms_ExpectAndReturn(CHARGE_TIME_OUT_MS + 1);
    
    TEST_ASSERT_EQUAL_INT(SUPPLY_DID_NOT_REACH_TH, supplyMonitor_check12VRail());
    TEST_ASSERT_FALSE(supply12vRailIsReady);
}

void test_check12VRail_minimumSampleTooLow(void)
{
    _DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    get_adc_procx_p_detect_volt_IgnoreAndReturn(12.0F);
    FreeRTOSDelay_Ignore();

    supplyMonitor_getMin_ExpectAnyArgsAndReturn(MIN_LIMIT - 0.1F);
    supplyMonitor_getMax_ExpectAnyArgsAndReturn(MAX_LIMIT - 0.1F);
    supplyMonitor_getAv_ExpectAnyArgsAndReturn(AV_MAX - 0.1F);
   
    TEST_ASSERT_EQUAL_INT(SUPPLY_MIN_TOO_LOW, supplyMonitor_check12VRail());
    TEST_ASSERT_FALSE(supply12vRailIsReady);
}

void test_check12VRail_maximumSampleTooHigh(void)
{
    _DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    get_adc_procx_p_detect_volt_IgnoreAndReturn(12.0F);
    FreeRTOSDelay_Ignore();

    supplyMonitor_getMin_ExpectAnyArgsAndReturn(MIN_LIMIT + 0.1F);
    supplyMonitor_getMax_ExpectAnyArgsAndReturn(MAX_LIMIT + 0.1F);
    supplyMonitor_getAv_ExpectAnyArgsAndReturn(AV_MAX - 0.1F);
   
    TEST_ASSERT_EQUAL_INT(SUPPLY_MAX_TOO_HIGH, supplyMonitor_check12VRail());
    TEST_ASSERT_FALSE(supply12vRailIsReady);
}

void test_check12VRail_averageTooLow(void)
{
    _DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    get_adc_procx_p_detect_volt_IgnoreAndReturn(12.0F);
    FreeRTOSDelay_Ignore();

    supplyMonitor_getMin_ExpectAnyArgsAndReturn(MIN_LIMIT + 0.1F);
    supplyMonitor_getMax_ExpectAnyArgsAndReturn(MAX_LIMIT - 0.1F);
    supplyMonitor_getAv_ExpectAnyArgsAndReturn(AV_MIN - 0.1F);
   
    TEST_ASSERT_EQUAL_INT(SUPPLY_AV_TOO_LOW, supplyMonitor_check12VRail());
    TEST_ASSERT_FALSE(supply12vRailIsReady);
}

void test_check12VRail_averageTooHigh(void)
{
    _DEBUG_OUT_TEST_NAME
    get_systick_ms_IgnoreAndReturn(0);
    get_adc_procx_p_detect_volt_IgnoreAndReturn(12.0F);
    FreeRTOSDelay_Ignore();

    supplyMonitor_getMin_ExpectAnyArgsAndReturn(MIN_LIMIT + 0.1F);
    supplyMonitor_getMax_ExpectAnyArgsAndReturn(MAX_LIMIT - 0.1F);
    supplyMonitor_getAv_ExpectAnyArgsAndReturn(AV_MAX + 0.1F);
   
    TEST_ASSERT_EQUAL_INT(SUPPLY_AV_TOO_HIGH, supplyMonitor_check12VRail());
    TEST_ASSERT_FALSE(supply12vRailIsReady);
}