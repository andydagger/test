#include "unity.h"
#include <stdio.h>
#include <string.h>
#include "supplyMonitor_impl.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{

}
void tearDown(void)
{

}

static void initialiseArray(float * array, uint8_t size, float value)
{
    uint8_t n;
    for(n=0; n<size; n++)
    {
        array[n] = value;
    }
}

//-----------------------------------------------------------------
// float supplyMonitor_getMin(float * sampleTable, uint8_t nSamples)
//-----------------------------------------------------------------
void test_getMin_firstSampleIsSmallest(void)
{
_DEBUG_OUT_TEST_NAME
    uint8_t nSamples = 10;
    float samples[10];
    initialiseArray(samples, nSamples, 12.3);
    samples[0] = 12.0F;
    TEST_ASSERT_EQUAL_FLOAT(12.0F, supplyMonitor_getMin(samples, nSamples));
}

void test_getMin_lastSampleIsSmallest(void)
{
_DEBUG_OUT_TEST_NAME
    uint8_t nSamples = 10;
    float samples[10];
    initialiseArray(samples, nSamples, 12.3);
    samples[9] = 12.1F;
    TEST_ASSERT_EQUAL_FLOAT(12.1F, supplyMonitor_getMin(samples, nSamples));
}

//-----------------------------------------------------------------
// float supplyMonitor_getMax(float * sampleTable, uint8_t nSamples)
//-----------------------------------------------------------------
void test_getMax_firstSampleIsHighest(void)
{
_DEBUG_OUT_TEST_NAME
    uint8_t nSamples = 10;
    float samples[10];
    initialiseArray(samples, nSamples, 12.3);
    samples[0] = 12.9F;
    TEST_ASSERT_EQUAL_FLOAT(12.9F, supplyMonitor_getMax(samples, nSamples));
}

void test_getMax_lastSampleIsHighest(void)
{
_DEBUG_OUT_TEST_NAME
    uint8_t nSamples = 10;
    float samples[10];
    initialiseArray(samples, nSamples, 12.3);
    samples[9] = 12.6F;
    TEST_ASSERT_EQUAL_FLOAT(12.6F, supplyMonitor_getMax(samples, nSamples));
}

//-----------------------------------------------------------------
// float supplyMonitor_getAv(float * sampleTable, uint8_t nSamples)
//-----------------------------------------------------------------
void test_getAv_tenSamples(void)
{
_DEBUG_OUT_TEST_NAME
    uint8_t nSamples = 10;
    float samples[10];
    initialiseArray(samples, nSamples, 12.3);
    samples[0] = 12.2F;
    samples[nSamples-1] = 12.4F;
    TEST_ASSERT_EQUAL_FLOAT(12.3F, supplyMonitor_getAv(samples, nSamples));
}

void test_getAv_twentySamples(void)
{
_DEBUG_OUT_TEST_NAME
    uint8_t nSamples = 20;
    float samples[20];
    initialiseArray(samples, nSamples, 12.3);
    samples[0] = 12.2F;
    samples[nSamples-1] = 12.4F;
    TEST_ASSERT_EQUAL_FLOAT(12.3F, supplyMonitor_getAv(samples, nSamples));
}