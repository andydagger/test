#include "unity.h"
#include "powerMonitor_impl.c"

#include "mock_debug.h"
#include "mock_FreeRTOS.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_powerMonitor.h"
#include "mock_stm8Metering.h"
#include "mock_stm8Metering_calibration.h"
#include "mock_microchipMetering.h"
#include "mock_microchipMetering_calibration.h"
#include "mock_ade9153aMetering.h"
#include "mock_ade9153aMetering_calibration.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);


void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// powerMonitor_HWType powerMonitor_detectHW( void)
//-----------------------------------------------------------------

//-----------------------------------------------------------------
// test powerMonitor_detectHW() 
// detecting analog device part ADE9153A
//-----------------------------------------------------------------
void test_detectHW_FoundADI_ADE9153A()
{
    _DEBUG_OUT_TEST_NAME

    ade9153aMetering_detectHW_ExpectAndReturn(POWER_MONITOR_RESULT_OK);
    
    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_HW_ADI_ADE9153A, powerMonitor_detectHW());
}

//-----------------------------------------------------------------
// test powerMonitor_detectHW() 
// detecting microchip device
//-----------------------------------------------------------------
void test_detectHW_FoundMicrochipDevice()
{
    _DEBUG_OUT_TEST_NAME

    ade9153aMetering_detectHW_ExpectAndReturn(POWER_MONITOR_RESULT_HW_NOT_DETECTED);
    microchipMetering_detectHW_ExpectAndReturn(POWER_MONITOR_RESULT_OK);
    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_HW_MICROCHIP, powerMonitor_detectHW());
}

//-----------------------------------------------------------------
// test powerMonitor_detectHW() 
// detecting stm8 device
//-----------------------------------------------------------------
void test_detectHW_DefaultsOnStm8()
{
    _DEBUG_OUT_TEST_NAME

    ade9153aMetering_detectHW_ExpectAndReturn(POWER_MONITOR_RESULT_HW_NOT_DETECTED);
    microchipMetering_detectHW_ExpectAndReturn(POWER_MONITOR_RESULT_HW_NOT_DETECTED);
    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_HW_STM8, powerMonitor_detectHW());
}

//-----------------------------------------------------------------
// test powerMonitor_detectHW()
// invalid retun value analog device detection
//-----------------------------------------------------------------
void test_detectHW_ReturnsUnexpectedValue()
{
    _DEBUG_OUT_TEST_NAME

    ade9153aMetering_detectHW_ExpectAndReturn(0xFF);
    microchipMetering_detectHW_ExpectAndReturn(POWER_MONITOR_RESULT_HW_NOT_DETECTED);
    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_HW_STM8, powerMonitor_detectHW());
}


//-----------------------------------------------------------------
// void powerMonitor_detectHW() 
// check error counters cleared
//-----------------------------------------------------------------
void test_detectHW_errorCounterValues()
{
    timeout_error_counter = 1;
    data_error_counter = 1;

    ade9153aMetering_detectHW_ExpectAndReturn(POWER_MONITOR_RESULT_HW_NOT_DETECTED);
    microchipMetering_detectHW_ExpectAndReturn(POWER_MONITOR_RESULT_HW_NOT_DETECTED);

    powerMonitor_detectHW();

    TEST_ASSERT_EQUAL_UINT32(0, data_error_counter);
    TEST_ASSERT_EQUAL_UINT32(0, timeout_error_counter);
}


//-----------------------------------------------------------------
// void powerMonitor_resetDataset() 
// test values in the structure have been initialised
//-----------------------------------------------------------------
void test_powerMonitor_resetDataset()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset dataset = { 0 };

    dataset.accumulatedEnergy_20WhrUnits = 10;
    dataset.instantPowerFactor = 100.0F;
    dataset.instantIrms = 100.0F;
    dataset.instantPower = 100.0F;
    dataset.instantVrms = 100.0F;
    dataset.vrmsAverage = 100.0F;
    dataset.vrmsMax = 100.0F;
    dataset.vrmsMin = 0.0F;

    powerMonitor_resetDataset(&dataset);

    TEST_ASSERT_EQUAL_UINT32(0, dataset.accumulatedEnergy_20WhrUnits);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, dataset.instantPowerFactor);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, dataset.instantIrms);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, dataset.instantPower);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, dataset.instantVrms);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, dataset.vrmsAverage);
    TEST_ASSERT_EQUAL_FLOAT(0.0F, dataset.vrmsMax);
    TEST_ASSERT_EQUAL_FLOAT(300.0F, dataset.vrmsMin);
}

//-----------------------------------------------------------------
// void powerMonitor_resetDataset() 
// test values in the structure have been not been initialised
//-----------------------------------------------------------------
void test_powerMonitor_resetDataset_NULLpointer()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_resetDataset(NULL);

}

//-----------------------------------------------------------------
// void test_powerMonitor_resultHandler() 
// Service call returning : POWER_MONITOR_RESULT_OK
//-----------------------------------------------------------------
void test_powerMonitor_resultHandler_ResultOK()
{
    _DEBUG_OUT_TEST_NAME

    clear_sysmon_fault_Expect(NO_POWER_DATA);

    get_sysmon_fault_status_ExpectAndReturn(POWER_FAILURE, false);

    powerMonitor_resultHandler(POWER_MONITOR_RESULT_OK);
}

//-----------------------------------------------------------------
// void test_powerMonitor_resultHandler() 
// Service call returning : POWER_MONITOR_RESULT_OK
// Test path with POWER_FAILURE flag set
//-----------------------------------------------------------------
void test_powerMonitor_resultHandler_ResultOKPowerFailureFlagSet()
{
    _DEBUG_OUT_TEST_NAME

    clear_sysmon_fault_Expect(NO_POWER_DATA);

    get_sysmon_fault_status_ExpectAndReturn(POWER_FAILURE, true);

    clear_sysmon_fault_Expect(POWER_FAILURE);

    clear_sysmon_last_gasp_flag_Expect();

    powerMonitor_resultHandler(POWER_MONITOR_RESULT_OK);
}

//-----------------------------------------------------------------
// void test_powerMonitor_resultHandler() 
// POWER_MONITOR_RESULT_TIMEOUT
// check error counter increment and NO_POWER_DATA flag set
//-----------------------------------------------------------------
void test_powerMonitor_resultHandler_ResultTimeout()
{
    _DEBUG_OUT_TEST_NAME

    timeout_error_counter = 0;

    set_sysmon_fault_Expect(NO_POWER_DATA);

    powerMonitor_resultHandler(POWER_MONITOR_RESULT_TIMEOUT);

    TEST_ASSERT_EQUAL_UINT32(timeout_error_counter, 1);
}

//-----------------------------------------------------------------
// void test_powerMonitor_resultHandler() 
// POWER_MONITOR_RESULT_DATA_ERROR
// check error counter increment and NO_POWER_DATA flag set
//-----------------------------------------------------------------
void test_powerMonitor_resultHandler_ResultDataError()
{
    _DEBUG_OUT_TEST_NAME

    data_error_counter = 0;

    clear_sysmon_fault_Expect(NO_POWER_DATA);

    powerMonitor_resultHandler(POWER_MONITOR_RESULT_DATA_ERROR);

    TEST_ASSERT_EQUAL_UINT32(data_error_counter, 1);
}

//-----------------------------------------------------------------
// void test_powerMonitor_resultHandler() 
// POWER_MONITOR_RESULT_POWER_FAIL
// check POWER_FAILURE flag set etc
//-----------------------------------------------------------------
void test_powerMonitor_resultHandler_ResultPowerFail()
{
    _DEBUG_OUT_TEST_NAME

    clear_sysmon_fault_Expect(NO_POWER_DATA);
    set_sysmon_fault_Expect(POWER_FAILURE);
    set_sysmon_last_gasp_flag_Expect();

    powerMonitor_resultHandler(POWER_MONITOR_RESULT_POWER_FAIL);
}

//-----------------------------------------------------------------
// void test_powerMonitor_resultHandler() 
// POWER_MONITOR_RESULT_POWER_RESTORE
// check POWER_FAILURE flag cleared
//-----------------------------------------------------------------
void test_powerMonitor_resultHandler_ResultPowerRestore()
{
    _DEBUG_OUT_TEST_NAME

    clear_sysmon_fault_Expect(NO_POWER_DATA);
    clear_sysmon_fault_Expect(POWER_FAILURE);
    clear_sysmon_last_gasp_flag_Expect();

    powerMonitor_resultHandler(POWER_MONITOR_RESULT_POWER_RESTORE);
}
/*------------------------------ End of File -------------------------------*/
