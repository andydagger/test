#include "unity.h"
#include "powerMonitor.c"

#include "mock_debug.h"
#include "mock_FreeRTOS.h"
#include "mock_task.h"
#include "mock_config_dataset.h"
#include "mock_sysmon.h"
#include "mock_powerMonitor_impl.h"
#include "mock_stm8Metering.h"
#include "mock_stm8Metering_calibration.h"
#include "mock_microchipMetering.h"
#include "mock_microchipMetering_calibration.h"
#include "mock_ade9153aMetering.h"
#include "mock_ade9153aMetering_calibration.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);


void setUp(void)
{
    debugprint_Ignore();
    hwType = POWER_MONITOR_HW_UNKNOWN;
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// powerMonitor_HWType powerMonitor_getHWType( void)
//-----------------------------------------------------------------
void test_getHwType_stm8()
{
    _DEBUG_OUT_TEST_NAME
    
    hwType = POWER_MONITOR_HW_STM8;

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_HW_STM8, powerMonitor_getHWType());
}


//-----------------------------------------------------------------
// void powerMonitor_init() 
// detecting stm8 and calling stm8 init function
//-----------------------------------------------------------------
void test_init_withStm8()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_detectHW_ExpectAndReturn(POWER_MONITOR_HW_STM8);

    stm8Metering_init_Expect();

    powerMonitor_init();
}

//-----------------------------------------------------------------
// void powerMonitor_init()
// detecting microchip and calling microchip init function
//-----------------------------------------------------------------
void test_init_withMicrochip()
{
    _DEBUG_OUT_TEST_NAME

    powerMonitor_detectHW_ExpectAndReturn(POWER_MONITOR_HW_MICROCHIP);

    microchipMetering_init_Expect();

    powerMonitor_init();
}

//-----------------------------------------------------------------
// void powerMonitor_init()
// detecting analog device and calling analog devices init function
//-----------------------------------------------------------------
void test_init_withADI_ADE9153A()
{
    _DEBUG_OUT_TEST_NAME
 
    powerMonitor_detectHW_ExpectAndReturn(POWER_MONITOR_HW_ADI_ADE9153A);

    ade9153aMetering_init_Expect();

    powerMonitor_init();
}

//-----------------------------------------------------------------
// void powerMonitor_initMeasurement()
// validation of stm8 init measurement call
//-----------------------------------------------------------------
void test_initMeasurement_withStm8()
{
    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_STM8;

    powerMonitor_resetDataset_ExpectAnyArgs();

    stm8Metering_initMeasurement_Expect(&powerDataset);

    powerMonitor_initMeasurement();
}

//-----------------------------------------------------------------
// void powerMonitor_initMeasurement()
// validation of microchip init measurement call
//-----------------------------------------------------------------
void test_initMeasurement_withMicrochip()
{
    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_MICROCHIP;

    powerMonitor_resetDataset_ExpectAnyArgs();
    
    microchipMetering_initMeasurement_Expect(&powerDataset);

    powerMonitor_initMeasurement();
}

//-----------------------------------------------------------------
// void powerMonitor_initMeasurement()
// validation of analog device ADE9153A init measurement call
//-----------------------------------------------------------------
void test_initMeasurement_withADI_ADE9153A()
{
    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_ADI_ADE9153A;

    powerMonitor_resetDataset_ExpectAnyArgs();

    ade9153aMetering_initMeasurement_Expect(&powerDataset);

    powerMonitor_initMeasurement();
}

//-----------------------------------------------------------------
// void powerMonitor_initMeasurement()
// validation of nothing being initialised when invalid HW type
//-----------------------------------------------------------------
void test_initMeasurement_withInvalidHWType()
{
    _DEBUG_OUT_TEST_NAME

    hwType = 0xFF;

    powerMonitor_resetDataset_ExpectAnyArgs();

    powerMonitor_initMeasurement();
}

//-----------------------------------------------------------------
// void powerMonitor_uartISR()
// Invalid HW type test
//-----------------------------------------------------------------
void test_uartISR_withInvalidHW()
{
    _DEBUG_OUT_TEST_NAME

    hwType = 0xFF;

    powerMonitor_uartISR();
}

//-----------------------------------------------------------------
// void powerMonitor_uartISR()
// STM8 HW type test
//-----------------------------------------------------------------
void test_uartISR_withStm8()
{
    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_STM8;

    stm8Metering_uartISR_Expect();

    powerMonitor_uartISR();
}

//-----------------------------------------------------------------
// void powerMonitor_uartISR()
// Microchip HW type test
//-----------------------------------------------------------------
void test_uartISR_withMicrochip()
{
    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_MICROCHIP;

    microchipMetering_uartISR_Expect();

    powerMonitor_uartISR();
}

//-----------------------------------------------------------------
// void powerMonitor_uartISR()
// ADI ADE9153A HW type test
//-----------------------------------------------------------------
void test_uartISR_withADI_ADE9153A()
{
    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_ADI_ADE9153A;

    ade9153aMetering_uartISR_Expect();

    powerMonitor_uartISR();
}


//-----------------------------------------------------------------
// void powerMonitor_getDataset()
// NULL pointer test
//-----------------------------------------------------------------
void test_getDatasetWithNullPointer()
{
    _DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, powerMonitor_getDataset(NULL));
}

//-----------------------------------------------------------------
// void powerMonitor_getDataset()
// valid pointer test
//-----------------------------------------------------------------
void test_getDatasetWithValidPointer()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset localDataSet;

    vTaskSuspendAll_Expect();
    xTaskResumeAll_ExpectAndReturn(0);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, powerMonitor_getDataset(&localDataSet));
}

//-----------------------------------------------------------------
// void powerMonitor_getDataset()
// data set copy validation
//-----------------------------------------------------------------
void test_getDatasetCopyTest()
{
    _DEBUG_OUT_TEST_NAME
    
    PowerCalcDataset localDataSet;

    localDataSet.accumulatedEnergy_20WhrUnits = 10;
    localDataSet.instantPowerFactor = 100.0F;
    localDataSet.instantIrms = 100.0F;
    localDataSet.instantPower = 100.0F;
    localDataSet.instantVrms = 100.0F;
    localDataSet.vrmsAverage = 100.0F;
    localDataSet.vrmsMax = 100.0F;
    localDataSet.vrmsMin = 0.0F;

    powerDataset.accumulatedEnergy_20WhrUnits = 120;
    powerDataset.instantPowerFactor = 4.0F;
    powerDataset.instantIrms = 3.0F;
    powerDataset.instantPower = 2.0F;
    powerDataset.instantVrms = 238.0F;
    powerDataset.vrmsAverage = 239.0F;
    powerDataset.vrmsMax = 240.0F;
    powerDataset.vrmsMin = 220.0F;
   
    vTaskSuspendAll_Ignore();
    xTaskResumeAll_IgnoreAndReturn(0);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, powerMonitor_getDataset(&localDataSet));

    TEST_ASSERT_EQUAL_UINT32(120, localDataSet.accumulatedEnergy_20WhrUnits);
    TEST_ASSERT_EQUAL_FLOAT(4.0F, localDataSet.instantPowerFactor);
    TEST_ASSERT_EQUAL_FLOAT(3.0F, localDataSet.instantIrms);
    TEST_ASSERT_EQUAL_FLOAT(2.0F, localDataSet.instantPower);
    TEST_ASSERT_EQUAL_FLOAT(238.0F, localDataSet.instantVrms);
    TEST_ASSERT_EQUAL_FLOAT(239.0F, localDataSet.vrmsAverage);
    TEST_ASSERT_EQUAL_FLOAT(240.0F, localDataSet.vrmsMax);
    TEST_ASSERT_EQUAL_FLOAT(220.0F, localDataSet.vrmsMin);
}

//-----------------------------------------------------------------
// void powerMonitor_isCalibrationActive()
// validate return of true
//-----------------------------------------------------------------
void test_isCalibrationActiveTrue()
{
    _DEBUG_OUT_TEST_NAME

    isCalibrationActive = true;

    TEST_ASSERT_TRUE( powerMonitor_isCalibrationActive());
}

//-----------------------------------------------------------------
// void powerMonitor_isCalibrationActive()
// validate return of false
//-----------------------------------------------------------------
void test_isCalibrationActiveFalse()
{
    _DEBUG_OUT_TEST_NAME

    isCalibrationActive = false;

    TEST_ASSERT_FALSE(powerMonitor_isCalibrationActive());
}

//-----------------------------------------------------------------
// void powerMonitor_getCalibReadings()
// NULL pointer test
//-----------------------------------------------------------------
void test_getCalibReadingsWithNullPointer()
{
    _DEBUG_OUT_TEST_NAME

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, powerMonitor_getCalibReadings(NULL));
}

//-----------------------------------------------------------------
// void powerMonitor_getCalibReadings()
// valid pointer test
//-----------------------------------------------------------------
void test_getCalibReadingsWithValidPointer()
{
    _DEBUG_OUT_TEST_NAME
    PowerCalcDataset localDataSet;

    vTaskSuspendAll_Expect();
    xTaskResumeAll_ExpectAndReturn(0);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, powerMonitor_getCalibReadings(&localDataSet));
}

//-----------------------------------------------------------------
// void powerMonitor_getCalibReadings()
// validation of dataset copy
//-----------------------------------------------------------------
void test_getCalibReadingsCopyTest()
{
    _DEBUG_OUT_TEST_NAME

    PowerCalcDataset localDataSet;

    localDataSet.accumulatedEnergy_20WhrUnits = 10;
    localDataSet.instantPowerFactor = 100.0F;
    localDataSet.instantIrms = 100.0F;
    localDataSet.instantPower = 100.0F;
    localDataSet.instantVrms = 100.0F;
    localDataSet.vrmsAverage = 100.0F;
    localDataSet.vrmsMax = 100.0F;
    localDataSet.vrmsMin = 0.0F;

    calibPowerDataset.accumulatedEnergy_20WhrUnits = 120;
    calibPowerDataset.instantPowerFactor = 4.0F;
    calibPowerDataset.instantIrms = 3.0F;
    calibPowerDataset.instantPower = 2.0F;
    calibPowerDataset.instantVrms = 238.0F;
    calibPowerDataset.vrmsAverage = 239.0F;
    calibPowerDataset.vrmsMax = 240.0F;
    calibPowerDataset.vrmsMin = 220.0F;

    vTaskSuspendAll_Ignore();
    xTaskResumeAll_IgnoreAndReturn(0);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, powerMonitor_getCalibReadings(&localDataSet));

    TEST_ASSERT_EQUAL_UINT32(120, localDataSet.accumulatedEnergy_20WhrUnits);
    TEST_ASSERT_EQUAL_FLOAT(4.0F, localDataSet.instantPowerFactor);
    TEST_ASSERT_EQUAL_FLOAT(3.0F, localDataSet.instantIrms);
    TEST_ASSERT_EQUAL_FLOAT(2.0F, localDataSet.instantPower);
    TEST_ASSERT_EQUAL_FLOAT(238.0F, localDataSet.instantVrms);
    TEST_ASSERT_EQUAL_FLOAT(239.0F, localDataSet.vrmsAverage);
    TEST_ASSERT_EQUAL_FLOAT(240.0F, localDataSet.vrmsMax);
    TEST_ASSERT_EQUAL_FLOAT(220.0F, localDataSet.vrmsMin);
}

//-----------------------------------------------------------------
// void powerMonitor_service() 
// HW type: stm8
// Service call returning : POWER_MONITOR_RESULT_OK
//-----------------------------------------------------------------
void test_service_withStm8_ResultOK()
{
    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_STM8;

    stm8Metering_service_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    powerMonitor_resultHandler_ExpectAnyArgs();

    powerMonitor_service();
}

//-----------------------------------------------------------------
// void powerMonitor_service() 
// HW type: Microchip
// Service call returning : POWER_MONITOR_RESULT_OK
//-----------------------------------------------------------------
void test_service_withMicrochip_ResultOK()
{
    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_MICROCHIP;
    
    microchipMetering_service_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    powerMonitor_resultHandler_ExpectAnyArgs();

    powerMonitor_service();
}


//-----------------------------------------------------------------
// void powerMonitor_service()
// Test with invalid HW type
//-----------------------------------------------------------------
void test_service_withInvalidDevice()
{
    _DEBUG_OUT_TEST_NAME

    hwType = 0xFF;

    powerMonitor_resultHandler_ExpectAnyArgs();

    powerMonitor_service();
}

//-----------------------------------------------------------------
// void powerMonitor_service() 
// HW type: Analog Devices ADE9153A
// Service call returning : POWER_MONITOR_RESULT_OK
//-----------------------------------------------------------------
void test_service_withADI_ADE9153A_ResultOK()
{
    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_ADI_ADE9153A;
  
    ade9153aMetering_service_ExpectAndReturn(POWER_MONITOR_RESULT_OK);

    powerMonitor_resultHandler_ExpectAnyArgs();

    powerMonitor_service();
}

//-----------------------------------------------------------------
// powerMonitor_Result powerMonitor_calibrate()
// Validate the calib active flag
//-----------------------------------------------------------------
void test_calibrate_validatecalibrationActiveFlag()
{
    _DEBUG_OUT_TEST_NAME
    powerMonitor_Result result;

    hwType = 0xFF;

    isCalibrationActive = true;

    powerMonitor_resetDataset_ExpectAnyArgs();

    set_sysmon_fault_Expect(PWR_ST_FAILED);

    result = powerMonitor_calibrate();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_IDLE,result);

    TEST_ASSERT_FALSE(isCalibrationActive);
}

//-----------------------------------------------------------------
// powerMonitor_Result powerMonitor_calibrate()
// HW type: stm8
// Validate stm8 calibration is called and PWR_ST_FAILED is cleared
//-----------------------------------------------------------------
void test_calibrate_withStm8_ResultOK()
{
    powerMonitor_Result result;
    
    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_STM8;

    powerMonitor_resetDataset_ExpectAnyArgs();

    stm8Metering_calibrate_ExpectAndReturn(&calibPowerDataset, POWER_MONITOR_RESULT_OK);

    clear_sysmon_fault_Expect(PWR_ST_FAILED);

    result = powerMonitor_calibrate();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// powerMonitor_Result powerMonitor_calibrate()
// HW type: stm8
// Validate stm8 calibration is called and PWR_ST_FAILED is set
//-----------------------------------------------------------------
void test_calibrate_withStm8_ResultCalibFailed()
{
    powerMonitor_Result result;

    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_STM8;

    powerMonitor_resetDataset_ExpectAnyArgs();

    stm8Metering_calibrate_ExpectAndReturn(&calibPowerDataset, POWER_MONITOR_RESULT_CALIBRATION_FAILED);

    set_sysmon_fault_Expect(PWR_ST_FAILED);

    result = powerMonitor_calibrate();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_CALIBRATION_FAILED, result);
}

//-----------------------------------------------------------------
// powerMonitor_Result powerMonitor_calibrate()
// HW type: Microchip
// Validate Microchip calibration is called and PWR_ST_FAILED is cleared
//-----------------------------------------------------------------
void test_calibrate_withMicrochip_ResultOK()
{
    powerMonitor_Result result;

    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_MICROCHIP;

    powerMonitor_resetDataset_ExpectAnyArgs();

    microchipMetering_calibrate_ExpectAndReturn(&calibPowerDataset, POWER_MONITOR_RESULT_OK);

    clear_sysmon_fault_Expect(PWR_ST_FAILED);

    result = powerMonitor_calibrate();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// powerMonitor_Result powerMonitor_calibrate()
// HW type: Microchip
// Validate Microchip calibration is called and PWR_ST_FAILED is set
//-----------------------------------------------------------------
void test_calibrate_withMicrochip_ResultCalibFailed()
{
    powerMonitor_Result result;

    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_MICROCHIP;

    powerMonitor_resetDataset_ExpectAnyArgs();

    microchipMetering_calibrate_ExpectAndReturn(&calibPowerDataset, POWER_MONITOR_RESULT_CALIBRATION_FAILED);

    set_sysmon_fault_Expect(PWR_ST_FAILED);

    result = powerMonitor_calibrate();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_CALIBRATION_FAILED, result);
}

//-----------------------------------------------------------------
// powerMonitor_Result powerMonitor_calibrate()
// HW type: Analog devices ADE9153A
// Validate Analog devices calibration is called and PWR_ST_FAILED is cleared
//-----------------------------------------------------------------
void test_calibrate_withADI_ADE9153A_ResultOK()
{
    powerMonitor_Result result;

    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_ADI_ADE9153A;

    powerMonitor_resetDataset_ExpectAnyArgs();

    ade9153aMetering_calibrate_ExpectAndReturn(&calibPowerDataset, POWER_MONITOR_RESULT_OK);

    clear_sysmon_fault_Expect(PWR_ST_FAILED);

    result = powerMonitor_calibrate();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}

//-----------------------------------------------------------------
// powerMonitor_Result powerMonitor_calibrate()
// HW type: Analog devices ADE9153A
// Validate Analog devices calibration is called and PWR_ST_FAILED is set
//-----------------------------------------------------------------
void test_calibrate_withADI_ADE9153A_ResultCalibFailed()
{
    powerMonitor_Result result;

    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_ADI_ADE9153A;

    powerMonitor_resetDataset_ExpectAnyArgs();

    ade9153aMetering_calibrate_ExpectAndReturn(&calibPowerDataset, POWER_MONITOR_RESULT_CALIBRATION_FAILED);

    set_sysmon_fault_Expect(PWR_ST_FAILED);

    result = powerMonitor_calibrate();

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_CALIBRATION_FAILED, result);
}

//-----------------------------------------------------------------
// powerMonitor_Result powerMonitor_applyCalibReadings()
// HW type: STM8
// Validate STM8 apply cal readings is called and returns OK
//-----------------------------------------------------------------
void test_applyCalibReadings_ResultOK()
{
    powerMonitor_Result result;

    _DEBUG_OUT_TEST_NAME

    PowerCalcDataset localDataSet;

    localDataSet.accumulatedEnergy_20WhrUnits = 10;
    localDataSet.instantPowerFactor = 100.0F;
    localDataSet.instantIrms = 100.0F;
    localDataSet.instantPower = 100.0F;
    localDataSet.instantVrms = 240.0F;
    localDataSet.vrmsAverage = 100.0F;
    localDataSet.vrmsMax = 240.0F;
    localDataSet.vrmsMin = 240.0F;

    hwType = POWER_MONITOR_HW_STM8;

    stm8Metering_applyCalibReadings_ExpectAndReturn( &localDataSet, POWER_MONITOR_RESULT_OK);

    result = powerMonitor_applyCalibReadings(&localDataSet);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_OK, result);
}


//-----------------------------------------------------------------
// powerMonitor_Result powerMonitor_applyCalibReadings()
// HW type: STM8
// Validate STM8 apply cal readings is called and returns PARAM error due to NULL pointer
//-----------------------------------------------------------------
void test_applyCalibReadings_ParamError()
{
    powerMonitor_Result result;

    _DEBUG_OUT_TEST_NAME

    hwType = POWER_MONITOR_HW_STM8;

    stm8Metering_applyCalibReadings_ExpectAndReturn(NULL, POWER_MONITOR_RESULT_PARAM_ERROR);

    result = powerMonitor_applyCalibReadings(NULL);

    TEST_ASSERT_EQUAL_UINT32(POWER_MONITOR_RESULT_PARAM_ERROR, result);
}

/*------------------------------ End of File -------------------------------*/
