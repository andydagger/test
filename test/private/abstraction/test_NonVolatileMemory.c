#include "unity.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "NonVolatileMemory.c"

#include "mock_debug.h"
#include "mock_config_queues.h"
#include "mock_FreeRTOS.h"
#include "mock_FreeRTOSConfig.h"
#include "mock_queue.h"
#include "mock_semphr.h"
#include "mock_hw.h"
#include "mock_config_eeprom.h"
#include "mock_CRC16.h"


#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
    debug_print_instant_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_NVM_initialise()
// Test the initialisation with a failed mutex creation
//-----------------------------------------------------------------
void test_NVM_initialise_InvalidMutex()
{
    _DEBUG_OUT_TEST_NAME
    NVM_result_t result;

    xQueueCreateMutex_ExpectAndReturn(queueQUEUE_TYPE_MUTEX, NULL);
 
    //debug_print_ExpectAnyArgs();

    result = NVM_initialise();

    TEST_ASSERT_EQUAL_UINT32(NVM_RESULT_ERROR_MUTEX, result);
}

//-----------------------------------------------------------------
// void test_NVM_initialise()
// Test the initialisation with a valid mutex creation
//-----------------------------------------------------------------
void test_NVM_initialise_MutexOK()
{
    _DEBUG_OUT_TEST_NAME
        NVM_result_t result;

    xQueueCreateMutex_ExpectAndReturn(queueQUEUE_TYPE_MUTEX, (void*)0x100);

    result = NVM_initialise();

    TEST_ASSERT_EQUAL_UINT32(NVM_RESULT_OK, result);
}


//-----------------------------------------------------------------
// NVM_read()
// test with CRC OK
//-----------------------------------------------------------------
void test_NVM_readCRC_OK()
{
    _DEBUG_OUT_TEST_NAME
    uint8_t testArray[10] = { 0x10,0x40,0x80,0x01,0x02,0x04,0x07,0xC0,0x90,0x0A };

    NVM_result_t result;
    // Make sure mutex is run
    nvmLock = (void*)0x100;

    xQueueGenericReceive_ExpectAnyArgsAndReturn( pdTRUE);
    
    eepromReadByte_ExpectAnyArgsAndReturn(10);
    eepromReadByte_ReturnArrayThruPtr_ptr(testArray, 10);
    
    CRC16_calculate_ExpectAndReturn(NVM_CRC16_SEED, testArray, 8, 0x900A);

    xQueueGenericSend_ExpectAnyArgsAndReturn(pdTRUE);

    result = NVM_read(0x100, testArray, 10);
    
    TEST_ASSERT_EQUAL_UINT32(NVM_RESULT_OK, result);
}

//-----------------------------------------------------------------
// NVM_read()
// test with CRC Invalid
//-----------------------------------------------------------------
void test_NVM_readCRC_Error()
{
    _DEBUG_OUT_TEST_NAME
        uint8_t testArray[10] = { 0x10,0x40,0x80,0x01,0x02,0x04,0x07,0xC0,0x90,0x0A };

    NVM_result_t result;
    // Make sure mutex is run
    nvmLock = (void*)0x100;

    xQueueGenericReceive_ExpectAnyArgsAndReturn(pdTRUE);

    eepromReadByte_ExpectAnyArgsAndReturn(10);
    eepromReadByte_ReturnArrayThruPtr_ptr(testArray, 10);
    
    CRC16_calculate_ExpectAndReturn(NVM_CRC16_SEED, testArray, 8, 0);

    xQueueGenericSend_ExpectAnyArgsAndReturn(pdTRUE);

    result = NVM_read(0x100, testArray, 10);

    TEST_ASSERT_EQUAL_UINT32(NVM_RESULT_ERROR_CRC, result);
}

//-----------------------------------------------------------------
// NVM_read()
// test with CRC Invalid
//-----------------------------------------------------------------
void test_NVM_read_NULLSemaphore()
{
    _DEBUG_OUT_TEST_NAME
        uint8_t testArray[10] = { 0x10,0x40,0x80,0x01,0x02,0x04,0x07,0xC0,0x90,0x0A };

    NVM_result_t result;
    // Make sure mutex is run
    nvmLock = (void*)NULL;

    eepromReadByte_ExpectAnyArgsAndReturn(10);
    eepromReadByte_ReturnArrayThruPtr_ptr(testArray, 10);

    CRC16_calculate_ExpectAndReturn(NVM_CRC16_SEED, testArray, 8, 0x900A);

    result = NVM_read(0x100, testArray, 10);

    TEST_ASSERT_EQUAL_UINT32(NVM_RESULT_OK, result);
}

//-----------------------------------------------------------------
// NVM_read()
// test with array length of 2 - bypassing the CRC calc
//-----------------------------------------------------------------
void test_NVM_read_ArrayLengthTwo()
{
    _DEBUG_OUT_TEST_NAME
    uint8_t testArray[10] = { 0x10,0x40 };

    NVM_result_t result;
    // Make sure mutex is run
    nvmLock = (void*)0x100;

    xQueueGenericReceive_ExpectAnyArgsAndReturn(pdTRUE);

    eepromReadByte_ExpectAnyArgsAndReturn(2);
    eepromReadByte_ReturnArrayThruPtr_ptr(testArray, 2);

    xQueueGenericSend_ExpectAnyArgsAndReturn(pdTRUE);

    result = NVM_read(0x100, testArray, 2);

    TEST_ASSERT_EQUAL_UINT32(NVM_RESULT_OK, result);
}


//-----------------------------------------------------------------
// NVM_write()
//-----------------------------------------------------------------
void test_NVM_write()
{
    _DEBUG_OUT_TEST_NAME
        uint8_t testArray[10] = { 0x10,0x40,0x80,0x01,0x02,0x04,0x07,0xC0,0x90,0x0A };

    // Make sure mutex is run
    nvmLock = (void*)0x100;

    xQueueGenericReceive_ExpectAnyArgsAndReturn(pdTRUE);

    CRC16_calculate_ExpectAndReturn(NVM_CRC16_SEED, testArray, 8, 0x900A);

    eepromWriteByte_ExpectWithArrayAndReturn(0x100,testArray, 10, 10, 10);
    
    xQueueGenericSend_ExpectAnyArgsAndReturn(pdTRUE);

    NVM_write(0x100, testArray, 10);

}

//-----------------------------------------------------------------
// NVM_write()
// NULL semaphore test
//-----------------------------------------------------------------
void test_NVM_write_NULLSemaphore()
{
    _DEBUG_OUT_TEST_NAME
        uint8_t testArray[10] = { 0x10,0x40,0x80,0x01,0x02,0x04,0x07,0xC0,0x90,0x0A };

    // Make sure mutex is run
    nvmLock = (void*)NULL;

    CRC16_calculate_ExpectAndReturn(NVM_CRC16_SEED, testArray, 8, 0x900A);

    eepromWriteByte_ExpectWithArrayAndReturn(0x100, testArray, 10, 10, 10);

    NVM_write(0x100, testArray, 10);

}

//-----------------------------------------------------------------
// NVM_write()
// test with array length of two -bypassing the CRC calc.
//-----------------------------------------------------------------
void test_NVM_write_ArrayLengthTwo()
{
    _DEBUG_OUT_TEST_NAME
        uint8_t testArray[2] = { 0x10,0x40 };

    // Make sure mutex is run
    nvmLock = (void*)0x100;

    xQueueGenericReceive_ExpectAnyArgsAndReturn(pdTRUE);

    eepromWriteByte_ExpectWithArrayAndReturn(0x100, testArray, 2, 2, 2);

    xQueueGenericSend_ExpectAnyArgsAndReturn(pdTRUE);

    NVM_write(0x100, testArray, 2);

}
