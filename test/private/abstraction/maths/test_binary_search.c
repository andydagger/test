#include <stdint.h>
#include <stdio.h>
#include "unity.h"
#include "binary_search.h"
#include "mock_debug.h"

// Check functions for binary_search()
static int32_t search_check_uint8(void* search_array, uint32_t index, void* ref);
static int32_t search_check_uint16(void* search_array, uint32_t index, void* ref);
static int32_t search_check_uint32(void* search_array, uint32_t index, void* ref);
static int32_t search_check_int8(void* search_array, uint32_t index, void* ref);
static int32_t search_check_int16(void* search_array, uint32_t index, void* ref);
static int32_t search_check_int32(void* search_array, uint32_t index, void* ref);

// prints the function name to STDIO
#define PRINT_FUNC_NAME() printf(" %s", __func__)



void setUp(void) {
    debugprint_Ignore();
}

void tearDown(void) {

}

//-----------------------------------------------------------------
// void binary_search()
// invalid params checks
//-----------------------------------------------------------------
void test_binary_search_ArrayPtrNull(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint8_t ref_value = 0;

    result = binary_search(NULL, &ref_value, 1, &search_check_uint8);

    TEST_ASSERT_EQUAL_INT32(-1, result);
}

void test_binary_search_RefPtrNull(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint8_t ref_value = 0;
    uint8_t array[10] = { 0 };

    result = binary_search(array, NULL, 10, &search_check_uint8);

    TEST_ASSERT_EQUAL_INT32(-1, result);
}

void test_binary_search_ElementCountZero(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint8_t ref_value = 0;
    uint8_t array[10] = { 0 };

    result = binary_search(array, &ref_value, 0, &search_check_uint8);

    TEST_ASSERT_EQUAL_INT32(-1, result);
}

void test_binary_search_NullCheckfunc(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint8_t ref_value = 0;
    uint8_t array[10] = { 0 };

    result = binary_search(array, &ref_value, 10, NULL);

    TEST_ASSERT_EQUAL_INT32(-1, result);
}


//-----------------------------------------------------------------
// void binary_search()
// Single byte array tests
//-----------------------------------------------------------------

void test_binary_search_SingleElementArrayNotFound(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint8_t ref_value = 20;
    uint8_t array[1] = { 1 };

    result = binary_search(array, &ref_value, 1, &search_check_uint8);

    TEST_ASSERT_EQUAL_INT32(-1, result);
}

void test_binary_search_SingleElementArrayIndex0(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint8_t ref_value = 20;
    uint8_t array[1] = { 20 };

    result = binary_search(array, &ref_value, 1, &search_check_uint8);

    TEST_ASSERT_EQUAL_INT32(0, result);
}



//-----------------------------------------------------------------
// void binary_search() uint8 search tests
//-----------------------------------------------------------------
void test_binary_search_uint8_ElementNotFound(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint8_t ref_value = 20;
    uint8_t array[10] = { 1,5,10,12,14,17,10,23,29,50 };

    result = binary_search(array, &ref_value, 10, &search_check_uint8);

    TEST_ASSERT_EQUAL_INT32(-1, result);
}

void test_binary_search_uint8_ElementIndex0(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint8_t ref_value = 1;
    uint8_t array[10] = { 1,5,10,12,14,17,10,23,29,50 };

    result = binary_search(array, &ref_value, 10, &search_check_uint8);

    TEST_ASSERT_EQUAL_INT32(0, result);
}

void test_binary_search_uint8_ElementIndex9(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint8_t ref_value = 50;
    uint8_t array[10] = { 1,5,10,12,14,17,10,23,29,50 };

    result = binary_search(array, &ref_value, 10, &search_check_uint8);

    TEST_ASSERT_EQUAL_INT32(9, result);
}

void test_binary_search_uint8_ElementIndex4(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint8_t ref_value = 14;
    uint8_t array[10] = { 1,5,10,12,14,17,10,23,29,50 };

    result = binary_search(array, &ref_value, 10, &search_check_uint8);

    TEST_ASSERT_EQUAL_INT32(4, result);
}

//-----------------------------------------------------------------
// void binary_search() uint16 search tests
//-----------------------------------------------------------------
void test_binary_search_uint16_ElementNotFound(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint16_t ref_value = 20;
    uint16_t array[10] = { 1,5,10,12,14,17,10,23,29,50 };

    result = binary_search(array, &ref_value, 10, &search_check_uint16);

    TEST_ASSERT_EQUAL_INT32(-1, result);
}

void test_binary_search_uint16_ElementIndex0(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint16_t ref_value = 1;
    uint16_t array[10] = { 1,5,10,12,14,17,10,23,29,50 };

    result = binary_search(array, &ref_value, 10, &search_check_uint16);

    TEST_ASSERT_EQUAL_INT32(0, result);
}

void test_binary_search_uint16_ElementIndex9(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint16_t ref_value = 50;
    uint16_t array[10] = { 1,5,10,12,14,17,10,23,29,50 };

    result = binary_search(array, &ref_value, 10, &search_check_uint16);

    TEST_ASSERT_EQUAL_INT32(9, result);
}

void test_binary_search_uint16_ElementIndex4(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint16_t ref_value = 14;
    uint16_t array[10] = { 1,5,10,12,14,17,10,23,29,50 };

    result = binary_search(array, &ref_value, 10, &search_check_uint16);

    TEST_ASSERT_EQUAL_INT32(4, result);
}

//-----------------------------------------------------------------
// void binary_search() uint32 tests 
//-----------------------------------------------------------------

void test_binary_search_uint32_ElementNotFound(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint32_t ref_value = 20;
    uint32_t array[10] = { 1,5,10,12,14,17,10,23,29,50 };

    result = binary_search(array, &ref_value, 10, &search_check_uint32);

    TEST_ASSERT_EQUAL_INT32(-1, result);
}

void test_binary_search_uint32_ElementIndex0(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint32_t ref_value = 1;
    uint32_t array[10] = { 1,5,10,12,14,17,10,23,29,50 };

    result = binary_search(array, &ref_value, 10, &search_check_uint32);

    TEST_ASSERT_EQUAL_INT32(0, result);
}

void test_binary_search_uint32_ElementIndex9(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint32_t ref_value = 50;
    uint32_t array[10] = { 1,5,10,12,14,17,10,23,29,50 };

    result = binary_search(array, &ref_value, 10, &search_check_uint32);

    TEST_ASSERT_EQUAL_INT32(9, result);
}

void test_binary_search_uint32_ElementIndex4(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    uint32_t ref_value = 14;
    uint32_t array[10] = { 1,5,10,12,14,17,10,23,29,50 };

    result = binary_search(array, &ref_value, 10, &search_check_uint32);

    TEST_ASSERT_EQUAL_INT32(4, result);
}

//-----------------------------------------------------------------
// void binary_search() int8
//-----------------------------------------------------------------

void test_binary_search_int8_ElementNotFound(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    int8_t ref_value = 20;
    int8_t array[10] = { -20,-10,-5,-1, 0, 4,17,10,23,29 };

    result = binary_search(array, &ref_value, 10, &search_check_int8);

    TEST_ASSERT_EQUAL_INT32(-1, result);
}

void test_binary_search_int8_ElementIndex0(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    int8_t ref_value = -20;
    int8_t array[10] = { -20,-10,-5,-1, 0, 4,17,10,23,29 };

    result = binary_search(array, &ref_value, 10, &search_check_int8);

    TEST_ASSERT_EQUAL_INT32(0, result);
}

void test_binary_search_int8_ElementIndex9(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    int8_t ref_value = 29;
    int8_t array[10] = { -20,-10,-5,-1, 0, 4,17,10,23,29 };

    result = binary_search(array, &ref_value, 10, &search_check_int8);

    TEST_ASSERT_EQUAL_INT32(9, result);
}

void test_binary_search_int8_ElementIndex4(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    int8_t ref_value = 0;
    int8_t array[10] = { -20,-10,-5,-1, 0, 4,17,10,23,29 };

    result = binary_search(array, &ref_value, 10, &search_check_int8);

    TEST_ASSERT_EQUAL_INT32(4, result);
}

//-----------------------------------------------------------------
// void binary_search() int16
//-----------------------------------------------------------------

void test_binary_search_int16_ElementNotFound(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    int16_t ref_value = 20;
    int16_t array[10] = { -20,-10,-5,-1, 0, 4,17,10,23,29 };

    result = binary_search(array, &ref_value, 10, &search_check_int16);

    TEST_ASSERT_EQUAL_INT32(-1, result);
}

void test_binary_search_int16_ElementIndex0(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    int16_t ref_value = -20;
    int16_t array[10] = { -20,-10,-5,-1, 0, 4,17,10,23,29 };

    result = binary_search(array, &ref_value, 10, &search_check_int16);

    TEST_ASSERT_EQUAL_INT32(0, result);
}

void test_binary_search_int16_ElementIndex9(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    int16_t ref_value = 29;
    int16_t array[10] = { -20,-10,-5,-1, 0, 4,17,10,23,29 };

    result = binary_search(array, &ref_value, 10, &search_check_int16);

    TEST_ASSERT_EQUAL_INT32(9, result);
}

void test_binary_search_int16_ElementIndex4(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    int16_t ref_value = 0;
    int16_t array[10] = { -20,-10,-5,-1, 0, 4,17,10,23,29 };

    result = binary_search(array, &ref_value, 10, &search_check_int16);

    TEST_ASSERT_EQUAL_INT32(4, result);
}

//-----------------------------------------------------------------
// void binary_search() int32 
//-----------------------------------------------------------------

void test_binary_search_int32_ElementNotFound(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    int32_t ref_value = 20;
    int32_t array[10] = { -20,-10,-5,-1, 0, 4,17,10,23,29 };

    result = binary_search(array, &ref_value, 10, &search_check_int32);

    TEST_ASSERT_EQUAL_INT32(-1, result);
}

void test_binary_search_int32_ElementIndex0(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    int32_t ref_value = -20;
    int32_t array[10] = { -20,-10,-5,-1, 0, 4,17,10,23,29 };

    result = binary_search(array, &ref_value, 10, &search_check_int32);

    TEST_ASSERT_EQUAL_INT32(0, result);
}

void test_binary_search_int32_ElementIndex9(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    int32_t ref_value = 29;
    int32_t array[10] = { -20,-10,-5,-1, 0, 4,17,10,23,29 };

    result = binary_search(array, &ref_value, 10, &search_check_int32);

    TEST_ASSERT_EQUAL_INT32(9, result);
}

void test_binary_search_int32_ElementIndex4(void) {
    PRINT_FUNC_NAME();
    int32_t result;
    int32_t ref_value = 0;
    int32_t array[10] = { -20,-10,-5,-1, 0, 4,17,10,23,29 };

    result = binary_search(array, &ref_value, 10, &search_check_int32);

    TEST_ASSERT_EQUAL_INT32(4, result);
}

/*!
    \brief      uint8_t comparison function

    \param      *search_array   Pointer to a sorted array of elements to search
    \param      index           The index in the array to compare
    \param      *ref            Pointer to the reference object to find

    \return     The result of the comparison.
*/
static int32_t search_check_uint8(void *search_array, uint32_t index, void *ref) {

    if ((search_array == NULL) || (ref == NULL))
    {
        return -1;
    }

    uint8_t ref_value = *(uint8_t*)ref;
    uint8_t* array_ptr = (uint8_t*)search_array;

    return (int32_t)(array_ptr[index] - ref_value);
}

/*!
    \brief      uint16_t comparison function

    \param      *search_array   Pointer to a sorted array of elements to search
    \param      index               The index in the array to compare
    \param      *ref            Pointer to the reference object to find

    \return     The result of the comparison.
*/
static int32_t search_check_uint16(void *search_array, uint32_t index, void *ref) {

    if ((search_array == NULL) || (ref == NULL))
    {
        return -1;
    }

    uint16_t ref_value = *(uint16_t*)ref;
    uint16_t* array_ptr = (uint16_t*)search_array;

    return (int32_t)(array_ptr[index] - ref_value);
}

/*!
    \brief      uint32_t comparison function

    \param      *search_array   Pointer to a sorted array of elements to search
    \param      index           The index in the array to compare
    \param      *ref            Pointer to the reference object to find

    \return     The result of the comparison.
*/
static int32_t search_check_uint32(void *search_array, uint32_t index, void *ref) {

    if ((search_array == NULL) || (ref == NULL))
    {
        return -1;
    }

    uint32_t ref_value = *(uint32_t*)ref;
    uint32_t* array_ptr = (uint32_t*)search_array;

    return (int32_t)(array_ptr[index] - ref_value);
}

/*!
    \brief      int8_t comparison function

    \param      *search_array   Pointer to a sorted array of elements to search
    \param      index           The index in the array to compare
    \param      *ref            Pointer to the reference object to find

    \return     The result of the comparison.
*/
static int32_t search_check_int8(void *search_array, uint32_t index, void *ref) {

    if ((search_array == NULL) || (ref == NULL))
    {
        return -1;
    }

    int8_t ref_value = *(int8_t*)ref;
    int8_t* array_ptr = (int8_t*)search_array;

    return (int32_t)(array_ptr[index] - ref_value);
}

/*!
    \brief      int16_t comparison function

    \param      *search_array   Pointer to a sorted array of elements to search
    \param      index           The index in the array to compare
    \param      *ref            Pointer to the reference object to find

    \return     The result of the comparison.
*/
static int32_t search_check_int16(void *search_array, uint32_t index, void *ref) {

    if ((search_array == NULL) || (ref == NULL))
    {
        return -1;
    }

    int16_t ref_value = *(int16_t*)ref;
    int16_t* array_ptr = (int16_t*)search_array;

    return (int32_t)(array_ptr[index] - ref_value);
}

/*!
    \brief      int32_t comparison function

    \param      *search_array   Pointer to a sorted array of elements to search
    \param      index           The index in the array to compare
    \param      *ref            Pointer to the reference object to find

    \return     The result of the comparison.
*/
static int32_t search_check_int32(void *search_array, uint32_t index, void *ref) {

    if ((search_array == NULL) || (ref == NULL))
    {
        return -1;
    }

    int32_t ref_value = *(int32_t*)ref;
    int32_t* array_ptr = (int32_t*)search_array;

    return (int32_t)(array_ptr[index] - ref_value);
}