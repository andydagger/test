#undef LORA_MICROCHIP

#include "unity.h"
#include "nwk_tek.c"
#include "mock_lorawan.h"
#include "mock_lorawaninit.h"
#include "mock_lorawanhw.h"
#include "mock_Murata.h"
#include "mock_loraDriver.h"
#include "mock_LorawanStatusFlags.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);
#define ALLOW_SESSION_RECOVERY true
#define FORCE_SESSION_RESET false

bool requestToRecoverSession = ALLOW_SESSION_RECOVERY;

void setUp(void)
{
    radioTekIsMicrochip = false;
}
void tearDown(void)
{

}


//-----------------------------------------------------------------

void test_init_autoDetect()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = false;
    Murata_Init_ExpectAnyArgsAndReturn(false);
    lorawan_init_ExpectAnyArgsAndReturn(true);
    TEST_ASSERT_TRUE(nwk_tek_init(&requestToRecoverSession));
    TEST_ASSERT_TRUE(radioTekIsMicrochip);
}

void test_Microchip_init_okWithSessionRestore()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    lorawan_init_ExpectAndReturn(&requestToRecoverSession, true);
    TEST_ASSERT_TRUE(nwk_tek_init(&requestToRecoverSession));
}
void test_Microchip_init_FailedToRestoreSession()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    lorawan_init_ExpectAndReturn(&requestToRecoverSession, true);
    TEST_ASSERT_TRUE(nwk_tek_init(&requestToRecoverSession));
}
void test_Microchip_init_failWithSessionRestore()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    lorawan_init_ExpectAndReturn(&requestToRecoverSession, false);
    TEST_ASSERT_FALSE(nwk_tek_init(&requestToRecoverSession));
}
void test_Microchip_init_okWithoutSessionRestore()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    requestToRecoverSession = FORCE_SESSION_RESET;
    lorawan_init_ExpectAndReturn(&requestToRecoverSession, true);
    TEST_ASSERT_TRUE(nwk_tek_init(&requestToRecoverSession));
}
void test_Microchip_init_failWithoutSessionRestore()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    requestToRecoverSession = FORCE_SESSION_RESET;
    lorawan_init_ExpectAndReturn(&requestToRecoverSession, false);
    TEST_ASSERT_FALSE(nwk_tek_init(&requestToRecoverSession));
}


void test_Murata_init_okWithSessionRestore()
{
_DEBUG_OUT_TEST_NAME

    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    Murata_Init_ExpectAndReturn(_EU868,&requestToRecoverSession, true);
    TEST_ASSERT_TRUE(nwk_tek_init(&requestToRecoverSession));
}
void test_Murata_init_failWithSessionRestore()
{
_DEBUG_OUT_TEST_NAME

    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    Murata_Init_ExpectAndReturn(_EU868,&requestToRecoverSession, false);
    lorawan_init_ExpectAnyArgsAndReturn(false);
    TEST_ASSERT_FALSE(nwk_tek_init(&requestToRecoverSession));
}
void test_Murata_init_okWithoutSessionRestore()
{
_DEBUG_OUT_TEST_NAME

    requestToRecoverSession = FORCE_SESSION_RESET;
    Murata_Init_ExpectAndReturn(_EU868,&requestToRecoverSession, true);
    TEST_ASSERT_TRUE(nwk_tek_init(&requestToRecoverSession));
}
void test_Murata_init_failWithoutSessionRestore()
{
_DEBUG_OUT_TEST_NAME

    requestToRecoverSession = FORCE_SESSION_RESET;
    Murata_Init_ExpectAndReturn(_EU868,&requestToRecoverSession, false);
    lorawan_init_ExpectAnyArgsAndReturn(false);
    TEST_ASSERT_FALSE(nwk_tek_init(&requestToRecoverSession));
}



//-----------------------------------------------------------------

void test_Microchip_join_okFromNotJoined()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_join_network_ExpectAndReturn(false, true);
    TEST_ASSERT_TRUE(nwk_tek_connect(false));
}
void test_Microchip_join_failFromNotJoined()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_join_network_ExpectAndReturn(false, false);
    TEST_ASSERT_FALSE(nwk_tek_connect(false));
}
void test_Microchip_join_okSessionRestore()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_join_network_ExpectAndReturn(true, true);
    TEST_ASSERT_TRUE(nwk_tek_connect(true));
}
void test_Microchip_join_failSessionRestore()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_join_network_ExpectAndReturn(true, false);
    TEST_ASSERT_FALSE(nwk_tek_connect(true));
}

void test_Murata_join_okFromNotJoined()
{
_DEBUG_OUT_TEST_NAME

    Murata_JoinNetwork_ExpectAndReturn(_EU868, false, true);
    TEST_ASSERT_TRUE(nwk_tek_connect(false));
}
void test_Murata_join_failFromNotJoined()
{
_DEBUG_OUT_TEST_NAME

    Murata_JoinNetwork_ExpectAndReturn(_EU868, false, false);
    TEST_ASSERT_FALSE(nwk_tek_connect(false));
}

//-----------------------------------------------------------------

void test_Microchip_send_msg_confirmedOk()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_send_msg_ExpectAndReturn("MSG\r\n", true, true);
    TEST_ASSERT_TRUE(nwk_tek_send_msg("MSG\r\n", true));
}
void test_Microchip_send_msg_confirmedFail()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_send_msg_ExpectAndReturn("MSG\r\n", true, false);
    TEST_ASSERT_FALSE(nwk_tek_send_msg("MSG\r\n", true));
}
void test_Microchip_send_msg_unconfirmedOk()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_send_msg_ExpectAndReturn("MSG\r\n", false, true);
    TEST_ASSERT_TRUE(nwk_tek_send_msg("MSG\r\n", false));
}
void test_Microchip_send_msg_unconfirmedFail()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_send_msg_ExpectAndReturn("MSG\r\n", false, false);
    TEST_ASSERT_FALSE(nwk_tek_send_msg("MSG\r\n", false));
}


void test_Murata_send_msg_confirmedOk()
{
_DEBUG_OUT_TEST_NAME

    Murata_SendMsg_ExpectAndReturn("MSG\r\n", true, CMD_SUCCESSFUL);
    TEST_ASSERT_TRUE(nwk_tek_send_msg("MSG\r\n", true));
}
void test_Murata_send_msg_confirmedFailed()
{
_DEBUG_OUT_TEST_NAME

    Murata_SendMsg_ExpectAndReturn("MSG\r\n", true, CMD_FAILED);
    TEST_ASSERT_FALSE(nwk_tek_send_msg("MSG\r\n", true));
}
void test_Murata_send_msg_confirmedPoorLink()
{
_DEBUG_OUT_TEST_NAME

    Murata_SendMsg_ExpectAndReturn("MSG\r\n", true, POOR_LINK);
    LorawanStatusFlags_SetModuleSettingsNeedOptimising_Expect();
    TEST_ASSERT_FALSE(nwk_tek_send_msg("MSG\r\n", true));
}
void test_Murata_send_msg_confirmedLostConnection()
{
_DEBUG_OUT_TEST_NAME

    Murata_SendMsg_ExpectAndReturn("MSG\r\n", true, LOST_CONNECTION);
    LorawanStatusFlags_ClearJoinedFlag_Expect();
    TEST_ASSERT_FALSE(nwk_tek_send_msg("MSG\r\n", true));
}
void test_Murata_send_msg_confirmedModuleNotResponding()
{
_DEBUG_OUT_TEST_NAME

    Murata_SendMsg_ExpectAndReturn("MSG\r\n", true, MOD_NOT_RESP);
    LorawanStatusFlags_SetModuleNeedsInitialising_Expect();
    TEST_ASSERT_FALSE(nwk_tek_send_msg("MSG\r\n", true));
}

void test_Murata_send_msg_unconfirmedOk()
{
_DEBUG_OUT_TEST_NAME

    Murata_SendMsg_ExpectAndReturn("MSG\r\n", false, CMD_SUCCESSFUL);
    TEST_ASSERT_TRUE(nwk_tek_send_msg("MSG\r\n", false));
}
void test_Murata_send_msg_unconfirmedFailed()
{
_DEBUG_OUT_TEST_NAME

    Murata_SendMsg_ExpectAndReturn("MSG\r\n", false, CMD_FAILED);
    TEST_ASSERT_FALSE(nwk_tek_send_msg("MSG\r\n", false));
}
void test_Murata_send_msg_unconfirmedPoorLink()
{
_DEBUG_OUT_TEST_NAME

    Murata_SendMsg_ExpectAndReturn("MSG\r\n", false, POOR_LINK);
    LorawanStatusFlags_SetModuleSettingsNeedOptimising_Expect();
    TEST_ASSERT_FALSE(nwk_tek_send_msg("MSG\r\n", false));
}
void test_Murata_send_msg_unconfirmedLostConnection()
{
_DEBUG_OUT_TEST_NAME

    Murata_SendMsg_ExpectAndReturn("MSG\r\n", false, LOST_CONNECTION);
    LorawanStatusFlags_ClearJoinedFlag_Expect();
    TEST_ASSERT_FALSE(nwk_tek_send_msg("MSG\r\n", false));
}
void test_Murata_send_msg_unconfirmedModuleNotResponding()
{
_DEBUG_OUT_TEST_NAME

    Murata_SendMsg_ExpectAndReturn("MSG\r\n", false, MOD_NOT_RESP);
    LorawanStatusFlags_SetModuleNeedsInitialising_Expect();
    TEST_ASSERT_FALSE(nwk_tek_send_msg("MSG\r\n", false));
}

//-----------------------------------------------------------------

void test_Microchip_read_msg()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_read_msg_ExpectAndReturn("STRING\r\n");
    TEST_ASSERT_EQUAL_STRING("STRING\r\n", nwk_tek_read_msg());
}

void test_Murata_read_msg()
{
_DEBUG_OUT_TEST_NAME

    Murata_ReadMsg_ExpectAndReturn("STRING\r\n");
    TEST_ASSERT_EQUAL_STRING("STRING\r\n", nwk_tek_read_msg());
}

//-----------------------------------------------------------------

void test_Microchip_enableMcastOk()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_update_mcast_state_ExpectAndReturn(true, true);
    TEST_ASSERT_TRUE(nwk_tek_update_broabcast_status(true));
}
void test_Microchip_enableMcastFail()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_update_mcast_state_ExpectAndReturn(true, false);
    TEST_ASSERT_FALSE(nwk_tek_update_broabcast_status(true));
}
void test_Microchip_disableeMcastOk()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_update_mcast_state_ExpectAndReturn(false, true);
    TEST_ASSERT_TRUE(nwk_tek_update_broabcast_status(false));
}
void test_Microchip_disableMcastFail()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_update_mcast_state_ExpectAndReturn(false, false);
    TEST_ASSERT_FALSE(nwk_tek_update_broabcast_status(false));
}

void test_Murata_enableMcastOk()
{
_DEBUG_OUT_TEST_NAME

    Murata_UpdateMcastState_ExpectAndReturn(true, true);
    TEST_ASSERT_TRUE(nwk_tek_update_broabcast_status(true));
}
void test_Murata_enableMcastFail()
{
_DEBUG_OUT_TEST_NAME

    Murata_UpdateMcastState_ExpectAndReturn(true, false);
    TEST_ASSERT_FALSE(nwk_tek_update_broabcast_status(true));
}
void test_Murata_disableeMcastOk()
{
_DEBUG_OUT_TEST_NAME

    Murata_UpdateMcastState_ExpectAndReturn(false, true);
    TEST_ASSERT_TRUE(nwk_tek_update_broabcast_status(false));
}
void test_Murata_disableMcastFail()
{
_DEBUG_OUT_TEST_NAME

    Murata_UpdateMcastState_ExpectAndReturn(false, false);
    TEST_ASSERT_FALSE(nwk_tek_update_broabcast_status(false));
}

//-----------------------------------------------------------------

void test_Microchip_save_session_info()
{
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_save_session_info_Expect();
    nwk_tek_save_session_info();
}
void test_Murata_save_session_info()
{
_DEBUG_OUT_TEST_NAME
    Murata_SaveSessionInfo_Expect();
    nwk_tek_save_session_info();
}
//-----------------------------------------------------------------
void test_Microchip_received_string_pending_true(){
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(true);
    TEST_ASSERT_TRUE(get_nwk_tek_rx_string_is_waiting());
}
void test_Microchip_received_string_pending_false(){
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    get_lorawanhw_rx_string_waiting_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(get_nwk_tek_rx_string_is_waiting());
}

void test_Murata_received_string_pending_true(){
_DEBUG_OUT_TEST_NAME

    Murata_getDownlinkPayloadIsWaiting_ExpectAndReturn(true);
    TEST_ASSERT_TRUE(get_nwk_tek_rx_string_is_waiting());
}
void test_Murata_received_string_pending_false(){
_DEBUG_OUT_TEST_NAME

    Murata_getDownlinkPayloadIsWaiting_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(get_nwk_tek_rx_string_is_waiting());
}

// //-----------------------------------------------------------------

void test_Microchip_optimise_settings_couldBeOptimised(){
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_optimise_settings_ExpectAndReturn(true);
    TEST_ASSERT_TRUE(nwk_tek_optimise_settings());
}
void test_Microchip_optimise_settings_couldNotBeOptimised(){
_DEBUG_OUT_TEST_NAME
    radioTekIsMicrochip = true;
    lorawan_optimise_settings_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(nwk_tek_optimise_settings());
}

void test_Murata_optimise_settings_couldBeOptimised(){
_DEBUG_OUT_TEST_NAME

    Murata_SetBestSettings_ExpectAndReturn(true);
    TEST_ASSERT_TRUE(nwk_tek_optimise_settings());
}
void test_Murata_optimise_settings_couldNotBeOptimised(){
_DEBUG_OUT_TEST_NAME

    Murata_SetBestSettings_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(nwk_tek_optimise_settings());
}

// //-----------------------------------------------------------------

//-----------------------------------------------------------------
void test_get_deveui_Murata()
{
_DEBUG_OUT_TEST_NAME
    char DevEUi[17]={0};
    radioTekIsMicrochip = false;
    Murata_getDevEUi_Expect(DevEUi);
    get_nwk_tek_dev_eui(DevEUi);
}
void test_get_deveui_Microchip()
{
_DEBUG_OUT_TEST_NAME
    char DevEUi[17]={0};
    radioTekIsMicrochip = true;
    get_lorawan_dev_eui_Expect(DevEUi);
    get_nwk_tek_dev_eui(DevEUi);
}
