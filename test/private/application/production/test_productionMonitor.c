#include "unity.h"
#include "string.h"
#include "stdio.h"
#include "productionMonitor.c"

#include "mock_productionMonitor_impl.h"
#include "mock_supplyMonitor.h"
#include "mock_nwk_com.h"
#include "mock_systick.h"
#include "mock_gps.h"
#include "mock_powerMonitor.h"
#include "mock_compileUplink.h"
#include "mock_debug.h"
#include "mock_sysmon.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
    memset(radioDevEui,0, sizeof radioDevEui);
}
void tearDown(void)
{

}

//-----------------------------------------------------------------
// void productionMonitor_checkIfUnitIsUnderTest(void);
//-----------------------------------------------------------------

void test_checkIfUnitIsUnderTest_keysArePresent(void)
{
_DEBUG_OUT_TEST_NAME
    unitIsUnderTest = true;
    readByteFromKeysSection_IgnoreAndReturn(0x12);
    productionMonitor_checkIfUnitIsUnderTest();
    TEST_ASSERT_FALSE(unitIsUnderTest);
}

void test_checkIfUnitIsUnderTest_singleByteNotBlank(void)
{
_DEBUG_OUT_TEST_NAME
    unitIsUnderTest = true;
    readByteFromKeysSection_ExpectAndReturn(1, 0xFF);
    readByteFromKeysSection_ExpectAndReturn(2, 0xFF);
    readByteFromKeysSection_ExpectAndReturn(3, 0xFF);
    readByteFromKeysSection_ExpectAndReturn(4, 0xFF);
    readByteFromKeysSection_ExpectAndReturn(5, 0xFF);
    readByteFromKeysSection_ExpectAndReturn(6, 0xFE);
    readByteFromKeysSection_IgnoreAndReturn(0xFF);
    productionMonitor_checkIfUnitIsUnderTest();
    TEST_ASSERT_FALSE(unitIsUnderTest);
}

void test_checkIfUnitIsUnderTest_keysAreBlank(void)
{
_DEBUG_OUT_TEST_NAME
    unitIsUnderTest = false;
    readByteFromKeysSection_IgnoreAndReturn(0xFF);
    productionMonitor_checkIfUnitIsUnderTest();
    TEST_ASSERT_TRUE(unitIsUnderTest);
}

//-----------------------------------------------------------------
// bool productionMonitor_getUnitIsUndertest(void)
//-----------------------------------------------------------------

void test_getUnitIsUnderTest_true(void)
{
_DEBUG_OUT_TEST_NAME
    unitIsUnderTest = true;
    TEST_ASSERT_TRUE(productionMonitor_getUnitIsUndertest());
}

void test_getUnitIsUnderTest_false(void)
{
_DEBUG_OUT_TEST_NAME
    unitIsUnderTest = false;
    TEST_ASSERT_FALSE(productionMonitor_getUnitIsUndertest());
}

//-----------------------------------------------------------------
// void productionMonitor_service(void)
//-----------------------------------------------------------------

void test_service_IDLE_stateChangeReportOverDali(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_12V_SUPPLY;
    prevProdMonState = PROD_IDLE;
    productionMonitor_sendNewStateOverDali_Expect(prodMonState);
    supplyMonitor_get12vRailIsReady_IgnoreAndReturn(false);
    productionMonitor_service();

}

void test_service_IDLE_to_WAIT_FOR_SUPPLY(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_IDLE;
    prevProdMonState = prodMonState;
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_12V_SUPPLY, prodMonState);
}
//----------------------------------
void test_service_WAIT_FOR_SUPPLY_to_WAIT_FOR_DEV_EUI(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_12V_SUPPLY;
    prevProdMonState = prodMonState;
    supplyMonitor_get12vRailIsReady_ExpectAndReturn(true);
    productionMonitor_enableDaliBusForComs_Expect();
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_DEVEUI, prodMonState);
}

void test_service_WAIT_FOR_SUPPLY(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_12V_SUPPLY;
    prevProdMonState = prodMonState;
    supplyMonitor_get12vRailIsReady_ExpectAndReturn(false);
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_12V_SUPPLY, prodMonState);
}
//----------------------------------

void test_service_WAIT_FOR_DEV_EUI_to_PROD_WHYTELIST_DELAY(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_DEVEUI;
    prevProdMonState = prodMonState;
    delayStateRefTimeMs = 0;
    char devEUI[17] = {0};
    sprintf(devEUI, "1122334455667788");
    get_nwk_com_dev_eui_Expect(devEUI);
    get_nwk_com_dev_eui_IgnoreArg_p_devEuiString();
    get_nwk_com_dev_eui_ReturnArrayThruPtr_p_devEuiString(devEUI, strlen(devEUI));
    productionMonitor_sendDevEuiOverDali_Expect(devEUI);
    get_systick_ms_ExpectAndReturn(1234);
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WHYTELIST_DELAY, prodMonState);
    TEST_ASSERT_EQUAL_UINT32(1234, delayStateRefTimeMs);
}

void test_service_WAIT_FOR_DEV_EUI_incompleteString(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_DEVEUI;
    prevProdMonState = prodMonState;
    char devEUI[17] = {0};
    sprintf(devEUI, "11223344556677");
    get_nwk_com_dev_eui_Expect(devEUI);
    get_nwk_com_dev_eui_IgnoreArg_p_devEuiString();
    get_nwk_com_dev_eui_ReturnArrayThruPtr_p_devEuiString(devEUI, strlen(devEUI));
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_DEVEUI, prodMonState);
}

void test_service_WAIT_FOR_DEV_EUI_noString(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_DEVEUI;
    prevProdMonState = prodMonState;
    char devEUI[17] = {0};
    get_nwk_com_dev_eui_Expect(devEUI);
    get_nwk_com_dev_eui_IgnoreArg_p_devEuiString();
    get_nwk_com_dev_eui_ReturnArrayThruPtr_p_devEuiString(devEUI, strlen(devEUI));
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_DEVEUI, prodMonState);
}
//----------------------------------
void test_service_PROD_WHYTELIST_DELAY_to_WAIT_FOR_GPS(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WHYTELIST_DELAY;
    prevProdMonState = prodMonState;
    delayStateRefTimeMs = 0;
    networkReadyForDevice = false;
    get_systick_ms_ExpectAndReturn(PROD_WHITELIST_ENTRY_DELAY_MS + 1);
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_GPS, prodMonState);
    TEST_ASSERT_TRUE(networkReadyForDevice);
}

void test_service_PROD_WHYTELIST_DELAY(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WHYTELIST_DELAY;
    prevProdMonState = prodMonState;
    delayStateRefTimeMs = 0;
    get_systick_ms_ExpectAndReturn(PROD_WHITELIST_ENTRY_DELAY_MS - 1);
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WHYTELIST_DELAY, prodMonState);
}
//----------------------------------
void test_service_WAIT_FOR_GPS_to_WAIT_FOR_CAL(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_GPS;
    prevProdMonState = prodMonState;
    gps_verifyTimeStringReceptionIsDone_ExpectAndReturn(true);
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_PWR_CAL, prodMonState);
}

void test_service_WAIT_FOR_GPS(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_GPS;
    prevProdMonState = prodMonState;
    gps_verifyTimeStringReceptionIsDone_ExpectAndReturn(false);
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_GPS, prodMonState);
}
//----------------------------------
void test_service_WAIT_FOR_CAL_to_WAIT_FOR_REF_READINGS(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_PWR_CAL;
    prevProdMonState = prodMonState;
    powerMonitor_isCalibrationActive_ExpectAndReturn(false);
    get_sysmon_self_test_is_done_ExpectAndReturn(true);
    compileUplink_ProductionTestReport_Expect();
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_REF_READINGS, prodMonState);
}
//----------------------------------
void test_service_WAIT_FOR_CAL_SelfTestNotFinished(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_PWR_CAL;
    prevProdMonState = prodMonState;
    powerMonitor_isCalibrationActive_IgnoreAndReturn(false);
    get_sysmon_self_test_is_done_IgnoreAndReturn(false);
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_PWR_CAL, prodMonState);
}

void test_service_WAIT_FOR_CAL_PowerCalNotFinished(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_PWR_CAL;
    prevProdMonState = prodMonState;
    powerMonitor_isCalibrationActive_IgnoreAndReturn(true);
    get_sysmon_self_test_is_done_IgnoreAndReturn(true);
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_PWR_CAL, prodMonState);
}
//----------------------------------
void test_service_WAIT_FOR_REF_READINGS_to_WAIT_FOR_NODE_INFO(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_REF_READINGS;
    prevProdMonState = prodMonState;
    PowerCalcDataset refReadings;
    receivedCalibrationReferenceReadings = false;
    powerMonitor_applyCalibReadings_IgnoreAndReturn(POWER_MONITOR_RESULT_OK);
    productionMonitor_calibrateReadings(&refReadings);
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_NODE_INFO, prodMonState);
}

void test_service_WAIT_FOR_REF_READINGS(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_REF_READINGS;
    prevProdMonState = prodMonState;
    receivedCalibrationReferenceReadings = false;
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_REF_READINGS, prodMonState);
}
//----------------------------------
void test_service_WAIT_FOR_NODE_INFO_to_READY(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_NODE_INFO;
    prevProdMonState = prodMonState;
    receivedNodeInfo = true;
    productionMonitor_checkAndSaveNodeInfo_ExpectAnyArgsAndReturn(true);
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_NODE_READY, prodMonState);
}

void test_service_WAIT_FOR_NODE_INFO_to_ERROR(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_NODE_INFO;
    prevProdMonState = prodMonState;
    receivedNodeInfo = true;
    productionMonitor_checkAndSaveNodeInfo_ExpectAnyArgsAndReturn(false);
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_ERROR, prodMonState);
}

void test_service_WAIT_FOR_NODE_INFO(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_WAIT_FOR_NODE_INFO;
    prevProdMonState = prodMonState;
    receivedNodeInfo = false;
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_WAIT_FOR_NODE_INFO, prodMonState);
}
//----------------------------------
void test_service_READY_to_DONE(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_NODE_READY;
    prevProdMonState = prodMonState;
    compileUplink_ProductionTestingDone_Expect();
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_DONE, prodMonState);
}
//----------------------------------
void test_service_DONE(void)
{
_DEBUG_OUT_TEST_NAME
    prodMonState = PROD_DONE;
    prevProdMonState = prodMonState;
    productionMonitor_service();
    TEST_ASSERT_EQUAL_INT(PROD_DONE, prodMonState);
}

//--------------------------------------------------------------------
// bool productionMonitor_getNetworkReadyForDevice(void);
//--------------------------------------------------------------------
void test_getNetworkReadyForDevice_returnTrueIfDeviceNotUnderTest()
{
_DEBUG_OUT_TEST_NAME
    unitIsUnderTest = false;
    networkReadyForDevice = true;
    TEST_ASSERT_TRUE(productionMonitor_getNetworkReadyForDevice());
}
void test_getNetworkReadyForDevice_returnFalseIfTestManagerNotReady()
{
_DEBUG_OUT_TEST_NAME
    unitIsUnderTest = true;
    networkReadyForDevice = false;
    TEST_ASSERT_FALSE(productionMonitor_getNetworkReadyForDevice());
}
void test_getNetworkReadyForDevice_returnTrueOnceTestManagerReady()
{
_DEBUG_OUT_TEST_NAME
    unitIsUnderTest = true;
    networkReadyForDevice = true;
    TEST_ASSERT_TRUE(productionMonitor_getNetworkReadyForDevice());
}


//-----------------------------------------------------------------
// void productionMonitor_calibrateReadings()
//-----------------------------------------------------------------
void test_calibrateReadings()
{
_DEBUG_OUT_TEST_NAME
    receivedCalibrationReferenceReadings = false;
    PowerCalcDataset refReadings;
    powerMonitor_applyCalibReadings_ExpectAndReturn(&refReadings, POWER_MONITOR_RESULT_OK);
    productionMonitor_calibrateReadings(&refReadings);
    TEST_ASSERT_TRUE(receivedCalibrationReferenceReadings);
}

//-----------------------------------------------------------------
// productionMonitor_receivedNodeInfo
//-----------------------------------------------------------------
void test_receivedNodeInfo()
{
_DEBUG_OUT_TEST_NAME
    receivedNodeInfo = false;

    nodeInfoDataset infoPayload = {0};
    snprintf(infoPayload.SerialNumberString, sizeof infoPayload.SerialNumberString, "UNIT01");
    infoPayload.SerialNumberString[5] = '1'; // TODO: avoid this !!!
    snprintf(infoPayload.AppKeyString, sizeof infoPayload.AppKeyString, "11223344556677881122334455667788");
    infoPayload.AppKeyString[31] = '8'; // TODO: avoid this !!!
    infoPayload.partNumber = (uint32_t)6810;
    infoPayload.powerBoardVersion = 0x1A;
    infoPayload.mainBoardVersion = 0x2B;
    infoPayload.radioBoardVersion = 0x3C;
    infoPayload.assemblyVersion = 0x40;

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Critical expected string with '?' where Null characters are expected.
    char keySectionString[] = "UNIT01?AAF77D2AF96B4A49?0102030405060708?11223344556677881122334455667788?F6810?\x1A?\x2B?\x3C?\x40?";
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    uint8_t i;
    for(i=0; i<sizeof keySectionString; i++)
    {
        if(keySectionString[i] == '?') keySectionString[i] = '\0';
    }

    char devEui[16] = "0102030405060708";
    get_nwk_com_dev_eui_Expect(devEui);
    get_nwk_com_dev_eui_IgnoreArg_p_devEuiString();
    get_nwk_com_dev_eui_ReturnArrayThruPtr_p_devEuiString(devEui, 16);

    productionMonitor_checkAndSaveNodeInfo_ExpectAnyArgsAndReturn(true);
    productionMonitor_receivedNodeInfo(&infoPayload);

    printf("\r\n Expected:\t");
    for(i=0; i<sizeof keySectionString; i++)
    {
        if(keySectionString[i] == '\0')
            printf("_");
        else
            printf("%c", keySectionString[i]);
    }
    printf("\r\n Result:\t");
    for(i=0; i<sizeof keySectionStringBuffer; i++)
    {
        if(keySectionStringBuffer[i] == '\0')
            printf("_");
        else
            printf("%c", keySectionStringBuffer[i]);
    }

    char errorMsg[20] = {0};
    for(i=0; i<sizeof keySectionStringBuffer; i++)
    {
        printf("%c",keySectionString[i]);
        sprintf(errorMsg , "Position = %d", i);
        TEST_ASSERT_EQUAL_UINT8_MESSAGE(keySectionString[i], keySectionStringBuffer[i], errorMsg);
    }
    TEST_ASSERT_TRUE(receivedNodeInfo);
}
