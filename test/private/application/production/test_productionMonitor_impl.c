#include "unity.h"
#include "productionMonitor_impl.h"

#include "mock_productionMonitor.h"
#include "mock_common.h"
#include "mock_hw.h"

#include "mock_DaliAL.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Critical expected string with '?' where Null characters are expected.
    char keySectionString[] = "UNIT01?AAF77D2AF96B4A49?0102030405060708?11223344556677881122334455667788?F6810?\x1A?\x2B?\x3C?\x40?";
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

void setUp(void)
{
    uint8_t i;
    sprintf(keySectionString, "UNIT01?AAF77D2AF96B4A49?0102030405060708?11223344556677881122334455667788?F6810?\x1A?\x2B?\x3C?\x40?");
    for(i=0; i<sizeof keySectionString; i++)
    {
        if(keySectionString[i] == '?') keySectionString[i] = '\0';
    }
}
void tearDown(void)
{

}

//-----------------------------------------------------------------
// uint8_t readByteFromKeysSection(uint8_t position)
//-----------------------------------------------------------------

void test_readByteFromKeysSection_returnFFIfPositionIsNull()
{
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_EQUAL_UINT8(0xFF, readByteFromKeysSection(0));
}

//-----------------------------------------------------------------
// bool productionMonitor_checkAndSaveNodeInfo()
//-----------------------------------------------------------------

void test_checkAndSaveNodeInfo_AllOk()
{
_DEBUG_OUT_TEST_NAME
    hw_writeToInternalFlashSingleSector_Expect(0xB000, (uint32_t *)keySectionString, NODE_INFO_STRING_LENGTH);
    TEST_ASSERT_TRUE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfNullCharacterMissing_1()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[6] = 'X';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfNullCharacterMissing_2()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[23] = 'X';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfNullCharacterMissing_3()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[40] = 'X';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfNullCharacterMissing_4()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[73] = 'X';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfNullCharacterMissing_5()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[79] = 'X';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfNullCharacterMissing_6()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[81] = 'X';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfNullCharacterMissing_7()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[83] = 'X';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfNullCharacterMissing_8()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[85] = 'X';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfNullCharacterMissing_9()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[87] = 'X';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfInvalidHexCharInAppEui_1()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[7] = '\0';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfInvalidHexCharInAppEui_2()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[22] = '\0';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfInvalidHexCharInDevEui_1()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[24] = '/';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfInvalidHexCharInDevEui_2()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[39] = ':';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfInvalidHexCharInAppKey_1()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[41] = '@';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

void test_checkAndSaveNodeInfo_RejectIfInvalidHexCharInAppKey_2()
{
_DEBUG_OUT_TEST_NAME
    keySectionString[72] = 'G';
    TEST_ASSERT_FALSE(productionMonitor_checkAndSaveNodeInfo(keySectionString));
}

//-----------------------------------------------------------------
// void productionMonitor_sendDevEuiOverDali(char * devEuiString)
//-----------------------------------------------------------------
void test_productionMonitor_sendDevEuiOverDali_NullPointer()
{
_DEBUG_OUT_TEST_NAME
    productionMonitor_sendDevEuiOverDali(NULL);
}

void test_productionMonitor_sendDevEuiOverDali_StringTooShort()
{
_DEBUG_OUT_TEST_NAME
    char devEUI[25] = {0};
    sprintf(devEUI, "112233445566778");
    productionMonitor_sendDevEuiOverDali(devEUI);
}

void test_productionMonitor_sendDevEuiOverDali_StringTooLong()
{
_DEBUG_OUT_TEST_NAME
    char devEUI[25] = {0};
    sprintf(devEUI, "112233445566778899");
    productionMonitor_sendDevEuiOverDali(devEUI);
}

void test_productionMonitor_sendDevEuiOverDali_StringA()
{
_DEBUG_OUT_TEST_NAME
    char devEUI[25] = {0};
    sprintf(devEUI, "1122334455667788");
    DaliAL_directArcPowerControl_Expect(0x11, NULL);
    DaliAL_directArcPowerControl_Expect(0x22, NULL);
    DaliAL_directArcPowerControl_Expect(0x33, NULL);
    DaliAL_directArcPowerControl_Expect(0x44, NULL);
    DaliAL_directArcPowerControl_Expect(0x55, NULL);
    DaliAL_directArcPowerControl_Expect(0x66, NULL);
    DaliAL_directArcPowerControl_Expect(0x77, NULL);
    DaliAL_directArcPowerControl_Expect(0x88, NULL);
    productionMonitor_sendDevEuiOverDali(devEUI);
}

void test_productionMonitor_sendDevEuiOverDali_StringB()
{
_DEBUG_OUT_TEST_NAME
    char devEUI[25] = {0};
    sprintf(devEUI, "AB2233EF55667788");
    DaliAL_directArcPowerControl_Expect(0xAB, NULL);
    DaliAL_directArcPowerControl_Expect(0x22, NULL);
    DaliAL_directArcPowerControl_Expect(0x33, NULL);
    DaliAL_directArcPowerControl_Expect(0xEF, NULL);
    DaliAL_directArcPowerControl_Expect(0x55, NULL);
    DaliAL_directArcPowerControl_Expect(0x66, NULL);
    DaliAL_directArcPowerControl_Expect(0x77, NULL);
    DaliAL_directArcPowerControl_Expect(0x88, NULL);
    productionMonitor_sendDevEuiOverDali(devEUI);
}

//-----------------------------------------------------------------
// void productionMonitor_sendNewStateOverDali(productionMonitorState newState)
//-----------------------------------------------------------------
void test_sendNewStateOverDali()
{
_DEBUG_OUT_TEST_NAME

    DaliAL_setFadeTime_Expect(PROD_WAIT_FOR_12V_SUPPLY, NULL);
    productionMonitor_sendNewStateOverDali(PROD_WAIT_FOR_12V_SUPPLY);    
}