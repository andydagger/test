#include "unity.h"
#include "msgRx_impl.c"
#include <stdio.h>

#include "mock_debug.h"
#include "mock_msg_rx.h"
#include "mock_config_network.h"
#include "mock_productionMonitor.h"
#include "mock_common.h"
#include "mock_config_eeprom.h"
#include "mock_NVMData.h"
#include "mock_im_main.h"
#include "mock_FreeRTOS.h"
#include "mock_task.h"
#include "mock_luminrop.h"
#include "mock_sysmon.h"


#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

pkt_format last_pkt;

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------------------
// bool receivedReferenceCalibrationData(void)
//-----------------------------------------------------------------------------
void test_receivedReferenceCalibrationData()
{
_DEBUG_OUT_TEST_NAME
    char * p_rx_pkt = NULL;
    char **p_p_rx_pkt = &p_rx_pkt;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Test Payload
    uint8_t msgLength = 16;
    PowerCalcDataset powerCalDataset = {230.45F, 0.256F, 0.935F, 432.16F, 0, 0, 0, 0};
    char testPayload[] = {0x43,0x66,0x73,0x33, 0x3E,0x83,0x12,0x6F, 0x3F,0x6F,0x5C,0x29, 0x43,0xD8,0x14,0x7B};
    last_pkt.content_byte_count = msgLength;
    while(msgLength > 0)
    {
        msgLength--;
        last_pkt.pkt_content[msgLength] = testPayload[msgLength];
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    common_array_to_uint32_ExpectAndReturn(p_p_rx_pkt, 0x43667333);
    common_array_to_uint32_IgnoreArg_p_buffer();
    common_array_to_uint32_ExpectAndReturn(p_p_rx_pkt, 0x3E83126F);
    common_array_to_uint32_IgnoreArg_p_buffer();
    common_array_to_uint32_ExpectAndReturn(p_p_rx_pkt, 0x3F6F5C29);
    common_array_to_uint32_IgnoreArg_p_buffer();
    common_array_to_uint32_ExpectAndReturn(p_p_rx_pkt, 0x43D8147B);
    common_array_to_uint32_IgnoreArg_p_buffer();

    productionMonitor_calibrateReadings_Expect(&powerCalDataset);
    TEST_ASSERT_TRUE(receivedReferenceCalibData());
}

void test_receivedReferenceCalibrationData_rejectIfPayloadTooShort()
{
_DEBUG_OUT_TEST_NAME
    PowerCalcDataset powerCalDataset = {0};
    last_pkt.content_byte_count = PWR_CALC_PAYLOAD_LENGTH - 1;
    TEST_ASSERT_FALSE(receivedReferenceCalibData());
}
void test_receivedReferenceCalibrationData_rejectIfPayloadTooLong()
{
_DEBUG_OUT_TEST_NAME
    PowerCalcDataset powerCalDataset = {0};
    last_pkt.content_byte_count = PWR_CALC_PAYLOAD_LENGTH + 1;
    TEST_ASSERT_FALSE(receivedReferenceCalibData());
}

//-----------------------------------------------------------------------------
// bool receivedNodeData(void)
//-----------------------------------------------------------------------------
void test_receivedNodeData()
{
_DEBUG_OUT_TEST_NAME
    char * p_rx_pkt = NULL;
    char **p_p_rx_pkt = &p_rx_pkt;

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Test Payload
    uint8_t msgLength = 44;
    nodeInfoDataset infoPayload = {0};
    snprintf(infoPayload.SerialNumberString, sizeof infoPayload.SerialNumberString, "ABCDEF");
    infoPayload.SerialNumberString[5] = 'F'; // TODO: avoid this
    snprintf(infoPayload.AppKeyString, sizeof infoPayload.AppKeyString, "11223344556677881122334455667788");
    infoPayload.AppKeyString[31] = '8'; // TODO: avoid this
    infoPayload.partNumber = (uint32_t)6810;
    infoPayload.powerBoardVersion = 0x1A;
    infoPayload.mainBoardVersion = 0x2B;
    infoPayload.radioBoardVersion = 0x3C;
    infoPayload.assemblyVersion = 0x40;

    char testPayload[] = {  'A', 'B', 'C', 'D', 'E', 'F',
                            '1','1','2','2','3','3','4','4','5','5','6','6','7','7','8','8','1','1','2','2','3','3','4','4','5','5','6','6','7','7','8','8',
                            0x1A, 0x9A,
                            0X1A,
                            0X2B,
                            0X3C,
                            0X40
                            };
    last_pkt.content_byte_count = msgLength;
    while(msgLength > 0)
    {
        msgLength--;
        last_pkt.pkt_content[msgLength] = testPayload[msgLength];
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    productionMonitor_receivedNodeInfo_Expect(&infoPayload);

    TEST_ASSERT_TRUE(receivedNodeData());
}
void test_receivedNodeData_rejectIfPayloadTooShort()
{
_DEBUG_OUT_TEST_NAME
    nodeInfoDataset infoPayload = {0};
    last_pkt.content_byte_count = sizeof infoPayload - 1;
    TEST_ASSERT_FALSE(receivedNodeData());
}
void test_receivedNodeData_rejectIfPayloadTooLong()
{
_DEBUG_OUT_TEST_NAME
    nodeInfoDataset infoPayload = {0};
    last_pkt.content_byte_count = sizeof infoPayload + 1;
    TEST_ASSERT_FALSE(receivedNodeData());
}


//-----------------------------------------------------------------------------
// bool receivedReferenceCalibData(void)
//-----------------------------------------------------------------------------
void test_receivedReferenceCalibData()
{
    _DEBUG_OUT_TEST_NAME
        char* p_rx_pkt = NULL;
    char** p_p_rx_pkt = &p_rx_pkt;
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Test Payload
    uint8_t msgLength = 16;
    PowerCalcDataset powerCalDataset = { 230.45F, 0.256F, 0.935F, 432.16F, 0, 0, 0, 0 };
    char testPayload[] = { 0x43,0x66,0x73,0x33, 0x3E,0x83,0x12,0x6F, 0x3F,0x6F,0x5C,0x29, 0x43,0xD8,0x14,0x7B };
    last_pkt.content_byte_count = msgLength;
    while (msgLength > 0)
    {
        msgLength--;
        last_pkt.pkt_content[msgLength] = testPayload[msgLength];
    }
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    common_array_to_uint32_ExpectAndReturn(p_p_rx_pkt, 0x43667333);
    common_array_to_uint32_IgnoreArg_p_buffer();
    common_array_to_uint32_ExpectAndReturn(p_p_rx_pkt, 0x3E83126F);
    common_array_to_uint32_IgnoreArg_p_buffer();
    common_array_to_uint32_ExpectAndReturn(p_p_rx_pkt, 0x3F6F5C29);
    common_array_to_uint32_IgnoreArg_p_buffer();
    common_array_to_uint32_ExpectAndReturn(p_p_rx_pkt, 0x43D8147B);
    common_array_to_uint32_IgnoreArg_p_buffer();

    productionMonitor_calibrateReadings_Expect(&powerCalDataset);
    TEST_ASSERT_TRUE(receivedReferenceCalibData());
}

void test_receivedReferenceCalibData_rejectIfPayloadTooShort()
{
    _DEBUG_OUT_TEST_NAME
        PowerCalcDataset powerCalDataset = { 0 };
    last_pkt.content_byte_count = PWR_CALC_PAYLOAD_LENGTH - 1;
    TEST_ASSERT_FALSE(receivedReferenceCalibData());
}
void test_receivedReferenceCalibData_rejectIfPayloadTooLong()
{
    _DEBUG_OUT_TEST_NAME
        PowerCalcDataset powerCalDataset = { 0 };
    last_pkt.content_byte_count = PWR_CALC_PAYLOAD_LENGTH + 1;
    TEST_ASSERT_FALSE(receivedReferenceCalibData());
}

//-----------------------------------------------------------------------------
// bool received_status_period(void)
//-----------------------------------------------------------------------------
void test_received_status_period()
{
    _DEBUG_OUT_TEST_NAME

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // Test Payload
    char testPayload[] = { 0x1E };
    last_pkt.content_byte_count = 1;
    last_pkt.pkt_content[0] = testPayload[0];
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    NVMData_readRuntimeParams_ExpectAnyArgsAndReturn(NVM_RESULT_OK);

    im_main_set_timer_period_ExpectAnyArgs();

    NVMData_writeRuntimeParams_ExpectAnyArgs();

    TEST_ASSERT_TRUE(received_status_period());
}


void test_received_status_period_rejectIfPayloadTooShort()
{
    _DEBUG_OUT_TEST_NAME
   
    last_pkt.content_byte_count = STATUS_PERIOD_MSG_CONTENT_LENGTH - 1;
    TEST_ASSERT_FALSE(received_status_period());
}
void test_received_status_period_rejectIfPayloadTooLong()
{
    _DEBUG_OUT_TEST_NAME
   
    last_pkt.content_byte_count = STATUS_PERIOD_MSG_CONTENT_LENGTH + 1;
    TEST_ASSERT_FALSE(received_status_period());
}

//-----------------------------------------------------------------------------
// bool received_driver_reset(void)
//-----------------------------------------------------------------------------
void test_received_driver_reset()
{
    _DEBUG_OUT_TEST_NAME

    last_pkt.content_byte_count = 0;

    NVMData_restoreLampOutputOverride_Expect();

    NVMData_restoreDaliAddresses_Expect();

    set_sysmon_delay_reset_ExpectAnyArgs();

    TEST_ASSERT_TRUE(received_driver_reset());
}
 

void test_received_driver_reset_rejectIfPayloadTooLong()
{
    _DEBUG_OUT_TEST_NAME

    last_pkt.content_byte_count = DRIVER_RESET_MSG_CONTENT_LENGTH + 1;
    TEST_ASSERT_FALSE(received_driver_reset());
}
