#include "unity.h"
#include "nwk_com.c"

#include "mock_nwk_tek.h"
#include "mock_FreeRTOS.h"
#include "mock_FreeRTOSConfig.h"
#include "mock_queue.h"
#include "mock_timers.h"
#include "mock_task.h"
#include "mock_config_timers.h"
#include "mock_msg_rx.h"
#include "mock_sysmon.h"
#include "mock_config_network.h"
#include "mock_config_queues.h"
#include "mock_LorawanStatusFlags.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

#define TEST_STRING_50BYTES "0011223344556677889900112233445566778899001122334455667788990011223344556677889900112233445566778899\r\n"
#define TEST_STRING_51BYTES "001122334455667788990011223344556677889900112233445566778899001122334455667788990011223344556677889900\r\n"

#define ALLOW_SESSION_RECOVERY true
#define FORCE_SESSION_RESET false

bool requestToRecoverSession = ALLOW_SESSION_RECOVERY;

TimerHandle_t xTimer[NUM_TIMERS];

void setUp(void)
{

}
void tearDown(void)
{

}

//-----------------------------------------------------------------
void test_init_okWithSessionRestore(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    bool sessionWasRestored = true;
    LorawanStatusFlags_GetModuleNeedsInitialising_ExpectAndReturn(false);
    nwk_tek_init_ExpectAndReturn(&requestToRecoverSession, true);
    nwk_tek_init_ReturnThruPtr_allowNetworkSessionRecovery(&sessionWasRestored);
    LorawanStatusFlags_SetJoinedFlag_Expect();
    LorawanStatusFlags_ClearModuleNeedsInitialising_Expect();
    TEST_ASSERT_TRUE(nwk_com_init());
}

void test_init_didNotRestoreSession(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    bool sessionWasRestored = false;
    LorawanStatusFlags_GetModuleNeedsInitialising_ExpectAndReturn(false);
    nwk_tek_init_ExpectAndReturn(&requestToRecoverSession, true);
    nwk_tek_init_ReturnThruPtr_allowNetworkSessionRecovery(&sessionWasRestored);
    LorawanStatusFlags_ClearModuleNeedsInitialising_Expect();
    TEST_ASSERT_TRUE(nwk_com_init());
}

void test_init_failWithSessionRestore(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = ALLOW_SESSION_RECOVERY;
    LorawanStatusFlags_GetModuleNeedsInitialising_ExpectAndReturn(false);       
    nwk_tek_init_ExpectAndReturn(&requestToRecoverSession, false);
    TEST_ASSERT_FALSE(nwk_com_init());
}
void test_init_okWithoutSessionRestore(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = FORCE_SESSION_RESET;
    LorawanStatusFlags_GetModuleNeedsInitialising_ExpectAndReturn(true); 
    LorawanStatusFlags_ClearJoinedFlag_Expect();  
    nwk_tek_init_ExpectAndReturn(&requestToRecoverSession, true);
    LorawanStatusFlags_ClearModuleNeedsInitialising_Expect();
    TEST_ASSERT_TRUE(nwk_com_init());
}

void test_init_failWithoutSessionRestore(){
_DEBUG_OUT_TEST_NAME
    requestToRecoverSession = FORCE_SESSION_RESET;
    LorawanStatusFlags_GetModuleNeedsInitialising_ExpectAndReturn(true); 
    LorawanStatusFlags_ClearJoinedFlag_Expect();
    nwk_tek_init_ExpectAndReturn(&requestToRecoverSession, false);
    TEST_ASSERT_FALSE(nwk_com_init());
}
//-----------------------------------------------------------------
void test_connect_okFromNotJoined(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(false);
    nwk_tek_connect_ExpectAndReturn(false, true);
    LorawanStatusFlags_SetJoinedFlag_Expect();
    nwk_tek_save_session_info_Expect();
    TEST_ASSERT_TRUE(nwk_com_connect());
}

void test_connect_failFromNotJoined(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(false);
    nwk_tek_connect_ExpectAndReturn(false, false);
    LorawanStatusFlags_ClearJoinedFlag_Expect();
    TEST_ASSERT_FALSE(nwk_com_connect());
}
void test_connect_okSessionRestore(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(true);
    nwk_tek_connect_ExpectAndReturn(true, true);
    LorawanStatusFlags_SetJoinedFlag_Expect();
    nwk_tek_save_session_info_Expect();
    TEST_ASSERT_TRUE(nwk_com_connect());
}

void test_connect_failSessionRestore(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(true);
    nwk_tek_connect_ExpectAndReturn(true, false);
    LorawanStatusFlags_ClearJoinedFlag_Expect();
    TEST_ASSERT_FALSE(nwk_com_connect());
}
//-----------------------------------------------------------------
void test_send_msg_okWithLastGaspInactive(){
_DEBUG_OUT_TEST_NAME
    get_sysmon_last_gasp_is_active_IgnoreAndReturn(false);
    nwk_tek_send_msg_ExpectAndReturn("TEST_STRING\r\n", true, true);
    TEST_ASSERT_TRUE(nwk_com_send_msg("TEST_STRING\r\n", true));
}
void test_send_msg_failWithLastGaspInactive(){
_DEBUG_OUT_TEST_NAME
    get_sysmon_last_gasp_is_active_IgnoreAndReturn(false);
    nwk_tek_send_msg_ExpectAndReturn("TEST_STRING\r\n", true, false);
    TEST_ASSERT_FALSE(nwk_com_send_msg("TEST_STRING\r\n", true));
}
void test_send_msg_failWithLastGaspActive(){
_DEBUG_OUT_TEST_NAME
    get_sysmon_last_gasp_is_active_IgnoreAndReturn(false);
    nwk_tek_send_msg_ExpectAndReturn("TEST_STRING\r\n", true, false);
    TEST_ASSERT_FALSE(nwk_com_send_msg("TEST_STRING\r\n", true));
}
void test_send_msg_oklWithLastGaspActive(){
_DEBUG_OUT_TEST_NAME
    get_sysmon_last_gasp_is_active_ExpectAndReturn(true);
    nwk_tek_save_session_info_Expect();
    nwk_tek_send_msg_ExpectAndReturn("TEST_STRING\r\n", true, true);
    get_sysmon_last_gasp_is_active_ExpectAndReturn(true);
    nwk_tek_save_session_info_Expect();
    TEST_ASSERT_TRUE(nwk_com_send_msg("TEST_STRING\r\n", true));
}
void test_send_msg_faillWithLastGaspActive(){
_DEBUG_OUT_TEST_NAME
    get_sysmon_last_gasp_is_active_ExpectAndReturn(true);
    nwk_tek_save_session_info_Expect();
    nwk_tek_send_msg_ExpectAndReturn("TEST_STRING\r\n", true, false);
    get_sysmon_last_gasp_is_active_ExpectAndReturn(true);
    nwk_tek_save_session_info_Expect();
    TEST_ASSERT_FALSE(nwk_com_send_msg("TEST_STRING\r\n", true));
}
void test_send_msg_nullPointerCatch(){
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(nwk_com_send_msg(NULL, false));   
}
void test_send_msg_emptyMsgCatch(){
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(nwk_com_send_msg("", false));
}
void test_send_msg_maxPayloadOk(){
_DEBUG_OUT_TEST_NAME
    get_sysmon_last_gasp_is_active_IgnoreAndReturn(false);
    nwk_tek_send_msg_ExpectAndReturn(TEST_STRING_50BYTES, false, true);
    TEST_ASSERT_TRUE(nwk_com_send_msg(TEST_STRING_50BYTES, false));
}
void test_send_msg_tooLongMsgMsgCatch(){
_DEBUG_OUT_TEST_NAME
    TEST_ASSERT_FALSE(nwk_com_send_msg(TEST_STRING_51BYTES, false));
}
//-----------------------------------------------------------------
void test_read_msg_msgValid(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetMcastSessionIsActive_IgnoreAndReturn(false);
    nwk_tek_read_msg_ExpectAndReturn("RETURN_STRING\r\n");
    msg_rx_process_ExpectAndReturn("RETURN_STRING\r\n", true);
    TEST_ASSERT_TRUE(nwk_com_read_msg());
}
void test_read_msg_msgNotValid(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetMcastSessionIsActive_IgnoreAndReturn(false);
    nwk_tek_read_msg_ExpectAndReturn(NULL);
    TEST_ASSERT_FALSE(nwk_com_read_msg());
}
void test_read_msg_resetMcastCounter(){
_DEBUG_OUT_TEST_NAME
    TickType_t xTicks = 1234;
    LorawanStatusFlags_GetMcastSessionIsActive_ExpectAndReturn(true);
    xTaskGetTickCount_ExpectAndReturn(xTicks);
    xTimerGenericCommand_ExpectAndReturn(xTimer[BCAST_TIME_OUT], tmrCOMMAND_RESET, xTicks, NULL, 0, pdTRUE);
    nwk_tek_read_msg_IgnoreAndReturn("X");
    msg_rx_process_ExpectAnyArgsAndReturn(true);
    TEST_ASSERT_TRUE(nwk_com_read_msg());
}
//-----------------------------------------------------------------
void test_update_broadcast_status_enableOk(){
_DEBUG_OUT_TEST_NAME
    TickType_t xTicks = 1234;
    xTaskGetTickCount_ExpectAndReturn(xTicks);
    xTimerGenericCommand_ExpectAndReturn(xTimer[BCAST_TIME_OUT], tmrCOMMAND_START, xTicks, NULL, 0, pdTRUE);
    LorawanStatusFlags_GetMcastSessionIsActive_ExpectAndReturn(false);
    nwk_tek_update_broabcast_status_ExpectAndReturn(true, true);
    LorawanStatusFlags_UpdateMcastStatus_Expect(true);
    TEST_ASSERT_TRUE(nwk_com_update_broabcast_status(true));
}
void test_update_broadcast_status_enableFail(){
_DEBUG_OUT_TEST_NAME
    TickType_t xTicks = 1234;
    xTaskGetTickCount_ExpectAndReturn(xTicks);
    xTimerGenericCommand_ExpectAndReturn(xTimer[BCAST_TIME_OUT], tmrCOMMAND_START, xTicks, NULL, 0, pdTRUE);
    LorawanStatusFlags_GetMcastSessionIsActive_ExpectAndReturn(false);
    nwk_tek_update_broabcast_status_ExpectAndReturn(true, false);
    TEST_ASSERT_FALSE(nwk_com_update_broabcast_status(true));
}
void test_update_broadcast_status_enableNoChange(){
_DEBUG_OUT_TEST_NAME
    TickType_t xTicks = 1234;
    xTaskGetTickCount_ExpectAndReturn(xTicks);
    xTimerGenericCommand_ExpectAndReturn(xTimer[BCAST_TIME_OUT], tmrCOMMAND_START, xTicks, NULL, 0, pdTRUE);
    LorawanStatusFlags_GetMcastSessionIsActive_ExpectAndReturn(true);
    TEST_ASSERT_TRUE(nwk_com_update_broabcast_status(true));
}
void test_update_broadcast_status_disableOk(){
_DEBUG_OUT_TEST_NAME
    xTimerGenericCommand_ExpectAndReturn(xTimer[BCAST_TIME_OUT], tmrCOMMAND_STOP, 0, NULL, 0, pdTRUE);
    LorawanStatusFlags_GetMcastSessionIsActive_ExpectAndReturn(true);
    nwk_tek_update_broabcast_status_ExpectAndReturn(false, true);
    LorawanStatusFlags_UpdateMcastStatus_Expect(false);
    TEST_ASSERT_TRUE(nwk_com_update_broabcast_status(false));
}
void test_update_broadcast_status_disableFail(){
_DEBUG_OUT_TEST_NAME
    xTimerGenericCommand_ExpectAndReturn(xTimer[BCAST_TIME_OUT], tmrCOMMAND_STOP, 0, NULL, 0, pdTRUE);
    LorawanStatusFlags_GetMcastSessionIsActive_ExpectAndReturn(true);
    nwk_tek_update_broabcast_status_ExpectAndReturn(false, false);
    TEST_ASSERT_FALSE(nwk_com_update_broabcast_status(false));
}
void test_update_broadcast_status_disableNoChange(){
_DEBUG_OUT_TEST_NAME
    xTimerGenericCommand_ExpectAndReturn(xTimer[BCAST_TIME_OUT], tmrCOMMAND_STOP, 0, NULL, 0, pdTRUE);
    LorawanStatusFlags_GetMcastSessionIsActive_ExpectAndReturn(false);
    TEST_ASSERT_TRUE(nwk_com_update_broabcast_status(false));
}
//-----------------------------------------------------------------
void test_get_broadcast_is_active_true(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetMcastSessionIsActive_ExpectAndReturn(true);
    TEST_ASSERT_TRUE(get_nwk_com_broadcast_is_active());
}
void test_get_broadcast_is_active_false(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetMcastSessionIsActive_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(get_nwk_com_broadcast_is_active());
}
//-----------------------------------------------------------------
void test_activate_broadcast_session(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_SetMcastSessionShouldBeActive_Expect(true);
    set_nwk_com_broadcast_session_should_be_active(true);
}
void test_deactivate_broadcast_session(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_SetMcastSessionShouldBeActive_Expect(false);
    set_nwk_com_broadcast_session_should_be_active(false);
}
//-----------------------------------------------------------------
void test_get_broadcast_should_be_active_true(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetMcastSessionShouldBeActive_ExpectAndReturn(true);
    TEST_ASSERT_TRUE(get_nwk_com_broadcast_session_should_be_active());
}
void test_get_broadcast_should_be_active_false(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetMcastSessionShouldBeActive_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(get_nwk_com_broadcast_session_should_be_active());
}
//-----------------------------------------------------------------
void test_broadcast_session_timeout_callback(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_SetMcastSessionShouldBeActive_Expect(false);
    vBroadcastTimeOutCallBack();
}
//-----------------------------------------------------------------
void test_get_network_is_connected_true(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(true);
    TEST_ASSERT_TRUE(get_nwk_com_is_connected());
}
void test_get_network_is_connected_false(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetJoinedFlag_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(get_nwk_com_is_connected());
}
//-----------------------------------------------------------------
void test_get_module_needs_initialising_true(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetModuleNeedsInitialising_ExpectAndReturn(true);
    TEST_ASSERT_TRUE(get_nwk_com_hw_needs_initialising());
}
void test_get_module_needs_initialising_false(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetModuleNeedsInitialising_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(get_nwk_com_hw_needs_initialising());
}
//-----------------------------------------------------------------
void test_get_received_string_is_waiting_true(){
_DEBUG_OUT_TEST_NAME
    get_nwk_tek_rx_string_is_waiting_ExpectAndReturn(true);
    TEST_ASSERT_TRUE(get_nwk_com_rx_msg_pending());
}
void test_get_received_string_is_waiting_false(){
_DEBUG_OUT_TEST_NAME
    get_nwk_tek_rx_string_is_waiting_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(get_nwk_com_rx_msg_pending());
}
//-----------------------------------------------------------------
void test_load_uplink_rtosNotStarted(){
_DEBUG_OUT_TEST_NAME
    xTaskGetSchedulerState_ExpectAndReturn(taskSCHEDULER_NOT_STARTED);
    TEST_ASSERT_FALSE(nwk_com_load_uplink(NO_DELAY, LOW_PRIORITY, QOS_0, "01020304"));
}
void test_load_uplink_queueFull(){
_DEBUG_OUT_TEST_NAME
    xTaskGetSchedulerState_IgnoreAndReturn(taskSCHEDULER_RUNNING);
    xQueueGenericSend_ExpectAnyArgsAndReturn(pdFALSE);
    TEST_ASSERT_FALSE(nwk_com_load_uplink(NO_DELAY, LOW_PRIORITY, QOS_0, "01020304"));
}
void test_load_uplink(){
_DEBUG_OUT_TEST_NAME
    xTaskGetSchedulerState_IgnoreAndReturn(taskSCHEDULER_RUNNING);
    xQueueGenericSend_ExpectAnyArgsAndReturn(pdTRUE);
    TEST_ASSERT_TRUE(nwk_com_load_uplink(NO_DELAY, LOW_PRIORITY, QOS_0, "01234567890123456789\r\n"));
}
void test_load_uplink_tooLong(){
_DEBUG_OUT_TEST_NAME
    xTaskGetSchedulerState_IgnoreAndReturn(taskSCHEDULER_RUNNING);
    xQueueGenericSend_ExpectAnyArgsAndReturn(pdTRUE);
    TEST_ASSERT_TRUE(nwk_com_load_uplink(NO_DELAY, LOW_PRIORITY, QOS_0, TEST_STRING_51BYTES));
}
//-----------------------------------------------------------------
void test_save_session(){
_DEBUG_OUT_TEST_NAME
    nwk_tek_save_session_info_Expect();
    nwk_com_save_session_info();
}
//-----------------------------------------------------------------
void test_full_hw_reset(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_UpdateMcastStatus_Expect(false);
    LorawanStatusFlags_ClearJoinedFlag_Expect();
    LorawanStatusFlags_SetModuleNeedsInitialising_Expect();
    nwk_com_full_hw_reset();
}
//-----------------------------------------------------------------
void test_force_rejoin(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_UpdateMcastStatus_Expect(false);
    LorawanStatusFlags_SetModuleNeedsInitialising_Expect();
    nwk_com_force_rejoin();
}
//-----------------------------------------------------------------
void test_optimise_settings_couldBeOptimised(){
_DEBUG_OUT_TEST_NAME
    nwk_tek_optimise_settings_ExpectAndReturn(true);
    LorawanStatusFlags_ClearModuleSettingsNeedOptimising_Expect();
    nwk_com_optimise_settings();
}
void test_optimise_settings_couldNotBeOptimisedTriggersRejoin(){
_DEBUG_OUT_TEST_NAME
    nwk_tek_optimise_settings_ExpectAndReturn(false);
    LorawanStatusFlags_SetModuleNeedsInitialising_Expect();
    LorawanStatusFlags_ClearModuleSettingsNeedOptimising_Expect();
    nwk_com_optimise_settings();
}
//-----------------------------------------------------------------
void test_get_optimise_settings_flag(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_GetModuleSettingsNeedOptimising_ExpectAndReturn(true);
    TEST_ASSERT_TRUE(get_nwk_com_settings_need_optimising());
    LorawanStatusFlags_GetModuleSettingsNeedOptimising_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(get_nwk_com_settings_need_optimising());
}
//-----------------------------------------------------------------
void test_set_optimise_settings_flag(){
_DEBUG_OUT_TEST_NAME
    LorawanStatusFlags_SetModuleSettingsNeedOptimising_Expect();
    set_nwk_com_settings_need_optimising();
}
//-----------------------------------------------------------------
void test_get_deveui()
{
_DEBUG_OUT_TEST_NAME
    char pDevEUi[17] = {0};
    get_nwk_tek_dev_eui_Expect(pDevEUi);
    get_nwk_com_dev_eui(pDevEUi);
}