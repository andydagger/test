#include "unity.h"
#include "compileUplink.c"
#include <string.h>

#include "mock_common.h"
#include "mock_sysmon.h"
#include "mock_config_dataset.h"
#include "mock_config_uplink_payloads.h"
#include "mock_config_network.h"
#include "mock_luminrop.h"
#include "mock_calendar.h"
#include "mock_photo.h"
#include "mock_gps.h"
#include "mock_powerMonitor.h"
#include "mock_patch_rx.h"
#include "mock_debug.h"
#include "mock_rtc_procx.h"
#include "mock_nwk_com.h"
#include "mock_luminr_tek.h"
#include "mock_Tilt.h"
#include "mock_errorCode.h"
#include "mock_systick.h"
#include "mock_FreeRTOS.h"
#include "mock_task.h"
#include "mock_queue.h"
#include "mock_config_eeprom.h"
#include "mock_NonVolatileMemory.h"
#include "mock_NVMData.h"
#include "mock_im_main.h"


#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

PowerCalcDataset power_calc_dataset = { (float)234.561, // x10 = 2345 = 0x0929
                                        (float)1.2311, // x100 = 123 = 0x007B
                                        (float)0.8731, // X100 = 87 = 0x57
                                        (float)134.5183, // x10 = 1345 = 0x0541
                                        (float)12345, // 0x00003039
                                        (float)235.829, // x10 = 2358 = 0x0936
                                        (float)231.9464, // x10 = 2319 = 0x090F
                                        (float)231.5328}; // x10 = 2315 = 0x090B

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
void test_Fault_PowerFailure(){
_DEBUG_OUT_TEST_NAME
    rtc_procx_GetCounter_ExpectAndReturn(0x01020304);
    nwk_com_load_uplink_ExpectAndReturn( 0, HIGH_PRIORITY, QOS_1, "%s", true);
    compileUplink_Fault(HIGH_PRIORITY, QOS_1, (1<<POWER_FAILURE));
    TEST_ASSERT_EQUAL_STRING("E001018000000001020304", ascii_payload);
}

void test_Fault_MultipleFlags(){
_DEBUG_OUT_TEST_NAME
    rtc_procx_GetCounter_ExpectAndReturn(0x01020304);
    nwk_com_load_uplink_ExpectAndReturn( 0, HIGH_PRIORITY, QOS_1, "%s", true);
    compileUplink_Fault(HIGH_PRIORITY, QOS_1, (1<<NO_VALID_CALENDAR)|(1<<COLUMN_NOT_VERTICAL));
    TEST_ASSERT_EQUAL_STRING("E001010002000201020304", ascii_payload);
}

void test_Fault_ForComparisonPreviousProtocol(){
_DEBUG_OUT_TEST_NAME
    rtc_procx_GetCounter_ExpectAndReturn(1620380079); /* 0x609509AF or 07/05/21 at 11:34:39 + DST */
    nwk_com_load_uplink_ExpectAndReturn( 0, HIGH_PRIORITY, QOS_1, "%s", true);
    compileUplink_Fault(HIGH_PRIORITY, QOS_1, (1<<NO_VALID_CALENDAR)|(1<<COLUMN_NOT_VERTICAL));
    TEST_ASSERT_EQUAL_STRING("E0010100020002609509AF", ascii_payload);
    /*Protocol v0.9
    TEST_ASSERT_EQUAL_STRING("E0010100020002609509AF", ascii_payload);
    */
}

//-----------------------------------------------------------------
void test_Initial_Check(){
_DEBUG_OUT_TEST_NAME
    GpsDataset       gps_dataset = {    0x01020304,
                                        0x05060708, 
                                        0x02, 
                                        true};
    LuminrDataset    luminr_dataset = { 0x0F, /* Minimum Dimming 15% */  /* NOT FLIPPED */
                                        0x01 | (0x06<<2), /* DALI w/ LED */  /* NOT FLIPPED */
                                        {0xA,0xB,0xC,0xD,0xE,0xF}, /* NOT FLIPPED */
                                        {1,2,3,4,5,6,7,8}, /* NOT FLIPPED */
                                        true};
    initial_payload_struct payload = {0};

    get_luminr_tek_dataset_ExpectAnyArgs();
    get_luminr_tek_dataset_ReturnThruPtr_dataset(&luminr_dataset);
    get_gps_dataset_ExpectAnyArgs();
    get_gps_dataset_ReturnThruPtr_dataset(&gps_dataset);
    fetch_fw_version_decimal_ExpectAndReturn(0x1601);
    get_sysmon_fault_flags_ExpectAndReturn(0x00010002);
    rtc_procx_GetCounter_ExpectAndReturn(0x21222324);
    nwk_com_load_uplink_ExpectAndReturn( 0, LOW_PRIORITY, QOS_0, "%s", true);
    compileUplink_Initial();
    TEST_ASSERT_EQUAL_STRING("1001011601000100020F19212223240201020304050607080A0B0C0D0E0F0102030405060708", ascii_payload);
    return;
}

void test_Initial_Real(){
_DEBUG_OUT_TEST_NAME
    GpsDataset       gps_dataset = {    5370763, /* 100,000 x lat as int32_t */
                                        -191254, /* 100,000 x long as int32_t */
                                        0x02, /* Accuracy value*/
                                        true};
    LuminrDataset    luminr_dataset = { 0x14, /* Minimum Dimming 20% */  /* NOT FLIPPED */
                                        0x01 | (0x06<<2), /* DALI w/ LED */  /* NOT FLIPPED */
                                        {0xA,0xB,0xC,0xD,0xE,0xF}, /* NOT FLIPPED */
                                        {1,2,3,4,5,6,7,8}, /* NOT FLIPPED */
                                        true};
    initial_payload_struct payload = {0};

    get_luminr_tek_dataset_ExpectAnyArgs();
    get_luminr_tek_dataset_ReturnThruPtr_dataset(&luminr_dataset);
    get_gps_dataset_ExpectAnyArgs();
    get_gps_dataset_ReturnThruPtr_dataset(&gps_dataset);
    fetch_fw_version_decimal_ExpectAndReturn(016300);
    get_sysmon_fault_flags_ExpectAndReturn(0x00000002);
    rtc_procx_GetCounter_ExpectAndReturn(1618507893); /* 04/15/2021 @ 5:31pm */
    nwk_com_load_uplink_ExpectAndReturn( 0, LOW_PRIORITY, QOS_0, "%s", true);
    compileUplink_Initial();
    TEST_ASSERT_EQUAL_STRING("1001011CC000000002141960787875020051F38BFFFD14EA0A0B0C0D0E0F0102030405060708", ascii_payload);
    return;
}

void test_Initial_ForComparisonPreviousProtocol(){
_DEBUG_OUT_TEST_NAME
    GpsDataset       gps_dataset = {    4152572, /* 100,000 x lat as int32_t => 41.52572*/
                                        -540280, /* 100,000 x long as int32_t => -5.40280*/
                                        0x02, /* Accuracy level*/
                                        true};
    LuminrDataset    luminr_dataset = { 0x14, /* => b15:8 Physical Minimum Dimming 20% so 0x14 or 20d*/
                                        0x01 | (0x06<<2), /* => b7:0 DALI driver with LED so 0x19 or 25d*/ 
                                        {0xA,0xB,0xC,0xD,0xE,0xF}, /* => 0x0A0B0C0D0E0F*/
                                        {1,2,3,4,5,6,7,8}, /* => 0X0102030405060708*/
                                        true};
    initial_payload_struct payload = {0};

    get_luminr_tek_dataset_ExpectAnyArgs();
    get_luminr_tek_dataset_ReturnThruPtr_dataset(&luminr_dataset);
    get_gps_dataset_ExpectAnyArgs();
    get_gps_dataset_ReturnThruPtr_dataset(&gps_dataset);
    fetch_fw_version_decimal_ExpectAndReturn((uint32_t)16400);
    get_sysmon_fault_flags_ExpectAndReturn(0x00010000);
    rtc_procx_GetCounter_ExpectAndReturn(65536); /* => 01/01/1970 18:12:16 + DST */
    nwk_com_load_uplink_ExpectAndReturn( 0, LOW_PRIORITY, QOS_0, "%s", true);
    compileUplink_Initial();
    TEST_ASSERT_EQUAL_STRING("10010140100001000014190001000002003F5CFCFFF7C1880A0B0C0D0E0F0102030405060708", ascii_payload);
    /* Protocol v0.9 fw version = 01.59.00
    TEST_ASSERT_EQUAL_STRING("1001011C3E0000010003000000010002FC5C3F0088C1F7FF0A0B0C0D0E0F0102030405060708", ascii_payload);
    */
    return;
}

//-----------------------------------------------------------------
static void clearAllPreviousPayload(void)
{
    payloadStatus.previous_dataset.fault_flags = 0;
    payloadStatus.previous_dataset.instantVrms_x10 = 0;
    payloadStatus.previous_dataset.instantIrms_x100 = 0;
    payloadStatus.previous_dataset.instantPowerFactor_x100 = 0;
    payloadStatus.previous_dataset.instantPower_x10 = 0;
    payloadStatus.previous_dataset.brightnessLevel = 0;
    payloadStatus.previous_dataset.measuredLux = 0;

    payloadStatus.current_dataset.fault_flags = 0;
    payloadStatus.current_dataset.instantVrms_x10 = 0;
    payloadStatus.current_dataset.instantIrms_x100 = 0;
    payloadStatus.current_dataset.instantPowerFactor_x100 = 0;
    payloadStatus.current_dataset.instantPower_x10 = 0;
    payloadStatus.current_dataset.brightnessLevel = 0;
    payloadStatus.current_dataset.measuredLux = 0;

    return;

}
void test_Status_FirstPayloadWithDelay()
{
    _DEBUG_OUT_TEST_NAME

    NVM_RuntimeParams_t  read_params = {
        .version = NVM_RUNTIME_PARAMS_STRUCT_VER, 
        .status_period = 30, 
        .crc = 0
    };

    clearAllPreviousPayload();

    powerMonitor_getDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    powerMonitor_getDataset_ReturnThruPtr_dataset_ptr(&power_calc_dataset);
    // jitter calc
    get_patch_download_is_in_progess_IgnoreAndReturn(false);
    im_main_get_timer_period_IgnoreAndReturn(NVM_DEFAULT_STATUS_PERIOD * 60 * 1000);
    get_systick_ms_IgnoreAndReturn((uint32_t)315844); // %120 => 4 

    fetch_fw_version_decimal_IgnoreAndReturn((uint16_t)16300); // 0x3FAC
    rtc_procx_GetCounter_IgnoreAndReturn((uint32_t)0x10203040); // +4 with randS
    get_sysmon_fault_flags_IgnoreAndReturn((uint32_t)0x00000002);
    get_luminrop_LampOutputBrightnessLevel_IgnoreAndReturn((uint8_t)100);
    get_photo_LUX_IgnoreAndReturn((float)213.123); // 213 = 0xD5
    nwk_com_load_uplink_ExpectAndReturn( 4, LOW_PRIORITY, QOS_0, "%s", true);

    compileUplink_Status(true);
    TEST_ASSERT_EQUAL_STRING("1101013FAC10203044000000020929007B57054164D500000000000000000000000000", ascii_payload);

    return;
}

void test_Status_FirstPayloadWithDelay_ForComparisonPreviousProtocol()
{
    _DEBUG_OUT_TEST_NAME
 
    PowerCalcDataset power_calc_dataset2 = {0};
    power_calc_dataset2.instantVrms = 234.1F;
    power_calc_dataset2.instantPower = 42.9F;
    power_calc_dataset2.instantIrms = 0.2F;
    power_calc_dataset2.instantPowerFactor = 0.87F;
    // VA => 48.5
    clearAllPreviousPayload();

    powerMonitor_getDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    powerMonitor_getDataset_ReturnThruPtr_dataset_ptr(&power_calc_dataset2);
    // jitter calc
    get_patch_download_is_in_progess_IgnoreAndReturn(false);   
    im_main_get_timer_period_IgnoreAndReturn(NVM_DEFAULT_STATUS_PERIOD*60*1000);
    get_systick_ms_IgnoreAndReturn(0); // %120 => 0 

    fetch_fw_version_decimal_IgnoreAndReturn((uint16_t)16400); 
    rtc_procx_GetCounter_IgnoreAndReturn((uint32_t)1620366323); /* => 07/05/2021 05:45:23 + DST */
    get_sysmon_fault_flags_IgnoreAndReturn((uint32_t)0x00000002); /* "No Valid Calendar Data" => Executing default profile*/
    get_luminrop_LampOutputBrightnessLevel_IgnoreAndReturn((uint8_t)75); /* Currently at 75% brightness level */
    get_photo_LUX_IgnoreAndReturn((float)213.123); // => 213 lux
    nwk_com_load_uplink_ExpectAndReturn( 0, LOW_PRIORITY, QOS_0, "%s", true);

    compileUplink_Status(true);

 //   TEST_ASSERT_EQUAL_STRING("11010140106094D3F300000002092500145801AD4BD500000000000000000000000000", ascii_payload);
    TEST_ASSERT_EQUAL_STRING("11010140106094D3F300000002092500145701AD4BD500000000000000000000000000", ascii_payload);
    /* Protocol v0.9 - fw version = 01.59.00
    TEST_ASSERT_EQUAL_STRING("1101011C3EF3D39460020000002509140058AD01E5010000000000000000000000000000000000", ascii_payload);
    */    
}


void test_Status_UsesPreviousPayloadAsShadowData()
{
    _DEBUG_OUT_TEST_NAME

    clearAllPreviousPayload();

    powerMonitor_getDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    powerMonitor_getDataset_ReturnThruPtr_dataset_ptr(&power_calc_dataset);
    // jitter calc
    get_patch_download_is_in_progess_IgnoreAndReturn(false);
    im_main_get_timer_period_IgnoreAndReturn(NVM_DEFAULT_STATUS_PERIOD * 60 * 1000);
    get_systick_ms_IgnoreAndReturn((uint32_t)315844); // %120 => 4

    fetch_fw_version_decimal_IgnoreAndReturn((uint16_t)16300); // 0x3FAC
   
    rtc_procx_GetCounter_IgnoreAndReturn((uint32_t)0x10203040); // +4 with randS
    
    get_sysmon_fault_flags_IgnoreAndReturn((uint32_t)0x00000002);
    get_luminrop_LampOutputBrightnessLevel_IgnoreAndReturn((uint8_t)100);
    get_photo_LUX_IgnoreAndReturn((float)213.123); // 213 = 0xD5

    nwk_com_load_uplink_ExpectAndReturn( 4, LOW_PRIORITY, QOS_0, "%s", true);

    compileUplink_Status(true);

    TEST_ASSERT_EQUAL_STRING("1101013FAC10203044000000020929007B57054164D500000000000000000000000000", ascii_payload);

    powerMonitor_getDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    powerMonitor_getDataset_ReturnThruPtr_dataset_ptr(&power_calc_dataset);
    // jitter calc
    get_patch_download_is_in_progess_IgnoreAndReturn(false);
    im_main_get_timer_period_IgnoreAndReturn(NVM_DEFAULT_STATUS_PERIOD * 60 * 1000);
    //...
    nwk_com_load_uplink_ExpectAndReturn( 4, LOW_PRIORITY, QOS_0, "%s", true);
    
    compileUplink_Status(true);
    
    TEST_ASSERT_EQUAL_STRING("1101013FAC10203044000000020929007B57054164D5000000020929007B57054164D5", ascii_payload);
}


void test_Status_DisableDelayDuringPatchDownload()
{
_DEBUG_OUT_TEST_NAME

    clearAllPreviousPayload();

    powerMonitor_getDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    powerMonitor_getDataset_ReturnThruPtr_dataset_ptr(&power_calc_dataset);

    fetch_fw_version_decimal_IgnoreAndReturn((uint16_t)16300); // 0x3FAC
    rtc_procx_GetCounter_IgnoreAndReturn((uint32_t)0x10203040); // +4 with randS
    get_sysmon_fault_flags_IgnoreAndReturn((uint32_t)0x00000002);
    get_luminrop_LampOutputBrightnessLevel_IgnoreAndReturn((uint8_t)100);
    get_photo_LUX_IgnoreAndReturn((float)213.123); // 213 = 0xD5

    nwk_com_load_uplink_ExpectAndReturn( 0, LOW_PRIORITY, QOS_0, "%s", true);
    
    compileUplink_Status(false);
    
    TEST_ASSERT_EQUAL_STRING("1101013FAC10203040000000020929007B57054164D500000000000000000000000000", ascii_payload);
}


void test_Status_DisableDelayIfRequested()
{
_DEBUG_OUT_TEST_NAME

    clearAllPreviousPayload();

    powerMonitor_getDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    powerMonitor_getDataset_ReturnThruPtr_dataset_ptr(&power_calc_dataset);

    fetch_fw_version_decimal_IgnoreAndReturn((uint16_t)16300); // 0x3FAC
    rtc_procx_GetCounter_IgnoreAndReturn((uint32_t)0x10203040); // +4 with randS
    get_sysmon_fault_flags_IgnoreAndReturn((uint32_t)0x00000002);
    get_luminrop_LampOutputBrightnessLevel_IgnoreAndReturn((uint8_t)100);
    get_photo_LUX_IgnoreAndReturn((float)213.123); // 213 = 0xD5

    nwk_com_load_uplink_ExpectAndReturn( 0, LOW_PRIORITY, QOS_0, "%s", true);
    
    compileUplink_Status(false);

    TEST_ASSERT_EQUAL_STRING("1101013FAC10203040000000020929007B57054164D500000000000000000000000000", ascii_payload);
}

//-----------------------------------------------------------------
void test_Daily_Typical(void)
{
_DEBUG_OUT_TEST_NAME
    Tilt_output latest_xyz = {0x0012, 0x0034, 0x0056};
    uint8_t idByte = 0x01;
    uint8_t valueByte = 0x02;
    BillingDataset billingData = {  (uint32_t)0x11121314, 
                                    (uint32_t)0x11223344, 
                                    (uint16_t)0x0102, 
                                    (uint32_t)0x000000A1, 
                                    (uint8_t)0x0F};         

    vTaskSuspendAll_Expect();
    Tilt_getRef_ExpectAndReturn((Tilt *)0x1234);
    Tilt_getXYZ_ExpectAnyArgs();
    Tilt_getXYZ_ReturnThruPtr_pOutput(&latest_xyz);
    xTaskResumeAll_ExpectAndReturn(pdTRUE);
    
    powerMonitor_getDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    powerMonitor_getDataset_ReturnThruPtr_dataset_ptr(&power_calc_dataset);

    errorCode_getCode_ExpectAnyArgs();
    errorCode_getCode_ReturnThruPtr_p_idByte(&idByte);
    errorCode_getCode_ReturnThruPtr_p_valueByte(&valueByte);
    errorCode_eraseCode_Expect();

    get_random_delay_ms_ExpectAndReturn(9876);

    nwk_com_load_uplink_ExpectAndReturn( 9876/14, LOW_PRIORITY, QOS_2, "%s", true);
    compileUplink_Daily(billingData);
    TEST_ASSERT_EQUAL_STRING("B1010111121314112233440012003400560102000000A10F000030390936090F090B0102", ascii_payload);
    return;
}


void test_Daily_ForComparisonPreviousProtocol(void)
{
_DEBUG_OUT_TEST_NAME
    // x => -14/1024 = -0.013672 g
    // y => 25/1024 = 0.024414 g
    // z => -1028/1024 = 1.003906 g
    Tilt_output latest_xyz = {(int16_t)-14, (int16_t)25, (int16_t)-1028};
    uint8_t idByte = 0x01; // Eng Debug parse as is
    uint8_t valueByte = 0x02; // Eng Debug parse as is
    BillingDataset billingData = {  (uint32_t) 1620247148, /* => Switch ON 05/05/21 at 20:39:08 + DST */
                                    (uint32_t) 1620281171, /* => Switch OFF 06/05/21 at 06:06:11 + DST */
                                    (uint16_t) 1, /* => Calendar Version 01 */
                                    (uint32_t) 248, /* Calendar Reference 248 */
                                    (uint8_t) 1}; /* Was executing profile id 1*/         

    vTaskSuspendAll_Expect();
    Tilt_getRef_ExpectAndReturn((Tilt *)0x1234);
    Tilt_getXYZ_ExpectAnyArgs();
    Tilt_getXYZ_ReturnThruPtr_pOutput(&latest_xyz);
    xTaskResumeAll_ExpectAndReturn(pdTRUE);
    
    /*
        => Absolute accumulated energy so far 12345 x 20Whr units
        => Vmax 235.8V since last daily report
        => Vmin 231.9V since last daily report
        => Vav 231.5V since last daily report
    */
    powerMonitor_getDataset_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    powerMonitor_getDataset_ReturnThruPtr_dataset_ptr(&power_calc_dataset);

    errorCode_getCode_ExpectAnyArgs();
    errorCode_getCode_ReturnThruPtr_p_idByte(&idByte);
    errorCode_getCode_ReturnThruPtr_p_valueByte(&valueByte);
    errorCode_eraseCode_Expect();

    get_random_delay_ms_ExpectAndReturn(9876);

    nwk_com_load_uplink_ExpectAndReturn( 9876/14, LOW_PRIORITY, QOS_2, "%s", true);
    compileUplink_Daily(billingData);
    TEST_ASSERT_EQUAL_STRING("B101016093026C60938753FFF20019FBFC0001000000F801000030390936090F090B0102", ascii_payload);
    /* Protocol v0.9
    TEST_ASSERT_EQUAL_STRING("B101016093026C60938753FFF20019FBFC0001000000F8010102", ascii_payload);
    */
    return;
}

//-----------------------------------------------------------------
void test_MissedPackets_Example(void)
{
_DEBUG_OUT_TEST_NAME

    nwk_com_load_uplink_ExpectAndReturn( 12, LOW_PRIORITY, QOS_0, "%s", true);
    compileUplink_MissedPackets(12, "00010203040506070809");
    TEST_ASSERT_EQUAL_STRING("DC010100010203040506070809", ascii_payload);
    return;
}

//-----------------------------------------------------------------
void test_CalendarConfirmation(void)
{
_DEBUG_OUT_TEST_NAME

    nwk_com_load_uplink_ExpectAndReturn( 132, LOW_PRIORITY, QOS_1, "%s", true);
    compileUplink_CalendarConfirmation((uint32_t)132, (uint32_t)0x12345678, (uint16_t)0x9876);
    TEST_ASSERT_EQUAL_STRING("DB0101123456789876", ascii_payload);
    return;
}

//-------------------------------------------------------------------------------------------
// void compileUplink_ProductionTestReport(void)
//-------------------------------------------------------------------------------------------
void test_ProductionTestReport_functionCalls()
{
_DEBUG_OUT_TEST_NAME
    powerMonitor_getCalibReadings_ExpectAnyArgsAndReturn(POWER_MONITOR_RESULT_OK);
    get_photo_LUX_ExpectAndReturn((float)213.123);
    get_sysmon_fault_flags_ExpectAndReturn(0x12345678);
    nwk_com_load_uplink_ExpectAndReturn( 0, HIGH_PRIORITY, QOS_2, "%s", true);
    compileUplink_ProductionTestReport();
}

void test_ProductionTestReport_correctPayload()
{
_DEBUG_OUT_TEST_NAME
    PowerCalcDataset powerCalDataset = {230.45F, 0.256F, 0.935F, 432.16F, 0, 0, 0, 0};
    powerMonitor_getCalibReadings_ExpectAndReturn(&powerCalDataset, POWER_MONITOR_RESULT_OK);
    powerMonitor_getCalibReadings_IgnoreArg_dataset_ptr();
    powerMonitor_getCalibReadings_ReturnMemThruPtr_dataset_ptr(&powerCalDataset, sizeof powerCalDataset);
    get_photo_LUX_ExpectAndReturn((float)100);
    get_sysmon_fault_flags_ExpectAndReturn(0x12345678);
    nwk_com_load_uplink_ExpectAnyArgsAndReturn(true);
    compileUplink_ProductionTestReport();
    TEST_ASSERT_EQUAL_STRING("A0010112345678436673333E83126F3F6F5C2943D8147B64", ascii_payload);
}

//-------------------------------------------------------------------------------------------
// void compileUplink_ProductionTestingDone(void)
//-------------------------------------------------------------------------------------------
void test_compileUplink_ProductionTestingDone()
{
_DEBUG_OUT_TEST_NAME
    nwk_com_load_uplink_ExpectAndReturn( 0, HIGH_PRIORITY, QOS_2, "%s", true);
    compileUplink_ProductionTestingDone();
    TEST_ASSERT_EQUAL_STRING("AF0101", ascii_payload);
}


