#include "unity.h"

#include "nwk_handler.c"

#include "mock_common.h"
#include "mock_systick.h"
#include "mock_debug.h"
#include "mock_sysmon.h"
#include "mock_nwk_com.h"
#include "mock_FreeRTOS.h"
#include "mock_task.h"
#include "mock_timers.h"
#include "mock_FreeRTOSMacros.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_productionMonitor.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);


void setUp(void)
{
    debugprint_Ignore();
    timeOfLastSessionSave = 0;
}
void tearDown(void)
{

}

//-----------------------------------------------------------------
void test_nwk_handler_run_RESET_to_SETUP()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_RESET;
    productionMonitor_getUnitIsUndertest_ExpectAndReturn(false);
    get_random_delay_ms_IgnoreAndReturn(1234);
    FreeRTOSDelay_Expect(1234);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_SETUP, nwk_handler_sm_state);
}
void test_nwk_handler_run_RESET_to_SETUP_underTestSkipDelay()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_RESET;
    productionMonitor_getUnitIsUndertest_ExpectAndReturn(true);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_SETUP, nwk_handler_sm_state);
}

void test_nwk_handler_run_setupFailFullResetNotRequired()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_SETUP;
    nwk_com_init_ExpectAndReturn(false);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_SETUP, nwk_handler_sm_state);    
}

void test_nwk_handler_run_SetupOkFullResetNotRequired()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_SETUP;
    nwk_com_init_ExpectAndReturn(true);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_JOIN, nwk_handler_sm_state);    
}

void test_nwk_handler_run_setupFailFullResetRequired()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_SETUP;
    nwk_com_init_ExpectAndReturn(false);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_SETUP, nwk_handler_sm_state);    
}

void test_nwk_handler_run_SetupOkFullResetRequired()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_SETUP;
    nwk_com_init_ExpectAndReturn(true);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_JOIN, nwk_handler_sm_state);    
}

void test_nwk_handler_run_joinWaitForTestManagerWhenUnderTest()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_JOIN;
    productionMonitor_getNetworkReadyForDevice_ExpectAndReturn(false);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_JOIN, nwk_handler_sm_state);    
}

void test_nwk_handler_run_joinFail()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_JOIN;
    productionMonitor_getNetworkReadyForDevice_ExpectAndReturn(true);
    nwk_com_connect_ExpectAndReturn(false);
    get_nwk_com_hw_needs_initialising_ExpectAndReturn(false);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_JOIN, nwk_handler_sm_state);    
}

void test_nwk_handler_run_joinOk()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_JOIN;
    productionMonitor_getNetworkReadyForDevice_ExpectAndReturn(true);
    nwk_com_connect_ExpectAndReturn(true);
    sysmon_status_uplink_enable_Expect();
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_IDLE, nwk_handler_sm_state);    
}

void test_nwk_handler_run_JOIN_to_SETUP()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_JOIN;
    productionMonitor_getNetworkReadyForDevice_ExpectAndReturn(true);
    nwk_com_connect_ExpectAndReturn(false);
    get_nwk_com_hw_needs_initialising_ExpectAndReturn(true);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_SETUP, nwk_handler_sm_state);    
}
//-----------------------------------------------------------------

static void mockRemainsInIdle()
{
    nwk_handler_sm_state = NWK_IDLE;
    get_nwk_com_is_connected_IgnoreAndReturn(true);
    get_nwk_com_hw_needs_initialising_IgnoreAndReturn(false);
    get_systick_sec_IgnoreAndReturn(0);
    get_nwk_com_settings_need_optimising_ExpectAndReturn(false);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_IDLE, nwk_handler_sm_state);
}

void test_nwk_handler_run_idleBroadcastSessionNeedsActivating()
{
_DEBUG_OUT_TEST_NAME
    get_nwk_com_broadcast_session_should_be_active_IgnoreAndReturn(true);
    get_nwk_com_broadcast_is_active_IgnoreAndReturn(false);
    nwk_com_update_broabcast_status_ExpectAndReturn(true, true);
    mockRemainsInIdle();
}
void test_nwk_handler_run_idleBroadcastSessionNeedsDeactivating()
{
_DEBUG_OUT_TEST_NAME
    get_nwk_com_broadcast_session_should_be_active_IgnoreAndReturn(false);
    get_nwk_com_broadcast_is_active_IgnoreAndReturn(true);
    nwk_com_update_broabcast_status_ExpectAndReturn(false, true); 
    mockRemainsInIdle();
}
void test_nwk_handler_run_idleBroadcastSessionNoChangeInactive()
{
_DEBUG_OUT_TEST_NAME
    get_nwk_com_broadcast_session_should_be_active_IgnoreAndReturn(false);
    get_nwk_com_broadcast_is_active_IgnoreAndReturn(false);
    mockRemainsInIdle();
}
void test_nwk_handler_run_idleBroadcastSessionNoChangeActive()
{
_DEBUG_OUT_TEST_NAME
    get_nwk_com_broadcast_session_should_be_active_IgnoreAndReturn(true);
    get_nwk_com_broadcast_is_active_IgnoreAndReturn(true);
    mockRemainsInIdle();
}

//-----------------------------------------------------------------

void test_nwk_handler_run_rejoinNetworkRequested()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_IDLE;
    get_nwk_com_broadcast_session_should_be_active_IgnoreAndReturn(false);
    get_nwk_com_broadcast_is_active_IgnoreAndReturn(false);
    get_nwk_com_is_connected_ExpectAndReturn(false);
    get_nwk_com_hw_needs_initialising_IgnoreAndReturn(false); 
    get_systick_sec_IgnoreAndReturn(0);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_JOIN, nwk_handler_sm_state);   
}

void test_nwk_handler_run_resetModuleRequested()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_IDLE;
    get_nwk_com_broadcast_session_should_be_active_IgnoreAndReturn(false);
    get_nwk_com_broadcast_is_active_IgnoreAndReturn(false);
    get_nwk_com_is_connected_IgnoreAndReturn(true);
    get_nwk_com_hw_needs_initialising_ExpectAndReturn(true); 
    get_systick_sec_IgnoreAndReturn(0);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_SETUP, nwk_handler_sm_state);   
}

void test_nwk_handler_run_resetModuleAndRejoinNetworkRequested()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_IDLE;
    get_nwk_com_broadcast_session_should_be_active_IgnoreAndReturn(false);
    get_nwk_com_broadcast_is_active_IgnoreAndReturn(false);
    get_nwk_com_is_connected_ExpectAndReturn(false);
    get_nwk_com_hw_needs_initialising_ExpectAndReturn(true); 
    get_systick_sec_IgnoreAndReturn(0);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_SETUP, nwk_handler_sm_state);   
}

//-----------------------------------------------------------------

void test_nwk_handler_run_doNotSaveSessionBeforeGapReached()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_IDLE;
    get_nwk_com_broadcast_session_should_be_active_IgnoreAndReturn(false);
    get_nwk_com_broadcast_is_active_IgnoreAndReturn(false);    
    get_nwk_com_is_connected_IgnoreAndReturn(true);
    get_nwk_com_hw_needs_initialising_IgnoreAndReturn(false);
    get_systick_sec_IgnoreAndReturn(SESSION_SAVE_GAP - 10);
    get_nwk_com_settings_need_optimising_IgnoreAndReturn(false);
    nwk_handler_run();
    get_systick_sec_IgnoreAndReturn(SESSION_SAVE_GAP + 1);
    nwk_com_save_session_info_Expect();
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_IDLE, nwk_handler_sm_state);
}

void test_nwk_handler_run_previousSessionSaveTimeStored()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_IDLE;
    get_nwk_com_broadcast_session_should_be_active_IgnoreAndReturn(false);
    get_nwk_com_broadcast_is_active_IgnoreAndReturn(false);    
    get_nwk_com_is_connected_IgnoreAndReturn(true);
    get_nwk_com_hw_needs_initialising_IgnoreAndReturn(false);
    get_systick_sec_ExpectAndReturn(SESSION_SAVE_GAP - 1);
        get_nwk_com_settings_need_optimising_IgnoreAndReturn(false);
    nwk_handler_run();
    get_systick_sec_ExpectAndReturn(SESSION_SAVE_GAP + 1);
    get_systick_sec_ExpectAndReturn(SESSION_SAVE_GAP + 1);
    nwk_com_save_session_info_Expect();
    nwk_handler_run();
    get_systick_sec_ExpectAndReturn(SESSION_SAVE_GAP + 10);
    nwk_handler_run();
    TEST_ASSERT_EQUAL( NWK_IDLE, nwk_handler_sm_state);
}

//-----------------------------------------------------------------

void test_get_nwk_handler_is_ready_inIdleStateWithInitialiseFlagSet()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_IDLE;
    get_nwk_com_is_connected_ExpectAndReturn(true);
    get_nwk_com_hw_needs_initialising_ExpectAndReturn(true);
    TEST_ASSERT_FALSE(get_nwk_handler_nwk_is_ready());
}
void test_get_nwk_handler_is_ready_inIdleStateWithJoinedFlagCleared()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_IDLE;
    get_nwk_com_is_connected_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(get_nwk_handler_nwk_is_ready());
}
void test_get_nwk_handler_is_ready_inResetState()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_RESET;
    get_nwk_com_is_connected_IgnoreAndReturn(true);
    get_nwk_com_hw_needs_initialising_IgnoreAndReturn(false);
    TEST_ASSERT_FALSE(get_nwk_handler_nwk_is_ready());
}
void test_get_nwk_handler_is_ready_inSetupState()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_SETUP;
    get_nwk_com_is_connected_IgnoreAndReturn(true);
    get_nwk_com_hw_needs_initialising_IgnoreAndReturn(false);
    TEST_ASSERT_FALSE(get_nwk_handler_nwk_is_ready());
}
void test_get_nwk_handler_is_ready_inJoinState()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_JOIN;
    get_nwk_com_is_connected_IgnoreAndReturn(true);
    get_nwk_com_hw_needs_initialising_IgnoreAndReturn(false);    
    TEST_ASSERT_FALSE(get_nwk_handler_nwk_is_ready());
}
void test_get_nwk_handler_is_ready_inIdleState()
{
_DEBUG_OUT_TEST_NAME
    nwk_handler_sm_state = NWK_IDLE;
    get_nwk_com_is_connected_IgnoreAndReturn(true);
    get_nwk_com_hw_needs_initialising_IgnoreAndReturn(false);    
    TEST_ASSERT_TRUE(get_nwk_handler_nwk_is_ready());
}

//-----------------------------------------------------------------

void test_recovery_routineCalledIfFlagSetFromIdle(){
_DEBUG_OUT_TEST_NAME  
    nwk_handler_sm_state = NWK_IDLE;  
    get_nwk_com_broadcast_session_should_be_active_IgnoreAndReturn(false);
    get_nwk_com_broadcast_is_active_IgnoreAndReturn(false);
    get_nwk_com_is_connected_IgnoreAndReturn(true);
    get_nwk_com_hw_needs_initialising_IgnoreAndReturn(false);
    get_systick_sec_IgnoreAndReturn(0);
    get_nwk_com_settings_need_optimising_ExpectAndReturn(true);
    nwk_com_optimise_settings_Expect();
    nwk_handler_run();
}
void test_recovery_routineNotCheckedIfFlagSetFromChangeToJoin(){
_DEBUG_OUT_TEST_NAME 
    nwk_handler_sm_state = NWK_IDLE;  
    get_nwk_com_broadcast_session_should_be_active_IgnoreAndReturn(false);
    get_nwk_com_broadcast_is_active_IgnoreAndReturn(false);
    get_nwk_com_is_connected_IgnoreAndReturn(false);
    get_nwk_com_hw_needs_initialising_IgnoreAndReturn(false);
    nwk_handler_run();
}
void test_recovery_routineNotCheckedIfFlagSetFromChangeToSetup(){
_DEBUG_OUT_TEST_NAME 
    nwk_handler_sm_state = NWK_IDLE;  
    get_nwk_com_broadcast_session_should_be_active_IgnoreAndReturn(false);
    get_nwk_com_broadcast_is_active_IgnoreAndReturn(false);
    get_nwk_com_is_connected_IgnoreAndReturn(true);
    get_nwk_com_hw_needs_initialising_IgnoreAndReturn(true);
    nwk_handler_run();
}

//-----------------------------------------------------------------
