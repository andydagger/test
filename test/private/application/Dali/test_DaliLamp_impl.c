#include <stdbool.h>

#include "DaliLamp_impl.h"

#include "mock_systick.h"
#include "mock_DaliAL.h"

#include "unity.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

TEST_FILE("ZaghaLamp_impl.c");

static dali_bus_status_t status_ok = DALI_BUS_STATUS_OK;
static dali_bus_status_t status_invalid = DALI_BUS_STATUS_INVALID;

void setUp(void)
{

}
void tearDown(void)
{

}

void run_brightnessToArc(uint8_t expectedArcLevel, uint8_t brightness)
{
	//the +1 ensures that the test cannot pass due to "residues" on the stack
	uint8_t arcLevel = expectedArcLevel + 1;
	TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_OK,
		ZaghaLamp_getArcLevelFromBrightness(brightness, &arcLevel));

	TEST_ASSERT_EQUAL_UINT8(expectedArcLevel, arcLevel);
}

/*******************************************************************************
	 ZAGHA_LAMP_RET ZaghaLamp_getArcLevelFromBrightness(
		 uint8_t brightness,
		 uint8_t* p_retVal);
*******************************************************************************/

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_0()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(0, 0);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_5()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(145, 5);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_10()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(170, 10);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_15()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(185, 15);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_20()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(196, 20);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_25()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(204, 25);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_30()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(210, 30);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_35()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(216, 35);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_40()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(221, 40);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_45()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(225, 45);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_50()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(229, 50);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_55()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(233, 55);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_60()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(236, 60);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_65()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(239, 65);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_70()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(241, 70);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_75()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(244, 75);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_80()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(246, 80);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_85()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(249, 85);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_90()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(251, 90);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_95()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(253, 95);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_100()
{
_DEBUG_OUT_TEST_NAME
	run_brightnessToArc(254, 100);
}

void test_getArcLevelFromBrightness_returnsErrorIfBrightnessIsOver100Percent()
{
_DEBUG_OUT_TEST_NAME

	uint8_t arcLevel;
	TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_ERR,
		ZaghaLamp_getArcLevelFromBrightness(101, &arcLevel));
}

void test_getArcLevelFromBrightness_returnsErrorIfArcPtrIsNull()
{
_DEBUG_OUT_TEST_NAME

	TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_ERR,
		ZaghaLamp_getArcLevelFromBrightness(0, NULL));
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_37()
{
_DEBUG_OUT_TEST_NAME
//N.B. this is the same value as 40%, as we are rounding up
	run_brightnessToArc(221, 37);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_83()
{
_DEBUG_OUT_TEST_NAME
//N.B. this is the same value as 85%, as we are rounding up
	run_brightnessToArc(249, 83);
}

void test_getArcLevelFromBrightness_GetsTheArcLevelForBrightness_9()
{
_DEBUG_OUT_TEST_NAME
//N.B. this is the same value as 10%, as we are rounding up
	run_brightnessToArc(170, 9);
}

void test_checkBrightnessLevel_withMinBrightness()
{
_DEBUG_OUT_TEST_NAME
	uint8_t actualLevel = 0;

	DaliAL_queryActualLevel_ExpectAnyArgsAndReturn(actualLevel);
	DaliAL_queryActualLevel_ReturnThruPtr_bus(&status_ok);

	TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_OK, ZaghaLamp_checkBrightnessLevel(0xff, 0));
}

void test_checkBrightnessLevel_withMaxBrightness()
{
_DEBUG_OUT_TEST_NAME
	uint8_t actualLevel = 255;

	DaliAL_queryActualLevel_ExpectAnyArgsAndReturn(actualLevel);
	DaliAL_queryActualLevel_ReturnThruPtr_bus(&status_ok);

	TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_OK, ZaghaLamp_checkBrightnessLevel(255, 0xff));
}

void test_checkBrightnessLevel_testBrightnessMismatch()
{
_DEBUG_OUT_TEST_NAME
	uint8_t actualLevel = 150;

	DaliAL_queryActualLevel_ExpectAnyArgsAndReturn(actualLevel);
	DaliAL_queryActualLevel_ReturnThruPtr_bus(&status_ok);

	TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_ERR, ZaghaLamp_checkBrightnessLevel(255, 0xff));
}

void test_checkBrightnessLevel_testDaliAL_queryActualLevelReturnsError()
{
_DEBUG_OUT_TEST_NAME
	uint8_t actualLevel = 150;

	DaliAL_queryActualLevel_ExpectAnyArgsAndReturn(actualLevel);
	DaliAL_queryActualLevel_ReturnThruPtr_bus(&status_invalid);

	TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_ERR, ZaghaLamp_checkBrightnessLevel(255, 0xff));
}
