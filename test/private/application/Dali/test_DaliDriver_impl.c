#include <stdint.h>

#include "unity.h"

#include "DaliDriver_impl.h"

#include "mock_DaliAL.h"
#include "mock_debug.h"

#include "mock_DaliController.h"
#include "mock_common.h"

TEST_FILE("ZaghaDriver_impl.c");

#define _DEBUG_OUT_TEST_NAME()    printf(" %s",__func__)

static dali_bus_status_t status_ok = DALI_BUS_STATUS_OK;
static dali_bus_status_t status_invalid = DALI_BUS_STATUS_INVALID;

void setUp(void)
{
	debugprint_Ignore();
}

void tearDown(void)
{

}

/*******************************************************************************
	ZAGHA_DRIVER_RET ZaghaDriver_readGTIN(ZaghaDriver_Information* p_info)
*******************************************************************************/

void test_readGTIN_AllOk()
{
	_DEBUG_OUT_TEST_NAME();

	//DTR1 = Memory bank 0
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	//DTR0 = Memory address 3
	DaliAL_dataTransferRegister0_Expect(3, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	//Read the memory
	uint8_t gtin[6] = {0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC};
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, gtin[0]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, gtin[1]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, gtin[2]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, gtin[3]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, gtin[4]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, gtin[5]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	ZaghaDriver_Information info = {0};
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_readGTIN(&info));
	TEST_ASSERT_EQUAL_HEX8_ARRAY(gtin, info.gtin, 6);
}

void test_readGTIN_AllOk_DifferentGTIN()
{
	_DEBUG_OUT_TEST_NAME();

	//DTR1 = Memory bank 0
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	//DTR0 = Memory address 3
	DaliAL_dataTransferRegister0_Expect(3, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	//Read the memory
	uint8_t gtin[6] = {0xF4, 0x2E, 0x93, 0x88, 0x8E, 0x0B};
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, gtin[0]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, gtin[1]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, gtin[2]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, gtin[3]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, gtin[4]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, gtin[5]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	ZaghaDriver_Information info = {0};
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_readGTIN(&info));
	TEST_ASSERT_EQUAL_HEX8_ARRAY(gtin, info.gtin, 6);
}

void test_readGTINreturnsErrorIfPtrIsNull()
{
	_DEBUG_OUT_TEST_NAME();

	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_readGTIN(NULL));
}
void test_readGTINreturnsErrorIfDTR1Fails()
{
	_DEBUG_OUT_TEST_NAME();

	//DTR1 = Memory bank 0
	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_invalid);

	ZaghaDriver_Information info = {0};
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_readGTIN(&info));
}

void test_readGTINreturnsErrorIfDTRFails()
{
	_DEBUG_OUT_TEST_NAME();

	//DTR1 = Memory bank 0
	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_invalid);

	ZaghaDriver_Information info = {0};
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_readGTIN(&info));
}


void test_readGTINreturnsErrorIfReadMemoryFails()
{
	_DEBUG_OUT_TEST_NAME();

	//DTR1 = Memory bank 0
	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	ZaghaDriver_Information info = {0};
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_readGTIN(&info));
}

void test_readGTINreturnsErrorIfReadMemoryFails_DifferentIndex()
{
	_DEBUG_OUT_TEST_NAME();

	//DTR1 = Memory bank 0
	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	ZaghaDriver_Information info = {0};
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_readGTIN(&info));
}

/*******************************************************************************
	ZAGHA_DRIVER_RET ZaghaDriver_readSerial(ZaghaDriver_Information* p_info)
*******************************************************************************/

void test_readSerial_AllOk()
{
	_DEBUG_OUT_TEST_NAME();

	//DTR1 = Memory bank 0
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	//DTR0 = Memory address 11
	DaliAL_dataTransferRegister0_Expect(11, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	//Read the memory
	uint8_t serial[8] = {0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0};
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[0]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[1]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[2]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[3]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[4]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[5]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[6]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[7]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	ZaghaDriver_Information info = {0};
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_readSerial(&info));
	TEST_ASSERT_EQUAL_HEX8_ARRAY(serial, info.serial, 8);
}

void test_readSerial_AllOk_DifferentValues()
{
	_DEBUG_OUT_TEST_NAME();

	//DTR1 = Memory bank 0
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	//DTR0 = Memory address 11
	DaliAL_dataTransferRegister0_Expect(11, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	//Read the memory
	uint8_t serial[8] = {0x92, 0x44, 0xA1, 0xA2, 0x00, 0x49, 0xB2, 0x2F};
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[0]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[1]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[2]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[3]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[4]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[5]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[6]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, serial[7]);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	ZaghaDriver_Information info = {0};
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_readSerial(&info));
	TEST_ASSERT_EQUAL_HEX8_ARRAY(serial, info.serial, 8);
}

void test_readSerial_returnsErrorIfPtrIsNull()
{
	_DEBUG_OUT_TEST_NAME();

	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_readSerial(NULL));
}

void test_readSerial_returnsErrorIfDTR1Fails()
{
	_DEBUG_OUT_TEST_NAME();

	//DTR1 = Memory bank 0
	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_invalid);

	ZaghaDriver_Information info = {0};
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_readSerial(&info));
}

void test_readSerial_returnsErrorIfDTRFails()
{
	_DEBUG_OUT_TEST_NAME();

	//DTR1 = Memory bank 0
	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_invalid);

	ZaghaDriver_Information info = {0};
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_readSerial(&info));
}

void test_readSerial_returnsErrorIfReadMemoryFails()
{
	_DEBUG_OUT_TEST_NAME();

	//DTR1 = Memory bank 0
	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	ZaghaDriver_Information info = {0};
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_readSerial(&info));
}

void test_readSerial_returnsErrorIfReadMemoryFails_DifferentIndex()
{
	_DEBUG_OUT_TEST_NAME();

	//DTR1 = Memory bank 0
	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	ZaghaDriver_Information info = {0};
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_readSerial(&info));
}


/*******************************************************************************
	bool ZaghaDriver_get101VersionInformation(uint8_t* p_major,
		uint8_t* p_minor)
*******************************************************************************/

void test_get101VersionInformation_AllOk()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t version = 4 << 2 | 1;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x15, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, version);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_get101VersionInformation(&major, &minor) );
}

void test_get101VersionInformation_CorrectVersion()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t version = 4 << 2 | 1;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x15, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, version);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_get101VersionInformation(&major, &minor) );
	TEST_ASSERT_EQUAL_UINT8(4, major);
	TEST_ASSERT_EQUAL_UINT8(1, minor);
}

void test_get101VersionInformation_CorrectVersion_DifferentValues()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t version = 35 << 2 | 2;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x15, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, version);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_get101VersionInformation(&major, &minor) );
	TEST_ASSERT_EQUAL_UINT8(35, major);
	TEST_ASSERT_EQUAL_UINT8(2, minor);
}

void test_get101VersionInformation_returnsErrorIfMajorVersionPrtIsNULL()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get101VersionInformation(NULL, &minor) );
}

void test_get101VersionInformation_returnsErrorIfMinorVersionPrtIsNULL()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t major;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get101VersionInformation(&major, NULL) );
}

void test_get101VersionInformation_returnsErrorIfDTR1Fails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get101VersionInformation(&major, &minor) );
}

void test_get101VersionInformation_returnsErrorIfDTRFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get101VersionInformation(&major, &minor) );
}

void test_get101VersionInformation_returnsErrorIfReadMemoryFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get101VersionInformation(&major, &minor) );
}


/*******************************************************************************
	bool ZaghaDriver_get102VersionInformation(uint8_t* p_major,
		uint8_t* p_minor)
*******************************************************************************/

void test_get102VersionInformation_AllOk()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t version = 4 << 2 | 1;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x16, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, version);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_get102VersionInformation(&major, &minor) );
}

void test_get102VersionInformation_CorrectVersion()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t version = 4 << 2 | 1;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x16, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, version);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_get102VersionInformation(&major, &minor) );
	TEST_ASSERT_EQUAL_UINT8(4, major);
	TEST_ASSERT_EQUAL_UINT8(1, minor);
}

void test_get102VersionInformation_CorrectVersion_DifferentValues()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t version = 35 << 2 | 2;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x16, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, version);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_get102VersionInformation(&major, &minor) );
	TEST_ASSERT_EQUAL_UINT8(35, major);
	TEST_ASSERT_EQUAL_UINT8(2, minor);
}

void test_get102VersionInformation_returnsErrorIfMajorVersionPrtIsNULL()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get102VersionInformation(NULL, &minor) );
}

void test_get102VersionInformation_returnsErrorIfMinorVersionPrtIsNULL()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t major;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get102VersionInformation(&major, NULL) );
}

void test_get102VersionInformation_returnsErrorIfDTR1Fails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get102VersionInformation(&major, &minor) );
}

void test_get102VersionInformation_returnsErrorIfDTRFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get102VersionInformation(&major, &minor) );
}

void test_get102VersionInformation_returnsErrorIfReadMemoryFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get102VersionInformation(&major, &minor) );
}

/*******************************************************************************
	bool ZaghaDriver_get103VersionInformation(uint8_t* p_major,
		uint8_t* p_minor)
*******************************************************************************/

void test_get103VersionInformation_AllOk()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t version = 4 << 2 | 1;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x17, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, version);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_get103VersionInformation(&major, &minor) );
}

void test_get103VersionInformation_CorrectVersion()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t version = 4 << 2 | 1;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x17, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, version);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_get103VersionInformation(&major, &minor) );
	TEST_ASSERT_EQUAL_UINT8(4, major);
	TEST_ASSERT_EQUAL_UINT8(1, minor);
}

void test_get103VersionInformation_CorrectVersion_DifferentValues()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t version = 35 << 2 | 2;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x17, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, version);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_get103VersionInformation(&major, &minor) );
	TEST_ASSERT_EQUAL_UINT8(35, major);
	TEST_ASSERT_EQUAL_UINT8(2, minor);
}

void test_get103VersionInformation_returnsErrorIfMajorVersionPrtIsNULL()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get103VersionInformation(NULL, &minor) );
}

void test_get103VersionInformation_returnsErrorIfMinorVersionPrtIsNULL()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t major;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get103VersionInformation(&major, NULL) );
}

void test_get103VersionInformation_returnsErrorIfDTR1Fails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get103VersionInformation(&major, &minor) );
}

void test_get103VersionInformation_returnsErrorIfDTRFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get103VersionInformation(&major, &minor) );
}

void test_get103VersionInformation_returnsErrorIfReadMemoryFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_get103VersionInformation(&major, &minor) );
}


/*******************************************************************************
	bool ZaghaDriver_getFirmwareVersionInformation(uint8_t* p_major,
		uint8_t* p_minor)
*******************************************************************************/

void test_getFirmwareVersionInformation_AllOk()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t versionMajor = 1;
	uint8_t versionMinor = 3;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x09, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMajor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMinor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_getFirmwareVersionInformation(&major, &minor) );
}

void test_getFirmwareVersionInformation_CorrectVersion()
{
	_DEBUG_OUT_TEST_NAME();

   uint8_t versionMajor = 1;
	uint8_t versionMinor = 3;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x09, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMajor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMinor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_getFirmwareVersionInformation(&major, &minor) );
	TEST_ASSERT_EQUAL_UINT8(1, major);
	TEST_ASSERT_EQUAL_UINT8(3, minor);
}

void test_getFirmwareVersionInformation_CorrectVersion_DifferentValues()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t versionMajor = 12;
	uint8_t versionMinor = 9;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x09, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMajor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMinor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_getFirmwareVersionInformation(&major, &minor) );
	TEST_ASSERT_EQUAL_UINT8(12, major);
	TEST_ASSERT_EQUAL_UINT8(9, minor);
}

void test_getFirmwareVersionInformation_returnsErrorIfMajorVersionPrtIsNULL()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getFirmwareVersionInformation(NULL, &minor) );
}

void test_getFirmwareVersionInformation_returnsErrorIfMinorVersionPrtIsNULL()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t major;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getFirmwareVersionInformation(&major, NULL) );
}

void test_getFirmwareVersionInformation_returnsErrorIfDTR1Fails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getFirmwareVersionInformation(&major, &minor) );
}

void test_getFirmwareVersionInformation_returnsErrorIfDTRFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getFirmwareVersionInformation(&major, &minor) );
}

void test_getFirmwareVersionInformation_returnsErrorIfReadMemoryFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getFirmwareVersionInformation(&major, &minor) );
}


void test_getFirmwareVersionInformation_returnsErrorIfReadMemoryFailsSecondTime()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getFirmwareVersionInformation(&major, &minor) );
}



/*******************************************************************************
	bool ZaghaDriver_getHardwareVersionInformation(uint8_t* p_major,
		uint8_t* p_minor)
*******************************************************************************/

void test_getHardwareVersionInformation_AllOk()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t versionMajor = 1;
	uint8_t versionMinor = 3;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x13, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMajor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMinor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_getHardwareVersionInformation(&major, &minor) );
}

void test_getHardwareVersionInformation_CorrectVersion()
{
	_DEBUG_OUT_TEST_NAME();

   uint8_t versionMajor = 1;
	uint8_t versionMinor = 3;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x13, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMajor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMinor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_getHardwareVersionInformation(&major, &minor) );
	TEST_ASSERT_EQUAL_UINT8(1, major);
	TEST_ASSERT_EQUAL_UINT8(3, minor);
}

void test_getHardwareVersionInformation_CorrectVersion_DifferentValues()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t versionMajor = 12;
	uint8_t versionMinor = 9;
	DaliAL_dataTransferRegister1_Expect(0, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x13, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMajor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMinor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_getHardwareVersionInformation(&major, &minor) );
	TEST_ASSERT_EQUAL_UINT8(12, major);
	TEST_ASSERT_EQUAL_UINT8(9, minor);
}

void test_getHardwareVersionInformation_returnsErrorIfMajorVersionPrtIsNULL()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getHardwareVersionInformation(NULL, &minor) );
}

void test_getHardwareVersionInformation_returnsErrorIfMinorVersionPrtIsNULL()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t major;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getHardwareVersionInformation(&major, NULL) );
}

void test_getHardwareVersionInformation_returnsErrorIfDTR1Fails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getHardwareVersionInformation(&major, &minor) );
}

void test_getHardwareVersionInformation_returnsErrorIfDTRFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getHardwareVersionInformation(&major, &minor) );
}

void test_getHardwareVersionInformation_returnsErrorIfReadMemoryFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getHardwareVersionInformation(&major, &minor) );
}

void test_getHardwareVersionInformation_returnsErrorIfReadMemoryFailsSecondTime()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getHardwareVersionInformation(&major, &minor) );
}

/*******************************************************************************
	bool ZaghaDriver_getSRVersionInformation(uint8_t* p_major,
		uint8_t* p_minor)
*******************************************************************************/

void test_getSRVersionInformation_AllOk()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t versionMajor = 1;
	uint8_t versionMinor = 3;
	DaliAL_dataTransferRegister1_Expect(50, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x08, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMajor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMinor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_getSRVersionInformation(&major, &minor) );
}

void test_getSRVersionInformation_CorrectVersion()
{
	_DEBUG_OUT_TEST_NAME();

   uint8_t versionMajor = 1;
	uint8_t versionMinor = 3;
	DaliAL_dataTransferRegister1_Expect(50, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x08, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMajor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMinor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_getSRVersionInformation(&major, &minor) );
	TEST_ASSERT_EQUAL_UINT8(1, major);
	TEST_ASSERT_EQUAL_UINT8(3, minor);
}

void test_getSRVersionInformation_CorrectVersion_DifferentValues()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t versionMajor = 12;
	uint8_t versionMinor = 9;
	DaliAL_dataTransferRegister1_Expect(50, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_Expect(0x08, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMajor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAndReturn(DALI_ADDRESS_IGNORE, NULL, versionMinor);
	DaliAL_readMemoryLocation_IgnoreArg_bus();
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_getSRVersionInformation(&major, &minor) );
	TEST_ASSERT_EQUAL_UINT8(12, major);
	TEST_ASSERT_EQUAL_UINT8(9, minor);
}

void test_getSRVersionInformation_returnsErrorIfMajorVersionPrtIsNULL()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getSRVersionInformation(NULL, &minor) );
}

void test_getSRVersionInformation_returnsErrorIfMinorVersionPrtIsNULL()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t major;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getSRVersionInformation(&major, NULL) );
}

void test_getSRVersionInformation_returnsErrorIfDTR1Fails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_invalid);
	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getSRVersionInformation(&major, &minor) );
}

void test_getSRVersionInformation_returnsErrorIfDTRFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getSRVersionInformation(&major, &minor) );
}

void test_getSRVersionInformation_returnsErrorIfReadMemoryFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getSRVersionInformation(&major, &minor) );
}

void test_getSRVersionInformation_returnsErrorIfReadMemoryFailsSecondTime()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_dataTransferRegister1_ExpectAnyArgs();
	DaliAL_dataTransferRegister1_ReturnThruPtr_bus(&status_ok);

	DaliAL_dataTransferRegister0_ExpectAnyArgs();
	DaliAL_dataTransferRegister0_ReturnThruPtr_bus(&status_ok);

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_ok);
	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(0);
	DaliAL_readMemoryLocation_ReturnThruPtr_bus(&status_invalid);

	uint8_t major;
	uint8_t minor;
	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_getSRVersionInformation(&major, &minor) );
}

/*******************************************************************************
	ZAGHA_DRIVER_RET ZaghaDriver_readPhyMin(ZaghaDriver_Information* p_info)
*******************************************************************************/

void test_readPhyMin_AllOk() {
	_DEBUG_OUT_TEST_NAME();

	uint8_t phyMin = 0x12;
	DaliAL_queryPhysicalMinimum_ExpectAnyArgsAndReturn(phyMin);
	DaliAL_queryPhysicalMinimum_ReturnThruPtr_bus(&status_ok);

	struct dali_app_state state = {
		.driver_count 		= 1,
		.driver_address 	= {
			1
		}
	};

	uint8_t measured = 0;

	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_readPhyMin(&state, &measured));
	TEST_ASSERT_EQUAL_HEX8(phyMin, measured);
}

void test_readPhyMin_AllOk_DifferentValues() {
	_DEBUG_OUT_TEST_NAME();

	uint8_t phyMin = 0x07;
	DaliAL_queryPhysicalMinimum_ExpectAnyArgsAndReturn(phyMin);
	DaliAL_queryPhysicalMinimum_ReturnThruPtr_bus(&status_ok);

	struct dali_app_state state = {
		.driver_count 		= 1,
		.driver_address 	= {
			1
		}
	};

	uint8_t measured = 0;

	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_readPhyMin(&state, &measured));
	TEST_ASSERT_EQUAL_HEX8(phyMin, measured);
}

void test_readPhyMin_returnsErrorIfPtrIsNull() {
	_DEBUG_OUT_TEST_NAME();

	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_readPhyMin(NULL, NULL));
}

void test_readPhyMin_returnsErrorIfDALIFails() {
	_DEBUG_OUT_TEST_NAME();

	DaliAL_queryPhysicalMinimum_ExpectAnyArgsAndReturn(0);
	DaliAL_queryPhysicalMinimum_ReturnThruPtr_bus(&status_invalid);

	struct dali_app_state state = {
		.driver_count 		= 1,
		.driver_address 	= {
			1
		}
	};

	uint8_t measured = 0;

	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_ERR, ZaghaDriver_readPhyMin(&state, &measured));
}

void test_readPhyMin_multiple_driver() {
	_DEBUG_OUT_TEST_NAME();

	struct dali_app_state state = {
		.driver_count 		= 4,
		.driver_address 	= {
			1, 11, 21, 32
		}
	};

	uint8_t minimums[] = {
		0x03, 0x00, 0x04, 0x02
	};

	for (uint8_t i = 0; i < state.driver_count; i++) {
		DaliAL_queryPhysicalMinimum_ExpectAndReturn(state.driver_address[i], NULL, minimums[i]);
		DaliAL_queryPhysicalMinimum_IgnoreArg_bus();
		DaliAL_queryPhysicalMinimum_ReturnThruPtr_bus(&status_ok);
	}

	uint8_t measured = 0;

	TEST_ASSERT_EQUAL_INT(ZAGHA_DRIVER_RET_OK, ZaghaDriver_readPhyMin(&state, &measured));
	TEST_ASSERT_EQUAL_UINT8(0x04, measured);
}
