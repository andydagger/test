#include "unity.h"
#include "stdbool.h"
#include "DaliController_impl.c"
#include "mock_DaliDriver.h"
#include "mock_DaliLamp.h"
#include "mock_DaliAL.h"
#include "mock_DaliLamp.h"
#include "mock_sysmon.h"
#include "mock_common.h"
#include "mock_systick.h"

#define _DEBUG_OUT_TEST_NAME()    printf(" %s",__func__)

#define DRIVER_IS_UNLOCKED  (true)
#define DRIVER_IS_LOCKED  (false)

TEST_FILE("DaliController_impl.c");

static dali_bus_status_t status_ok = DALI_BUS_STATUS_OK;
static dali_bus_status_t status_invalid = DALI_BUS_STATUS_INVALID;

/*******************************************************************************
	Callbacks for the mocks and Expected values.
*******************************************************************************/

void setUp(void)
{

}

void tearDown(void)
{

}

/*******************************************************************************
	ZAGHA_CTRL_RET ZaghaController_checkState().
*******************************************************************************/
void queryDiagnosticsMock(uint8_t *diagnostics)
{
	DaliAL_dataTransferRegister1_Expect(124, NULL);
	DaliAL_dataTransferRegister1_IgnoreArg_bus();

	DaliAL_dataTransferRegister0_Expect(12, NULL);
	DaliAL_dataTransferRegister0_IgnoreArg_bus();

	DaliAL_readMemoryLocation_ExpectAnyArgsAndReturn(*diagnostics);
}

void DaliLayerMocking(uint8_t TestStatus, uint8_t FailureTestStatus, uint16_t expectedDaliFault)
{
	uint8_t addresses[] = {
		DALI_ADDRESS_IGNORE
	};

	DaliAL_queryStatus_ExpectAnyArgsAndReturn(TestStatus);
	DaliAL_queryStatus_ReturnThruPtr_bus(&status_ok);

	DaliAL_queryFailureStatus_ExpectAnyArgsAndReturn(FailureTestStatus);
	DaliAL_queryFailureStatus_ReturnThruPtr_bus(&status_ok);

	ZaghaLamp_getArcReadBackFault_ExpectAndReturn(false);

	sysmon_update_dali_faults_Expect(expectedDaliFault);

	ZAGHA_CTRL_RET ret = ZaghaController_checkState(ARRAY_LEN(addresses), addresses);
	TEST_ASSERT_EQUAL_INT(ZAGHA_CTRL_RET_OK, ret);
}

void test_ZaghaController_checkState_testControlGearFailureIsDriverFaultFlag()
{
	_DEBUG_OUT_TEST_NAME();

	DaliLayerMocking(DALI_DRIVER_STATUS_CONTROL_GEAR_FAILURE, 0x0, DRIVER_FAULT_FLAG);
}

void test_ZaghaController_checkState_testLampFailureIsLampFaultFlag()
{
	_DEBUG_OUT_TEST_NAME();
	DaliLayerMocking(DALI_DRIVER_STATUS_LAMP_FAILURE, 0x0, LAMP_FAULT_FLAG);
}

void test_ZaghaController_checkState_testShortCircuitIsShortCircuitFlag()
{
	_DEBUG_OUT_TEST_NAME();
	DaliLayerMocking(0x0, DALI_DRIVER_FAILURE_STATUS_SHORT_CIRCUIT, SHORT_CIRCUIT_FLAG);
}

void test_ZaghaController_checkState_testOpenCircuitIsOpenCircuitFlag()
{
	_DEBUG_OUT_TEST_NAME();
	DaliLayerMocking(0x0, DALI_DRIVER_FAILURE_STATUS_OPEN_CIRCUIT, OPEN_CIRCUIT_FLAG);
}

void test_ZaghaController_checkState_testLoadDecreaseIsLoadDecreaseFlag()
{
	_DEBUG_OUT_TEST_NAME();
	DaliLayerMocking(0x0, DALI_DRIVER_FAILURE_STATUS_LOAD_DECREASE, LOAD_DECREASED_FLAG);
}

void test_ZaghaController_checkState_testLoadIncreaseIsLoadIncreaseFlag()
{
	_DEBUG_OUT_TEST_NAME();
	DaliLayerMocking(0x0, DALI_DRIVER_FAILURE_STATUS_LOAD_INCREASE, LOAD_INCREASED_FLAG);
}

void test_ZaghaController_checkState_testCurrentProtectorIsCurrentProtectorFlag()
{
	_DEBUG_OUT_TEST_NAME();
	DaliLayerMocking(0x0, DALI_DRIVER_FAILURE_STATUS_CURRENT_PROTECTOR_ACTIVE, CURRENT_PROTECT_FLAG);
}

void test_ZaghaController_checkState_testThermalShutdownIsThermalShutdownFlag()
{
	_DEBUG_OUT_TEST_NAME();
	DaliLayerMocking(0x0, DALI_DRIVER_FAILURE_STATUS_THERMAL_SHUTDOWN, THERMAL_SHUT_DOWN_FLAG);
}

void test_ZaghaController_checkState_testThermalOverloadLightReducedIsThermalOverloadFlag()
{
	_DEBUG_OUT_TEST_NAME();
	DaliLayerMocking(0x0, DALI_DRIVER_FAILURE_STATUS_THERMAL_OVERLOAD_LIGHT_REDUCED, THERMAL_OVERLOAD_FLAG);
}

void test_ZaghaController_checkState_testRefMeasurmentFailedIsRefMeasurementFailedFlag()
{
	_DEBUG_OUT_TEST_NAME();
	DaliLayerMocking(0x0, DALI_DRIVER_FAILURE_STATUS_REFERENCE_MEASUREMENT_FAILED, REF_MEASUREMENT_FAILED);
}

void test_ZaghaController_checkState_testQueryStatusReturnsError()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t addresses[] = {
		DALI_ADDRESS_IGNORE
	};

	DaliAL_queryStatus_ExpectAnyArgsAndReturn(DALI_DRIVER_STATUS_LAMP_FAILURE);
	DaliAL_queryStatus_ReturnThruPtr_bus(&status_invalid);

	DaliAL_queryFailureStatus_ExpectAnyArgsAndReturn(DALI_DRIVER_FAILURE_STATUS_NONE);

	ZaghaLamp_getArcReadBackFault_ExpectAndReturn(false);

	sysmon_update_dali_faults_Expect(0x00);

	ZAGHA_CTRL_RET ret = ZaghaController_checkState(ARRAY_LEN(addresses), addresses);
	TEST_ASSERT_EQUAL_INT(ZAGHA_CTRL_RET_ERROR, ret);
}


void test_ZaghaController_checkState_queryFailureStatusUsesTheDeviceType()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t addresses[] = {
		DALI_ADDRESS_IGNORE
	};

	DaliAL_queryStatus_ExpectAnyArgsAndReturn(DALI_DRIVER_STATUS_NONE);

	DaliAL_queryFailureStatus_ExpectAndReturn(DALI_ADDRESS_IGNORE, DALI_DEVICE_TYPE_LED, NULL, DALI_DRIVER_FAILURE_STATUS_NONE);
	DaliAL_queryFailureStatus_IgnoreArg_bus();

	ZaghaLamp_getArcReadBackFault_ExpectAndReturn(false);

	sysmon_update_dali_faults_Expect(0x00);

	ZAGHA_CTRL_RET ret = ZaghaController_checkState(ARRAY_LEN(addresses), addresses);
	TEST_ASSERT_EQUAL_INT(ZAGHA_CTRL_RET_ERROR, ret);
}

void test_ZaghaController_checkState_testQueryFailureStatusReturnsError()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t addresses[] = {
		DALI_ADDRESS_IGNORE
	};

	DaliAL_queryStatus_ExpectAnyArgsAndReturn(DALI_DRIVER_STATUS_NONE);

	DaliAL_queryFailureStatus_ExpectAnyArgsAndReturn(DALI_DRIVER_FAILURE_STATUS_REFERENCE_MEASUREMENT_FAILED);
	DaliAL_queryFailureStatus_ReturnThruPtr_bus(&status_invalid);

	ZaghaLamp_getArcReadBackFault_ExpectAndReturn(false);

	sysmon_update_dali_faults_Expect(0x00);

	ZAGHA_CTRL_RET ret = ZaghaController_checkState(ARRAY_LEN(addresses), addresses);
	TEST_ASSERT_EQUAL_INT(ZAGHA_CTRL_RET_ERROR, ret);
}

void test_ZaghaController_checkState_testMultipleReturnStates1()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t addresses[] = {
		DALI_ADDRESS_IGNORE
	};

	DaliAL_queryStatus_ExpectAnyArgsAndReturn(DALI_DRIVER_STATUS_LAMP_FAILURE);
	DaliAL_queryStatus_ReturnThruPtr_bus(&status_ok);

	DaliAL_queryFailureStatus_ExpectAnyArgsAndReturn(DALI_DRIVER_FAILURE_STATUS_REFERENCE_MEASUREMENT_FAILED);
	DaliAL_queryFailureStatus_ReturnThruPtr_bus(&status_ok);

	ZaghaLamp_getArcReadBackFault_ExpectAndReturn(false);

	sysmon_update_dali_faults_Expect(((uint16_t)REF_MEASUREMENT_FAILED | (uint16_t)LAMP_FAULT_FLAG));

	ZAGHA_CTRL_RET ret = ZaghaController_checkState(ARRAY_LEN(addresses), addresses);
	TEST_ASSERT_EQUAL_INT(ZAGHA_CTRL_RET_OK, ret);
}

void test_ZaghaController_checkState_testMultipleReturnStates2()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t addresses[] = {
		DALI_ADDRESS_IGNORE
	};

	uint8_t queryTestStatus = 0;
	uint8_t queryFailureTestStatus = 0;

	queryTestStatus = ((uint8_t)DALI_DRIVER_STATUS_LAMP_FAILURE | (uint8_t)DALI_DRIVER_STATUS_CONTROL_GEAR_FAILURE);
	DaliAL_queryStatus_ExpectAnyArgsAndReturn(queryTestStatus);
	DaliAL_queryStatus_ReturnThruPtr_bus(&status_ok);

	queryFailureTestStatus = (uint8_t)DALI_DRIVER_FAILURE_STATUS_REFERENCE_MEASUREMENT_FAILED;
	DaliAL_queryFailureStatus_ExpectAnyArgsAndReturn(queryFailureTestStatus);
	DaliAL_queryFailureStatus_ReturnThruPtr_bus(&status_ok);

	ZaghaLamp_getArcReadBackFault_ExpectAndReturn(false);

	sysmon_update_dali_faults_Expect(((uint16_t)REF_MEASUREMENT_FAILED | (uint16_t)LAMP_FAULT_FLAG | (uint16_t)DRIVER_FAULT_FLAG ));

	ZAGHA_CTRL_RET ret = ZaghaController_checkState(ARRAY_LEN(addresses), addresses);
	TEST_ASSERT_EQUAL_INT(ZAGHA_CTRL_RET_OK, ret);
}

void test_ZaghaController_checkState_testMultipleReturnStates3()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t addresses[] = {
		DALI_ADDRESS_IGNORE
	};

	uint8_t queryTestStatus = 0;
	uint8_t queryFailureTestStatus = 0;

	queryTestStatus = ((uint8_t)DALI_DRIVER_STATUS_LAMP_FAILURE | (uint8_t)DALI_DRIVER_STATUS_CONTROL_GEAR_FAILURE);
	DaliAL_queryStatus_ExpectAnyArgsAndReturn(queryTestStatus);
	DaliAL_queryStatus_ReturnThruPtr_bus(&status_ok);

	queryFailureTestStatus = ((uint8_t)DALI_DRIVER_FAILURE_STATUS_REFERENCE_MEASUREMENT_FAILED | (uint8_t)DALI_DRIVER_FAILURE_STATUS_LOAD_INCREASE);
	DaliAL_queryFailureStatus_ExpectAnyArgsAndReturn(queryFailureTestStatus);
	DaliAL_queryFailureStatus_ReturnThruPtr_bus(&status_ok);

	ZaghaLamp_getArcReadBackFault_ExpectAndReturn(false);

	sysmon_update_dali_faults_Expect((  (uint16_t)REF_MEASUREMENT_FAILED |
										(uint16_t)LAMP_FAULT_FLAG |
										(uint16_t)DRIVER_FAULT_FLAG |
										(uint16_t)LOAD_INCREASED_FLAG) );

	ZAGHA_CTRL_RET ret = ZaghaController_checkState(ARRAY_LEN(addresses), addresses);
	TEST_ASSERT_EQUAL_INT(ZAGHA_CTRL_RET_OK, ret);
}


void test_ZaghaController_checkState_setsTheArcReadBackFlag()
{
	_DEBUG_OUT_TEST_NAME();

	uint8_t addresses[] = {
		DALI_ADDRESS_IGNORE
	};

	uint8_t queryTestStatus = 0;
	uint8_t queryFailureTestStatus = 0;

	queryTestStatus = ((uint8_t)DALI_DRIVER_STATUS_LAMP_FAILURE | (uint8_t)DALI_DRIVER_STATUS_CONTROL_GEAR_FAILURE);
	DaliAL_queryStatus_ExpectAnyArgsAndReturn(queryTestStatus);
	DaliAL_queryStatus_ReturnThruPtr_bus(&status_ok);

	queryFailureTestStatus = ((uint8_t)DALI_DRIVER_FAILURE_STATUS_REFERENCE_MEASUREMENT_FAILED | (uint8_t)DALI_DRIVER_FAILURE_STATUS_LOAD_INCREASE);
	DaliAL_queryFailureStatus_ExpectAnyArgsAndReturn(queryFailureTestStatus);
	DaliAL_queryFailureStatus_ReturnThruPtr_bus(&status_ok);

	ZaghaLamp_getArcReadBackFault_ExpectAndReturn(true);

	sysmon_update_dali_faults_Expect((  (uint16_t)REF_MEASUREMENT_FAILED |
										(uint16_t)LAMP_FAULT_FLAG |
										(uint16_t)DRIVER_FAULT_FLAG |
										(uint16_t)LOAD_INCREASED_FLAG |
										(uint16_t)READ_BACK_DIFF_FLAG));

	ZAGHA_CTRL_RET ret = ZaghaController_checkState(ARRAY_LEN(addresses), addresses);
	TEST_ASSERT_EQUAL_INT(ZAGHA_CTRL_RET_OK, ret);
}

void test_timeToDaliPing_BeforeFirstPing()
{
	_DEBUG_OUT_TEST_NAME();
	firstPing = false;
	uint32_t firstPinfRandomDelayMs = (uint32_t)213000;
	common_trueRandomGenerator_ExpectAndReturn(DALI_FIRST_PING_MAX_DELAY_MS,firstPinfRandomDelayMs);
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs - 1);
	TEST_ASSERT_FALSE(ZaghaController_timeToDaliPing());
}
void test_timeToDaliPing_FirstPing()
{
	_DEBUG_OUT_TEST_NAME();
	firstPing = false;
	uint32_t firstPinfRandomDelayMs = (uint32_t)213000;
	common_trueRandomGenerator_ExpectAndReturn(DALI_FIRST_PING_MAX_DELAY_MS,firstPinfRandomDelayMs);
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs - 1);
	ZaghaController_timeToDaliPing();
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs + 1);
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs + 1);
	TEST_ASSERT_TRUE(ZaghaController_timeToDaliPing());
}
void test_timeToDaliPing_BeforeSecondPing()
{
	_DEBUG_OUT_TEST_NAME();
	firstPing = false;
	uint32_t firstPinfRandomDelayMs = (uint32_t)213000;
	common_trueRandomGenerator_ExpectAndReturn(DALI_FIRST_PING_MAX_DELAY_MS,firstPinfRandomDelayMs);
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs - 1);
	ZaghaController_timeToDaliPing();
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs + 1);
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs + 1);
	ZaghaController_timeToDaliPing();
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs + DALI_PING_INTERVAL_MS - 1);
	TEST_ASSERT_FALSE(ZaghaController_timeToDaliPing());
}
void test_timeToDaliPing_SecondSecondPing()
{
	_DEBUG_OUT_TEST_NAME();
	firstPing = false;
	uint32_t firstPinfRandomDelayMs = (uint32_t)213000;
	common_trueRandomGenerator_ExpectAndReturn(DALI_FIRST_PING_MAX_DELAY_MS,firstPinfRandomDelayMs);
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs - 1);
	ZaghaController_timeToDaliPing();
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs + 1);
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs + 1);
	ZaghaController_timeToDaliPing();
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs + DALI_PING_INTERVAL_MS - 1);
	ZaghaController_timeToDaliPing();
	get_systick_ms_ExpectAndReturn(firstPinfRandomDelayMs + DALI_PING_INTERVAL_MS + 1);
	TEST_ASSERT_FALSE(ZaghaController_timeToDaliPing());
}