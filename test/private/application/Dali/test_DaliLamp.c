#include "unity.h"
#include "stdbool.h"
#include "DaliLamp.c"
#include "mock_DaliLamp_impl.h"
#include "mock_DaliLamp_service.h"
#include "mock_DaliAL.h"
#include "mock_systick.h"
#include "mock_sysmon.h"
#include "mock_debug.h"

TEST_FILE("DaliLamp.c");

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

#define CONNECTED_DRIVERS 16

static dali_bus_status_t status_ok = DALI_BUS_STATUS_OK;
static dali_bus_status_t status_invalid = DALI_BUS_STATUS_INVALID;

struct dali_app_state test_dali_state = {
	.driver_count = CONNECTED_DRIVERS,
	.driver_address = {	0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 }
};

void setUp(void) {
	debugprint_Ignore();
}

void tearDown(void) {

}

/*******************************************************************************
	 bool ZaghaLamp_init();
*******************************************************************************/

void test_Init_SetsTheFadeTimeTo1Second()
{
_DEBUG_OUT_TEST_NAME

	//N.B. FadeTime of 2 = 1 second
	DaliAL_setFadeTime_Expect(2, NULL);
	DaliAL_setFadeTime_IgnoreArg_bus();
	DaliAL_setFadeTime_ReturnThruPtr_bus(&status_ok);

	DaliAL_queryDimmingCurve_ExpectAnyArgsAndReturn(DALI_DIMMING_CURVE_LOGARITHMIC);
	DaliAL_queryDimmingCurve_ReturnThruPtr_bus(&status_ok);

	TEST_ASSERT_TRUE(ZaghaLamp_init());
}

void test_Init_returnsFalseIfSetFadeTimeFails()
{
_DEBUG_OUT_TEST_NAME

	DaliAL_setFadeTime_ExpectAnyArgs();
	DaliAL_setFadeTime_ReturnThruPtr_bus(&status_invalid);

	TEST_ASSERT_FALSE(ZaghaLamp_init());
}

void test_Init_QueriesTheDimmingCurve()
{
_DEBUG_OUT_TEST_NAME

	DaliAL_setFadeTime_ExpectAnyArgs();
	DaliAL_setFadeTime_ReturnThruPtr_bus(&status_ok);

	DaliAL_queryDimmingCurve_ExpectAndReturn(DALI_ADDRESS_IGNORE, DALI_DEVICE_TYPE_LED, NULL, DALI_DIMMING_CURVE_LOGARITHMIC);
	DaliAL_queryDimmingCurve_IgnoreArg_bus();
	DaliAL_queryDimmingCurve_ReturnThruPtr_bus(&status_ok);

	TEST_ASSERT_TRUE(ZaghaLamp_init());
}

void test_Init_QueriesTheDimmingCurve_DifferentDeviceType()
{
_DEBUG_OUT_TEST_NAME

	DaliAL_setFadeTime_ExpectAnyArgs();
	DaliAL_setFadeTime_ReturnThruPtr_bus(&status_ok);

	DaliAL_queryDimmingCurve_ExpectAndReturn(DALI_ADDRESS_IGNORE, DALI_DEVICE_TYPE_LED, NULL, DALI_DIMMING_CURVE_LOGARITHMIC);
	DaliAL_queryDimmingCurve_IgnoreArg_bus();
	DaliAL_queryDimmingCurve_ReturnThruPtr_bus(&status_ok);

	TEST_ASSERT_TRUE(ZaghaLamp_init());
}


void test_Init_returnsFalseIfQueryDimmingCurveFails()
{
_DEBUG_OUT_TEST_NAME

	DaliAL_setFadeTime_ExpectAnyArgs();
	DaliAL_setFadeTime_ReturnThruPtr_bus(&status_ok);

	DaliAL_queryDimmingCurve_ExpectAnyArgsAndReturn(0);
	DaliAL_queryDimmingCurve_ReturnThruPtr_bus(&status_invalid);

	TEST_ASSERT_FALSE(ZaghaLamp_init());
}

void test_Init_SelectsTheDimmingCurveIfLinear()
{
_DEBUG_OUT_TEST_NAME

	DaliAL_setFadeTime_ExpectAnyArgs();
	DaliAL_setFadeTime_ReturnThruPtr_bus(&status_ok);

	DaliAL_queryDimmingCurve_ExpectAnyArgsAndReturn(DALI_DIMMING_CURVE_LINEAR);
	DaliAL_queryDimmingCurve_ReturnThruPtr_bus(&status_ok);

	DaliAL_selectDimmingCurve_Expect(DALI_DEVICE_TYPE_LED, DALI_DIMMING_CURVE_LOGARITHMIC, NULL);
	DaliAL_selectDimmingCurve_IgnoreArg_bus();
	DaliAL_selectDimmingCurve_ReturnThruPtr_bus(&status_ok);

	TEST_ASSERT_TRUE(ZaghaLamp_init());
}

void test_Init_ReturnsFalseIfSelectDimmingCurveFails()
{
_DEBUG_OUT_TEST_NAME

	DaliAL_setFadeTime_ExpectAnyArgs();
	DaliAL_setFadeTime_ReturnThruPtr_bus(&status_ok);

	DaliAL_queryDimmingCurve_ExpectAnyArgsAndReturn(DALI_DIMMING_CURVE_LINEAR);
	DaliAL_queryDimmingCurve_ReturnThruPtr_bus(&status_ok);

	DaliAL_selectDimmingCurve_ExpectAnyArgs();
	DaliAL_selectDimmingCurve_ReturnThruPtr_bus(&status_invalid);

	TEST_ASSERT_FALSE(ZaghaLamp_init());
}


/******************************************************************************
	 ZAGHA_LAMP_RET ZaghaLamp_service(void);
******************************************************************************/

static void initServiceTest() {
	current_brightness_level = 0;
	next_brightness_level = 0;
	arc_read_back_fault = false;
	arc_read_back_fault_debounce = 0;
	read_back_arc_level_is_pending = false;
}

void test_service_NewLevelRequestTriggersDAPC() {
	_DEBUG_OUT_TEST_NAME
	initServiceTest();
	ZaghaLamp_updateTargetLevel(50);
	ZaghaLamp_directArcPowerControl_ExpectAnyArgsAndReturn(ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state);
}

void test_service_NewLevelRequestApplyCorrectLevelInDAPC() {
	_DEBUG_OUT_TEST_NAME
	initServiceTest();
	ZaghaLamp_updateTargetLevel(50);
	ZaghaLamp_directArcPowerControl_ExpectAndReturn(50,0, ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state);
}

void test_service_DAPCErrLeadsToDriverLevelCheck() {
	_DEBUG_OUT_TEST_NAME
	initServiceTest();
	ZaghaLamp_directArcPowerControl_ExpectAnyArgsAndReturn(ZAGHA_LAMP_RET_ERR);
	for (uint8_t i= 0; i< CONNECTED_DRIVERS; i++) {
		ZaghaLamp_checkdriverArcLevel_ExpectAndReturn(test_dali_state.driver_address[i],
				                                      0, ZAGHA_LAMP_RET_OK);
	}
	ZaghaLamp_service(&test_dali_state);
}

void test_service_callFollowingDAPCChecksLevelOnly() {
	_DEBUG_OUT_TEST_NAME
	initServiceTest();
	ZaghaLamp_updateTargetLevel(50);
	ZaghaLamp_directArcPowerControl_ExpectAnyArgsAndReturn(ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state);
	for (uint8_t i= 0; i< CONNECTED_DRIVERS; i++) {
		ZaghaLamp_checkdriverArcLevel_ExpectAndReturn(test_dali_state.driver_address[i],
			                                      	  50, ZAGHA_LAMP_RET_OK);
	}
	ZaghaLamp_service(&test_dali_state);
}

void test_service_resendDAPCInCaseOFMismatch() {
	_DEBUG_OUT_TEST_NAME
	initServiceTest();
	ZaghaLamp_updateTargetLevel(50);
	ZaghaLamp_directArcPowerControl_ExpectAndReturn(50, 0, ZAGHA_LAMP_RET_OK);
	ZaghaLamp_directArcPowerControl_IgnoreArg_prev_brightness();
	ZaghaLamp_service(&test_dali_state); // sends DAPC

	for (uint8_t i= 0; i< (CONNECTED_DRIVERS-1); i++) {
		ZaghaLamp_checkdriverArcLevel_ExpectAndReturn(test_dali_state.driver_address[i],
					                                      50, ZAGHA_LAMP_RET_OK);
	}
	ZaghaLamp_checkdriverArcLevel_ExpectAndReturn(
			                        test_dali_state.driver_address[CONNECTED_DRIVERS-1],
									50, ZAGHA_LAMP_RET_ERR);

	ZaghaLamp_service(&test_dali_state); // mismatch
	ZaghaLamp_directArcPowerControl_ExpectAndReturn(50, 0, ZAGHA_LAMP_RET_OK);
	ZaghaLamp_directArcPowerControl_IgnoreArg_prev_brightness();
	ZaghaLamp_service(&test_dali_state); // resend DAPC
	ZaghaLamp_checkdriverArcLevel_ExpectAnyArgsAndReturn(ZAGHA_LAMP_RET_ERR);
	ZaghaLamp_service(&test_dali_state);
}

void test_service_consecutiveMimatchLeadsToReadBackFault() {
	_DEBUG_OUT_TEST_NAME
	initServiceTest();
	ZaghaLamp_updateTargetLevel(50);
	ZaghaLamp_directArcPowerControl_IgnoreAndReturn(ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state); // applies new level
	ZaghaLamp_checkdriverArcLevel_ExpectAnyArgsAndReturn(ZAGHA_LAMP_RET_ERR);
	ZaghaLamp_service(&test_dali_state); // mismatch
	ZaghaLamp_service(&test_dali_state); // retry new level
	ZaghaLamp_checkdriverArcLevel_ExpectAnyArgsAndReturn(ZAGHA_LAMP_RET_ERR);
	ZaghaLamp_service(&test_dali_state); // second mismatch leads to fault
	TEST_ASSERT_TRUE(arc_read_back_fault);
}

void test_service_mismatchDebounceCannotOverflow() {
_DEBUG_OUT_TEST_NAME
	initServiceTest();
	ZaghaLamp_updateTargetLevel(50);
	ZaghaLamp_directArcPowerControl_IgnoreAndReturn(ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state); // applies new level
	ZaghaLamp_checkdriverArcLevel_ExpectAnyArgsAndReturn(ZAGHA_LAMP_RET_ERR);
	ZaghaLamp_service(&test_dali_state); // mismatch
	ZaghaLamp_service(&test_dali_state); // retry new level
	ZaghaLamp_checkdriverArcLevel_ExpectAnyArgsAndReturn(ZAGHA_LAMP_RET_ERR);
	ZaghaLamp_service(&test_dali_state); // second mismatch leads to fault
	TEST_ASSERT_EQUAL_INT(1, arc_read_back_fault_debounce);
}

void test_service_mismatchFollowedByMatchDoesNotLeadToFault() {
    _DEBUG_OUT_TEST_NAME
	initServiceTest();
	ZaghaLamp_updateTargetLevel(50);
	ZaghaLamp_directArcPowerControl_IgnoreAndReturn(ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state); // applies new level
	ZaghaLamp_checkdriverArcLevel_ExpectAndReturn(0,50,ZAGHA_LAMP_RET_ERR);
	ZaghaLamp_service(&test_dali_state); // mismatch
	ZaghaLamp_service(&test_dali_state); // retry new level
	ZaghaLamp_checkdriverArcLevel_IgnoreAndReturn(ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state); // second mismatch does not lead to fault
	TEST_ASSERT_FALSE(arc_read_back_fault);
}

void test_service_mismatchFollowedByMatchResetsCounter() {
_DEBUG_OUT_TEST_NAME
	initServiceTest();
	ZaghaLamp_updateTargetLevel(50);
	ZaghaLamp_directArcPowerControl_IgnoreAndReturn(ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state); // applies new level
	ZaghaLamp_checkdriverArcLevel_IgnoreAndReturn(ZAGHA_LAMP_RET_ERR);
	ZaghaLamp_service(&test_dali_state); // mismatch
	ZaghaLamp_service(&test_dali_state); // retry new level
	ZaghaLamp_checkdriverArcLevel_IgnoreAndReturn(ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state); // second mismatch does not lead to fault
	ZaghaLamp_updateTargetLevel(40);
	ZaghaLamp_service(&test_dali_state); // applies new level
	ZaghaLamp_checkdriverArcLevel_ExpectAnyArgsAndReturn(ZAGHA_LAMP_RET_ERR);
	ZaghaLamp_service(&test_dali_state); // mismatch
	TEST_ASSERT_FALSE(arc_read_back_fault);
}

void test_service_levelUpdateFollowingMismatchTriggersExpectedDAPC() {
_DEBUG_OUT_TEST_NAME
	initServiceTest();
	// Successfully applies 50%
	ZaghaLamp_updateTargetLevel(50);
	ZaghaLamp_directArcPowerControl_ExpectAndReturn(50, 0, ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state);
	ZaghaLamp_checkdriverArcLevel_IgnoreAndReturn(ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state);

	// Fails to apply 10%
	ZaghaLamp_updateTargetLevel(10);
	ZaghaLamp_directArcPowerControl_ExpectAndReturn(10, 50, ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state); // applies new level
	ZaghaLamp_checkdriverArcLevel_IgnoreAndReturn(ZAGHA_LAMP_RET_ERR);
	ZaghaLamp_service(&test_dali_state); // mismatch
	ZaghaLamp_directArcPowerControl_ExpectAndReturn(10, 0xFF, ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state); // retry new level
	ZaghaLamp_checkdriverArcLevel_IgnoreAndReturn(ZAGHA_LAMP_RET_ERR);
	ZaghaLamp_service(&test_dali_state); // second mismatch leads to fault

	// Successfully applies 50%
	ZaghaLamp_updateTargetLevel(50);
	// currentBrightnessLevel != nextBrightnessLevel --> DAPC
	ZaghaLamp_directArcPowerControl_ExpectAndReturn(50, 0xFF, ZAGHA_LAMP_RET_OK);
	ZaghaLamp_service(&test_dali_state);
}

/******************************************************************************
	 ZAGHA_LAMP_RET ZaghaLamp_setFadeTime(uint8_t fadeTime);
******************************************************************************/
void test_setFadeTime_SetsTheFadeTime()
{
_DEBUG_OUT_TEST_NAME

	DaliAL_setFadeTime_Expect(10, NULL);
	DaliAL_setFadeTime_IgnoreArg_bus();
	DaliAL_setFadeTime_ReturnThruPtr_bus(&status_ok);

	TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_OK, ZaghaLamp_setFadeTime(10));
}

void test_setFadeTime_SetsTheFadeTime_DifferentValue()
{
_DEBUG_OUT_TEST_NAME

	DaliAL_setFadeTime_Expect(19, NULL);
	DaliAL_setFadeTime_IgnoreArg_bus();
	DaliAL_setFadeTime_ReturnThruPtr_bus(&status_ok);

	TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_OK, ZaghaLamp_setFadeTime(19));
}

void test_setFadeTime_failsIfDaliSetFadeTimeFails()
{
_DEBUG_OUT_TEST_NAME

	DaliAL_setFadeTime_Expect(19, NULL);
	DaliAL_setFadeTime_IgnoreArg_bus();
	DaliAL_setFadeTime_ReturnThruPtr_bus(&status_invalid);

	TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_ERR, ZaghaLamp_setFadeTime(19));
}
