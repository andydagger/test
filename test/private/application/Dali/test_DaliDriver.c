#include "unity.h"
#include "DaliDriver.h"
#include "mock_DaliDriver_impl.h"
#include "mock_debug.h"

#define _DEBUG_OUT_TEST_NAME()    printf(" %s", __func__)
TEST_FILE("ZaghaDriver.c");


void setUp(void)
{
	debugprint_Ignore();
}

void tearDown(void)
{
	;
}

/*******************************************************************************
	bool ZaghaDriver_init()
*******************************************************************************/

void test_init_AllOk()
{
	_DEBUG_OUT_TEST_NAME();

	ZaghaDriver_readGTIN_ExpectAnyArgsAndReturn(ZAGHA_DRIVER_RET_OK);
	ZaghaDriver_readSerial_ExpectAnyArgsAndReturn(ZAGHA_DRIVER_RET_OK);
	ZaghaDriver_readPhyMin_ExpectAnyArgsAndReturn(ZAGHA_DRIVER_RET_OK);

	struct dali_app_state state = {
		.driver_count = 1,
		.driver_address = {
			0
		}
	};

	TEST_ASSERT_TRUE(ZaghaDriver_init(&state));
}

void test_init_FailsIfreadGTINFails()
{
	_DEBUG_OUT_TEST_NAME();

	ZaghaDriver_readGTIN_IgnoreAndReturn(ZAGHA_DRIVER_RET_ERR);

	struct dali_app_state state = {
		.driver_count = 1,
		.driver_address = {
			0
		}
	};

	TEST_ASSERT_FALSE(ZaghaDriver_init(&state));
}

void test_init_FailsIfreadSerialFails()
{
	_DEBUG_OUT_TEST_NAME();

	ZaghaDriver_readGTIN_IgnoreAndReturn(ZAGHA_DRIVER_RET_OK);
	ZaghaDriver_readSerial_IgnoreAndReturn(ZAGHA_DRIVER_RET_ERR);

	struct dali_app_state state = {
		.driver_count = 1,
		.driver_address = {
			0
		}
	};

	TEST_ASSERT_FALSE(ZaghaDriver_init(&state));
}


void test_init_FailsIfreadPhyMinFails()
{
	_DEBUG_OUT_TEST_NAME();

	ZaghaDriver_readGTIN_IgnoreAndReturn(ZAGHA_DRIVER_RET_OK);
	ZaghaDriver_readSerial_IgnoreAndReturn(ZAGHA_DRIVER_RET_OK);
	ZaghaDriver_readPhyMin_IgnoreAndReturn(ZAGHA_DRIVER_RET_ERR);

	struct dali_app_state state = {
		.driver_count = 1,
		.driver_address = {
			0
		}
	};

	TEST_ASSERT_FALSE(ZaghaDriver_init(&state));
}

/*******************************************************************************
	void ZaghaDriver_getDriverInfo(ZaghaDriver_Information* p_info)
*******************************************************************************/
void test_getDriverInfo_returnsTheDataReadDuringInit()
{
	_DEBUG_OUT_TEST_NAME();

	ZaghaDriver_Information info = {
		.gtin = {0xB9, 0xA9, 0xA0, 0x45, 0xFF, 0x09},
		.serial = {0x55, 0xAA, 0x55, 0x94, 0x4F, 0xB6, 0xB7, 0x13},
		.phyMin = 0x18
	};
	ZaghaDriver_readGTIN_ExpectAnyArgsAndReturn(ZAGHA_DRIVER_RET_OK);
	ZaghaDriver_readGTIN_ReturnThruPtr_p_info(&info);
	ZaghaDriver_readSerial_ExpectAnyArgsAndReturn(ZAGHA_DRIVER_RET_OK);
	ZaghaDriver_readSerial_ReturnThruPtr_p_info(&info);
	ZaghaDriver_readPhyMin_ExpectAnyArgsAndReturn(ZAGHA_DRIVER_RET_OK);
	ZaghaDriver_readPhyMin_ReturnThruPtr_minimum(&info.phyMin);

	struct dali_app_state state = {
		.driver_count = 1,
		.driver_address = {
			0
		}
	};

	TEST_ASSERT_TRUE(ZaghaDriver_init(&state));

	ZaghaDriver_Information driverInfo;
	ZaghaDriver_getDriverInfo(&driverInfo);
	TEST_ASSERT_EQUAL_MEMORY(&info, &driverInfo, sizeof(driverInfo));
}


void test_getDriverInfo_returnsTheDataReadDuringInit_DifferentValues()
{
	_DEBUG_OUT_TEST_NAME();

	ZaghaDriver_Information info = {
		.gtin = {0xEE, 0xE3, 0x49, 0x90, 0xA1, 0xA2},
		.serial = {0xF5, 0x4A, 0x26, 0x76, 0xEF, 0x1F, 0x1E, 0x09},
		.phyMin = 0x15
	};
	ZaghaDriver_readGTIN_ExpectAnyArgsAndReturn(ZAGHA_DRIVER_RET_OK);
	ZaghaDriver_readGTIN_ReturnThruPtr_p_info(&info);
	ZaghaDriver_readSerial_ExpectAnyArgsAndReturn(ZAGHA_DRIVER_RET_OK);
	ZaghaDriver_readSerial_ReturnThruPtr_p_info(&info);
	ZaghaDriver_readPhyMin_ExpectAnyArgsAndReturn(ZAGHA_DRIVER_RET_OK);
	ZaghaDriver_readPhyMin_ReturnThruPtr_minimum(&info.phyMin);

	struct dali_app_state state = {
		.driver_count = 1,
		.driver_address = {
			0
		}
	};

	TEST_ASSERT_TRUE(ZaghaDriver_init(&state));

	ZaghaDriver_Information driverInfo;
	ZaghaDriver_getDriverInfo(&driverInfo);
	TEST_ASSERT_EQUAL_MEMORY(&info, &driverInfo, sizeof(driverInfo));
}

void test_getDriverInfo_doesNotFailIfPtrIsNull()
{
	_DEBUG_OUT_TEST_NAME();

	ZaghaDriver_getDriverInfo(NULL);
}
