#include "unity.h"
#include "stdbool.h"
#include "DaliLamp_service.h"
#include "mock_DaliLamp_impl.h"
#include "mock_DaliAL.h"
#include "mock_common.h"

#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

TEST_FILE("DaliLamp_service.c");

static dali_bus_status_t status_ok = DALI_BUS_STATUS_OK;
static dali_bus_status_t status_invalid = DALI_BUS_STATUS_INVALID;

/*******************************************************************************
    Callbacks for the mocks and Expected values.
*******************************************************************************/

void setUp(void)
{

}

void tearDown(void)
{

}


/******************************************************************************
    ZAGHA_LAMP_RET ZaghaLamp_directArcPowerControl(uint8_t brightness);
******************************************************************************/
void test_DAPC_sendsTheDAPCCommandToTheApplicationLayer()
{
_DEBUG_OUT_TEST_NAME

   //expect brightness level 10, return arc level 129
   uint8_t arcLevel = 129;
   ZaghaLamp_getArcLevelFromBrightness_ExpectAndReturn(10, NULL, ZAGHA_LAMP_RET_OK);
   ZaghaLamp_getArcLevelFromBrightness_ReturnThruPtr_p_retVal(&arcLevel);
   ZaghaLamp_getArcLevelFromBrightness_IgnoreArg_p_retVal();

   //expect that the 129 is forwarded to the application layer
   DaliAL_directArcPowerControl_Expect(129, NULL);
   DaliAL_directArcPowerControl_IgnoreArg_bus();
   DaliAL_directArcPowerControl_ReturnThruPtr_bus(&status_ok);

//send the DAPC command with brightness 10
   TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_OK, ZaghaLamp_directArcPowerControl(10, 20));
}

void test_DAPC_sendsTheDAPCCommandToTheApplicationLayer_2()
{
_DEBUG_OUT_TEST_NAME

   //expect brightness level 94, return arc level 251
   uint8_t arcLevel = 251;
   ZaghaLamp_getArcLevelFromBrightness_ExpectAndReturn(94, NULL, ZAGHA_LAMP_RET_OK);
   ZaghaLamp_getArcLevelFromBrightness_ReturnThruPtr_p_retVal(&arcLevel);
   ZaghaLamp_getArcLevelFromBrightness_IgnoreArg_p_retVal();

   //expect that the 251 is forwarded to the application layer
   DaliAL_directArcPowerControl_Expect(251, NULL);
   DaliAL_directArcPowerControl_IgnoreArg_bus();
   DaliAL_directArcPowerControl_ReturnThruPtr_bus(&status_ok);

//send the DAPC command with brightness 94
   TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_OK, ZaghaLamp_directArcPowerControl(94, 10));
}

void test_DAPC_returnsERRIfGetArcLevelReturnsAnERR()
{
_DEBUG_OUT_TEST_NAME

   ZaghaLamp_getArcLevelFromBrightness_IgnoreAndReturn(ZAGHA_LAMP_RET_ERR);

//send the DAPC command with brightness 94
   TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_ERR, ZaghaLamp_directArcPowerControl(94, 10));
}

void test_DAPC_returnsERRIfDaliDAPCReturnsAnERR()
{
_DEBUG_OUT_TEST_NAME

   ZaghaLamp_getArcLevelFromBrightness_IgnoreAndReturn(ZAGHA_LAMP_RET_OK);
   DaliAL_directArcPowerControl_ExpectAnyArgs();
   DaliAL_directArcPowerControl_ReturnThruPtr_bus(&status_invalid);

//send the DAPC command with brightness 94
   TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_ERR, ZaghaLamp_directArcPowerControl(94, 10));
}

void test_DAPC_instantReturnIfNoCHange()
{
_DEBUG_OUT_TEST_NAME

   TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_BRIGHTNESS_EQUAL, ZaghaLamp_directArcPowerControl(10, 10));
}

/******************************************************************************
    ZAGHA_LAMP_RET ZaghaLamp_directArcPowerControl(uint8_t brightness);
******************************************************************************/
void test_checkdriverArcLevel_returnsERRIfMismatch()
{
_DEBUG_OUT_TEST_NAME
   ZaghaLamp_getArcLevelFromBrightness_IgnoreAndReturn(ZAGHA_LAMP_RET_OK);
   ZaghaLamp_checkBrightnessLevel_ExpectAnyArgsAndReturn(ZAGHA_LAMP_RET_ERR);
   TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_ERR,ZaghaLamp_checkdriverArcLevel(50,0xff));
}

void test_checkdriverArcLevel_returnsOKWhenReadBackMatch()
{
_DEBUG_OUT_TEST_NAME
   ZaghaLamp_getArcLevelFromBrightness_IgnoreAndReturn(ZAGHA_LAMP_RET_OK);
   ZaghaLamp_checkBrightnessLevel_ExpectAnyArgsAndReturn(ZAGHA_LAMP_RET_OK);
   TEST_ASSERT_EQUAL_INT(ZAGHA_LAMP_RET_OK,ZaghaLamp_checkdriverArcLevel(50,0xff));
}

void test_checkdriverArcLevel_checksWithProcessedArcLevel()
{
_DEBUG_OUT_TEST_NAME
   uint8_t arcLevel = 100;
   ZaghaLamp_getArcLevelFromBrightness_ExpectAndReturn(50, NULL, ZAGHA_LAMP_RET_OK);
   ZaghaLamp_getArcLevelFromBrightness_IgnoreArg_p_retVal();
   ZaghaLamp_getArcLevelFromBrightness_ReturnThruPtr_p_retVal(&arcLevel);
   ZaghaLamp_checkBrightnessLevel_ExpectAndReturn(0xff, 100, ZAGHA_LAMP_RET_OK);
   ZaghaLamp_checkdriverArcLevel(0xff, 50);
}
