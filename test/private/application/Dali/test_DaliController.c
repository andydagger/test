#include "unity.h"

#include "DaliController.c"

#include "mock_DaliController_impl.h"
#include "mock_DaliAL.h"
#include "mock_DaliLamp.h"
#include "mock_DaliDriver.h"
#include "mock_debug.h"
#include "mock_common.h"
#include "mock_sysmon.h"
#include "mock_FreeRTOSCommonHooks.h"
#include "mock_systick.h"
#include "mock_relay.h"

TEST_FILE("ZaghaController.c");

#define _DEBUG_OUT_TEST_NAME()    printf(" %s",__func__)

#define INVALID_POWER_VALUE_U8      0xFF

static dali_bus_status_t status_ok 		= DALI_BUS_STATUS_OK;
static dali_bus_status_t status_invalid = DALI_BUS_STATUS_INVALID;

void setUp(void)
{
	debugprint_Ignore();
	ZaghaController_reset();
}

void tearDown(void)
{
	;
}

void perform_successful_init()
{
	DaliAL_reset_ExpectAnyArgs();
	DaliAL_reset_ReturnThruPtr_bus(&status_ok);

	ZaghaDriver_init_ExpectAnyArgsAndReturn(true);
	ZaghaLamp_init_ExpectAndReturn(true);

	ZaghaController_init();
}

/*******************************************************************************
	bool ZaghaController_init(void)
*******************************************************************************/

void test_init_AllOk()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_reset_ExpectAnyArgs();
	DaliAL_reset_ReturnThruPtr_bus(&status_ok);

	ZaghaDriver_init_ExpectAnyArgsAndReturn(true);
	ZaghaLamp_init_ExpectAndReturn(true);

	TEST_ASSERT_TRUE(ZaghaController_init());
}


void test_init_ReturnFalseIfDaliAlResetFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_reset_ExpectAnyArgs();
	DaliAL_reset_ReturnThruPtr_bus(&status_invalid);

	TEST_ASSERT_FALSE(ZaghaController_init());
}

void test_init_ReturnTrueIfDriverInitFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_reset_ExpectAnyArgs();
	DaliAL_reset_ReturnThruPtr_bus(&status_ok);

	ZaghaDriver_init_IgnoreAndReturn(false);
	ZaghaLamp_init_ExpectAndReturn(true);

	TEST_ASSERT_TRUE(ZaghaController_init());
}

void test_init_ReturnTrueIfLampInitFails()
{
	_DEBUG_OUT_TEST_NAME();

	DaliAL_reset_ExpectAnyArgs();
	DaliAL_reset_ReturnThruPtr_bus(&status_ok);

	ZaghaDriver_init_IgnoreAndReturn(true);

	ZaghaLamp_init_IgnoreAndReturn(false);
	TEST_ASSERT_TRUE(ZaghaController_init());
}

/*******************************************************************************
	void ZaghaController_service(void)
*******************************************************************************/

void test_service_AllOk()
{
	_DEBUG_OUT_TEST_NAME();

	perform_successful_init();
	get_sysmon_last_gasp_is_active_ExpectAndReturn(false);
	b_get_relay_state_ExpectAndReturn(RELAY_CLOSE);

	get_systick_ms_ExpectAndReturn(6001);
	get_systick_ms_ExpectAndReturn(6001);

	ZaghaController_timeToDaliPing_ExpectAndReturn(false);

	ZaghaController_checkState_ExpectAnyArgsAndReturn(ZAGHA_CTRL_RET_OK);

	ZaghaLamp_service_Ignore();

	ZaghaController_service();
}

void test_service_SkipIfServiceFlagNotSet()
{
	_DEBUG_OUT_TEST_NAME();

	perform_successful_init();
	g_flags.serviceStackFlag = false;
	get_sysmon_last_gasp_is_active_IgnoreAndReturn(false);
	b_get_relay_state_IgnoreAndReturn(RELAY_CLOSE);

	get_systick_ms_IgnoreAndReturn(6001);

	ZaghaController_service();
}

void test_service_SkipIfRelayOpen()
{
	_DEBUG_OUT_TEST_NAME();

	perform_successful_init();
	get_sysmon_last_gasp_is_active_IgnoreAndReturn(false);
	b_get_relay_state_IgnoreAndReturn(RELAY_OPEN);

	get_systick_ms_IgnoreAndReturn(6001);

	ZaghaController_service();
}

void test_service_SkipIfPowerFailureActive()
{
	_DEBUG_OUT_TEST_NAME();

	perform_successful_init();
	get_sysmon_last_gasp_is_active_IgnoreAndReturn(true);

	get_systick_ms_IgnoreAndReturn(6001);

	ZaghaController_service();
}

void test_service_DoesNotCallServicesIfTimeoutNotReached()
{
	_DEBUG_OUT_TEST_NAME();

	perform_successful_init();
	get_sysmon_last_gasp_is_active_ExpectAndReturn(false);
	b_get_relay_state_ExpectAndReturn(RELAY_CLOSE);

	get_systick_ms_ExpectAndReturn(5000);

	ZaghaController_service();
}

void test_service_LampOutputUpdateForcesInstantServiceStatck()
{
	_DEBUG_OUT_TEST_NAME();

	perform_successful_init();
	get_sysmon_last_gasp_is_active_ExpectAndReturn(false);
	b_get_relay_state_ExpectAndReturn(RELAY_CLOSE);

	get_systick_ms_IgnoreAndReturn(2000);

	ZaghaController_checkState_IgnoreAndReturn(ZAGHA_CTRL_RET_OK);

	ZaghaController_service();

	ZaghaLamp_updateTargetLevel_Ignore();
	get_systick_ms_IgnoreAndReturn(2000);
	ZaghaController_updateLampOutput(50);

	get_sysmon_last_gasp_is_active_ExpectAndReturn(false);
	b_get_relay_state_ExpectAndReturn(RELAY_CLOSE);

	get_systick_ms_IgnoreAndReturn(3000);

	ZaghaController_timeToDaliPing_ExpectAndReturn(false);

	ZaghaLamp_service_Ignore();

	ZaghaController_service();
}


void test_service_UpdatesTheFadeTimeIfTheFlagIsSet()
{
	_DEBUG_OUT_TEST_NAME();

	perform_successful_init();
	get_sysmon_last_gasp_is_active_ExpectAndReturn(false);
	b_get_relay_state_ExpectAndReturn(RELAY_CLOSE);

	get_systick_ms_ExpectAndReturn(6001);
	get_systick_ms_ExpectAndReturn(6001);

	ZaghaController_timeToDaliPing_ExpectAndReturn(false);
	ZaghaLamp_setFadeTime_ExpectAndReturn(10, ZAGHA_LAMP_RET_OK);

	ZaghaController_checkState_ExpectAnyArgsAndReturn(ZAGHA_CTRL_RET_OK);

	ZaghaLamp_service_Ignore();

	ZaghaController_updateFadeTime(10);
	ZaghaController_service();
}

void test_service_ServicePingIfTheFlagIsSet()
{
	_DEBUG_OUT_TEST_NAME();

	perform_successful_init();
	get_sysmon_last_gasp_is_active_IgnoreAndReturn(false);
	b_get_relay_state_IgnoreAndReturn(RELAY_CLOSE);

	get_systick_ms_IgnoreAndReturn(6001);

	ZaghaController_timeToDaliPing_ExpectAndReturn(true);

	DaliAL_ping_Expect(NULL);

	ZaghaController_checkState_IgnoreAndReturn(ZAGHA_CTRL_RET_OK);

	ZaghaLamp_service_Ignore();

	ZaghaController_service();
}

void test_service_InitiStackRequestedAllOk()
{
	_DEBUG_OUT_TEST_NAME();

	perform_successful_init();

	// Request stack init
	ZaghaController_requestZaghaStackInit();
	TEST_ASSERT_TRUE(ZaghaController_getZaghaStackInitRequested());

	DaliAL_reset_ExpectAnyArgs();
	DaliAL_reset_ReturnThruPtr_bus(&status_ok);

	ZaghaDriver_init_ExpectAnyArgsAndReturn(true);
	ZaghaLamp_init_ExpectAndReturn(true);

	ZaghaController_service();
	TEST_ASSERT_FALSE(ZaghaController_getZaghaStackInitRequested());
}

void test_service_InitiStackRequestedInitFailed()
{
	_DEBUG_OUT_TEST_NAME();

	perform_successful_init();
	// Request stack init
	ZaghaController_requestZaghaStackInit();
	TEST_ASSERT_TRUE(ZaghaController_getZaghaStackInitRequested());

	DaliAL_reset_ExpectAnyArgs();
	DaliAL_reset_ReturnThruPtr_bus(&status_invalid);

	ZaghaController_service();
	TEST_ASSERT_TRUE(ZaghaController_getZaghaStackInitRequested());
}

/*******************************************************************************
	bool ZaghaController_updateLampOutput(uint8_t level)
*******************************************************************************/

void test_updateLampOutput_AllOk()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t testBrightness = 100;
	ZaghaLamp_updateTargetLevel_Expect(testBrightness);
	debugprint_Ignore();
	get_systick_ms_IgnoreAndReturn(0);
	TEST_ASSERT_TRUE( ZaghaController_updateLampOutput(testBrightness) );
}

void test_updateLampOutput_AcceptsZero()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t testBrightness = 0;
	ZaghaLamp_updateTargetLevel_Expect(testBrightness);
	debugprint_Ignore();
	get_systick_ms_IgnoreAndReturn(0);
	TEST_ASSERT_TRUE( ZaghaController_updateLampOutput(testBrightness) );
}

void test_updateLampOutput_LevelOutOfRangeReturnFalse()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t testBrightness = 101;
	debugprint_Ignore();
	get_systick_ms_IgnoreAndReturn(0);
	TEST_ASSERT_FALSE( ZaghaController_updateLampOutput(testBrightness) );
}

void test_updateLampOutput_adjustTheServiceDelayAccordingly()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t testBrightness = 50;
	g_timeSinceLastServiceMs = 10000;
	debugprint_Ignore();
	get_systick_ms_IgnoreAndReturn(20000);
	ZaghaLamp_updateTargetLevel_Expect(testBrightness);
	ZaghaController_updateLampOutput(testBrightness);
	TEST_ASSERT_EQUAL_INT(14500, g_timeSinceLastServiceMs);
}


/*******************************************************************************
	void ZaghaController_getDataset(LuminrDataset * dataset)
*******************************************************************************/

void test_getDataset_CopiesGTIN()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t testBrightness = 100;
	ZaghaDriver_Information info = {
		.gtin = {0x57, 0x12, 0x52, 0xE0, 0x11, 0x94}
	};
	ZaghaDriver_getDriverInfo_ExpectAnyArgs();
	ZaghaDriver_getDriverInfo_ReturnThruPtr_p_info(&info);
	LuminrDataset dataset;
	ZaghaController_getDataset(&dataset);
	TEST_ASSERT_EQUAL_MEMORY(info.gtin,dataset.gtin, sizeof(dataset.gtin));
}


void test_getDataset_CopiesGTIN_DifferentValues()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t testBrightness = 100;
	ZaghaDriver_Information info = {
		.gtin = {0x9E, 0x43, 0x44, 0x4F, 0x5F, 0xE0}
	};
	ZaghaDriver_getDriverInfo_ExpectAnyArgs();
	ZaghaDriver_getDriverInfo_ReturnThruPtr_p_info(&info);
	LuminrDataset dataset;
	ZaghaController_getDataset(&dataset);
	TEST_ASSERT_EQUAL_MEMORY(info.gtin,dataset.gtin, sizeof(dataset.gtin));
}


void test_getDataset_CopiesSerial()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t testBrightness = 100;
	ZaghaDriver_Information info = {
		.serial = {0x11, 0x29, 0x1E, 0xFE, 0x00, 0x99, 0x12, 0xFF}
	};
	ZaghaDriver_getDriverInfo_ExpectAnyArgs();
	ZaghaDriver_getDriverInfo_ReturnThruPtr_p_info(&info);
	LuminrDataset dataset;
	ZaghaController_getDataset(&dataset);
	TEST_ASSERT_EQUAL_MEMORY(info.serial, dataset.serno, sizeof(dataset.serno));
}


void test_getDataset_CopiesSerial_DifferentValues()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t testBrightness = 100;
	ZaghaDriver_Information info = {
		.serial = {0x0C, 0xC5, 0xD9, 0x93, 0xFF, 0xF0, 0x72, 0x82}
	};
	ZaghaDriver_getDriverInfo_ExpectAnyArgs();
	ZaghaDriver_getDriverInfo_ReturnThruPtr_p_info(&info);
	LuminrDataset dataset;
	ZaghaController_getDataset(&dataset);
	TEST_ASSERT_EQUAL_MEMORY(info.serial, dataset.serno, sizeof(dataset.serno));
}



void test_getDataset_CopiesPhyMin()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t testBrightness = 100;
	ZaghaDriver_Information info = {
		.phyMin = 0x47
	};
	ZaghaDriver_getDriverInfo_ExpectAnyArgs();
	ZaghaDriver_getDriverInfo_ReturnThruPtr_p_info(&info);
	LuminrDataset dataset;
	ZaghaController_getDataset(&dataset);
	TEST_ASSERT_EQUAL_UINT8(info.phyMin, dataset.info.phy_min);
}


void test_getDataset_CopiesPhyMin_DifferentValues()
{
	_DEBUG_OUT_TEST_NAME();
	uint8_t testBrightness = 100;
	ZaghaDriver_Information info = {
		.phyMin = 0x09
	};
	ZaghaDriver_getDriverInfo_ExpectAnyArgs();
	ZaghaDriver_getDriverInfo_ReturnThruPtr_p_info(&info);
	LuminrDataset dataset;
	ZaghaController_getDataset(&dataset);
	TEST_ASSERT_EQUAL_UINT8(info.phyMin, dataset.info.phy_min);
}
