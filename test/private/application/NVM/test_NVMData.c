#include "unity.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "NVMData.c"

#include "mock_debug.h"
#include "mock_config_lorawan.h"
#include "mock_config_eeprom.h"
#include "mock_hw.h"
#include "mock_NonVolatileMemory.h"


#define _DEBUG_OUT_TEST_NAME    printf(" %s",__func__);

void setUp(void)
{
    debugprint_Ignore();
}

void tearDown(void)
{

}

//-----------------------------------------------------------------
// void test_NVM_initialise()
// Test the initialisation with NMV_initialise returning OK
//-----------------------------------------------------------------
void test_NVMData_initialise()
{
    _DEBUG_OUT_TEST_NAME
    NVM_result_t result;

    NVM_initialise_ExpectAndReturn(NVM_RESULT_OK);

    NVM_read_ExpectAnyArgsAndReturn(NVM_RESULT_OK);
    NVM_write_ExpectAnyArgs();
    NVM_read_ExpectAnyArgsAndReturn(NVM_RESULT_OK);
    NVM_write_ExpectAnyArgs();
    NVM_read_ExpectAnyArgsAndReturn(NVM_RESULT_OK);
    NVM_write_ExpectAnyArgs();
    NVM_read_ExpectAnyArgsAndReturn(NVM_RESULT_OK);
    NVM_write_ExpectAnyArgs();
    NVM_read_ExpectAnyArgsAndReturn(NVM_RESULT_OK);
    NVM_write_ExpectAnyArgs();

    result = NVMData_initialise();

    TEST_ASSERT_EQUAL_UINT32(NVM_RESULT_OK, result);
}

//-----------------------------------------------------------------
// void test_NVMData_restoreAll()
// Test the restore of all sections in NVM
//-----------------------------------------------------------------
void test_NVMData_restoreAll()
{
    _DEBUG_OUT_TEST_NAME

    NVM_write_ExpectAnyArgs();
    NVM_write_ExpectAnyArgs();
    NVM_write_ExpectAnyArgs();
    NVM_write_ExpectAnyArgs();
    NVM_write_ExpectAnyArgs();

    NVMData_restoreAll();
}


//-----------------------------------------------------------------
// NVMData_restoreErrorCode
//-----------------------------------------------------------------
void test_NVMData_restoreErrorCode()
{
    _DEBUG_OUT_TEST_NAME

    NVM_write_ExpectAnyArgs();

    NVMData_restoreErrorCode();
}

//-----------------------------------------------------------------
// NVMData_readErrorCode
//-----------------------------------------------------------------
void test_NVMData_readErrorCode()
{
    _DEBUG_OUT_TEST_NAME
    NVM_errorCode_t errorCode = {0};
    NVM_errorCode_t errorCodeRead =
    {
         .version = NVM_ERROR_CODE_STRUCT_VER,
         .id      = NVM_DEFAULT_ERROR_CODE_ID,
         .value   = NVM_DEFAULT_ERROR_CODE_VALUE,
         .crc     = 0
    };

    NVM_result_t result;

    NVM_read_ExpectAnyArgsAndReturn(NVM_RESULT_OK);
    NVM_read_ReturnArrayThruPtr_dataPtr((uint8_t*)&errorCodeRead, sizeof(NVM_errorCode_t));

    result = NVMData_readErrorCode( &errorCode);

    TEST_ASSERT_EQUAL_UINT32(NVM_RESULT_OK, result);
    TEST_ASSERT_EQUAL_UINT8(errorCode.version, errorCodeRead.version);
    TEST_ASSERT_EQUAL_UINT8(errorCode.id, errorCodeRead.id);
    TEST_ASSERT_EQUAL_UINT8(errorCode.value, errorCodeRead.value);
    TEST_ASSERT_EQUAL_UINT16(errorCode.crc, errorCodeRead.crc);
}

//-----------------------------------------------------------------
// NVMData_writeErrorCode
//-----------------------------------------------------------------
void test_NVMData_writeErrorCode()
{
    NVM_errorCode_t errorCodeWrite =
    {
         .version = NVM_ERROR_CODE_STRUCT_VER,
         .id      = NVM_DEFAULT_ERROR_CODE_ID,
         .value   = NVM_DEFAULT_ERROR_CODE_VALUE,
         .crc     = 0
    };

    NVM_write_ExpectAnyArgs();

    NVMData_writeErrorCode(&errorCodeWrite);
}


//-----------------------------------------------------------------
// NVMData_restoreMeteringCalibCoef
//-----------------------------------------------------------------
void test_NVMData_restoreMeteringCalibCoef()
{
    _DEBUG_OUT_TEST_NAME

    NVM_write_ExpectAnyArgs();

    NVMData_restoreMeteringCalibCoef();
}

//-----------------------------------------------------------------
// NVMData_readMeteringCalibCoef
//-----------------------------------------------------------------
void test_NVMData_readMeteringCalibCoef()
{
    _DEBUG_OUT_TEST_NAME
    NVM_meteringCalibCoef_t calibCoef = {0};
    NVM_meteringCalibCoef_t calibCoefRead =
    {
         .version     = NVM_METERING_CALIB_COEF_STRUCT_VER,
         .calibPassed = NVM_DEFAULT_METERING_CALIB_PASSED,
         .voltage     = NVM_DEFAULT_METERING_CALIB_VOLTAGE,
         .current     = NVM_DEFAULT_METERING_CALIB_CURRENT,
         .power       = NVM_DEFAULT_METERING_CALIB_POWER,
         .crc     = 0
    };

    NVM_result_t result;

    NVM_read_ExpectAnyArgsAndReturn(NVM_RESULT_OK);
    NVM_read_ReturnArrayThruPtr_dataPtr((uint8_t*)&calibCoefRead, sizeof(NVM_meteringCalibCoef_t));

    result = NVMData_readMeteringCalibCoef( &calibCoef);

    TEST_ASSERT_EQUAL_UINT32(NVM_RESULT_OK, result);
    TEST_ASSERT_EQUAL_UINT8(calibCoef.version, calibCoefRead.version);
    TEST_ASSERT_EQUAL_UINT8(calibCoef.calibPassed, calibCoefRead.calibPassed);
    TEST_ASSERT_EQUAL_FLOAT(calibCoef.voltage, calibCoefRead.voltage);
    TEST_ASSERT_EQUAL_FLOAT(calibCoef.current, calibCoefRead.current);
    TEST_ASSERT_EQUAL_FLOAT(calibCoef.power, calibCoefRead.power);
    TEST_ASSERT_EQUAL_UINT16(calibCoef.crc, calibCoefRead.crc);
}


//-----------------------------------------------------------------
// NVMData_writeMeteringCalibCoef
//-----------------------------------------------------------------
void test_NVMData_writeMeteringCalibCoef()
{
    NVM_meteringCalibCoef_t calibCoefWrite =
    {
         .version     = NVM_METERING_CALIB_COEF_STRUCT_VER,
         .calibPassed = NVM_DEFAULT_METERING_CALIB_PASSED,
         .voltage     = NVM_DEFAULT_METERING_CALIB_VOLTAGE,
         .current     = NVM_DEFAULT_METERING_CALIB_CURRENT,
         .power       = NVM_DEFAULT_METERING_CALIB_POWER,
         .crc     = 0
    };

    NVM_write_ExpectAnyArgs();

    NVMData_writeMeteringCalibCoef(&calibCoefWrite);
}


//-----------------------------------------------------------------
// NVMData_restoreLampOutputOverride
//-----------------------------------------------------------------
void test_NVMData_restoreLampOutputOverride()
{
    _DEBUG_OUT_TEST_NAME

    NVM_write_ExpectAnyArgs();

    NVMData_restoreLampOutputOverride();
}


//-----------------------------------------------------------------
// NVMData_readLampOutputOverride
//-----------------------------------------------------------------
void test_NVMData_readLampOutputOverride()
{
    _DEBUG_OUT_TEST_NAME
    NVM_lampOutputOverride_t lampOutputOverride = {0};
    NVM_lampOutputOverride_t lampOutputOverrideRead =
    {
         .version      = NVM_LAMP_OUTPUT_OVERRIDE_STRUCT_VER,
         .state        = 1,
         .outputLevel  = 67,
         .startTimeUtc = 0x12345678,
         .stopTimeUtc  = 0x87654321,
         .crc          = 0
    };

    NVM_result_t result;

    NVM_read_ExpectAnyArgsAndReturn(NVM_RESULT_OK);
    NVM_read_ReturnArrayThruPtr_dataPtr((uint8_t*)&lampOutputOverrideRead, sizeof(NVM_lampOutputOverride_t));

    result = NVMData_readLampOutputOverride( &lampOutputOverride);

    TEST_ASSERT_EQUAL_UINT32(NVM_RESULT_OK, result);
    TEST_ASSERT_EQUAL_UINT8(lampOutputOverride.version, lampOutputOverrideRead.version);
    TEST_ASSERT_EQUAL_UINT8(lampOutputOverride.state, lampOutputOverrideRead.state);
    TEST_ASSERT_EQUAL_UINT8(lampOutputOverride.outputLevel, lampOutputOverrideRead.outputLevel);
    TEST_ASSERT_EQUAL_UINT32(lampOutputOverride.startTimeUtc, lampOutputOverrideRead.startTimeUtc);
    TEST_ASSERT_EQUAL_UINT32(lampOutputOverride.stopTimeUtc, lampOutputOverrideRead.stopTimeUtc);
    TEST_ASSERT_EQUAL_UINT16(lampOutputOverride.crc, lampOutputOverrideRead.crc);
}


//-----------------------------------------------------------------
// NVMData_writeLampOutputOverride
//-----------------------------------------------------------------
void test_NVMData_writeLampOutputOverride()
{
    NVM_lampOutputOverride_t lampOutputOverrideWrite =
    {
         .version      = NVM_LAMP_OUTPUT_OVERRIDE_STRUCT_VER,
         .state        = 1,
         .outputLevel  = 67,
         .startTimeUtc = 0x12345678,
         .stopTimeUtc  = 0x87654321,
         .crc          = 0
    };

    NVM_write_ExpectAnyArgs();

    NVMData_writeLampOutputOverride(&lampOutputOverrideWrite);
}


//-----------------------------------------------------------------
// NVMData_restoreDaliAddresses
//-----------------------------------------------------------------
void test_NVMData_restoreDaliAddresses()
{
    _DEBUG_OUT_TEST_NAME

    NVM_write_ExpectAnyArgs();

    NVMData_restoreDaliAddresses();
}

//-----------------------------------------------------------------
// NVMData_readDaliAddresses
//-----------------------------------------------------------------
void test_NVMData_readDaliAddresses()
{
    _DEBUG_OUT_TEST_NAME
    NVM_DaliAddresses_t daliAddresses = {0};
    NVM_DaliAddresses_t daliAddressesRead =
    {
         .version      = NVM_DALI_ADDRESSES_STRUCT_VER,
         .addressMask  = 0x1234567890ABCDEF,
         .crc          = 0
    };

    NVM_result_t result;

    NVM_read_ExpectAnyArgsAndReturn(NVM_RESULT_OK);
    NVM_read_ReturnArrayThruPtr_dataPtr((uint8_t*)&daliAddressesRead, sizeof(NVM_DaliAddresses_t));

    result = NVMData_readDaliAddresses( &daliAddresses);

    TEST_ASSERT_EQUAL_UINT32(NVM_RESULT_OK, result);
    TEST_ASSERT_EQUAL_UINT8(daliAddresses.version, daliAddressesRead.version);
    TEST_ASSERT_EQUAL_UINT64(daliAddresses.addressMask, daliAddressesRead.addressMask);
    TEST_ASSERT_EQUAL_UINT16(daliAddresses.crc, daliAddressesRead.crc);
}


//-----------------------------------------------------------------
// NVMData_writeDaliAddresses
//-----------------------------------------------------------------
void test_NVMData_writeDaliAddresses()
{
    NVM_DaliAddresses_t daliAddressesWrite =
    {
         .version      = NVM_DALI_ADDRESSES_STRUCT_VER,
         .addressMask  = 0x1234567890ABCDEF,
         .crc          = 0
    };

    NVM_write_ExpectAnyArgs();

    NVMData_writeDaliAddresses(&daliAddressesWrite);
}

//-----------------------------------------------------------------
// NVMData_restoreRuntimeParams
//-----------------------------------------------------------------
void test_NVMData_restoreRuntimeParams()
{
    _DEBUG_OUT_TEST_NAME

        NVM_write_ExpectAnyArgs();

    NVMData_restoreRuntimeParams();
}

//-----------------------------------------------------------------
// NVMData_readRuntimeParams
//-----------------------------------------------------------------
void test_NVMData_readRuntimeParams()
{
    _DEBUG_OUT_TEST_NAME
        NVM_RuntimeParams_t runtimeParams = { 0 };
    NVM_RuntimeParams_t runtimeParamsRead =
    {
         .version = NVM_RUNTIME_PARAMS_STRUCT_VER,
         .status_period = 30,
         .crc = 0
    };

    NVM_result_t result;

    NVM_read_ExpectAnyArgsAndReturn(NVM_RESULT_OK);
    NVM_read_ReturnArrayThruPtr_dataPtr((uint8_t*)&runtimeParamsRead, sizeof(NVM_RuntimeParams_t));

    result = NVMData_readRuntimeParams(&runtimeParams);

    TEST_ASSERT_EQUAL_UINT32(NVM_RESULT_OK, result);
    TEST_ASSERT_EQUAL_UINT8(runtimeParams.version, runtimeParamsRead.version);
    TEST_ASSERT_EQUAL_UINT8(runtimeParams.status_period, runtimeParamsRead.status_period);
    TEST_ASSERT_EQUAL_UINT16(runtimeParams.crc, runtimeParamsRead.crc);
}


//-----------------------------------------------------------------
// NVMData_writeRuntimeParams
//-----------------------------------------------------------------
void test_NVMData_writeRuntimeParams()
{
    NVM_RuntimeParams_t runtimeParams =
    {
         .version = NVM_RUNTIME_PARAMS_STRUCT_VER,
         .status_period = 30,
         .crc = 0
    };

    NVM_write_ExpectAnyArgs();

    NVMData_writeRuntimeParams(&runtimeParams);
}
