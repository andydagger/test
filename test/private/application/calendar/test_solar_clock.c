#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "unity.h"
#include "solar_clock.h"
#include "mock_debug.h"
#include "mock_gps.h"
#include "mock_task.h"
#include "mock_rtc_procx.h"


/*
==========================================================================
The solar_clock.c source code has been verified against the original 
alogrithm source at https://github.com/buelowp/sunset to be identical,
unit tests for different time zones have beed verified as correct using 
the website:
https://www.calculatorsoup.com/calculators/time/sunrise_sunset.php
verified by: jeff barker 02.07.21
==========================================================================
*/

#define DEBUG_PRINT_FUNC_NAME()   printf(" %s",__func__)

static SolarClockDataset   	solar_clock_dataset = {0};
static RTCTime t;

#define SOLAR_MARGIN_SEC 70

void setDateTimeStruct( RTCTime * p_TestStruct,
                        uint32_t RTC_Sec,
                        uint32_t RTC_Min,
                        uint32_t RTC_Hour,
                        uint32_t RTC_Mday,
                        uint32_t RTC_Mon,
                        uint32_t RTC_Year,
                        uint32_t RTC_Wday,
                        uint32_t RTC_Yday) {

    p_TestStruct->RTC_Sec = RTC_Sec;
    p_TestStruct->RTC_Min = RTC_Min;
    p_TestStruct->RTC_Hour = RTC_Hour;
    p_TestStruct->RTC_Mday = RTC_Mday;
    p_TestStruct->RTC_Mon = RTC_Mon;
    p_TestStruct->RTC_Year = RTC_Year;
    p_TestStruct->RTC_Yday = RTC_Yday;
    p_TestStruct->RTC_Wday = 0;
}

struct TestDataSet {
	float latitude;
	float longitude;
    uint32_t day;
    uint32_t month;
    uint32_t year;
    uint32_t sunSetUtc;
    uint32_t sunRiseUtc;
};

void setUp(void) {

    debugprint_Ignore();

    solar_clock_dataset.solar_sun_rise = 0;

    solar_clock_dataset.solar_sun_set = 0;

    memset((void*)&t, 0, sizeof(RTCTime));
}

void tearDown(void) {


}


void calculateSolarTriggers(struct TestDataSet * pData) {

    int32_t latitude = (uint32_t)(pData->latitude * 100000);
    int32_t longitude = (uint32_t)(pData->longitude * 100000);
    time_t debug_tm;

    GpsDataset LatLong = {latitude, longitude, 2, true}; // Lat, Long

    setDateTimeStruct(&t, 00, 00, 12, pData->day, pData->month, pData->year, 0, 0);

    rtc_procx_GetTime_ExpectAndReturn(t);

    get_gps_dataset_ExpectAnyArgs();
    //get_gps_dataset_ReturnArrayThruPtr_dataset(&LatLong, 4);
    get_gps_dataset_ReturnThruPtr_dataset(&LatLong);

    solar_main();

    vTaskSuspendAll_Expect();
    xTaskResumeAll_ExpectAndReturn(0);
    
    get_solar_dataset(&solar_clock_dataset);
    
    debug_tm = solar_clock_dataset.solar_sun_set;

    struct tm* ss_tm_ptr = gmtime(&debug_tm);
    
    printf("Sunset  = %u | %02u.%02u.%u, %02u:%02u:%02u | UTC Diff %d\r",
        solar_clock_dataset.solar_sun_set,
        ss_tm_ptr->tm_mday, ss_tm_ptr->tm_mon + 1, ss_tm_ptr->tm_year + 1900,
        ss_tm_ptr->tm_hour, ss_tm_ptr->tm_min, ss_tm_ptr->tm_sec,
        (int)(solar_clock_dataset.solar_sun_set - (int)pData->sunSetUtc));

    debug_tm = solar_clock_dataset.solar_sun_rise;

    struct tm* sr_tm_ptr = gmtime(&debug_tm);

    printf("Sunrise = %u | %02u.%02u.%u, %02u:%02u:%02u | UTC Diff %d\r\n",
        solar_clock_dataset.solar_sun_rise,
        sr_tm_ptr->tm_mday, sr_tm_ptr->tm_mon + 1, sr_tm_ptr->tm_year + 1900,
        sr_tm_ptr->tm_hour, sr_tm_ptr->tm_min, sr_tm_ptr->tm_sec,
        (int)(solar_clock_dataset.solar_sun_rise - (int)pData->sunRiseUtc));
}

/*******************************************************************************
    solar_main();
*******************************************************************************/


void test_solar_main_London_270620_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = {51.49472, -0.13527, 27, 06, 2020, 1593289260, 1593315960 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}

void test_solar_main_Paris_270620_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = {48.87375, 2.29494, 27, 06, 2020, 1593289011, 1593317293 };

    calculateSolarTriggers(&TestData);

    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}

void test_solar_main_Dubai_270620_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = {25.276987, 55.296249, 27, 06, 2020, 1593297325, 1593334415 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}

void test_solar_main_Luxembourg_270620_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { 49.61162, 6.13193, 27, 06, 2020, 1593290148, 1593317998 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}

void test_solar_main_Madrid_270620_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { 40.416775, -3.70379, 27, 06, 2020, 1593285571, 1593317851 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}


void test_solar_main_Sydney_270620_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { -33.853154, 151.214951, 27, 06, 2020, 1593270344, 1593321040 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}



void test_solar_main_London_010121_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { 51.49472, -0.13527, 01, 01, 2021, 1609516891, 1609574694 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}

void test_solar_main_Paris_300621_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { 48.87375,  -2.29494, 30, 06, 2021, 1625083084, 1625111487 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}

void test_solar_main_Dubai_010121_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { 25.276987, 55.296249, 01, 01, 2021, 1609534987, 1609583232 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}

void test_solar_main_Luxembourg_010121_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = {49.61162, 6.13193, 01, 01, 2021, 1609518923, 1609575671 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}

void test_solar_main_Madrid_010121_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { 40.416775, -3.70379, 01, 01, 2021, 1609518574, 1609571305 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}

void test_solar_main_Sydney_010121_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = {-33.865143, 151.2099, 01, 01, 2021, 1609521555, 1609556319 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
}

void test_solar_main_Cape_Town_010121_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { 33.9249, 18.4241, 01, 01, 2021, 1609524953, 1609575550 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
}

void test_solar_main_Tokyo_010121_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { 35.71561, 139.26982, 01, 01, 2021, 1609510493, 1609561632 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}


void test_solar_main_Rio_de_Janeiro_010121_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { -22.90278, -43.2075, 01, 01, 2021, 1609516569, 1609554356 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}

void test_solar_main_New_York_010121_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { 40.71427, -74.00597, 01, 01, 2021, 1609501634, 1609554486 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}

void test_solar_main_Bangalore_010121_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { 12.97194, 77.59369, 01, 01, 2021, 1609541739, 1609587185 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}

void test_solar_main_Christchurch_NZ_010121_UTC(void) {

    DEBUG_PRINT_FUNC_NAME();
    struct TestDataSet TestData = { -43.5288, 172.67865, 01, 01, 2021, 1609528488, 1609559708 };
    calculateSolarTriggers(&TestData);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunSetUtc, solar_clock_dataset.solar_sun_set);
    TEST_ASSERT_INT32_WITHIN(SOLAR_MARGIN_SEC, TestData.sunRiseUtc, solar_clock_dataset.solar_sun_rise);
}




/*
import time
import datetime
from suntime import Sun, SunTimeException
import os

os.environ["TZ"] = "UTC"
time.tzset()

def returnSolarTriggersFromToday(lat, long, today):
    sun = Sun(lat, long)
    today_ss = sun.get_sunset_time(today)
    tomorrow = today + datetime.timedelta(days=1)
    today_sr = sun.get_sunrise_time(tomorrow)    
    print('Today at given location the sun will set at {} UTC and rise tomorow at {} UTC'.
      format(today_ss.strftime("%d/%m/%Y %H:%M%S"), today_sr.strftime('%H:%M')))
    ssDate = datetime.datetime.strptime(today_ss.strftime("%d/%m/%Y %H:%M%S"), "%d/%m/%Y %H:%M%S").timetuple()
    srDate = datetime.datetime.strptime(today_sr.strftime("%d/%m/%Y %H:%M%S"), "%d/%m/%Y %H:%M%S").timetuple()
    return int(time.mktime(ssDate)), int(time.mktime(srDate))

testDate270620 = datetime.datetime(2020, 6, 27, 12, 0, 0, 0, None)
DateString = ', 27, 06, 2020, '
calcSS = 0
calcSR = 0

# London
latitude = 51.49472
longitude = -0.13527
print('London')
calcSS, calcSR = returnSolarTriggersFromToday(latitude, longitude, testDate270620)
print(latitude, ", ", longitude, DateString, calcSS, ", ", calcSR)

# Paris
latitude = 48.87375
longitude = 2.29494
print('Paris')
calcSS, calcSR = returnSolarTriggersFromToday(latitude, longitude, testDate270620)
print(latitude, ", ", longitude, DateString, calcSS, ", ", calcSR)

# Dubai
latitude = 25.276987
longitude = 55.296249
print('Dubai')
calcSS, calcSR = returnSolarTriggersFromToday(latitude, longitude, testDate270620)
print(latitude, ", ", longitude, DateString, calcSS, ", ", calcSR)

# Luxembourg
latitude = 49.61162
longitude = 6.13193
print('Luxembourg')
calcSS, calcSR = returnSolarTriggersFromToday(latitude, longitude, testDate270620)
print(latitude, ", ", longitude, DateString, calcSS, ", ", calcSR)

# Madrid
latitude = 40.416775
longitude = -3.703790
print('Madrid')
calcSS, calcSR = returnSolarTriggersFromToday(latitude, longitude, testDate270620)
print(latitude, ", ", longitude, DateString, calcSS, ", ", calcSR)

# Sydney
latitude = -33.865143
longitude = 151.209900
print('Madrid')
calcSS, calcSR = returnSolarTriggersFromToday(latitude, longitude, testDate270620)
print(latitude, ", ", longitude, DateString, calcSS, ", ", calcSR)

print('____________________________________________')
print(' ')

testDate270620 = datetime.datetime(2021, 1, 1, 12, 0, 0, 0, None)
DateString = ', 01, 01, 2021, '

# London
latitude = 51.49472
longitude = -0.13527
print('London')
calcSS, calcSR = returnSolarTriggersFromToday(latitude, longitude, testDate270620)
print(latitude, ", ", longitude, DateString, calcSS, ", ", calcSR)

# Paris
latitude = 48.87375
longitude = 2.29494
print('Paris')
calcSS, calcSR = returnSolarTriggersFromToday(latitude, longitude, testDate270620)
print(latitude, ", ", longitude, DateString, calcSS, ", ", calcSR)

# Dubai
latitude = 25.276987
longitude = 55.296249
print('Dubai')
calcSS, calcSR = returnSolarTriggersFromToday(latitude, longitude, testDate270620)
print(latitude, ", ", longitude, DateString, calcSS, ", ", calcSR)

# Luxembourg
latitude = 49.61162
longitude = 6.13193
print('Luxembourg')
calcSS, calcSR = returnSolarTriggersFromToday(latitude, longitude, testDate270620)
print(latitude, ", ", longitude, DateString, calcSS, ", ", calcSR)

# Madrid
latitude = 40.416775
longitude = -3.703790
print('Madrid')
calcSS, calcSR = returnSolarTriggersFromToday(latitude, longitude, testDate270620)
print(latitude, ", ", longitude, DateString, calcSS, ", ", calcSR)

# Sydney
latitude = -33.865143
longitude = 151.209900
print('Madrid')
calcSS, calcSR = returnSolarTriggersFromToday(latitude, longitude, testDate270620)
print(latitude, ", ", longitude, DateString, calcSS, ", ", calcSR)

*/